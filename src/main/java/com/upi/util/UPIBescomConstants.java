package com.upi.util;

import com.payqwikapp.util.DeploymentConstants;

public class UPIBescomConstants {

	
	private static final String URL = DeploymentConstants.WEB_URL+"/User/"; // Live
	
	public static final String URL_TRANSACTION = URL + "UPI/BesomCollectMoney";
	public static final String URL_MERCHANT_TOKEN = URL + "UPI/ValidateBesomToken";
	public static final String URL_UPI_TXN_AFTER_REDIRECT = URL + "UPI/UpiRedirectSuccess";
	public static final String URL_UPI_STATUS = URL + "UPI/UpiStatus";
	
	public static final String API_KEY_AMOUNT = "amount";
	public static final String API_KEY_PAYER_VIR_ADDR = "payerVirAddr";
	public static final String API_KEY_EXP_TIME = "expTime";
	public static final String API_KEY_MSG_ID = "msgId";
	
	public static final String REQ_DESCRIPTION = "UPI Load Money Request in VPayQwik Wallet of User";
	public static final String RES_DESCRIPTION = "UPI Load Money in VPayQwik Wallet of User";
	
	public static final String UAT_MERCHANT_VPA = "vpayqwik01@vijb";
	
	public static final String BESCOM_S2S_CALL_URL =  DeploymentConstants.WEB_URL + "/ws/api/BescomS2SCall";
	
	public static final String BECOM_RURAL_MERCHANT = "bescom@rural.in";
	public static final String BECOM_RURAL_KEY = "BESCOM";
	public static final String BECOM_RURAL_TOKEN = "VijayaBHIMUPI";
	public static final String BECOM_ARBAN_MERCHANT = "bescom@arban.in";
}
