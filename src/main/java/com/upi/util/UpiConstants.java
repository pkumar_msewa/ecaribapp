package com.upi.util;

import com.payqwikapp.util.DeploymentConstants;

public class UpiConstants {

	
	private static final String URL = DeploymentConstants.WEB_URL+"/User/"; // Live
	
	public static final String URL_TRANSACTION = URL + "UPI/CollectMoney";
	public static final String URL_MERCHANT_TOKEN = URL + "UPI/ValidateToken";
	public static final String URL_UPI_STATUS = URL + "UPI/UpiLoadStatus";

	public static final String API_KEY_AMOUNT = "amount";
	public static final String API_KEY_PAYER_VIR_ADDR = "payerVirAddr";
	public static final String API_KEY_EXP_TIME = "expTime";
	public static final String API_KEY_MSG_ID = "msgId";
	public static final String API_KEY_TXN_REF_ID = "refId";
	
	public static final String UPI_SERVICE_CODE = "LMU";
	
	public static final String REQ_DESCRIPTION = "UPI Load Money Request in VPayQwik Wallet of User";
	public static final String RES_DESCRIPTION = "Load Money Through UPI in VPayQw ";
	
	public static final String UAT_MERCHANT_VPA = "vpayqwik01@vijb";
	public static final String LIVE_MERCHANT_VPA = "vpayqwik101@vijb";
}
