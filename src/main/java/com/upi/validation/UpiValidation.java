package com.upi.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.validation.CommonValidation;
import com.upi.model.UpiErrorRequest;
import com.upi.model.UpiRequest;

public class UpiValidation {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public UpiValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public UpiErrorRequest upiValidation(UpiRequest request, String serviceCode) {
		UpiErrorRequest error = new UpiErrorRequest();
		boolean valid = true;
		/*if (CommonValidation.isNull(request.getAmount())) {
			valid = false;
			error.setErrMsg("please enter amount");
		}
		if(request)
		if (CommonValidation.isNull(request.getPayerVirAddr())) {
			valid = false;
			error.setErrMsg("please enter Payer Virtual Address");
		}*/

		PQService service = pqServiceRepository.findServiceByCode(serviceCode);
		if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				request.getAmount())) {
			error.setErrMsg(
					"Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

}
