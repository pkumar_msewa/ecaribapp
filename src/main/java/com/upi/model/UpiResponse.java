package com.upi.model;

import com.payqwikapp.model.mobile.ResponseStatus;

public class UpiResponse {
	private String code;
	private String status;
	private String message;
	private String referenceNo;
	private String upiId;
	private String merchantToken;

	public String getMerchantToken() {
		return merchantToken;
	}

	public void setMerchantToken(String merchantToken) {
		this.merchantToken = merchantToken;
	}

	public String getUpiId() {
		return upiId;
	}

	public void setUpiId(String upiId) {
		this.upiId = upiId;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setCode(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

}
