package com.upi.model;

public class UpiRequest {

	private String sessionId;
	private String amount;
	private String payerVirAddr;
	private String transactionRefNo;
	private String description;
	private String expTime;
	private String merchantVpa;
	private long mid;
	private String mTxnId;
	private String msgId;
	private boolean sdk;
	private String deviceId;
	private String txnPwd;
	private String payeeVirAddr;
	private String payeeMobile;
	private String kek;
	private String merchantId;
	private String vpaTerminalId;
	private String planDek;
	private String publicKey;
	private String orgId;
	private String subMerchantId;
	private String terminalId;

	public String getMerchantVpa() {
		return merchantVpa;
	}

	public void setMerchantVpa(String merchantVpa) {
		this.merchantVpa = merchantVpa;
	}

	public String getExpTime() {
		return expTime;
	}

	public void setExpTime(String expTime) {
		this.expTime = expTime;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayerVirAddr() {
		return payerVirAddr;
	}

	public void setPayerVirAddr(String payerVirAddr) {
		this.payerVirAddr = payerVirAddr;
	}

	public String getmTxnId() {
		return mTxnId;
	}

	public long getMid() {
		return mid;
	}

	public void setMid(long mid) {
		this.mid = mid;
	}

	public void setmTxnId(String mTxnId) {
		this.mTxnId = mTxnId;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public boolean isSdk() {
		return sdk;
	}

	public void setSdk(boolean sdk) {
		this.sdk = sdk;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	public String getTxnPwd() {
		return txnPwd;
	}

	public void setTxnPwd(String txnPwd) {
		this.txnPwd = txnPwd;
	}

	public String getPayeeVirAddr() {
		return payeeVirAddr;
	}

	public void setPayeeVirAddr(String payeeVirAddr) {
		this.payeeVirAddr = payeeVirAddr;
	}

	public String getPayeeMobile() {
		return payeeMobile;
	}

	public void setPayeeMobile(String payeeMobile) {
		this.payeeMobile = payeeMobile;
	}

	public String getKek() {
		return kek;
	}

	public void setKek(String kek) {
		this.kek = kek;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getVpaTerminalId() {
		return vpaTerminalId;
	}

	public void setVpaTerminalId(String vpaTerminalId) {
		this.vpaTerminalId = vpaTerminalId;
	}

	public String getPlanDek() {
		return planDek;
	}

	public void setPlanDek(String planDek) {
		this.planDek = planDek;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(String subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
}
