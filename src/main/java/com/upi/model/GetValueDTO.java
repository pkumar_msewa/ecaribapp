package com.upi.model;

public class GetValueDTO {

	private double poolBalance;
	private long totalTxns;
	private long totalSuccessTxns;
	private long totalFailedTxns;
	private long totalProccessTxns;

	public double getPoolBalance() {
		return poolBalance;
	}

	public void setPoolBalance(double poolBalance) {
		this.poolBalance = poolBalance;
	}

	public long getTotalTxns() {
		return totalTxns;
	}

	public void setTotalTxns(long totalTxns) {
		this.totalTxns = totalTxns;
	}

	public long getTotalSuccessTxns() {
		return totalSuccessTxns;
	}

	public void setTotalSuccessTxns(long totalSuccessTxns) {
		this.totalSuccessTxns = totalSuccessTxns;
	}

	public long getTotalFailedTxns() {
		return totalFailedTxns;
	}

	public void setTotalFailedTxns(long totalFailedTxns) {
		this.totalFailedTxns = totalFailedTxns;
	}

	public long getTotalProccessTxns() {
		return totalProccessTxns;
	}

	public void setTotalProccessTxns(long totalProccessTxns) {
		this.totalProccessTxns = totalProccessTxns;
	}
}
