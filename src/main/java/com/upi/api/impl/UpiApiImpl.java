package com.upi.api.impl;

import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.UpiBscmMerTokenDetails;
import com.payqwikapp.entity.UpiMerchantDetail;
import com.payqwikapp.entity.UpiSdkMerchantDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UPIResponse;
import com.payqwikapp.model.UpiMobileRedirectReq;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UpiBscmMerTokenDetailRepository;
import com.payqwikapp.repositories.UpiMerchantDetailRepository;
import com.payqwikapp.repositories.UpiSdkMerchantDetailsRepository;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.util.ConvertUtil;
import com.upi.api.IUpiApi;
import com.upi.model.MerchantTxnStatusResponse;
import com.upi.model.UpiRequest;
import com.upi.model.UpiResponse;
import com.upi.util.UPIBescomConstants;
import com.upi.util.UpiConstants;

public class UpiApiImpl implements IUpiApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final ITransactionApi transactionApi;
	private final UpiMerchantDetailRepository upiMerchantDetailRepository;
	private final UpiBscmMerTokenDetailRepository upiBscmMerTokenDetailRepository;
	private final UpiSdkMerchantDetailsRepository upiSdkMerchantDetailsRepository;

	public UpiApiImpl(ITransactionApi transactionApi, UpiMerchantDetailRepository upiMerchantDetailRepository,
			UpiBscmMerTokenDetailRepository upiBscmMerTokenDetailRepository, UpiSdkMerchantDetailsRepository upiSdkMerchantDetailsRepository) {
		this.transactionApi = transactionApi;
		this.upiMerchantDetailRepository = upiMerchantDetailRepository;
		this.upiBscmMerTokenDetailRepository = upiBscmMerTokenDetailRepository;
		this.upiSdkMerchantDetailsRepository = upiSdkMerchantDetailsRepository;
	}

	@Override
	public UpiResponse upiRequest(UpiRequest request) {
		UpiResponse response = new UpiResponse();
		try {
			if(request.isSdk()){
				logger.info("Inside true : : " + request.isSdk());
				logger.info("Merchant VPA :: ::: ::: " + request.getMerchantVpa());
				logger.info("Transaction ID : : : : : : : : : : :  " + request.getTransactionRefNo());
				response = validateUpiSdkMerchantToken(request);
			}else{
				logger.info("Merchant VPA :: ::: ::: " + request.getMerchantVpa());
				logger.info("Transaction ID : : : : : : : : : : :  " + request.getTransactionRefNo());
				response = validateUpiMerchantToken(request);
				logger.info("After Validate Token Repsonse is --------- ---------------- --------------  " + response.getCode());
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					String stringResponse = "";
					Client colMoney = new Client();
					colMoney.addFilter(new LoggingFilter(System.out));
					WebResource resource = colMoney.resource(UpiConstants.URL_TRANSACTION)
							.queryParam(UpiConstants.API_KEY_AMOUNT,
									(request.getAmount() == null) ? "" : request.getAmount())
							.queryParam(UpiConstants.API_KEY_PAYER_VIR_ADDR,
									(request.getPayerVirAddr() == null) ? "" : request.getPayerVirAddr())
							.queryParam(UpiConstants.API_KEY_TXN_REF_ID,(request.getTransactionRefNo() == null) ? "" : request.getTransactionRefNo());
					ClientResponse clientResponse = resource.header("expTime", request.getExpTime())
							.get(ClientResponse.class);
					stringResponse = clientResponse.getEntity(String.class);
					if (clientResponse.getStatus() == 200) {
						JSONObject jobj = new JSONObject(stringResponse);
						if (jobj.getString("code").equals(ResponseStatus.SUCCESS.getValue())) {
							response.setCode(ResponseStatus.SUCCESS);
							response.setMessage(jobj.getString("message"));
							response.setReferenceNo(jobj.getString("referenceNo"));
							response.setUpiId(jobj.getString("msgId"));
							return response;
						} else {
							response.setCode(ResponseStatus.FAILURE);
							response.setMessage(jobj.getString("message"));
							return response;
						}
					} else {
						response.setCode(ResponseStatus.BAD_REQUEST);
						response.setMessage("Bad Request");
					}
				}
			}
		} catch (Exception e) {
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		logger.info("Token : : " + response.getMerchantToken());
		return response;
	}

	@Override
	public ResponseDTO upiHandler(UpiRequest request, String username, PQService service) {
		ResponseDTO result = new ResponseDTO();
		String transactionRefNo = "" + System.currentTimeMillis();
		try {
			double amount = Double.parseDouble(request.getAmount());
			request.setTransactionRefNo(transactionRefNo);
			result.setTxnId(transactionRefNo);
			transactionApi.initiateUPILoadMoney(amount, UpiConstants.REQ_DESCRIPTION + " " + amount, service, transactionRefNo, username, request.isSdk());
			if(!request.isSdk()){
				UpiResponse upiResp = upiRequest(request);
				if (upiResp.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Load Money request has been initiated and a notification has been sent to mobile");
					result.setDetails(upiResp);
					transactionApi.updateUpiTrasaction(request.getTransactionRefNo(), upiResp.getUpiId(),
							upiResp.getReferenceNo());
					return result;
				} else {
					transactionApi.failedUPILoadMoneyWithTxnRef(transactionRefNo);
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Failed, " + upiResp.getMessage());
					result.setDetails(upiResp);
					return result;
				}
			}else{
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Load Money request has been initiated successfully");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			transactionApi.failedUPILoadMoneyWithTxnRef(transactionRefNo);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			result.setMessage("Internal Server Error");
			return result;
		}
	}

	@Override
	public ResponseDTO upiResponseHandler(UPIResponse dto) {
		ResponseDTO result = new ResponseDTO();
		if (dto.getRespCode().equalsIgnoreCase("0")) {
			transactionApi.successUPILoadMoney(dto.getReferenceId());
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Load Money Successful");
		} else if (dto.getRespCode().equalsIgnoreCase("1")) {
			transactionApi.failedUPILoadMoney(dto.getReferenceId());
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Load Money failed");
		}
		return result;
	}

	@Override
	public ResponseDTO upiSdkResponseHandler(UpiMobileRedirectReq dto) {
		ResponseDTO result = new ResponseDTO();
		try{
			
			PQTransaction txnExits = transactionApi.getTransactionByRefNo(dto.getTxnRefNo()+"C");
			if(txnExits != null){
				if(txnExits.getStatus().equals(Status.Success)){
					/*MerchantTxnStatusResponse response =  upiCrossVerificationApi(dto);
					if(response.getCode().equals(Status.Success.getValue())){
						if(response.equals("SUCCESS")){
							// TODO call success methods
							if (dto.getTxnsRespCode().equals("000")) {
								if (dto.getStatus().equalsIgnoreCase("Success")) {
									transactionApi.successUPISdkLoadMoney(dto);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Load Money Successful");
								} else if (dto.getStatus().equalsIgnoreCase("Failure")) {
									transactionApi.failedUPISdkLoadMoney(dto);
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Load Money failed");
								}
							} else {
								transactionApi.failedUPISdkLoadMoney(dto);
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Load Money failed");
							}
						}else if(response.equals("DECLINED")){
							// TODO Fail the txns
							transactionApi.failedUPISdkLoadMoney(dto);
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Load Money failed");
						}else{
							// TODO tail the txns
							transactionApi.failedUPISdkLoadMoney(dto);
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Load Money failed");
						}
					}else{
						// TODO Fail the txns
						transactionApi.failedUPISdkLoadMoney(dto);
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Load Money failed");
					}*/
					
					if (dto.getTxnsRespCode().equals("000")) {
						if (dto.getStatus().equalsIgnoreCase("Success")) {
							transactionApi.successUPISdkLoadMoney(dto);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Load Money Successful");
						} else if (dto.getStatus().equalsIgnoreCase("Failure")) {
							transactionApi.failedUPISdkLoadMoney(dto);
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Load Money failed");
						}else{
							transactionApi.failedUPISdkLoadMoney(dto);
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Load Money failed");
						}
					} else {
						transactionApi.failedUPISdkLoadMoney(dto);
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Load Money failed");
					}
					
				}else{
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Txns already processed");
				}
			}else{
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Txns Id doesn't exits");
			}
			
		}catch (Exception e){
			e.printStackTrace();
			/*transactionApi.failedUPISdkLoadMoney(dto);*/
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Load Money failed");
		}
		return result;
	}

	@Override
	public UpiResponse validateUpiMerchantToken(UpiRequest request) {
		// TODO Auto-generated method stub
		UpiResponse response = new UpiResponse();
		try {
			UpiMerchantDetail md = upiMerchantDetailRepository.getUpiMerchant(request.getMerchantVpa());
			if (md == null) {
				md = new UpiMerchantDetail();
				response = getMerchantResponse(request);
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					md.setExpTime(new Date());
					md.setMerchantVpa(request.getMerchantVpa());
					md.setMerchantToken(response.getMerchantToken());
					upiMerchantDetailRepository.save(md);
					response.setCode(ResponseStatus.SUCCESS);
					System.err.println("Merchant Details Saved First Time");
				}
			} else {
				Date date = md.getExpTime();
				if (date != null) {
					long lastTransactionTimeStamp = date.getTime();
					System.out.println("lastTransactionTimeStamp  ----  --------  " + lastTransactionTimeStamp);
					long currentTimeInMillis = System.currentTimeMillis();
					System.out.println("currentTimeInMillis  ----  --------  " + currentTimeInMillis);
					System.out.println("Diffe :: " + (currentTimeInMillis - lastTransactionTimeStamp));
					System.out.println(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp));
					if (!(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp))) {
						System.err.println("Time Expired...!! Call API");
						response = getMerchantResponse(request);
						if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							md.setExpTime(new Date());
							md.setMerchantVpa(request.getMerchantVpa());
							md.setMerchantToken(response.getMerchantToken());
							upiMerchantDetailRepository.save(md);
							response.setCode(ResponseStatus.SUCCESS);
						}
					} else {
						response.setCode(ResponseStatus.SUCCESS);
						response.setMerchantToken(md.getMerchantToken());
						System.err.println("You can use");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		System.err.println("Merchant Token thier : : " + response.getMerchantToken());
		return response;
	}
	
	@Override
	public UpiResponse validateUpiSdkMerchantToken(UpiRequest request) {
		// TODO Auto-generated method stub
		UpiResponse response = new UpiResponse();
		try {
			UpiSdkMerchantDetails md = upiSdkMerchantDetailsRepository.getUpiMerchant(request.getMerchantVpa());
			if (md == null) {
				md = new UpiSdkMerchantDetails();
				response = getMerchantResponse(request);
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					md.setExpTime(new Date());
					md.setMerchantVpa(request.getMerchantVpa());
					md.setMerchantToken(response.getMerchantToken());
					upiSdkMerchantDetailsRepository.save(md);
					response.setCode(ResponseStatus.SUCCESS);
					System.err.println("Merchant Details Saved First Time");
				}
			} else {
				Date date = md.getExpTime();
				if (date != null) {
					long lastTransactionTimeStamp = date.getTime();
					System.out.println("lastTransactionTimeStamp  ----  --------  " + lastTransactionTimeStamp);
					long currentTimeInMillis = System.currentTimeMillis();
					System.out.println("currentTimeInMillis  ----  --------  " + currentTimeInMillis);
					System.out.println("Diffe :: " + (currentTimeInMillis - lastTransactionTimeStamp));
					System.out.println("true or false ::" + (1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp)));
					if (!(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp))) {
						System.err.println("Time Expired...!! Call API");
						response = getMerchantResponse(request);
						if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							md.setExpTime(new Date());
							md.setMerchantVpa(request.getMerchantVpa());
							md.setMerchantToken(response.getMerchantToken());
							upiSdkMerchantDetailsRepository.save(md);
							response.setCode(ResponseStatus.SUCCESS);
						}
					} else {
						response.setCode(ResponseStatus.SUCCESS);
						response.setMerchantToken(md.getMerchantToken());
						System.err.println("You can use");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		System.err.println("Merchant Token thier : : " + response.getMerchantToken());
		return response;
	}

	UpiResponse getMerchantResponse(UpiRequest request) {
		UpiResponse response = new UpiResponse();
		try {
			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UpiConstants.URL_MERCHANT_TOKEN);
			ClientResponse clientResponse = resource.header("sdk", request.isSdk())
					.header("deviceId", request.getDeviceId()).get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				if (jobj.getString("code").equals(ResponseStatus.SUCCESS.getValue())) {
					response.setCode(ResponseStatus.SUCCESS);
					response.setMessage(jobj.getString("message"));
					response.setMerchantToken(jobj.getString("token"));
				} else {
					response.setCode(ResponseStatus.FAILURE);
					response.setMessage(jobj.getString("message"));
				}
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST);
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	// TODO BESCOM UPI Integration Process
	@Override
	public UpiResponse upiRequestMerchant(UpiRequest request) {
		UpiResponse response = new UpiResponse();
		try {
			response = validateUpiMerchantTokenForMerchant(request);
			System.out.println("After Validate Token Repsonse is --------- ---------------- --------------  "
					+ response.getCode());
			if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				String stringResponse = "";
				Client colMoney = new Client();
				colMoney.addFilter(new LoggingFilter(System.out));
				WebResource resource = colMoney.resource(UPIBescomConstants.URL_TRANSACTION)
						.queryParam(UPIBescomConstants.API_KEY_AMOUNT, (request.getAmount() == null) ? "" : request.getAmount())
						.queryParam(UPIBescomConstants.API_KEY_PAYER_VIR_ADDR, (request.getMerchantVpa() == null) ? "" : request.getMerchantVpa())
						.queryParam("txnPwd", (request.getTxnPwd() == null) ? "" : request.getTxnPwd())
						.queryParam("payeeVirAddr", (request.getPayeeVirAddr() == null) ? "" : request.getPayeeVirAddr())
						.queryParam("payeeMobile", (request.getPayeeMobile() == null) ? "" : request.getPayeeMobile())
						.queryParam("kek", (request.getKek() == null) ? "" : request.getKek())
						.queryParam("merchantId", (request.getMerchantId() == null) ? "" : request.getMerchantId())
						.queryParam("vpaTerminalId", (request.getVpaTerminalId() == null) ? "" : request.getVpaTerminalId())
						.queryParam("planDek", (request.getPlanDek() == null) ? "" : request.getPlanDek())
						.queryParam("publicKey", (request.getPublicKey() == null) ? "" : request.getPublicKey())
						.queryParam("orgId", (request.getOrgId() == null) ? "" : request.getOrgId())
						.queryParam("subMerchantId", (request.getSubMerchantId() == null) ? "" : request.getSubMerchantId())
						.queryParam("terminalId", (request.getTerminalId() == null) ? "" : request.getTerminalId());
				ClientResponse clientResponse = resource.header("expTime", request.getExpTime()).get(ClientResponse.class);
				stringResponse = clientResponse.getEntity(String.class);
				if (clientResponse.getStatus() == 200) {
					JSONObject jobj = new JSONObject(stringResponse);
					if (jobj.getString("code").equals(ResponseStatus.SUCCESS.getValue())) {
						response.setCode(ResponseStatus.SUCCESS);
						response.setMessage(jobj.getString("message"));
						response.setReferenceNo(jobj.getString("referenceNo"));
						response.setUpiId(jobj.getString("msgId"));
						return response;
					} else {
						response.setCode(ResponseStatus.FAILURE);
						response.setMessage(jobj.getString("message"));
						return response;
					}
				} else {
					response.setCode(ResponseStatus.BAD_REQUEST);
					response.setMessage("Bad Request");
				}
			}
		} catch (Exception e) {
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	@Override
	public ResponseDTO upiHandlerMerchant(UpiRequest request, User merchant, PQService service) {
		ResponseDTO result = new ResponseDTO();
		String transactionRefNo = "" + System.currentTimeMillis();
		try {
			String name = merchant.getUserDetail().getFirstName() + " " + merchant.getUserDetail().getLastName();
			String description = "UPI Payment of " + request.getAmount() + " to " + name + " of " + request.getmTxnId();
			double amount = Double.parseDouble(request.getAmount());
			request.setTransactionRefNo(transactionRefNo);
			result.setTransactionRefNO(transactionRefNo);
			
			PQTransaction txn = transactionApi.initiateMerchantUPIPayent(amount, description, service, transactionRefNo,
					merchant, request.getmTxnId());
			result.setTransactionDate(ConvertUtil.convertDate(txn.getCreated()));
			
			UpiResponse upiResp = upiRequestMerchant(request);
			if (upiResp.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Request has been initiated and a notification has been sent to mobile please check and approve the transaction");
				result.setDetails(upiResp);
				transactionApi.updateMerchantUPIPayentTrasaction(request.getTransactionRefNo(), upiResp.getUpiId(),
						upiResp.getReferenceNo());
				return result;
			} else {
				transactionApi.failedUPILoadMoneyWithTxnRef(transactionRefNo);
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Failed, " + upiResp.getMessage());
				result.setDetails(upiResp);
				return result;
			}
		} catch (Exception e) {
			transactionApi.failedUPILoadMoneyWithTxnRef(transactionRefNo);
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			result.setMessage("Internal Server Error");
			return result;
		}
	}

	@Override
	public ResponseDTO upiResponseHandlerMerchant(UPIResponse dto) {
		ResponseDTO result = new ResponseDTO();
		String newRefNo = System.currentTimeMillis() + "";
		if (dto.getRespCode().equalsIgnoreCase("0")) {
			transactionApi.successMerchantUPIPayent(dto.getReferenceId(), dto.getConsumerId(), dto.getOrgRefId());
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Load Money Successful");
		} else if (dto.getRespCode().equalsIgnoreCase("1")) {
			transactionApi.failedMerchantUPIPayent(dto.getReferenceId(), dto.getOrgRefId());
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Load Money failed");
		}
		result.setTxnId(newRefNo);
		return result;
	}

	@Override
	public UpiResponse validateUpiMerchantTokenForMerchant(UpiRequest request) {
		// TODO Auto-generated method stub
		UpiResponse response = new UpiResponse();
		try {
			UpiBscmMerTokenDetails md = upiBscmMerTokenDetailRepository.getUpiMerchant(request.getMerchantVpa());
			if (md == null) {
				md = new UpiBscmMerTokenDetails();
				response = getMerchantResponseMerchant(request);
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					md.setExpTime(new Date());
					md.setMerchantVpa(request.getMerchantVpa());
					md.setMerchantToken(response.getMerchantToken());
					upiBscmMerTokenDetailRepository.save(md);
					response.setCode(ResponseStatus.SUCCESS);
					System.err.println("Merchant Details Saved First Time");
				}
			} else {
				Date date = md.getExpTime();
				if (date != null) {
					long lastTransactionTimeStamp = date.getTime();
					System.out.println("lastTransactionTimeStamp  ----  --------  " + lastTransactionTimeStamp);
					long currentTimeInMillis = System.currentTimeMillis();
					System.out.println("currentTimeInMillis  ----  --------  " + currentTimeInMillis);
					System.out.println("Diffe :: " + (currentTimeInMillis - lastTransactionTimeStamp));
					System.out.println(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp));
					if (!(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp))) {
						System.err.println("Time Expired...!! Call API");
						response = getMerchantResponseMerchant(request);
						if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							md.setExpTime(new Date());
							md.setMerchantVpa(request.getMerchantVpa());
							md.setMerchantToken(response.getMerchantToken());
							upiBscmMerTokenDetailRepository.save(md);
							response.setCode(ResponseStatus.SUCCESS);
						}
					} else {
						response.setCode(ResponseStatus.SUCCESS);
						System.err.println("You can use");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	UpiResponse getMerchantResponseMerchant(UpiRequest request) {
		UpiResponse response = new UpiResponse();
		try {
			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UPIBescomConstants.URL_MERCHANT_TOKEN)
					.queryParam("txnPwd", (request.getTxnPwd() == null) ? "" : request.getTxnPwd())
					.queryParam("payeeVirAddr", (request.getPayeeVirAddr() == null) ? "" : request.getPayeeVirAddr())
					.queryParam("payeeMobile", (request.getPayeeMobile() == null) ? "" : request.getPayeeMobile())
					.queryParam("kek", (request.getKek() == null) ? "" : request.getKek())
					.queryParam("merchantId", (request.getMerchantId() == null) ? "" : request.getMerchantId())
					.queryParam("vpaTerminalId", (request.getVpaTerminalId() == null) ? "" : request.getVpaTerminalId())
					.queryParam("planDek", (request.getPlanDek() == null) ? "" : request.getPlanDek())
					.queryParam("publicKey", (request.getPublicKey() == null) ? "" : request.getPublicKey())
					.queryParam("orgId", (request.getOrgId() == null) ? "" : request.getOrgId())
					.queryParam("subMerchantId", (request.getSubMerchantId() == null) ? "" : request.getSubMerchantId())
					.queryParam("terminalId", (request.getTerminalId() == null) ? "" : request.getTerminalId());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				if (jobj.getString("code").equals(ResponseStatus.SUCCESS.getValue())) {
					response.setCode(ResponseStatus.SUCCESS);
					response.setMessage(jobj.getString("message"));
					response.setMerchantToken(jobj.getString("token"));
				} else {
					response.setCode(ResponseStatus.FAILURE);
					response.setMessage(jobj.getString("message"));
				}
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST);
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}
	
	MerchantTxnStatusResponse upiCrossVerificationApi(UpiMobileRedirectReq request){
		MerchantTxnStatusResponse response = new MerchantTxnStatusResponse();
		try {
			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UpiConstants.URL_UPI_STATUS)
					.queryParam(UPIBescomConstants.API_KEY_MSG_ID, (request.getUpiId() == null) ? "" : request.getUpiId());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				response.setValid(true);
				response.setCode(ResponseStatus.SUCCESS.getValue());
				response.setReferenceNo(jobj.getString("referenceNo"));
				response.setStatus(jobj.getString("upiStatus"));
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST.getValue());
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			response.setMessage("Internal Server Error");
		}
		return response;
	}
}
