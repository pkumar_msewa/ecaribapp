package com.upi.api;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.UPIResponse;
import com.payqwikapp.model.UpiMobileRedirectReq;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.upi.model.MerchantTxnStatusResponse;
import com.upi.model.UpiRequest;
import com.upi.model.UpiResponse;

public interface IUpiApi {

	UpiResponse upiRequest(UpiRequest request);
	
	ResponseDTO upiHandler(UpiRequest request, String username, PQService service);
	
	ResponseDTO upiResponseHandler(UPIResponse dto);
	
	ResponseDTO upiSdkResponseHandler(UpiMobileRedirectReq dto);
	
	UpiResponse validateUpiMerchantToken(UpiRequest request);
	
	UpiResponse validateUpiSdkMerchantToken(UpiRequest request);
	
	// TODO BESCOM UPI Integration Process\
	
	ResponseDTO upiHandlerMerchant(UpiRequest request, User merchant, PQService service);
	
	UpiResponse upiRequestMerchant(UpiRequest request);
	
	ResponseDTO upiResponseHandlerMerchant(UPIResponse dto);
	
	UpiResponse validateUpiMerchantTokenForMerchant(UpiRequest request);
}
