package com.m2p.util;

import com.payqwikapp.util.DeploymentConstants;

public class M2PConstants {

    private static final String BASE_URL = DeploymentConstants.WEB_URL+"/M2P/";
    //private static final String BASE_URL = DeploymentConstants.WEB_URL+"/M2P/";
    public static final String MERCHANT_REGISTRATION = BASE_URL+"AddVisaMerchant";
    public static final String GET_QR = BASE_URL+"GetLiveQRCode";
    public static final String MERCHANT_REGISTRATION_LIVE = BASE_URL+"AddLiveVisaMerchant";

}
