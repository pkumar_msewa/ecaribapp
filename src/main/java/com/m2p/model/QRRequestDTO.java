package com.m2p.model;

import com.thirdparty.model.request.JSONWrapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class QRRequestDTO implements JSONWrapper{
	
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	@Override
	public JSONObject toJSON() {
		JSONObject payload = new JSONObject();
		try {
			payload.put("username",getUsername());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return payload;
	}
}
