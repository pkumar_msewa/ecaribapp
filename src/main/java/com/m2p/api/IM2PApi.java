package com.m2p.api;

import com.m2p.model.QRRequestDTO;
import com.m2p.model.QRResponseDTO;
import com.m2p.model.VisaMerchantRequest;
import com.m2p.model.VisaSignUpResponse;
import com.payqwikapp.model.merchant.MerchantSignupDTO;

public interface IM2PApi {

    VisaSignUpResponse request(VisaMerchantRequest request);

    VisaSignUpResponse requestForLive(MerchantSignupDTO request);

    QRResponseDTO request(QRRequestDTO request);

}
