package com.m2p.api.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import com.m2p.api.IM2PApi;
import com.m2p.model.QRRequestDTO;
import com.m2p.model.QRResponseDTO;
import com.m2p.model.VisaMerchantRequest;
import com.m2p.model.VisaSignUpResponse;
import com.m2p.util.M2PConstants;
import com.payqwik.visa.utils.SecurityTest;
import com.payqwikapp.model.merchant.MerchantSignupDTO;
import com.payqwikapp.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class M2PApi implements IM2PApi{


    @Override
    public VisaSignUpResponse request(VisaMerchantRequest request) {
        VisaSignUpResponse response = new VisaSignUpResponse();
        try {
            String stringResponse = "";
            WebResource resource = Client.create().resource(M2PConstants.MERCHANT_REGISTRATION);
            ClientResponse clientResponse = resource.post(ClientResponse.class,request.toJSON());
                stringResponse = clientResponse.getEntity(String.class);
                System.err.println("response::"+stringResponse);
            if (clientResponse.getStatus() == 200) {
                JSONObject o = new JSONObject(stringResponse);
                String code = JSONParserUtil.getString(o,"code");
                response.setCode(code);
                response.setMessage(JSONParserUtil.getString(o,"message"));
                if(code.equalsIgnoreCase("S00")){
                    response.setmVisaId(JSONParserUtil.getString(o,"mVisaId"));
                    response.setEntityId(JSONParserUtil.getString(o,"entityId"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public VisaSignUpResponse requestForLive(MerchantSignupDTO request) {
        VisaSignUpResponse response = new VisaSignUpResponse();
        try {
            String stringResponse = "";
            SecurityTest securityTest = new SecurityTest();
            ObjectMapper mapper = new ObjectMapper();
            String req = mapper.writeValueAsString(request);
            System.err.println("req::"+req);
//            String encrypted = securityTest.encodeRequest(req,"9876543211234562","VIJAYA");
            Client client=Client.create();
            WebResource resource = client.resource(M2PConstants.MERCHANT_REGISTRATION_LIVE);
            org.codehaus.jettison.json.JSONObject payload = new org.codehaus.jettison.json.JSONObject();
            payload.put("request",req);
            ClientResponse clientResponse = resource.post(ClientResponse.class,payload);
            stringResponse = clientResponse.getEntity(String.class);
            System.err.println("response::"+stringResponse);
            if (clientResponse.getStatus() == 200) {
//                String decrytped = securityTest.decodeResponse(mapper.readValue(stringResponse, YappayEncryptedResponse.class));
//                System.err.println("decrypted ::"+decrytped);
                JSONObject resp = new JSONObject(stringResponse);
                if (resp != null) {
                    JSONObject result = resp.getJSONObject("result");
                    if (result != null) {
                        response.setCode("S00");
                        response.setEntityId(JSONParserUtil.getString(result, "customerId"));
                        response.setCustomerId(JSONParserUtil.getString(result, "customerId"));
                        response.setmVisaId(JSONParserUtil.getString(result, "mvisaId"));
                        response.setRupayId(JSONParserUtil.getString(result,"rupayId"));
                    } else {
                        response.setCode("F00");
                        response.setMessage("Service Unavailable");
                    }
                }else {
                    response.setCode("F00");
                    response.setMessage("Service Unavailable");
                }
            }else {
                response.setCode("F00");
                response.setMessage("Service Unavailable");
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.setCode("F00");
            response.setMessage("Service Unavailable");
        }
        return response;
    }

    @Override
    public QRResponseDTO request(QRRequestDTO request) {
        QRResponseDTO response = new QRResponseDTO();
        try {
            String stringResponse = "";
            WebResource resource = Client.create().resource(M2PConstants.GET_QR);
            ClientResponse clientResponse = resource.post(ClientResponse.class,request.toJSON());
                stringResponse = clientResponse.getEntity(String.class);
                System.err.println("response::"+stringResponse);
            if (clientResponse.getStatus() == 200) {
                JSONObject o = new JSONObject(stringResponse);
                String code = JSONParserUtil.getString(o,"code");
                response.setCode(code);
                response.setMessage(JSONParserUtil.getString(o,"message"));
                if(code.equalsIgnoreCase("S00")){
                    response.setEntityId(JSONParserUtil.getString(o,"entityId"));
                    response.setQrCode(JSONParserUtil.getString(o,"qrCode"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
