package com.payqwikapp.startup;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.ImagicaSessionRepository;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;

public class YuppTvCreator {
	
	    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	    private final UserRepository userRepository;
	    private final UserDetailRepository userDetailRepository;
	    private final PQServiceRepository serviceRepository;
	    private final PQServiceTypeRepository serviceTypeRepository;
	    private final ICommissionApi commissionApi;
	    private final PQAccountDetailRepository accountDetailRepository;
	    private final PQAccountTypeRepository accountTypeRepository;
	    private final PasswordEncoder passwordEncoder;
	    private final PQOperatorRepository operatorRepository;
	    public YuppTvCreator(UserRepository userRepository, UserDetailRepository userDetailRepository, PQServiceRepository serviceRepository, PQServiceTypeRepository serviceTypeRepository, ICommissionApi commissionApi, PQAccountDetailRepository accountDetailRepository, PQAccountTypeRepository accountTypeRepository,PasswordEncoder passwordEncoder,PQOperatorRepository operatorRepository) {
	        this.userRepository = userRepository;
	        this.userDetailRepository = userDetailRepository;
	        this.serviceRepository = serviceRepository;
	        this.serviceTypeRepository = serviceTypeRepository;
	        this.commissionApi = commissionApi;
	        this.accountDetailRepository = accountDetailRepository;
	        this.accountTypeRepository = accountTypeRepository;
	        this.passwordEncoder = passwordEncoder;
	        this.operatorRepository = operatorRepository;
	    }
	    
	    public void create(){
	    	createYuppTVUser();
	    	createYuppTVService();
    }

    private void createYuppTVUser(){
        User yuppTv = userRepository.findByUsername(StartupUtil.YUPPTV);
        if (yuppTv == null) {
            UserDetail detail = new UserDetail();
            detail.setAddress("Karnataka");
            detail.setContactNo("7022620747");
            detail.setFirstName("YuppTv");
            detail.setMiddleName("_");
            detail.setLastName(" ");
            detail.setEmail(StartupUtil.YUPPTV);
            userDetailRepository.save(detail);

            PQAccountDetail pqAccountDetail = new PQAccountDetail();
            pqAccountDetail.setBalance(0);
            pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
            pqAccountDetail.setAccountType(accountTypeRepository.findByCode("KYC"));
            accountDetailRepository.save(pqAccountDetail);

            yuppTv = new User();
            yuppTv.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
            yuppTv.setPassword(passwordEncoder.encode("9980672277"));
            yuppTv.setCreated(new Date());
            yuppTv.setMobileStatus(Status.Active);
            yuppTv.setEmailStatus(Status.Active);
            yuppTv.setUserType(UserType.Merchant);
            yuppTv.setUsername(detail.getEmail());
            yuppTv.setUserDetail(detail);
            yuppTv.setAccountDetail(pqAccountDetail);
            userRepository.save(yuppTv);
        }

    }

    private void createYuppTVService() {

        PQOperator imagicaOperator = operatorRepository.findOperatorByName("YUPPTV");
        if (imagicaOperator == null) {
            imagicaOperator = new PQOperator();
            imagicaOperator.setName("YUPPTV");
            imagicaOperator.setStatus(Status.Active);
            operatorRepository.save(imagicaOperator);
        }

        PQServiceType yuppTvService = serviceTypeRepository.findServiceTypeByName("YUPPTV Subscription");
        if (yuppTvService == null) {
            yuppTvService = new PQServiceType();
            yuppTvService.setName("YUPPTV Subscription");
            yuppTvService.setDescription("YuppTV Subscription in VPayQwik");
            yuppTvService = serviceTypeRepository.save(yuppTvService);
        }

        PQService yuppTVService = serviceRepository.findServiceByCode(StartupUtil.YuppTv_SERVICE);
        if (yuppTVService == null) {
            yuppTVService = new PQService();
            yuppTVService.setName("YUPPTV");
            yuppTVService.setDescription("YuppTV Subscription in VPayQwik");
            yuppTVService.setMinAmount(10);
            yuppTVService.setMaxAmount(100000);
            yuppTVService.setCode(StartupUtil.YuppTv_SERVICE);
            yuppTVService.setOperator(imagicaOperator);
            yuppTVService.setOperatorCode(StartupUtil.YuppTv_SERVICE);
            yuppTVService.setStatus(Status.Active);
            yuppTVService.setServiceType(yuppTvService);
            serviceRepository.save(yuppTVService);
        }

        PQCommission yuppTvCommission = new PQCommission();
        yuppTvCommission.setMinAmount(10);
        yuppTvCommission.setMaxAmount(100000);
        yuppTvCommission.setType("POST");
        yuppTvCommission.setValue(0);
        yuppTvCommission.setFixed(true);
        yuppTvCommission.setService(yuppTVService);
        yuppTvCommission.setIdentifier(commissionApi.createCommissionIdentifier(yuppTvCommission));
        if (commissionApi.findCommissionByIdentifier(yuppTvCommission.getIdentifier()) == null) {
            commissionApi.save(yuppTvCommission);
        }


    }

}
