package com.payqwikapp.startup;

import com.payqwikapp.entity.ApiVersion;
import com.payqwikapp.entity.VersionLogs;
import com.payqwikapp.repositories.ApiVersionRepository;
import com.payqwikapp.repositories.VersionLogsRepository;

public class VersionControlCreator {

	private final VersionLogsRepository versionLogsRepository;
	private final ApiVersionRepository apiVersionRepository;
	public VersionControlCreator(VersionLogsRepository versionLogsRepository,
			ApiVersionRepository apiVersionRepository) {
		super();
		this.versionLogsRepository = versionLogsRepository;
		this.apiVersionRepository = apiVersionRepository;
	}
	
	public void create(){
		
		VersionLogs version=new VersionLogs();
		version.setVersion("1.2.4");
		version.setStatus("Active");
		ApiVersion clientApilogs=apiVersionRepository.getByNameStatus("Android", "Active");
		version.setAndroidVersion(clientApilogs);
		versionLogsRepository.save(version);
	}
}
