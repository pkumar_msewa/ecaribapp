package com.payqwikapp.startup;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.instantpay.util.InstantPayConstants;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.ServiceStatus;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.entity.YTVKeys;
import com.payqwikapp.model.Gender;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.ServiceStatusRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.YtvKeysRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.CommonValidation;

public class StartupCreator {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final ServiceStatusRepository serviceStatusRepository;
	private final YtvKeysRepository ytvKeysRepository;

	public StartupCreator(UserRepository userRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, PQAccountDetailRepository pqAccountDetailRepository,
			PQAccountTypeRepository pqAccountTypeRepository, ServiceStatusRepository serviceStatusRepository,YtvKeysRepository ytvKeysRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.serviceStatusRepository = serviceStatusRepository;
		this.ytvKeysRepository=ytvKeysRepository;
	}

	public void create() throws IOException {

		PQAccountType acTypeKYC = pqAccountTypeRepository.findByCode(StartupUtil.KYC);
		if (acTypeKYC == null) {
			acTypeKYC = new PQAccountType();
			acTypeKYC.setCode(StartupUtil.KYC);
			acTypeKYC.setDailyLimit(50000);
			acTypeKYC.setMonthlyLimit(50000);
			acTypeKYC.setBalanceLimit(50000);
			acTypeKYC.setName("KYC");
			acTypeKYC.setDescription("Verified Account");
			acTypeKYC.setTransactionLimit(100);
			pqAccountTypeRepository.save(acTypeKYC);
		}

		PQAccountType acTypeSuperKYC = pqAccountTypeRepository.findByCode(StartupUtil.SUPER_KYC);
		if (acTypeSuperKYC == null) {
			acTypeSuperKYC = new PQAccountType();
			acTypeSuperKYC.setCode(StartupUtil.SUPER_KYC);
			acTypeSuperKYC.setDailyLimit(5000000);
			acTypeSuperKYC.setMonthlyLimit(5000000);
			acTypeSuperKYC.setBalanceLimit(5000000);
			acTypeSuperKYC.setName("KYC - MERCHANT");
			acTypeSuperKYC.setDescription("Verified Account");
			acTypeSuperKYC.setTransactionLimit(10000000);
			pqAccountTypeRepository.save(acTypeSuperKYC);
		}

		PQAccountType acTypeNonKYC = pqAccountTypeRepository.findByCode(StartupUtil.NON_KYC);
		if (acTypeNonKYC == null) {
			acTypeNonKYC = new PQAccountType();
			acTypeNonKYC.setCode(StartupUtil.NON_KYC);
			acTypeNonKYC.setDailyLimit(20000);
			acTypeNonKYC.setMonthlyLimit(20000);
			acTypeNonKYC.setBalanceLimit(20000);
			acTypeNonKYC.setName("Non - KYC");
			acTypeNonKYC.setDescription("Unverified Account");
			acTypeNonKYC.setTransactionLimit(5);
			pqAccountTypeRepository.save(acTypeNonKYC);
		}

		User admin = userRepository.findByUsername(StartupUtil.ADMIN);
		if (admin == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("JP Nagar");
			detail.setContactNo(StartupUtil.ADMIN);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Admin");
			detail.setEmail(StartupUtil.ADMIN);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			admin = new User();
			admin.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
			admin.setPassword(passwordEncoder.encode("admin@1234"));
			admin.setCreated(new Date());
			admin.setMobileStatus(Status.Active);
			admin.setEmailStatus(Status.Active);
			admin.setUserType(UserType.Admin);
			admin.setUsername(detail.getContactNo());
			admin.setUserDetail(detail);
			admin.setAccountDetail(pqAccountDetail);
			userRepository.save(admin);
		}

		User settlement = userRepository.findByUsername(StartupUtil.SETTLEMENT);
		if (settlement == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("JP Nagar");
			detail.setContactNo(StartupUtil.SETTLEMENT);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Settlement");
			detail.setEmail(StartupUtil.SETTLEMENT);
			userDetailRepository.save(detail);
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);
			settlement = new User();
			settlement.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			settlement.setPassword(passwordEncoder.encode("1234567890"));
			settlement.setCreated(new Date());
			settlement.setMobileStatus(Status.Active);
			settlement.setEmailStatus(Status.Active);
			settlement.setUserType(UserType.Admin);
			settlement.setUsername(detail.getContactNo());
			settlement.setUserDetail(detail);
			settlement.setAccountDetail(pqAccountDetail);
			userRepository.save(settlement);
		}

		User commission = userRepository.findByUsername(StartupUtil.COMMISSION);
		if (commission == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("JP Nagar");
			detail.setContactNo(StartupUtil.COMMISSION);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Settlement");
			detail.setEmail(StartupUtil.COMMISSION);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			commission = new User();
			commission.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			commission.setPassword(passwordEncoder.encode("1234567890"));
			commission.setCreated(new Date());
			commission.setMobileStatus(Status.Active);
			commission.setEmailStatus(Status.Active);
			commission.setUserType(UserType.Admin);
			commission.setUsername(detail.getContactNo());
			commission.setUserDetail(detail);
			commission.setAccountDetail(pqAccountDetail);
			userRepository.save(commission);
		}

		User travel = userRepository.findByUsername(StartupUtil.TRAVEL);
		if (travel == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("JP Nagar");
			detail.setContactNo(StartupUtil.TRAVEL);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Travel");
			detail.setEmail(StartupUtil.TRAVEL);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			travel = new User();
			travel.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			travel.setPassword(passwordEncoder.encode("1234567890"));
			travel.setCreated(new Date());
			travel.setMobileStatus(Status.Active);
			travel.setEmailStatus(Status.Active);
			travel.setUserType(UserType.Locked);
			travel.setUsername(detail.getContactNo());
			travel.setUserDetail(detail);
			travel.setAccountDetail(pqAccountDetail);
			userRepository.save(travel);
		}

		User bank = userRepository.findByUsername(StartupUtil.BANK);
		if (bank == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("JP Nagar");
			detail.setContactNo(StartupUtil.BANK);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Bank");
			detail.setEmail(StartupUtil.BANK);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			bank = new User();
			bank.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			bank.setPassword(passwordEncoder.encode("1234567890"));
			bank.setCreated(new Date());
			bank.setMobileStatus(Status.Active);
			bank.setEmailStatus(Status.Active);
			bank.setUserType(UserType.Admin);
			bank.setUsername(detail.getContactNo());
			bank.setUserDetail(detail);
			bank.setAccountDetail(pqAccountDetail);
			userRepository.save(bank);
		}

		User mobileBank = userRepository.findByUsername(StartupUtil.MOBILE_BANK);
		if (mobileBank == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.MOBILE_BANK);
			detail.setFirstName("Vijaya");
			detail.setMiddleName("_");
			detail.setLastName("Mobile Banking");
			detail.setEmail(StartupUtil.MOBILE_BANK);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(100000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			mobileBank = new User();
			mobileBank.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			mobileBank.setPassword(passwordEncoder.encode("1234567890"));
			mobileBank.setCreated(new Date());
			mobileBank.setMobileStatus(Status.Active);
			mobileBank.setEmailStatus(Status.Active);
			mobileBank.setUserType(UserType.Locked);
			mobileBank.setUsername(detail.getContactNo());
			mobileBank.setUserDetail(detail);
			mobileBank.setAccountDetail(pqAccountDetail);
			userRepository.save(mobileBank);
		}

		User superAdmin = userRepository.findByUsername(StartupUtil.SUPER_ADMIN);
		if (superAdmin == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("#106, 4th Cross, 2nd Block, Koramangala");
			detail.setContactNo("7022620747");
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Super Admin");
			detail.setEmail(StartupUtil.SUPER_ADMIN);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			superAdmin = new User();
			superAdmin.setAuthority(Authorities.SUPER_ADMIN + "," + Authorities.AUTHENTICATED);
			System.err.println(superAdmin.getAuthority());
			superAdmin.setPassword(passwordEncoder.encode("1234567890"));
			superAdmin.setCreated(new Date());
			superAdmin.setMobileStatus(Status.Active);
			superAdmin.setEmailStatus(Status.Active);
			superAdmin.setUserType(UserType.SuperAdmin);
			superAdmin.setUsername(detail.getEmail());
			superAdmin.setUserDetail(detail);
			superAdmin.setAccountDetail(pqAccountDetail);
			userRepository.save(superAdmin);
		}

		User vatUser = userRepository.findByUsername(StartupUtil.VAT);
		if (vatUser == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.VAT);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("VAT");
			detail.setEmail(StartupUtil.VAT);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			vatUser = new User();
			vatUser.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			vatUser.setPassword(passwordEncoder.encode("1234567890"));
			vatUser.setCreated(new Date());
			vatUser.setMobileStatus(Status.Active);
			vatUser.setEmailStatus(Status.Active);
			vatUser.setUserType(UserType.Locked);
			vatUser.setUsername(detail.getContactNo());
			vatUser.setUserDetail(detail);
			vatUser.setAccountDetail(pqAccountDetail);
			userRepository.save(vatUser);
		}

		User kkCess = userRepository.findByUsername(StartupUtil.KRISHI_KALYAN);
		if (kkCess == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.KRISHI_KALYAN);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Krishi Kalyan Cess");
			detail.setEmail(StartupUtil.KRISHI_KALYAN);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			kkCess = new User();
			kkCess.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			kkCess.setPassword(passwordEncoder.encode("1234567890"));
			kkCess.setCreated(new Date());
			kkCess.setMobileStatus(Status.Active);
			kkCess.setEmailStatus(Status.Active);
			kkCess.setUserType(UserType.Locked);
			kkCess.setUsername(detail.getContactNo());
			kkCess.setUserDetail(detail);
			kkCess.setAccountDetail(pqAccountDetail);
			userRepository.save(kkCess);
		}

		User sbCess = userRepository.findByUsername(StartupUtil.SWACCH_BHARAT);
		if (sbCess == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.SWACCH_BHARAT);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Swacch Bharat Cess");
			detail.setEmail(StartupUtil.SWACCH_BHARAT);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			sbCess = new User();
			sbCess.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			sbCess.setPassword(passwordEncoder.encode("1234567890"));
			sbCess.setCreated(new Date());
			sbCess.setMobileStatus(Status.Active);
			sbCess.setEmailStatus(Status.Active);
			sbCess.setUserType(UserType.Locked);
			sbCess.setUsername(detail.getContactNo());
			sbCess.setUserDetail(detail);
			sbCess.setAccountDetail(pqAccountDetail);
			userRepository.save(sbCess);
		}

		User mVisa = userRepository.findByUsername(StartupUtil.MVISA);
		if (mVisa == null) {

			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo("7022620747");
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("mVisa");
			detail.setEmail(StartupUtil.MVISA);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			mVisa = new User();
			mVisa.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			mVisa.setPassword(passwordEncoder.encode("1234567890"));
			mVisa.setCreated(new Date());
			mVisa.setMobileStatus(Status.Active);
			mVisa.setEmailStatus(Status.Active);
			mVisa.setUserType(UserType.Locked);
			mVisa.setUsername(detail.getEmail());
			mVisa.setUserDetail(detail);
			mVisa.setAccountDetail(pqAccountDetail);
			userRepository.save(mVisa);
		}

		User nikki = userRepository.findByUsername(StartupUtil.NIKKI_CHAT);
		if (nikki == null) {

			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo("7022620747A");
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("Nikki chat");
			detail.setEmail(StartupUtil.NIKKI_CHAT);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			nikki = new User();
			nikki.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			nikki.setPassword(passwordEncoder.encode("1234567890"));
			nikki.setCreated(new Date());
			nikki.setMobileStatus(Status.Active);
			nikki.setEmailStatus(Status.Active);
			nikki.setUserType(UserType.Locked);
			nikki.setUsername(detail.getEmail());
			nikki.setUserDetail(detail);
			nikki.setAccountDetail(pqAccountDetail);
			userRepository.save(nikki);
		}

		User paylo = userRepository.findByUsername(StartupUtil.PAYLO_MERCHANT);
		if (paylo == null) {

			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo("7022620747A");
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("Paylo Merchant");
			detail.setEmail(StartupUtil.PAYLO_MERCHANT);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			paylo = new User();
			paylo.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			paylo.setPassword(passwordEncoder.encode("1234567890"));
			paylo.setCreated(new Date());
			paylo.setMobileStatus(Status.Active);
			paylo.setEmailStatus(Status.Active);
			paylo.setUserType(UserType.Locked);
			paylo.setUsername(detail.getEmail());
			paylo.setUserDetail(detail);
			paylo.setAccountDetail(pqAccountDetail);
			userRepository.save(paylo);
		}

		User meraEvents = userRepository.findByUsername(StartupUtil.MERA_EVENTS);
		if (meraEvents == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Karnataka");
			detail.setContactNo("8686242628");
			detail.setFirstName("Mera Events");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartupUtil.MERA_EVENTS);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			meraEvents = new User();
			meraEvents.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			meraEvents.setPassword(passwordEncoder.encode("123456"));
			meraEvents.setCreated(new Date());
			meraEvents.setMobileStatus(Status.Active);
			meraEvents.setEmailStatus(Status.Active);
			meraEvents.setUserType(UserType.Locked);
			meraEvents.setUsername(detail.getEmail());
			meraEvents.setUserDetail(detail);
			meraEvents.setAccountDetail(pqAccountDetail);
			userRepository.save(meraEvents);
		}
		User giftcart = userRepository.findByUsername(StartupUtil.Giftcart);
		if (giftcart == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Karnataka");
			detail.setContactNo("8686242628");
			detail.setFirstName("Gitf Cart");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartupUtil.Giftcart);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			giftcart = new User();
			giftcart.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			giftcart.setPassword(passwordEncoder.encode("123456"));
			giftcart.setCreated(new Date());
			giftcart.setMobileStatus(Status.Active);
			giftcart.setEmailStatus(Status.Active);
			giftcart.setUserType(UserType.Locked);
			giftcart.setUsername(detail.getEmail());
			giftcart.setUserDetail(detail);
			giftcart.setAccountDetail(pqAccountDetail);
			userRepository.save(giftcart);
		}

		/* Super Agent Account Creator */
		User superAgent = userRepository.findByUsername(StartupUtil.SUPER_AGENT);
		if (superAgent == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Karnataka");
			detail.setContactNo("9850264933");
			detail.setFirstName("Super Agent");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartupUtil.SUPER_AGENT);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			superAgent = new User();
			superAgent.setAuthority(Authorities.SUPER_AGENT + "," + Authorities.AUTHENTICATED);
			superAgent.setPassword(passwordEncoder.encode("1234567890"));
			superAgent.setCreated(new Date());
			superAgent.setMobileStatus(Status.Active);
			superAgent.setEmailStatus(Status.Active);
			superAgent.setUserType(UserType.SuperAgent);
			superAgent.setUsername(detail.getEmail());
			superAgent.setUserDetail(detail);
			superAgent.setAccountDetail(pqAccountDetail);
			userRepository.save(superAgent);
		}

		User instantPay = userRepository.findByUsername(InstantPayConstants.USERNAME);
		if (instantPay == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Koramangala");
			detail.setContactNo("1234567890");
			detail.setFirstName("Instant");
			detail.setMiddleName("P");
			detail.setLastName("Pay");
			detail.setEmail(InstantPayConstants.USERNAME);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			instantPay = new User();
			instantPay.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			instantPay.setPassword(passwordEncoder.encode("1234567890"));
			instantPay.setCreated(new Date());
			instantPay.setMobileStatus(Status.Active);
			instantPay.setEmailStatus(Status.Active);
			instantPay.setUserType(UserType.Locked);
			instantPay.setUsername(detail.getEmail());
			instantPay.setUserDetail(detail);
			instantPay.setAccountDetail(pqAccountDetail);
			userRepository.save(instantPay);
		}

		for (int i = 0; i < 5; i++) {
			String username = "";
			String email = "";
			String firstName = "";
			String lastName = "";
			String date = "";
			String mpin = "";
			switch (i) {
			case 0:
				username = "9999998888";
				email = "testuser@test.com";
				firstName = "Test";
				lastName = "User";
				date = "1984-03-14";
				mpin = "1234";
				break;
			case 1:
				username = "8769986881";
				email = "vibhanshuvyas60@gmail.com";
				firstName = "Vibhanshu";
				lastName = "Vyas";
				date = "1994-07-19";
				mpin = "4585";
				break;
			case 2:
				username = "9461553581";
				email = "gupta.gaurav@gmail.com";
				firstName = "Gaurav";
				lastName = "Gupta";
				date = "1985-07-21";
				mpin = "8775";
				break;
			case 3:
				username = "9999998881";
				email = "testuser2@msewa.com";
				firstName = "Test2";
				lastName = "User";
				date = "1987-08-28";
				mpin = "1234";
				break;
			case 4:
				username = "9206102977";
				email = "sdawn@msewa.com";
				firstName = "Subir";
				lastName = "Dawn";
				date = "1993-06-11";
				mpin = "1993";
				break;
			default:
				username = "7022620747";
				email = "kumar.pankaj11@gmail.com";
				firstName = "Pankaj";
				lastName = "Kumar";
				date = "1985-04-13";
				mpin = "1994";
				break;
			}

			User user = userRepository.findByUsername(username);
			if (user == null) {
				UserDetail detail = new UserDetail();
				detail.setAddress("Trinity Circle,Bangalore");
				detail.setContactNo(username);
				detail.setFirstName(firstName);
				detail.setMiddleName(" ");
				detail.setLastName(lastName);
				detail.setEmail(email);
				detail.setGender(Gender.M);
				try {
					detail.setMpin(SecurityUtil.sha512(mpin));
					detail.setDateOfBirth(sdf.parse(date));
				} catch (Exception e) {
					e.printStackTrace();
				}
				userDetailRepository.save(detail);
				PQAccountDetail pqAccountDetail = new PQAccountDetail();
				pqAccountDetail.setBalance(100000);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
				pqAccountDetail.setAccountType(acTypeNonKYC);
				pqAccountDetailRepository.save(pqAccountDetail);

				user = new User();
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode("123456"));
				user.setCreated(new Date());
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUserType(UserType.User);
				user.setUsername(detail.getContactNo());
				user.setUserDetail(detail);
				user.setAccountDetail(pqAccountDetail);
				userRepository.save(user);
			}

			User promoCode = userRepository.findByUsername(StartupUtil.PROMO_CODE);
			if (promoCode == null) {
				UserDetail detail = new UserDetail();
				detail.setAddress("BTM");
				detail.setFirstName("Promo");
				detail.setMiddleName(" ");
				detail.setLastName("Code");
				detail.setEmail(StartupUtil.PROMO_CODE);
				detail.setContactNo(StartupUtil.PROMO_CODE);
				userDetailRepository.save(detail);

				PQAccountDetail pqAccountDetail = new PQAccountDetail();
				pqAccountDetail.setBalance(10000);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

				pqAccountDetail.setAccountType(acTypeKYC);
				pqAccountDetailRepository.save(pqAccountDetail);

				promoCode = new User();
				promoCode.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
				promoCode.setPassword(passwordEncoder.encode("12345678"));
				promoCode.setCreated(new Date());
				promoCode.setMobileStatus(Status.Active);
				promoCode.setEmailStatus(Status.Active);
				promoCode.setUserType(UserType.Locked);
				promoCode.setUsername(detail.getContactNo());
				promoCode.setUserDetail(detail);
				promoCode.setAccountDetail(pqAccountDetail);
				userRepository.save(promoCode);
			}
		}

		User predictAndWin = userRepository.findByUsername(StartupUtil.PRDEICT_AND_WIN);
		if (predictAndWin == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("Predict");
			detail.setMiddleName(" ");
			detail.setLastName("Win");
			detail.setEmail(StartupUtil.PRDEICT_AND_WIN);
			detail.setContactNo(StartupUtil.PRDEICT_AND_WIN);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(10000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			predictAndWin = new User();
			predictAndWin.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			predictAndWin.setPassword(passwordEncoder.encode("12345678"));
			predictAndWin.setCreated(new Date());
			predictAndWin.setMobileStatus(Status.Active);
			predictAndWin.setEmailStatus(Status.Active);
			predictAndWin.setUserType(UserType.Locked);
			predictAndWin.setUsername(detail.getContactNo());
			predictAndWin.setUserDetail(detail);
			predictAndWin.setAccountDetail(pqAccountDetail);
			userRepository.save(predictAndWin);
		}

		User refundManager = userRepository.findByUsername(StartupUtil.REFUND);
		if (refundManager == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("TRINITY CIRCLE");
			detail.setFirstName("REFUND");
			detail.setMiddleName(" ");
			detail.setLastName("MANAGER");
			detail.setEmail(StartupUtil.REFUND);
			detail.setContactNo(StartupUtil.REFUND);
			userDetailRepository.save(detail);
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);
			refundManager = new User();
			refundManager.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			refundManager.setPassword(passwordEncoder.encode("12345678"));
			refundManager.setCreated(new Date());
			refundManager.setMobileStatus(Status.Active);
			refundManager.setEmailStatus(Status.Active);
			refundManager.setUserType(UserType.Locked);
			refundManager.setUsername(detail.getContactNo());
			refundManager.setUserDetail(detail);
			refundManager.setAccountDetail(pqAccountDetail);
			userRepository.save(refundManager);
		}

		User riskManager = userRepository.findByUsername(StartupUtil.RISK);
		if (riskManager == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("Risk");
			detail.setMiddleName(" ");
			detail.setLastName("Manager");
			detail.setEmail(StartupUtil.RISK);
			detail.setContactNo(StartupUtil.RISK);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(10000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			riskManager = new User();
			riskManager.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			riskManager.setPassword(passwordEncoder.encode("12345678"));
			riskManager.setCreated(new Date());
			riskManager.setMobileStatus(Status.Active);
			riskManager.setEmailStatus(Status.Active);
			riskManager.setUserType(UserType.Locked);
			riskManager.setUsername(detail.getContactNo());
			riskManager.setUserDetail(detail);
			riskManager.setAccountDetail(pqAccountDetail);
			userRepository.save(riskManager);
		}

		User savaari = userRepository.findByUsername(StartupUtil.SAVAARI);
		if (savaari == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("Savaari");
			detail.setMiddleName(" ");
			detail.setLastName("booking");
			detail.setEmail(StartupUtil.SAVAARI);
			detail.setContactNo(StartupUtil.SAVAARI);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(10000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			savaari = new User();
			savaari.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			savaari.setPassword(passwordEncoder.encode("12345678"));
			savaari.setCreated(new Date());
			savaari.setMobileStatus(Status.Active);
			savaari.setEmailStatus(Status.Active);
			savaari.setUserType(UserType.Locked);
			savaari.setUsername(detail.getContactNo());
			savaari.setUserDetail(detail);
			savaari.setAccountDetail(pqAccountDetail);
			userRepository.save(savaari);
		}

		///   for Adlabs///////////////////
		/////

		User adlab = userRepository.findByUsername(StartupUtil.Adlabs);
		if (adlab == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Karnataka");
			detail.setContactNo("8808408405");
			detail.setFirstName("Adalbs");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartupUtil.Adlabs);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			adlab = new User();
			adlab.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			adlab.setPassword(passwordEncoder.encode("123456"));
			adlab.setCreated(new Date());
			adlab.setMobileStatus(Status.Active);
			adlab.setEmailStatus(Status.Active);
			adlab.setUserType(UserType.Locked);
			adlab.setUsername(detail.getEmail());
			adlab.setUserDetail(detail);
			adlab.setAccountDetail(pqAccountDetail);
			userRepository.save(adlab);
		}

		///// adlab end ////////////////////
		YTVKeys saveKeys=ytvKeysRepository.findByKey("VPAYQWIK");
		if(saveKeys==null){
		YTVKeys keys=new YTVKeys();
		keys.setAPIKey("VPAYQWIK");
		keys.setAPISecret("befb821e72a6a771f2a7d4767c73cd2f");
		ytvKeysRepository.save(keys);
		}


		User mBank = userRepository.findByUsername(StartupUtil.MBANK);
		if (mBank == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.MBANK);
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("Merchant's Bank transfer");
			detail.setEmail(StartupUtil.MBANK);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			sbCess = new User();
			sbCess.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			sbCess.setPassword(passwordEncoder.encode("1234567890"));
			sbCess.setCreated(new Date());
			sbCess.setMobileStatus(Status.Active);
			sbCess.setEmailStatus(Status.Active);
			sbCess.setUserType(UserType.Locked);
			sbCess.setUsername(detail.getContactNo());
			sbCess.setUserDetail(detail);
			sbCess.setAccountDetail(pqAccountDetail);
			userRepository.save(sbCess);
		}
		
		User aBank = userRepository.findByUsername(StartupUtil.ABANK);
		if (aBank == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.ABANK);
			detail.setFirstName("Ecarib");
			detail.setMiddleName("_");
			detail.setLastName("Agent's Bank transfer");
			detail.setEmail(StartupUtil.ABANK);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			sbCess = new User();
			sbCess.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			sbCess.setPassword(passwordEncoder.encode("1234567890"));
			sbCess.setCreated(new Date());
			sbCess.setMobileStatus(Status.Active);
			sbCess.setEmailStatus(Status.Active);
			sbCess.setUserType(UserType.Locked);
			sbCess.setUsername(detail.getContactNo());
			sbCess.setUserDetail(detail);
			sbCess.setAccountDetail(pqAccountDetail);
			userRepository.save(sbCess);
		}
		
		for (int i = 0; i < 3; i++) {
			String name = "";
			switch (i) {
			case 0:
				name = "TOP UP";
				break;
			case 1:
				name = "Load Money ";
				break;
			default:
				name = "Bill Payment";

				break;
			}
			ServiceStatus fpStatus = serviceStatusRepository.findByName(name);
			if (fpStatus == null) {
				fpStatus = new ServiceStatus();
				fpStatus.setName(name);
				fpStatus.setStatus(Status.Inactive);
				serviceStatusRepository.save(fpStatus);
			}
		}
		User qwikrPay = userRepository.findByUsername(StartupUtil.QWIKR_PAY);
		if (qwikrPay == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Karnataka");
			detail.setContactNo("8895093009");
			detail.setFirstName("qwikrpay");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartupUtil.QWIKR_PAY);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(100000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			qwikrPay = new User();
			qwikrPay.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			qwikrPay.setPassword(passwordEncoder.encode("123456"));
			qwikrPay.setCreated(new Date());
			qwikrPay.setMobileStatus(Status.Active);
			qwikrPay.setEmailStatus(Status.Active);
			qwikrPay.setUserType(UserType.Locked);
			qwikrPay.setUsername(detail.getEmail());
			qwikrPay.setUserDetail(detail);
			qwikrPay.setAccountDetail(pqAccountDetail);
			userRepository.save(qwikrPay);
		}
		User flight = userRepository.findByUsername(StartupUtil.FLIGHT_BOOKING);
		if (flight == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("EaseMytrip");
			detail.setMiddleName(" ");
			detail.setLastName("Flight");
			detail.setEmail(StartupUtil.FLIGHT_BOOKING);
			detail.setContactNo(StartupUtil.FLIGHT_BOOKING);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(100000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			flight = new User();
			flight.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			flight.setPassword(passwordEncoder.encode("12345678"));
			flight.setCreated(new Date());
			flight.setMobileStatus(Status.Active);
			flight.setEmailStatus(Status.Active);
			flight.setUserType(UserType.Locked);
			flight.setUsername(detail.getContactNo());
			flight.setUserDetail(detail);
			flight.setAccountDetail(pqAccountDetail);
			userRepository.save(flight);
		}

		
		User bus = userRepository.findByUsername(StartupUtil.BUS_BOOKING);
		if (bus == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("EaseMytrip");
			detail.setMiddleName(" ");
			detail.setLastName("Bus");
			detail.setEmail(StartupUtil.BUS_BOOKING);
			detail.setContactNo(StartupUtil.BUS_BOOKING);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			bus = new User();
			bus.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			bus.setPassword(passwordEncoder.encode("12345678"));
			bus.setCreated(new Date());
			bus.setMobileStatus(Status.Active);
			bus.setEmailStatus(Status.Active);
			bus.setUserType(UserType.Locked);
			bus.setUsername(detail.getContactNo());
			bus.setUserDetail(detail);
			bus.setAccountDetail(pqAccountDetail);
			userRepository.save(bus);
		}
		
		User travelkhana = userRepository.findByUsername(StartupUtil.TRAVELKHANA);
		if (travelkhana == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("Travel");
			detail.setMiddleName(" ");
			detail.setLastName("Khana");
			detail.setEmail(StartupUtil.TRAVELKHANA);
			detail.setContactNo(StartupUtil.TRAVELKHANA);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());

			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			travelkhana = new User();
			travelkhana.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			travelkhana.setPassword(passwordEncoder.encode("12345678"));
			travelkhana.setCreated(new Date());
			travelkhana.setMobileStatus(Status.Active);
			travelkhana.setEmailStatus(Status.Active);
			travelkhana.setUserType(UserType.Locked);
			travelkhana.setUsername(detail.getContactNo());
			travelkhana.setUserDetail(detail);
			travelkhana.setAccountDetail(pqAccountDetail);
			userRepository.save(travelkhana);
		}
		
		User houseJoy = userRepository.findByUsername(StartupUtil.HOUSE_JOY);
		if (houseJoy == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Karnataka");
			detail.setContactNo("7022620747");
			detail.setFirstName("houseJoy");
			detail.setMiddleName("_");
			detail.setLastName(" ");
			detail.setEmail(StartupUtil.HOUSE_JOY);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(100000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			qwikrPay = new User();
			qwikrPay.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			qwikrPay.setPassword(passwordEncoder.encode("123456"));
			qwikrPay.setCreated(new Date());
			qwikrPay.setMobileStatus(Status.Active);
			qwikrPay.setEmailStatus(Status.Active);
			qwikrPay.setUserType(UserType.Locked);
			qwikrPay.setUsername(detail.getEmail());
			qwikrPay.setUserDetail(detail);
			qwikrPay.setAccountDetail(pqAccountDetail);
			userRepository.save(qwikrPay);
		}
		
		User woohoo = userRepository.findByUsername(StartupUtil.WOOHOO);
		if (woohoo == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Koramangala");
			detail.setContactNo("1234567890");
			detail.setFirstName("Woohoo");
			detail.setMiddleName("P");
			detail.setLastName("Gift");
			detail.setEmail(StartupUtil.WOOHOO);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(acTypeKYC);
			pqAccountDetailRepository.save(pqAccountDetail);

			instantPay = new User();
			instantPay.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			instantPay.setPassword(passwordEncoder.encode("woohooG!FT"));
			instantPay.setCreated(new Date());
			instantPay.setMobileStatus(Status.Active);
			instantPay.setEmailStatus(Status.Active);
			instantPay.setUserType(UserType.Locked);
			instantPay.setUsername(detail.getEmail());
			instantPay.setUserDetail(detail);
			instantPay.setAccountDetail(pqAccountDetail);
			userRepository.save(instantPay);
		}
	
	}

	private boolean isRawMpin(String mpin) {
		boolean valid = false;
		if (!CommonValidation.isNull(mpin) && CommonValidation.checkLength4(mpin) && CommonValidation.isNumeric(mpin)) {
			valid = true;
		}
		return valid;
	}
	
}
