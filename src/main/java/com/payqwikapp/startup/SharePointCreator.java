package com.payqwikapp.startup;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;

public class SharePointCreator {
	

	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public SharePointCreator(PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, PQServiceRepository pqServiceRepository,
			ICommissionApi commissionApi) {
	
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		createReferAndEarnService();
	}

	private void createReferAndEarnService() {

		PQOperator vpayqwik_redeem = pqOperatorRepository.findOperatorByName("VPayQwik_Redeem");
		if(vpayqwik_redeem==null){
			vpayqwik_redeem = new PQOperator();
			vpayqwik_redeem.setName("VPayQwik");
			vpayqwik_redeem.setStatus(Status.Active);
			pqOperatorRepository.save(vpayqwik_redeem);
		}
		
		PQServiceType serviceRedeemPoint = pqServiceTypeRepository.findServiceTypeByName("Redeem Points");
        if(serviceRedeemPoint==null){
        	serviceRedeemPoint = new PQServiceType();
        	serviceRedeemPoint .setName("Redeem Points");
        	serviceRedeemPoint.setDescription("Redeem Points convert to cash");
        	serviceRedeemPoint = pqServiceTypeRepository.save(serviceRedeemPoint);
        }
        
		PQService redeem= pqServiceRepository.findServiceByCode("REDEEM");
        if(redeem==null){
        	redeem = new PQService();
        	redeem.setName("REDEEM POINT");
        	redeem.setDescription("REDEEM POINT in VPayQwik");
        	redeem.setMinAmount(10);
        	redeem.setMaxAmount(10000000);
        	redeem.setCode("REDEEM");
        	redeem.setOperator(vpayqwik_redeem);
        	redeem.setOperatorCode("REDEEM");
        	redeem.setStatus(Status.Active);
        	redeem.setServiceType(serviceRedeemPoint);
			pqServiceRepository.save(redeem);
        }
        PQCommission redeemPointCommision = new PQCommission();
        redeemPointCommision.setMinAmount(1);
        redeemPointCommision.setMaxAmount(10000);
        redeemPointCommision.setType("POST");
        redeemPointCommision.setValue(0);
        redeemPointCommision.setFixed(true);
        redeemPointCommision.setService(redeem);
        redeemPointCommision.setIdentifier(commissionApi.createCommissionIdentifier(redeemPointCommision));
        if(commissionApi.findCommissionByIdentifier(redeemPointCommision.getIdentifier())==null){
        	commissionApi.save(redeemPointCommision);
        }
	}

}
