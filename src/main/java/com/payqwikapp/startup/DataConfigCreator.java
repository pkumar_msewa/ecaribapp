package com.payqwikapp.startup;

import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.DataConfigRepository;

public class DataConfigCreator {

    private final DataConfigRepository dataConfigRepository;

    public DataConfigCreator(DataConfigRepository dataConfigRepository) {
        this.dataConfigRepository = dataConfigRepository;
    }

    public void create(){
        	DataConfig access = dataConfigRepository.getByConfigType("Recharge Biller",Status.Active);
            if(access == null) {
                access = new DataConfig();
                access.setConfigType("Recharge Biller");
                access.setStatus(Status.Active);
                access.setDescription("configuration for enabling biller");
                access.setMdex(Status.Active.getValue());
                access.setCode("MDEX");
                dataConfigRepository.save(access);
            }
        

    }

}
