package com.payqwikapp.startup;

import com.m2p.api.IM2PApi;
import com.m2p.model.QRResponseDTO;
import com.m2p.model.VisaSignUpResponse;
import com.payqwikapp.entity.POSMerchants;
import com.payqwikapp.repositories.POSMerchantsRepository;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.StartupUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class POSMerchantCreator {

    private final POSMerchantsRepository posMerchantsRepository;
    private final IM2PApi m2pApi;

    public POSMerchantCreator(POSMerchantsRepository posMerchantsRepository,IM2PApi m2pApi) {
        this.posMerchantsRepository = posMerchantsRepository;
        this.m2pApi = m2pApi;
    }

    public void create(){
//        String fileName = StartupUtil.CSV_FILE+"pos_merchants_19.csv";
//        List<POSMerchants> merchants = readMerchantsFromFile(fileName);
//        for(POSMerchants merchant: merchants){
        /*	try{
                merchant.setBankName("Vijaya Bank");
                String accountNumber = merchant.getAccountNumber();
                System.err.println("account number ::" + accountNumber);
                merchant.setEmail(getEmail(accountNumber));
//                merchant.setIfscCode(getIfscCode(accountNumber));
                String mobileNumber = merchant.getMobileNumber();
                System.err.println("mobile Number ::" + mobileNumber);
                if (merchant.getMcc() != null) {
                    if (mobileNumber != null && accountNumber != null) {
                        POSMerchants exists = posMerchantsRepository.findByMobileAndAccount(mobileNumber, accountNumber);
                        if (exists == null) {
                            posMerchantsRepository.save(merchant);
                        }
                    }
                }*/
//              registerMerchant();
                getQRString();
         /*   }catch (Exception ex) {
                ex.printStackTrace();
           }*/
//        }
    }

    public void registerMerchant(){
//        List<POSMerchants> merchants = (List<POSMerchants>) posMerchantsRepository.findTodaysPOSMerchants();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, 6);
        cal.set(Calendar.DATE,30);
        cal.set(Calendar.YEAR, 2018);
        Date date = cal.getTime();
        System.err.println(date);
        List<POSMerchants> merchants = posMerchantsRepository.findPOSMerchantsByDate(date);
        for(POSMerchants merchant:merchants) {
                VisaSignUpResponse response = m2pApi.requestForLive(ConvertUtil.convertFromPOSToDTO(merchant));
                if (response.getCode().equalsIgnoreCase("S00")) {
                    merchant.setCustomerId(response.getEntityId());
                    merchant.setMvisaId(response.getmVisaId());
                    merchant.setRupayId(response.getRupayId());
                    posMerchantsRepository.save(merchant);
                    CommonUtil.sleep(10 * 1000);
                }
        }
    }

    public void getQRString(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, 6);
        cal.set(Calendar.DATE,30);
        cal.set(Calendar.YEAR, 2018);
        Date date = cal.getTime();
        System.err.println(date);
        List<POSMerchants> merchants = posMerchantsRepository.findPOSMerchantsByDate(date);

//        List<POSMerchants> merchants = posMerchantsRepository.findTodaysPOSMerchants();
        for(POSMerchants merchant:merchants){
            QRResponseDTO response = m2pApi.request(ConvertUtil.getFromCustomerId(merchant.getCustomerId()));
            if(response.getCode().equalsIgnoreCase("S00")) {
                try {
//                    JSONObject json = new JSONObject(response.getQrCode());
                    merchant.setQrCode(response.getQrCode());
                    posMerchantsRepository.save(merchant);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public String getEmail(String accountNumber){
        StringBuilder builder = new StringBuilder();
        if(accountNumber.length() >= 10) {
            builder.append("VB");
            builder.append(accountNumber.substring(0, 4));
            builder.append("@VIJAYABANK.CO.IN");
        }else {
            builder.append("VPAYQWIKCARE@VIJAYABANK.CO.IN");
        }
        return builder.toString();
    }
    
    public String getIfscCode(String accountNumber){
        StringBuilder builder = new StringBuilder();
        if(accountNumber.length() >= 10) {
            builder.append("VIJB");
            builder.append("000");
            builder.append(accountNumber.substring(0, 4));
        }else {
            builder.append("VIJB0001331");
        }
        return builder.toString();
    }


    public ArrayList<POSMerchants> readMerchantsFromFile(String fileName) {
        ArrayList<POSMerchants> merchants = new ArrayList<>();
        System.err.println(fileName);
        BufferedReader br = null;
        String line = "";
        try {

            br = new BufferedReader(new FileReader(fileName));
            POSMerchants merchant = null;
            while ((line = br.readLine()) != null) {
                Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
                Matcher m = p.matcher(line);
                merchant = new POSMerchants();
                String value = null;
                int index = 1;
                while (m.find()) {
                    if (m.group(2) != null) {
                        value = m.group(2);
                    }

                    if (m.group(3) != null) {
                        value = m.group(3);
                    }

                    if (value != null) {
                        if (merchant != null) {
                            switch (index) {
                                case 1:
                                    merchant.setMcc(value);
                                    break;
                                case 2:
                                    merchant.setPanNo(value);
                                    break;
                                case 3:
                                    merchant.setMerchantName(value);
                                    break;
                                case 4:
                                    merchant.setAccountNumber(value);
                                    break;
                                case 5:
                                    merchant.setBankLocation(value);
                                    break;
                                case 6:
                                    merchant.setAddress(value);
                                    break;
                                case 7:
                                    merchant.setAddress(merchant.getAddress() +" "+value);
                                    break;
                                case 8:
                                    merchant.setCity(value);
                                    break;
                                case 9:
                                    merchant.setState(value);
                                    break;
                                case 10:
                                    merchant.setIfscCode(value);
                                    break;
                                case 11:
                                    merchant.setPincode(value);
                                    break;
                                case 12:
                                    merchant.setMobileNumber(value);
                                    break;
                                case 13:
                                    merchant.setEmail(value);
                                    break;
                                default:
                                    break;
                            }
                            index = index + 1;
                        }
                    }
                    merchants.add(merchant);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return merchants;
    }

}
