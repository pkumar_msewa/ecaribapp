package com.payqwikapp.startup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.repositories.FlightListRepository;
import com.payqwikapp.util.StartupUtil;

public class AirportListCreator {
	private FlightListRepository flightListRepository;

	public AirportListCreator(FlightListRepository flightListRepository) {
		this.flightListRepository=flightListRepository;
	
	}

	public void create() {
		saveAirportList();
	}

	private void saveAirportList() {
		
	String fileNameService = StartupUtil.CSV_FILE + "Airportlist.csv";
	ArrayList<FlightAirLineList> nameDetailsList = readTelcoCircleFromFile(fileNameService);
	if(nameDetailsList != null){
		for (FlightAirLineList flightNameDetails : nameDetailsList) {
			System.out.println("Flight code : " + flightNameDetails.getCityCode());
			System.out.println("Flight Name : " + flightNameDetails.getCityName());
			System.out.println("Flight Description : " + flightNameDetails.getAirportName());
			FlightAirLineList fnd = flightListRepository.getCityByCode(flightNameDetails.getCityCode());
			if(fnd == null){
				fnd = new FlightAirLineList();
				fnd.setCityCode(flightNameDetails.getCityCode());
				fnd.setCityName(flightNameDetails.getCityName());
				fnd.setAirportName(flightNameDetails.getAirportName());
				fnd.setCountry(flightNameDetails.getCountry());
				flightListRepository.save(fnd);
			}
		}
    	}
	}
	
	
	public ArrayList<FlightAirLineList> readTelcoCircleFromFile(String fileName) {
		ArrayList<FlightAirLineList> flightDetails = new ArrayList<>();
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(fileName));
			FlightAirLineList c = null;
			while ((line = br.readLine()) != null) {
				Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
				Matcher m = p.matcher(line);

				c = new FlightAirLineList();
				String value = null;
				int index = 1;
				while (m.find()) {
					if (m.group(2) != null) {
						value = m.group(2);
					}

					if (m.group(3) != null) {
						value = m.group(3);
					}

					if (value != null) {
						if (c != null) {
							switch (index) {
							case 1:
								c.setAirportName(value);
								break;
							case 2:
								c.setCityCode(value);
								break;
							case 3:
								c.setCityName(value);
								break;
							case 4:
								c.setCountry(value);
								break;
							default:
								break;
							}
							index = index + 1;
						}
					}

					if (m.hitEnd()) {
						flightDetails.add(c);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return flightDetails;
	}
	
}
