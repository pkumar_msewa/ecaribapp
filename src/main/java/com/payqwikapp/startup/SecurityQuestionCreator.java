package com.payqwikapp.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.SecurityQuestion;
import com.payqwikapp.repositories.SecurityQuestionRepository;

public class SecurityQuestionCreator {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private SecurityQuestionRepository securityQuestionRepository;

	public SecurityQuestionCreator(SecurityQuestionRepository securityQuestionRepository) {
		this.securityQuestionRepository = securityQuestionRepository;
	}

	public void create() {
		for (int i = 0; i < 5; i++) {
			String question = "";
			String code = "";
			switch (i) {
			case 0:
				question = "What is your first pet name?";
				code = "1";
				break;
			case 1:
				question = "What was the name of your first school? ";
				code = "2";
				break;
			case 2:
				question = "Who was your Childhood hero?";
				code = "3";

				break;
			case 3:
				question = "What is your favourite past-time?";
				code = "4";

				break;
			default:
				question = "What is your all time favourite sports team?";
				code = "5";

				break;
			}
			SecurityQuestion questions=securityQuestionRepository.findSecurityQuestionByCode(code);
			if(questions == null){
			questions = new SecurityQuestion();
			questions.setQuestion(question);
			questions.setCode(code);
			securityQuestionRepository.save(questions);
			}
		}
	}
}
