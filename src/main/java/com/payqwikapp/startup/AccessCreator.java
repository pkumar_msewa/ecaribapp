package com.payqwikapp.startup;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.AccessDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.repositories.AccessLogRepository;
import com.payqwikapp.util.StartupUtil;

public class AccessCreator {

    private final IUserApi userApi;
    private final AccessLogRepository accessLogRepository;

    public AccessCreator(IUserApi userApi, AccessLogRepository accessLogRepository) {
        this.userApi = userApi;
        this.accessLogRepository = accessLogRepository;
    }

    public void create(){
        User superAdmin = userApi.findByUserName(StartupUtil.SUPER_ADMIN);
        if(superAdmin != null){
            AccessDetails access = accessLogRepository.getByUser(superAdmin);
            if(access == null) {
                access = new AccessDetails();
                access.setDeductMoney(true);
                access.setLoadMoney(true);
                access.setRequestMoney(true);
                access.setSendGCM(true);
                access.setSendMail(true);
                access.setSendMoney(true);
                access.setSendSMS(true);
                access.setUpdateAuthAdmin(true);
                access.setUpdateAuthAgents(true);
                access.setUpdateAuthMerchants(true);
                access.setUpdateAuthUser(true);
                access.setUpdateLimits(true);
                access.setUser(superAdmin);
                accessLogRepository.save(access);
            }
        }
        User admin = userApi.findByUserName(StartupUtil.ADMIN);
        if(admin != null) {
            AccessDetails access = accessLogRepository.getByUser(admin);
            if(access == null){
                access = new AccessDetails();
                access.setDeductMoney(false);
                access.setLoadMoney(false);
                access.setRequestMoney(false);
                access.setSendGCM(true);
                access.setSendMail(false);
                access.setSendMoney(false);
                access.setSendSMS(false);
                access.setUpdateAuthAdmin(false);
                access.setUpdateAuthAgents(false);
                access.setUpdateAuthMerchants(false);
                access.setUpdateAuthUser(true);
                access.setUpdateLimits(false);
                access.setUser(admin);
                accessLogRepository.save(access);
            }
        }

    }

}
