package com.payqwikapp.startup;

import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;

public class ReferAndEarnCreator {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public ReferAndEarnCreator(UserRepository userRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, PQAccountDetailRepository pqAccountDetailRepository,
			PQAccountTypeRepository pqAccountTypeRepository, PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, PQServiceRepository pqServiceRepository,
			ICommissionApi commissionApi) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		createReferAndEarnUser();
		createReferAndEarnService();
	}

	private void createReferAndEarnUser() {
		User refer = userRepository.findByUsername(StartupUtil.REFER_EARN);
		if (refer == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("Refer");
			detail.setMiddleName(" ");
			detail.setLastName("Earn");
			detail.setEmail(StartupUtil.REFER_EARN);
			detail.setContactNo(StartupUtil.REFER_EARN);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(10000);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(pqAccountTypeRepository.findByCode("KYC"));
			pqAccountDetailRepository.save(pqAccountDetail);

			refer = new User();
			refer.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			refer.setPassword(passwordEncoder.encode("12345678"));
			refer.setCreated(new Date());
			refer.setMobileStatus(Status.Active);
			refer.setEmailStatus(Status.Active);
			refer.setUserType(UserType.Locked);
			refer.setUsername(detail.getContactNo());
			refer.setUserDetail(detail);
			refer.setAccountDetail(pqAccountDetail);
			userRepository.save(refer);
		}

	}

	private void createReferAndEarnService() {

		PQOperator referOp = pqOperatorRepository.findOperatorByName("ReferNearn");
		if (referOp == null) {
			referOp = new PQOperator();
			referOp.setName("ReferNearn");
			referOp.setStatus(Status.Active);
			pqOperatorRepository.save(referOp);
		}

		PQServiceType referServiceType = pqServiceTypeRepository.findServiceTypeByName("ReferNearn");
		if (referServiceType == null) {
			referServiceType = new PQServiceType();
			referServiceType.setName("ReferNearn");
			referServiceType.setDescription("ReferNearn payment in VPayQwik");
			referServiceType = pqServiceTypeRepository.save(referServiceType);
		}

		PQService referService = pqServiceRepository.findServiceByCode("RFNE");
		if (referService == null) {
			referService = new PQService();
			referService.setName("ReferNearn");
			referService.setDescription("ReferNearn Payment in VPayQwik");
			referService.setMinAmount(1);
			referService.setMaxAmount(21);
			referService.setCode("RFNE");
			referService.setOperator(referOp);
			referService.setOperatorCode("RFNE");
			referService.setStatus(Status.Active);
			referService.setServiceType(referServiceType);
			pqServiceRepository.save(referService);
			System.err.println("service saved...." + referService.getCode());
		}

		PQCommission referCommision = new PQCommission();
		referCommision.setMinAmount(1);
		referCommision.setMaxAmount(10000);
		referCommision.setType("POST");
		referCommision.setValue(0);
		referCommision.setFixed(true);
		referCommision.setService(referService);
		referCommision.setIdentifier(commissionApi.createCommissionIdentifier(referCommision));
		if (commissionApi.findCommissionByIdentifier(referCommision.getIdentifier()) == null) {
			commissionApi.save(referCommision);
		}

	}

}
