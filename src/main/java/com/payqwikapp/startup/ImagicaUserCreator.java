package com.payqwikapp.startup;


import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.*;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.*;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;

public class ImagicaUserCreator {

    private final ImagicaSessionRepository imagicaSessionRepository;
    private final UserRepository userRepository;
    private final UserDetailRepository userDetailRepository;
    private final PQServiceRepository serviceRepository;
    private final PQServiceTypeRepository serviceTypeRepository;
    private final ICommissionApi commissionApi;
    private final PQAccountDetailRepository accountDetailRepository;
    private final PQAccountTypeRepository accountTypeRepository;
    private final PasswordEncoder passwordEncoder;
    private final PQOperatorRepository operatorRepository;
    public ImagicaUserCreator(ImagicaSessionRepository imagicaSessionRepository, UserRepository userRepository, UserDetailRepository userDetailRepository, PQServiceRepository serviceRepository, PQServiceTypeRepository serviceTypeRepository, ICommissionApi commissionApi, PQAccountDetailRepository accountDetailRepository, PQAccountTypeRepository accountTypeRepository,PasswordEncoder passwordEncoder,PQOperatorRepository operatorRepository) {
        this.imagicaSessionRepository = imagicaSessionRepository;
        this.userRepository = userRepository;
        this.userDetailRepository = userDetailRepository;
        this.serviceRepository = serviceRepository;
        this.serviceTypeRepository = serviceTypeRepository;
        this.commissionApi = commissionApi;
        this.accountDetailRepository = accountDetailRepository;
        this.accountTypeRepository = accountTypeRepository;
        this.passwordEncoder = passwordEncoder;
        this.operatorRepository = operatorRepository;
    }

    public void create(){
           // createUser();
           // createService();
            createSessionUser();
    }

    private void createUser(){
        User imagica = userRepository.findByUsername(StartupUtil.IMAGICA);
        if (imagica == null) {
            UserDetail detail = new UserDetail();
            detail.setAddress("Karnataka");
            detail.setContactNo("7022620747");
            detail.setFirstName("Imagica");
            detail.setMiddleName("_");
            detail.setLastName(" ");
            detail.setEmail(StartupUtil.IMAGICA);
            userDetailRepository.save(detail);

            PQAccountDetail pqAccountDetail = new PQAccountDetail();
            pqAccountDetail.setBalance(0);
            pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
            pqAccountDetail.setAccountType(accountTypeRepository.findByCode("KYC"));
            accountDetailRepository.save(pqAccountDetail);

            imagica = new User();
            imagica.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
            imagica.setPassword(passwordEncoder.encode("123456789"));
            imagica.setCreated(new Date());
            imagica.setMobileStatus(Status.Active);
            imagica.setEmailStatus(Status.Active);
            imagica.setUserType(UserType.Merchant);
            imagica.setUsername(detail.getEmail());
            imagica.setUserDetail(detail);
            imagica.setAccountDetail(pqAccountDetail);
            userRepository.save(imagica);
        }

    }

    private void createService() {

        PQOperator imagicaOperator = operatorRepository.findOperatorByName("Imagica");
        if (imagicaOperator == null) {
            imagicaOperator = new PQOperator();
            imagicaOperator.setName("Imagica");
            imagicaOperator.setStatus(Status.Active);
            operatorRepository.save(imagicaOperator);
        }

        PQServiceType adlabServiceType = serviceTypeRepository.findServiceTypeByName("Imagica Booking");
        if (adlabServiceType == null) {
            adlabServiceType = new PQServiceType();
            adlabServiceType.setName("Imagica Booking");
            adlabServiceType.setDescription("Adlabs Imagica ticket booking in VPayQwik");
            adlabServiceType = serviceTypeRepository.save(adlabServiceType);
        }

        PQService imagicaService = serviceRepository.findServiceByCode(StartupUtil.IMAGICA_SERVICE);
        if (imagicaService == null) {
            imagicaService = new PQService();
            imagicaService.setName("Adlabs Imagica");
            imagicaService.setDescription("Adlabs Imagica ticket booking in VPayQwik");
            imagicaService.setMinAmount(10);
            imagicaService.setMaxAmount(100000);
            imagicaService.setCode(StartupUtil.IMAGICA_SERVICE);
            imagicaService.setOperator(imagicaOperator);
            imagicaService.setOperatorCode(StartupUtil.IMAGICA_SERVICE);
            imagicaService.setStatus(Status.Active);
            imagicaService.setServiceType(adlabServiceType);
            serviceRepository.save(imagicaService);
        }

        PQCommission adlabCommission = new PQCommission();
        adlabCommission.setMinAmount(10);
        adlabCommission.setMaxAmount(100000);
        adlabCommission.setType("POST");
        adlabCommission.setValue(0);
        adlabCommission.setFixed(true);
        adlabCommission.setService(imagicaService);
        adlabCommission.setIdentifier(commissionApi.createCommissionIdentifier(adlabCommission));
        if (commissionApi.findCommissionByIdentifier(adlabCommission.getIdentifier()) == null) {
            commissionApi.save(adlabCommission);
        }


    }

    private void createSessionUser(){
        ImagicaSession imagicaSession = imagicaSessionRepository.getByUserName("info@msewa.com");
        if(imagicaSession == null) {
            imagicaSession = new ImagicaSession();
            imagicaSession.setUsername("info@msewa.com");
            imagicaSession.setPassword(SecurityUtil.encryptString("123456"));
            imagicaSession.setLoginTime(new Date());
            imagicaSessionRepository.save(imagicaSession);
        }
    }




}
