package com.payqwikapp.startup;

import com.payqwikapp.entity.PartnerDetail;
import com.payqwikapp.repositories.PartnerDetailRepository;
import com.payqwikapp.util.SecurityUtil;

public class PartnerCreator {

    private final PartnerDetailRepository partnerDetailRepository;

    public PartnerCreator(PartnerDetailRepository partnerDetailRepository) {
        this.partnerDetailRepository = partnerDetailRepository;
    }

    public void create() {
        try {
            String lavaPartnerKey = SecurityUtil.md5(SecurityUtil.getPartnerConstantKey()+"LAVA");
            PartnerDetail lava = partnerDetailRepository.findByKey(lavaPartnerKey);
            if(lava == null) {
                lava = new PartnerDetail();
                lava.setApiKey(lavaPartnerKey);
                lava.setName("LAVA");
                partnerDetailRepository.save(lava);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
