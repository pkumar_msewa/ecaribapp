package com.payqwikapp.startup;
import com.ebs.model.EBSRequest;
import com.instantpay.model.Balance;
import com.instantpay.util.InstantPayConstants;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.*;
import com.payqwikapp.model.*;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.URegisterRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DemoCreator {

    static long count = 0;
    private final UserRepository userRepository;
    private final PQAccountDetailRepository pqAccountDetailRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailRepository userDetailRepository;
    private final URegisterRepository uRegisterRepository;
    public DemoCreator(UserRepository userRepository, PQAccountDetailRepository pqAccountDetailRepository,PasswordEncoder passwordEncoder,UserDetailRepository userDetailRepository,URegisterRepository uRegisterRepository) {
        this.userRepository = userRepository;
        this.pqAccountDetailRepository = pqAccountDetailRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDetailRepository = userDetailRepository;
        this.uRegisterRepository = uRegisterRepository;
    }

    public void create() throws Exception {

//        String fileName = StartupUtil.CSV_FILE+"mobile_nos.csv";
//        saveNos(readMobileNosFromFile(fileName).subList(240000,480000));
        List<String> maleList = readNamesFromFile(StartupUtil.CSV_FILE+"male_csv.csv");
        List<String> femaleList = readNamesFromFile(StartupUtil.CSV_FILE+"female_csv.csv");
        List<String> dobList = getDOB();
        List<String> locationList = getLocationCode();

        System.err.println("MLIST SIZE"+maleList.size());
        System.err.println("FLIST SIZE"+femaleList.size());

        Random mrandom = new Random();
//        List<URegister> mobileNosList = uRegisterRepository.findByStatus(Status.Inactive);
        List<URegister> mobileNosList = uRegisterRepository.findByStatus(Status.Inactive);
        if(mobileNosList != null && mobileNosList.size() > 0) {
            for(URegister user: mobileNosList) {
                String mobile = user.getUsername();
                int mindex = mrandom.nextInt(maleList.size());
                int findex = mrandom.nextInt(femaleList.size());
                int loc_index = mrandom.nextInt(locationList.size());
                int dob_index = mrandom.nextInt(dobList.size());
                String firstName = "";
                URegisterDTO registerDTO  = new URegisterDTO();
                if(mindex%2 ==0) {
                    firstName = maleList.get(mindex);
                    registerDTO.setGender(Gender.M);
                }else {
                    firstName = femaleList.get(findex);
                    registerDTO.setGender(Gender.F);
                }
                registerDTO.setFirstName(firstName.toUpperCase());
                registerDTO.setLastName(" ");
                registerDTO.setContactNo(mobile);
                registerDTO.setPassword(registerDTO.getContactNo().substring(0,6));
                registerDTO.setLocationCode(locationList.get(loc_index));
                registerDTO.setMpin(mobile.substring(0,4));
                registerDTO.setEmail(""+firstName.trim().toUpperCase()+mobile.substring(4,6)+"@gmail.com");
                registerDTO.setDateOfBirth(dobList.get(dob_index));
//              registerDTO = fetchName(registerDTO);
                System.err.println(registerDTO.toJSON());
                if(registerUser(registerDTO)) {
                    count += 1;
                    user.setStatus(Status.Active);
                    uRegisterRepository.save(user);
                    if(count >= 60_000) {
                        break;
                    }
                }
                System.err.print(count);
            }
        }
    }

    private void saveNos(List<String> mobileNos) {
        if(mobileNos != null && mobileNos.size()>0) {
            for(String mobile: mobileNos) {
                URegister exists = uRegisterRepository.findByUsername(mobile);
                if(exists == null) {
                    exists = new URegister();
                    exists.setUsername(mobile);
                    exists.setStatus(Status.Inactive);
                    uRegisterRepository.save(exists);
                }
            }
        }
    }
    private URegisterDTO fetchName(URegisterDTO registerDTO){
        try {
            CommonUtil.sleep(1000);
            String stringResponse = "";
            WebResource resource = Client.create().resource("https://www.truecaller.com/api/search").queryParam("type","4").queryParam("countryCode","IN").queryParam("q",registerDTO.getContactNo());
            ClientResponse clientResponse = resource.type("application/json").header("authorization","Bearer ZLsBmjASpe7BlHqv3JI~BWk0p3defxpp").header("cookie","__cfduid=d25131a99949cad0e7f5f9294c91f07111497273480; _ga=GA1.2.231773868.1497273475; _gid=GA1.2.252919665.1497273475; _gat=1; XLBS3=XLBS1|WT6Z+|WT6Ui").header("user-agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36").get(ClientResponse.class);
            stringResponse = clientResponse.getEntity(String.class);
            System.err.println(stringResponse);
            if (clientResponse.getStatus() == 200) {
                JSONObject json = new JSONObject(stringResponse);
                if(json != null) {
                    JSONArray data = json.getJSONArray("data");
                    JSONObject object = data.getJSONObject(0);
                    registerDTO.setFirstName(object.getString("name"));
                    if(object.has("gender")){
                       String gender =  object.getString("gender");
                       if(gender.equalsIgnoreCase("MALE")) {
                           registerDTO.setGender(Gender.M);
                       }else{
                           registerDTO.setGender(Gender.F);
                       }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return registerDTO;

    }

    private boolean registerUser(URegisterDTO registerDTO){
        boolean valid = false;
        try {
            String stringResponse = "";
            WebResource resource = Client.create().resource("https://www.vpayqwik.com/Api/v1/User/Android/en/WebRegistration/URegisterUser");
            ClientResponse clientResponse = resource.header("API_KEY",SecurityUtil.getKey()).post(ClientResponse.class,registerDTO.toJSON());
            stringResponse = clientResponse.getEntity(String.class);
            System.err.println(stringResponse);
            if (clientResponse.getStatus() == 200) {
                JSONObject json = new JSONObject(stringResponse);
                String code  = json.getString("code");
                if(code.equals("S00")) {
                    valid = true;
                }else if(code.equals("F04")) {
                    String message = json.getString("message");
                    if(message.equalsIgnoreCase("User Already Exist")) {
                        valid = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valid;

    }


    public List<String> getLocationCode(){
        List<String> locationList = new ArrayList<>();
        locationList.add("121103");
        locationList.add("172029");
        locationList.add("221003");
        locationList.add("757001");
        locationList.add("560034");
        locationList.add("560068");
        locationList.add("851211");
        locationList.add("560034");
        locationList.add("560076");
        locationList.add("560045");
        locationList.add("302001");
        locationList.add("302005");
        return locationList;
    }

    public List<String> getDOB(){
        List<String> dob = new ArrayList<>();
        dob.add("1950-01-06");
        dob.add("1956-04-21");
        dob.add("1956-02-15");
        dob.add("1994-07-18");
        dob.add("1984-12-07");
        dob.add("1978-06-13");
        dob.add("1980-01-12");
        dob.add("1990-09-05");
        dob.add("1997-10-30");
        dob.add("1995-05-29");
        dob.add("1992-04-11");
        dob.add("1991-03-20");
        return dob;
    }


    public ArrayList<String> readMobileNosFromFile(String fileName) {
        ArrayList<String> mobileNo = new ArrayList<>();
        System.err.println(fileName);
        BufferedReader br = null;
        String line = "";
        try {

            br = new BufferedReader(new FileReader(fileName));
            String mobile = null;
            while ((line = br.readLine()) != null) {
                Pattern p = Pattern.compile("(([^\"][^;]*)|\"([^\"]*)\");?");
                Matcher m = p.matcher(line);
                mobile = new String();
                String value = null;
                int index = 1;
                while (m.find()) {
                    if (m.group(2) != null) {
                        value = m.group(2);
                    }

                    if (m.group(3) != null) {
                        value = m.group(3);
                    }

                    if (value != null) {
                        if (mobile != null) {
                            switch (index) {
                                case 1:
                                    System.err.println("mobile no is "+value);
                                    mobile = value.trim().substring(2);
                                    break;
                                default:
                                    break;
                            }
                            index = index + 1;
                        }
                    }
                    mobileNo.add(mobile);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return mobileNo;
    }


    public ArrayList<String> readNamesFromFile(String fileName) {
        ArrayList<String> mnames = new ArrayList<>();
        System.err.println(fileName);
        BufferedReader br = null;
        String line = "";
        try {
            br = new BufferedReader(new FileReader(fileName));
            String mname = null;
            while ((line = br.readLine()) != null) {
                Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
                Matcher m = p.matcher(line);
                mname = new String();
                String value = null;
                int index = 1;
                while (m.find()) {
                    if (m.group(2) != null) {
                        value = m.group(2);

                    }

                    if (m.group(3) != null) {
                        value = m.group(3);
                    }

                    if (value != null) {
                        if (mname != null) {
                            switch (index) {
                                case 1:
                                    System.err.println(""+value);
                                    mname = value;
                                    break;
                                default:
                                    break;
                            }
                            index = index + 1;
                        }
                    }
                    mnames.add(mname);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return mnames;
    }


    private InsuranceBillPaymentDTO convertInsuranceRequest(String request) {
        InsuranceBillPaymentDTO dto = new InsuranceBillPaymentDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setServiceProvider(JSONParserUtil.getString(json,"serviceProvider"));
            dto.setPolicyDate(JSONParserUtil.getString(json,"policyDate"));
            dto.setPolicyNumber(JSONParserUtil.getString(json,"policyNumber"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

    private ElectricityBillPaymentDTO convertECityRequest(String request) {
        ElectricityBillPaymentDTO dto = new ElectricityBillPaymentDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setAccountNumber(JSONParserUtil.getString(json,"accountNumber"));
            dto.setServiceProvider(JSONParserUtil.getString(json,"serviceProvider"));
            dto.setCityName(JSONParserUtil.getString(json,"cityName"));
            dto.setProcessingCycle(JSONParserUtil.getString(json,"processingCycle"));
            dto.setCycleNumber(JSONParserUtil.getString(json,"cycleNumber"));
            dto.setBillingUnit(JSONParserUtil.getString(json,"billingUnit"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

    private GasBillPaymentDTO convertGasRequest(String request) {
        GasBillPaymentDTO dto = new GasBillPaymentDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setServiceProvider(JSONParserUtil.getString(json,"serviceProvider"));
            dto.setAccountNumber(JSONParserUtil.getString(json,"accountNumber"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

    private LandlineBillPaymentDTO convertLandlineRequest(String request) {
        LandlineBillPaymentDTO dto = new LandlineBillPaymentDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setServiceProvider(JSONParserUtil.getString(json,"serviceProvider"));
            dto.setAccountNumber(JSONParserUtil.getString(json,"accountNumber"));
            dto.setLandlineNumber(JSONParserUtil.getString(json,"landlineNumber"));
            dto.setStdCode(JSONParserUtil.getString(json,"stdCode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }
    private MobileTopupDTO convertMTRequest(String request){
        MobileTopupDTO dto  = new MobileTopupDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setArea(JSONParserUtil.getString(json,"area"));
            dto.setMobileNo(JSONParserUtil.getString(json,"mobileNo"));
            dto.setServiceProvider(JSONParserUtil.getString(json,"serviceProvider"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

    private SendMoneyBankDTO convertBTransferRequest(String request){
        SendMoneyBankDTO dto = new SendMoneyBankDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setAccountName(JSONParserUtil.getString(json,"accountName"));
            dto.setAccountNumber(JSONParserUtil.getString(json,"accountNumber"));
            dto.setBankCode(JSONParserUtil.getString(json,"bankCode"));
            dto.setIfscCode(JSONParserUtil.getString(json,"ifscCode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

    private DTHBillPaymentDTO convertDTHRequest(String request){
        DTHBillPaymentDTO dto = new DTHBillPaymentDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setServiceProvider(JSONParserUtil.getString(json,"serviceProvider"));
            dto.setDthNo(JSONParserUtil.getString(json,"dthNo"));
        }catch(JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

    private SendMoneyMobileDTO convertSMRequest(String request){
        SendMoneyMobileDTO dto = new SendMoneyMobileDTO();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json,"amount"));
            dto.setMessage(JSONParserUtil.getString(json,"message"));
            dto.setMobileNumber(JSONParserUtil.getString(json,"mobileNumber"));
        }catch(JSONException e){
            e.printStackTrace();
        }
        return dto;
    }


    private EBSRequest convertLMRequest(String request) {
        EBSRequest dto = new EBSRequest();
        try {
            JSONObject json = new JSONObject(request);
            dto.setAmount(JSONParserUtil.getString(json, "amount"));
            dto.setAccount_id(JSONParserUtil.getString(json, "account_id"));
            dto.setAddress(JSONParserUtil.getString(json, "address"));
            dto.setChannel(JSONParserUtil.getString(json, "channel"));
            dto.setCity(JSONParserUtil.getString(json, "city"));
            dto.setCountry(JSONParserUtil.getString(json, "country"));
            dto.setDescription(JSONParserUtil.getString(json, "description"));
            dto.setCurrency(JSONParserUtil.getString(json, "currency"));
            dto.setEmail(JSONParserUtil.getString(json, "email"));
            dto.setMode(JSONParserUtil.getString(json, "mode"));
            dto.setName(JSONParserUtil.getString(json, "name"));
            dto.setPhone(JSONParserUtil.getString(json, "phone"));
            dto.setReference_no(JSONParserUtil.getString(json, "reference_no"));
            dto.setPostal_code(JSONParserUtil.getString(json, "postal_code"));
            dto.setReturn_url(JSONParserUtil.getString(json, "return_url"));
            dto.setSecure_hash(JSONParserUtil.getString(json, "secure_hash"));
            dto.setShip_address(JSONParserUtil.getString(json, "ship_address"));
            dto.setShip_city(JSONParserUtil.getString(json, "ship_city"));
            dto.setShip_country(JSONParserUtil.getString(json, "ship_country"));
            dto.setShip_name(JSONParserUtil.getString(json, "ship_name"));
            dto.setShip_postal_code(JSONParserUtil.getString(json, "ship_postal_code"));
            dto.setShip_phone(JSONParserUtil.getString(json, "ship_phone"));
            dto.setState(JSONParserUtil.getString(json, "state"));
            dto.setShip_state(JSONParserUtil.getString(json, "ship_state"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dto;
    }

//    private void processECityRequest(PQTransaction transaction) {
//        ElectricityBillPaymentDTO dto = convertECityRequest(transaction.getRequest());
//        userApi.saveECityRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processBTransferRequest(PQTransaction transaction){
//            SendMoneyBankDTO dto = convertBTransferRequest(transaction.getRequest());
//            userApi.saveBTransferRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processEBSRequest(PQTransaction transaction){
//        EBSRequest dto = convertLMRequest(transaction.getRequest());
//        userApi.saveLMRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processSMRequest(PQTransaction transaction){
//        SendMoneyMobileDTO dto = convertSMRequest(transaction.getRequest());
//        userApi.saveSendMoneyRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processMTRequest(PQTransaction transaction){
//        MobileTopupDTO dto = convertMTRequest(transaction.getRequest());
//        userApi.saveTopupRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processDTHRequest(PQTransaction transaction){
//        DTHBillPaymentDTO dto = convertDTHRequest(transaction.getRequest());
//        userApi.saveDTHRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processLandlineRequest(PQTransaction transaction){
//        LandlineBillPaymentDTO  dto = convertLandlineRequest(transaction.getRequest());
//        userApi.saveLandlineRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processGasRequest(PQTransaction transaction) {
//        GasBillPaymentDTO dto = convertGasRequest(transaction.getRequest());
//        userApi.saveGasRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private void processInsuranceRequest(PQTransaction transaction){
//        InsuranceBillPaymentDTO dto = convertInsuranceRequest(transaction.getRequest());
//        userApi.saveInsuranceRequest(dto,getActualRefNo(transaction.getTransactionRefNo()));
//    }
//
//    private String getActualRefNo(String transactionRefNo){
//        String actualRefNo = transactionRefNo.substring(0,transactionRefNo.length()-1);
//        return actualRefNo;
//    }


}
