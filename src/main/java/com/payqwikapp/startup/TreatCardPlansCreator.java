package com.payqwikapp.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.TreatCardPlans;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.TreatCardPlansRepository;

public class TreatCardPlansCreator {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private TreatCardPlansRepository treatCardPlansRepository;

	public TreatCardPlansCreator(TreatCardPlansRepository treatCardPlansRepository) {
		this.treatCardPlansRepository = treatCardPlansRepository;
	}
	
	public void create() {
		for (int i = 0; i < 5; i++) {
			String days = "";
			double amount = 0;
			switch (i) {
			case 0:
				days = "30";
				amount = 100;
				break;
			case 1:
				days = "180";
				amount = 200;
				break;
			default:
				days = "360";
				amount = 300;
				break;
			}
			
			TreatCardPlans plans=treatCardPlansRepository.findPlansByDays(days);
			if(plans == null){
				plans = new TreatCardPlans();
				plans.setDays(days);
				plans.setAmount(amount);
				plans.setStatus(Status.Active);
				treatCardPlansRepository.save(plans);
			}
		}
	}

}
