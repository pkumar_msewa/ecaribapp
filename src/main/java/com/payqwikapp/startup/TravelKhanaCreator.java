package com.payqwikapp.startup;

import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;

public class TravelKhanaCreator {
	
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public TravelKhanaCreator(UserRepository userRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, PQAccountDetailRepository pqAccountDetailRepository,
			PQAccountTypeRepository pqAccountTypeRepository, PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, PQServiceRepository pqServiceRepository,
			ICommissionApi commissionApi) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		System.err.println("inside travel khana creator");
		createTravelKhanaUser();
		createTravelKhanaService();
	}

	private void createTravelKhanaUser() {
		System.err.println("inside travelkhana user");
		User travelkhana = userRepository.findByUsername(StartupUtil.TRAVELKHANA);
		if (travelkhana == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("Travel");
			detail.setMiddleName(" ");
			detail.setLastName("Khana");
			detail.setEmail(StartupUtil.TRAVELKHANA);
			detail.setContactNo(StartupUtil.TRAVELKHANA);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(pqAccountTypeRepository.findByCode("KYC"));
			pqAccountDetailRepository.save(pqAccountDetail);

			travelkhana = new User();
			travelkhana.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			travelkhana.setPassword(passwordEncoder.encode("12345678"));
			travelkhana.setCreated(new Date());
			travelkhana.setMobileStatus(Status.Active);
			travelkhana.setEmailStatus(Status.Active);
			travelkhana.setUserType(UserType.Locked);
			travelkhana.setUsername(detail.getContactNo());
			travelkhana.setUserDetail(detail);
			travelkhana.setAccountDetail(pqAccountDetail);
			userRepository.save(travelkhana);
		}

	}

	private void createTravelKhanaService() {

		PQOperator vpayqwik_travelkhana = pqOperatorRepository.findOperatorByName("Travelkhana");
		if(vpayqwik_travelkhana==null){
			vpayqwik_travelkhana = new PQOperator();
			vpayqwik_travelkhana.setName("Travelkhana");
			vpayqwik_travelkhana.setStatus(Status.Active);
			pqOperatorRepository.save(vpayqwik_travelkhana);
		}
		
		PQServiceType serviceTravelkhana = pqServiceTypeRepository.findServiceTypeByName("Travelkhana");
        if(serviceTravelkhana==null){
        	serviceTravelkhana = new PQServiceType();
        	serviceTravelkhana .setName("Travelkhana");
        	serviceTravelkhana.setDescription("Payment to travel khana");
        	serviceTravelkhana = pqServiceTypeRepository.save(serviceTravelkhana);
        }
        
		PQService travelkhana= pqServiceRepository.findServiceByCode(StartupUtil.TRAVELKAHANA_SERVICECODE);
        if(travelkhana==null){
        	travelkhana = new PQService();
        	travelkhana.setName("TKHANA");
        	travelkhana.setDescription("Payment to travel khana");
        	travelkhana.setMinAmount(10);
        	travelkhana.setMaxAmount(10000000);
        	travelkhana.setCode(StartupUtil.TRAVELKAHANA_SERVICECODE);
        	travelkhana.setOperator(vpayqwik_travelkhana);
        	travelkhana.setOperatorCode(StartupUtil.TRAVELKAHANA_SERVICECODE);
        	travelkhana.setStatus(Status.Active);
        	travelkhana.setServiceType(serviceTravelkhana);
			pqServiceRepository.save(travelkhana);
        }
        PQCommission travelkhanaCommision = new PQCommission();
        travelkhanaCommision.setMinAmount(1);
        travelkhanaCommision.setMaxAmount(10000);
        travelkhanaCommision.setType("POST");
        travelkhanaCommision.setValue(0);
        travelkhanaCommision.setFixed(true);
        travelkhanaCommision.setService(travelkhana);
        travelkhanaCommision.setIdentifier(commissionApi.createCommissionIdentifier(travelkhanaCommision));
        if(commissionApi.findCommissionByIdentifier(travelkhanaCommision.getIdentifier())==null){
        	commissionApi.save(travelkhanaCommision);
        }
        
  }

}
