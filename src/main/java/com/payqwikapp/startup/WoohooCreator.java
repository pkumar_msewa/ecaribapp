package com.payqwikapp.startup;

import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;

public class WoohooCreator {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public WoohooCreator(UserRepository userRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, PQAccountDetailRepository pqAccountDetailRepository,
			PQAccountTypeRepository pqAccountTypeRepository, PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, PQServiceRepository pqServiceRepository,
			ICommissionApi commissionApi) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		createWoohooUser();
		createWoohooService();

	}

	private void createWoohooUser() {
		User woohoo = userRepository.findByUsername(StartupUtil.WOOHOO);
		if (woohoo == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Koramangala");
			detail.setContactNo("1234567890");
			detail.setFirstName("Woohoo");
			detail.setMiddleName("P");
			detail.setLastName("Gift");
			detail.setEmail(StartupUtil.WOOHOO);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(pqAccountTypeRepository.findByCode("KYC"));
			pqAccountDetailRepository.save(pqAccountDetail);

			woohoo = new User();
			woohoo.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			woohoo.setPassword(passwordEncoder.encode("woohooG!FT"));
			woohoo.setCreated(new Date());
			woohoo.setMobileStatus(Status.Active);
			woohoo.setEmailStatus(Status.Active);
			woohoo.setUserType(UserType.Locked);
			woohoo.setUsername(detail.getEmail());
			woohoo.setUserDetail(detail);
			woohoo.setAccountDetail(pqAccountDetail);
			userRepository.save(woohoo);
		}

	}

	private void createWoohooService() {

		PQOperator WoohooOperator = pqOperatorRepository.findOperatorByName("Woohoo");
		if (WoohooOperator == null) {
			WoohooOperator = new PQOperator();
			WoohooOperator.setName("Woohoo");
			WoohooOperator.setStatus(Status.Active);
			pqOperatorRepository.save(WoohooOperator);
		}

		PQServiceType woohoGift = pqServiceTypeRepository.findServiceTypeByName("woohoo Gift");
		if (woohoGift == null) {
			woohoGift = new PQServiceType();
			woohoGift.setName("woohoo Gift");
			woohoGift.setDescription("woohoo Gift Card in VPayqwik.");
			woohoGift = pqServiceTypeRepository.save(woohoGift);
		}

		PQService woohoo = pqServiceRepository.findServiceByCode(StartupUtil.WOOHOO_SERVICECODE);
		if (woohoo == null) {
			woohoo = new PQService();
			woohoo.setName("Woohoo_Gift");
			woohoo.setDescription("WOOHOO Payment in VPayQwik");
			woohoo.setMinAmount(100);
			woohoo.setMaxAmount(20000);
			woohoo.setCode(StartupUtil.WOOHOO_SERVICECODE);
			woohoo.setOperator(WoohooOperator);
			woohoo.setOperatorCode(StartupUtil.WOOHOO_SERVICECODE);
			woohoo.setStatus(Status.Active);
			woohoo.setServiceType(woohoGift);
			pqServiceRepository.save(woohoo);

			PQCommission Commision = new PQCommission();
			Commision.setMinAmount(1);
			Commision.setMaxAmount(20000);
			Commision.setType("PRE");
			Commision.setValue(0);
			Commision.setFixed(false);
			Commision.setService(woohoo);
			Commision.setIdentifier(commissionApi.createCommissionIdentifier(Commision));
			if (commissionApi.findCommissionByIdentifier(Commision.getIdentifier()) == null) {
				commissionApi.save(Commision);
			}
		}
	}

}