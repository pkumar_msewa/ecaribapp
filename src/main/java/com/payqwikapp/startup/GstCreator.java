package com.payqwikapp.startup;

import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;

public class GstCreator {
	
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;
	
	public GstCreator(UserRepository userRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, PQAccountDetailRepository pqAccountDetailRepository,
			PQAccountTypeRepository pqAccountTypeRepository,PQServiceTypeRepository pqServiceTypeRepository, PQOperatorRepository pqOperatorRepository,
			PQServiceRepository pqServiceRepository, ICommissionApi commissionApi) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		createGstUser();
		createGstService();
	}

	private void createGstUser() {
		
		User stateGST = userRepository.findByUsername(StartupUtil.STATE_GST);
		if (stateGST == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.STATE_GST);
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("State Gov Service Tax");
			detail.setEmail(StartupUtil.STATE_GST);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(pqAccountTypeRepository.findByCode("KYC"));
			pqAccountDetailRepository.save(pqAccountDetail);

			stateGST = new User();
			stateGST.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			stateGST.setPassword(passwordEncoder.encode("P@yQw!k_!^d!@"));
			stateGST.setCreated(new Date());
			stateGST.setMobileStatus(Status.Active);
			stateGST.setEmailStatus(Status.Active);
			stateGST.setUserType(UserType.Locked);
			stateGST.setUsername(detail.getContactNo());
			stateGST.setUserDetail(detail);
			stateGST.setAccountDetail(pqAccountDetail);
			userRepository.save(stateGST);
		}
		
		User centralGST = userRepository.findByUsername(StartupUtil.CENTRAL_GST);
		if (centralGST == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("Trinity Circle");
			detail.setContactNo(StartupUtil.CENTRAL_GST);
			detail.setFirstName("VPayQwik");
			detail.setMiddleName("_");
			detail.setLastName("Central Gov Service Tax");
			detail.setEmail(StartupUtil.CENTRAL_GST);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(pqAccountTypeRepository.findByCode("KYC"));
			pqAccountDetailRepository.save(pqAccountDetail);

			centralGST = new User();
			centralGST.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			centralGST.setPassword(passwordEncoder.encode("P@yQw!k_!^d!@"));
			centralGST.setCreated(new Date());
			centralGST.setMobileStatus(Status.Active);
			centralGST.setEmailStatus(Status.Active);
			centralGST.setUserType(UserType.Locked);
			centralGST.setUsername(detail.getContactNo());
			centralGST.setUserDetail(detail);
			centralGST.setAccountDetail(pqAccountDetail);
			userRepository.save(centralGST);
		}

	}

	private void createGstService() {
		
		
		PQServiceType serviceTax = pqServiceTypeRepository.findServiceTypeByName("Service Tax");
		if (serviceTax == null) {
			serviceTax = new PQServiceType();
			serviceTax.setName("Service Tax");
			serviceTax.setDescription("Service Tax in VPayQwik");
			serviceTax = pqServiceTypeRepository.save(serviceTax);
		}
		
		PQOperator vpayqwik = pqOperatorRepository.findOperatorByName("VPayQwik");
		if (vpayqwik == null) {
			vpayqwik = new PQOperator();
			vpayqwik.setName("VPayQwik");
			vpayqwik.setStatus(Status.Active);
			pqOperatorRepository.save(vpayqwik);
		}
		
		PQService stateGovGST = pqServiceRepository.findServiceByCode("SGST");
		if (stateGovGST == null) {
			stateGovGST = new PQService();
			stateGovGST.setName("State GST");
			stateGovGST.setDescription("State Government Goods and Service Tax  in VPayQwik");
			stateGovGST.setMinAmount(0);
			stateGovGST.setMaxAmount(100000);
			stateGovGST.setCode("SGST");
			stateGovGST.setOperator(vpayqwik);
			stateGovGST.setOperatorCode("SGST");
			stateGovGST.setStatus(Status.Active);
			stateGovGST.setServiceType(serviceTax);
			pqServiceRepository.save(stateGovGST);
		}

		PQCommission stateGSTCommission = new PQCommission();
		stateGSTCommission.setMinAmount(1);
		stateGSTCommission.setMaxAmount(10000);
		stateGSTCommission.setType("POST");
		stateGSTCommission.setValue(9);
		stateGSTCommission.setFixed(false);
		stateGSTCommission.setService(stateGovGST);
		stateGSTCommission.setIdentifier(commissionApi.createCommissionIdentifier(stateGSTCommission));
		if (commissionApi.findCommissionByIdentifier(stateGSTCommission.getIdentifier()) == null) {
			commissionApi.save(stateGSTCommission);
		}
		
		
		PQService centralGovGST = pqServiceRepository.findServiceByCode("CGST");
		if (centralGovGST == null) {
			centralGovGST = new PQService();
			centralGovGST.setName("Central GST");
			centralGovGST.setDescription("Central Goverment Goods and Service Tax in VPayQwik");
			centralGovGST.setMinAmount(0);
			centralGovGST.setMaxAmount(100000);
			centralGovGST.setCode("CGST");
			centralGovGST.setOperator(vpayqwik);
			centralGovGST.setOperatorCode("CGST");
			centralGovGST.setStatus(Status.Active);
			centralGovGST.setServiceType(serviceTax);
			pqServiceRepository.save(centralGovGST);
		}

		PQCommission centralGSTCommission = new PQCommission();
		centralGSTCommission.setMinAmount(1);
		centralGSTCommission.setMaxAmount(10000);
		centralGSTCommission.setType("POST");
		centralGSTCommission.setValue(9);
		centralGSTCommission.setFixed(false);
		centralGSTCommission.setService(centralGovGST);
		centralGSTCommission.setIdentifier(commissionApi.createCommissionIdentifier(centralGSTCommission));
		if (commissionApi.findCommissionByIdentifier(centralGSTCommission.getIdentifier()) == null) {
			commissionApi.save(centralGSTCommission);
		}


	}

}
