package com.payqwikapp.startup;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;

public class BillPaymentServieCreator {

	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public BillPaymentServieCreator( PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, PQServiceRepository pqServiceRepository,
			ICommissionApi commissionApi) {
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		System.err.println("inside billPayment service creator .");
		createBillPaymentService();

	}
	private void createBillPaymentService() {
		PQOperator vpayqwik = pqOperatorRepository.findOperatorByName("VPayQwik");
		if (vpayqwik == null) {
			vpayqwik = new PQOperator();
			vpayqwik.setName("VPayQwik");
			vpayqwik.setStatus(Status.Active);
			pqOperatorRepository.save(vpayqwik);
		}

		PQServiceType serviceBillPayment = pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
		if (serviceBillPayment == null) {
			serviceBillPayment = new PQServiceType();
			serviceBillPayment.setName("Bill Payment");
			serviceBillPayment.setDescription("Pay mobile and other utility bills.");
			serviceBillPayment = pqServiceTypeRepository.save(serviceBillPayment);
		}

		PQService easternPower = pqServiceRepository.findServiceByOperatorCode("AEE");
		if (easternPower == null) {
			easternPower = new PQService();
			easternPower.setName("APEPDCL-ANDHRA PRADESH");
			easternPower.setDescription("APEPDCL - ANDHRA PRADESH");
			easternPower.setMinAmount(1);
			easternPower.setMaxAmount(100000);
			easternPower.setCode("VAEE");
			easternPower.setOperatorCode("AEE");
			easternPower.setServiceType(serviceBillPayment);
			easternPower.setOperator(vpayqwik);
			easternPower.setStatus(Status.Active);
			pqServiceRepository.save(easternPower);
		}
		PQCommission commission = new PQCommission();
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(easternPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService southernPower = pqServiceRepository.findServiceByOperatorCode("ASE");
		if (southernPower == null) {
			southernPower = new PQService();
			southernPower.setName("APSPDCL - ANDHRA PRADESH");
			southernPower.setDescription("APSPDCL - ANDHRA PRADESH");
			southernPower.setMinAmount(1);
			southernPower.setMaxAmount(100000);
			southernPower.setCode("VASE");
			southernPower.setOperatorCode("ASE");
			southernPower.setServiceType(serviceBillPayment);
			southernPower.setOperator(vpayqwik);
			southernPower.setStatus(Status.Active);
			pqServiceRepository.save(southernPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(southernPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService bharatpurPower = pqServiceRepository.findServiceByOperatorCode("BPE");
		if (bharatpurPower == null) {
			bharatpurPower = new PQService();
			bharatpurPower.setName("BESL - BHARATPUR");
			bharatpurPower.setDescription("BESL - BHARATPUR");
			bharatpurPower.setMinAmount(1);
			bharatpurPower.setMaxAmount(100000);
			bharatpurPower.setCode("VBPE");
			bharatpurPower.setOperatorCode("BPE");
			bharatpurPower.setServiceType(serviceBillPayment);
			bharatpurPower.setOperator(vpayqwik);
			bharatpurPower.setStatus(Status.Active);
			pqServiceRepository.save(bharatpurPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(bharatpurPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService bikanerPower = pqServiceRepository.findServiceByOperatorCode("BKE");
		if (bikanerPower == null) {
			bikanerPower = new PQService();
			bikanerPower.setName("TorrentPower");
			bikanerPower.setDescription("TorrentPower Electricity Board");
			bikanerPower.setMinAmount(1);
			bikanerPower.setMaxAmount(100000);
			bikanerPower.setCode("VBKE");
			bikanerPower.setOperatorCode("BKE");
			bikanerPower.setServiceType(serviceBillPayment);
			bikanerPower.setOperator(vpayqwik);
			bikanerPower.setStatus(Status.Active);
			pqServiceRepository.save(bikanerPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(bikanerPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService damandiuPower = pqServiceRepository.findServiceByOperatorCode("DDE");
		if (damandiuPower == null) {
			damandiuPower = new PQService();
			damandiuPower.setName("Daman and Diu Electricity");
			damandiuPower.setDescription("Daman and Diu Electricity");
			damandiuPower.setMinAmount(1);
			damandiuPower.setMaxAmount(100000);
			damandiuPower.setCode("VDDE");
			damandiuPower.setOperatorCode("DDE");
			damandiuPower.setServiceType(serviceBillPayment);
			damandiuPower.setOperator(vpayqwik);
			damandiuPower.setStatus(Status.Active);
			pqServiceRepository.save(damandiuPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0); 
		commission.setFixed(true);
		commission.setService(damandiuPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService gujuratPower = pqServiceRepository.findServiceByOperatorCode("DGE");
		if (gujuratPower == null) {
			gujuratPower = new PQService();
			gujuratPower.setName("DGVCL - GUJARAT");
			gujuratPower.setDescription("DGVCL - GUJARAT");
			gujuratPower.setMinAmount(1);
			gujuratPower.setMaxAmount(100000);
			gujuratPower.setCode("VDGE");
			gujuratPower.setOperatorCode("DGE");
			gujuratPower.setServiceType(serviceBillPayment);
			gujuratPower.setOperator(vpayqwik);
			gujuratPower.setStatus(Status.Active);
			pqServiceRepository.save(gujuratPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(gujuratPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService gescomKarnatakaPower = pqServiceRepository.findServiceByOperatorCode("GKE");
		if (gescomKarnatakaPower == null) {
			gescomKarnatakaPower = new PQService();
			gescomKarnatakaPower.setName("GESCOM - KARNATAKA");
			gescomKarnatakaPower.setDescription("GESCOM - KARNATAKA");
			gescomKarnatakaPower.setMinAmount(1);
			gescomKarnatakaPower.setMaxAmount(100000);
			gescomKarnatakaPower.setCode("VGKE");
			gescomKarnatakaPower.setOperatorCode("GKE");
			gescomKarnatakaPower.setServiceType(serviceBillPayment);
			gescomKarnatakaPower.setOperator(vpayqwik);
			gescomKarnatakaPower.setStatus(Status.Active);
			pqServiceRepository.save(gescomKarnatakaPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(gescomKarnatakaPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService indiaPower = pqServiceRepository.findServiceByOperatorCode("IBE");
		if (indiaPower == null) {
			indiaPower = new PQService();
			indiaPower.setName("India Power - BIHAR");
			indiaPower.setDescription("India Power - BIHAR");
			indiaPower.setMinAmount(1);
			indiaPower.setMaxAmount(100000);
			indiaPower.setCode("VIBE");
			indiaPower.setOperatorCode("IBE");
			indiaPower.setServiceType(serviceBillPayment);
			indiaPower.setOperator(vpayqwik);
			indiaPower.setStatus(Status.Active);
			pqServiceRepository.save(indiaPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(indiaPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService rajasthanPower = pqServiceRepository.findServiceByOperatorCode("KRE");
		if (rajasthanPower == null) {
			rajasthanPower = new PQService();
			rajasthanPower.setName("Kota Electricity Distribution - RAJASTHAN");
			rajasthanPower.setDescription("Kota Electricity Distribution - RAJASTHAN");
			rajasthanPower.setMinAmount(1);
			rajasthanPower.setMaxAmount(100000);
			rajasthanPower.setCode("VKRE");
			rajasthanPower.setOperatorCode("KRE");
			rajasthanPower.setServiceType(serviceBillPayment);
			rajasthanPower.setOperator(vpayqwik);
			rajasthanPower.setStatus(Status.Active);
			pqServiceRepository.save(rajasthanPower);
		}
 		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(rajasthanPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService meghalayaPower = pqServiceRepository.findServiceByOperatorCode("MEE");
		if (meghalayaPower == null) {
			meghalayaPower = new PQService();
			meghalayaPower.setName("MEPDCL - MEGHALAYA");
			meghalayaPower.setDescription("MEPDCL - MEGHALAYA");
			meghalayaPower.setMinAmount(1);
			meghalayaPower.setMaxAmount(100000);
			meghalayaPower.setCode("VMME");
			meghalayaPower.setOperatorCode("MEE");
			meghalayaPower.setServiceType(serviceBillPayment);
			meghalayaPower.setOperator(vpayqwik);
			meghalayaPower.setStatus(Status.Active);
			pqServiceRepository.save(meghalayaPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(meghalayaPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService gujuratPower1 = pqServiceRepository.findServiceByOperatorCode("MGE");
		if (gujuratPower1 == null) {
			gujuratPower1 = new PQService();
			gujuratPower1.setName("MGVCL - GUJARAT");
			gujuratPower1.setDescription("MGVCL - GUJARAT");
			gujuratPower1.setMinAmount(1);
			gujuratPower1.setMaxAmount(100000);
			gujuratPower1.setCode("VMGE");
			gujuratPower1.setOperatorCode("MGE");
			gujuratPower1.setServiceType(serviceBillPayment);
			gujuratPower1.setOperator(vpayqwik);
			gujuratPower1.setStatus(Status.Active);
			pqServiceRepository.save(gujuratPower1);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(gujuratPower1);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService muzaffurPower = pqServiceRepository.findServiceByOperatorCode("MBE");
		if (muzaffurPower == null) {
			muzaffurPower = new PQService();
			muzaffurPower.setName("Muzaffarpur Vidyut Vitran");
			muzaffurPower.setDescription("Muzaffarpur Vidyut Vitran");
			muzaffurPower.setMinAmount(1);
			muzaffurPower.setMaxAmount(100000);
			muzaffurPower.setCode("VMBE");
			muzaffurPower.setOperatorCode("MBE");
			muzaffurPower.setServiceType(serviceBillPayment);
			muzaffurPower.setOperator(vpayqwik);
			muzaffurPower.setStatus(Status.Active);
			pqServiceRepository.save(muzaffurPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(muzaffurPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService biharPower = pqServiceRepository.findServiceByOperatorCode("NBE");
		if (biharPower == null) {
			biharPower = new PQService();
			biharPower.setName("NBPDCL - BIHAR");
			biharPower.setDescription("NBPDCL - BIHAR");
			biharPower.setMinAmount(1);
			biharPower.setMaxAmount(100000);
			biharPower.setCode("VNBE");
			biharPower.setOperatorCode("NBE");
			biharPower.setServiceType(serviceBillPayment);
			biharPower.setOperator(vpayqwik);
			biharPower.setStatus(Status.Active);
			pqServiceRepository.save(biharPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(biharPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService tataPower = pqServiceRepository.findServiceByOperatorCode("NOE");
		if (tataPower == null) {
			tataPower = new PQService();
			tataPower.setName("NESCO - ODISHA");
			tataPower.setDescription("NESCO - ODISHA");
			tataPower.setMinAmount(1);
			tataPower.setMaxAmount(100000);
			tataPower.setCode("VNOE");
			tataPower.setOperatorCode("NOE");
			tataPower.setServiceType(serviceBillPayment);
			tataPower.setOperator(vpayqwik);
			tataPower.setStatus(Status.Active);
			pqServiceRepository.save(tataPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(tataPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService paschimGujuratPower = pqServiceRepository.findServiceByOperatorCode("PGE");
		if (paschimGujuratPower == null) {
			paschimGujuratPower = new PQService();
			paschimGujuratPower.setName("PGVCL - GUJARAT");
			paschimGujuratPower.setDescription("PGVCL - GUJARAT");
			paschimGujuratPower.setMinAmount(1);
			paschimGujuratPower.setMaxAmount(100000);
			paschimGujuratPower.setCode("VPGE");
			paschimGujuratPower.setOperatorCode("PGE");
			paschimGujuratPower.setServiceType(serviceBillPayment);
			paschimGujuratPower.setOperator(vpayqwik);
			paschimGujuratPower.setStatus(Status.Active);
			pqServiceRepository.save(paschimGujuratPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(paschimGujuratPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService southBiharPower = pqServiceRepository.findServiceByOperatorCode("SBE");
		if (southBiharPower == null) {
			southBiharPower = new PQService();
			southBiharPower.setName("SBPDCL-BIHAR");
			southBiharPower.setDescription("SBPDCL - BIHAR");
			southBiharPower.setMinAmount(1);
			southBiharPower.setMaxAmount(100000);
			southBiharPower.setCode("VSBE");
			southBiharPower.setOperatorCode("SBE");
			southBiharPower.setServiceType(serviceBillPayment);
			southBiharPower.setOperator(vpayqwik);
			southBiharPower.setStatus(Status.Active);
			pqServiceRepository.save(southBiharPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(southBiharPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService nagpurPower = pqServiceRepository.findServiceByOperatorCode("SNE");
		if (nagpurPower == null) {
			nagpurPower = new PQService();
			nagpurPower.setName("SNDL Power-NAGPUR");
			nagpurPower.setDescription("SNDL Power - NAGPUR");
			nagpurPower.setMinAmount(1);
			nagpurPower.setMaxAmount(100000);
			nagpurPower.setCode("VSNE");
			nagpurPower.setOperatorCode("SNE");
			nagpurPower.setServiceType(serviceBillPayment);
			nagpurPower.setOperator(vpayqwik);
			nagpurPower.setStatus(Status.Active);
			pqServiceRepository.save(nagpurPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(nagpurPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService southOdishaPower = pqServiceRepository.findServiceByOperatorCode("SOE");
		if (southOdishaPower == null) {
			southOdishaPower = new PQService();
			southOdishaPower.setName("SOUTHCO - ODISHA");
			southOdishaPower.setDescription("SOUTHCO - ODISHA");
			southOdishaPower.setMinAmount(1);
			southOdishaPower.setMaxAmount(100000);
			southOdishaPower.setCode("VSOE");
			southOdishaPower.setOperatorCode("SOE");
			southOdishaPower.setServiceType(serviceBillPayment);
			southOdishaPower.setOperator(vpayqwik);
			southOdishaPower.setStatus(Status.Active);
			pqServiceRepository.save(southOdishaPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(southOdishaPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		
		PQService tataPower1 = pqServiceRepository.findServiceByOperatorCode("TME");
		if (tataPower1 == null) {
			tataPower1 = new PQService();
			tataPower1.setName("Tata Power - MUMBAI");
			tataPower1.setDescription("Tata Power - MUMBAI");
			tataPower1.setMinAmount(1);
			tataPower1.setMaxAmount(100000);
			tataPower1.setCode("VTME");
			tataPower1.setOperatorCode("TME");
			tataPower1.setServiceType(serviceBillPayment);
			tataPower1.setOperator(vpayqwik);
			tataPower1.setStatus(Status.Active);
			pqServiceRepository.save(tataPower1);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(tataPower1);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService tamilnaduPower = pqServiceRepository.findServiceByOperatorCode("TNE");
		if (tamilnaduPower == null) {
			tamilnaduPower = new PQService();
			tamilnaduPower.setName("TNEB - TAMIL NADU");
			tamilnaduPower.setDescription("TNEB - TAMIL NADU");
			tamilnaduPower.setMinAmount(1);
			tamilnaduPower.setMaxAmount(100000);
			tamilnaduPower.setCode("VTPE");
			tamilnaduPower.setOperatorCode("TPE");
			tamilnaduPower.setServiceType(serviceBillPayment);
			tamilnaduPower.setOperator(vpayqwik);
			tamilnaduPower.setStatus(Status.Active);
			pqServiceRepository.save(tamilnaduPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(tamilnaduPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService ajmerPower = pqServiceRepository.findServiceByOperatorCode("AJE");
		if (ajmerPower == null) {
			ajmerPower = new PQService();
			ajmerPower.setName("TPADL-AJMER");
			ajmerPower.setDescription("TPADL - AJMER");
			ajmerPower.setMinAmount(1);
			ajmerPower.setMaxAmount(100000);
			ajmerPower.setCode("VAJE");
			ajmerPower.setOperatorCode("AJE");
			ajmerPower.setServiceType(serviceBillPayment);
			ajmerPower.setOperator(vpayqwik);
			ajmerPower.setStatus(Status.Active);
			pqServiceRepository.save(ajmerPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(ajmerPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService uttarPower = pqServiceRepository.findServiceByOperatorCode("UGE");
		if (uttarPower == null) {
			uttarPower = new PQService();
			uttarPower.setName("UGVCL - GUJARAT");
			uttarPower.setDescription("UGVCL - GUJARAT");
			uttarPower.setMinAmount(1);
			uttarPower.setMaxAmount(100000);
			uttarPower.setCode("VUGE");
			uttarPower.setOperatorCode("UGE");
			uttarPower.setServiceType(serviceBillPayment);
			uttarPower.setOperator(vpayqwik);
			uttarPower.setStatus(Status.Active);
			pqServiceRepository.save(uttarPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(uttarPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService uttrakhandPower = pqServiceRepository.findServiceByOperatorCode("UKE");
		if (uttrakhandPower == null) {
			uttrakhandPower = new PQService();
			uttrakhandPower.setName("UPCL-UTTARAKHAND");
			uttrakhandPower.setDescription("UPCL - UTTARAKHAND");
			uttrakhandPower.setMinAmount(1);
			uttrakhandPower.setMaxAmount(100000);
			uttrakhandPower.setCode("VUKE");
			uttrakhandPower.setOperatorCode("UKE");
			uttrakhandPower.setServiceType(serviceBillPayment);
			uttrakhandPower.setOperator(vpayqwik);
			uttrakhandPower.setStatus(Status.Active);
			pqServiceRepository.save(uttrakhandPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(uttrakhandPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		
		PQService uttarPradeshPower = pqServiceRepository.findServiceByOperatorCode("URE");
		if (uttarPradeshPower == null) {
			uttarPradeshPower = new PQService();
			uttarPradeshPower.setName("UPPCL (RURAL) - UTTAR PRADESH");
			uttarPradeshPower.setDescription("UPPCL (RURAL) - UTTAR PRADESH");
			uttarPradeshPower.setMinAmount(1);
			uttarPradeshPower.setMaxAmount(100000);
			uttarPradeshPower.setCode("VURE");
			uttarPradeshPower.setOperatorCode("URE");
			uttarPradeshPower.setServiceType(serviceBillPayment);
			uttarPradeshPower.setOperator(vpayqwik);
			uttarPradeshPower.setStatus(Status.Active);
			pqServiceRepository.save(uttarPradeshPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(uttarPradeshPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		PQService uttarPradeshPower1 = pqServiceRepository.findServiceByOperatorCode("UPE");
		if (uttarPradeshPower1 == null) {
			uttarPradeshPower1 = new PQService();
			uttarPradeshPower1.setName("UPPCL (URBAN) - UTTAR PRADESH");
			uttarPradeshPower1.setDescription("UPPCL (URBAN) - UTTAR PRADESH");
			uttarPradeshPower1.setMinAmount(1);
			uttarPradeshPower1.setMaxAmount(100000);
			uttarPradeshPower1.setCode("VUPE");
			uttarPradeshPower1.setOperatorCode("UPE");
			uttarPradeshPower1.setServiceType(serviceBillPayment);
			uttarPradeshPower1.setOperator(vpayqwik);
			uttarPradeshPower1.setStatus(Status.Active);
			pqServiceRepository.save(uttarPradeshPower1);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(uttarPradeshPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		PQService wescoOdishaPower = pqServiceRepository.findServiceByOperatorCode("WOE");
		if (wescoOdishaPower == null) {
			wescoOdishaPower = new PQService();
			wescoOdishaPower.setName("WESCO-ODISHA");
			wescoOdishaPower.setDescription("WESCO - ODISHA");
			wescoOdishaPower.setMinAmount(1);
			wescoOdishaPower.setMaxAmount(100000);
			wescoOdishaPower.setCode("VWOE");
			wescoOdishaPower.setOperatorCode("WOE");
			wescoOdishaPower.setServiceType(serviceBillPayment);
			wescoOdishaPower.setOperator(vpayqwik);
			wescoOdishaPower.setStatus(Status.Active);
			pqServiceRepository.save(wescoOdishaPower);
		}
		commission = new PQCommission();
		commission.setMinAmount(1);
		commission.setMaxAmount(100000);
		commission.setType("POST");
		commission.setValue(0);
		commission.setFixed(true);
		commission.setService(wescoOdishaPower);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
  }
}
