package com.payqwikapp.startup;

import com.payqwikapp.entity.ApiVersion;
import com.payqwikapp.repositories.ApiVersionRepository;
import com.payqwikapp.util.SecurityUtil;

public class ClientApiLogsCreator {

	private final ApiVersionRepository apiVersionRepository;

	public ClientApiLogsCreator(ApiVersionRepository apiVersionRepository) {
		super();
		this.apiVersionRepository = apiVersionRepository;
	}
	
	public void create(){
		ApiVersion apiLogs=new ApiVersion();
		apiLogs.setName("Android");
		apiLogs.setApiStatus("Active");
		apiLogs.setApiVersion("v1");
		try {
			apiLogs.setApiKey(SecurityUtil.md5("7022620747"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		apiVersionRepository.save(apiLogs);
		
		ApiVersion apiLogs1=new ApiVersion();
		apiLogs1.setName("IOS");
		apiLogs1.setApiStatus("Active");
		apiLogs1.setApiVersion("v1");
		try {
			apiLogs1.setApiKey(SecurityUtil.md5("8895093779"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		apiVersionRepository.save(apiLogs1);
}
}
