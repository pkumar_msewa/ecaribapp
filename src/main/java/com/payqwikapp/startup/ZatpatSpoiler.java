package com.payqwikapp.startup;

	import com.m2p.api.IM2PApi;
	import com.payqwikapp.api.ISendMoneyApi;
	import com.payqwikapp.entity.PQService;
	import com.payqwikapp.model.SendMoneyMobileDTO;
	import com.payqwikapp.repositories.POSMerchantsRepository;
	import com.payqwikapp.repositories.PQServiceRepository;
	import com.payqwikapp.util.StartupUtil;

	import java.io.BufferedReader;
	import java.io.FileNotFoundException;
	import java.io.FileReader;
	import java.io.IOException;
	import java.util.*;
	import java.util.regex.Matcher;
	import java.util.regex.Pattern;

	/**
	 * Created on 27-09-2017 by Vibhanshu
	 * this class is used on startup to credit amount in customer wallet
	 * the amount is the transaction amount recorded in zatpat wallet
	 *
	 */
	public class ZatpatSpoiler {

	    private ISendMoneyApi sendMoneyApi;
	    private PQServiceRepository serviceRepository;
	    //TODO sender username here
	    private static final String SENDER = "9999999123";
	    public ZatpatSpoiler(ISendMoneyApi sendMoneyApi,PQServiceRepository serviceRepository) {
	        this.sendMoneyApi = sendMoneyApi;
	        this.serviceRepository = serviceRepository;
	    }

	    public void create(){
	        /*TODO switch file name here */
	        String fileName = StartupUtil.CSV_FILE+"preloadedaccounts_nov.csv";
	        Set<SendMoneyMobileDTO> sendMoneyList = readFromFile(fileName);
	        Iterator<SendMoneyMobileDTO> iterator = sendMoneyList.iterator();
	        if(sendMoneyList != null && !sendMoneyList.isEmpty()) {
	            while(iterator.hasNext()) {
	                SendMoneyMobileDTO dto = iterator.next();
	               String serviceCode =  sendMoneyApi.prepareSendMoney(dto.getMobileNumber());
	                PQService service = serviceRepository.findServiceByCode(serviceCode);
	                if(service != null && Double.parseDouble(dto.getAmount()) > 0 )
	                sendMoneyApi.sendMoneyMobile(dto,SENDER,service);
	            }
	        }
	    }


	    public Set<SendMoneyMobileDTO> readFromFile(String fileName) {
	        Set<SendMoneyMobileDTO> sendMoneyList = new HashSet<>();
	        System.err.println(fileName);
	        BufferedReader br = null;
	        String line = "";
	        try {

	            br = new BufferedReader(new FileReader(fileName));
	            SendMoneyMobileDTO dto = null;
	            while ((line = br.readLine()) != null) {
	                Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
	                Matcher m = p.matcher(line);
	                dto = new SendMoneyMobileDTO();
	                String value = null;
	                int index = 1;
	                while (m.find()) {
	                    if (m.group(2) != null) {
	                        value = m.group(2);
	                    }

	                    if (m.group(3) != null) {
	                        value = m.group(3);
	                    }

	                    if (value != null) {
	                        if (dto != null) {
	                            switch (index) {
	                                case 1:
	                                    dto.setMobileNumber(value);
	                                    break;
	                                case 2:
	                                    dto.setMessage(value);
	                                    break;
	                                case 3:
	                                    dto.setAmount(value);
	                                    break;
	                                default:
	                                    break;
	                            }
	                            index = index + 1;
	                        }
	                    }
	                    sendMoneyList.add(dto);
	                }
	            }
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }

	        return sendMoneyList;
	    }

	}

