package com.payqwikapp.startup;

import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;

public class HouseJoyCreator {
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public HouseJoyCreator(UserRepository userRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, PQAccountDetailRepository pqAccountDetailRepository,
			PQAccountTypeRepository pqAccountTypeRepository, PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, PQServiceRepository pqServiceRepository,
			ICommissionApi commissionApi) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi = commissionApi;
	}

	public void create() {
		createHousejoyUser();
		createHousejoyService();
	}

	private void createHousejoyUser() {
		User travelkhana = userRepository.findByUsername(StartupUtil.HOUSE_JOY);
		if (travelkhana == null) {
			UserDetail detail = new UserDetail();
			detail.setAddress("BTM");
			detail.setFirstName("House");
			detail.setMiddleName(" ");
			detail.setLastName("Joy");
			detail.setEmail(StartupUtil.HOUSE_JOY);
			detail.setContactNo(StartupUtil.HOUSE_JOY);
			userDetailRepository.save(detail);

			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
			pqAccountDetail.setAccountType(pqAccountTypeRepository.findByCode("KYC"));
			pqAccountDetailRepository.save(pqAccountDetail);

			travelkhana = new User();
			travelkhana.setAuthority(Authorities.LOCKED + "," + Authorities.AUTHENTICATED);
			travelkhana.setPassword(passwordEncoder.encode("12345678"));
			travelkhana.setCreated(new Date());
			travelkhana.setMobileStatus(Status.Active);
			travelkhana.setEmailStatus(Status.Active);
			travelkhana.setUserType(UserType.Locked);
			travelkhana.setUsername(detail.getContactNo());
			travelkhana.setUserDetail(detail);
			travelkhana.setAccountDetail(pqAccountDetail);
			userRepository.save(travelkhana);
		}

	}

	private void createHousejoyService() {

		 PQOperator housJoyOp = pqOperatorRepository.findOperatorByName("HouseJoy");
		 if (housJoyOp == null) {
			 housJoyOp = new PQOperator();
			 housJoyOp.setName("HouseJoy");
			 housJoyOp.setStatus(Status.Active);
			 pqOperatorRepository.save(housJoyOp);
		 }

		 PQServiceType housJoyServiceType = pqServiceTypeRepository.findServiceTypeByName("HouseJoy_VPQ");
		 if (housJoyServiceType == null) {
			 housJoyServiceType = new PQServiceType();
			 housJoyServiceType.setName("HouseJoy_VPQ");
			 housJoyServiceType.setDescription("HouseJoy payment in VPayQwik");
			 housJoyServiceType = pqServiceTypeRepository.save(housJoyServiceType);
		 }

		 PQService housJoyService = pqServiceRepository.findServiceByCode(StartupUtil.HOUSE_JOY_CODE);
		 if (housJoyService == null) {
			 housJoyService = new PQService();
			 housJoyService.setName("HouseJoy_VPayQwik");
			 housJoyService.setDescription("HouseJoy Payment in VPayQwik");
			 housJoyService.setMinAmount(100);
			 housJoyService.setMaxAmount(20000);
			 housJoyService.setCode(StartupUtil.HOUSE_JOY_CODE);
			 housJoyService.setOperator(housJoyOp);
			 housJoyService.setOperatorCode(StartupUtil.HOUSE_JOY_CODE);
			 housJoyService.setStatus(Status.Active);
			 housJoyService.setServiceType(housJoyServiceType);
			 pqServiceRepository.save(housJoyService);
			 System.err.println("service saved...."+housJoyService.getCode());
		 }

		 PQCommission houseJoyCommision = new PQCommission();
		 houseJoyCommision.setMinAmount(1);
		 houseJoyCommision.setMaxAmount(20000);
		 houseJoyCommision.setType("PRE");
		 houseJoyCommision.setValue(5);
		 houseJoyCommision.setFixed(false);
		 houseJoyCommision.setService(housJoyService);
		 houseJoyCommision.setIdentifier(commissionApi.createCommissionIdentifier(houseJoyCommision));
		 if (commissionApi.findCommissionByIdentifier(houseJoyCommision.getIdentifier()) == null) {
			 commissionApi.save(houseJoyCommision);
		 }
        
  }

}
