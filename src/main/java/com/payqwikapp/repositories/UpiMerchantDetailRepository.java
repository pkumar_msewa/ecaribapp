package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.UpiMerchantDetail;

public interface UpiMerchantDetailRepository extends CrudRepository<UpiMerchantDetail,Long>,JpaSpecificationExecutor<UpiMerchantDetail>{

    @Query("select u from UpiMerchantDetail u where u.merchantVpa=?1")
    UpiMerchantDetail getUpiMerchant(String merchantVpa);

}
