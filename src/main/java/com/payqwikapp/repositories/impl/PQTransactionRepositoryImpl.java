package com.payqwikapp.repositories.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserDetailDTO;
import com.payqwikapp.repositories.PQTransactionRepositoryCustom;

public class PQTransactionRepositoryImpl   implements PQTransactionRepositoryCustom {

	private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    
    public void setDataSource(DataSource ds) {
        jdbcTemplate = new JdbcTemplate(ds);
    }

      
	PQTransactionRepositoryImpl() {}


	@Override
	public long getCountOfT() {
		String query = "Select count(*) from PQTransaction";
		Long resultSet = jdbcTemplate.queryForLong(query);
		return resultSet;
	}


	@Override
	public UserDetailDTO getUserDataForValidation(String username,int currentDay, int currentMonth,int currentYear) {
		String query = "Select u.username,u.mobileStatus,us.contactNo,pa.balance,pat.balanceLimit,pat.dailyLimit,pat.monthlyLimit,"
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=false) as monthlyCreditAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=true) as monthlyDebitAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND DAY(u.created) = '"+currentDay +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=false) as dailyCreditAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND DAY(u.created) = '"+currentDay +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=true) as dailyDebitAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND pqtr.account_id = pa.id AND pqtr.status='Success') as totalMonthlyTransaction, "
				+ " (SELECT MAX(pqtr.created) FROM PQTransaction pqtr where pqtr.account_id = pa.id) as lastTransactionTime, "
				+ " (SELECT MAX(pqtr.created) FROM PQTransaction pqtr where pqtr.account_id = pa.id and pqtr.status='"+Status.Reversed.getValue()+"') as lastReverseTransactionTime, "
				+ " (SELECT pqtr.currentBalance FROM PQTransaction pqtr where pqtr.account_id = pa.id and pqtr.status='Success' order by created desc limit 1) as lastSuccessTransactionBalance   "
				+ " from User u "
				+ " left join UserDetail us on u.userDetail_id=us.id  "
				+ " left join PQAccountDetail pa on u.accountDetail_id=pa.id "
				+ " left join PQAccountType pat on pa.accountType_id = pat.id "
				+ " where u.username='"+username+"' ";
		System.out.println("\n \n Transaction Validation Query---\n "+ query +"\n \n");
		List<Map<String, Object>> resultSet = jdbcTemplate.queryForList(query);
		UserDetailDTO user = new UserDetailDTO();
		if (resultSet.size() > 0) {
			for (Map map : resultSet) {
				user.setUsername(getStringFromObject(map.get("username")));
				user.setContactNo(getStringFromObject(map.get("contactNo")));
				user.setMobileStatus(getStringFromObject(map.get("mobileStatus")));
				user.setBalance(getDoubleFromObject(map.get("balance")));
				user.setBalanceLimit(getDoubleFromObject(map.get("balanceLimit")));
				user.setDailyLimit(getDoubleFromObject(map.get("dailyLimit")));
				user.setMonthlyLimit(getDoubleFromObject(map.get("monthlyLimit")));
				user.setCurrentMonthCredit(getDoubleFromObject(map.get("monthlyCreditAmount")));
				user.setCurrentMonthDebit(getDoubleFromObject(map.get("monthlyDebitAmount")));
				user.setDailyCrebitAmount(getDoubleFromObject(map.get("dailyCreditAmount")));
				user.setDailyDebitAmount(getDoubleFromObject(map.get("dailyDebitAmount")));
				user.setLastSuccessTransactionBalance(getDoubleFromObject(map.get("lastSuccessTransactionBalance")));
				user.setTotalMonthlyTransaction(getDoubleFromObject(map.get("totalMonthlyTransaction")));
				try {
					user.setLastTransactionTime(getDateFromObject(map.get("lastTransactionTime")));
					user.setLastReverseTransactionTime(getDateFromObject(map.get("lastReverseTransactionTime")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return user;
	}
	
	
	@Override
	public UserDetailDTO getUserDataForP2PValidation(String username,int currentDay, int currentMonth,int currentYear) {
		String query = "Select u.username,u.mobileStatus,us.contactNo,pa.balance,pat.balanceLimit,pat.dailyLimit,pat.monthlyLimit,"
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=false) as monthlyCreditAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=true) as monthlyDebitAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND DAY(u.created) = '"+currentDay +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=false) as dailyCreditAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND DAY(u.created) = '"+currentDay +"' AND pqtr.account_id = pa.id AND pqtr.status='Success' AND pqtr.debit=true) as dailyDebitAmount, "
				+ " (SELECT SUM(pqtr.amount) FROM PQTransaction pqtr where YEAR(pqtr.created) ='"+currentYear +"' AND MONTH(pqtr.created) = '"+currentMonth +"' AND pqtr.account_id = pa.id AND pqtr.status='Success') as totalMonthlyTransaction, "
				+ " (SELECT MAX(pqtr.created) FROM PQTransaction pqtr where pqtr.account_id = pa.id) as lastTransactionTime, "
				+ " (SELECT MAX(pqtr.created) FROM PQTransaction pqtr where pqtr.account_id = pa.id and pqtr.status='"+Status.Reversed.getValue()+"') as lastReverseTransactionTime, "
				+ " (SELECT pqtr.currentBalance FROM PQTransaction pqtr where pqtr.account_id = pa.id and pqtr.status='Success' order by created desc limit 1) as lastSuccessTransactionBalance   "
				+ " from User u "
				+ " left join UserDetail us on u.userDetail_id=us.id  "
				+ " left join PQAccountDetail pa on u.accountDetail_id=pa.id "
				+ " left join PQAccountType pat on pa.accountType_id = pat.id "
				+ " where u.username='"+username+"' ";
		System.out.println("\n \n Transaction Validation Query---\n "+ query +"\n \n");
		List<Map<String, Object>> resultSet = jdbcTemplate.queryForList(query);
		UserDetailDTO user = new UserDetailDTO();
		if (resultSet.size() > 0) {
			for (Map map : resultSet) {
				user.setUsername(getStringFromObject(map.get("username")));
				user.setContactNo(getStringFromObject(map.get("contactNo")));
				user.setMobileStatus(getStringFromObject(map.get("mobileStatus")));
				user.setBalance(getDoubleFromObject(map.get("balance")));
				user.setBalanceLimit(getDoubleFromObject(map.get("balanceLimit")));
				user.setDailyLimit(getDoubleFromObject(map.get("dailyLimit")));
				user.setMonthlyLimit(getDoubleFromObject(map.get("monthlyLimit")));
				user.setCurrentMonthCredit(getDoubleFromObject(map.get("monthlyCreditAmount")));
				user.setCurrentMonthDebit(getDoubleFromObject(map.get("monthlyDebitAmount")));
				user.setDailyCrebitAmount(getDoubleFromObject(map.get("dailyCreditAmount")));
				user.setDailyDebitAmount(getDoubleFromObject(map.get("dailyDebitAmount")));
				user.setLastSuccessTransactionBalance(getDoubleFromObject(map.get("lastSuccessTransactionBalance")));
				user.setTotalMonthlyTransaction(getDoubleFromObject(map.get("totalMonthlyTransaction")));
				try {
					user.setLastTransactionTime(getDateFromObject(map.get("lastTransactionTime")));
					user.setLastReverseTransactionTime(getDateFromObject(map.get("lastReverseTransactionTime")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return user;
	}
	
	
	private String getStringFromObject(Object obj){
		if(obj !=null){
			return obj.toString();
		}
		return null;
	}
	
	private double getDoubleFromObject(Object obj){
		double d = 0.0;
		if(obj !=null){
			String s = getStringFromObject(obj);
			if(s!=null && s!=""){
				d = Double.parseDouble(s);
			}
			return d;
		}
		return d;
	}
	
	private Date getDateFromObject(Object obj) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d = null;
		if(obj !=null){
			String s = getStringFromObject(obj);
			System.out.println("s--\n"+s);
			if(s!=null && s!=""){
				d = sdf.parse(s);
			}
			return d;
		}
		return d;
	}

	
	
	
}
