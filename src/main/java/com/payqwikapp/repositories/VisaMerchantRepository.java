package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.payqwikapp.entity.VisaMerchant;

public interface VisaMerchantRepository extends CrudRepository<VisaMerchant, Long>,
		PagingAndSortingRepository<VisaMerchant, Long>, JpaSpecificationExecutor<VisaMerchant> {

	@Query("select u from VisaMerchant u where u.emailAddress=?1")
	VisaMerchant findByEmailAddress(String emailAddress);

	@Query("select u from VisaMerchant u where u.entityId=?1 and u.mVisaId=?2")
	VisaMerchant findByMvisaIdEntityId(String entityId, String mvisaId);

}
