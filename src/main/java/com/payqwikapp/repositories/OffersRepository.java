package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.Offers;

public interface OffersRepository extends CrudRepository<Offers, Long>, JpaSpecificationExecutor<Offers> {
	
	@Query("select u from Offers u where u.offers=?1")
	Offers findOffersByName(String name);
	
	@Query("select u from Offers u order by u.created DESC")
	List<Offers> findAllOffers();

}
