package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.ABankTransfer;
import com.payqwikapp.entity.MBankTransfer;

public interface ABankTransferRepository extends CrudRepository<ABankTransfer, Long>,
PagingAndSortingRepository<ABankTransfer, Long>, JpaSpecificationExecutor<ABankTransfer> {
	
	@Query("select s from ABankTransfer s order by s.created desc")
	List<ABankTransfer> findAllNeftTransaction();

	@Query("select s from ABankTransfer s where s.created=?1")
	List<ABankTransfer> findAllNeftTransactionByDate(String date);

	@Query("select b from ABankTransfer b where b.created between ?1 and ?2 order by b.created desc")
	List<ABankTransfer> getListByDate(Date startDate, Date endDate);
	
}
