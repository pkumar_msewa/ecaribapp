package com.payqwikapp.repositories;

import com.payqwikapp.entity.POSMerchants;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface POSMerchantsRepository extends CrudRepository<POSMerchants,Long>,JpaSpecificationExecutor<POSMerchants>,PagingAndSortingRepository<POSMerchants,Long> {

    @Query("select p from POSMerchants p where p.accountNumber=?1")
    POSMerchants findByAccountNumber(String accountNumber);

    @Query("select p from POSMerchants p where date(p.created)=current_date")
    List<POSMerchants> findTodaysPOSMerchants();

    @Query("select p from POSMerchants p where date(p.created)=date(?1)")
    List<POSMerchants> findPOSMerchantsByDate(Date date);


    @Query("select p from POSMerchants p where p.mobileNumber=?1")
    POSMerchants findByMobileNumber(String mobileNumber);

    @Query("select p from POSMerchants p where p.mobileNumber=?1 OR p.accountNumber=?2")
    POSMerchants findByMobileAndAccount(String mobileNumber,String accountNumber);

    @Query("select p from POSMerchants p where p.customerId IS NOT NULL")
    List<POSMerchants> getAllPOSHavingCustomerID();

    @Query("select p from POSMerchants p where p.mcc IS NOT NULL")
    List<POSMerchants> getAllPOSHavingMCC();
    
    
    @Query("select p from POSMerchants p where p.rupayId=?1")
    POSMerchants findByMerchantId(String rupayId);


}
