package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.BulkFile;
import com.payqwikapp.model.Status;

public interface BulkFileRepository extends CrudRepository<BulkFile, Long>, PagingAndSortingRepository<BulkFile, Long>,
		JpaSpecificationExecutor<BulkFile> {
	
	@Query("select b from BulkFile b where b.mobileNumber=?1 and b.status='Open'")
	BulkFile findByMobileNumber(String name);
	
	@Query("select b from BulkFile b where b.mobileNumber=?1 and status=?2")
	BulkFile findByMobileNumberWithSatus(String name,Status status);
	
	
	@Query("select b from BulkFile b where b.status=?1")
	List<BulkFile> findAllFiles(Status status);

}
