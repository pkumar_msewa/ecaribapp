package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.ApiVersion;

public interface ApiVersionRepository extends CrudRepository<ApiVersion,Long>,JpaSpecificationExecutor<ApiVersion>,PagingAndSortingRepository<ApiVersion,Long>{

	@Query("select a from apiversion a where a.name=?1 AND a.apiStatus=?2")
	ApiVersion getByNameStatus(String name,String apiStatus);
}
