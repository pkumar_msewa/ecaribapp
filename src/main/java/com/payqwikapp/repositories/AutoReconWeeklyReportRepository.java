package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.AutoReconWeeklyReport;

public interface AutoReconWeeklyReportRepository extends CrudRepository<AutoReconWeeklyReport, Long>,
		PagingAndSortingRepository<AutoReconWeeklyReport, Long>, JpaSpecificationExecutor<AutoReconWeeklyReport> {

	@Query("SELECT u FROM AutoReconWeeklyReport u where DATE(u.fromDate)=?1")
	AutoReconWeeklyReport getDataByDate(Date date);
	
	@Query("select u from AutoReconWeeklyReport u order by u.created desc")
	List<AutoReconWeeklyReport> findAllReconList();
}
