package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.Voucher;

public interface VoucherRepository extends CrudRepository<Voucher, Long> {

	@Query("select v from Voucher v where v.voucherNo=?1")
	Voucher getVoucherByNumber(String no);
	
	@Modifying
	@Transactional
	@Query("update Voucher v set v.expired=true,v.redeemed=true,v.pqTransaction=?1 where v.voucherNo=?2")
	Voucher updateVoucher(PQTransaction pqTransaction, String no);
	
	@Query("select v from Voucher v where v.voucherNo=?1")
	Voucher checkVoucherIsExpired(String no);
	
	@Query("select v from Voucher v")
	Page<Voucher> findAll(Pageable pageable);
	
	@Query("select t from Voucher t where t.created BETWEEN ?1 AND ?2 ")
	List<Voucher> findVouchersByDate(Date from,Date to);
	
	@Query("select t from Voucher t")
	List<Voucher> findAllVouchers();
}
