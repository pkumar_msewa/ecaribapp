package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.BusCityList;


public interface BusCityListRepository extends CrudRepository<BusCityList,Long>,JpaSpecificationExecutor<BusCityList>{
	
	
	@Query("select b from BusCityList b")
	List<BusCityList> getAllCityList();
}
