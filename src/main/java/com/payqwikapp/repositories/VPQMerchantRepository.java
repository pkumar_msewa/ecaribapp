package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.User;
import com.payqwikapp.entity.VPQMerchant;

public interface VPQMerchantRepository extends CrudRepository<VPQMerchant, Long>, PagingAndSortingRepository<VPQMerchant,Long>,JpaSpecificationExecutor<VPQMerchant>  
{

	@Query("select q from VPQMerchant q where q.qrCode=?1")
	VPQMerchant getQrcode(String qrCode);
	
	@Query("select q from VPQMerchant q where q.user=?")
	VPQMerchant getByUserId(User user);
	
	
}
