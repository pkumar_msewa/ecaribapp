package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.SenderBankTransferInfo;

public interface SenderBankTransferInfoRepository extends CrudRepository<SenderBankTransferInfo,Long>,
PagingAndSortingRepository<SenderBankTransferInfo, Long>,JpaSpecificationExecutor<SenderBankTransferInfo>{

}
