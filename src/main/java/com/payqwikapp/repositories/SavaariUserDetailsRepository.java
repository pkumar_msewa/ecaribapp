package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.SavaariUserDetails;

public interface SavaariUserDetailsRepository extends CrudRepository<SavaariUserDetails, Long>,
PagingAndSortingRepository<SavaariUserDetails, Long>, JpaSpecificationExecutor<SavaariUserDetails> {

}
