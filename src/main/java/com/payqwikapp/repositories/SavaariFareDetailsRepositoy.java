package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.SavaariFareDetails;

public interface SavaariFareDetailsRepositoy extends CrudRepository<SavaariFareDetails, Long>,
PagingAndSortingRepository<SavaariFareDetails, Long>, JpaSpecificationExecutor<SavaariFareDetails> {

}
