package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.FRMDetails;

public interface FRMDetailsRepository extends CrudRepository<FRMDetails, Long>,JpaSpecificationExecutor<FRMDetails>{
	
}
