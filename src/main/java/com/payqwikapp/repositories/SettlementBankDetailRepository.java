package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.SettlementBankDetail;

public interface SettlementBankDetailRepository extends CrudRepository<SettlementBankDetail, Long>, PagingAndSortingRepository<SettlementBankDetail,Long>,JpaSpecificationExecutor<SettlementBankDetail> {

}
