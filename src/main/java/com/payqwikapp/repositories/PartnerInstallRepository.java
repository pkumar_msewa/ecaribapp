package com.payqwikapp.repositories;

import com.payqwikapp.entity.PartnerDetail;
import com.payqwikapp.entity.PartnerInstall;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface PartnerInstallRepository extends CrudRepository<PartnerInstall,Long>,JpaSpecificationExecutor<PartnerInstall>{

    @Query("select pi from PartnerInstall pi where pi.partnerDetail=?1 ORDER BY pi.created DESC")
    List<PartnerInstall> findByPartner(PartnerDetail partnerDetail);

    @Query("select pi from PartnerInstall pi where pi.imei=?1")
    PartnerInstall findByIMEI(String imei);
    
    
    @Query("select pi from PartnerInstall pi where pi.partnerDetail=?1 and pi.created BETWEEN ?2 AND ?3 ORDER BY pi.created DESC")
    List<PartnerInstall> findByPartnerFilter(PartnerDetail partnerDetail,Date from,Date to);

    
}
	