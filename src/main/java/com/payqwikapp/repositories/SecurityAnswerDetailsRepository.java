package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.SecurityAnswerDetails;

public interface SecurityAnswerDetailsRepository extends CrudRepository<SecurityAnswerDetails, Long>, JpaSpecificationExecutor<SecurityAnswerDetails>  {

}
