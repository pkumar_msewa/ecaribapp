package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.MerchantDetails;
import com.payqwikapp.entity.User;

public interface MerchantDetailsRepository extends CrudRepository<MerchantDetails, Long>, JpaSpecificationExecutor<MerchantDetails> {

	@Query("select m from MerchantDetails m where m.panCardNo=?1")
	MerchantDetails findByPanCardNo(String panNo);
	
	@Query("select m from MerchantDetails m where m.aadharNo=?1")
	MerchantDetails findByAadharNo(String aadharNo);
	
	@Query("select m from MerchantDetails m where m.panCardNo=?1 AND m.aadharNo=?2")
	MerchantDetails findByPanAndAadharNo(String panNo, String aadharNo);
	
	@Query("select m from MerchantDetails m where user=?1")
	MerchantDetails findByuserId(User user);
	


}
