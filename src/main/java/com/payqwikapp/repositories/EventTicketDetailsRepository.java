package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.EventTicketDetails;

public interface EventTicketDetailsRepository extends CrudRepository<EventTicketDetails, Long>,
				 PagingAndSortingRepository<EventTicketDetails, Long>, JpaSpecificationExecutor<EventTicketDetails> {

}
