package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.VijayaBankReport;

public interface VijayaBankReportRepository extends CrudRepository<VijayaBankReport, Long>,
		PagingAndSortingRepository<VijayaBankReport, Long>, JpaSpecificationExecutor<VijayaBankReport> {
	
	@Query("select b from VijayaBankReport b where b.created=?1")
	VijayaBankReport findByDate(Date date);
	
	 @Query("select u from VijayaBankReport u")
	List<VijayaBankReport> getMerchantReport();
	 
	 @Query("select u from VijayaBankReport u where DATE(u.created) BETWEEN ?1 AND ?2 and u.modeOfPayment=?3 and u.consumerType=?4")
		List<VijayaBankReport> getFilteredMerchantReport(Date startDate,Date endDate,String consumerType,String paymentMode);

}
