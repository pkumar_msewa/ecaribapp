package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.GciProduct;

public interface GciProductRepository

extends CrudRepository<GciProduct, String>, PagingAndSortingRepository<GciProduct, String>, JpaSpecificationExecutor<GciProduct> {
	


//  @Query("select * from GciProduct u where gcicart_id = ?1")
//    List<GciProduct> findByTransactionRefNo(String transactionRefNo);
}

