package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PQNotifications;
import com.payqwikapp.entity.User;

public interface PQNotificationRepository  extends CrudRepository<PQNotifications, Long>,
PagingAndSortingRepository<PQNotifications, Long>, JpaSpecificationExecutor<PQNotifications>{

	@Query("select q from PQNotifications q where q.user=?1 ORDER BY q.created desc")
	List<PQNotifications> getAllNotifications(User user);
	
	@Query("select q.message from PQNotifications q where q.user=?1 ORDER BY q.created desc")
	List<String> getAllMessages(User user);
	
}
