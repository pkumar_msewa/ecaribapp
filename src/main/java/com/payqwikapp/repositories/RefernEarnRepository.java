package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;

public interface RefernEarnRepository extends CrudRepository<RefernEarnlogs,Long>,JpaSpecificationExecutor<RefernEarnlogs>{

	@Query("select l from RefernEarnlogs l where l.user=?1")
	RefernEarnlogs getLogsByUser(User user);
	
	@Query("select l from RefernEarnlogs l")
	List<RefernEarnlogs> getLogs();
	
	
}
