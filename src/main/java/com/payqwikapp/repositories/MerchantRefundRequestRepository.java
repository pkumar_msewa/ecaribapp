package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.MerchantRefundRequest;

public interface MerchantRefundRequestRepository extends CrudRepository<MerchantRefundRequest, Long>, JpaSpecificationExecutor<MerchantRefundRequest>{
	
	@Query("select m from MerchantRefundRequest m ORDER BY m.created DESC")
	List<MerchantRefundRequest> findAllRefundRequest();
	
	@Query("select m from MerchantRefundRequest m where m.trnasactionRefNo=?1")
	MerchantRefundRequest findTrnasactionRefNo(String trnasactionRefNo);

}
