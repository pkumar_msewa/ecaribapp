package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.UserMaxLimit;

public interface UserMaxLimitRepository
		extends JpaSpecificationExecutor<UserMaxLimit>, CrudRepository<UserMaxLimit, Long> {

}
