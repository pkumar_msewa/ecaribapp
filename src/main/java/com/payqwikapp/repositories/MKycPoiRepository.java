package com.payqwikapp.repositories;

import com.payqwikapp.entity.MKycPoi;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MKycPoiRepository extends CrudRepository<MKycPoi,Long>, PagingAndSortingRepository<MKycPoi,Long>,JpaSpecificationExecutor<MKycPoi>{

}
