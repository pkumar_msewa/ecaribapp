package com.payqwikapp.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.BescomRefundRequest;
import com.payqwikapp.model.Status;

public interface BescomRefundRequestRepository extends CrudRepository<BescomRefundRequest, Long>,
		PagingAndSortingRepository<BescomRefundRequest, Long>, JpaSpecificationExecutor<BescomRefundRequest> {

	@Query("select u from BescomRefundRequest u where u.trnasactionRefNo=?1")
	BescomRefundRequest findByTransactionRefNo(String transactionRefNo);

	@Query("select u from BescomRefundRequest u where u.status='Processing'")
	Page<BescomRefundRequest> findAll(Pageable page);
	
	@Query("select u from BescomRefundRequest u where u.trnasactionRefNo=?1 and u.status=?2")
	BescomRefundRequest findByTransactionRefNoAndStatus(String transactionRefNo, Status status);
}
