package com.payqwikapp.repositories;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.RequestLog;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RequestLogRepository extends CrudRepository<RequestLog,Long>,JpaSpecificationExecutor<RequestLog>{

    @Query("select u from RequestLog u where u.user=?1")
    List<RequestLog> getByUser(User user);

    @Query("select u from RequestLog u where u.service=?1 and u.user=?2")
    List<RequestLog> getByUserAndService(PQService service, User user);

    @Query("select u from RequestLog u where u.user=?1 and u.created=(select MAX(m.created) from RequestLog m where m.user=?1)")
    RequestLog getLastRequestOfUser(User user);
    
    @Query("select u from RequestLog u where u.user=?1")
    List<RequestLog> getLastRequestOfUserList(User user);

    @Query("select u from RequestLog u where u.user=?1 and u.timeStamp=?2")
    RequestLog findByUserRequestTimeStamp(User user,long timeStamp);
    
    @Query("select u from RequestLog u where u.status=?1")
    List<RequestLog> findProcessingTransactions(Status status);

}
