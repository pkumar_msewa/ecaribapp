package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.UpiSdkMerchantDetails;


public interface UpiSdkMerchantDetailsRepository  extends CrudRepository<UpiSdkMerchantDetails,Long>,JpaSpecificationExecutor<UpiSdkMerchantDetails>{

	@Query("select u from UpiSdkMerchantDetails u where u.merchantVpa=?1")
	UpiSdkMerchantDetails getUpiMerchant(String merchantVpa);
}
