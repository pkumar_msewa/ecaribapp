package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionType;

public interface PQTransactionRepository extends CrudRepository<PQTransaction, Long>,
		PagingAndSortingRepository<PQTransaction, Long>, JpaSpecificationExecutor<PQTransaction>, PQTransactionRepositoryCustom {
	
	@Query("select u from PQTransaction u where u.transactionType=?1 and u.account NOT IN (?2) ORDER BY u.created DESC")
	Page<PQTransaction> findTodaysTransactions(Pageable page,TransactionType transactionType,List<PQAccountDetail> accounts);


	@Query("select u from PQTransaction u where u.service in (?1) and u.created BETWEEN ?2 AND ?3  and u.transactionType=?4 and u.status=?5 ORDER BY u.created DESC")
	List<PQTransaction> getTransactionBetween(List<PQService> services,Date startDate,Date endDate,TransactionType transactionType,Status status);
	
	@Query("select u from PQTransaction u where u.service in (?1) and u.created BETWEEN ?2 AND ?3  and u.transactionType=?4 and u.status=?5 and u.debit=true ORDER BY u.created DESC")
	List<PQTransaction> getTransactionBetweenDebit(List<PQService> services,Date startDate,Date endDate,TransactionType transactionType,Status status);
	
	@Query("select u from PQTransaction u where  u.created BETWEEN ?1 AND ?2  and u.transactionType=?3 and u.transactionRefNo=?4 and status='Success' ORDER BY u.created DESC")
	List<PQTransaction> getTransactionBetweenDateAndRefNo(Date startDate,Date endDate,TransactionType transactionType,String transactionRefNo);

	@Query("select u from PQTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2 ORDER BY u.created DESC")
	Page<PQTransaction> findMerchantTransactions(Pageable page,PQService service,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2 and u.debit=1")
	Page<PQTransaction> findlast20MerchantTransactions(Pageable page,PQService service,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where YEAR(u.created)=YEAR(now()) and u.account=?1 and u.transactionType = ?2 and debit=1")
	Page<PQTransaction> findDebitMerchantTransactions(Pageable page,PQAccountDetail account,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where  u.service=?1 and u.transactionType = ?2 and debit=false")
	Page<PQTransaction> findSuccessMerchantTransactions(Pageable page,PQService service,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where u.service=?1 and u.transactionType = ?2 and debit=false")
	Page<PQTransaction> findBescomMerchantTransactions(Pageable page,PQService service,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where  u.service=?1 and u.transactionType = ?2 and debit=true")
	Page<PQTransaction> findSuccessMerchantTransactionsForTreatCard(Pageable page,PQService service,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2 and u.debit=1 and  u.created BETWEEN ?3 AND ?4")
	Page<PQTransaction> findMerchantTransactionsFiltered(Pageable page,PQService service,TransactionType transactionType,Date startDate,Date endDate);
    
	@Query("select sum(u.amount) from PQTransaction u where u.transactionType=?1 and u.account = ?2 and u.status='Success' and u.debit=?3")
	double  calculateTotalCrDb(TransactionType transactionType,PQAccountDetail account,boolean debit);

	@Query("select u from PQTransaction u where u.transactionType=?1  and u.created BETWEEN ?3 AND ?4 and u.account NOT IN  (?2) ORDER BY u.created DESC")
	List<PQTransaction> getTransactionBetween(TransactionType transactionType,List<PQAccountDetail> accounts,Date startDate,Date endDate);

	@Query("select u from PQTransaction u where u.created BETWEEN ?1 AND ?2 and u.transactionType=?3 and debit=true ORDER BY u.created DESC")
	List<PQTransaction> getDailyTrasnactionBetweenDate(Date startDate,Date endDate,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where u.created BETWEEN ?1 AND ?2 and u.transactionType=?3 and debit=false and u.service IN(2,112,631) ORDER BY u.created DESC")
	List<PQTransaction> getDailyTrasnactionBetweenDateForCredit(Date startDate,Date endDate,TransactionType transactionType);
	
	@Query("select u from PQTransaction u where u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4)")
    List<PQTransaction> getTransactionBetweenForServices(TransactionType transactionType,Date startDate,Date endDate,List<PQService> service);

    @Query("select u from PQTransaction u where u.debit=true AND u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4)")
    List<PQTransaction> getTransactionBetweenTopup(TransactionType transactionType,Date startDate,Date endDate,List<PQService> service);
    
    @Query("select u from PQTransaction u where u.debit=true AND u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4) AND u.status=?5")
    List<PQTransaction> getTransactionBetweenTopupByStatus(TransactionType transactionType,Date startDate,Date endDate,List<PQService> service, Status status);
    
    @Query("select u from PQTransaction u where u.debit=false AND u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4)")
    List<PQTransaction> getAllLoadMoney(TransactionType transactionType,Date startDate,Date endDate,PQService service);
    
    @Query("select u from PQTransaction u where u.debit=false AND u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.service IN (?4) AND u.status=?5")
    List<PQTransaction> getLoadMoneyByStatus(TransactionType transactionType,Date startDate,Date endDate,PQService service, Status status);

	@Query("select u from PQTransaction u where u.transactionRefNo=?1")
	PQTransaction findByTransactionRefNo(String transactionRefNo);
	
	@Query("select u from PQTransaction u where u.retrivalReferenceNo=?1")
	PQTransaction findByTransactionRetivalReferenceNo(String retrivalReferenceNo);
	
    @Query("select u from TPTransaction u where u.transactionRefNo=?1")
	TPTransaction findTransactionRefNo(String transactionRefNo);
	
	@Query("select u from PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.status=?3 and u.service=?4")
	List<PQTransaction> findByTranRefNoAndStatus(Date date,Date to,Status Status,PQService service);

	@Query("select u from PQTransaction u where u.transactionRefNo=?1 and u.status=?2")
	PQTransaction findByTransactionRefNoAndStatus(String transactionRefNo, Status status);

	@Query("select COUNT(u) from PQTransaction u where u.status=?1 AND u.transactionType IN (0,4)")
	Long countTotalTransactionsByStatus(Status status);
	
	@Query("select SUM(u.amount) from PQTransaction u where u.status=?1 AND u.transactionType = ?2 and u.account=?3 and debit=false")
	double countTotalCommission(Status status,TransactionType transactionType,PQAccountDetail account);

	@Query("select COUNT(u) from PQTransaction u where DATE(u.created) = ?1 AND status = ?2 AND u.transactionType = ?3")
	long getDailyTransactionCount(Date date,Status status,TransactionType transactionType);

	@Query("select SUM(u) from PQTransaction u where DATE(u.created) = ?1 AND status = ?2 AND u.transactionType = ?3")
	double getDailyTransactionAmount(Date date,Status status,TransactionType transactionType);

	@Modifying
	@Transactional
	@Query("update PQTransaction c set c.status=?1, c.currentBalance=?2 where c.transactionRefNo =?3")
	int updateTransaction(Status status, double currentBalance, String transactionRefNo);

	@Modifying
	@Transactional
	@Query("update PQTransaction c set c.favourite=?2 where c.transactionRefNo =?1")
	int updateFavouriteTransaction(String transactionRefNo,boolean favourite);

	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 ")
	List<PQTransaction> getMonthlyTransaction(int year, int month, PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u where u.service IN ( ?1 , ?2 ) AND u.status = ?3")
	List<PQTransaction> getLoadMoneyTransactions(PQService ebs,PQService vnet,Status status);

	@Query("SELECT u FROM PQTransaction u where u.transactionType = ?1")
	List<PQTransaction> getTotalDefaultTransactions(TransactionType transactionType);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.debit=?1 AND u.status=?2 ")
	double getValidAmount(boolean debit,Status status);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND u.transactionType IN(0,11)")
	Double getValidAmountByService(PQService service);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service IN(select p.id from PQService p where p.serviceType=?1) AND u.status='Success' AND u.debit=true AND u.transactionType=0 ")
	double getValidAmountByTopUpService(PQServiceType serviceType);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND u.transactionType=0")
	double getValidAmountByBankService(PQService service);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND transactionType=2 ")
	double getValidAmountByBankServiceCommission(PQService service);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	double getValidAmountByServiceForDate(PQService service,Date date);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	double getValidTopupAmountForDate(PQAccountDetail account,Date date);

	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4")
	List<PQTransaction> getDailyTransaction(int year, int month, int day, PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u where u.service=?1")
	List<PQTransaction> findTransactionByService(PQService service);

	@Query("SELECT u FROM PQTransaction u where u.created BETWEEN ?1 AND ?2  AND u.debit=?3 AND u.transactionType=?4 AND u.status=?5 ORDER BY u.created DESC")
	List<PQTransaction> findTransactionByServiceAndDate(Date from, Date to, boolean debit, TransactionType transactionType, Status status);
	
                //	for nikki
	
	@Query("SELECT u FROM PQTransaction u where u.debit=?1 AND u.created BETWEEN ?2 AND ?3  AND u.service=?4 AND u.status=?5 and u.transactionType=0 ORDER BY u.created DESC")
	List<PQTransaction> findTransactionByDate(boolean debit, Date from, Date to,PQService service,Status status);
	
	
//	for debit
	
	
	@Query("SELECT u FROM PQTransaction u where u.debit=?1 AND u.created BETWEEN ?2 AND ?3    AND u.status=?4 ORDER BY u.created DESC")
	List<PQTransaction> findDebitTransactionByDate(boolean debit, Date from, Date to, Status status);
	
//	for credit
	
	
	@Query("SELECT u FROM PQTransaction u where u.debit=?1 AND u.created BETWEEN ?2 AND ?3   AND u.status=?4 ORDER BY u.created DESC")
	List<PQTransaction> findCreditTransactionByDate(boolean debit, Date from, Date to,  Status status);
	
	@Query("SELECT u FROM PQTransaction u where u.created BETWEEN ?1 AND ?2  AND u.debit=?3 AND u.transactionType=?4 AND u.status=?5 and u.account=71")
	List<PQTransaction> findCommissionTransactionByServiceAndDate(Date from, Date to, boolean debit, TransactionType transactionType, Status status);

	@Query("SELECT u FROM PQTransaction u where u.service=?1 AND u.debit=?2")
	List<PQTransaction> findTransactionByServiceAndDebit(PQService service,boolean debit);
	
	@Query("SELECT u FROM PQTransaction u where u.service=?1 AND u.debit=?2 ORDER BY u.created DESC")
	Page<PQTransaction> findPagewiseTransactionByServiceAndDebit(Pageable page,PQService service,boolean debit);
	
	@Query("SELECT u FROM PQTransaction u where u.service=?1 AND u.debit=?2 and DATE(u.created) BETWEEN ?3 AND ?4 ORDER BY u.created DESC")
	List<PQTransaction> findPagewiseTransactionByServiceAndDebitFiltered(PQService service,boolean debit,Date from,Date to);

	@Query("SELECT u FROM PQTransaction u where u.service=?1 AND u.status=?2")
	List<PQTransaction> findTransactionByServiceAndStatus(PQService service, Status status);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status = 'Success'")
	double getMonthlyTransactionTotal(int year, int month, PQAccountDetail account);
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.service=5 AND u.transactionType=0 AND u.debit=true AND u.status='Success' ")
	long getMonthlyTransactionTotalCountBank(int year, int month,PQAccountDetail account);
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2  AND DAY(u.created) = ?3 AND u.account = ?4 AND u.service=5 AND u.transactionType=0 AND u.debit=true AND u.status='Success' ")
	long getDailyTransactionTotalCountBank(int year, int month, int day,PQAccountDetail account);
	
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3  AND u.transactionType=0 AND u.debit=true AND u.status='Success' ")
	long getMonthlyTransactionCount(int year, int month,PQAccountDetail account);
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2  AND DAY(u.created) = ?3 AND u.account = ?4 AND u.transactionType=0 AND u.debit=true AND u.status='Success' ")
	long getDailyTransactionCount(int year, int month, int day,PQAccountDetail account);
	
	
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.debit = ?4 AND u.status = 'Success'")
	double getMonthlyTotalByCreditOrDebit(int year, int month, PQAccountDetail account,boolean debit);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.service = ?4 AND u.status = 'Success'")
	double getMonthlyTransactionTotalByServiceType(int year, int month, PQAccountDetail account, PQService service);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status = 'Success'")
	double getDailyTransactionTotal(int year, int month, int day, PQAccountDetail account);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit = ?4 AND u.status = 'Success'")
	double getDailyTotalByCreditOrDebit(int year, int month, int day, PQAccountDetail account,boolean debit);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit = ?5")
	double getDailyDebitTransactionTotal(int year, int month, int day, PQAccountDetail account, boolean debit);

	@Query("SELECT u FROM PQTransaction u where  MONTH(created) = ?1")
	List<PQTransaction> getMonthlyTransaction(int month);

	@Query("SELECT u FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2")
	List<PQTransaction> getDailyTransactionBetween(Date from, Date to);

	@Query("SELECT u FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2")
	List<PQTransaction> getTransactionBetween(Date from, Date to);


	@Query("SELECT u FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3")
	List<PQTransaction> getDailyTransactionBetweenForAccount(Date from, Date to, PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u where DATE(u.created) = ?1")
	List<PQTransaction> getDailyTransactionByDate(Date from);

	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) = ?1")
	Long countDailyTransactionByDate(Date from);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where DATE(u.created) = ?1")
	Double countDailyTransactionAmountByDate(Date from);

	@Query("SELECT MAX(t.created) FROM PQTransaction t where t.account=?1")
	Date getTimeStampOfLastTransaction(PQAccountDetail account);

	@Query("SELECT MAX(t.created) FROM PQTransaction t where t.account=?1 AND t.status=?2")
	Date getTimeStampOfLastTransactionByStatus(PQAccountDetail account,Status status);


	@Query("SELECT u FROM PQTransaction u where DATE(u.created) = ?1 AND u.account=?2")
	List<PQTransaction> getDailyTransactionByDateForAccount(Date from, PQAccountDetail account);

	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) = ?1 AND u.account=?2")
	Long countDailyTransactionByDateForAccount(Date from, PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u where u.account= ?1 order by u.created")
	List<PQTransaction> getTotalTransactions(PQAccountDetail account);
	
	@Query("SELECT u FROM PQTransaction u where u.account= ?1 AND u.status='Reversed' ORDER BY u.id DESC ")
	List<PQTransaction> getReverseTransactionsOfUser(PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u where u.account= ?1 AND u.status='Reversed' AND u.created=(select MAX(f.created) from PQTransaction f where f.account=?1)")
	PQTransaction getLastReverseTransactionsOfUser(PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u")
	List<PQTransaction> getTotalTransactions();

	@Query("select u from PQTransaction u")
	Page<PQTransaction> findAll(Pageable page);
	
	
	@Query("select u from PQTransaction u where  DATE(u.created) BETWEEN ?1 AND ?2")
	Page<PQTransaction> findAllByFiltered(Pageable page,Date from,Date to);

	@Query("select u from PQTransaction u where u.account = ?1")
	Page<PQTransaction> findAllByAccount(Pageable page,PQAccountDetail account);

	@Query("select u from PQTransaction u where u.account=?1 order by u.created DESC")
	Page<PQTransaction> findAllTransactionByUser(Pageable page, PQAccountDetail account);
	
	
	@Query("select u from PQTransaction u where  DATE(u.created) BETWEEN ?1 AND ?2 and u.account=?3")
	Page<PQTransaction> findAllTransactionByUserFilter(Pageable page,Date StartDate,Date endDate, PQAccountDetail account);
	
	@Query("select u from PQTransaction u where  DATE(u.created) BETWEEN ?1 AND ?2 and u.account=?3")
	Page<PQTransaction> findAllTransactionByUserFilterPagewise(Pageable page,Date StartDate,Date endDate,PQAccountDetail account);
	
	@Query("select u from PQTransaction u where u.account=?1")
	List<PQTransaction> findAllTransactionByMerchant(PQAccountDetail account);
	
	@Query("select u from PQTransaction u where u.service=?1 and debit=true and u.transactionType=0")
	Page<PQTransaction> findAllTransactionByMerchantService(PQService service ,Pageable page);


	@Query("select u from PQTransaction u where u.account=?1 AND u.status=?2 AND u.transactionRefNo LIKE '%D'")
	List<PQTransaction> findAllTransactionByUserAndStatus(PQAccountDetail account,Status status);
	
	@Query("select u from PQTransaction u where u.account=?1 AND u.status=?2  and u.transactionType=0")
	List<PQTransaction> findAllTransactionByUserAndStatusAndType(PQAccountDetail account,Status status);

	@Transactional
	@Query("SELECT u FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3 AND u.amount=?4")
	List<PQTransaction> findTransactionByAccount(Date from, Date to, PQAccountDetail account, double amount);

	@Transactional
	@Query("SELECT u FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3 AND u.amount>=?4")
	List<PQTransaction> findTransactionByAccountAndAmount(Date from, Date to, PQAccountDetail account, double amount);

	@Query("SELECT u FROM PQTransaction u where u.account=?1")
	List<PQTransaction> findTransactionByAccount(PQAccountDetail account);

//	@Transactional
//	@Query("SELECT t FROM PQTransaction t LEFT JOIN t.service s WHERE s.code=?1 AND t.account=?2")
//	List<PQTransaction> findTransactionDateByAccountAndService(String serviceType, PQAccountDetail account);
	
	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit = ?5")
	List<PQTransaction> getDailyDebitTransaction(int year, int month, int day, PQAccountDetail account, boolean debit);
	
	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true")
	List<PQTransaction> getMonthlyDebitTransaction(int year, int month, PQAccountDetail account);

	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=true OR u.debit=false")
	List<PQTransaction> getDailyCreditAndDebitTransation(int year, int month, int day, PQAccountDetail account);
	
	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true OR u.debit=false")
	List<PQTransaction> getMonthlyCreditAndDebitTransation(int year, int month, PQAccountDetail account);
	
	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=false")
	List<PQTransaction> getDailyCreditTransation(int year, int month, int day, PQAccountDetail account);
	
	@Query("SELECT u FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=false")
	List<PQTransaction> getMonthlyCreditTransation(int year, int month, PQAccountDetail account);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit=true")
	double getDailyDebitTransactionTotalAmount(int year, int month, int day, PQAccountDetail account);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.debit=true AND u.transactionType=0 AND u.service=5 AND u.status='Success'")
	double getDailyDebitTransactionTotalAmountBank(int year, int month, int day, PQAccountDetail account);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true")
	double getMonthlyDebitTransactionTotalAmount(int year, int month, PQAccountDetail account);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true AND u.transactionType=0 AND u.service=5")
	double getMonthlyDebitTransactionTotalAmountBank(int year, int month, PQAccountDetail account);
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=true OR u.debit=false")
	double getDailyCreditAndDebitTransationTotalAmount(int year, int month, int day, PQAccountDetail account);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=true OR u.debit=false")
	double getMonthlyCreditAndDebitTransationTotalAmount(int year, int month, PQAccountDetail account);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND DAY(u.created) = ?3 AND u.account = ?4 AND u.status='Success' AND u.debit=false")
	double getDailyCreditTransationTotalAmount(int year, int month, int day, PQAccountDetail account);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2 AND u.account = ?3 AND u.status='Success' AND u.debit=false")
	double getMonthlyCreditTransationTotalAmount(int year, int month, PQAccountDetail account);
	
	@Query("SELECT u FROM PQTransaction u where u.account= ?1 AND u.status in('Success','Booked') order by u.lastModified")
	List<PQTransaction> getTotalSuccessTransactions(PQAccountDetail account);
//
	@Query("select t from PQTransaction t where t.service=?1 AND (t.transactionRefNo LIKE '%C' OR t.transactionRefNo LIKE '%D')")
	List<PQTransaction> findAllTransactionForMerchant(PQService service);

	@Query("select t from PQTransaction t where t.created BETWEEN ?1 AND ?2 AND transactionType=0 and account_id!=3 and account_id!=72)")
	List<PQTransaction> findTransactionsByDate(Date from,Date to);
	
	@Query("select t from PQTransaction t where t.service=162 AND t.transactionType=0 AND t.account!=125119 ORDER BY t.created desc")
	List<PQTransaction> findVisaMerchantTransactions();
	
	@Query("select t from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.service=162 AND t.transactionType=0 AND t.account!=125119 ORDER BY t.created desc")
	List<PQTransaction> findVisaMerchantTransactionsFilter(Date from,Date to);
	
	// Agent
	
	@Query("select u from PQService u where u.code=?1")
	PQService findBypqservicetype(String pqservicecode);
	
	
	@Query("select t from PQTransaction t where t.service=?1 AND status='Initiated'")
	List<PQTransaction> getAgentRequestLoadMoney(PQService service);
	
	@Query("select t from PQTransaction t where t.service=135 AND t.transactionType=0 AND t.transactionRefNo=?1")
	PQTransaction getStatusByTransactionRefNo(String transactionRefNo);
	
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.account = ?1 AND u.debit=true and u.status='Success'")
	double getAllTransactionDebitByAgent(PQAccountDetail account);
	
	@Query("select count(t),sum(t.amount),t.service from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 and t.debit=true and t.status='Success' GROUP BY t.service")
	List<Object[]> getAllTransactionsByService(Date from,Date to);
	
	@Query("select count(t),sum(t.amount),t.service from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.account=?3 and t.service NOT IN(?4)   AND t.transactionType=0 and t.debit=true and t.status='Success' GROUP BY t.service")
	List<Object[]> getAllTransactionsByServiceOfUser(Date from,Date to,PQAccountDetail account,List<PQService> service);
	
	@Query("select count(t),sum(t.amount),t.service from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.account=?3 and t.service IN(?4)   AND t.transactionType=0 and t.debit=true and t.status='Success'")
	List<Object[]> getAllTopUpByServiceOfUser(Date from,Date to,PQAccountDetail account,List<PQService> service);
	
	@Query("select count(t),sum(t.amount),t.service from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.account=?3  AND t.transactionType IN(0,4) AND t.status='Success' AND t.debit=false GROUP BY t.service")
	List<Object[]> getCreditTransactionsOfUser(Date from,Date to,PQAccountDetail account);
	
	@Query("select t from PQTransaction t where t.created between ?1 and ?2  AND status='Success' AND t.account=?3 order by t.created asc")
	List<PQTransaction> getUserTransaction(Date from,Date to,PQAccountDetail account);
	
	@Query("select  SUM(t.amount) from PQTransaction t where t.status='Success'  AND t.account=?1 AND t.debit=true")
	Double getFilteredUserDebitTransaction(PQAccountDetail account);
	
	@Query("select SUM(t.amount) from PQTransaction t where  t.status='Success' AND t.account=?1 AND t.debit=false")
	Double getFilteredUserCreditTransaction(PQAccountDetail account);
	
	//API3
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.debit=true")
	Double getTotalDebit(Date to,Date from,PQAccountDetail account);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?3 AND t.debit=false")
	Double getTotalcredit(Date to,Date from,PQAccountDetail account);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?3 AND t.service=?4 AND t.debit=false")
	double getLoadMoneyCredit(Date to,Date from,PQAccountDetail account,PQService service);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%C') AND t.account=?3 AND t.service=?4 AND t.debit=false")
	double getSendMoneyCredit(Date to,Date from,PQAccountDetail account,PQService service);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 AND t.debit=true")
	double getSendMoneydebit(Date to,Date from,PQAccountDetail account,PQService service);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 AND t.debit=true")
	double getTopupAndBillPaymentDebit(Date to,Date from,PQAccountDetail account,PQService service);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 AND t.debit=true")
	double getBankTransferdebit(Date to,Date from,PQAccountDetail account,PQService service);
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service=?4 ")
	double getServiceDebit(Date to,Date from,PQAccountDetail account,PQService service);
	
	@Query("select t from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 AND t.status='Success' AND t.service=?3")
	List<PQTransaction> getUserListAnalytics(Date from,Date to,PQService service);
	
/*	@Query("select t from PQTransaction t LEFT JOIN t.PQAccountDetail q ON t.account=q.id LEFT JOIN User u ON t.account_id=u.accountDetail ")
*/	
	@Query("select count(t),sum(t.amount),t.service from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 AND t.status='Success' AND t.service IN(112,1,2) AND t.debit=false GROUP BY t.service")
	List<Object[]> getCreditTransactions(Date from,Date to);
	
	
	@Query("select SUM(t.amount) from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0  AND t.status='Success' AND (t.transactionRefNo LIKE '%D') AND t.account=?3 AND t.service NOT IN (4,5,171,162,163,201,211,231,241,421,531,135) AND t.debit=true")
	double getTopupAndBillPayment(Date to,Date from,PQAccountDetail account);
	
	//for sending sms for transaction reversed(topups and billpayment)
	
	@Query("select t from PQTransaction t where DATE(t.created) between ?1 AND ?2 AND t.transactionType=0 AND t.status='Reversed'")
	List<PQTransaction> getInitiatedTransaction(Date toDate,Date fromDate);
	
	@Query("select u from PQTransaction u where u.status='Success' and u.account=?1 and u.transactionType IN (0,4) and u.currentBalance >=0 and u.created BETWEEN ?2 AND ?3")
	List<PQTransaction> findMonthlyUserTransaction(PQAccountDetail account,Date startDate,Date endDate);
	
	@Query("select sum(u.amount) from PQTransaction u where u.transactionType IN(0) AND u.status='Success' AND u.debit=false AND u.account=?1 and u.created BETWEEN ?2 AND ?3 ")
	double findAllCreditBalance(PQAccountDetail detail,Date startDate,Date endDate);
	
	@Query("select sum(u.amount) from PQTransaction u where u.transactionType=0 AND u.status='Success' AND u.debit=true AND u.account=?1 and u.created BETWEEN ?2 AND ?3 ")
	double findAllDebitBalance(PQAccountDetail detail,Date startDate,Date endDate);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2")
	long getDailyTransactionCountBetween(Date from, Date to);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.account=?3")
	long getDailyTransactionCountBetweenForAccount(Date from, Date to, PQAccountDetail account);
	
	@Query("select t from PQTransaction t where t.service=?1 AND t.account=?2")
	List<PQTransaction> findByService(PQService service,PQAccountDetail accountDetail);
	
	@Query("select t from PQTransaction t where  t.account=?1 and t.service IN (select p.id from PQService p where p.serviceType=6) and t.status='Success' and t.debit=true and t.transactionType=0 and timestampdiff(day,date(t.created),current_date) between 25 and 28")
	List<PQTransaction> findAllBillPaymentTransaction(PQAccountDetail accountDetail);

	@Query("select t from PQTransaction t where t.service=?1 AND t.transactionType=0 order by t.created desc")
	List<PQTransaction> findByServiceFlight(PQService service);
	
	@Query("select u from PQTransaction u where u.service=?1  and u.transactionType=2 and u.debit=false")
	Page<PQTransaction> findAllCommissionByMerchantServicePage(PQService service,Pageable pageable);
	
	@Query("select u from PQTransaction u where u.service=?1  and u.transactionType=2 order by u.created desc")
	List<PQTransaction> findAllCommissionByMerchantService(PQService service);
	
	@Query("select t from PQTransaction t where t.service=?1 AND t.transactionType=0 AND t.status='Success' and t.debit=true ORDER BY t.created desc")
	List<PQTransaction> findTreatCardTransactions(PQService service);


	@Query("select count(t) from PQTransaction t where t.created BETWEEN ?1 AND ?2 AND t.transactionType=?3 AND status='Success' AND t.account not in (125119,3,177111,72)")
	long findCountByMonth(Date parse, Date parse2,TransactionType transactionType);
	
	@Query("select count(t) from PQTransaction t where t.created BETWEEN ?1 AND ?2 AND t.account=?3 AND status='Success'")
	long findCountByMonthUser(Date parse, Date parse2,PQAccountDetail account);
	
//	@Query("select t from PQTransaction t where  AND t.account=?1 and t.service IN (select p.id from PQService p where p.serviceType=6) and t.status='Success' and t.debit=true and t.transactionType=0 and timestampdiff(day,date(t.created),t.date=?1) between  25 and 28")
//	List<PQTransaction> findAllBillPaymentTransaction(PQAccountDetail accountDetail,Date date);
	
	
	@Query("select sum(u.amount) from PQTransaction u where u.transactionType=0 AND u.status='Success' and u.created BETWEEN ?1 AND ?2 and u.service NOT IN(?3)")
	double getSumOfAllCreditAndDebitAmount(Date startDate, Date endDate, PQService service);
	
	@Query("SELECT count(u) from PQTransaction u where u.transactionType=0 AND u.status='Success' and u.created BETWEEN ?1 AND ?2 and u.service NOT IN(?3)")
	long getCountOfAllCreditAndDebitTxns(Date from, Date to, PQService service);
	
	@Query("select sum(u.amount) from PQTransaction u")
	double getSumOfAllTxnAmount();

	@Query("SELECT count(u) from PQTransaction u")
	long getCountOfAllTxns();
	
	@Query("SELECT count(u) FROM PQTransaction u")
	long getransactionCount();
	
	@Query("SELECT count(u) FROM PQTransaction u where u.debit=?1")
	long getdebitTransactioncount(boolean s);
	
	@Query("select u from PQTransaction u order by u.created desc")
	Page<PQTransaction> findAllTransactionList(Pageable page);
    
	/*@Query("select u from PQTransaction u order by u.created desc LIMIT 10")
	List<PQTransaction> findAllTransactionListCheck();*/
	
	/*@Query("select sum(u.amount) from PQTransaction u where u.account=?1 and u.debit=?2 and u.status=?3")
	Double getDebitSum(PQAccountDetail pq,boolean s,Status sa);*/
	
	@Query("select u from PQTransaction u where u.debit=?1 order by u.created desc")
	Page<PQTransaction> findDebitTransactionList(Pageable page,boolean val);
	
	@Query("SELECT count(u) FROM PQTransaction u where u.service.id in (select p.id from PQService p where p.id=?1)")
	long getAllRecharge(long pq);
    
	//@Query("SELECT count(u) FROM PQTransaction u where u.service.id in (select p.id from PQService p where p.id=?1")
	//long getTravelList(long pq);
	
	@Query("SELECT sum(u.amount) FROM PQTransaction u where u.debit=?1")
	Double getdebitTransactionSum(boolean s);

	@Query("SELECT count(u) FROM PQTransaction u where YEAR(u.created) = ?1 AND MONTH(u.created) = ?2")
   long getCurrentTransaction(int year, int i);

	@Query("select u from PQTransaction u where u.created BETWEEN ?1 AND ?2")
	Page<PQTransaction> findAllTransactionListDateWise(Pageable page, Date startDate, Date endDate);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.status='Success' AND u.debit = true and u.transactionType IN (0,4)")
	double getSumOfAmountofTxnsByFromAndToDate(Date from, Date to);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2  AND u.status='Success' AND u.debit = true and u.transactionType IN (0,4)")
	long getCountOfTxnsByFromAndToDate(Date from, Date to);
	
	@Query("select u from PQTransaction u where YEAR(u.created)=YEAR(now()) and u.service=?1 and u.transactionType = ?2 and debit=true ORDER BY u.created DESC")
	Page<PQTransaction> findTreatCardTransaction(Pageable page,PQService service,TransactionType transactionType);

	
	
	/*Admin Panel Report*/
	
	@Query("SELECT DATE(t.created) FROM PQTransaction t where t.transactionType='0' AND t.status='Success'")
	List<Date> getBeginDateOfAllTxns();
	
	@Query("SELECT distinct u.account FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.status='Success'")
	List<PQAccountDetail> getTotalAccountByFromAndToDate(Date from, Date to);
	
	
	@Query("SELECT u FROM PQTransaction u where u.status='Success' AND u.account =?1 AND DATE(u.created) BETWEEN ?2 AND ?3")
	List<PQTransaction> getTotalTxnsByStatusAndAccountId(PQAccountDetail account, Date from, Date to);
	
	/*Admin Panel Report*/
	
	@Query("SELECT distinct u.account FROM PQTransaction u where DATE(u.created) =?1 AND u.status='Success' order by u.created desc")
	List<PQAccountDetail> getTotalAccountByFromDate(Date from);
	
	@Query("SELECT u FROM PQTransaction u where u.status='Success' AND u.account =?1")
	List<PQTransaction> getTotalTxnByStatusAndAccountId(PQAccountDetail account);
	
	@Query("SELECT DATE(t.created) FROM PQTransaction t where t.debit=false AND t.status='Success'")
	List<Date> getLastDateOfLoadMoney();
	
	@Query("SELECT distinct u.account FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.transactionType='0' AND u.status='Success'")
	List<PQAccountDetail> getTotalTxnsAccount(Date from, Date to);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	double getTotalSMUCreditByDate(Date date, PQService smu);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	double getTotalSMRCreditByDate(Date date, PQService smr);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	double getTotalSMRDebitByDate(Date date, PQService smr);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	double getTotalSMUDebitByDate(Date date, PQService smu);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	long getCountSMUCreditByDate(Date date, PQService smu);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=false AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	long getCountSMRCreditByDate(Date date, PQService smr);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	long getCountSMUDebitByDate(Date date, PQService smu);
	
	@Query("SELECT count(u) FROM PQTransaction u where DATE(u.created) =?1 AND u.debit=true AND u.transactionType='0' AND u.status='Success' AND u.service=?2")
	long getCountSMRDebitByDate(Date date,PQService smr);
	
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	double getValidAmountByService(PQService service, Date date);

	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false")
	double getValidAmountByServiceForDate(PQService service);
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	Long getValidCountByServiceForDate(PQService service,Date date);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=true AND DATE(u.created)=?2")
	double getValidDebitAmountByService(PQService service, Date date);
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	Long getCountValidTopupAmountForDate(PQAccountDetail account,Date date);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	double getTotalBankAmountForDate(PQAccountDetail account, Date date);
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.status='Success' AND u.debit=false AND DATE(u.created)=?1 AND u.service = ?2")
	double getTotalAmountPromoCodes(Date date, PQService service);
	
	/*@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.status='Success' AND u.refundStatus=true AND DATE(u.created)=?1")
	double getTotalAmountMerchantRefunds(Date date);*/
	
	@Query("SELECT COUNT(u) FROM PQTransaction u where u.status='Success' AND u.debit=false AND DATE(u.created)=?1 AND u.service = ?2")
	Long getCountPromoCodes(Date date, PQService service);
	
	/*@Query("SELECT COUNT(u) FROM PQTransaction u where u.status='Success' AND u.refundStatus=true AND DATE(u.created)=?1")
	Long getCountMerchantRefunds(Date date);*/
	
	@Query("SELECT SUM(u.amount) FROM PQTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	double getTotalMerchatAmountForDate(PQAccountDetail account, Date date);
	
	@Query("SELECT count(*) FROM PQTransaction u where u.account = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)=?2")
	long getCountMerchatAmountForDate(PQAccountDetail account, Date date);
	
	@Query("SELECT u FROM PQTransaction u where u.service = ?1 AND u.status='Success' AND u.debit=false AND DATE(u.created)>=?2")
	List<PQTransaction> getValidAmountByServiceForMIS(PQService service,Date from);
	
	@Transactional
	@Query("SELECT t FROM PQTransaction t LEFT JOIN t.service s WHERE DATE(t.created)>=?1 and DATE(t.created)<=?2 and t.status='Success' and (s.code=?3 or s.code=?4)")
	List<PQTransaction> getallLoadMoney(Date date,Date date2,String service1,String service2);
	
	
	@Query("SELECT count(*) FROM PQTransaction u where u.account = ?1 AND u.status=?2")
	long getCountTxnsByAccountAndStatus(PQAccountDetail account, Status status);
	
	@Query("SELECT count(*) FROM PQTransaction u where u.account = ?1")
	long getCountTxnsByAccount(PQAccountDetail account);
   
	/*@Query("SELECT u FROM PQTransaction u where DATE(u.created) BETWEEN ?1 AND ?2 AND u.refundStatus=true AND u.status='Success'")
	List<PQTransaction> getIPayRefundedTnxBetween(Date from, Date to);
	
	@Transactional
	@Query("SELECT t FROM PQTransaction t LEFT JOIN t.service s LEFT JOIN s.serviceType u WHERE u=?1 AND t.refundStatus=true AND t.status='Success'")
	List<PQTransaction> getAllRefundedInstanPayTxn(PQServiceType pqServiceType);*/
	

	@Query("SELECT count(u) FROM PQTransaction u where YEAR(u.created) <= ?1")
	   long getransactionCountyear(int year);


	@Query("SELECT u FROM PQTransaction u where u.checkStatus=true AND u.service=?1 and status='Success'")
	List<PQTransaction> findPendingTransacions(PQService service);
	
	@Query("select u from PQTransaction u where u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.account= ?4")
	List<PQTransaction> getAllBescomUpiTxns(TransactionType transactionType,Date startDate,Date endDate, PQAccountDetail account);
	
	@Query("select u from PQTransaction u where u.transactionType=?1 and u.created BETWEEN ?2 AND ?3 AND u.account= ?4")
	List<PQTransaction> getAllVijayaUpiTxns(TransactionType transactionType,Date startDate,Date endDate);
	
	 @Query("select u from PQTransaction u where u.service=?1 ")
	Page<PQTransaction> getUPITxn(Pageable pageable,PQService upiService);

	@Query("select u from PQTransaction u where u.service=?1 and u.created BETWEEN ?2 AND ?3 ")
	List<PQTransaction> getUPITxn(PQService upiService, Date fromDate, Date toDate);
	   
	@Query("select u from PQTransaction u where u.account=?1")
	List<PQTransaction> findUserByAccId(PQAccountDetail accountDetail);

	@Query("select u from PQTransaction u where u.service=?1")
	Page<PQTransaction> findAllCommissionByTKService(Pageable pageable, PQService service);
	
	
	@Query("select t from PQTransaction t where t.suspicious=1 and t.status=?1")
	List<PQTransaction> getSuspisiousTxn(Status success);
	
	@Query("select count(t) from PQTransaction t where Date(t.created) =?1 and debit=1 and status='Success'")
	double findCountOfTransactionForPPI(Date date);
	
	@Query("select sum(t.amount) from PQTransaction t where Date(t.created) =?1 and debit=1 and status='Success'")
	double findAmountOfTransactionForPPI(Date date);
	
	@Query("select sum(amount) from PQTransaction u where u.service in (?1) and u.created BETWEEN ?2 AND ?3  and u.transactionType=?4 and u.status=?5 ORDER BY u.created DESC")
	double getTransactionServiceWiseMonthly(List<PQService> services,Date startDate,Date endDate,TransactionType transactionType,Status status);
		
	@Query("select s from PQTransaction s where Date(s.created) =?1 and status='Success' and debit=true")
	List<PQTransaction> findTransactionByDateFilter(Date date);
	
	@Query("select t from PQTransaction t where t.account=?1 and t.created >=?2 and t.created <=?3 and t.status IN ('Success','Booked')  order by t.created asc")
	List<PQTransaction> getOpeningBalance(PQAccountDetail accountDetail, Date start, Date end);

	@Query("select sum(pq.amount) from PQTransaction pq where pq.account=?1 and date(pq.created) >=?2 "
			+ " AND date(pq.created) <=?3  AND pq.debit=1 AND pq.status IN ('Success','Booked')")
	Double getTotalDebitAmount(PQAccountDetail accountDetail, Date start, Date end);
	
	@Query("select sum(pq.amount) from PQTransaction pq where pq.account=?1 and date(pq.created) >=?2 "
			+ " AND date(pq.created) <=?3  AND pq.debit=0 and pq.service IN (?4) AND pq.status IN ('Success','Booked')")
	Double getTotalCreditAmount(PQAccountDetail accountDetail, Date start, Date end, List<PQService> creditService);
	
	///////Daily Report
	
	@Query("SELECT count(u) FROM PQTransaction u WHERE u.status='Success' AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 and u.account NOT IN (?3,?4) and u.transactionType in (0,11)")
	 Long getTotalTransactionByDate(Date fromDate, Date toDate,List<PQAccountDetail> accounts,PQAccountDetail account1);

	 @Query("SELECT SUM(u.amount) FROM PQTransaction u WHERE u.status='Success' AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.debit=true AND u.transactionRefNo NOT LIKE '%DS' AND u.transactionType=0 AND u.service not in (?3,?4,?5)")
	 Double getTotalDebitTransactionByDate(Date fromDate, Date toDate, PQService service1, PQService service2,PQService service3);

	 @Query("SELECT SUM(u.amount) FROM PQTransaction u WHERE u.status='Success' and u.debit=false AND u.service in(2,112,631) and u.transactionType in (0,11) AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2")
	 Double getTotalCreditTransactionByDate(Date fromDate, Date toDate);
	 
	 @Query("SELECT SUM(u.amount) FROM PQTransaction u WHERE u.status='Success' and u.debit=false and u.transactionType in (0,11) AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.service in(?3)")
	 Double getTotalEBSCreditTransactionByDate(Date fromDate, Date toDate,PQService service);
	 
	 @Query("SELECT SUM(u.amount) FROM PQTransaction u WHERE u.status='Success' and u.debit=false and u.transactionType in (0,11) AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.service in(?3)")
	 Double getTotalVNETCreditTransactionByDate(Date fromDate, Date toDate,PQService service);
	 
	 @Query("SELECT SUM(u.amount) FROM PQTransaction u WHERE u.status='Success' and u.debit=false and u.transactionType in (0,11) AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2 AND u.service in(?3)")
	 Double getTotalUPICreditTransactionByDate(Date fromDate, Date toDate,PQService service);
	 
	 @Query("SELECT count(u) FROM PQTransaction u WHERE u.status='Initiated' AND u.transactionType=0 and debit=true")
	 Long getTotalInitiatedTransactionByDate();
	 
	 @Query("SELECT count(u) FROM PQTransaction u WHERE u.status='Reversed' AND u.service NOT IN(2,112,631) AND u.transactionRefNo like '%D' AND u.transactionType=0 and debit=true AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2")
	 Long getTotalFailedTransactionByDate(Date fromDate, Date toDate);
	 
	 @Query("select u from PQTransaction u where  u.account=?1 and  u.service in (?2) and u.status='Success'")
		List<PQTransaction> findLoadMoneyTransactions(PQAccountDetail account,List<PQService> service);
}
