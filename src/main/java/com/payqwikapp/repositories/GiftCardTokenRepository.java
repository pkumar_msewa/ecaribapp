package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.GiftCardToken;

public interface GiftCardTokenRepository extends CrudRepository<GiftCardToken, String>, PagingAndSortingRepository<GiftCardToken, String>, JpaSpecificationExecutor<GiftCardToken> {

	 @Query("select g from GiftCardToken g where g.id=(select max(g.id) from GiftCardToken s)")
	 GiftCardToken getLatestToken();
}
