package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.model.Status;


public interface BusTicketRepository extends CrudRepository<BusTicket,Long>,JpaSpecificationExecutor<BusTicket>{

	@Query("select b from BusTicket b where b.transaction=?1")
	BusTicket getByTransaction(PQTransaction transaction);
	
	@Query("select b from BusTicket b order by b.created desc")
	List<BusTicket> getByStatus();
	
	@Query("select b from BusTicket b where b.status!=?1 and b.transaction.account=?2  order by b.created desc")
	List<BusTicket> getByStatusAndAccount(Status status,PQAccountDetail accout);
	
	@Query("select b from BusTicket b where b.emtTxnId=?1 order by b.created desc")
	BusTicket getTicketByTxnId(String emtTxnId);
	
	@Query("select t from BusTicket t where t.created BETWEEN ?1 AND ?2 order by t.created desc")
	List<BusTicket> getBusTicketByDate(Date from, Date to);
	
	@Query("select b from BusTicket b where b.seatHoldId=?1")
	BusTicket getTicketBySeatHoldId(String seatHoldId);
	
	@Query("select b from BusTicket b where b.tripId=?1")
	BusTicket getTicketBytripId(String tripId);
}
