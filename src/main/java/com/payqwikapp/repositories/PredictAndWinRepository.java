package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PredictAndWinPayment;
import com.payqwikapp.model.Status;

public interface PredictAndWinRepository extends CrudRepository<PredictAndWinPayment, Long>,
		PagingAndSortingRepository<PredictAndWinPayment, Long>, JpaSpecificationExecutor<PredictAndWinPayment> {

	@Query("select b from PredictAndWinPayment b where b.mobileNumber=?1 and b.status='Open'")
	PredictAndWinPayment findByMobileNumber(String name);

	@Query("select b from PredictAndWinPayment b where b.mobileNumber=?1 and status=?2")
	PredictAndWinPayment findByMobileNumberWithSatus(String name, Status status);

	@Query("select b from PredictAndWinPayment b where b.status=?1")
	List<PredictAndWinPayment> findAllFiles(Status status);

}
