package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PromoCode;

public interface PromoCodeRepository extends CrudRepository<PromoCode, Long>, PagingAndSortingRepository<PromoCode, Long>, JpaSpecificationExecutor<PromoCode>{

	@Query("select s from PromoCode s where s.id=?1")
	PromoCode findById(Long id);
//
//	@Query("select s from PromoCode s where s.serviceType=?1")
//	PromoCode findByValue(ServiceType service);
//
	@Query("select s from PromoCode s where s.promoCode=?1")
	PromoCode findByPromoCode(String code);
	
	
	@Query("select pc from PromoCode pc order by  pc.created desc")
	List<PromoCode> findAllDesc();
}
