package com.payqwikapp.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.YesBankPoolBal;

public interface YesBankPoolBalRepository extends CrudRepository<YesBankPoolBal, Long>,
		PagingAndSortingRepository<YesBankPoolBal, Long>, JpaSpecificationExecutor<YesBankPoolBal> {

	@Query("SELECT u FROM YesBankPoolBal u where DATE(u.created) = ?1")
	YesBankPoolBal getBalanceByDate(Date date);
}
