package com.payqwikapp.repositories;

import com.payqwikapp.entity.GasRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GasRequestLogRepository extends CrudRepository<GasRequestLog, Long>,
        PagingAndSortingRepository<GasRequestLog, Long>, JpaSpecificationExecutor<GasRequestLog> {

    @Query("select l from GasRequestLog l where l.transactionRefNo = ?1")
    GasRequestLog findByTransactionRefNo(String transactionRefNo);

}
