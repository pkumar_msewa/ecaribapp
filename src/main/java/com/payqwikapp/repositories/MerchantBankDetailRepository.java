
package com.payqwikapp.repositories;

import com.payqwikapp.entity.MerchantBankDetail;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface MerchantBankDetailRepository extends CrudRepository<MerchantBankDetail, Long>,
		PagingAndSortingRepository<MerchantBankDetail, Long>, JpaSpecificationExecutor<MerchantBankDetail> {
}
