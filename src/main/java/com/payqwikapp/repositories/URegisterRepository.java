package com.payqwikapp.repositories;

import com.payqwikapp.entity.*;
import com.payqwikapp.model.Gender;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface URegisterRepository extends CrudRepository<URegister, Long>, PagingAndSortingRepository<URegister, Long>, JpaSpecificationExecutor<URegister> {

	@Query("select u from URegister u where u.username=?1")
	URegister findByUsername(String username);

	@Query("select u from URegister u where u.username=?1 and u.status=?2")
	URegister findByUsernameAndStatus(String username, Status status);

	@Query("select u from URegister u where u.status=?1 ORDER BY u.id desc")
	List<URegister> findByStatus(Status status);

	@Query("select u from URegister u where u.id > ?1 and u.status=?2")
	List<URegister> getByIds(long id,Status status);

	@Query("select u from URegister u where u.status=?1 and u.id between ?2 and ?3")
	List<URegister> findByStatusLimit(Status status,long from,long to);

}



