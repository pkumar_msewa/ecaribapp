package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.GiftCardRequestLog;
import com.payqwikapp.entity.MTRequestLog;

public interface GiftCardRequestLogRepository extends CrudRepository<GiftCardRequestLog, Long>,
		PagingAndSortingRepository<GiftCardRequestLog, Long>, JpaSpecificationExecutor<MTRequestLog> {

	@Query("select m from GiftCardRequestLog m where m.transactionRefNo = ?1")
	GiftCardRequestLog findByTransactionRefNo(String transactionRefNo);
}
