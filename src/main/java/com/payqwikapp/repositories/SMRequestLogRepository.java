package com.payqwikapp.repositories;

import com.payqwikapp.entity.SMRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SMRequestLogRepository extends CrudRepository<SMRequestLog, Long>,
        PagingAndSortingRepository<SMRequestLog, Long>, JpaSpecificationExecutor<SMRequestLog> {

    @Query("SELECT u FROM SMRequestLog u where u.transactionRefNo = ?1")
    SMRequestLog findByTransactionRefNo(String transactionRefNo);
}
