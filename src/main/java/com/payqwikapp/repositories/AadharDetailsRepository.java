package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.AadharDetails;

public interface AadharDetailsRepository extends CrudRepository<AadharDetails, Long>,
PagingAndSortingRepository<AadharDetails, Long>, JpaSpecificationExecutor<AadharDetails>{
	
	@Query("select a from AadharDetails a where a.aadharNumber=?1")
	AadharDetails findByAadharNumber(String aadharNumber);
	
	@Query("select a from AadharDetails a order by a.created DESC ")
	List<AadharDetails> findAllAadharDetails();
	

}
