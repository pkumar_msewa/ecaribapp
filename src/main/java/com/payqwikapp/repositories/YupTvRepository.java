package com.payqwikapp.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.YTVAccess;

public interface YupTvRepository extends CrudRepository<YTVAccess, Long>,
PagingAndSortingRepository<YTVAccess, Long>, JpaSpecificationExecutor<YTVAccess>{
	
	@Query("select u from YTVAccess u where u.uniqueId=?1")
	YTVAccess findByUniqueId(String uniqueId);
	
	@Query("select u from YTVAccess u where u.expiryDate=?1")
	YTVAccess findByExpiryDate(Date uniqueId);
	
	@Query("select u from YTVAccess u where u.userId=?1")
	YTVAccess findByUserId(String userId);
	

}
