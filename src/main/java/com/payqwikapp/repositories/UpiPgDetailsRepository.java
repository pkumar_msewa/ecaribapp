package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.UpiPgDetails;

public interface UpiPgDetailsRepository extends CrudRepository<UpiPgDetails, Long>,
		PagingAndSortingRepository<UpiPgDetails, Long>, JpaSpecificationExecutor<UpiPgDetails> {

	@Query("select u from UpiPgDetails u where u.pgDetails=?1")
	UpiPgDetails getUpiPgDetails(PGDetails details);
}
