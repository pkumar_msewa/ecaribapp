package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.AUserSignUp;
import com.payqwikapp.entity.AgentDetail;
import com.payqwikapp.entity.User;

public interface AUserSignUpRepository extends CrudRepository<AUserSignUp, Long>,
		PagingAndSortingRepository<AUserSignUp, Long>, JpaSpecificationExecutor<AgentDetail> {
	

	
	@Query("select COUNT(u) from AUserSignUp u Where u.agent_id=?1")
	Long getUserCount(User id);
}
