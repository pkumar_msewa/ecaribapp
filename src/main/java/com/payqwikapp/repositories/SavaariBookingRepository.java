package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.SavaariBooking;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;

public interface SavaariBookingRepository extends CrudRepository<SavaariBooking, Long>,
PagingAndSortingRepository<SavaariBooking, Long>, JpaSpecificationExecutor<SavaariBooking> {
	
	@Query("select u from SavaariBooking u where u.transctionRefNo=?1")
	SavaariBooking findByTransactionRefNo(String transctionRefNo);
	
	@Query("select u from SavaariBooking u where u.bookingId=?1 and u.status=?2 and u.userDetails=?3")
	SavaariBooking findByBookingId(String bookingId,Status booked,User user);
	
	
	@Query("SELECT t.pickupDateTime FROM SavaariBooking t where t.bookingId=?1 AND t.status=?2")
	Date getTimeStampOfLastTransactionByStatus(String bookingId,Status status);
	
	@Query("SELECT u FROM SavaariBooking u where u.userDetails=?1 AND u.status = ?2")
	List<SavaariBooking> getTransactions(User u,Status status);


}
