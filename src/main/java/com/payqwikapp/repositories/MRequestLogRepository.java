package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.MRequestLog;

public interface MRequestLogRepository extends CrudRepository<MRequestLog, Long>,
        JpaSpecificationExecutor<MRequestLog> {

}
