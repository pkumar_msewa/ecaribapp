package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.ServiceStatus;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;


public interface ServiceStatusRepository extends CrudRepository<ServiceStatus, Long>, PagingAndSortingRepository<ServiceStatus,Long>,JpaSpecificationExecutor<ServiceStatus> { 

	@Query("select u from ServiceStatus u where u.name=?1")
	ServiceStatus findByName(String fpName);
	
	@Modifying
	@Transactional
	@Query("update ServiceStatus s set s.status=?1 where s.id=?2")
	int updateServiceStatus(Status status, long id);
	
	@Query("select u from ServiceStatus u where   u.name=?1 and u.status=?2")
	ServiceStatus findByNameAndStatus(String name, Status active);
	
}
