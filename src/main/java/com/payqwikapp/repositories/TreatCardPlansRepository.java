package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.TreatCardPlans;


public interface TreatCardPlansRepository extends CrudRepository<TreatCardPlans, String>, PagingAndSortingRepository<TreatCardPlans, String>, JpaSpecificationExecutor<TreatCardPlans> {

	@Query("select t from TreatCardPlans t where t.days=?1")
	TreatCardPlans findPlansByDays(String days);
	
	@Query("select t from TreatCardPlans t")
	List<TreatCardPlans> findAllPlans();
	
	
	@Query("select t from TreatCardPlans t where t.id=?1")
	TreatCardPlans findPlansById(long id);

}
