package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.MBankTransfer;

public interface MBankTransferRepository extends CrudRepository<MBankTransfer, Long>,
		PagingAndSortingRepository<MBankTransfer, Long>, JpaSpecificationExecutor<MBankTransfer> {

	@Query("select s from MBankTransfer s order by s.created desc")
	List<MBankTransfer> findAllNeftTransaction();

	@Query("select s from MBankTransfer s where s.created=?1")
	List<MBankTransfer> findAllNeftTransactionByDate(String date);

	@Query("select b from MBankTransfer b where b.created between ?1 and ?2 order by b.created desc")
	List<MBankTransfer> getListByDate(Date startDate, Date endDate);

}
