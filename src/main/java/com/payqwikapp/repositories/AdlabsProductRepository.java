package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.AdlabsProduct;

public interface AdlabsProductRepository

extends CrudRepository<AdlabsProduct, String>, PagingAndSortingRepository<AdlabsProduct, String>, JpaSpecificationExecutor<AdlabsProduct> {
	


//  @Query("select * from GciProduct u where gcicart_id = ?1")
//    List<GciProduct> findByTransactionRefNo(String transactionRefNo);
}

