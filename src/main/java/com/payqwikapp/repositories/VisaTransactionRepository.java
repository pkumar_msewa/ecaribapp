package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.User;
import com.payqwikapp.entity.Visa;

public interface VisaTransactionRepository extends CrudRepository<Visa, Long>, JpaSpecificationExecutor<Visa>{

	
	@Modifying
	@Transactional
	@Query("update Visa a set a.visaTransactionRef=?1 where a.externalTransactionId=?2")
	int updateVisaTransactionRefNo(String a, String b);
	
		
}
