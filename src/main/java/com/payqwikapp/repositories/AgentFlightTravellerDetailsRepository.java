package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.AgentFlightTicket;
import com.payqwikapp.entity.AgentFlightTravellers;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.FlightTravellers;

public interface AgentFlightTravellerDetailsRepository  extends CrudRepository<AgentFlightTravellers,Long>,JpaSpecificationExecutor<AgentFlightTravellers>{
	@Query("select t from AgentFlightTravellers t where t.flightTicket=?1")
	List<AgentFlightTravellers> getTravellersByTicket(AgentFlightTicket flightTicket);
}
