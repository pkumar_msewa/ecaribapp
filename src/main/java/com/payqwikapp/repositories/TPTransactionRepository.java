package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionType;

public interface TPTransactionRepository extends CrudRepository<TPTransaction, Long>, JpaSpecificationExecutor<TPTransaction> {

    @Query("select t from TPTransaction t where t.orderId = ?1")
    TPTransaction findByOrderId(String orderId);

    @Query("select t from TPTransaction t where t.orderId = ?1 AND t.merchant = ?2")
    TPTransaction findByOrderIdAndMerchant(String orderId,User merchant);
    
    @Query("select t from TPTransaction t where t.orderId = ?1 AND t.merchant = ?2 AND t.amount = ?3")
    TPTransaction findByOrderIdAndMerchantAmount(String orderId,User merchant, double amount);

    @Query("select t from TPTransaction t where t.transactionRefNo = ?1")
    TPTransaction findByTransactionRefNo(String transactionRefNo);
    
    @Query("select t from TPTransaction t where t.transactionRefNo = ?1 AND t.merchant = ?2 ")
    TPTransaction findByTransactionRefNoAndMerchant(String transactionRefNo, User merchant);

    @Modifying
    @Transactional
    @Query("update TPTransaction t set t.status = ?2 where t.transactionRefNo = ?1")
    int updateTransactionStatus(String transactionRefNo,Status status);

    @Query("select t from TPTransaction t where t.merchant = ?1")
    List<TPTransaction> findByMerchant(User u);
    
	@Query("select u from TPTransaction u where YEAR(u.created)=YEAR(now()) and u.merchant=?1 and status='Success'")
	Page<TPTransaction> findMerchantTransactions(Pageable page,User merchant);
	
	@Query("select u from TPTransaction u where YEAR(u.created)=YEAR(now()) and u.merchant=?1 and status='Success'")
	List<TPTransaction> findTransactions(User merchant);
	
	@Query("select u from TPTransaction u where YEAR(u.created)=YEAR(now()) and u.merchant=?1 and u.created BETWEEN ?2 AND ?3 and status='Success'")
	Page<TPTransaction> findMerchantTransactionsFiltered(Pageable page,User merchant,Date startDate,Date endDate);
	
	@Query("select u from TPTransaction u where YEAR(u.created)=YEAR(now()) and u.merchant=?1")
	List<TPTransaction> findMerchantTransactionsByOrderId(User merchant);
	
	@Query("select u from TPTransaction u where u.approvedStatus='Success'")
	List<TPTransaction> getAllApprovedTxns();
}
