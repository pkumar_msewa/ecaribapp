package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.TravelkhanaItems;

public interface TravelkhanaItemsRepository extends CrudRepository<TravelkhanaItems,Long>,JpaSpecificationExecutor<TravelkhanaItems>{

}
