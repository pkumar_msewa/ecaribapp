package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.ApiVersion;
import com.payqwikapp.entity.VersionLogs;

public interface VersionLogsRepository extends CrudRepository<VersionLogs,Long>,JpaSpecificationExecutor<VersionLogs>,PagingAndSortingRepository<VersionLogs,Long>{

	@Query("select v from VersionLogs v where v.androidVersion=?1")
	VersionLogs getVersion(ApiVersion logs);
	
	@Query("select v from VersionLogs v where v.version=?1")
	VersionLogs getVersionByName(String version);
}
