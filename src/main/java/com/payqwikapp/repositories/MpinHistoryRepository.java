package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.MpinHistory;
import com.payqwikapp.entity.User;

public interface MpinHistoryRepository extends CrudRepository<MpinHistory,Long>, PagingAndSortingRepository<MpinHistory,Long>,JpaSpecificationExecutor<MpinHistory>{

	@Query("select u from MpinHistory u where u.user=?1")
	List<MpinHistory> findByListUser(User user);
	
	@Query("select u from MpinHistory u where u.user=?1")
	MpinHistory findByUser(User user);
	
	@Query("select u from MpinHistory u where u.user=?1 and date(created)=(select MIN(DATE(created)) from MpinHistory u where u.user=?1)")
	List<MpinHistory> updateUser(User user);
	
	
}
