package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.BankTransfer;

public interface BankTransferRepository extends CrudRepository<BankTransfer, Long>,
		PagingAndSortingRepository<BankTransfer, Long>, JpaSpecificationExecutor<BankTransfer> {

	@Query("select b from BankTransfer b order by b.created desc")
	Page<BankTransfer> getListByPage(Pageable page);

    @Query("select b from BankTransfer b where b.created between ?1 and ?2 order by b.created desc")
    List<BankTransfer> getListByDate(Date startDate,Date endDate);
    
    @Query("select s from BankTransfer s order by s.created desc")
	List<BankTransfer> findAllNeftTransaction();

	@Query("select s from BankTransfer s where Date(s.created) =?1")
	List<BankTransfer> findAllNeftTransactionByDate(Date date);
	
	@Query("select s from BankTransfer s where s.bankDetails=?1")
	List<BankTransfer> findAllTrasnactionExceptVijayaBank(Date date);
	
	 @Query("select b from BankTransfer b where b.created between ?1 and ?2 order by b.created desc")
	    Page<BankTransfer> getListByDateAndPageWise(Pageable pageable,Date startDate,Date endDate);

}
