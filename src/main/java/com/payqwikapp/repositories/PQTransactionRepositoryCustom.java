package com.payqwikapp.repositories;

import com.payqwikapp.model.UserDetailDTO;

public interface PQTransactionRepositoryCustom {

	
	long getCountOfT();
	
	
	UserDetailDTO getUserDataForValidation(String username,int currentDay,int currentMonth,int currentYear);
	
	UserDetailDTO getUserDataForP2PValidation(String username,int currentDay,int currentMonth,int currentYear);
	
	
	
}
