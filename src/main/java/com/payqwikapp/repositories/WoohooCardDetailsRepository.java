package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.WoohooCardDetails;

public interface WoohooCardDetailsRepository  extends CrudRepository<WoohooCardDetails, Long> ,
PagingAndSortingRepository<WoohooCardDetails, Long>,JpaSpecificationExecutor<WoohooCardDetails> {

	@Query("select w from WoohooCardDetails w where w.transactionRefNo=?1")
	WoohooCardDetails findByTransactionRefNo(String transactionRefNo);
	
	@Query("select w from WoohooCardDetails w where w.retrivalRefNo=?1")
	WoohooCardDetails findByRetrivalRefNo(String transactionRefNo);

	@Query("select w from WoohooCardDetails w order by w.created desc")
	List<WoohooCardDetails> findAllCards();

	@Query("select w from WoohooCardDetails w where w.transaction=?1")
	WoohooCardDetails findByTransaction(PQTransaction transaction);

//	List<WoohooCardDetails> findPendingTransacions(String string);
}
