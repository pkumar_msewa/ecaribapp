package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.TreatCardCount;

public interface TreatCardCountRepository extends CrudRepository<TreatCardCount, Long>,
PagingAndSortingRepository<TreatCardCount, Long>{
	
	@Query("select t from TreatCardCount t where t.username=?1")
	TreatCardCount findByUser(String username);
	
	@Query("select t from TreatCardCount t  order by t.count desc")
	List<TreatCardCount> findAllCountList();

}
