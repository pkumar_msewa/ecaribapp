package com.payqwikapp.repositories;

import com.payqwikapp.entity.BusPassengerTrip;
import com.payqwikapp.entity.BusTripDetails;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BusTripPassengerRepository extends CrudRepository<BusPassengerTrip,Long>,JpaSpecificationExecutor<BusPassengerTrip>,PagingAndSortingRepository<BusPassengerTrip,Long> {

    @Query("SELECT b FROM BusPassengerTrip b where b.busTripDetails = ?1")
    List<BusPassengerTrip> getAllPassengerByTrip(BusTripDetails tripDetails);

}
