package com.payqwikapp.repositories;

import com.payqwikapp.entity.ImagicaOrders;
import com.payqwikapp.entity.PQTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ImagicaOrdersRepository extends CrudRepository<ImagicaOrders, String>, PagingAndSortingRepository<ImagicaOrders, String>, JpaSpecificationExecutor<ImagicaOrders> {


    @Query("select o from ImagicaOrders o where o.orderId=?1")
    ImagicaOrders findByOrderId(String orderId);

    @Query("select o from ImagicaOrders o where o.transaction=?1")
    ImagicaOrders findByTransaction(PQTransaction transaction);
}

