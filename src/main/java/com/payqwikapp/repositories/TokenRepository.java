package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.TokenKey;

public interface TokenRepository extends CrudRepository<TokenKey, Long>, JpaSpecificationExecutor<TokenKey> {

	
	@Query("Select t from TokenKey t where t.consumerSecretKey=?1 AND t.consumerTokenKey=?2")
	TokenKey getTokenByConsumerKeyAndConsumerToken(String consumerKey, String consumerToken);
}
