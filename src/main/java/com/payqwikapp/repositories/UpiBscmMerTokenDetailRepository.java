package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.UpiBscmMerTokenDetails;

public interface UpiBscmMerTokenDetailRepository extends CrudRepository<UpiBscmMerTokenDetails,Long>,JpaSpecificationExecutor<UpiBscmMerTokenDetails>{

    @Query("select u from UpiBscmMerTokenDetails u where u.merchantVpa=?1")
    UpiBscmMerTokenDetails getUpiMerchant(String merchantVpa);

}
