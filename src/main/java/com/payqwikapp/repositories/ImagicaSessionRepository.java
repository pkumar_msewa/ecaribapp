package com.payqwikapp.repositories;

import com.payqwikapp.entity.ImagicaSession;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ImagicaSessionRepository extends CrudRepository<ImagicaSession, Long>, JpaSpecificationExecutor<ImagicaSession> {

    @Query("select i from ImagicaSession i where i.username=?1")
    ImagicaSession getByUserName(String username);

    @Query("select i from ImagicaSession i where i.id=(select max(s.id) from ImagicaSession s)")
    ImagicaSession getLatestCredentials();
}

