package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.EventDetails;

public interface EventDetailsRepository extends CrudRepository<EventDetails, Long>, PagingAndSortingRepository<EventDetails, Long>, JpaSpecificationExecutor<EventDetails> {


}
