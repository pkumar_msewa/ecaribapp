package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.SendNotification;
import com.payqwikapp.model.Status;

public interface SendNotificationRepository extends CrudRepository<SendNotification, Long>, JpaSpecificationExecutor<SendNotification>  {
	
	@Query("select s from SendNotification s where s.status=?1")
	SendNotification findNotificationByStatus(Status status);
	
	@Query("select s from SendNotification s where s.status=?1")
	List<SendNotification> findListNotificationByStatus(Status status);

	@Query("select u from SendNotification u ")
	Page<SendNotification> getGcmList(Pageable pageable);
}
