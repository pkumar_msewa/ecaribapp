package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PasswordHistory;
import com.payqwikapp.entity.User;

public interface PasswordHistoryRepository extends CrudRepository<PasswordHistory,Long>, PagingAndSortingRepository<PasswordHistory,Long>,JpaSpecificationExecutor<PasswordHistory>{

	@Query("select u from PasswordHistory u where u.user=?1")
	List<PasswordHistory> findByListUser(User user);
	
	@Query("select u from PasswordHistory u where u.user=?1")
	PasswordHistory findByUser(User user);
	
	@Query("select u from PasswordHistory u where u.user=?1 and date(created)=(select MIN(DATE(created)) from PasswordHistory u where u.user=?1)")
	List<PasswordHistory> updateUser(User user);
	
	@Query("select count(u) from PasswordHistory u where u.user=?1 ")
	long countNoOfPasswords(User user);

	@Query("select u from PasswordHistory u where u.user=?1 and u.password IS NULL")
	PasswordHistory findPasswordNull(User u);
}
