package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.FlightTravellers;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TravellerDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;


public interface FlightTravellerDetailsRepository extends CrudRepository<FlightTravellers,Long>,JpaSpecificationExecutor<FlightTravellers>{

	@Query("select t from FlightTravellers t where t.flightTicket=?1")
	List<FlightTravellers> getTravellersByTicket(FlightTicket flightTicket);
	
	
}
