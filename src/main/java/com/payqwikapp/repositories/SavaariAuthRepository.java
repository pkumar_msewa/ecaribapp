package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.SavaariAuth;


public interface SavaariAuthRepository extends CrudRepository<SavaariAuth, Long>,
PagingAndSortingRepository<SavaariAuth, Long>, JpaSpecificationExecutor<SavaariAuth>{
	
	@Query("select e from SavaariAuth e where e.account=?1")
	SavaariAuth findByAccountDetails(PQAccountDetail accountDetail);

}
