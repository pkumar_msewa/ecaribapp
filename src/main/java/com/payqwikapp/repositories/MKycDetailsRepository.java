package com.payqwikapp.repositories;

import com.payqwikapp.entity.MKycDetails;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MKycDetailsRepository extends CrudRepository<MKycDetails,Long>,PagingAndSortingRepository<MKycDetails,Long>,JpaSpecificationExecutor<MKycDetails>{

}
