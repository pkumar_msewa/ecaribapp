package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.MpinLog;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;

public interface MpinLogRepository extends CrudRepository<MpinLog, Long>,JpaSpecificationExecutor<MpinLog> {

	
	@Query("select c from MpinLog c where c.created > CURRENT_DATE and c.user=?1 and c.status=?2")
	List<MpinLog> findTodayEntryForUserWithStatus(User user, Status status);
}
