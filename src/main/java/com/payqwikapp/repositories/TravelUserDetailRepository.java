package com.payqwikapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.TravelUserDetail;

public interface TravelUserDetailRepository extends CrudRepository<TravelUserDetail, Long>,
PagingAndSortingRepository<TravelUserDetail, Long>{

}
