package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.*;
import com.payqwikapp.model.Gender;
import com.payqwikapp.model.UserType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.model.Status;

public interface UserRepository
extends CrudRepository<User, Long>, PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {

	@Query("select u from User u where u.accountDetail IN (?1)")
	List<User> getUsersByAccount(List<PQAccountDetail> accounts);

	@Query("select u from User u where  u.userType IN (?1)")
	List<User> getByUserType(List<UserType> userType);

	@Query("select u from User u where  u.userType IN (?1)")
	User getUserType(UserType userType);

	@Query("select u.userDetail.email from User u where u.mobileStatus='Active' AND u.userType=?1")
	List<String> getMailsOfActiveUser(UserType user);

	@Query("select u from User u where u.username=?1")
	User checkForLogin(String username);

	@Query("select u.accountDetail from User u where u.authority=?1")
	List<PQAccountDetail> getAccountsByAuthority(String authority);
	
	@Query("select u.accountDetail from User u where u.authority in(?1,?2)")
	List<PQAccountDetail> getAccountsByAuthorities(String authority,String authority1);
	
	
	@Query("select u from User u where u.authority='ROLE_LOCKED,ROLE_AUTHENTICATED'")
	List<User> getAccountByAuthority();

	@Query("select u from User u where u.accountDetail=?1")
	User getByAccount(PQAccountDetail account);

	@Query("select u from User u where u.userType=?1 ORDER BY u.created DESC ")
	Page<User> getTodaysUser(Pageable page,UserType userType);

	@Query("select u from User u where u.userType=?1 and u.userDetail.gender=?2 ORDER BY u.created DESC")
	Page<User> getTodaysUser(Pageable page,UserType userType,Gender gender);

	@Query("select u from User u where u.userType=?1 and u.accountDetail.accountType=?2 ORDER BY u.created DESC ")
	Page<User> getTodaysUser(Pageable page,UserType userType, PQAccountType accountType);

	@Query("select u from User u where u.userType=?1 and u.authority=?2 ORDER BY u.created DESC ")
	Page<User> getTodaysUser(Pageable page,UserType userType,String authority);

	@Query("select u from User u where u.userType=?1 and u.mobileStatus=?2 ORDER BY u.created DESC ")
	Page<User> getTodaysUser(Pageable page,UserType userType,Status status);

	@Query("select u from User u where u.userType=?1 and u.created BETWEEN ?2 AND ?3")
	List<User> getUserBetween(UserType userType,Date startDate,Date endDate);

	@Query("select u from User u where u.userType=?1 and u.authority=?4 and u.created BETWEEN ?2 AND ?3")
	List<User> getUserBetween(UserType userType,Date startDate,Date endDate,String authority);


	@Query("select u from User u where u.userType=?1 and u.mobileStatus=?4 and u.created BETWEEN ?2 AND ?3")
	List<User> getUserBetween(UserType userType,Date startDate,Date endDate,Status status);

	@Query("select u from User u where u.userType=?1 and u.accountDetail.accountType=?4 and u.created BETWEEN ?2 AND ?3")
	List<User> getUserBetween(UserType userType,Date startDate,Date endDate,PQAccountType accountType);

	@Query("select u from User u where u.userType=?1 and u.userDetail.gender=?4 and u.created BETWEEN ?2 AND ?3")
	List<User> getUserBetween(UserType userType,Date startDate,Date endDate,Gender gender);

	@Query("select u from User u where u.userDetail.email=?1")
	List<User> getByMail(String email);

	@Query("select u from User u where u.username=?1")
	User findByUsername(String username);

	@Query("select u from User u where u.username=?1")
	List<User> findByMobileNo(String username);

	@Query("select u.gcmId from User u where u.userType=?1 AND u.mobileStatus='Active' AND u.gcmId IS NOT NULL")
	Page<String> getGCMIds(Pageable pageable,UserType userType);

	@Query("select u from User u where u.userType IN (?1,?2)")
	List<User> getAllBlockedAdmins(UserType admin,UserType superAdmin);

	@Query("select u from User u where u.userType = ?1 and u.emailStatus='Active' and u.mobileStatus='Active'")
	List<User> getAllActiveUsers(UserType user);

	@Query("select u from User u where u.username=?1 and u.mobileStatus=?2")
	User findByUsernameAndStatus(String username, Status status);

	@Query("select u from User u where u.username=?1 and u.mobileStatus=?2 and u.emailStatus=?3")
	User findByUsernameAndMobileStatusAndEmailStatus(String username, Status mobileStatus, Status emailStatus);

	@Query("select u from User u where u.userType='1' Order by u.username ASC")
	List<User> findAllUser();

	@Query("select u from User u where u.authority='ROLE_USER,ROLE_BLOCKED'")
	List<User> findAllBlockedUsers();

	@Query("select u from User u where u.authority LIKE '%ROLE_USER%' and u.userType='1' and u.mobileStatus='Active'")
	Page<User> findAuthenticateUser(Pageable page);

	@Query("select u from User u where u.authority in('ROLE_USER,ROLE_AUTHENTICATED','ROLE_USER,ROLE_LOCKED') and u.userType='1' and u.mobileStatus='Active' and Date(u.created) BETWEEN date(?1) AND date(?2)")
	List<User> findAuthenticateUsers(Date startDate,Date endDate);

	@Query("select u from User u where u.authority in('ROLE_USER,ROLE_AUTHENTICATED','ROLE_USER,ROLE_LOCKED') and u.userType='1' and u.mobileStatus='Active' and u.created <=?1")
	List<User> findAuthenticateUserss(Date endDate);

	@Query("select count(u) from User u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active'")
	long findAllAuthenticateUser();

	@Query("select u from User u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active'")
	List<User> findAllAuthenticateUserList();

	@Query("select u from User u where u.emailToken=?1")
	User findByEmailToken(String key);

	@Query("select u from User u where u.emailToken=?1 and u.emailStatus=?2")
	User findByEmailTokenAndStatus(String key, Status active);

	@Query("select u from User u where u.emailToken=?1 and u.emailStatus=?2 and u.mobileStatus=?3")
	User findByEmailTokenAndEmailStatusAndMobileStatus(String key, Status emailStatus, Status mobileStatus);

	@Query("select u from User u where u.mobileToken=?1 and u.mobileStatus=?2")
	User findByOTPAndStatus(String key, Status active);

	@Query("select u from User u where u.mobileToken=?1 and u.username=?2 and u.mobileStatus=?3")
	User findByMobileTokenAndStatus(String key, String mobile, Status active);

	@Query("select u from User u where u.mobileToken=?1 and u.username=?2 and u.mobileStatus=?3")
	User findByMobileTokenAndStatusForDirectPayment(String key, String mobile, Status active);


	@Query("select u from User u where u.emailToken=?1 and u.username=?2")
	User findByEmailTokenAndUsername(String key, String username);

	@Query("select u from User u where u.mobileToken=?1 and u.username=?2")
	User findByMobileTokenAndUsername(String key, String username);

	@Query("select u from User u where u.userDetail=?1")
	User findByUserDetails(UserDetail userDetail);

	@Query("select u from User u where u.accountDetail=?1")
	User findByAccountDetails(PQAccountDetail accountDetail);

	@Query("select p from User p")
	public List<User> findWithPageable(Pageable pageable);

	@Query("select p from User p where p.userType=2")
	public List<User> findAllMerchants();

	@Query("select p from User p")
	List<User> findWithVerifiedUsersPageable(Pageable pageable);

	@Query("select COUNT(p) from User p where p.userType= ?1")
	Long getTotalUsersCount(UserType userType);

	@Query("select count(u) from User u")
	Long getUserCount();

	@Modifying
	@Transactional
	@Query("update User c set c.authority=?1 where c.id =?2")
	int updateUserAuthority(String authority, long id);

	@Query("select u from User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED'")
	Page<User> findAll(Pageable page);

	//	select * from payqwikdb.user where authority='ROLE_USER,ROLE_AUTHENTICATED' AND emailStatus='Active' AND mobileStatus='Active';
	@Query("select u from User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.emailStatus='Active' AND u.mobileStatus='Active'")
	Page<User> getActiveUsers(Pageable pageable);

	//	@Query("select u from User u where u.mobileStatus='Inactive' ")
	@Query("select u from User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.emailStatus='Inactive' OR u.mobileStatus='Inactive'")
	Page<User> getInactiveUsers(Pageable pageable);

	@Query("select u from User u where u.authority='ROLE_USER,ROLE_BLOCKED'")
	Page<User> getBlockedUsers(Pageable pageable);

	@Query("select u from User u where u.authority='ROLE_USER,ROLE_LOCKED'")
	Page<User> getLockedUsers(Pageable pageable);

	@Query("select u from User u where u.id=?1")
	User findUserByUserId(long id);

	@Query("select u from User u where u.userType=?1")
	List<User> getTotalUsers(UserType userType);

	@Modifying
	@Transactional
	@Query("update User u set u.gcmId=?1 where u.username=?2")
	int updateGCMID(String gcmId, String username);

	@Query("select u from User u where u.created=?2")
	List<User> getUsersByDate(Date created);

	@Query("select t from User t where t.accountDetail=?1")
	User getUserFromAccountId(PQAccountDetail accountDetail);

	@Query("select u from User u where u.username LIKE ?1 ")
	List<User> getPossibilitiesByMobile(String mobile);

	@Query("select u.username from User u where u.userType=?1 and u.mobileStatus='Active' ")
	List<String> getAllActiveUsername(UserType userType);

	@Query("select t from User t where t.created BETWEEN ?1 AND ?2 ")
	List<User> findUsersByDate(Date from,Date to);

	@Query("select u from User u  where u.userType=1 and u.created BETWEEN ?1 AND ?2 ")
	List<User>findActiveUsersByDate(Date from, Date to);
	
	@Query("select u from AgentDetail u where u.panCardNo=?1")
	AgentDetail findByAgentPannumber(String Agent);

	@Query("select u from User u where u.authority='ROLE_LOCKED,ROLE_AUTHENTICATED' AND u.userType=3")
	List<User> findAllLockedAccounts();

	@Query("select u from User u where u.authority='ROLE_AGENT,ROLE_AUTHENTICATED' AND u.userType=5 order by u.created desc")
	List<User> findAllAgent();

	@Query("select u from User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.userDetail=?1")
	List<User> findUserByUserDetails(UserDetail userDetail);

	@Query("select SUM(u.accountDetail.balance) from User u where u.userType=1")
	Double findUsersWalletBalance();

	@Query("select u from User u where u.userDetail IN(select ud.id from UserDetail ud where ud.location=?1) and u.created BETWEEN ?2 AND ?3 ")
	List<User> findUserByLocationCode(LocationDetails locationDetails,Date StartDate,Date endDate);


	@Query("select u from User u where u.authority='ROLE_MERCHANT,ROLE_AUTHENTICATED' AND u.userType=2")
	List<User> findAllMerchantList();
	
	@Query("select u from User u where u.authority='ROLE_MERCHANT,ROLE_AUTHENTICATED' AND u.userType=2")
	List<User> getAllMerchantList();

	@Query("select u from User u where u.username=?1")
	User findByUser(String username);

	//	for single user notification

	@Query("select u.gcmId from User u where u.username=?2 AND u.userType=?1 AND u.emailStatus='Active' AND u.gcmId IS NOT NULL")
	String getGCMIdsByUserName(UserType userType,String username);

	@Query("select count(u) from User u where u.userType='1' and u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.created BETWEEN ?1 AND ?2")
	long findUserCountByMonth(Date parse, Date parse2);


	@Query("SELECT COUNT(u) FROM User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED'")
	long getCountUser();

	@Query("select u.userDetail.id from User u where u.accountDetail=?1")
	long getUser(PQAccountDetail pq);

	/*@Query("select u.userDetail from User u where u.=?1")
	UserDetail getUserDetail(User user);*/



	/*Admin Panel Backend Report*/

	@Query("SELECT DATE(u.created) FROM User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED'")
	List<Date> getBeginDateOfUsers();

	@Query("select u from User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND DATE(u.created) BETWEEN ?1 AND ?2")
	Page<User> findAllByDate(Date from, Date to, Pageable page);

	@Query("select u from User u where u.authority='ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED'")
	Page<User> findSuperAdminList(Pageable pageable);
	
	@Query("select u from User u where u.username=?1")
	User checkSuperAdminUserName(String userName);
	
	@Query("select u from User u where u.authority='ROLE_SUPER_ADMIN,ROLE_AUTHENTICATED' and u.username=?1")
	User findUserByNameContact(String userName);
	
	@Modifying
	@Transactional
	@Query("update User u set u.password=?2 where u.username =?1")
	void changeSuperPwd(String uname,String pass);
	
	@Modifying
	@Transactional
	@Query("update User u set u.authority='ROLE_SUPER_ADMIN,ROLE_BLOCKED' where u.username =?1")
	void blockSuperAdmin(String uname);
	
	
	@Query("select u from User u where u.authority LIKE '%ROLE_USER%' and u.userType='1' and u.mobileStatus='Active' and u.accountDetail !=125119 and date(u.created) BETWEEN ?1 AND ?2 ")
	List<User> findAllAuthenticateUsers(Date startDate,Date endDate);
	
	@Query("SELECT COUNT(u) FROM User u where u.userType='1' and u.created BETWEEN ?1 AND ?2")
	long getCountUser(Date startDate, Date endDate);
	
	@Query("SELECT COUNT(u) FROM User u where u.userType='1' and u.created BETWEEN ?1 AND ?2 and u.mobileStatus='Active'")
	long getCountActiveUser(Date startDate, Date endDate);
	
	@Query("SELECT count(u) FROM User u where YEAR(u.created) < ?1 and u.authority='ROLE_USER,ROLE_AUTHENTICATED'")
	 long getCountUseryear(int year);
	
	@Query("select u from User u where u.authority='ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED'")
	Page<User> findAdminList(Pageable pageable);
	
	@Query("select u from User u where u.authority='ROLE_SUPER_ADMIN,ROLE_BLOCKED'")
	Page<User> findSuperAdminBlockList(Pageable pageable);
	

	@Query("select u from User u where u.authority='ROLE_ADMINISTRATOR,ROLE_BLOCKED'")
	Page<User> findAdminBlockList(Pageable pageable);
	

	@Query("select u from User u where u.authority='ROLE_ADMINISTRATOR,ROLE_AUTHENTICATED' and u.username=?1")
	User findUserByNameContactb(String userName);
	

	@Modifying
	@Transactional
	@Query("update User u set u.authority='ROLE_ADMINISTRATOR,ROLE_BLOCKED' where u.username =?1")
	void blockSuperAdminb(String uname);
	

	@Query("select u from User u where u.userDetail.contactNo=?1")
	User findByContactNo(String cno);

	@Query("select u from User u where u.agentDetails=?1 and u.mobileStatus=?2")
	User findUserByAgentIdAndStatus(AgentDetail agentDetail, Status active);

	@Query("select u from User u where u.accountDetail=?1 and u.mobileStatus=?2")
	User findUserByAccountNumberStatus(PQAccountDetail accountDetail, Status active);

	
	@Query("select u from User u where u.authority = 'ROLE_USER,ROLE_AUTHENTICATED' and mobileStatus=?1")
	Page<User> findActiveUser(Status active,Pageable pageable);
	
	@Query("select u from User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND u.accountDetail=?1")
	List<User> findUserByAccount(PQAccountDetail detail);
	
	@Query("SELECT COUNT(u) FROM User u where u.authority='ROLE_USER,ROLE_AUTHENTICATED' and u.mobileStatus='Active'")
	Long getCountActiveUser();
	
	@Query("SELECT count(u) FROM User u WHERE u.userType='1' and u.mobileStatus='Active'and u.authority='ROLE_USER,ROLE_AUTHENTICATED' AND DATE(u.created) >= ?1 AND DATE(u.created) <= ?2")
	 Long findAllAuthenticateUserByDate(Date fromDate, Date toDate);
	
}



