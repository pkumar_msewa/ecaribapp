package com.payqwikapp.repositories;

import com.payqwikapp.entity.MTRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MTRequestLogRepository extends CrudRepository<MTRequestLog, Long>,
        PagingAndSortingRepository<MTRequestLog, Long>, JpaSpecificationExecutor<MTRequestLog> {

    @Query("select m from MTRequestLog m where m.transactionRefNo = ?1")
    MTRequestLog findByTransactionRefNo(String transactionRefNo);
}
