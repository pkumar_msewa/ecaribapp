package com.payqwikapp.repositories;

import com.payqwikapp.entity.BTransferRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BTransferRequestLogRepository extends CrudRepository<BTransferRequestLog, Long>,
        PagingAndSortingRepository<BTransferRequestLog, Long>, JpaSpecificationExecutor<BTransferRequestLog> {

    @Query("select l from BTransferRequestLog l where l.transactionRefNo = ?1")
    BTransferRequestLog findByTransactionRefNo(String transactionRefNo);

}
