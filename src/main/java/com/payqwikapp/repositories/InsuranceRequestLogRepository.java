package com.payqwikapp.repositories;

import com.payqwikapp.entity.InsuranceRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InsuranceRequestLogRepository extends CrudRepository<InsuranceRequestLog, Long>,
        PagingAndSortingRepository<InsuranceRequestLog, Long>, JpaSpecificationExecutor<InsuranceRequestLog> {


    @Query("select l from InsuranceRequestLog l where l.transactionRefNo = ?1")
    InsuranceRequestLog findByTransactionRefNo(String transactionRefNo);

}
