package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.TravellerDetails;
import com.payqwikapp.model.Status;


public interface TravellerDetailsRepository extends CrudRepository<TravellerDetails,Long>,JpaSpecificationExecutor<TravellerDetails>{

	@Query("select t from TravellerDetails t where t.busTicketId.status!=?1")
	List<TravellerDetails> getTicketDetails(Status status);
	
	@Query("select t from TravellerDetails t where t.busTicketId=?1 order by t.created desc")
	List<TravellerDetails> getTicketByBusId(BusTicket busTicket);
}
