package com.payqwikapp.repositories;

import com.payqwikapp.entity.LandlineRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LandlineRequestLogRepository extends CrudRepository<LandlineRequestLog, Long>,
        PagingAndSortingRepository<LandlineRequestLog, Long>, JpaSpecificationExecutor<LandlineRequestLog> {

    @Query("select l from LandlineRequestLog l where l.transactionRefNo = ?1")
    LandlineRequestLog findByTransactionRefNo(String transactionRefNo);

}
