package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.EventAccessToken;
import com.payqwikapp.entity.PQAccountDetail;

public interface EventAccessTokenRepository extends CrudRepository<EventAccessToken, Long>, 
				 PagingAndSortingRepository<EventAccessToken, Long>, JpaSpecificationExecutor<EventAccessToken> {
	
	@Query("select e from EventAccessToken e where e.accountDetail=?1")
	EventAccessToken findByAccountDetails(PQAccountDetail accountDetail);


}
