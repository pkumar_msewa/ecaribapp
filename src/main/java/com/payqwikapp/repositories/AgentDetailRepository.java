package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.AgentDetail;

public interface AgentDetailRepository  extends CrudRepository<AgentDetail, Long>,
PagingAndSortingRepository<AgentDetail, Long>, JpaSpecificationExecutor<AgentDetail>{
	
	@Query("select a from AgentDetail a where a.bankAccountNo=?1")
	AgentDetail findUserbyAccountNumber(String number);
	
	@Query("select a from AgentDetail a where a.panCardNo=?1")
	AgentDetail findUserByPanCardNo(String panCardNo);
	
	@Query("select s from AgentDetail s where s.id=?1")
	AgentDetail findById(Long id);

}
