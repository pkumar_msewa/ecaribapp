package com.payqwikapp.repositories;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TrainList;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;

public interface TKOrderDetailRepository extends CrudRepository<TKOrderDetail,Long>,JpaSpecificationExecutor<TKOrderDetail>{
	@Modifying
	@Transactional
	@Query("update TKOrderDetail c set c.orderStatus=?1,c.userOrderId=?2 ,c.txnStatus=?1 where c.transactionRefNo=?3")
	int updateTKOrderDetailsByTxnRefNo(Status orderStatus,long userOrderId,String transactionRefNo);
	
	/*@Query("select f from TKOrderDetail f where f.user=?1 and f.userOrderId>0 order by f.created")
	List<TKOrderDetail> findByUser(User user);*/
	
	@Query("select f from TKOrderDetail f where f.user=?1 order by f.created desc")
	List<TKOrderDetail> findByUser(User user);
	
	@Query("select b from TKOrderDetail b order by b.created desc")
	Page<TKOrderDetail> getByStatus(Pageable pageable);
	
	@Query("select b from TKOrderDetail b order by b.created desc")
	List<TKOrderDetail> getByStatus();
	
	@Query("select b from TKOrderDetail b where b.transactionRefNo=?1 order by b.created desc")
	TKOrderDetail getDetailsByTxnRefNo(String transactionRefNo);

	@Query("select b from TrainList b")
	List<TrainList> getTrainlist();

	@Query("select b from TKOrderDetail b where b.created between ?1 and ?2 order by b.created desc")
	List<TKOrderDetail> getDetailsBydate(Date from, Date to);
}

