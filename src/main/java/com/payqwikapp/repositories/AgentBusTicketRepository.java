package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.AgentBusTicket;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.model.Status;

public interface AgentBusTicketRepository extends CrudRepository<AgentBusTicket,Long>,JpaSpecificationExecutor<AgentBusTicket>{

	@Query("select b from AgentBusTicket b where b.transaction=?1")
	AgentBusTicket getByTransaction(PQTransaction transaction);
	
	@Query("select b from AgentBusTicket b order by b.created desc")
	List<AgentBusTicket> getByStatus();
	
	@Query("select b from AgentBusTicket b where b.status!=?1 and b.transaction.account=?2  order by b.created desc")
	List<AgentBusTicket> getByStatusAndAccount(Status status,PQAccountDetail accout);
	
	@Query("select b from AgentBusTicket b where b.emtTxnId=?1 order by b.created desc")
	AgentBusTicket getTicketByTxnId(String emtTxnId);
	
	@Query("select t from AgentBusTicket t where t.created BETWEEN ?1 AND ?2 order by t.created desc")
	List<AgentBusTicket> getBusTicketByDate(Date from, Date to);
	
	@Query("select b from AgentBusTicket b where b.seatHoldId=?1")
	AgentBusTicket getTicketBySeatHoldId(String seatHoldId);
	
	@Query("select b from AgentBusTicket b where b.tripId=?1")
	AgentBusTicket getTicketBytripId(String tripId);

}
