package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.UpiCustDetails;

public interface UpiCustDetailsRepository 
extends CrudRepository<UpiCustDetails, Long>, PagingAndSortingRepository<UpiCustDetails, Long>, JpaSpecificationExecutor<UpiCustDetails> {

}
