package com.payqwikapp.repositories;


import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.SessionLog;
import com.payqwikapp.entity.SharePointsLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SharePointsLogRepository extends CrudRepository<SharePointsLog, Long>, JpaSpecificationExecutor<SharePointsLog> {
    @Query("select s from SharePointsLog s")
    List<SharePointsLog> getAllRequests();

    @Query("select s from SharePointsLog s where s.account=?1")
    List<SharePointsLog> getRequestByAccount(PQAccountDetail account);

    @Query("select s from SharePointsLog s where s.transactionRefNo=?1")
    SharePointsLog getByTransactionRefNo(String transactionRefNo);

}

