package com.payqwikapp.repositories;

import com.payqwikapp.entity.AuthUpdateLog;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.RequestType;
import com.payqwikapp.model.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AuthUpdateLogRepository extends CrudRepository<AuthUpdateLog,Long>,PagingAndSortingRepository<AuthUpdateLog,Long>,JpaSpecificationExecutor<AuthUpdateLog>{

    @Query("select a from AuthUpdateLog a where a.requestType=?1")
    List<AuthUpdateLog> getAllByRequestType(RequestType request);

    @Query("select a from AuthUpdateLog a where a.requestType=?1 and a.status=?2")
    List<AuthUpdateLog> getAllByRequestType(RequestType request,Status status);

    @Query("select a from AuthUpdateLog a where a.id=?1")
    AuthUpdateLog getById(long id);

    @Query("select a from AuthUpdateLog a where a.user=?1 AND a.requestType=?2 AND a.status=?3")
    AuthUpdateLog getByUserAndRequest(User user, RequestType requestType,Status status);

}
