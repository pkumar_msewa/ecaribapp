package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.TreatCardDetails;
import com.payqwikapp.entity.User;

public interface TreatCardDetailsRepository extends CrudRepository<TreatCardDetails, String>, PagingAndSortingRepository<TreatCardDetails, String>, JpaSpecificationExecutor<TreatCardDetails> {

	@Query("select t from TreatCardDetails t where t.user=?1")
	TreatCardDetails findByUser(User user);
	
	
	@Query("select t from TreatCardDetails t order by created desc")
	Page<TreatCardDetails> findAllRegisterList(Pageable page);
	
	@Query("select t from TreatCardDetails t where date(t.created) BETWEEN ?1 AND ?2 order by created desc")
	List<TreatCardDetails> findAllRegisterListFiltered(Date start,Date end);

}
