package com.payqwikapp.repositories;

import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.User;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.PQService;

import java.util.ArrayList;
import java.util.List;

public interface PQServiceRepository extends CrudRepository<PQService, Long>, JpaSpecificationExecutor<PQService> {

	@Query("select u from PQService u where u.code=?1")
	PQService findServiceByCode(String code);
	
	@Query("select u from PQService u where u.operatorCode=?1")
	PQService findServiceByOperatorCode(String name);

	@Query("select u from PQService u where u.serviceType.id=?1")
	List<PQService> findServiceByServiceTypeID(long id);

	@Query("select s from PQService s where s.serviceType=?1")
	List<PQService> findByServiceType(PQServiceType serviceType);
	
	@Query("select s from PQService s where s.serviceType=?1")
	PQService findServiceType(PQServiceType serviceType);
	
	@Query("select s from PQService s where s.operatorCode=?1")
	PQService findByName(String opCode);
	
	@Query("select u from PQService u where u.code IN ('PPS','LMC','SMR','LMB','LMU')")
	List<PQService> findServicesByCodes();
	
	@Query("select u from PQService u where u.code=?1 and u.status='Active' ")
	PQService findActiveServiceByCode(String code);
	
	@Query("select u from PQService u where u.id=?1")
	PQService findServiceById(long id);

	@Query("select u from PQService u where u.id IN(?1)")
	List<PQService> getServiceByIds(ArrayList<Long> list);
	
	@Query("select u from PQService u order by DATE(u.created) DESC")
	List<PQService> findAllServices();
}
