package com.payqwikapp.repositories;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.HouseJoy;
import com.payqwikapp.entity.PQTransaction;


public interface HouseJoyRepository extends CrudRepository<HouseJoy,Long>,JpaSpecificationExecutor<HouseJoy>{

	@Query("select b from HouseJoy b where b.transaction=?1")
	HouseJoy getByTransaction(PQTransaction transaction);

	@Query("select t from HouseJoy t order by created desc")
	List<HouseJoy> getAllHouseJoyDetails();
	
	@Query("select t from HouseJoy t where t.created BETWEEN ?1 AND ?2 order by created desc")
	List<HouseJoy> getAllHouseJoyDetailsByDate(Date from,Date to);
}
