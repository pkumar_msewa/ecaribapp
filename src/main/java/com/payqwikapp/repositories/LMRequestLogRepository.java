package com.payqwikapp.repositories;

import com.payqwikapp.entity.LMRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LMRequestLogRepository extends CrudRepository<LMRequestLog, Long>,
        PagingAndSortingRepository<LMRequestLog, Long>, JpaSpecificationExecutor<LMRequestLog> {


    @Query("select l from LMRequestLog l where l.transactionRefNo = ?1")
    LMRequestLog findByTransactionRefNo(String transactionRefNo);

}
