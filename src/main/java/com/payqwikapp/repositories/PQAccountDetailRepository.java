package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.VBankAccountDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.User;

public interface PQAccountDetailRepository extends CrudRepository<PQAccountDetail, Long>,
		PagingAndSortingRepository<PQAccountDetail, Long>, JpaSpecificationExecutor<PQAccountDetail> {

	@Query("select u from PQAccountDetail u")
	List<PQAccountDetail> findAll();

	@Query("select u from PQAccountDetail u where u.vBankAccount=?1")
	PQAccountDetail findUsingVBankAccount(VBankAccountDetail vBankAccountDetail);
	
	@Query("select u from PQAccountDetail u where u.id=?1")
	PQAccountDetail findAccount(long id);

	@Query("select SUM(u.balance) from PQAccountDetail u")
	double getTotalBalance();

	@Query("select u.balance from PQAccountDetail u where u.id = ?1")
	double getUserWalletBalance(long id);

	@Modifying
	@Transactional
	@Query("update PQAccountDetail c set c.balance=?1 where c.id =?2")
	int updateUserBalance(double balance, long accountDetailId);

	@Query("select u from PQAccountDetail u")
	Page<PQAccountDetail> findAll(Pageable page);


	@Modifying
	@Transactional
	@Query("update PQAccountDetail c set c.points=?1 where c.id =?2")
	int updateUserPoints(long points, long id);


	@Modifying
	@Transactional
	@Query("update PQAccountDetail c set c.balance=?1 where c.id =?2")
	int updateUserBalance(String authority, long id);

	@Modifying
	@Transactional
	@Query("update PQAccountDetail a set a.points= a.points+?1 where a.accountNumber=?2")
	int addUserPoints(long points, long accountNumber);

	@Query("select u.balance from PQAccountDetail u where u.id=?1")
	double getBalanceByUserAccount(long accountDetailId);
	
	/*Admin Panel Report*/
	@Query("select u from PQAccountDetail u ")
	List<PQAccountDetail> findAllAccounts();
	
	@Query("select u.balance from PQAccountDetail u where DATE(u.created)<=?1 and (accountType=?2 or accountType=?3)")
	List<PQAccountDetail> getAllAccountTillDate(Date date,PQAccountType accType,PQAccountType accountType);

	@Query("select COUNT(u) from PQAccountDetail u where DATE(u.created)=?1 and (accountType=?2 or accountType=?3)")
	long getTotalCreated(Date date,PQAccountType accType,PQAccountType accountType);
	
	@Query("select u from PQAccountDetail u where u.accountNumber=?1")
	PQAccountDetail findUserbyAccountNumber(long number);

}
