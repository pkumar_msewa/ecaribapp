package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.SavaariTripDetails;

public interface SavaariTripDetailsRepository extends CrudRepository<SavaariTripDetails, Long>,
PagingAndSortingRepository<SavaariTripDetails, Long>, JpaSpecificationExecutor<SavaariTripDetails> {
	
}
