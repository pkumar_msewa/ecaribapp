package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PassengerDetails;

public interface PassengerDetailsRepository extends CrudRepository<PassengerDetails, Long>,
PagingAndSortingRepository<PassengerDetails, Long>, JpaSpecificationExecutor<PassengerDetails> {
	

}
