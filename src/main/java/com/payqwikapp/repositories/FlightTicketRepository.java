package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;

public interface FlightTicketRepository extends CrudRepository<FlightTicket,Long>,JpaSpecificationExecutor<FlightTicket>{

	@Query("select f from FlightTicket f where f.transaction=?1")
	FlightTicket getByTransaction(PQTransaction transaction);

	@Query("select t from FlightTicket t where t.flightStatus!=?1 and t.user=?2  order by created desc")
	List<FlightTicket> getByStatusAndUser(Status status,User user);
	
	@Query("select t from FlightTicket t order by created desc")
	List<FlightTicket> getAllTicket();
	
	@Query("select t from FlightTicket t where t.created BETWEEN ?1 AND ?2")
	List<FlightTicket> getAllTicketByDate(Date from,Date to);
	
	@Query("select f from FlightAirLineList f where f.cityCode=?1")
	FlightAirLineList getCityByCode(String code);
	
	@Query("select t from FlightTicket t order by created desc")
	List<FlightTicket> getAllAgentTicket();
}
