package com.payqwikapp.repositories;

import com.payqwikapp.entity.MKycPoa;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MKycPoaRepository extends CrudRepository<MKycPoa,Long>,PagingAndSortingRepository<MKycPoa,Long>,JpaSpecificationExecutor<MKycPoa> {


}
