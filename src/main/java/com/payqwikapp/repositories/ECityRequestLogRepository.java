package com.payqwikapp.repositories;

import com.payqwikapp.entity.ECityRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ECityRequestLogRepository extends CrudRepository<ECityRequestLog, Long>,
        PagingAndSortingRepository<ECityRequestLog, Long>, JpaSpecificationExecutor<ECityRequestLog> {

    @Query("select l from ECityRequestLog l where l.transactionRefNo = ?1")
    ECityRequestLog findByTransactionRefNo(String transactionRefNo);

}
