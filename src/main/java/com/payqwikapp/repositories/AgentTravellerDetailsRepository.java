package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.AgentBusTicket;
import com.payqwikapp.entity.AgentTravellerDetails;
import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.TravellerDetails;
import com.payqwikapp.model.Status;

public interface AgentTravellerDetailsRepository extends CrudRepository<AgentTravellerDetails,Long>,JpaSpecificationExecutor<AgentTravellerDetails>{

	@Query("select t from AgentTravellerDetails t where t.busTicketId.status!=?1")
	List<AgentTravellerDetails> getTicketDetails(Status status);
	
	@Query("select t from AgentTravellerDetails t where t.busTicketId=?1 order by t.created desc")
	List<AgentTravellerDetails> getTicketByBusId(AgentBusTicket busTicket);

}
