package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.PPIFiles;
import com.payqwikapp.model.Status;

public interface PPIFilesRepository extends CrudRepository<PPIFiles, Long>, PagingAndSortingRepository<PPIFiles,Long>,JpaSpecificationExecutor<PPIFiles> {

    @Query("select u from PPIFiles u where u.name=?1")
    PPIFiles findByName(String name);

    @Query("select u from PPIFiles u where DATE(u.created)= current_date and status=?1")
    List<PPIFiles> getPPIFiles(Status status);
    
}
