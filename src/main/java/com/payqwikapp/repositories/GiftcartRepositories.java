package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.GciCart;
import com.payqwikapp.entity.User;



public interface GiftcartRepositories

	extends CrudRepository<GciCart, String>, PagingAndSortingRepository<GciCart, String>, JpaSpecificationExecutor<GciCart> {


	  @Query("select u from GciCart u where u.user=?1")
	    List<GciCart> findByUserCart(User userid);
	  
	  @Query("select u from GciCart u where u.productid=?1")
	    GciCart findByUserCartTest(String dd);
}
