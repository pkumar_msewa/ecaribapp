package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.PQTransaction;


public interface FlightDetailsRepository extends CrudRepository<FlightDetails,Long>,JpaSpecificationExecutor<FlightDetails>{

	@Query("select f from FlightDetails f where f.transaction=?1 order by f.created")
	FlightDetails getByTransaction(PQTransaction transaction);
	
	@Query("select f from FlightDetails f where f.transaction=?1 order by f.created desc")
	FlightDetails getByMerchantRefNo(String merchantRefNo);
}
