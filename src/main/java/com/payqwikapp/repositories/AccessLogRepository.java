package com.payqwikapp.repositories;

import com.payqwikapp.entity.AccessDetails;
import com.payqwikapp.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AccessLogRepository extends CrudRepository<AccessDetails,Long>,JpaSpecificationExecutor<AccessDetails>{

    @Query("select a from AccessDetails a where a.user=?1")
    AccessDetails getByUser(User u);

}
