package com.payqwikapp.repositories;

import com.payqwikapp.entity.PartnerDetail;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PartnerDetailRepository extends CrudRepository<PartnerDetail,Long>,JpaSpecificationExecutor<PartnerDetail>{

    @Query("select p from PartnerDetail p where p.apiKey=?1")
    PartnerDetail findByKey(String apiKey);
    
    @Query("select p from PartnerDetail p where p.name=?1")
    PartnerDetail findByPartnerName(String partnerName);

}
