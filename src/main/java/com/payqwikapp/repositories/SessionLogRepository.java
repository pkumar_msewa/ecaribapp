package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.SessionLog;


public interface SessionLogRepository extends CrudRepository<SessionLog, Long>, JpaSpecificationExecutor<SessionLog> {

	@Query("select s from SessionLog s where s.sessionId=?1")
	SessionLog findBySessionId(String sessionId);

	@Query("select s from SessionLog s where s.userId=?1 order by created desc")
	List<SessionLog> getUserLogById(long userId);

	
}
