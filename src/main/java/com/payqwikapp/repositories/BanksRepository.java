package com.payqwikapp.repositories;


import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.Banks;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BanksRepository extends CrudRepository<Banks, Long>, PagingAndSortingRepository<Banks,Long>,JpaSpecificationExecutor<Banks> {

    @Query("select u from Banks u where u.code=?1")
    Banks findByCode(String code);

    @Query("select u from Banks u")
	List<Banks>getAllList();

    @Query("select u from Banks u where u.name=?1")
	Banks getBankCode(String bankName);

}
