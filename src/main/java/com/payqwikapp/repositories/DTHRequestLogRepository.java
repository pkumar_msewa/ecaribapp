package com.payqwikapp.repositories;

import com.payqwikapp.entity.DTHRequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DTHRequestLogRepository extends CrudRepository<DTHRequestLog, Long>,
        PagingAndSortingRepository<DTHRequestLog, Long>, JpaSpecificationExecutor<DTHRequestLog> {

    @Query("select l from DTHRequestLog l where l.transactionRefNo = ?1")
    DTHRequestLog findByTransactionRefNo(String transactionRefNo);

}
