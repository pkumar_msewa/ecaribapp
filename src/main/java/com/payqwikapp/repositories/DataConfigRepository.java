package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.model.Status;

public interface DataConfigRepository extends CrudRepository<DataConfig,Long>,JpaSpecificationExecutor<DataConfig>{

	
	@Query("select a from DataConfig a where a.configType=?1 and a.status=?2")
	public DataConfig getByConfigType(String configType,Status status);
	
	
	
	@Query("select a from DataConfig a")
	public List<DataConfig> getAllDataConfig();
	
	

	@Query("select u from DataConfig u where u.code=?1")
	DataConfig findServiceByCode(String name);
	
	
	@Query("select a from DataConfig a where a.status=?1")
	public DataConfig getByStatus(Status status);
	
}
