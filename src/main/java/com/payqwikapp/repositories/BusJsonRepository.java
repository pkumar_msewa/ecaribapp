package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.BusResponseOfMdex;

public interface BusJsonRepository extends CrudRepository<BusResponseOfMdex,Long> ,PagingAndSortingRepository<BusResponseOfMdex,Long>,JpaSpecificationExecutor<BusResponseOfMdex>
{
	
	@Query("select b from BusResponseOfMdex b where b.emtTxnId=?1")
	BusResponseOfMdex getTxnByEmtTxnId(String emtTxnId);
	
	@Query("select b from BusResponseOfMdex b where b.seatHoldId=?1")
	BusResponseOfMdex getTxnBySeatHoldId(String seatHoldId);
	
}
