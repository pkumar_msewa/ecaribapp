package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.SecurityQuestion;
import com.payqwikapp.entity.TelcoCircle;

public interface SecurityQuestionRepository extends CrudRepository<SecurityQuestion, Long>, JpaSpecificationExecutor<SecurityQuestion>  {

	@Query("select s from SecurityQuestion s where s.code=?1")
	SecurityQuestion findSecurityQuestionByCode(String code);
	
	@Query("select s from SecurityQuestion s")
	List<SecurityQuestion> findAllQuestions();
	

}
