package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.entity.AgentFlightTicket;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;

public interface AgentFlightTicketRepository extends CrudRepository<AgentFlightTicket,Long>,JpaSpecificationExecutor<AgentFlightTicket>{
	@Query("select f from AgentFlightTicket f where f.transaction=?1")
	AgentFlightTicket getByTransaction(PQTransaction transaction);

	@Query("select t from AgentFlightTicket t where t.flightStatus!=?1 and t.user=?2  order by created desc")
	List<AgentFlightTicket> getByStatusAndUser(Status status,User user);
	
	@Query("select t from AgentFlightTicket t order by created desc")
	List<AgentFlightTicket> getAllTicket();
	
	@Query("select t from AgentFlightTicket t where t.created BETWEEN ?1 AND ?2 order by created desc")
	List<AgentFlightTicket> getAllTicketByDate(Date from,Date to);
	
	@Query("select f from FlightAirLineList f where f.cityCode=?1")
	FlightAirLineList getCityByCode(String code);
	
	@Query("select t from AgentFlightTicket t order by created desc")
	List<AgentFlightTicket> getAllAgentTicket();
}
