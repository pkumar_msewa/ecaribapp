package com.payqwikapp.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.WalletDetails;

public interface WalletDetailRepository extends CrudRepository<WalletDetails, Long>,
		PagingAndSortingRepository<WalletDetails, Long>, JpaSpecificationExecutor<WalletDetails> {
	
	@Query("SELECT u FROM WalletDetails u where DATE(u.dot) =?1")
	WalletDetails getWalletDetailsByDate(Date date);
	
	@Query("SELECT u FROM WalletDetails u")
	List<WalletDetails> getAllWalletDetails();

}
