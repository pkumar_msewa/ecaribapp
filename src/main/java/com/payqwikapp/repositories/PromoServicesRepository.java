package com.payqwikapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PromoCode;
import com.payqwikapp.entity.PromoServices;

public interface PromoServicesRepository extends CrudRepository<PromoServices, Long>, PagingAndSortingRepository<PromoServices,Long>,JpaSpecificationExecutor<PromoServices> {

    @Query("SELECT u FROM PromoServices u where u.promoCode=?1 AND u.service = ?2")
    PromoServices getByPromoAndService(PromoCode promoCode, PQService service);


    @Query("SELECT u FROM PromoServices u where u.promoCode=?1")
    List<PromoServices> getByPromoCode(PromoCode promoCode);

//
//    @Modifying
//    @Transactional
//    @Query("DELETE FROM PromoServices p WHERE p.promoCode=?1")
//	void deleteServices(PromoCode savedCode);


}
