package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.YTVKeys;

public interface YtvKeysRepository extends CrudRepository<YTVKeys, Long>, JpaSpecificationExecutor<YTVKeys>{
	
	@Query("select s from YTVKeys s where s.APIKey=?1")
	YTVKeys findByKey(String key);

}
