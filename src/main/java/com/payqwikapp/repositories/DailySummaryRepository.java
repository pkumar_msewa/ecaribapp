package com.payqwikapp.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.payqwikapp.entity.DailySummaryDetails;

public interface DailySummaryRepository extends CrudRepository<DailySummaryDetails, Long>,
		PagingAndSortingRepository<DailySummaryDetails, Long>, JpaSpecificationExecutor<DailySummaryDetails> {
	
	@Query("SELECT d FROM DailySummaryDetails d WHERE DATE(d.date)= DATE(?1)")
	 DailySummaryDetails getLastDetails(Date date);

}
