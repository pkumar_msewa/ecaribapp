package com.payqwikapp.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.TrainList;

public interface TrainListRepository extends CrudRepository<TrainList,Long>,JpaSpecificationExecutor<TrainList> {

}
