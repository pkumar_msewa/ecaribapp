package com.payqwikapp.entity;

import javax.persistence.Entity;

import javax.persistence.FetchType;

import javax.persistence.OneToOne;

@Entity
public class AGcommission extends AbstractEntity<Long> {
	private static final long serialVersionUID = 1L;

	private String type;
	private boolean fixed;
	private String value;
	private double minAmount;
	private double maxAmount;

	@OneToOne(fetch = FetchType.EAGER)
	private PQService ag_Service;

	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail ag_AccountDetail;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public double getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(double minAmount) {
		this.minAmount = minAmount;
	}

	public double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public PQService getAg_Service() {
		return ag_Service;
	}

	public void setAg_Service(PQService ag_Service) {
		this.ag_Service = ag_Service;
	}

	public PQAccountDetail getAg_AccountDetail() {
		return ag_AccountDetail;
	}

	public void setAg_AccountDetail(PQAccountDetail ag_AccountDetail) {
		this.ag_AccountDetail = ag_AccountDetail;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
