package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;

@Entity
public class AgServiceType extends AbstractEntity<Long> {
	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
	private PQServiceType pQserviceType;
	
	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail pqaccountdetail;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	public PQServiceType getpQserviceType() {
		return pQserviceType;
	}

	public void setpQserviceType(PQServiceType pQserviceType) {
		this.pQserviceType = pQserviceType;
	}

	public PQAccountDetail getPqaccountdetail() {
		return pqaccountdetail;
	}

	public void setPqaccountdetail(PQAccountDetail pqaccountdetail) {
		this.pqaccountdetail = pqaccountdetail;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
