package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.payqwikapp.model.Status;

@Entity
public class MerchantRefundRequest extends AbstractEntity<Long> {

	private static final long serialVersionUID = 7184799860853214548L;

	private long orderId;
	
	private String username;
	
	private double amount;
	
	private String trnasactionRefNo;
	
	private double refundAmount;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getTrnasactionRefNo() {
		return trnasactionRefNo;
	}

	public void setTrnasactionRefNo(String trnasactionRefNo) {
		this.trnasactionRefNo = trnasactionRefNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(double refundAmount) {
		this.refundAmount = refundAmount;
	}
	
}
