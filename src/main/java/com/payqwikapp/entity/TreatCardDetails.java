package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class TreatCardDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private long membershipCode;
	
	private Date membershipCodeExpire;
	
	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	public long getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(long membershipCode) {
		this.membershipCode = membershipCode;
	}

	public Date getMembershipCodeExpire() {
		return membershipCodeExpire;
	}

	public void setMembershipCodeExpire(Date membershipCodeExpire) {
		this.membershipCodeExpire = membershipCodeExpire;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
