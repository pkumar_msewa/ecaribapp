package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;

@Entity
public class AGService extends AbstractEntity<Long> {
	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
	private PQService pqservice;
	
	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail pqaccountdetail;
	
	@OneToOne(fetch = FetchType.EAGER)
	private AgServiceType agservice;
	
	@Enumerated(EnumType.STRING)
	private Status status;

	public PQService getPqservice() {
		return pqservice;
	}

	public void setPqservice(PQService pqservice) {
		this.pqservice = pqservice;
	}

	public PQAccountDetail getPqaccountdetail() {
		return pqaccountdetail;
	}

	public void setPqaccountdetail(PQAccountDetail pqaccountdetail) {
		this.pqaccountdetail = pqaccountdetail;
	}

	public AgServiceType getAgservice() {
		return agservice;
	}

	public void setAgservice(AgServiceType agservice) {
		this.agservice = agservice;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

