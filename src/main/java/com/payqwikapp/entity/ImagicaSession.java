package com.payqwikapp.entity;

import com.payqwikapp.model.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ImagicaSession extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column(unique = true,nullable = false,length = 50)
	private String username;

	@Column(nullable = false)
	private String password;

	@Enumerated(EnumType.STRING)
	private Status status =  Status.Inactive;

	@Column
	private String sessionName;

	@Column
	private String token;

	@Column
	private String sessionId;


	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date loginTime;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
}