package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.payqwikapp.model.Status;

@Entity
public class ServiceStatus extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Column(nullable = false, unique=false)
	private String name;
	
	@Enumerated(EnumType.STRING)
	private Status status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}


	
	
}
