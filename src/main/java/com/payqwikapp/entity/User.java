package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;

@Entity
public class User extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	
	@Column(unique = true, nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private UserType userType;

	@Column(nullable = false)
	private String authority;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status emailStatus;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status mobileStatus;

	@Temporal(TemporalType.TIMESTAMP)
	private Date otpGenerationTime;

	@OneToOne(fetch = FetchType.EAGER)
	@NotFound( action = NotFoundAction.IGNORE)
	private UserDetail userDetail;

	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail accountDetail;
	
	@OneToOne(fetch = FetchType.EAGER)
	private MerchantDetails merchantDetails;
	
	@OneToOne(fetch = FetchType.EAGER)
	private AgentDetail agentDetails;

	@Column
	private String emailToken;

	@Column
	private String mobileToken;
	
	@Lob
	private String gcmId;
	
	@Lob
	private String qrCode;
	
	private String osName;
	
	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getOtpGenerationTime() {
		return otpGenerationTime;
	}

	public void setOtpGenerationTime(Date otpGenerationTime) {
		this.otpGenerationTime = otpGenerationTime;
	}

	public String getGcmId() {
		return gcmId;
	}
	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}
	public String getUsername() {
		return username;
	}
	public String getEmailToken() {
		return emailToken;
	}
	public void setEmailToken(String emailToken) {
		this.emailToken = emailToken;
	}
	public String getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserType getUserType() {
		return userType;
	}
	public void setUserType(UserType userType) {
		this.userType = userType;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public Status getEmailStatus() {
		return emailStatus;
	}
	public void setEmailStatus(Status emailStatus) {
		this.emailStatus = emailStatus;
	}
	public Status getMobileStatus() {
		return mobileStatus;
	}
	public void setMobileStatus(Status mobileStatus) {
		this.mobileStatus = mobileStatus;
	}
	public UserDetail getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}
	public PQAccountDetail getAccountDetail() {
		return accountDetail;
	}
	public void setAccountDetail(PQAccountDetail accountDetail) {
		this.accountDetail = accountDetail;
	}
	public MerchantDetails getMerchantDetails() {
		return merchantDetails;
	}
	public void setMerchantDetails(MerchantDetails merchantDetails) {
		this.merchantDetails = merchantDetails;
	}
	public AgentDetail getAgentDetails() {
		return agentDetails;
	}

	public void setAgentDetails(AgentDetail agentDetails) {
		this.agentDetails = agentDetails;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", userType=" + userType + ", authority="
				+ authority + ", emailStatus=" + emailStatus + ", mobileStatus=" + mobileStatus + ", userDetail="
				+ userDetail + ", accountDetail=" + accountDetail + ", merchantDetails=" + merchantDetails
				+ ", emailToken=" + emailToken + ", mobileToken=" + mobileToken + ", gcmId=" + gcmId + "]";
	}
	
}
