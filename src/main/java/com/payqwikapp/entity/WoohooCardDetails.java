package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class WoohooCardDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private String emailId;
	private String contanctNo;
	private String orderId;
	private String status;
	private String transactionRefNo;
	private String cardName;
	private String expiry_date;
	private String cardnumber;
	private String card_price;
	private String pin_or_url;
	private String retrivalRefNo;
	@OneToOne(fetch = FetchType.EAGER)
	private User user;
	@OneToOne(fetch = FetchType.EAGER)
	private PQTransaction transaction;

	public String getContanctNo() {
		return contanctNo;
	}

	public void setContanctNo(String contanctNo) {
		this.contanctNo = contanctNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public String getCard_price() {
		return card_price;
	}

	public void setCard_price(String card_price) {
		this.card_price = card_price;
	}

	public String getPin_or_url() {
		return pin_or_url;
	}

	public void setPin_or_url(String pin_or_url) {
		this.pin_or_url = pin_or_url;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}

	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}

	public PQTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(PQTransaction transaction) {
		this.transaction = transaction;
	}

}