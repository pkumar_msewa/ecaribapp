package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GiftCardRequestLog extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 1L;

	private String senderMobileNo;

	private String receiverMobileNo;

	private String brandName;
	
	private String brandNumber;
	
	private String amount;

	@Column(unique = true, nullable = false)
	private String transactionRefNo;
	
	private String retrivalRefNo;
	

	public String getBrandNumber() {
		return brandNumber;
	}

	public void setBrandNumber(String brandNumber) {
		this.brandNumber = brandNumber;
	}

	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}

	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}

	public String getSenderMobileNo() {
		return senderMobileNo;
	}

	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}

	public String getReceiverMobileNo() {
		return receiverMobileNo;
	}

	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

}
