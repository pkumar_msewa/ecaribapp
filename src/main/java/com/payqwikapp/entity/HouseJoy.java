package com.payqwikapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payqwikapp.model.BusType;
import com.payqwikapp.model.TripType;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
public class HouseJoy extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Column
	private String jobId;

	@Column
	private double amount;

	@Column(nullable = false)
	private String serviceName;
	
	@Column
	private String customerName;

	@Column
	private String address;
	
	@Column
	private String date;
	
	@Column
	private String mobile;
	
	@Column
	private String time;
	
	@Column
	private String paymentType;
	
	@Column
	private String pqtxnRefNo;
	
	@Column
	private String hjtxnRefNo;
	
	@OneToOne
	@JsonIgnore
	private PQTransaction transaction;

	@OneToOne
	private User user;

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public PQTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(PQTransaction transaction) {
		this.transaction = transaction;
	}

	public String getPqtxnRefNo() {
		return pqtxnRefNo;
	}

	public void setPqtxnRefNo(String pqtxnRefNo) {
		this.pqtxnRefNo = pqtxnRefNo;
	}

	public String getHjtxnRefNo() {
		return hjtxnRefNo;
	}

	public void setHjtxnRefNo(String hjtxnRefNo) {
		this.hjtxnRefNo = hjtxnRefNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
