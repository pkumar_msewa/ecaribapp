package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class AutoReconWeeklyReport extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String week;
	private Date fromDate;
	private Date toDate;
	private String periodInDays;
	private String customers;
	private String percent;
	private String totalTxns;
	private double totalAmount;
	private String avgRevenue;
	private String percentTxns;
	private String txnPerCustomer;
	private String activeUsers;

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getPeriodInDays() {
		return periodInDays;
	}

	public void setPeriodInDays(String periodInDays) {
		this.periodInDays = periodInDays;
	}

	public String getCustomers() {
		return customers;
	}

	public void setCustomers(String customers) {
		this.customers = customers;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getTotalTxns() {
		return totalTxns;
	}

	public void setTotalTxns(String totalTxns) {
		this.totalTxns = totalTxns;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAvgRevenue() {
		return avgRevenue;
	}

	public void setAvgRevenue(String avgRevenue) {
		this.avgRevenue = avgRevenue;
	}

	public String getPercentTxns() {
		return percentTxns;
	}

	public void setPercentTxns(String percentTxns) {
		this.percentTxns = percentTxns;
	}

	public String getTxnPerCustomer() {
		return txnPerCustomer;
	}

	public void setTxnPerCustomer(String txnPerCustomer) {
		this.txnPerCustomer = txnPerCustomer;
	}

	public String getActiveUsers() {
		return activeUsers;
	}

	public void setActiveUsers(String activeUsers) {
		this.activeUsers = activeUsers;
	}
}
