package com.payqwikapp.entity;

import com.payqwikapp.model.UIDType;

import javax.persistence.*;

@Entity
public class MKycDetails extends AbstractEntity<Long>{


    @Enumerated(EnumType.STRING)
    private UIDType type;

    @Column(unique = true)
    private String uidNumber;

    @OneToOne(fetch = FetchType.EAGER)
    private User merchant;

    @OneToOne(fetch = FetchType.EAGER)
    private MKycPoi poi;

    @OneToOne(fetch = FetchType.EAGER)
    private MKycPoa poa;

    public UIDType getType() {
        return type;
    }

    public void setType(UIDType type) {
        this.type = type;
    }

    public String getUidNumber() {
        return uidNumber;
    }

    public void setUidNumber(String uidNumber) {
        this.uidNumber = uidNumber;
    }

    public User getMerchant() {
        return merchant;
    }

    public void setMerchant(User merchant) {
        this.merchant = merchant;
    }

    public MKycPoi getPoi() {
        return poi;
    }

    public void setPoi(MKycPoi poi) {
        this.poi = poi;
    }

    public MKycPoa getPoa() {
        return poa;
    }

    public void setPoa(MKycPoa poa) {
        this.poa = poa;
    }
}
