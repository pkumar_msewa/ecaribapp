package com.payqwikapp.entity;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ECityRequestLog extends AbstractEntity<Long>{

    private String amount;

    private String serviceProvider;

    private String cycleNumber;

    private String accountNumber;

    private String billingUnit;

    private String processingCycle;

    private String cityName;

    @Column(unique = true,nullable = false)
    private String transactionRefNo;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getCycleNumber() {
        return cycleNumber;
    }

    public void setCycleNumber(String cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBillingUnit() {
        return billingUnit;
    }

    public void setBillingUnit(String billingUnit) {
        this.billingUnit = billingUnit;
    }

    public String getProcessingCycle() {
        return processingCycle;
    }

    public void setProcessingCycle(String processingCycle) {
        this.processingCycle = processingCycle;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }
}
