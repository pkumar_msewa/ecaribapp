package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class YesBankPoolBal extends AbstractEntity<Long> {

	private static final long serialVersionUID = -9163185658863000713L;

	private double balance;
	
	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
//	@Basic
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date createdBal;
	
	
}
