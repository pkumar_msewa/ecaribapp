package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class AUserSignUp extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
	private User agent_id;

	@OneToOne(fetch = FetchType.EAGER)
	private User user_id;

	public User getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(User agent_id) {
		this.agent_id = agent_id;
	}

	public User getUser_id() {
		return user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
