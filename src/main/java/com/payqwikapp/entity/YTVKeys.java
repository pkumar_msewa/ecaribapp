package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class YTVKeys extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	@Column(unique = true, nullable = false)
	private String APIKey;
	
	@Column(unique = true, nullable = false)
	private String APISecret;

	public String getAPIKey() {
		return APIKey;
	}

	public void setAPIKey(String aPIKey) {
		APIKey = aPIKey;
	}

	public String getAPISecret() {
		return APISecret;
	}

	public void setAPISecret(String aPISecret) {
		APISecret = aPISecret;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
