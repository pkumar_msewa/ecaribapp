package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.payqwikapp.model.Status;

@Entity
public class Offers extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String offers;

	@Enumerated(EnumType.STRING)
	private Status activeOffer;
	
	private String services;
	
	
	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public String getOffers() {
		return offers;
	}

	public void setOffers(String offers) {
		this.offers = offers;
	}

	public Status getActiveOffer() {
		return activeOffer;
	}

	public void setActiveOffer(Status activeOffer) {
		this.activeOffer = activeOffer;
	}

}
