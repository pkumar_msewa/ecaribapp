package com.payqwikapp.entity;

import com.payqwikapp.model.RequestType;
import com.payqwikapp.model.Status;

import javax.persistence.*;

@Entity
public class AuthUpdateLog extends AbstractEntity<Long>{

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RequestType requestType;

    @Lob
    @Column(nullable = true)
    private String reason;

    @Column(nullable = false)
    private Status status;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
