package com.payqwikapp.entity;

import javax.persistence.*;

@Entity
public class MerchantDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column
	private String merchantName;

	@Column(unique=true, nullable=false)
	private String panCardNo;
	
	@Column(unique=false, nullable=true)
	private String aadharNo;
	
	@Column(nullable=true)
	private String bankName;
	
	@Column(nullable=true)
	private String bankAccountName;
	
	@Column(nullable=true)
	private String bankAccountNo;
	
	@Column(nullable=true)
	private String branchName;
	
	@Column(nullable=true)
	private String ifscCode;
	
	@Column(nullable=true)
	private String mVisaId;
	
	@Column(nullable=true)
	private String entityId;
	
	@Column(nullable=true)
	private String customerId;
	
	@Column(nullable=true)
	private String qrCode;

	@Column(nullable=true)
	private String bankLocation;

	@Column(nullable=true)
	private String locationCode;

	@Lob
	@Column(nullable=true)
	private String address;


	@Column(nullable=true)
	private String rupayId;
	


	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getBankLocation() {
		return bankLocation;
	}

	public void setBankLocation(String bankLocation) {
		this.bankLocation = bankLocation;
	}

	public String getPanCardNo() {
		return panCardNo;
	}

	public void setPanCardNo(String panCardNo) {
		this.panCardNo = panCardNo;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getmVisaId() {
		return mVisaId;
	}

	public void setmVisaId(String mVisaId) {
		this.mVisaId = mVisaId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getRupayId() {
		return rupayId;
	}

	public void setRupayId(String rupayId) {
		this.rupayId = rupayId;
	}

}
