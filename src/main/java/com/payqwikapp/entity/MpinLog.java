package com.payqwikapp.entity;

import javax.persistence.*;
import com.payqwikapp.model.Status;

@Entity
public class MpinLog extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@Column
	private String serverIpAddress;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public void setServerIpAddress(String serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	
}
