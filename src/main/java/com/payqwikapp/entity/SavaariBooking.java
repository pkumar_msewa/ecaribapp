package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import com.payqwikapp.model.Status;

@Entity
public class SavaariBooking extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@OneToOne
	private SavaariAuth auth;

	private String state;

	private String carType;

	private String carName;

	@Enumerated(EnumType.STRING)
	private Status status;

	private String amount;
	
	@Column(unique = true)
	private String transctionRefNo;

	private String description;
	
	private String bookingId;
	
	private String reservationId;

	@OneToOne
	private SavaariFareDetails fare;

	@OneToOne
	private SavaariTripDetails trip;

	@OneToOne
	private SavaariUserDetails savaariDetails;
	
	@ManyToOne(optional = false)
	private User userDetails;
	
	@ManyToOne(optional = false)
	private PQAccountDetail account;
	
	
	private Date pickupDateTime;
	
	
	public Date getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(Date pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	public SavaariUserDetails getSavaariDetails() {
		return savaariDetails;
	}

	public void setSavaariDetails(SavaariUserDetails savaariDetails) {
		this.savaariDetails = savaariDetails;
	}

	public User getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(User userDetails) {
		this.userDetails = userDetails;
	}

	public String getTransctionRefNo() {
		return transctionRefNo;
	}

	public void setTransctionRefNo(String transctionRefNo) {
		this.transctionRefNo = transctionRefNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public SavaariAuth getAuth() {
		return auth;
	}

	public void setAuth(SavaariAuth auth) {
		this.auth = auth;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public SavaariFareDetails getFare() {
		return fare;
	}

	public void setFare(SavaariFareDetails fare) {
		this.fare = fare;
	}

	public SavaariTripDetails getTrip() {
		return trip;
	}

	public void setTrip(SavaariTripDetails trip) {
		this.trip = trip;
	}

	public PQAccountDetail getAccount() {
		return account;
	}

	public void setAccount(PQAccountDetail account) {
		this.account = account;
	}

}
