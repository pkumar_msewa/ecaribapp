package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class SavaariAuth extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	@Lob
	private String savaariToken;
	
	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail account;

	public String getSavaariToken() {
		return savaariToken;
	}

	public void setSavaariToken(String savaariToken) {
		this.savaariToken = savaariToken;
	}

	public PQAccountDetail getAccount() {
		return account;
	}

	public void setAccount(PQAccountDetail account) {
		this.account = account;
	}
	
	

}
