package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class SettlementBankDetail extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String settlementAccountName;
	private String settlementBankName;
	private String settlementAccountNumber;
	private String settlementIfscCode;
	private String settlementBankLoction;

	public String getSettlementAccountName() {
		return settlementAccountName;
	}

	public void setSettlementAccountName(String settlementAccountName) {
		this.settlementAccountName = settlementAccountName;
	}

	public String getSettlementBankName() {
		return settlementBankName;
	}

	public void setSettlementBankName(String settlementBankName) {
		this.settlementBankName = settlementBankName;
	}

	public String getSettlementAccountNumber() {
		return settlementAccountNumber;
	}

	public void setSettlementAccountNumber(String settlementAccountNumber) {
		this.settlementAccountNumber = settlementAccountNumber;
	}

	public String getSettlementIfscCode() {
		return settlementIfscCode;
	}

	public void setSettlementIfscCode(String settlementIfscCode) {
		this.settlementIfscCode = settlementIfscCode;
	}

	public String getSettlementBankLoction() {
		return settlementBankLoction;
	}

	public void setSettlementBankLoction(String settlementBankLoction) {
		this.settlementBankLoction = settlementBankLoction;
	}

}
