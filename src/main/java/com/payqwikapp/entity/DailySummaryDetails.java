package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DailySummaryDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = -7970432461133777527L;

	@Column
	private long noOfUsers;

	@Column
	private long noOfTransactions;

	@Column
	private double totalDebitTransaction;

	@Column
	private double totalCreditTransaction;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@Column
	private long noOfInitiatedTransactions;
	
	@Column
	private double totalEBSCreditTrasnaction;
	
	@Column
	private double totalVnetCreditTransaction;
	
	@Column
	private double totalUpiCreditTransaction;
	
	@Column
	private long noOfFailedTransactions;
	

	public long getNoOfFailedTransactions() {
		return noOfFailedTransactions;
	}

	public void setNoOfFailedTransactions(long noOfFailedTransactions) {
		this.noOfFailedTransactions = noOfFailedTransactions;
	}

	public double getTotalEBSCreditTrasnaction() {
		return totalEBSCreditTrasnaction;
	}

	public void setTotalEBSCreditTrasnaction(double totalEBSCreditTrasnaction) {
		this.totalEBSCreditTrasnaction = totalEBSCreditTrasnaction;
	}

	public double getTotalVnetCreditTransaction() {
		return totalVnetCreditTransaction;
	}

	public void setTotalVnetCreditTransaction(double totalVnetCreditTransaction) {
		this.totalVnetCreditTransaction = totalVnetCreditTransaction;
	}

	public double getTotalUpiCreditTransaction() {
		return totalUpiCreditTransaction;
	}

	public void setTotalUpiCreditTransaction(double totalUpiCreditTransaction) {
		this.totalUpiCreditTransaction = totalUpiCreditTransaction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getNoOfInitiatedTransactions() {
		return noOfInitiatedTransactions;
	}

	public void setNoOfInitiatedTransactions(long noOfInitiatedTransactions) {
		this.noOfInitiatedTransactions = noOfInitiatedTransactions;
	}

	public long getNoOfUsers() {
		return noOfUsers;
	}

	public void setNoOfUsers(long noOfUsers) {
		this.noOfUsers = noOfUsers;
	}

	public long getNoOfTransactions() {
		return noOfTransactions;
	}

	public void setNoOfTransactions(long noOfTransactions) {
		this.noOfTransactions = noOfTransactions;
	}

	public double getTotalDebitTransaction() {
		return totalDebitTransaction;
	}

	public void setTotalDebitTransaction(double totalDebitTransaction) {
		this.totalDebitTransaction = totalDebitTransaction;
	}

	public double getTotalCreditTransaction() {
		return totalCreditTransaction;
	}

	public void setTotalCreditTransaction(double totalCreditTransaction) {
		this.totalCreditTransaction = totalCreditTransaction;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "DailySummaryDetails [noOfUsers=" + noOfUsers + ", noOfTransactions=" + noOfTransactions
				+ ", totalDebitTransaction=" + totalDebitTransaction + ", totalCreditTransaction="
				+ totalCreditTransaction + ", date=" + date + ", noOfInitiatedTransactions=" + noOfInitiatedTransactions
				+ "]";
	}
	
	

}
