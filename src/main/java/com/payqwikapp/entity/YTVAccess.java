package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;

@Entity
public class YTVAccess extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	@Column
	private String status;
	
	@Column
	private Date expiryDate;
	
	@Column
	private String token;
	
	@Column(unique = true, nullable = false)
	private String userId;
	
	@Column
	private String partnerId;
	
	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail account;
	
	@Column
	private String description;
	
	@Column(unique = true, nullable = false)
	private String uniqueId;
	

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public PQAccountDetail getAccount() {
		return account;
	}

	public void setAccount(PQAccountDetail account) {
		this.account = account;
	}
	
	
}
