package com.payqwikapp.entity;

import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class URegister extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column(unique = true, nullable = false)
	private String username;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status = Status.Inactive;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
