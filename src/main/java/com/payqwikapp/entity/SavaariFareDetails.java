package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class SavaariFareDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private double baseFare;

	private double serviceTax;

	private double driverAllowence;

	private double nightCharge;

	private double totalFare;

	private double discount;

	private double cashToCollect;

	public double getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}

	public double getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}

	public double getDriverAllowence() {
		return driverAllowence;
	}

	public void setDriverAllowence(double driverAllowence) {
		this.driverAllowence = driverAllowence;
	}

	public double getNightCharge() {
		return nightCharge;
	}

	public void setNightCharge(double nightCharge) {
		this.nightCharge = nightCharge;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getCashToCollect() {
		return cashToCollect;
	}

	public void setCashToCollect(double cashToCollect) {
		this.cashToCollect = cashToCollect;
	}

}
