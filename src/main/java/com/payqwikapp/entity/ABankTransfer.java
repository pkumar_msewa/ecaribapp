package com.payqwikapp.entity;

import javax.jws.Oneway;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class ABankTransfer extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private long beneficiaryAccountNumber;
	@Column(nullable = false)
	private double amount;
	@Column(nullable = false)
	private String beneficiaryName;
	@Column(nullable = false)
	private String transactionRefNo;
	
	@Column(nullable = false)
	private String receiverEmailId;
	@Column(nullable = false)
	private String receiverMobileNo;
	
	@Column
	private String image;
	
	@Column
	private String addImageType;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column
	private byte[] imageContent;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] addressImage;
	
	@OneToOne
	private BankDetails bankDetails;
	@OneToOne
	private User sender;
	
	@OneToOne
	private SenderBankTransferInfo senderInfo;
	
	public String getAddImageType() {
		return addImageType;
	}
	public void setAddImageType(String addImageType) {
		this.addImageType = addImageType;
	}
	public byte[] getAddressImage() {
		return addressImage;
	}
	public void setAddressImage(byte[] addressImage) {
		this.addressImage = addressImage;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public byte[] getImageContent() {
		return imageContent;
	}
	public void setImageContent(byte[] imageContent) {
		this.imageContent = imageContent;
	}
	public SenderBankTransferInfo getSenderInfo() {
		return senderInfo;
	}
	public void setSenderInfo(SenderBankTransferInfo senderInfo) {
		this.senderInfo = senderInfo;
	}
	public String getReceiverEmailId() {
		return receiverEmailId;
	}
	public void setReceiverEmailId(String receiverEmailId) {
		this.receiverEmailId = receiverEmailId;
	}
	public String getReceiverMobileNo() {
		return receiverMobileNo;
	}
	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}
	public long getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}
	public void setBeneficiaryAccountNumber(long beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public BankDetails getBankDetails() {
		return bankDetails;
	}
	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	
}
