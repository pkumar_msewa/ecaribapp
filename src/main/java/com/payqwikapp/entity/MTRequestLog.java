package com.payqwikapp.entity;


import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
public class MTRequestLog extends AbstractEntity<Long>{

    private String area;

    private String amount;

    private String serviceProvider;

    private String mobileNo;

    @Column(unique = true,nullable = false)
    private String transactionRefNo;
    
    private boolean fromMdex;
    
    

    public boolean isFromMdex() {
		return fromMdex;
	}

	public void setFromMdex(boolean fromMdex) {
		this.fromMdex = fromMdex;
	}

	public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }
}

