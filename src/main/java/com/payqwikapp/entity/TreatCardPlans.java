package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.payqwikapp.model.Status;


@Entity
public class TreatCardPlans extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String days;
	
	private double amount;
	
	@Enumerated(EnumType.STRING)
	private Status status;

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}



}
