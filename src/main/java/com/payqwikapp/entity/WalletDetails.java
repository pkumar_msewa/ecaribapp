package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class WalletDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = -9163185658863000713L;

	private double balance;

	private double credit;

	private double debit;

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date dot;

	public Date getDot() {
		return dot;
	}

	public void setDot(Date dot) {
		this.dot = dot;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}
}
