package com.payqwikapp.entity;

import com.payqwikapp.model.Gender;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class MKycPoi extends AbstractEntity<Long>{

    @Column(nullable = false)
    private String dob;

    @Column(nullable = false)
    private Gender gender;

    @Column(nullable = false)
    private String name;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
