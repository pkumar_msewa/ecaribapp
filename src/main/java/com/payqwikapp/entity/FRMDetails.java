package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class FRMDetails extends AbstractEntity<Long>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 882136635249086399L;
	
	private String decideResponse;
	private String transactionRefNo;
	private String userContact;
	private boolean enqueueResponse;
	private String requestFor;
	
	public boolean isEnqueueResponse() {
		return enqueueResponse;
	}
	public void setEnqueueResponse(boolean enqueueResponse) {
		this.enqueueResponse = enqueueResponse;
	}
	public String getRequestFor() {
		return requestFor;
	}
	public void setRequestFor(String requestFor) {
		this.requestFor = requestFor;
	}
	public String getDecideResponse() {
		return decideResponse;
	}
	public void setDecideResponse(String decideResponse) {
		this.decideResponse = decideResponse;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getUserContact() {
		return userContact;
	}
	public void setUserContact(String userContact) {
		this.userContact = userContact;
	}
}
