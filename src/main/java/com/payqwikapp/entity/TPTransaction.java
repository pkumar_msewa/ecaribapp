package com.payqwikapp.entity;

import com.payqwikapp.model.Status;

import javax.persistence.*;

@Entity
public class TPTransaction extends AbstractEntity<Long>{

    @Column
    private String orderId;

    @Column
    private double amount;

    @Column
    private String transactionRefNo;

    @OneToOne
    private User merchant;

    @OneToOne
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status;
    
    @OneToOne
    private UpiCustDetails custDetails;
    
    @Column
    @Enumerated(EnumType.STRING)
    private Status approvedStatus;
    
    private String acknowledgeNo;
    
    @Lob
    private String acknowledgeResp;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }

    public User getMerchant() {
        return merchant;
    }

    public void setMerchant(User merchant) {
        this.merchant = merchant;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

	public UpiCustDetails getCustDetails() {
		return custDetails;
	}

	public void setCustDetails(UpiCustDetails custDetails) {
		this.custDetails = custDetails;
	}

	public Status getApprovedStatus() {
		return approvedStatus;
	}

	public void setApprovedStatus(Status approvedStatus) {
		this.approvedStatus = approvedStatus;
	}

	public String getAcknowledgeNo() {
		return acknowledgeNo;
	}

	public void setAcknowledgeNo(String acknowledgeNo) {
		this.acknowledgeNo = acknowledgeNo;
	}

	public String getAcknowledgeResp() {
		return acknowledgeResp;
	}

	public void setAcknowledgeResp(String acknowledgeResp) {
		this.acknowledgeResp = acknowledgeResp;
	}
	
}
