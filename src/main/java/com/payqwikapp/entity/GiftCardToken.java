package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class GiftCardToken extends AbstractEntity<Long>  {

	private static final long serialVersionUID = 1L;
	private String secretId;
	private String secretKey;
	private String token;
	private Date expiryDate;

	public String getSecretId() {
		return secretId;
	}

	public void setSecretId(String secretId) {
		this.secretId = secretId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

}
