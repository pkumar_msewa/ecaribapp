package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.net.URL;

@Entity
public class PartnerDetail extends AbstractEntity<Long>  {


    @Column(unique = true)
    private String apiKey;

    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}
