package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
public class POSMerchants extends AbstractEntity<Long>{

    private String merchantName;

    private String email;

    private String address;

    private String city;

    private String state;

    private final String country = "India";

    private String pincode;

    private String panNo;

    @Column(nullable = false,unique = true)
    private String mobileNumber;

    private String accountName;

    private String bankName;



    @Column(nullable = false,unique = true)
    private String accountNumber;

    private String ifscCode;

    private String bankLocation;

    private String mvisaId;

    private String rupayId;

    private String masterCardId;


    private String customerId;

    @Lob
    private String qrCode;

    private String mcc;

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getRupayId() {
        return rupayId;
    }

    public void setRupayId(String rupayId) {
        this.rupayId = rupayId;
    }

    public String getMasterCardId() {
        return masterCardId;
    }

    public void setMasterCardId(String masterCardId) {
        this.masterCardId = masterCardId;
    }

    public String getMvisaId() {
        return mvisaId;
    }

    public void setMvisaId(String mvisaId) {
        this.mvisaId = mvisaId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBankLocation() {
        return bankLocation;
    }

    public void setBankLocation(String bankLocation) {
        this.bankLocation = bankLocation;
    }

    @Override
    public String toString() {
        return "POSMerchants{" +
                "merchantName='" + merchantName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", pincode='" + pincode + '\'' +
                ", panNo='" + panNo + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", accountName='" + accountName + '\'' +
                ", bankName='" + bankName + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", ifscCode='" + ifscCode + '\'' +
                ", bankLocation='" + bankLocation + '\'' +
                ", mvisaId='" + mvisaId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", qrCode='" + qrCode + '\'' +
                '}';
    }
}
