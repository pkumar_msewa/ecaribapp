package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.payqwikapp.model.Status;

@Entity
public class SendNotification extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	    @Column(nullable = true)
	    private String title;
	   
	    @Column(nullable = true)
	    private String message;
	   
	    @Column(nullable = true)
	    private String image;
	   
	    @Enumerated(EnumType.STRING)
	    private Status status;
	    
	    private long deliveredCount;
	    
	    public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}
	    public String getTitle() {
	        return title;
	    }

	    public void setTitle(String title) {
	        this.title = title;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

	    public String getImage() {
	        return image;
	    }

	    public void setImage(String image) {
	        this.image = image;
	    }

		public long getDeliveredCount() {
			return deliveredCount;
		}

		public void setDeliveredCount(long deliveredCount) {
			this.deliveredCount = deliveredCount;
		}

}
