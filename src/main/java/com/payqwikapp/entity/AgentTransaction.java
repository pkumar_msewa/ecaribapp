package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionType;

@Entity
public class AgentTransaction extends AbstractEntity<Long> {
	private static final long serialVersionUID = 1L;

	@Column(nullable = true)
	private String OTP;

	private double amount;

	private boolean debit;

	private double currentBalance;

	@Column(nullable = true)
	private String description;

	@Column(nullable = true)
	private boolean favourite;

	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(unique = true)
	private String transactionRefNo;

	@ManyToOne(optional = false)
	private PQAccountDetail ag_Account;

	

	@OneToOne(fetch = FetchType.EAGER)
	private PQService ag_Service;
	
	private TransactionType transactionType = TransactionType.DEFAULT;

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String oTP) {
		OTP = oTP;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isDebit() {
		return debit;
	}

	public void setDebit(boolean debit) {
		this.debit = debit;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public PQAccountDetail getAg_Account() {
		return ag_Account;
	}

	public void setAg_Account(PQAccountDetail ag_Account) {
		this.ag_Account = ag_Account;
	}

	public PQService getAg_Service() {
		return ag_Service;
	}

	public void setAg_Service(PQService ag_Service) {
		this.ag_Service = ag_Service;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
