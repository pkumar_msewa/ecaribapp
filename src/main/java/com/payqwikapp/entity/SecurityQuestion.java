package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
	public class SecurityQuestion extends AbstractEntity<Long> {

		private static final long serialVersionUID = 1L;
		
		@Column(nullable = false)
		private String question;
		
		private String code;

		public String getQuestion() {
			return question;
		}

		public void setQuestion(String question) {
			this.question = question;
		}
		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}



}
