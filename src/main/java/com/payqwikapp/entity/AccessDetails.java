package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class AccessDetails extends AbstractEntity<Long>{

    private boolean updateAuthUser;

    private boolean updateAuthAdmin;

    private boolean updateAuthMerchants;

    private boolean updateAuthAgents;

    private boolean sendGCM;

    private boolean sendSMS;

    private boolean sendMail;

    private boolean updateLimits;

    private boolean loadMoney;

    private boolean sendMoney;

    private boolean requestMoney;

    private boolean deductMoney;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    public boolean isUpdateAuthUser() {
        return updateAuthUser;
    }

    public void setUpdateAuthUser(boolean updateAuthUser) {
        this.updateAuthUser = updateAuthUser;
    }

    public boolean isUpdateAuthAdmin() {
        return updateAuthAdmin;
    }

    public void setUpdateAuthAdmin(boolean updateAuthAdmin) {
        this.updateAuthAdmin = updateAuthAdmin;
    }

    public boolean isUpdateAuthMerchants() {
        return updateAuthMerchants;
    }

    public void setUpdateAuthMerchants(boolean updateAuthMerchants) {
        this.updateAuthMerchants = updateAuthMerchants;
    }

    public boolean isUpdateAuthAgents() {
        return updateAuthAgents;
    }

    public void setUpdateAuthAgents(boolean updateAuthAgents) {
        this.updateAuthAgents = updateAuthAgents;
    }

    public boolean isSendGCM() {
        return sendGCM;
    }

    public void setSendGCM(boolean sendGCM) {
        this.sendGCM = sendGCM;
    }

    public boolean isSendSMS() {
        return sendSMS;
    }

    public void setSendSMS(boolean sendSMS) {
        this.sendSMS = sendSMS;
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    public boolean isUpdateLimits() {
        return updateLimits;
    }

    public void setUpdateLimits(boolean updateLimits) {
        this.updateLimits = updateLimits;
    }

    public boolean isLoadMoney() {
        return loadMoney;
    }

    public void setLoadMoney(boolean loadMoney) {
        this.loadMoney = loadMoney;
    }

    public boolean isSendMoney() {
        return sendMoney;
    }

    public void setSendMoney(boolean sendMoney) {
        this.sendMoney = sendMoney;
    }

    public boolean isRequestMoney() {
        return requestMoney;
    }

    public void setRequestMoney(boolean requestMoney) {
        this.requestMoney = requestMoney;
    }

    public boolean isDeductMoney() {
        return deductMoney;
    }

    public void setDeductMoney(boolean deductMoney) {
        this.deductMoney = deductMoney;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
