package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class SavaariTripDetails extends AbstractEntity<Long> {


	private static final long serialVersionUID = 1L;
	
	private String locallity;
	
	private String sourceCity;
	
	private String destinationCity;
	
	private Date pickupDateTime;
	
	private String pickUpLocation;
	
	private Date returnDateTime;
	

	public String getLocallity() {
		return locallity;
	}

	public void setLocallity(String locallity) {
		this.locallity = locallity;
	}

	public String getSourceCity() {
		return sourceCity;
	}

	public void setSourceCity(String sourceCity) {
		this.sourceCity = sourceCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public Date getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(Date pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public String getPickUpLocation() {
		return pickUpLocation;
	}

	public void setPickUpLocation(String pickUpLocation) {
		this.pickUpLocation = pickUpLocation;
	}

	public Date getReturnDateTime() {
		return returnDateTime;
	}

	public void setReturnDateTime(Date returnDateTime) {
		this.returnDateTime = returnDateTime;
	}
	
	
	

}
