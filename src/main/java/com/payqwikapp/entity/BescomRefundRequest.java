package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;

@Entity
public class BescomRefundRequest extends AbstractEntity<Long> {

	private static final long serialVersionUID = 7184799860853214548L;

	private double amount;

	private String trnasactionRefNo;

	@OneToOne(fetch = FetchType.EAGER)
	private PQTransaction transaction;

	@Enumerated(EnumType.STRING)
	private Status status;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTrnasactionRefNo() {
		return trnasactionRefNo;
	}

	public void setTrnasactionRefNo(String trnasactionRefNo) {
		this.trnasactionRefNo = trnasactionRefNo;
	}

	public PQTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(PQTransaction transaction) {
		this.transaction = transaction;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
