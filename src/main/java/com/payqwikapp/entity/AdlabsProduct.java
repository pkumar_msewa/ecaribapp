package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class AdlabsProduct extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@Column(nullable = false)
	private String success_payment;

	@Column(nullable = false)
	private String success_profile;

	@Column(nullable = false)
	private String amount;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSuccess_payment() {
		return success_payment;
	}

	public void setSuccess_payment(String success_payment) {
		this.success_payment = success_payment;
	}

	public String getSuccess_profile() {
		return success_profile;
	}

	public void setSuccess_profile(String success_profile) {
		this.success_profile = success_profile;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	

	
}