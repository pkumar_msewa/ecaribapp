package com.payqwikapp.entity;

import com.payqwikapp.model.Status;
import com.payqwikapp.model.TripType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class BusJourneyDetails extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;
	
    @OneToOne(fetch= FetchType.EAGER)
    private BusDetails busDetails;

    @Column
    private String tripId;

    @Column
    private Date journeyDate;

    @Column(unique = true)
    private String bookingRefNo;

    @Enumerated(EnumType.STRING)
    private Status bookingStatus;

    @OneToOne(fetch = FetchType.EAGER)
    private PQAccountDetail account;

    public BusDetails getBusDetails() {
        return busDetails;
    }

    public void setBusDetails(BusDetails busDetails) {
        this.busDetails = busDetails;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public Date getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(Date journeyDate) {
        this.journeyDate = journeyDate;
    }

    public String getBookingRefNo() {
        return bookingRefNo;
    }

    public void setBookingRefNo(String bookingRefNo) {
        this.bookingRefNo = bookingRefNo;
    }
    public Status getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Status bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public PQAccountDetail getAccount() {
        return account;
    }

    public void setAccount(PQAccountDetail account) {
        this.account = account;
    }
}
