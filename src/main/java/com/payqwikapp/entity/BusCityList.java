package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class BusCityList extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;
	
	@Column(unique=true)
	private long cityId;
	@Column
	private String cityName;
	
	
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
