package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;

@Entity
public class FlightTicket extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	private String bookingRefId;
	private double baseFare;
	private String mdexTxnRefNo;
	private String email;
	private String mobile;
	private double commissionAmt;
	private double paymentAmount;
	private String tripType;
	private String flightNumberOnward;
	private String flightNumberReturn;
	
	@Lob
	private String ticketDetails;
	@Enumerated(EnumType.STRING)
	private Status flightStatus;
	@Enumerated(EnumType.STRING)
	private Status paymentStatus;
	private String paymentMethod;
	@OneToOne(fetch = FetchType.EAGER)
	private PQTransaction transaction;
	@ManyToOne(fetch = FetchType.EAGER)
	private User user;
	
	
	
	public double getBaseFare() {
		return baseFare;
	}
	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}
	public String getBookingRefId() {
		return bookingRefId;
	}
	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}
	public String getMdexTxnRefNo() {
		return mdexTxnRefNo;
	}
	public void setMdexTxnRefNo(String mdexTxnRefNo) {
		this.mdexTxnRefNo = mdexTxnRefNo;
	}
	public double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public Status getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(Status flightStatus) {
		this.flightStatus = flightStatus;
	}
	public Status getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(Status paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public PQTransaction getTransaction() {
		return transaction;
	}
	public void setTransaction(PQTransaction transaction) {
		this.transaction = transaction;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public double getCommissionAmt() {
		return commissionAmt;
	}
	public void setCommissionAmt(double commissionAmt) {
		this.commissionAmt = commissionAmt;
	}
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	public String getFlightNumberOnward() {
		return flightNumberOnward;
	}
	public void setFlightNumberOnward(String flightNumberOnward) {
		this.flightNumberOnward = flightNumberOnward;
	}
	public String getFlightNumberReturn() {
		return flightNumberReturn;
	}
	public void setFlightNumberReturn(String flightNumberReturn) {
		this.flightNumberReturn = flightNumberReturn;
	}
	
}
