package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class TreatCardCount extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String username;
	
	@Column(nullable = false)
	private String emailId;
	
	@Column(nullable = false)
	private long membershipCode;
	
	@Column(nullable = false)
	private long count;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(long membershipCode) {
		this.membershipCode = membershipCode;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}



}
