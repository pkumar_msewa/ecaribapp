package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class UpiPgDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String txnPwdLive;

	@Column(nullable = false)
	private String payeeVirAddrLive;

	@Column(nullable = false)
	private String payeeMobileLive;

	@Column(nullable = false)
	private String kekLive;

	@Column(nullable = false)
	private String merchantIdLive;

	@Column(nullable = false)
	private String vpaTerminalIdLive;

	@Column(nullable = false)
	private String planDekLive;

	@Lob
	private String publicKeyLive;

	@Column(nullable = false)
	private String orgIdLive;

	@Column(nullable = false)
	private String txnPwdTest;

	@Column(nullable = false)
	private String payeeVirAddrTest;

	@Column(nullable = false)
	private String payeeMobileTest;

	@Column(nullable = false)
	private String kekTest;

	@Column(nullable = false)
	private String merchantIdTest;

	@Column(nullable = false)
	private String vpaTerminalIdTest;

	@Column(nullable = false)
	private String planDekTest;

	@Lob
	private String publicKeyTest;

	@Column(nullable = false)
	private String orgIdTest;

	@Column(nullable = false)
	private String subMerchantId;

	@Column(nullable = false)
	private String terminalId;

	@OneToOne
	private PGDetails pgDetails;

	public String getTxnPwdLive() {
		return txnPwdLive;
	}

	public void setTxnPwdLive(String txnPwdLive) {
		this.txnPwdLive = txnPwdLive;
	}

	public String getPayeeVirAddrLive() {
		return payeeVirAddrLive;
	}

	public void setPayeeVirAddrLive(String payeeVirAddrLive) {
		this.payeeVirAddrLive = payeeVirAddrLive;
	}

	public String getPayeeMobileLive() {
		return payeeMobileLive;
	}

	public void setPayeeMobileLive(String payeeMobileLive) {
		this.payeeMobileLive = payeeMobileLive;
	}

	public String getKekLive() {
		return kekLive;
	}

	public void setKekLive(String kekLive) {
		this.kekLive = kekLive;
	}

	public String getMerchantIdLive() {
		return merchantIdLive;
	}

	public void setMerchantIdLive(String merchantIdLive) {
		this.merchantIdLive = merchantIdLive;
	}

	public String getVpaTerminalIdLive() {
		return vpaTerminalIdLive;
	}

	public void setVpaTerminalIdLive(String vpaTerminalIdLive) {
		this.vpaTerminalIdLive = vpaTerminalIdLive;
	}

	public String getPlanDekLive() {
		return planDekLive;
	}

	public void setPlanDekLive(String planDekLive) {
		this.planDekLive = planDekLive;
	}

	public String getPublicKeyLive() {
		return publicKeyLive;
	}

	public void setPublicKeyLive(String publicKeyLive) {
		this.publicKeyLive = publicKeyLive;
	}

	public String getOrgIdLive() {
		return orgIdLive;
	}

	public void setOrgIdLive(String orgIdLive) {
		this.orgIdLive = orgIdLive;
	}

	public String getTxnPwdTest() {
		return txnPwdTest;
	}

	public void setTxnPwdTest(String txnPwdTest) {
		this.txnPwdTest = txnPwdTest;
	}

	public String getPayeeVirAddrTest() {
		return payeeVirAddrTest;
	}

	public void setPayeeVirAddrTest(String payeeVirAddrTest) {
		this.payeeVirAddrTest = payeeVirAddrTest;
	}

	public String getPayeeMobileTest() {
		return payeeMobileTest;
	}

	public void setPayeeMobileTest(String payeeMobileTest) {
		this.payeeMobileTest = payeeMobileTest;
	}

	public String getKekTest() {
		return kekTest;
	}

	public void setKekTest(String kekTest) {
		this.kekTest = kekTest;
	}

	public String getMerchantIdTest() {
		return merchantIdTest;
	}

	public void setMerchantIdTest(String merchantIdTest) {
		this.merchantIdTest = merchantIdTest;
	}

	public String getVpaTerminalIdTest() {
		return vpaTerminalIdTest;
	}

	public void setVpaTerminalIdTest(String vpaTerminalIdTest) {
		this.vpaTerminalIdTest = vpaTerminalIdTest;
	}

	public String getPlanDekTest() {
		return planDekTest;
	}

	public void setPlanDekTest(String planDekTest) {
		this.planDekTest = planDekTest;
	}

	public String getPublicKeyTest() {
		return publicKeyTest;
	}

	public void setPublicKeyTest(String publicKeyTest) {
		this.publicKeyTest = publicKeyTest;
	}

	public String getOrgIdTest() {
		return orgIdTest;
	}

	public void setOrgIdTest(String orgIdTest) {
		this.orgIdTest = orgIdTest;
	}

	public String getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(String subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public PGDetails getPgDetails() {
		return pgDetails;
	}

	public void setPgDetails(PGDetails pgDetails) {
		this.pgDetails = pgDetails;
	}

}
