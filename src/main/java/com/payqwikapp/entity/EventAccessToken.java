package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class EventAccessToken extends AbstractEntity<Long> {
	private static final long serialVersionUID=1L;
	
	@Column(nullable=false, unique = true)
	private String accessToken; 
	
	@OneToOne(fetch = FetchType.EAGER)
	private PQAccountDetail accountDetail;

	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public PQAccountDetail getAccountDetail() {
		return accountDetail;
	}
	public void setAccountDetail(PQAccountDetail accountDetail) {
		this.accountDetail = accountDetail;
	}
}
