package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class ImagicaOrders extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;


	@Column(unique = true,nullable = false)
	private String orderId;

	private String uid;

	private double baseAmount;

	private double CGST;

	private double SGST;

	private String profileId;

	@OneToOne(fetch = FetchType.EAGER)
	private PQTransaction transaction;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getCGST() {
		return CGST;
	}

	public void setCGST(double cGST) {
		CGST = cGST;
	}

	public double getSGST() {
		return SGST;
	}

	public void setSGST(double sGST) {
		SGST = sGST;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}


	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public PQTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(PQTransaction transaction) {
		this.transaction = transaction;
	}
}