package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;
@Entity
public class ReferUserLog extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
    private PQService service;

    @Lob
    private String request;

    @OneToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private Status status;

    private long timeStamp;

    public PQService getService() {
        return service;
    }

    public void setService(PQService service) {
        this.service = service;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
