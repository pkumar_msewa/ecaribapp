package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class VPQMerchant extends AbstractEntity<Long>{
	
	
	private static final long serialVersionUID = 1L;
	@Column(nullable=false,unique=false)
	private String merchantName;
	@Column(nullable=false,unique=false)
	private String address;
	@OneToOne(fetch=FetchType.EAGER)
	private BankDetails bankDetails;
	@Column(nullable=false,unique=true)
	private String bankAccountNumber;
	@Column(nullable=false,unique=false)
	private String contactNo;
	@Column(nullable=false,unique=false)
	private String alternateContactNo;
	@Column(nullable=false,unique=false)
	private String email;
	@OneToOne(fetch=FetchType.EAGER)
	private LocationDetails locationDetails;
	@Column(nullable=false,unique=true)
	private String qrCode;
	@OneToOne(fetch=FetchType.EAGER)
	private User user;
	@Column(nullable=false,unique=false)
	private String panCardNo;
	@Column(nullable=false,unique=false)
	private String adharNo;
	
	public String getPanCardNo() {
		return panCardNo;
	}
	public void setPanCardNo(String panCardNo) {
		this.panCardNo = panCardNo;
	}
	public String getAdharNo() {
		return adharNo;
	}
	public void setAdharNo(String adharNo) {
		this.adharNo = adharNo;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BankDetails getBankDetails() {
		return bankDetails;
	}
	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getAlternateContactNo() {
		return alternateContactNo;
	}
	public void setAlternateContactNo(String alternateContactNo) {
		this.alternateContactNo = alternateContactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocationDetails getLocationDetails() {
		return locationDetails;
	}
	public void setLocationDetails(LocationDetails locationDetails) {
		this.locationDetails = locationDetails;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	

}
