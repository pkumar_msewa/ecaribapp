package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class RefernEarnlogs extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToOne(fetch=FetchType.EAGER)
	private User user;
	private boolean hasEarned;
	private int referalCount;
	
	 public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isHasEarned() {
		return hasEarned;
	}
	public void setHasEarned(boolean hasEarned) {
		this.hasEarned = hasEarned;
	}
	public int getReferalCount() {
		return referalCount;
	}
	public void setReferalCount(int referalCount) {
		this.referalCount = referalCount;
	}
	
	
	
}
