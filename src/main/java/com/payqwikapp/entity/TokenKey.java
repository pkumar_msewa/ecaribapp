package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class TokenKey extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	public String consumerSecretKey;
	public String consumerTokenKey;

	@OneToOne(fetch = FetchType.EAGER)
	public PQService service;

	public String getConsumerScretKey() {
		return consumerSecretKey;
	}

	public void setConsumerScretKey(String consumerScretKey) {
		this.consumerSecretKey = consumerScretKey;
	}

	public String getConsumerTokenKey() {
		return consumerTokenKey;
	}

	public void setConsumerTokenKey(String consumerTokenKey) {
		this.consumerTokenKey = consumerTokenKey;
	}

	public PQService getService() {
		return service;
	}

	public void setService(PQService service) {
		this.service = service;
	}

}
