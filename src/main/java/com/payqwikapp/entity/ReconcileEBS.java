package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class ReconcileEBS extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String paymentId;
	private String authDate;
	private String capDate;
	private String merchantRefNo;
	private String customer;
	private String captured;
	private double refunded;
	private double chargeBack;
	private double TDR;
	private double serviceTax;
	private double swachhaBharat;
	private double krishiKalyan;
	private double GST;
	private double netAmount;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getAuthDate() {
		return authDate;
	}

	public void setAuthDate(String authDate) {
		this.authDate = authDate;
	}

	public String getCapDate() {
		return capDate;
	}

	public void setCapDate(String capDate) {
		this.capDate = capDate;
	}

	public String getMerchantRefNo() {
		return merchantRefNo;
	}

	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCaptured() {
		return captured;
	}

	public void setCaptured(String captured) {
		this.captured = captured;
	}

	public double getRefunded() {
		return refunded;
	}

	public void setRefunded(double refunded) {
		this.refunded = refunded;
	}

	public double getChargeBack() {
		return chargeBack;
	}

	public void setChargeBack(double chargeBack) {
		this.chargeBack = chargeBack;
	}

	public double getTDR() {
		return TDR;
	}

	public void setTDR(double tDR) {
		TDR = tDR;
	}

	public double getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}

	public double getSwachhaBharat() {
		return swachhaBharat;
	}

	public void setSwachhaBharat(double swachhaBharat) {
		this.swachhaBharat = swachhaBharat;
	}

	public double getKrishiKalyan() {
		return krishiKalyan;
	}

	public void setKrishiKalyan(double krishiKalyan) {
		this.krishiKalyan = krishiKalyan;
	}

	public double getGST() {
		return GST;
	}

	public void setGST(double gST) {
		GST = gST;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

}
