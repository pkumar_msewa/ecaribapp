package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class GciCart extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@Column(nullable = false)
	private String productType;

	@Column(nullable = false)
	private String quantity;

	@Column(nullable = false)
	private String amount;
	
	@Column(nullable = false)
	private String totalamount;

	@Column(nullable = false)
	private String brandName;

	@Column(nullable = false)
	private String imagepath;

	@Column(nullable = false)
	private String brandHash;
	
	@Column(nullable = false)
	private String productid;
	
	@Column(nullable = false)
	private String skuId;

	public User getUser() {
		return user;
	}

	public String getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getBrandHash() {
		return brandHash;
	}

	public void setBrandHash(String brandHash) {
		this.brandHash = brandHash;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	




	

	

}
