package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class MerchantBankDetail extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String merchantAccountName;
	private String merchantBankName;
	private String merchantAccountNumber;
	private String merhantIfscCode;
	private String merchantBankLoction;

	public String getMerchantAccountName() {
		return merchantAccountName;
	}

	public void setMerchantAccountName(String merchantAccountName) {
		this.merchantAccountName = merchantAccountName;
	}

	public String getMerchantBankName() {
		return merchantBankName;
	}

	public void setMerchantBankName(String merchantBankName) {
		this.merchantBankName = merchantBankName;
	}

	public String getMerchantAccountNumber() {
		return merchantAccountNumber;
	}

	public void setMerchantAccountNumber(String merchantAccountNumber) {
		this.merchantAccountNumber = merchantAccountNumber;
	}

	public String getMerhantIfscCode() {
		return merhantIfscCode;
	}

	public void setMerhantIfscCode(String merhantIfscCode) {
		this.merhantIfscCode = merhantIfscCode;
	}

	public String getMerchantBankLoction() {
		return merchantBankLoction;
	}

	public void setMerchantBankLoction(String merchantBankLoction) {
		this.merchantBankLoction = merchantBankLoction;
	}
}
