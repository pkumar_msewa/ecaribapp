package com.payqwikapp.entity;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.payqwikapp.model.Status;

@Entity
public class TKOrderDetail extends AbstractEntity<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Enumerated(EnumType.STRING)
	private Status orderStatus;
	@Column
	private long userOrderId;
	@Column
	private String name;	
	@Column
	private String contact_no;
	@Column
	private String mail_id;
	@Column
	private String customer_comment;
	@Column
	private String order_outlet_id;
	@Column
	private String train_number;
	@Column
	private double totalCustomerPayable;
	@Column
	private String station_code;
	@Column
	private String date;
	@Column
	private String pnr;
	@Column
	private String coach;
	@Column
	private String seat;
	@Column
	private String eta;
	@Column
	private String cod;
	
	@Column
	private String transactionRefNo;
	
	@Enumerated(EnumType.STRING)
	@Column
    private Status txnStatus;
  
	public Status getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(Status txnStatus) {
		this.txnStatus = txnStatus;
	}

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getUserOrderId() {
		return userOrderId;
	}

	public void setUserOrderId(long userOrderId) {
		this.userOrderId = userOrderId;
	}

	public Status getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Status orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact_no() {
		return contact_no;
	}

	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	public String getMail_id() {
		return mail_id;
	}

	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}

	public String getCustomer_comment() {
		return customer_comment;
	}

	public void setCustomer_comment(String customer_comment) {
		this.customer_comment = customer_comment;
	}

	public String getOrder_outlet_id() {
		return order_outlet_id;
	}

	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}

	public String getTrain_number() {
		return train_number;
	}

	public void setTrain_number(String train_number) {
		this.train_number = train_number;
	}

	public double getTotalCustomerPayable() {
		return totalCustomerPayable;
	}

	public void setTotalCustomerPayable(double totalCustomerPayable) {
		this.totalCustomerPayable = totalCustomerPayable;
	}

	public String getStation_code() {
		return station_code;
	}

	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}