package com.payqwikapp.entity;

import javax.persistence.Entity;

import com.payqwikapp.model.Status;

@Entity
public class DataConfig extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	
	private String configType;

	private String description;
	
	private String mdex;
	
	private Status status;
	
	private String toEmail;

	private String ccEmail;

	private String bccEmail;
	
	private String code;
	
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getConfigType() {
		return configType;
	}

	public void setConfigType(String configType) {
		this.configType = configType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMdex() {
		return mdex;
	}

	public void setMdex(String mdex) {
		this.mdex = mdex;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public String getBccEmail() {
		return bccEmail;
	}

	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}


	
}
