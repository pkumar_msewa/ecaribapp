package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class TravelkhanaItems extends AbstractEntity<Long>{
	
	private static final long serialVersionUID = 1L;
	
	@Column
	private String itemid;
	
	@Column
	private long quantity;
    
	@ManyToOne
	private TKOrderDetail tkOrderId;
	
	
	public TKOrderDetail getTkOrderId() {
		return tkOrderId;
	}

	public void setTkOrderId(TKOrderDetail tkOrderId) {
		this.tkOrderId = tkOrderId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	
	
}
