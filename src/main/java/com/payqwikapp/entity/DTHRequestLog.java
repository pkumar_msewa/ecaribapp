package com.payqwikapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class DTHRequestLog extends AbstractEntity<Long>{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String amount;

    private String serviceProvider;

    private String dthNo;

    @Column(unique = true,nullable = false)
    private String transactionRefNo;

    private boolean fromMdex;
    
    public boolean isFromMdex() {
		return fromMdex;
	}

	public void setFromMdex(boolean fromMdex) {
		this.fromMdex = fromMdex;
	}

	public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getDthNo() {
        return dthNo;
    }

    public void setDthNo(String dthNo) {
        this.dthNo = dthNo;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }
}
