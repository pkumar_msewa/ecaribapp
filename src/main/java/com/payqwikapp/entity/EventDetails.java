package com.payqwikapp.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.thirdparty.util.EventsType;

@Entity
public class EventDetails  extends AbstractEntity<Long> {

	private static final long serialVersionUID=1L;
	
	@Column(nullable = true)
	private int eventId;
	
	@Column(nullable = true)
	private String category;
	
	@Column(nullable = true)
	private String city;
	
	@Column(nullable = true)
	private String state;
	
	@Column(nullable = true)
	private String country;
	
	@Column(nullable = true)
	private EventsType eventType;
	
	@Column(nullable = true)
	private String eventName;
	
	@Column(nullable = true)
	private String vanue;
	
	@Column(nullable = true)
	private double totalAmount;
	
	@Column(nullable = true)
	private int totalQuantity;
	
	@Column(nullable = true)
	private String orderId;
	
	@Column(nullable = true)
	private int eventSignUpId;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date bookingDate;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private PQAccountDetail accountDetail;

	@ManyToOne(fetch = FetchType.EAGER)
	private PQTransaction transactions;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private EventTicketDetails ticketDetails;
	
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public EventsType getEventType() {
		return eventType;
	}
	public void setEventType(EventsType eventType) {
		this.eventType = eventType;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getVanue() {
		return vanue;
	}
	public void setVanue(String vanue) {
		this.vanue = vanue;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public int getEventSignUpId() {
		return eventSignUpId;
	}
	public void setEventSignUpId(int eventSignUpId) {
		this.eventSignUpId = eventSignUpId;
	}
	public Date getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public PQAccountDetail getAccountDetail() {
		return accountDetail;
	}
	public void setAccountDetail(PQAccountDetail accountDetail) {
		this.accountDetail = accountDetail;
	}
	public PQTransaction getTransactions() {
		return transactions;
	}
	public void setTransactions(PQTransaction transactions) {
		this.transactions = transactions;
	}
	public EventTicketDetails getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(EventTicketDetails ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
}
