package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
public class Visa extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String contactNo;
	private String senderCardNo;
	private String fromEntityId;
	private String merchantData;
	private String description;
	private double amount;
	private String toEntityId;
	private String business;
	private String transactionType;
	private String productId;
	private String externalTransactionId;
	private String billRefNo;
	private String visaTransactionRef;
	
	@OneToOne(fetch = FetchType.EAGER)
	private PQTransaction transaction;
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getSenderCardNo() {
		return senderCardNo;
	}

	public void setSenderCardNo(String senderCardNo) {
		this.senderCardNo = senderCardNo;
	}

	public String getFromEntityId() {
		return fromEntityId;
	}

	public void setFromEntityId(String fromEntityId) {
		this.fromEntityId = fromEntityId;
	}

	public String getMerchantData() {
		return merchantData;
	}

	public void setMerchantData(String merchantData) {
		this.merchantData = merchantData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getToEntityId() {
		return toEntityId;
	}

	public void setToEntityId(String toEntityId) {
		this.toEntityId = toEntityId;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getExternalTransactionId() {
		return externalTransactionId;
	}

	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}

	public String getBillRefNo() {
		return billRefNo;
	}

	public void setBillRefNo(String billRefNo) {
		this.billRefNo = billRefNo;
	}

	public PQTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(PQTransaction transaction) {
		this.transaction = transaction;
	}

	public String getVisaTransactionRef() {
		return visaTransactionRef;
	}

	public void setVisaTransactionRef(String visaTransactionRef) {
		this.visaTransactionRef = visaTransactionRef;
	}
	
}
