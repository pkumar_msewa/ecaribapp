package com.payqwikapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Voucher extends AbstractEntity<Long>{
	private static final long serialVersionUID = 1L;
	
	@Column(unique = true)
	private String voucherNo;
	@Column(nullable = false)
	private double amount;
	@Column(nullable = false)
	private Date expiryDate;
	@Column
	private boolean expired;
	@Column
	private boolean redeemed;
	
	@OneToOne
	private PQTransaction pqTransaction;
	
	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public boolean isRedeemed() {
		return redeemed;
	}

	public void setRedeemed(boolean redeemed) {
		this.redeemed = redeemed;
	}

	public PQTransaction getPqTransaction() {
		return pqTransaction;
	}

	public void setPqTransaction(PQTransaction pqTransaction) {
		this.pqTransaction = pqTransaction;
	}
}
