package com.payqwikapp.entity;

import javax.persistence.Entity;

@Entity
public class UserMaxLimit extends AbstractEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6136911984046794019L;
	
	private long merchantDailyNoOfTxn;
	private double merchantDailyAmountOfTxn;
	private long merchantMonthlyNoOfTxn;
	private double merchantMonthlyAmountOfTxn;
	private long userDailyNoOfTxn;
	private double userDailyAmountOfTxn;
	private long userMonthlyNoOfTxn;
	private double userMonthlyAmountOfTxn;
	private long userDailyNoOfBT; // User daily number of Bank Transfer
	private double userDailyAmountOfBT;
	private long userMonthlyNoOfBT;
	private double userMonthlyAmountOfBT;

	public long getMerchantDailyNoOfTxn() {
		return merchantDailyNoOfTxn;
	}

	public void setMerchantDailyNoOfTxn(long merchantDailyNoOfTxn) {
		this.merchantDailyNoOfTxn = merchantDailyNoOfTxn;
	}

	public double getMerchantDailyAmountOfTxn() {
		return merchantDailyAmountOfTxn;
	}

	public void setMerchantDailyAmountOfTxn(double merchantDailyAmountOfTxn) {
		this.merchantDailyAmountOfTxn = merchantDailyAmountOfTxn;
	}

	public long getMerchantMonthlyNoOfTxn() {
		return merchantMonthlyNoOfTxn;
	}

	public void setMerchantMonthlyNoOfTxn(long merchantMonthlyNoOfTxn) {
		this.merchantMonthlyNoOfTxn = merchantMonthlyNoOfTxn;
	}

	public double getMerchantMonthlyAmountOfTxn() {
		return merchantMonthlyAmountOfTxn;
	}

	public void setMerchantMonthlyAmountOfTxn(double merchantMonthlyAmountOfTxn) {
		this.merchantMonthlyAmountOfTxn = merchantMonthlyAmountOfTxn;
	}

	public long getUserDailyNoOfTxn() {
		return userDailyNoOfTxn;
	}

	public void setUserDailyNoOfTxn(long userDailyNoOfTxn) {
		this.userDailyNoOfTxn = userDailyNoOfTxn;
	}

	public double getUserDailyAmountOfTxn() {
		return userDailyAmountOfTxn;
	}

	public void setUserDailyAmountOfTxn(double userDailyAmountOfTxn) {
		this.userDailyAmountOfTxn = userDailyAmountOfTxn;
	}

	public long getUserMonthlyNoOfTxn() {
		return userMonthlyNoOfTxn;
	}

	public void setUserMonthlyNoOfTxn(long userMonthlyNoOfTxn) {
		this.userMonthlyNoOfTxn = userMonthlyNoOfTxn;
	}

	public double getUserMonthlyAmountOfTxn() {
		return userMonthlyAmountOfTxn;
	}

	public void setUserMonthlyAmountOfTxn(double userMonthlyAmountOfTxn) {
		this.userMonthlyAmountOfTxn = userMonthlyAmountOfTxn;
	}

	public long getUserDailyNoOfBT() {
		return userDailyNoOfBT;
	}

	public void setUserDailyNoOfBT(long userDailyNoOfBT) {
		this.userDailyNoOfBT = userDailyNoOfBT;
	}

	public double getUserDailyAmountOfBT() {
		return userDailyAmountOfBT;
	}

	public void setUserDailyAmountOfBT(double userDailyAmountOfBT) {
		this.userDailyAmountOfBT = userDailyAmountOfBT;
	}

	public long getUserMonthlyNoOfBT() {
		return userMonthlyNoOfBT;
	}

	public void setUserMonthlyNoOfBT(long userMonthlyNoOfBT) {
		this.userMonthlyNoOfBT = userMonthlyNoOfBT;
	}

	public double getUserMonthlyAmountOfBT() {
		return userMonthlyAmountOfBT;
	}

	public void setUserMonthlyAmountOfBT(double userMonthlyAmountOfBT) {
		this.userMonthlyAmountOfBT = userMonthlyAmountOfBT;
	}

}
