package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SecurityAnswerDetails extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String answer;
	
	@OneToOne(fetch = FetchType.EAGER)
	private User user;
	
	@ManyToOne(fetch = FetchType.EAGER , optional = false)
	private SecurityQuestion secQuestion;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public SecurityQuestion getSecQuestion() {
		return secQuestion;
	}

	public void setSecQuestion(SecurityQuestion secQuestion) {
		this.secQuestion = secQuestion;
	}

}
