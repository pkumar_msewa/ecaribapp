
package com.payqwikapp.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.payqwikapp.model.Status;

@Entity
public class VisaMerchant extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String pinCode;
	private String merchantBusinessType;
	private String latitude;
	private String longitude;
	private String isEnabled;
	private String panNo;
	private String mobileNo;
	private String aggregator;
	private String entityId;
	private String mVisaId;

	@Enumerated(EnumType.STRING)
	private Status status;

	@OneToOne(fetch = FetchType.EAGER)
	private User user;

	@OneToOne(fetch = FetchType.EAGER)
	private MerchantBankDetail mBankDedail;

	@OneToOne(fetch = FetchType.EAGER)
	private SettlementBankDetail sBankDetail;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getMerchantBusinessType() {
		return merchantBusinessType;
	}

	public void setMerchantBusinessType(String merchantBusinessType) {
		this.merchantBusinessType = merchantBusinessType;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAggregator() {
		return aggregator;
	}

	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getmVisaId() {
		return mVisaId;
	}

	public void setmVisaId(String mVisaId) {
		this.mVisaId = mVisaId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MerchantBankDetail getmBankDedail() {
		return mBankDedail;
	}

	public void setmBankDedail(MerchantBankDetail mBankDedail) {
		this.mBankDedail = mBankDedail;
	}

	public SettlementBankDetail getsBankDetail() {
		return sBankDetail;
	}

	public void setsBankDetail(SettlementBankDetail sBankDetail) {
		this.sBankDetail = sBankDetail;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
