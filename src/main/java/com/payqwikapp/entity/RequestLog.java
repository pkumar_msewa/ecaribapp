package com.payqwikapp.entity;

import com.payqwikapp.model.Status;

import javax.persistence.*;

@Entity
public class RequestLog extends AbstractEntity<Long> {

    @OneToOne(fetch = FetchType.EAGER)
    private PQService service;

    @Lob
    private String request;

    @OneToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private Status status;

    private long timeStamp;
    

	public PQService getService() {
        return service;
    }

    public void setService(PQService service) {
        this.service = service;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
