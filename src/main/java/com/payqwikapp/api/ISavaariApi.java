package com.payqwikapp.api;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.SavaariBooking;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.BookSavaariDTO;
import com.payqwikapp.model.SavaariTokenDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ISavaariApi {
	
	ResponseDTO saveAccessToken(SavaariTokenDTO requset, String username);

	ResponseDTO getAccessToken(SavaariTokenDTO request, String username);
	
	void saveSavaariTicket(BookSavaariDTO req, User user, PQService service);

	void bookSavaariTicket(BookSavaariDTO req, User user) throws ParseException;

	void failBookSavaariTicket(BookSavaariDTO req);

	boolean checkBalance(User user, BookSavaariDTO req);

	boolean checkBookingId(User user, BookSavaariDTO dto);

	boolean checkUserBookingId(User user, BookSavaariDTO dto);

	void CancelBookSavaariTicket(BookSavaariDTO dto);

	List<SavaariBooking> getBookingList(User u, Status status);

	Date getLastTranasactionTimeByStatus(String bookingId, Status status);
}
