package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.TreatCardPlans;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.TreatCardDTO;
import com.payqwikapp.model.TreatCardPaymentResponse;
import com.payqwikapp.model.TreatCardPlansDTO;
import com.payqwikapp.model.TreatCardRegisterDTO;
import com.payqwikapp.model.TreatCardResponse;
import com.payqwikapp.model.UpdateTreatCardDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ITreatCardApi {
	
	TreatCardResponse requestCardDetails(TreatCardDTO dto);

	TreatCardResponse getTreatCardDetails(User user);

	TreatCardResponse registerTreatCard(TreatCardDTO dto);

	List<TreatCardPlansDTO> getAllPlansDTO();

	ResponseDTO updatePlans(TreatCardPlansDTO dto);

	List<TreatCardPlans> getAllPlans();

	TreatCardPaymentResponse updateTreatCard(UpdateTreatCardDTO dto, String username, PQService service,
			PQCommission commission, double netCommissionValue);

	TreatCardPaymentResponse updateTreatCardWeb(UpdateTreatCardDTO treatCard, String username);
	
	
	List<TreatCardRegisterDTO> registerList();

	List<TreatCardRegisterDTO> registerListFiltered(Date startDate, Date endDate);

	List<TreatCardRegisterDTO> countList();

	List<TListDTO> getTreatCardTransactions();

}
