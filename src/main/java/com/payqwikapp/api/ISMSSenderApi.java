package com.payqwikapp.api;

import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.WoohooCardDetails;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.model.travel.AgentTicketDetailsDTOFlight;
import com.payqwikapp.model.travel.TicketDetailsDTOFlight;
import com.payqwikapp.model.travel.bus.AgentTicketDetailsDTO;
import com.payqwikapp.model.travel.bus.TicketDetailsDTO;
import com.payqwikapp.sms.util.SMSAccount;

public interface ISMSSenderApi {

	void sendUserSMS(SMSAccount smsAccount, String smsTemplate, User user,String additionalInfo);

	void sendAlertSMS(SMSAccount smsAccount, String smsTemplate, String destination,String additionalInfo);

	void sendMerchantSMS(SMSAccount smsAccount, String smsTemplate, User user,String additionalInfo);

	void sendPromotionalSMS(SMSAccount smsAccount, String smsTemplate, User user,String additionalInfo);

	void sendTransactionSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,String additionalInfo);

	void promotionalEmails(String number, String message);

	void sendKYCSMS(SMSAccount smsAccount, String smsTemplate,User user,String mobileNumber,String additionalInfo);
	
	void sendUserQWIKRSMS(SMSAccount smsAccount, String smsTemplate, User user, QwikrPayRequest dto);

	void sendUserGCISMS(SMSAccount smsAccount, String smsTemplate, GiftCardTransactionResponse response, User u,
			ProcessGiftCardDTO dto);


	void sendReferNearnMsg(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo);


	boolean sendUserBillpaySMS(SMSAccount smsAccount, String smsTemplate,User u, String description, String additionalInfo);

	void sendBusTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,TicketDetailsDTO additionalInfo);
	
	void sendAnotherUserSMS(SMSAccount smsAccount, String smsTemplate, User mobile, String additionalInfo);

	void sendFlightTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,FlightDetails additionalInfo);

	void sendFlightTicketPaymentGatewaySMS(SMSAccount smsAccount, String smsTemplate, User user, String transaction,
			FlightDetails additionalInfo);

	void sendRedeemPointsSMS(SMSAccount smsAccount, String smsTemplate, User user, double amount,long point,PQTransaction receiveTransaction);

	void sendFlightTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,TicketDetailsDTOFlight additionalInfo);

	void sendTransactionSMSOrderFood(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			String additionalInfo, long userOrderId, TKOrderDetail orderDetail);

	void sendUserSMSNew(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo);

	void sendBulkFileSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			String additionalInfo);

	void sendBescomTransactionSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,String consumerId);
	
	void sendUserWoohooSMS(SMSAccount smsAccount, String smsTemplate, WoohooCardDetails dto, User u);

	void sendWoohooSMS(SMSAccount smsAccount, String smsTemplate, WoohooCardDetails dto, User u);

	void sendFlightTicketSMS(SMSAccount payqwikTransactional, String fLIGHTTICKET_SUCCESS, User user,
			PQTransaction senderTransaction, AgentTicketDetailsDTOFlight additionalInfo);

	void sendBusTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			AgentTicketDetailsDTO additionalInfo);

	void senderBankTransferSMS(SMSAccount payqwikTransactional, String smsTemplate,
			AgentSendMoneyBankDTO dto, String senderMobileNo);

}
