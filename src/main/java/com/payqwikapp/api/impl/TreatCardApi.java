package com.payqwikapp.api.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ITreatCardApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TreatCardCount;
import com.payqwikapp.entity.TreatCardDetails;
import com.payqwikapp.entity.TreatCardPlans;
import com.payqwikapp.entity.User;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.TreatCardDTO;
import com.payqwikapp.model.TreatCardPaymentResponse;
import com.payqwikapp.model.TreatCardPlansDTO;
import com.payqwikapp.model.TreatCardRegisterDTO;
import com.payqwikapp.model.TreatCardResponse;
import com.payqwikapp.model.UpdateTreatCardDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.TreatCardCountRepository;
import com.payqwikapp.repositories.TreatCardDetailsRepository;
import com.payqwikapp.repositories.TreatCardPlansRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.StartupUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class TreatCardApi implements ITreatCardApi {

	private final static String LOGIN_URL = DeploymentConstants.WEB_URL + "/Api/v1/User/Android/en/TreatCard/Register/Process";
	private final static String UPDATE_URL = DeploymentConstants.WEB_URL + "/Api/v1/User/Android/en/TreatCard/Update/Process";

	private final UserRepository userRepository;
	private final TreatCardDetailsRepository treatCardDetailsRepository;
	private final TreatCardPlansRepository treatCardPlansRepository;
	private final TransactionApi transactionApi;
	private final IMailSenderApi mailSenderApi;
	private final TreatCardCountRepository treatCardCountRepository;
	private final PQServiceRepository pqServiceRepository;
	private final PQTransactionRepository pqTransactionRepository;
	

	public TreatCardApi(UserRepository userRepository,
			TreatCardDetailsRepository treatCardDetailsRepository,TreatCardPlansRepository treatCardPlansRepository,TransactionApi transactionApi,IMailSenderApi mailSenderApi,
			TreatCardCountRepository treatCardCountRepository,PQServiceRepository pqServiceRepository,PQTransactionRepository pqTransactionRepository) {
		this.userRepository = userRepository;
		this.treatCardDetailsRepository = treatCardDetailsRepository;
		this.treatCardPlansRepository = treatCardPlansRepository;
		this.transactionApi=transactionApi;
		this.mailSenderApi=mailSenderApi;
		this.treatCardCountRepository=treatCardCountRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.pqTransactionRepository = pqTransactionRepository;
	}

	@Override
	public TreatCardResponse requestCardDetails(TreatCardDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreatCardResponse getTreatCardDetails(User user) {
		// TODO Auto-generated method stub
		TreatCardResponse response= new TreatCardResponse();
		TreatCardDetails details=treatCardDetailsRepository.findByUser(user);
		long counts = 1;
		if(details == null){
			TreatCardDTO dto= new TreatCardDTO();
			dto.setPhoneNumber(user.getUsername());
			response=registerTreatCard(dto);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setMessage("You are register at TreatCard");
			response.setDetails("You are register at TreatCard");
			TreatCardCount count=treatCardCountRepository.findByUser(user.getUsername());
			if(count==null){
				count= new TreatCardCount();
				count.setUsername(user.getUsername());
				count.setMembershipCode(response.getMembershipCode());
				count.setEmailId(user.getUserDetail().getEmail());
				count.setCount(1);
				treatCardCountRepository.save(count);
			}else{
				counts +=count.getCount();
				count.setUsername(user.getUsername());
				count.setMembershipCode(response.getMembershipCode());
				count.setEmailId(user.getUserDetail().getEmail());
				count.setCount(counts);
				count.setCreated(new Date());
				count.setLastModified(new Date());
				treatCardCountRepository.save(count);
			}
			return response;
		}else if(details.getMembershipCodeExpire().before(new Date())){
			response.setMembershipCode(details.getMembershipCode());
			response.setMemberShipCodeExpire(details.getMembershipCodeExpire());
			response.setStatus(ResponseStatus.BAD_REQUEST);
			response.setMessage("Your MembershipCard has been expired,please activate plans.");
			response.setDetails("Your MembershipCard Has been expired,please activate plans.");
			TreatCardCount count=treatCardCountRepository.findByUser(user.getUsername());
			if(count==null){
				count= new TreatCardCount();
				count.setUsername(user.getUsername());
				count.setMembershipCode(response.getMembershipCode());
				count.setEmailId(user.getUserDetail().getEmail());
				count.setCount(1);
				treatCardCountRepository.save(count);
			}else{
				counts +=count.getCount();
				count.setUsername(user.getUsername());
				count.setMembershipCode(response.getMembershipCode());
				count.setEmailId(user.getUserDetail().getEmail());
				count.setCount(counts);
				count.setCreated(new Date());
				count.setLastModified(new Date());
				treatCardCountRepository.save(count);
				
			}
			return response;
		}else{
			response.setMembershipCode(details.getMembershipCode());
			response.setMemberShipCodeExpire(details.getMembershipCodeExpire());
			response.setStatus(ResponseStatus.FAILURE);
			response.setMessage("You are already registered at TreatCard .");
			response.setDetails("You are already registered at TreatCard .");
			TreatCardCount count=treatCardCountRepository.findByUser(user.getUsername());
			if(count==null){
				count= new TreatCardCount();
				count.setUsername(user.getUsername());
				count.setMembershipCode(response.getMembershipCode());
				count.setEmailId(user.getUserDetail().getEmail());
				count.setCount(1);
				treatCardCountRepository.save(count);
			}else{
				counts +=count.getCount();
				count.setUsername(user.getUsername());
				count.setMembershipCode(response.getMembershipCode());
				count.setEmailId(user.getUserDetail().getEmail());
				count.setCount(counts);
				count.setCreated(new Date());
				count.setLastModified(new Date());
				treatCardCountRepository.save(count);
				
			}
			return response;
		}
	}

	@Override
	public TreatCardResponse registerTreatCard(TreatCardDTO treatCard) {
		TreatCardResponse response= new TreatCardResponse();
		User user = userRepository.findByUsername(treatCard.getPhoneNumber());
		JSONObject payload = new JSONObject();
		try {
			treatCard.setEmail(user.getUserDetail().getEmail());
			treatCard.setFirstName(user.getUserDetail().getFirstName());
			treatCard.setLastName(user.getUserDetail().getFirstName());
			payload.put("phoneNumber", "91" + treatCard.getPhoneNumber());
			payload.put("emailId", treatCard.getEmail());
			payload.put("first", treatCard.getFirstName());
			payload.put("last", treatCard.getFirstName());
			Client client = Client.create();
			WebResource webResource = client.resource(LOGIN_URL);
			ClientResponse result = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, payload);
			String strResponse = result.getEntity(String.class);
			if (result.getStatus() == 200) {
				org.json.JSONObject object = new org.json.JSONObject(strResponse);
				if (object != null) {
					String message = JSONParserUtil.getString(object, "message");
					String code = JSONParserUtil.getString(object, "code");
					org.json.JSONObject data = object.getJSONObject("data");
					boolean isExist = JSONParserUtil.getBoolean(data, "isExist");
					if (isExist) {
						TreatCardDetails cardDetails = treatCardDetailsRepository.findByUser(user);
						if(cardDetails == null){
						cardDetails=new TreatCardDetails();
						cardDetails.setMembershipCode(JSONParserUtil.getLong(data, "membershipCode"));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
						sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
						cardDetails.setMembershipCodeExpire(sdf.parse(JSONParserUtil.getString(data, "membershipCodeExpire")));
						cardDetails.setUser(user);
						treatCardDetailsRepository.save(cardDetails);
						response.setMembershipCode(cardDetails.getMembershipCode());
						response.setMemberShipCodeExpire(cardDetails.getMembershipCodeExpire());
						mailSenderApi.treatCardEmail("TreatCard Registration", MailTemplate.TREATCARD_EMAIL,user);
						return response;
						}		
					} else {
						TreatCardDetails cardDetails = treatCardDetailsRepository.findByUser(user);
						if(cardDetails == null){
						cardDetails=new TreatCardDetails();
						cardDetails.setMembershipCode(JSONParserUtil.getLong(data, "membershipCode"));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
						sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
						cardDetails.setMembershipCodeExpire(sdf.parse(JSONParserUtil.getString(data, "membershipCodeExpire")));
						cardDetails.setUser(user);
						treatCardDetailsRepository.save(cardDetails);
						response.setMembershipCode(cardDetails.getMembershipCode());
						response.setMemberShipCodeExpire(cardDetails.getMembershipCodeExpire());
						mailSenderApi.treatCardEmail("TreatCard Registration", MailTemplate.TREATCARD_EMAIL,user);
						return response;
						}
								
					}
					response.setSuccess(true);
				}
			} else if (result.getStatus() == 400) {
				org.json.JSONObject object = new org.json.JSONObject(strResponse);
				if (object != null) {
					String message = JSONParserUtil.getString(object, "message");
					String code = JSONParserUtil.getString(object, "code");
					response.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}
	
	@Override
	public List<TreatCardPlansDTO> getAllPlansDTO() {
		List<TreatCardPlans> plans = treatCardPlansRepository.findAllPlans();
		if (plans != null && plans.size() != 0) {
			List<TreatCardPlansDTO> plansDTOs = ConvertUtil.convertTreatCardPlansList(plans);
			return plansDTOs;
		}
		return null;
	}
	
	@Override
	public List<TreatCardPlans> getAllPlans() {
		return treatCardPlansRepository.findAllPlans();
	}
	
	@Override
	public ResponseDTO updatePlans(TreatCardPlansDTO dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			TreatCardPlans plans = treatCardPlansRepository.findPlansById(dto.getId());
			if (plans != null) {
				plans.setDays(dto.getDays());
				plans.setAmount(dto.getAmount());
				plans.setStatus(dto.getStatus());
				TreatCardPlans updated = treatCardPlansRepository.save(plans);
				if (updated != null) {
					List<TreatCardPlans> updatedPlans = getAllPlans();
					result.setMessage("Updated Successfully");
					result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails(updatedPlans);
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("No plans found");
			}
		} catch (Exception ex) {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Bad Request");
		}
		return result;
	}
	
	
	@Override
	public TreatCardPaymentResponse updateTreatCard(UpdateTreatCardDTO dto, String username, PQService service ,PQCommission commission,double netCommissionValue) {
		
		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.initiateTreatCardPayment(Double.parseDouble(dto.getAmount()),"Update TreatCard Rs " + dto.getAmount() + " to " + username,
						service, transactionRefNo, username,StartupUtil.TREAT_CARD,commission,netCommissionValue);
		TreatCardPaymentResponse response=updateTreatCardWeb(dto,username);
		if (response.isSuccess()) {
			transactionApi.successTreatCardPayment(transactionRefNo,commission,netCommissionValue);
		} else {
			transactionApi.failedTreatCardPayment(transactionRefNo);
		}
		return response;
	}
	
	
	@Override
	public TreatCardPaymentResponse updateTreatCardWeb(UpdateTreatCardDTO treatCard,String username) {
		TreatCardPaymentResponse response= new TreatCardPaymentResponse();
		User user = userRepository.findByUsername(username);
		JSONObject payload = new JSONObject();
		try {
			payload.put("phoneNumber", "91" + username);
			payload.put("membershipCardValidityInDays", treatCard.getDays());
			Client client = Client.create();
			WebResource webResource = client.resource(UPDATE_URL);
			ClientResponse result = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, payload);
			String strResponse = result.getEntity(String.class);
			if (result.getStatus() == 200) {
				org.json.JSONObject object = new org.json.JSONObject(strResponse);
				if (object != null) {
					String message = JSONParserUtil.getString(object, "message");
					String code = JSONParserUtil.getString(object, "code");
					org.json.JSONObject data = object.getJSONObject("data");
						TreatCardDetails cardDetails = treatCardDetailsRepository.findByUser(user);
						if(cardDetails !=null){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
						sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
						cardDetails.setMembershipCodeExpire(sdf.parse(JSONParserUtil.getString(data, "membershipCodeExpire")));
						treatCardDetailsRepository.save(cardDetails);
						response.setSuccess(true);
						response.setMemberShipCodeExpire(cardDetails.getMembershipCodeExpire());
						response.setMembershipCode(cardDetails.getMembershipCode());
						return response;
						}		
				}
			} else if (result.getStatus() == 400) {
				org.json.JSONObject object = new org.json.JSONObject(strResponse);
				if (object != null) {
					String message = JSONParserUtil.getString(object, "message");
					String code = JSONParserUtil.getString(object, "code");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}

	@Override
	public List<TreatCardRegisterDTO> registerList() {
		Pageable page = new PageRequest(0,500);
		Page<TreatCardDetails> registerDetails=treatCardDetailsRepository.findAllRegisterList(page);
		return ConvertUtil.convertRegisterList(registerDetails.getContent());
	}
	
	
	@Override
	public List<TreatCardRegisterDTO> registerListFiltered(Date startDate,Date endDate) {
		List<TreatCardDetails> registerDetails=treatCardDetailsRepository.findAllRegisterListFiltered(startDate,endDate);
		return ConvertUtil.convertRegisterList(registerDetails);
	}
	
	
	@Override
	public List<TreatCardRegisterDTO> countList() {
		List<TreatCardCount> countDetails=treatCardCountRepository.findAllCountList();
		return ConvertUtil.convertTreatCardList(countDetails);
	}
	
	@Override
	public List<TListDTO> getTreatCardTransactions() {
		List<TListDTO> listDTOs = new ArrayList<>();
		Pageable page = new PageRequest(0,1000);
		try {
			PQService service = pqServiceRepository.findServiceByCode("TECD");
			Page<PQTransaction> todaysTransactions = pqTransactionRepository.findTreatCardTransaction(page,service,TransactionType.DEFAULT);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(todaysTransactions.getContent());
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, todaysTransactions.getContent());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

}
