package com.payqwikapp.api.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.api.ISavaariBookingApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQServiceRepository;

public class SavaariBookingApi implements ISavaariBookingApi {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final IUserApi userApi;
	private final PQTransactionRepository pqTransactionRepository;
	private final ISMSSenderApi smsSenderApi;
	private final IMailSenderApi mailSenderApi;
	private final ICommissionApi commissionApi;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQServiceRepository pqServiceRepository;

	public SavaariBookingApi(IUserApi userApi, PQTransactionRepository pqTransactionRepository, ISMSSenderApi smsSenderApi,
			IMailSenderApi mailSenderApi, ICommissionApi commissionApi,
			PQAccountDetailRepository pqAccountDetailRepository, PQServiceRepository pqServiceRepository) {
		super();
		this.userApi = userApi;
		this.pqTransactionRepository = pqTransactionRepository;
		this.smsSenderApi = smsSenderApi;
		this.mailSenderApi = mailSenderApi;
		this.commissionApi = commissionApi;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqServiceRepository = pqServiceRepository;
	}
	
	@Override
	public void initiateSavaariBooking(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, String json, String favourite) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();

		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setAccount(senderAccountDetail);
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setRequest(json);
			logger.error("Sender savaari booking Initiated");
			pqTransactionRepository.save(senderTransaction);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			System.err
					.print("*************************sender current balance in initiate case ********************************  "
							+ senderCurrentBalance);

			User temp = userApi.findByUserName(senderUsername);
			System.err
					.print("*************************sender updated balance in initiate case ********************************  "
							+ temp.getAccountDetail().getBalance());
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		System.err.println("settlement Transaction" + settlementTransactionExists);
		if (settlementTransactionExists == null) {
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			logger.error("Settlement Bill Payment Success");
			pqTransactionRepository.save(settlementTransaction);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			// userApi.updateBalance(settlementCurrentBalance,
			// settlementAccount);
		}

		User savaariAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (instantpayTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			PQAccountDetail savaariAccountDetail = savaariAccount.getAccountDetail();
			double receiverCurrentBalance = savaariAccount.getAccountDetail().getBalance();
			instantpayTransaction.setCurrentBalance(receiverCurrentBalance);
			instantpayTransaction.setAmount(receiverTransactionAmount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(savaariAccount.getAccountDetail());
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Initiated);
			logger.error("Receiver Bill Payment Initiated");
			pqTransactionRepository.save(instantpayTransaction);
			savaariAccountDetail.setBalance(receiverCurrentBalance);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			logger.error("commission Bill Payment Initiated");
			pqTransactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public PQTransaction getTransactionByRefNo(String transactionRefNo) {
		return pqTransactionRepository.findByTransactionRefNo(transactionRefNo);
	}

	@Override
	public void successSavaariBooking(String transactionRefNo,String description) {

		String senderUsername = null;
		String receiverUsername = null;

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();

		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			logger.error("SenderTransaction TransactionRefNo in case of sucess " + senderTransaction);
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			 netTransactionAmount = senderTransaction.getAmount();
//			netTransactionAmount = 0.0;
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);

			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setDescription(description);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
                long points = calculatePoints(netTransactionAmount);
                pqAccountDetailRepository.addUserPoints(points, accountNumber);
				logger.error("sender Debit success ");
				PQTransaction t = pqTransactionRepository.save(senderTransaction);

				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					
//					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
//							senderTransaction," ",null);
				}
			}
		}

		PQTransaction settlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransaction != null) {
			User settlement = userApi.findByAccountDetail(settlementTransaction.getAccount());
			PQAccountDetail accountDetail = settlementTransaction.getAccount();
			double settlementTransactionAmount = settlementTransaction.getAmount();
			String settlementDescription = settlementTransaction.getDescription();
			double settlementCurrentBalance = settlementTransaction.getCurrentBalance();
			PQTransaction settlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail settlementAccountDetail = settlement.getAccountDetail();
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				settlementTransaction1.setAmount(settlementTransactionAmount);
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setDescription(settlementDescription);
				settlementTransaction1.setService(senderService);
				settlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				settlementTransaction1.setDebit(false);
				settlementTransaction1.setAccount(accountDetail);
				settlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				settlementTransaction1.setStatus(Status.Success);
				settlementTransaction1.setDescription(description);
				logger.error("Debit settlement 2 success ");
				settlementAccountDetail.setBalance(settlementCurrentBalance);
				pqAccountDetailRepository.save(settlementAccountDetail);
				pqTransactionRepository.save(settlementTransaction1);
				// userApi.updateBalance(settlementCurrentBalance, settlement);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverUsername = receiver.getUsername();
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setDescription(description);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = pqTransactionRepository.save(receiverTransaction);
				// userApi.updateBalance(receiverCurrentBalance, receiver);
				logger.error("Receiver BillPayment success ");
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, receiver,
							senderTransaction," ",null);
				}
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		System.err.println("commissionTransaction :: " + commissionTransaction);
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				System.err.println("commissionAccount :: " + commissionAccount);
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				System.err.println("commissionAccountDetail :: " + commissionAccountDetail.getId());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setDescription(description);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				logger.error("Commission success ");
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				pqTransactionRepository.save(commissionTransaction);
				// userApi.updateBalance(commissionCurrentBalance,
				// commissionAccount);
			}
		}
	}

	@Override
	public void failedSavaariBooking(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			/*
			 * double netTransactionAmount = senderTransaction.getAmount();
			 * double netCommissionValue =
			 * commissionApi.getCommissionValue(senderCommission,
			 * netTransactionAmount);
			 */
			double netTransactionAmount = 0.0;
			double netCommissionValue = 0.0;
			double senderCurrentBalance = 0;
			// double senderUserBalance =
			// sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if (senderTransaction.getStatus().equals(Status.Initiated)){

				List<PQTransaction> lastTransList = pqTransactionRepository
						.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance = lastTrans.getCurrentBalance();
					System.err.println("Last Transation current Balance == " + senderCurrentBalance);
				}
				// senderCurrentBalance = senderUserBalance +
				// netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Failed);
				logger.error("Sender Bill Payment Reversed");
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				pqTransactionRepository.save(senderTransaction);
				// userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
//						mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
//								sender, senderTransaction, receiverUsername,null);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				logger.error("Debit settlement 4 success ");
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				pqTransactionRepository.save(debitSettlementTransaction1);
				// userApi.updateBalance(debitSettlementCurrentBalance,
				// debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				logger.error("Receiver Bill Payment Failed");
				pqTransactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
//				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
//						sender, senderTransaction, receiverUsername,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				logger.error("Commission Bill Payment Failed");
				pqTransactionRepository.save(commissionTransaction);
			}
		}
	}
	
	@Override
	public void CancelledSavaariBooking(String transactionRefNo,double amount,String description) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			/*
			 * double netTransactionAmount = senderTransaction.getAmount();
			 * double netCommissionValue =
			 * commissionApi.getCommissionValue(senderCommission,
			 * netTransactionAmount);
			 */
			double netTransactionAmount = 0.0;
			double netCommissionValue = 0.0;
			double senderCurrentBalance = 0;
			// double senderUserBalance =
			// sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if (senderTransaction.getStatus().equals(Status.Success)){
				List<PQTransaction> lastTransList = pqTransactionRepository
						.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance = lastTrans.getCurrentBalance();
					System.err.println("Last Transation current Balance == " + senderCurrentBalance);
				}
				senderTransaction.setCurrentBalance(senderCurrentBalance+amount);
				senderTransaction.setStatus(Status.Cancelled);
				senderTransaction.setDescription(description);
				logger.error("Sender Bill Payment Reversed");
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance+amount);
				pqAccountDetailRepository.save(senderAccount);
				pqTransactionRepository.save(senderTransaction);
				// userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
//						mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
//								sender, senderTransaction, receiverUsername,null);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				System.err.println("balance ::" +receiverCurrentBalance);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance-amount);
				System.err.println("current balance ::" + receiverTransaction.getCurrentBalance());
				PQAccountDetail senderAccount = receiver.getAccountDetail();
				senderAccount.setBalance(receiverCurrentBalance-amount);
				System.err.println("acc balance ::" + senderAccount.getBalance());
				receiverTransaction.setStatus(Status.Cancelled);
				receiverTransaction.setDescription(description);
				logger.error("Receiver Bill Payment Failed");
				pqTransactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
//				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
//						sender, senderTransaction, receiverUsername,null);
			}
		}

	}
	
    private long calculatePoints(double amount) {
        long points = 0;
        if (amount >= 100) {
            points = (long) amount / 100;
        }
        return points;

    }
}
