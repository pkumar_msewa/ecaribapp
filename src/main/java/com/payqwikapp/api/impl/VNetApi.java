package com.payqwikapp.api.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IVNetApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.FrmLoadMoneyDTO;
import com.payqwikapp.model.VNetDTO;
import com.payqwikapp.model.VNetResponse;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.StartupUtil;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;

public class VNetApi implements IVNetApi{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ITransactionApi transactionApi;
    private final IFRMApi frmApi;

    public VNetApi(ITransactionApi transactionApi,IFRMApi frmApi) {
        this.transactionApi = transactionApi;
        this.frmApi=frmApi;
    }

	@Override
	public ResponseDTO processRequest(VNetDTO dto, String username, PQService service, User u) {
		ResponseDTO result = new ResponseDTO();
		FrmLoadMoneyDTO loadmoney = new FrmLoadMoneyDTO();
		FRMResponseDTO frmResp = new FRMResponseDTO();
		String transactionRefNo = "" + System.currentTimeMillis();
		VNetDTO newDTO = ConvertUtil.convertVNet(dto, transactionRefNo);
		String description = "Load Money through V-Net Banking";
		loadmoney.setTransactionRefNo(transactionRefNo);
		loadmoney.setAmount(Double.parseDouble(dto.getAmount()));
		loadmoney.setServiceCode(service.getCode());
		frmResp.setSuccess(true);
		if (DeploymentConstants.PRODUCTION) {
			frmResp = frmApi.decideLoadMoney(loadmoney, u);
		}
		if (frmResp.isSuccess()) {
			transactionApi.initiateLoadMoney(Double.parseDouble(dto.getAmount()), description, service,
					transactionRefNo, username);
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Success");
			result.setDetails(newDTO);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Cause of some security reason,Your Transaction has been blocked.");
		}
		return result;
	}

    @Override
    public ResponseDTO handleResponse(VNetResponse dto, PQService service, User u) {
        ResponseDTO result = new ResponseDTO();
		FrmLoadMoneyDTO loadmoney = new FrmLoadMoneyDTO();
		FRMResponseDTO frmResp = new FRMResponseDTO();
        String paid = dto.getPaid();
    	loadmoney.setTransactionRefNo(dto.getPrn());
		loadmoney.setAmount(Double.parseDouble(dto.getAmt()));
		loadmoney.setServiceCode(service.getCode());
        if (DeploymentConstants.PRODUCTION) {
			frmResp = frmApi.decideLoadMoney(loadmoney, u);
		}
        if(frmResp.isSuccess() && paid.equalsIgnoreCase("Y")){
            transactionApi.successLoadMoney(dto.getPrn(), dto.getBid());
            result.setStatus(ResponseStatus.SUCCESS);
            result.setMessage("Load Money Successful with Bank Reference No. :"+dto.getBid());
        }else{
            transactionApi.failedLoadMoney(dto.getPrn());
            result.setStatus(ResponseStatus.FAILURE);
            result.setMessage("Load Money failed");
        }
        return result;
    }
    
    
    @Override
    public VNetDTO processRequestFlight(VNetDTO dto,String username,PQService service) {
        String transactionRefNo = ""+System.currentTimeMillis();
        VNetDTO newDTO = ConvertUtil.convertVNet(dto,transactionRefNo);
        newDTO.setTransactionRefNo(transactionRefNo);
        String description = "Booking of flight through V-Net Banking of Rs. "+dto.getAmount();
        PQTransaction pqTransaction= transactionApi.initiateFlightPaymentVnet(Double.parseDouble(dto.getAmount()),description,service,transactionRefNo,username,StartupUtil.FLIGHT_BOOKING);
        newDTO.setPqTransaction(pqTransaction);
        return newDTO;
    }

    @Override
    public ResponseDTO handleResponseFlight(VNetResponse dto) {
        ResponseDTO result = new ResponseDTO();
        String paid = dto.getPaid();
        if(paid.equalsIgnoreCase("Y")){
            transactionApi.successFlightPaymentVnet(dto.getPrn(),dto.getBid());
            result.setTxnId(dto.getPrn());
            result.setStatus(ResponseStatus.SUCCESS);
            result.setMessage("Load Money Successful with Bank Reference No. :"+dto.getBid());
        }else{
            transactionApi.failedFlightVnet(dto.getPrn());
            result.setStatus(ResponseStatus.FAILURE);
            result.setMessage("Load Money failed");
        }
        return result;
    }
}
