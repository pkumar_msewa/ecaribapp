package com.payqwikapp.api.impl;

import java.util.Date;
import java.util.List;

import com.payqwikapp.api.IPartnerApi;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PartnerDetail;
import com.payqwikapp.entity.PartnerInstall;
import com.payqwikapp.model.PDeviceUpdateDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.error.PDeviceUpdateError;
import com.payqwikapp.repositories.PartnerDetailRepository;
import com.payqwikapp.repositories.PartnerInstallRepository;
import com.payqwikapp.validation.CommonValidation;

public class PartnerApi implements IPartnerApi{
    private final PartnerDetailRepository partnerDetailRepository;
    private final PartnerInstallRepository partnerInstallRepository;

    public PartnerApi(PartnerDetailRepository partnerDetailRepository, PartnerInstallRepository partnerInstallRepository) {
        this.partnerDetailRepository = partnerDetailRepository;
        this.partnerInstallRepository = partnerInstallRepository;
    }

    @Override
    public PDeviceUpdateError saveDetails(PDeviceUpdateDTO dto) {
        PDeviceUpdateError error = new PDeviceUpdateError();
        if(!CommonValidation.isNull(dto.getApiKey())) {
            PartnerDetail partner = partnerDetailRepository.findByKey(dto.getApiKey());
            if(partner != null) {
                    if(!CommonValidation.isNull(dto.getImei())) {
                        PartnerInstall install = partnerInstallRepository.findByIMEI(dto.getImei());
                        if(install == null) {
                            install = new PartnerInstall();
                            install.setDevice(dto.getDevice());
                            install.setImei(dto.getImei());
                            install.setModel(dto.getModel());
                            install.setPartnerDetail(partner);
                            partnerInstallRepository.save(install);
                            error.setSuccess(true);
                            error.setMessage("Details Updated");
                        } else {
                            error.setSuccess(false);
                            error.setMessage("Already Updated");
                        }
                    } else {
                        error.setSuccess(false);
                        error.setMessage("Enter IMEI");
                    }
            } else {
                error.setSuccess(false);
                error.setMessage("Invalid API Key");
            }
        } else {
            error.setSuccess(false);
            error.setMessage("Enter API Key");
        }
        return error;
    }
    
    @Override
    public List<PartnerInstall> listInstallsByPartnerName(PartnerDetail partnerDetail) {
        return partnerInstallRepository.findByPartner(partnerDetail);
    }
    
    @Override
    public List<PartnerInstall> listInstallsByPartnerNameFilter(PartnerDetail partnerDetail,Date from,Date to) {
        return partnerInstallRepository.findByPartnerFilter(partnerDetail,from,to);
    }
}
