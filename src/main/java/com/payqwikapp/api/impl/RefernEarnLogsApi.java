package com.payqwikapp.api.impl;

import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.RefernEarnRepository;
import com.payqwikapp.validation.TransactionValidation;

public class RefernEarnLogsApi implements IRefernEarnLogsApi {

	private final RefernEarnRepository referNearnRepository;
	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final PQServiceRepository serviceRepository;
	private final TransactionValidation transactionValidation;

	public RefernEarnLogsApi(RefernEarnRepository referNearnRepository, ITransactionApi transactionApi,
			IUserApi userApi, PQServiceRepository serviceRepository, TransactionValidation transactionValidation) {
		super();
		this.referNearnRepository = referNearnRepository;
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.serviceRepository = serviceRepository;
		this.transactionValidation = transactionValidation;
	}

	@Override
	public RefernEarnlogs getlogs(User user) {
		return referNearnRepository.getLogsByUser(user);
	}

	@Override
	public boolean sendRefernEarnMoney(double amount, User user) {
		String transactionRefNo = System.currentTimeMillis() + "";
		boolean isValid = false;
		RefernEarnlogs log = referNearnRepository.getLogsByUser(user);
		PQService service = serviceRepository.findServiceByCode("RFNE");
		User sender=userApi.findByUserName("9482706183");
		if (log != null) {
			if (log.isHasEarned() == false) {
				TransactionError error = transactionValidation.refernEarnValidation(String.valueOf(amount),user.getUsername(),"9482706183", service);
				if (error != null) {
					if (error.isValid()) {
						transactionApi.initiatiateRefernEarn(amount, "Earned Rs 20 by referring", transactionRefNo,service, user.getUsername(), "9482706183");
						transactionApi.successRefernEarn(transactionRefNo);
						isValid = true;
					}
				}
			}
		}
		return isValid;
	}

}
