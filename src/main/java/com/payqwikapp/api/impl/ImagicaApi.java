package com.payqwikapp.api.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.api.IImagicaApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.ImagicaOrders;
import com.payqwikapp.entity.ImagicaSession;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.model.ImagicaAuthDTO;
import com.payqwikapp.model.ImagicaLoginRequest;
import com.payqwikapp.model.ImagicaLoginResponse;
import com.payqwikapp.model.ImagicaOrder;
import com.payqwikapp.model.ImagicaOrderResponse;
import com.payqwikapp.model.ImagicaPaymentDTO;
import com.payqwikapp.model.ImagicaPaymentResponse;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.ImagicaOrdersRepository;
import com.payqwikapp.repositories.ImagicaSessionRepository;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class ImagicaApi implements IImagicaApi{

    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final static String LOGIN_URL=DeploymentConstants.WEB_URL+"/Imagica/Login";
    private ImagicaSessionRepository imagicaSessionRepository;
    private ITransactionApi transactionApi;
    private ImagicaOrdersRepository imagicaOrdersRepository;
    private IUserApi userApi;

    public ImagicaApi(ImagicaSessionRepository imagicaSessionRepository, ITransactionApi transactionApi, ImagicaOrdersRepository imagicaOrdersRepository, IUserApi userApi) {
        this.imagicaSessionRepository = imagicaSessionRepository;
        this.transactionApi = transactionApi;
        this.imagicaOrdersRepository = imagicaOrdersRepository;
        this.userApi = userApi;
    }

    @Override
    public void createSession() {
        ImagicaSession session = imagicaSessionRepository.getLatestCredentials();
        if(session != null){
            if(session.getLoginTime() != null) {
                if(isSessionTimeout(session.getLoginTime())) {
                    session.setStatus(Status.Inactive);
                    ImagicaLoginRequest loginRequest = new ImagicaLoginRequest();
                    loginRequest.setUsername(session.getUsername());
                    logger.info(loginRequest.getUsername());
                    loginRequest.setPassword(SecurityUtil.decryptData(session.getPassword()));
                    ImagicaLoginResponse response = requestLogin(loginRequest);
                    if(response.isSuccess()) {
                        session.setToken(response.getToken());
                        session.setSessionId(response.getSessionId());
                        session.setLoginTime(response.getLoginDate());
                        session.setStatus(Status.Active);
                        session.setSessionName(response.getSessionName());
                        imagicaSessionRepository.save(session);
                    }
                }
            }

        }
    }

    @Override
    public long getSessionValidityMins() {
        return 6*60;
    }

    @Override
    public boolean isSessionTimeout(Date tokenGenerationTime) {
        boolean valid = false;
        long currentMillis = System.currentTimeMillis();
        long previousMillis = tokenGenerationTime.getTime();
        long time = (currentMillis-previousMillis)/(1000*60);
        if(time >= getSessionValidityMins()) {
            valid = true;
           
        }
        return valid;
    }

    @Override
    public ImagicaLoginResponse requestLogin(ImagicaLoginRequest request) {
        ImagicaLoginResponse response = new ImagicaLoginResponse();
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(LOGIN_URL);
            ClientResponse result = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).header("x-api-key", SecurityUtil.getKey()).post(ClientResponse.class, request.toJSON());
            String strResponse = result.getEntity(String.class);
             if(result.getStatus() == 200) {
                JSONObject object = new JSONObject(strResponse);
                if(object != null) {
                    boolean success = object.getBoolean("success");
                    response.setSuccess(success);
                    if(success) {
                        response.setSessionName(JSONParserUtil.getString(object, "sessionName"));
                        response.setToken(JSONParserUtil.getString(object, "token"));
                        response.setSessionId(JSONParserUtil.getString(object,"sessionId"));
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(JSONParserUtil.getLong(object, "loginTime"));
                        response.setLoginDate(calendar.getTime());
                    }
                }
            } else {
                response.setSuccess(false);
                response.setMessage("Service Unavailable Response Status "+result.getStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setSuccess(false);
            response.setMessage("Service Unavailable");
        }

        return response;
    }

    @Override
    public ImagicaAuthDTO getLatestCredentials() {
        ImagicaAuthDTO authDTO = new ImagicaAuthDTO();
        ImagicaSession session = imagicaSessionRepository.getLatestCredentials();
        if(session != null) {
            if(session.getStatus().equals(Status.Active)) {
                if(!isSessionTimeout(session.getLoginTime())) {
                    authDTO.setActive(true);
                    authDTO.setCookie(session.getSessionName()+"="+session.getSessionId());
                    authDTO.setToken(session.getToken());
                } else {
                    createSession();
                }
            }
        }
        return authDTO;
    }

    @Override
    public ImagicaOrderResponse placeOrder(ImagicaOrder order, PQService service,String senderUsername) {
    	
        String transactionRefNo = String.valueOf(System.currentTimeMillis());
        ImagicaOrderResponse response =  new ImagicaOrderResponse();
        PQTransaction valid = initiateTransaction(transactionRefNo,order.getTotalAmount(),service,senderUsername);
        if(valid != null) {
            ImagicaOrders orders = saveImagicaOrders(order,valid);
            if(orders != null) {
                response.setOrderId(orders.getOrderId());
                response.setUid(orders.getUid());
                response.setSuccess(true);
                response.setMessage("Transaction Initiated");
                Double amt=order.getBaseAmount()+order.getSbCess()+order.getServiceTax();
                double finalAmt= BigDecimal.valueOf(amt).setScale(2, RoundingMode.HALF_UP).doubleValue();
                response.setAmount(Double.toString(finalAmt));
                response.setTransactionId(transactionRefNo);
            }else {
                response.setSuccess(false);
                response.setMessage("Order not saved,Try again later");
            }
        }else {
            response.setSuccess(false);
            response.setMessage("Transaction  not saved,Try again later");
        }
        return response;
    }

    @Override
    public PQTransaction initiateTransaction(String transactionRefNo,double amount,PQService service, String senderUsername) {
        String description = "Booking by Adlabs Imagica of Rs."+amount;
        return transactionApi.initiateImagicaPayment(amount,description,service,transactionRefNo,senderUsername, StartupUtil.IMAGICA);
    }

    @Override
    public double calculateTotalAmount(ImagicaOrder order) {
        return order.getBaseAmount()+order.getServiceTax()+order.getSbCess()+order.getKkCess();
    }

    @Override
    public ImagicaOrders saveImagicaOrders(ImagicaOrder order,PQTransaction transaction) {
        ImagicaOrders orders = imagicaOrdersRepository.findByOrderId(order.getOrderId());
        if (orders == null) {
            orders = new ImagicaOrders();
            orders.setBaseAmount(order.getBaseAmount());
            orders.setCGST(order.getServiceTax());
            orders.setSGST(order.getSbCess());
            orders.setOrderId(order.getOrderId());
            orders.setUid(order.getUid());
            orders.setTransaction(transaction);
            imagicaOrdersRepository.save(orders);
        }
        return orders;
    }

    @Override
    public ImagicaPaymentResponse processPayment(ImagicaPaymentDTO dto) {
        ImagicaPaymentResponse response = new ImagicaPaymentResponse();
        if(dto.isSuccess()) {
            PQTransaction senderTransaction = transactionApi.successImagicaPayment(dto.getTransactionRefNo());
            if(senderTransaction != null) {
                if(senderTransaction.getStatus().equals(Status.Success)) {
                    ImagicaOrders orders = imagicaOrdersRepository.findByTransaction(senderTransaction);
                    if(orders != null ) {
                        orders.setProfileId(dto.getProfileId());
                        imagicaOrdersRepository.save(orders);
                        response.setSuccess(true);
                        response.setMessage("Transaction Successful");
                    } else {
                        response.setSuccess(false);
                        response.setMessage("Order Details not found");
                    }
                } else {
                    response.setSuccess(false);
                    response.setMessage("Transaction is "+senderTransaction.getStatus());
                }
            }else {
                response.setSuccess(false);
                response.setMessage("Transaction not found with this reference no");
            }
        } else {
            transactionApi.failedImagicaPayment(dto.getTransactionRefNo());
            response.setSuccess(false);
            response.setMessage("Transaction Failed");
        }
        return response;
    }
}
