package com.payqwikapp.api.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.payqwikapp.api.ITKPlaceOrderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TrainList;
import com.payqwikapp.entity.TravelkhanaItems;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TKOrderDTO;
import com.payqwikapp.model.TKPlaceOrderRequest;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.TKOrderDetailRepository;
import com.payqwikapp.repositories.TrainListRepository;
import com.payqwikapp.repositories.TravelkhanaItemsRepository;
import com.payqwikapp.util.DeploymentConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class TKPlaceOrderApi implements ITKPlaceOrderApi{

//	private final A dlabsProductRepository adlabsProductRepository;
	private final ITransactionApi transactionApi;
	private final TravelkhanaItemsRepository  travelkhanaItemsRepository;
	private final TKOrderDetailRepository tKOrderDetailRepository;
	private final TrainListRepository trainListRepository;
	
//	private final static String WEB_URL="http://localhost:8080/Api/v1/User/Android/en/Travelkhana/ApiOrderSuccess";           
	private final static String WEB_URL= DeploymentConstants.WEB_URL+"/Api/v1/User/Android/en/Travelkhana/ApiOrderSuccess";
	private final static String WEB_URL_TK = DeploymentConstants.WEB_URL+"/Api/v1/User/Android/en/Travelkhana/getTrainListFromTK";
		public TKPlaceOrderApi(ITransactionApi transactionApi,TravelkhanaItemsRepository  travelkhanaItemsRepository,TKOrderDetailRepository tKOrderDetailRepository,TrainListRepository trainListRepository) {
	//s	this.adlabsProductRepository = adlabsProductRepository;
		this.transactionApi = transactionApi;
		this.travelkhanaItemsRepository = travelkhanaItemsRepository;
		this.tKOrderDetailRepository = tKOrderDetailRepository;
		this.trainListRepository = trainListRepository;
	}
	@Override
	public String tkpaymentInitiate(TKPlaceOrderRequest tkReq, User user, PQService service){
		
		String username = user.getUsername();
		System.err.println(" UserName:::::::::::::::"+user.getUsername());
		String transactionRefNo = System.currentTimeMillis() + "";
		System.out.println("transactionRefNo="+transactionRefNo);
		transactionApi.initiateTKPayment(tkReq.getTotalCustomerPayable(),
				"Travelkhana Place Order on Rs " + tkReq.getTotalCustomerPayable() , service, transactionRefNo,username);
		
	//	boolean status=false;
		TKOrderDetail orderDetails = new TKOrderDetail();
		orderDetails.setName(tkReq.getName());
		orderDetails.setContact_no(tkReq.getContact_no());
		orderDetails.setMail_id(tkReq.getMail_id());
		orderDetails.setCustomer_comment(tkReq.getCustomer_comment());
		orderDetails.setOrder_outlet_id(tkReq.getOrder_outlet_id());
		orderDetails.setTrain_number(tkReq.getTrain_number());
		orderDetails.setCoach(tkReq.getCoach());
		orderDetails.setSeat(tkReq.getSeat());
		orderDetails.setTotalCustomerPayable(tkReq.getTotalCustomerPayable());
		orderDetails.setDate(tkReq.getDate());
		orderDetails.setStation_code(tkReq.getStation_code());
		orderDetails.setEta(tkReq.getEta());
		orderDetails.setCod(tkReq.getCod());
		orderDetails.setPnr(tkReq.getPnr());
		orderDetails.setOrderStatus(Status.Processing);
		orderDetails.setTransactionRefNo(transactionRefNo);
	    orderDetails.setUser(user);
		orderDetails.setTxnStatus(Status.Initiated);
		tKOrderDetailRepository.save(orderDetails);
		
		
		ArrayList<TravelkhanaItems> itemsObj = new ArrayList<TravelkhanaItems>();
		for(int i=0;i<tkReq.getItems().size();i++){
		  TravelkhanaItems items = new TravelkhanaItems();
		  items.setItemid(tkReq.getItems().get(i).getItemId());
		  items.setQuantity(tkReq.getItems().get(i).getQuantity());
		  items.setTkOrderId(orderDetails);
		  travelkhanaItemsRepository.save(items);
		//  itemsObj.add(items);
		}
		
		/*items.setTkOrderId(orderDetails);
		travelkhanaItemsRepository.save(itemsObj);*/
		return transactionRefNo;
	}
	@Override
	public TKOrderDTO tkpaymentSuccess(TKPlaceOrderRequest dto, String username, PQService service, User u,String transactionRefNo) {
		TKOrderDTO resp = new TKOrderDTO();
		 try {
			    JSONObject payload = new JSONObject();
				String sessionId = dto.getSessionId();
			
				System.err.println("payload for SUCCESS amount for placeOrder IN WEB+++++++++++++++++"+dto.getJsonRequest());
				
	            Client client = Client.create();
	            client.addFilter(new LoggingFilter(System.out));
	 //           WebResource webResource = client.resource(WEB_URL);
	            WebResource webResource = client.resource(WEB_URL);
	            ClientResponse result = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class,dto.getJsonRequest());
	            String strResponse = result.getEntity(String.class);
	            System.out.println("result.getStatus()="+result.getStatus());
	            System.err.println("response ::" + strResponse);
	             if(result.getStatus()!= 200) {
	            	resp.setSuccess(false);
	 				resp.setCode("F00");
	 				resp.setMessage(strResponse);
	 				System.err.println("Failed ======+++");
	             }
	             else{
	                JSONObject object = new JSONObject(strResponse);
	                if(object != null) {
						//	final String status = (String) object.get("status");
							final String code = (String) object.get("code");
							final String message = (String) object.get("message");
							TKOrderDetail orderDetail = tKOrderDetailRepository.getDetailsByTxnRefNo(transactionRefNo);
	                    	if (code.equalsIgnoreCase("S00"))
	                    		{
	                    		//    final int userOrderId2 = (int) object.get("userOrderId");
	                    		    final Long userOrderId = object.getLong("userOrderId");
	                    			System.err.println("########################################SUCCESS############################################");
	                    			transactionApi.successTKPayment(transactionRefNo,userOrderId,service,dto.getTotalCustomerPayable(),orderDetail);
	                    			tKOrderDetailRepository.updateTKOrderDetailsByTxnRefNo(Status.Success,userOrderId,transactionRefNo);
	                    			resp.setSuccess(true);
	                    			resp.setUserOrderId(userOrderId);
	                    			resp.setMessage("Order placed successfully"); 
	                    		}
	                    	else {
	                    			System.err.println("######################################FAILED##############################################");
	                    			transactionApi.failedTKPayment(transactionRefNo,orderDetail);
	                    			long userOrderId=0;
	                    			tKOrderDetailRepository.updateTKOrderDetailsByTxnRefNo(Status.Failed,userOrderId,transactionRefNo);
	                    			resp.setSuccess(false);
	                    			resp.setMessage(message); 
	                    		}
	                    	resp.setCode(code);
	                    	
	                    //	resp.setResponse(strResponse);
	             		 }  
	                }
	         
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
		return resp;
	}
	
	@Override
	public void getTrainListFromTK() {
		TKOrderDTO resp = new TKOrderDTO();
		
	  try {
		   Client client = Client.create();
		   client.addFilter(new LoggingFilter(System.out));
		   WebResource webResource = client.resource(WEB_URL_TK);
		   ClientResponse result = webResource.accept(MediaType.APPLICATION_JSON).
				   type(MediaType.APPLICATION_JSON).post(ClientResponse.class);
		   String strResponse = result.getEntity(String.class);
		   
		   if(result.getStatus()!=200){
			   resp.setCode("F00");
			   resp.setMessage("service unavailable");
			   resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			   resp.setSuccess(false);
		   }
		   else{
			   JSONObject jobj = new JSONObject(strResponse);
			   final String code = jobj.getString("code");
			   final String message = jobj.getString("message");
			   
			   if(code.equalsIgnoreCase("S00")){
				   final String detail = jobj.getString("listOfTrain");
				   System.out.println("detail=="+detail);
				   JSONArray jArr = new JSONArray(detail);
				   for(int i=0;i<jArr.length();i++){
					   TrainList train = new TrainList();
					   String trainNo = jArr.getJSONObject(i).getString("trainNumber");
					   String trainName = jArr.getJSONObject(i).getString("trainName");
					   train.setTrainNumber(trainNo);
					   train.setTrainName(trainName);
					   trainListRepository.save(train);
				   }
			   }
		   }
		   
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@Override
	public List<TKOrderDetail> myorder(User user){
		List<TKOrderDetail> orderDetails =  tKOrderDetailRepository.findByUser(user);
		return orderDetails;
		
	}
	
	@Override
	public List<TrainList>getTrainList(){
		return tKOrderDetailRepository.getTrainlist();
	}
	/*@Override
	public List<TKOrderDetail> getTxnStatus(User user){
		List<TKOrderDetail> orderDetails =  tKOrderDetailRepository.findByUser(user);
		return orderDetails;
		
	}
	
	*/
	
	@Override
	public Page<TKOrderDetail> getOrderDetailsForAdmin(Pageable pageable) {
		Page<TKOrderDetail>orderDetails=tKOrderDetailRepository.getByStatus(pageable);
		
		return orderDetails;
	}
	
	@Override
	public List<TKOrderDetail> getOrderDetailsForAdmin() {
		List<TKOrderDetail>orderDetails=tKOrderDetailRepository.getByStatus();
		
		return orderDetails;
	}

	
	@Override
	public List<TKOrderDetail>getOrderDetailsByDateForAdmin(Date from, Date to){
		List<TKOrderDetail>orderDetails = tKOrderDetailRepository.getDetailsBydate(from,to);
		System.out.println("orderDetails=="+orderDetails);
		return orderDetails;
	}
}

