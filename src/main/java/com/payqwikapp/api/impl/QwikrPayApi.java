package com.payqwikapp.api.impl;

import com.instantpay.model.request.GiftCartOrderRequest;
import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.IQwikrPayApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.GciProduct;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.StartupUtil;

public class QwikrPayApi implements IQwikrPayApi{

	private final ITransactionApi transactionApi;
	private final IMailSenderApi mailSenderApi;
	private final ISMSSenderApi smsSenderApi;
	
	
	public QwikrPayApi(ITransactionApi transactionApi, IMailSenderApi mailSenderApi, ISMSSenderApi smsSenderApi) {
		super();
		this.transactionApi = transactionApi;
		this.mailSenderApi = mailSenderApi;
		this.smsSenderApi = smsSenderApi;
	}

	@Override
	public String initiateQwikrPayPayment(QwikrPayRequest dto, String username, PQService service) {

		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.initiateQwikrPayPayment(Double.parseDouble(dto.getAmount()),
				" QWIKRPAY Payment of Rs " + dto.getAmount()  , service, transactionRefNo, username,
				StartupUtil.QWIKR_PAY,dto.getOrderId());

		return transactionRefNo;
	}

	@Override
	public TransactionResponse successQwikrPayPayment(QwikrPayRequest dto, String username, PQService service,User user) {
		// TODO Auto-generated method stub
		TransactionResponse resp=new TransactionResponse();
			if(dto.getCode().equalsIgnoreCase("S00")){
			transactionApi.successQwikrPayPayment(dto.getTransactionRefNo());
			//mailSenderApi.sendQwikrPayMail("VPayQwik Gift Card", MailTemplate.QWIK_PAY, dto, user);
			//smsSenderApi.sendUserQWIKRSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.QWIK_PAY, user,  dto);
			resp.setSuccess(true);
			
		} else {
			transactionApi.failedQwikrPayPayment(dto.getTransactionRefNo());
			resp.setSuccess(false);
		}

		return resp;
	}


}
