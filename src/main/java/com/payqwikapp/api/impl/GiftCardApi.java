package com.payqwikapp.api.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikapp.api.IGiftCardApi;
import com.payqwikapp.entity.GciProduct;
import com.payqwikapp.entity.GiftCardRequestLog;
import com.payqwikapp.entity.GiftCardToken;
import com.payqwikapp.entity.MTRequestLog;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.GiftCardAuthDTO;
import com.payqwikapp.model.GiftCardTokenResponse;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.WoohooTransactionRequest;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.GciProductRepository;
import com.payqwikapp.repositories.GiftCardRequestLogRepository;
import com.payqwikapp.repositories.GiftCardTokenRepository;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class GiftCardApi implements IGiftCardApi {

	private GiftCardTokenRepository giftCardTokenRepository;
	private TransactionApi transactionApi;
	private GciProductRepository gciProductRepository;
	private GiftCardRequestLogRepository giftCardRequestLogRepository;

	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private final SimpleDateFormat dateOnly = new SimpleDateFormat("dd/MM/yyyy");

	public GiftCardApi(GiftCardTokenRepository giftCardTokenRepository, TransactionApi transactionApi,
			GciProductRepository gciProductRepository,GiftCardRequestLogRepository giftCardRequestLogRepository) {
		this.giftCardTokenRepository = giftCardTokenRepository;
		this.transactionApi = transactionApi;
		this.gciProductRepository = gciProductRepository;
		this.giftCardRequestLogRepository= giftCardRequestLogRepository;
	}

	private final static String ADD_ORDER_URL = DeploymentConstants.WEB_URL + "/GciOrder/Transaction/Process";
	private final static String WOOHOO_ADD_ORDER_URL = DeploymentConstants.WEB_URL + "/GciOrder/Woohoo/Transaction/Process";
	private final static String LOGIN_URL = DeploymentConstants.WEB_URL + "/Gci/AccessToken";
	private final static String SECRET_KEY = "121b4b4ce66f14177905e36233a8dff13a951e305a81f395ccb049b35925e610";// live
	private final static String SECRET_ID = "68ce6e0a66ae9b3b12d83f96658d827e";// live

	@Override
	public GiftCardTokenResponse requestAccessToken(GiftCardAuthDTO dto) {
		GiftCardTokenResponse response = new GiftCardTokenResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(LOGIN_URL);
			ClientResponse result = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.header("x-api-key", SecurityUtil.getKey()).post(ClientResponse.class, payload);
			String strResponse = result.getEntity(String.class);
			if (result.getStatus() == 200) {
				org.json.JSONObject object = new org.json.JSONObject(strResponse);
				if (object != null) {
					boolean success = object.getBoolean("success");
					response.setSuccess(success);
					if (success) {
						response.setToken(JSONParserUtil.getString(object, "token"));
						String time = JSONParserUtil.getString(object, "expiryTime");
						Date expiryDate = dateFormat.parse(time);
						response.setExpiryDate(expiryDate);
					}
				}
			} else {
				response.setSuccess(false);
				response.setMessage("Service Unavailable Response Status " + result.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setMessage("Service Unavailable");
		}

		return response;
	}

	@Override
	public GiftCardToken createSessionForGiftCard(String sessionId) {
		GiftCardToken accessToken = giftCardTokenRepository.getLatestToken();
		if (accessToken == null) {
			GiftCardAuthDTO authDTO = new GiftCardAuthDTO();
			authDTO.setSessionId(sessionId);
			GiftCardTokenResponse response = requestAccessToken(authDTO);
			if (response.isSuccess()) {
				GiftCardToken access = new GiftCardToken();
				access.setToken(response.getToken());
				access.setExpiryDate(response.getExpiryDate());
				access.setSecretId(SECRET_ID);
				access.setSecretKey(SECRET_KEY);
				giftCardTokenRepository.save(access);
				authDTO.setToken(response.getToken());
				authDTO.setActive(true);
				return access;
			}
		} else if (accessToken.getExpiryDate().before(new Date())) {
			GiftCardAuthDTO authDTO = new GiftCardAuthDTO();
			authDTO.setSessionId(sessionId);
			GiftCardTokenResponse response = requestAccessToken(authDTO);
			if (response.isSuccess()) {
				accessToken.setToken(response.getToken());
				accessToken.setExpiryDate(response.getExpiryDate());
				accessToken.setSecretId(SECRET_ID);
				accessToken.setSecretKey(SECRET_KEY);
				giftCardTokenRepository.save(accessToken);
				authDTO.setToken(response.getToken());
				authDTO.setActive(true);
			}
		} else {
			GiftCardAuthDTO authDTO = new GiftCardAuthDTO();
			authDTO.setActive(true);
			authDTO.setToken(accessToken.getToken());
		}
		return accessToken;
	}

	@Override
	public GiftCardTransactionResponse processOrder(ProcessGiftCardDTO dto, User user, String transactionRefNo,
			PQService service, PQCommission commission, double netCommissionValue) {
		transactionApi.initiateGiftCartPayment(Double.parseDouble(dto.getAmount()),
				"Gift Card Payment of Rs " + dto.getAmount() + " to " + user.getUsername(), service, transactionRefNo,
				user, StartupUtil.Giftcart, commission, netCommissionValue);
		GiftCardTransactionResponse response = request(dto);
		return response;
	}

	public GiftCardTransactionResponse request(ProcessGiftCardDTO request) {
		GiftCardTransactionResponse response = new GiftCardTransactionResponse();
		try {

			String today = "";
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			today = (dateFormat.format(date));
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("orderDate", today);
			formData.add("billingName", request.getBillingName());
			formData.add("billingEmail", request.getBillingEmail());
			formData.add("billingAddressLine1", request.getBillingAddressLine1());
			formData.add("billingCity", request.getBillingCity());
			formData.add("billingState", request.getBillingState());
			formData.add("billingCountry", request.getBillingCountry());
			formData.add("billingZip", request.getBillingZip());
			formData.add("receiversName", request.getReceiversName());
			formData.add("receiversEmail", request.getReceiversEmail());
			formData.add("shippingName", request.getBillingName());
			formData.add("shippingEmail", request.getBillingEmail());
			formData.add("shippingCity", request.getBillingCity());
			formData.add("shippingState", request.getBillingState());
			formData.add("shippingCountry", request.getBillingCountry());
			formData.add("shippingAddressLine1", request.getBillingAddressLine1());
			formData.add("shippingZip", request.getBillingZip());
			formData.add("clientOrderId", request.getClientOrderId());
			formData.add("brand", request.getBrandHash());
			formData.add("amount", request.getAmount());
			formData.add("product", request.getProductType());
			formData.add("quantity", "1");
			formData.add("paymentMode", "NEFT");
			formData.add("paymentStatus", "Payment Received");
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(ADD_ORDER_URL).queryParam("token", request.getToken());
			ClientResponse clientResponse = webResource.post(ClientResponse.class, formData);
			String stringResponse = clientResponse.getEntity(String.class);
			org.json.JSONObject o = new org.json.JSONObject(stringResponse);
			if (clientResponse.getStatus() == 200) {
				if (JSONParserUtil.getBoolean(o, "success")) {
					response.setReceiptNo(JSONParserUtil.getString(o, "receiptNo"));
					response.setVoucherNumber(JSONParserUtil.getString(o, "voucherNumber"));
					response.setVoucherPin(JSONParserUtil.getString(o, "voucherPin"));
					response.setBrandName(JSONParserUtil.getString(o, "brandName"));
					response.setExpiryDate(JSONParserUtil.getString(o, "expiryDate"));
					response.setSuccess(true);
				} else {
					if (JSONParserUtil.getString(o, "errorCode")
							.equalsIgnoreCase("ORDER_TOTAL_IS_GREATER_THAN_AVAILABLE_BALANCE")) {
						response.setSuccess(false);
						response.setMessage("Transaction declined,Please try again later.");
					} else {
						response.setSuccess(false);
						response.setMessage(JSONParserUtil.getString(o, "message"));
					}

				}
			} else {
				response.setMessage("response from server" + clientResponse.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
		}
		return response;
	}

	@Override
	public void saveUserReceipt(GiftCardTransactionResponse response, User u, ProcessGiftCardDTO dto) {
		GciProduct req = new GciProduct();
		req.setAmount(dto.getAmount());
		req.setBrandName(response.getBrandName());
		req.setExpiryDate(response.getExpiryDate());
		req.setReceiptno(response.getReceiptNo());
		req.setUser(u);
		req.setVoucherNumber(response.getVoucherNumber());
		req.setVoucherPin(response.getVoucherPin());
		req.setReceiverName(dto.getReceiversName());
		req.setReceiverEmail(dto.getReceiversEmail());
		gciProductRepository.save(req);
	}

	@Override
	public OrderPlaceResponse processWoohooOrder(WoohooTransactionRequest dto, User user, String transactionRefNo,
			PQService service, PQCommission commission, double netCommissionValue) {
		transactionApi.initiateWoohooPayment(dto,
				"Woohoo Gift Card Payment of Rs " + dto.getPrice() + " to " + user.getUsername(), service,
				transactionRefNo, user, StartupUtil.WOOHOO, commission, netCommissionValue);
		OrderPlaceResponse response = placeOrder(dto);
		saveWoohooRequest(response,transactionRefNo,user.getUsername(),dto.getTelephone());
		return response;
	}

	@Override
	public OrderPlaceResponse placeOrder(WoohooTransactionRequest request) {
		// TODO Auto-generated method stub
		OrderPlaceResponse orderPlaceResponse = new OrderPlaceResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		JSONObject payload = buildOrderRequest(request);
		WebResource webResource = client.resource(WOOHOO_ADD_ORDER_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, payload);
		String strResponse = response.getEntity(String.class);
		try {
			org.json.JSONObject jsonObject = new org.json.JSONObject(strResponse);
			String message = jsonObject.getString("message");
			boolean success = jsonObject.getBoolean("success");
			int code = jsonObject.getInt("code");
			if (response.getStatus() != 200) {
				orderPlaceResponse.setMessage(message);
				orderPlaceResponse.setSuccess(success);
				orderPlaceResponse.setCode(String.valueOf(code));
			} else {
				if (success) {
					org.json.JSONObject data = jsonObject.getJSONObject("data");
					String order_status = data.getString("status");
					String order_id = data.getString("order_id");
					String transactionRefNo = data.getString("transactionRefNo");
					String terms = data.getString("termsConditions");
					org.json.JSONObject carddetails = data.getJSONObject("carddetails");
					if (carddetails != null) {
						orderPlaceResponse.setSuccess(true);
						Iterator<String> iterator = carddetails.keys();
						while (iterator.hasNext()) {
							String key = iterator.next();
							orderPlaceResponse.setCardName(key);
							org.json.JSONArray jsonArray = carddetails.getJSONArray(key);
							for (int i = 0; i < jsonArray.length(); i++) {
								org.json.JSONObject card = jsonArray.getJSONObject(i);
								orderPlaceResponse.setExpiry_date(card.isNull("expiry_date")?"NA" : card.getString("expiry_date"));
								orderPlaceResponse.setCard_price(card.isNull("card_price") ? "NA" : card.getString("card_price"));
								if(card.has("cardnumber")){
									String cardNumber=card.getString("cardnumber");
									if(StringUtils.isEmpty(cardNumber)){
										orderPlaceResponse.setCardnumber("NA");
									}else{
										orderPlaceResponse.setCardnumber(card.getString("cardnumber"));
									}
								}else{
									orderPlaceResponse.setCardnumber("NA");
								}
								if(card.has("pin_or_url")){
									String pinUrl=card.getString("pin_or_url");
									if(StringUtils.isEmpty(pinUrl)){
										orderPlaceResponse.setPin_or_url("NA");
									}else{
										orderPlaceResponse.setPin_or_url(card.getString("pin_or_url"));
									}
								}else{
									orderPlaceResponse.setPin_or_url("NA");
								}
							}
						}
					}
					orderPlaceResponse.setCode(String.valueOf(code));
					orderPlaceResponse.setMessage(message);
					orderPlaceResponse.setOrder_status(order_status);
					orderPlaceResponse.setSuccess(true);
					orderPlaceResponse.setOrderNumber(order_id);
					orderPlaceResponse.setRetrivalRefNo(transactionRefNo);
					orderPlaceResponse.setTermsAndConditions(terms);
					orderPlaceResponse.setResponseCode("00");
				} else if (!success) {
					    if (code == 200) {
						org.json.JSONObject data = jsonObject.getJSONObject("data");
						String order_status = data.getString("status");
						if (order_status.equalsIgnoreCase("Processing")) {
							String order_id = data.getString("order_id");
							String transactionRefNo = data.getString("transactionRefNo");
							orderPlaceResponse.setCode(String.valueOf(code));
							orderPlaceResponse.setMessage(message);
							orderPlaceResponse.setOrder_status(order_status);
							orderPlaceResponse.setSuccess(success);
							orderPlaceResponse.setStatus(ResponseStatus.WOOHOO_PENDING);
							orderPlaceResponse.setOrderNumber(order_id);
							orderPlaceResponse.setRetrivalRefNo(transactionRefNo);
							orderPlaceResponse.setResponseCode("11");
						} else {
							orderPlaceResponse.setSuccess(false);
							orderPlaceResponse.setMessage(message);
							orderPlaceResponse.setResponseCode("33");
						}
					} else {
						orderPlaceResponse.setSuccess(false);
						orderPlaceResponse.setCode(String.valueOf(code));
						orderPlaceResponse.setMessage(message);
						orderPlaceResponse.setResponseCode("22");
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			orderPlaceResponse.setCode("F00");
			orderPlaceResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
			orderPlaceResponse.setSuccess(false);
		}

		return orderPlaceResponse;
	}

	public static JSONObject buildOrderRequest(WoohooTransactionRequest request) {

		JSONObject jObject = new JSONObject();
		JSONObject jsonObject = new JSONObject();
		JSONObject billing = new JSONObject();
		JSONObject shipping = new JSONObject();
		JSONArray products = new JSONArray();
		try {
			billing.put("firstname", request.getFirstname());
			billing.put("lastname", request.getLastname());
			billing.put("email", request.getEmail());
			billing.put("telephone", request.getTelephone());
			billing.put("line_1", request.getLine_1());
			billing.put("line_2", request.getLine_2());
			billing.put("city", request.getCity());
			billing.put("region", request.getRegion());
			billing.put("country_id", request.getCountry_id());
			billing.put("postcode", request.getPostcode());
			shipping.put("firstname", request.getFirstname());
			shipping.put("lastname", request.getLastname());
			shipping.put("email", request.getEmail());
			shipping.put("telephone", request.getTelephone());
			shipping.put("line_1", request.getLine_1());
			shipping.put("line_2", request.getLine_2());
			shipping.put("city", request.getCity());
			shipping.put("region", request.getRegion());
			shipping.put("country_id", request.getCountry_id());
			shipping.put("postcode", request.getPostcode());
			jsonObject.put("product_id", request.getProduct_id());
			jsonObject.put("price", request.getPrice());
			jsonObject.put("qty", request.getQty());
			jsonObject.put("brandCode", request.getBrandCode());
			products.put(jsonObject);
			jObject.put("billingAddress", billing);
			jObject.put("shippingAddress", shipping);
			jObject.put("productValue", products);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jObject;
	}
	
	@Override
	public void saveWoohooRequest(OrderPlaceResponse dto, String transactionRefNo,String senderMobileNo,String receiverMobileNo) {
		GiftCardRequestLog log = giftCardRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new GiftCardRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setAmount(dto.getCard_price());
			log.setSenderMobileNo(senderMobileNo);
			log.setReceiverMobileNo(receiverMobileNo);
			log.setBrandName(dto.getCardName());
			log.setBrandNumber(dto.getCardnumber());
			log.setRetrivalRefNo(dto.getRetrivalRefNo());
			giftCardRequestLogRepository.save(log);
		}
	}
	

}
