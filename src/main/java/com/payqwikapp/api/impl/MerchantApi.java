package com.payqwikapp.api.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.*;
import com.payqwikapp.model.*;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.*;
import com.payqwikapp.util.SecurityUtil;

import org.apache.xml.serializer.utils.SystemIDResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IMerchantApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.MerchantBankDetailRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.SettlementBankDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.VisaMerchantRepository;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;

public class MerchantApi implements IMerchantApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final IUserApi userApi;
	private final PGDetailsRepository pgDetailsRepository;
	private final PQServiceRepository pqServiceRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQOperatorRepository pqOperatorRepository;
	private final ICommissionApi commissionApi;
	private final VisaMerchantRepository visaMerchantRepository;
	private final MerchantBankDetailRepository merchantBankDetailRepository;
	private final SettlementBankDetailRepository settlementBankDetailRepository;
	private final PQTransactionRepository pqTransactionRepository;
	private final VijayaBankReportRepository vijayaBankReportRepository;
	
	public MerchantApi(IUserApi userApi, UserRepository userRepository, PGDetailsRepository pgDetailsRepository,
			PQServiceRepository pqServiceRepository, PQServiceTypeRepository pqServiceTypeRepository,
			PQOperatorRepository pqOperatorRepository, ICommissionApi commissionApi,
			VisaMerchantRepository visaMerchantRepository, MerchantBankDetailRepository merchantBankDetailRepository,
			SettlementBankDetailRepository settlementBankDetailRepository,PQTransactionRepository pqTransactionRepository,VijayaBankReportRepository vijayaBankReportRepository) {
		this.userApi = userApi;
		this.pgDetailsRepository = pgDetailsRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqOperatorRepository = pqOperatorRepository;
		this.commissionApi = commissionApi;
		this.visaMerchantRepository = visaMerchantRepository;
		this.merchantBankDetailRepository = merchantBankDetailRepository;
		this.settlementBankDetailRepository = settlementBankDetailRepository;
		this.pqTransactionRepository=pqTransactionRepository;
		this.vijayaBankReportRepository= vijayaBankReportRepository;
	}
	@Override
	public ResponseDTO addMerchant(MRegisterDTO user) {
		ResponseDTO result = new ResponseDTO();
		user.setUserType(UserType.Merchant);
		String firstName = user.getFirstName();
		int length = firstName.length();
		int i = 1;
		String serviceCode = (firstName.charAt(0)+""+firstName.charAt(1)+""+firstName.charAt(length-1)).toUpperCase();
		PQService exists  = pqServiceRepository.findServiceByOperatorCode(serviceCode);
		while((exists != null) && (i <= length)){
			serviceCode = (serviceCode+firstName.charAt(i)).toUpperCase();
			exists  = pqServiceRepository.findServiceByOperatorCode(serviceCode);
			i++;
		}
		try {
			userApi.saveMerchant(user);
			PQService service = new PQService();
			PQOperator operator = pqOperatorRepository.findOperatorByName("VPayQwik");
			PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName(user.getServiceName());
			if(serviceType != null){
				service.setServiceType(serviceType);
			}
			if(operator != null){
				service.setOperator(operator);
			}
			service.setCode("V"+serviceCode);
			service.setStatus(Status.Active);
			service.setOperatorCode(serviceCode);
			service.setName(firstName+" Payment");
			service.setDescription("Payment to "+firstName);
			service.setMinAmount(1);
			service.setMaxAmount(100000);
			pqServiceRepository.save(service);
			PQCommission commission  = new PQCommission();
			commission.setMinAmount(user.getMinAmount());
			commission.setMaxAmount(user.getMaxAmount());
			commission.setFixed(user.isFixed());
			commission.setType("POST");
			commission.setValue(user.getValue());
			commission.setService(service);
			commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
			if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
				commissionApi.save(commission);
			}
			PGDetails pgDetails = new PGDetails();
			pgDetails.setService(service);
			pgDetails.setSuccessURL(user.getSuccessURL());
			pgDetails.setReturnURL(user.getFailureURL());
			pgDetails.setIpAddress(user.getIpAddress());
			pgDetails.setPaymentGateway(user.isPaymentGateway());
			pgDetails.setStore(user.isStore());
			pgDetails.setToken(SecurityUtil.md5(user.getIpAddress()+"|"+user.getContactNo()));
			User merchant = userApi.findByUserName(user.getEmail());
			pgDetails.setUser(merchant);
			pgDetailsRepository.save(pgDetails);
			result.setMessage("Merchant Added Successfully");
			result.setStatus(ResponseStatus.SUCCESS);
			result.setDetails("Merchant Added and id is "+merchant.getId()+" and secret key is "+pgDetails.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	//donatee
		
	@Override
	public List<MerchantDTO> getAll(String type,String to,String from) {
		List<PGDetails> pgList = null;
		
		SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd");
		Date toDate = null;
		Date fromDate = null;
		
		if (to!=null && from!=null) {
			
			/*String a[] = from.split(" ");
			a[1] = " 23:59";
			from = a[0] + a[1];*/

			try {
				fromDate = dateFilter.parse(from);
				toDate = dateFilter.parse(to);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if ("STORE".equalsIgnoreCase(type)) {
			pgList = pgDetailsRepository.findMerchantsOfStore(true);
			
		}else if ("PG".equalsIgnoreCase(type)) {
			pgList = pgDetailsRepository.findMerchantsOfPaymentGateway(true);
		}
		else if ("ALL".equalsIgnoreCase(type)) {
			
			if (toDate!=null && fromDate!=null) {
				pgList = (List<PGDetails>) pgDetailsRepository.fetchAllDetailsByDate(fromDate,toDate);
			}
			else {
				pgList = (List<PGDetails>) pgDetailsRepository.findAll();
			}
		}
		
		/*switch(type.toUpperCase()){
			case "STORE":
				pgList = pgDetailsRepository.findMerchantsOfStore(true);
				break;
			case "PG":
				pgList = pgDetailsRepository.findMerchantsOfPaymentGateway(true);
				break;
			default :
				pgList = (List<PGDetails>) pgDetailsRepository.findAll();
				break;
		}*/
		
		List<User> userList = ConvertUtil.convertFromPGDetails(pgList);
		List<MerchantDTO> merchantList = ConvertUtil.convertMerchantList(userList);
		return merchantList;
	}
	
	@Override
	public VisaMerchant findByEmail(String emailAddress) {
		return visaMerchantRepository.findByEmailAddress(emailAddress);
	}
	

	@Override
	public ResponseDTO addVisaMerchant(VisaMerchantRequest dto, User merchant) {
		ResponseDTO result = new ResponseDTO();
		try{
			MerchantBankDetail bankDetail =  new MerchantBankDetail();
			bankDetail.setMerchantAccountName(dto.getMerchantAccountName());
			bankDetail.setMerchantAccountNumber(dto.getMerchantAccountNumber());
			bankDetail.setMerchantBankLoction(dto.getMerchantBankLocation());
			bankDetail.setMerchantBankName(dto.getMerchantBankName());
			bankDetail.setMerhantIfscCode(dto.getMerchantBankIfscCode());
			merchantBankDetailRepository.save(bankDetail);
			
			SettlementBankDetail sBankDetail = new SettlementBankDetail();
			sBankDetail.setSettlementAccountName(dto.getSettlementAccountName());
			sBankDetail.setSettlementAccountNumber(dto.getSettlementAccountNumber());
			sBankDetail.setSettlementBankLoction(dto.getSettlementBankLocation());
			sBankDetail.setSettlementBankName(dto.getSettlementBankName());
			sBankDetail.setSettlementIfscCode(dto.getSettlementIfscCode());
			settlementBankDetailRepository.save(sBankDetail);
			
			VisaMerchant visaMerchant = new VisaMerchant();
			visaMerchant.setFirstName(dto.getFirstName());
			visaMerchant.setLastName(dto.getLastName());
			visaMerchant.setEmailAddress(dto.getEmailAddress());
			visaMerchant.setMobileNo(dto.getMobileNo());
			visaMerchant.setCity(dto.getCity());
			visaMerchant.setState(dto.getState());
			visaMerchant.setCountry(dto.getCountry());
			visaMerchant.setPinCode(dto.getPinCode());
			visaMerchant.setLatitude(dto.getLattitude());
			visaMerchant.setLongitude(dto.getLongitude());
			visaMerchant.setPanNo(dto.getPanNo());
			visaMerchant.setAddress1(dto.getAddress1());
			visaMerchant.setAddress2(dto.getAddress2());
			visaMerchant.setAggregator(dto.getAggregator());
			visaMerchant.setMerchantBusinessType(dto.getMerchantBusinessType());
			visaMerchant.setmBankDedail(bankDetail);
			visaMerchant.setsBankDetail(sBankDetail);
			visaMerchant.setEntityId(dto.getEntityId());
			visaMerchant.setmVisaId(dto.getMvisaId());
			visaMerchant.setUser(merchant);
			visaMerchantRepository.save(visaMerchant);
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Merchant added successfully");
			result.setDetails("Merchant added successfully");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public List<VisaMerchant> getAllVisaMerchant() {
		List<VisaMerchant> virsaMerchantList = null;
		virsaMerchantList = (List<VisaMerchant>) visaMerchantRepository.findAll();
		return virsaMerchantList;
	}

	@Override
	public ResponseDTO getEntityId(String username){
		ResponseDTO resp=new ResponseDTO();
		User user=userApi.findByUserName(username);
		if(user!=null){
		String entityId=user.getMerchantDetails().getCustomerId();
		if(entityId!=null){
		resp.setMessage("entity id fetched.....");
		resp.setDetails(entityId);
		
		
		}
		}
		return resp;
	}
	
	  @Override
	    public PQTransaction getStatusByTran(String transactionRefNo){
		  System.err.println("here"+transactionRefNo);
	    	PQTransaction transaction=pqTransactionRepository.getStatusByTransactionRefNo(transactionRefNo);
	    	System.err.println(transaction);
	    	return transaction;
	    }
	  
	@Override
	public void saveBulkFile(MerchantReportDTO bulkUpload) {
		/* TODO switch file name here */
		List<MerchantReportDetails> sendMoneyList = bulkUpload.getMobileDTO();
		Iterator<MerchantReportDetails> iterator = sendMoneyList.iterator();
		if (bulkUpload.getMobileDTO() != null && !bulkUpload.getMobileDTO().isEmpty()) {
			while (iterator.hasNext()) {
				try {
					MerchantReportDetails dto = iterator.next();
					VijayaBankReport report = new VijayaBankReport();
					report.setModeOfPayment(dto.getModeOfPayment());
					report.setConsumerType(dto.getConsumerType());
					report.setPaymentChannel(dto.getPaymentChannel());
					report.setTypeOfPayment(dto.getTypeOfPayment());
					report.setTransactionType(dto.getTransactionType());
					report.setSubDivisonCode(dto.getSubDivisonCode());
					report.setAccountId(dto.getAccountId());
					report.setTransactionRefId(dto.getTransactionRefId());
					report.setPgId(dto.getPgId());
					report.setVendorRefId(dto.getVendorRefId());
					report.setBillAmount(dto.getBillAmount());
					report.setCharges(dto.getCharges());
					report.setNetAmount(dto.getNetAmount());
					report.setTransactionDate(dto.getTransactionDate());
					report.setPostedDate(dto.getPostedDate());
					report.setNameOfConsumer(dto.getNameOfConsumer());
					report.setContactNumber(dto.getContactNumber());
					report.setEmailId(dto.getEmailId());
					report.setVendorRrn(dto.getVendorRrn());
					vijayaBankReportRepository.save(report);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
		
	@Override
	public List<VijayaBankReport> getMerchantReport() {
		return vijayaBankReportRepository.getMerchantReport();
	}
	
	@Override
	public List<VijayaBankReport> getFilteredMerchantReport(Date start,Date end,String paymentMode,String consumerType) {
		return vijayaBankReportRepository.getFilteredMerchantReport(start,end,paymentMode,consumerType);
	}
}
