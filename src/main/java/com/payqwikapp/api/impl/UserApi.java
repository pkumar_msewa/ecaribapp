package com.payqwikapp.api.impl;

import static com.payqwikapp.util.ConvertUtil.convertFromUser;
import static com.payqwikapp.util.ConvertUtil.getAccountFromEntity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ebs.api.IEBSRequestHandlerApi;
import com.ebs.model.EBSRequest;
import com.ebs.model.EBSStatusRequest;
import com.ebs.model.EBSStatusResponse;
import com.ebs.util.EBSConstants;
import com.instantpay.api.IBalanceApi;
import com.instantpay.api.IStatusCheckApi;
import com.instantpay.model.Balance;
import com.instantpay.model.request.BalanceRequest;
import com.instantpay.model.request.StatusCheckRequest;
import com.instantpay.model.response.BalanceResponse;
import com.instantpay.model.response.StatusCheckResponse;
import com.instantpay.util.InstantPayConstants;
import com.letsManage.model.dto.BlockDto;
import com.letsManage.model.dto.BulkMailRequestDTO;
import com.letsManage.model.dto.BulkSmsRequest;
import com.letsManage.model.dto.DateWiseTransDTO;
import com.letsManage.model.dto.DebitCreditListDTO;
import com.letsManage.model.dto.DebitDTO;
import com.letsManage.model.dto.GCMDto;
import com.letsManage.model.dto.GetAllUserList;
import com.letsManage.model.dto.GetMainList;
import com.letsManage.model.dto.GetSeviceListDTO;
import com.letsManage.model.dto.GetTransDTO;
import com.letsManage.model.dto.MailRequestDTO;
import com.letsManage.model.dto.PassWordSuperAdminDTO;
import com.letsManage.model.dto.ResponseLetsDTO;
import com.letsManage.model.dto.ResponseStatusLetsMan;
import com.letsManage.model.dto.SmsRequest;
import com.letsManage.model.dto.SuperAdminAddDTO;
import com.letsManage.model.dto.SuperAdminDTO;
import com.letsManage.model.dto.SuperAdminListDTO;
import com.letsManage.model.dto.TransChartCount;
import com.letsManage.model.dto.UserTransactionListDTO;
import com.letsManage.model.dto.UserTypeDto;
import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IEmailLogApi;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.IMessageLogApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.AUserSignUp;
import com.payqwikapp.entity.AadharDetails;
import com.payqwikapp.entity.AccessDetails;
import com.payqwikapp.entity.AgentDetail;
import com.payqwikapp.entity.AuthUpdateLog;
import com.payqwikapp.entity.AutoReconWeeklyReport;
import com.payqwikapp.entity.BTransferRequestLog;
import com.payqwikapp.entity.BankDetails;
import com.payqwikapp.entity.Banks;
import com.payqwikapp.entity.DTHRequestLog;
import com.payqwikapp.entity.DailySummaryDetails;
import com.payqwikapp.entity.ECityRequestLog;
import com.payqwikapp.entity.GasRequestLog;
import com.payqwikapp.entity.InsuranceRequestLog;
import com.payqwikapp.entity.InviteLog;
import com.payqwikapp.entity.LMRequestLog;
import com.payqwikapp.entity.LandlineRequestLog;
import com.payqwikapp.entity.LocationDetails;
import com.payqwikapp.entity.LoginLog;
import com.payqwikapp.entity.MTRequestLog;
import com.payqwikapp.entity.MerchantDetails;
import com.payqwikapp.entity.MerchantRefundRequest;
import com.payqwikapp.entity.MpinHistory;
import com.payqwikapp.entity.MpinLog;
import com.payqwikapp.entity.Offers;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.POSMerchants;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PQVersion;
import com.payqwikapp.entity.PasswordHistory;
import com.payqwikapp.entity.RequestLog;
import com.payqwikapp.entity.SMRequestLog;
import com.payqwikapp.entity.SecurityAnswerDetails;
import com.payqwikapp.entity.SecurityQuestion;
import com.payqwikapp.entity.SendNotification;
import com.payqwikapp.entity.ServiceStatus;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.UpiBscmMerTokenDetails;
import com.payqwikapp.entity.UpiMerchantDetail;
import com.payqwikapp.entity.UpiSdkMerchantDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.entity.UserMaxLimit;
import com.payqwikapp.entity.VBankAccountDetail;
import com.payqwikapp.entity.VPQMerchant;
import com.payqwikapp.entity.VersionLogs;
import com.payqwikapp.entity.VisaMerchant;
import com.payqwikapp.entity.WalletDetails;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.AadharDTO;
import com.payqwikapp.model.AadharDetailsDTO;
import com.payqwikapp.model.AgentDTO;
import com.payqwikapp.model.AgentDetailsDTO;
import com.payqwikapp.model.AgentRequest;
import com.payqwikapp.model.AmountDTO;
import com.payqwikapp.model.ApproveKycDTO;
import com.payqwikapp.model.AuthUpdateRequest;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.DRegisterDTO;
import com.payqwikapp.model.DTHBillPaymentDTO;
import com.payqwikapp.model.ElectricityBillPaymentDTO;
import com.payqwikapp.model.GasBillPaymentDTO;
import com.payqwikapp.model.Gender;
import com.payqwikapp.model.ImageDTO;
import com.payqwikapp.model.InsuranceBillPaymentDTO;
import com.payqwikapp.model.KYCResponse;
import com.payqwikapp.model.KycDTO;
import com.payqwikapp.model.KycLimitDTO;
import com.payqwikapp.model.LandlineBillPaymentDTO;
import com.payqwikapp.model.LockUnlockDTO;
import com.payqwikapp.model.LoginDTO;
import com.payqwikapp.model.MRegisterDTO;
import com.payqwikapp.model.MTransactionResponseDTO;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.MpinChangeDTO;
import com.payqwikapp.model.MpinDTO;
import com.payqwikapp.model.NearAgentsDetailsDTO;
import com.payqwikapp.model.NearByAgentsDTO;
import com.payqwikapp.model.NearByMerchantsDTO;
import com.payqwikapp.model.NearMerchantDetailsDTO;
import com.payqwikapp.model.NotificationDTO;
import com.payqwikapp.model.OffersDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.PaymentRequestDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.RequestType;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyDonateeDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.SendNotificationDTO;
import com.payqwikapp.model.ServiceStatusDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.SummaryReportDTO;
import com.payqwikapp.model.TransactionListDTO;
import com.payqwikapp.model.TransactionReport;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.UserBalanceDTO;
import com.payqwikapp.model.UserByLocationDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserMaxLimitDto;
import com.payqwikapp.model.UserMonthlyBalanceDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.UsernameListDTO;
import com.payqwikapp.model.VNetStatusRequest;
import com.payqwikapp.model.VNetStatusResponse;
import com.payqwikapp.model.VPQMerchantDTO;
import com.payqwikapp.model.VersionListDTO;
import com.payqwikapp.model.admin.AccessDTO;
import com.payqwikapp.model.admin.AddServicesDTO;
import com.payqwikapp.model.admin.ServiceListDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.admin.UpdateServiceDTO;
import com.payqwikapp.model.admin.UserListDTO;
import com.payqwikapp.model.admin.report.GetDailyAmountDTO;
import com.payqwikapp.model.admin.report.MISReportDTO;
import com.payqwikapp.model.admin.report.WalletLoadRepot;
import com.payqwikapp.model.admin.report.WalletOutstandingDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.merchant.MerchantRegisterDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.mobile.UserResponseDTO;
import com.payqwikapp.repositories.AUserSignUpRepository;
import com.payqwikapp.repositories.AadharDetailsRepository;
import com.payqwikapp.repositories.AccessLogRepository;
import com.payqwikapp.repositories.AgentDetailRepository;
import com.payqwikapp.repositories.AuthUpdateLogRepository;
import com.payqwikapp.repositories.AutoReconWeeklyReportRepository;
import com.payqwikapp.repositories.BTransferRequestLogRepository;
import com.payqwikapp.repositories.BankDetailRepository;
import com.payqwikapp.repositories.BanksRepository;
import com.payqwikapp.repositories.DTHRequestLogRepository;
import com.payqwikapp.repositories.DailySummaryRepository;
import com.payqwikapp.repositories.ECityRequestLogRepository;
import com.payqwikapp.repositories.GasRequestLogRepository;
import com.payqwikapp.repositories.InsuranceRequestLogRepository;
import com.payqwikapp.repositories.InviteLogRepository;
import com.payqwikapp.repositories.LMRequestLogRepository;
import com.payqwikapp.repositories.LandlineRequestLogRepository;
import com.payqwikapp.repositories.LocationDetailsRepository;
import com.payqwikapp.repositories.LoginLogRepository;
import com.payqwikapp.repositories.MRequestLogRepository;
import com.payqwikapp.repositories.MTRequestLogRepository;
import com.payqwikapp.repositories.MerchantDetailsRepository;
import com.payqwikapp.repositories.MerchantRefundRequestRepository;
import com.payqwikapp.repositories.MpinHistoryRepository;
import com.payqwikapp.repositories.MpinLogRepository;
import com.payqwikapp.repositories.OffersRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.POSMerchantsRepository;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQCommissionRepository;
import com.payqwikapp.repositories.PQOperatorRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.PQVersionRepository;
import com.payqwikapp.repositories.PasswordHistoryRepository;
import com.payqwikapp.repositories.RequestLogRepository;
import com.payqwikapp.repositories.SMRequestLogRepository;
import com.payqwikapp.repositories.SecurityAnswerDetailsRepository;
import com.payqwikapp.repositories.SecurityQuestionRepository;
import com.payqwikapp.repositories.SendNotificationRepository;
import com.payqwikapp.repositories.ServiceStatusRepository;
import com.payqwikapp.repositories.UpiBscmMerTokenDetailRepository;
import com.payqwikapp.repositories.UpiMerchantDetailRepository;
import com.payqwikapp.repositories.UpiSdkMerchantDetailsRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserMaxLimitRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.VBankAccountDetailRepository;
import com.payqwikapp.repositories.VPQMerchantRepository;
import com.payqwikapp.repositories.VersionLogsRepository;
import com.payqwikapp.repositories.VisaMerchantRepository;
import com.payqwikapp.repositories.WalletDetailRepository;
import com.payqwikapp.repositories.YesBankPoolBalRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ClientException;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.util.VNetUtils;
import com.payqwikapp.validation.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.model.GetValueDTO;
import com.upi.model.MerchantTxnStatusResponse;
import com.upi.model.UpiRequest;
import com.upi.model.UpiResponse;
import com.upi.util.UPIBescomConstants;
import com.upi.util.UpiConstants;
import com.vnet.api.IVnetStatusApi;

public class UserApi implements IUserApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static DecimalFormat decimalFormatter = new DecimalFormat("###.##");
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat dateForm = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	private final static String SEND_NOTIFICATION_URL = DeploymentConstants.WEB_URL + "/Send/Notification";

	private MessageSource messageSource;
	private final UserRepository userRepository;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PQServiceRepository pqServiceRepository;
	private final PQTransactionRepository pqTransactionRepository;
	private final LoginLogRepository loginLogRepository;
	private final MpinLogRepository mpinLogRepository;
	private final PasswordEncoder passwordEncoder;
	private final IMailSenderApi mailSenderApi;
	private final ISMSSenderApi smsSenderApi;
	private final PQVersionRepository pqVersionRepository;
	private final InviteLogRepository inviteLogRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final ISessionApi sessionApi;
	private final VBankAccountDetailRepository vBankAccountDetailRepository;
	private final LocationDetailsRepository locationDetailsRepository;
	private final SecurityQuestionRepository securityQuestionRepository;
	private final SecurityAnswerDetailsRepository securityAnswerDetailsRepository;
	private final BTransferRequestLogRepository bTransferRequestLogRepository;
	private final DTHRequestLogRepository dthRequestLogRepository;
	private final ECityRequestLogRepository eCityRequestLogRepository;
	private final GasRequestLogRepository gasRequestLogRepository;
	private final InsuranceRequestLogRepository insuranceRequestLogRepository;
	private final LandlineRequestLogRepository landlineRequestLogRepository;
	private final LMRequestLogRepository lmRequestLogRepository;
	private final MRequestLogRepository mRequestLogRepository;
	private final MTRequestLogRepository mtRequestLogRepository;
	private final SMRequestLogRepository smRequestLogRepository;
	private final ServiceStatusRepository serviceStatusRepository;
	private final VisaMerchantRepository visaMerchantRepository;
	private final MerchantDetailsRepository merchantDetailsRepository;
	private final AuthUpdateLogRepository authUpdateLogRepository;
	private final RequestLogRepository requestLogRepository;
	private final PQCommissionRepository pqCommissionRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final IEmailLogApi emailLogApi;
	private final IMessageLogApi messageLogApi;
	private final IBalanceApi balanceApi;
	private final POSMerchantsRepository posMerchantsRepository;
	private final AccessLogRepository accessLogRepository;
	private final AgentDetailRepository agentDetailRepository;
	private final ICommissionApi commissionApi;
	private final PQOperatorRepository pqOperatorRepository;
	private final MpinHistoryRepository mpinHistoryRepository;
	private final AUserSignUpRepository aUserSignUpRepository;
	private final PasswordHistoryRepository passwordHistoryRepository;
	private final BankDetailRepository bankDetailRepository;
	private final VPQMerchantRepository vpqMerchantRepository;
	private final VersionLogsRepository versionLogsRepository;
	private final MerchantRefundRequestRepository merchantRefundRequestRepository;
	private final IStatusCheckApi statusCheckApi;
	private final IEBSRequestHandlerApi requestHandlerApi;
	private final IVnetStatusApi vnetStatusApi;
	private final AutoReconWeeklyReportRepository autoReconWeeklyReportRepository;
	private final WalletDetailRepository walletDetailRepository;
	private final YesBankPoolBalRepository yesBankPoolBalRepository;
	private final SendNotificationRepository sendNotificationRepository;
	private final UpiMerchantDetailRepository upiMerchantDetailRepository;
	private final UpiBscmMerTokenDetailRepository upiBscmMerTokenDetailRepository;
	private final UpiSdkMerchantDetailsRepository upiSdkMerchantDetailsRepository;
	private final BanksRepository banksRepository;
	private final OffersRepository offersRepository;
	private final DailySummaryRepository dailySummaryRepository;
	private final AadharDetailsRepository aadharDetailsRepository;
	private final UserMaxLimitRepository userMaxLimitRepository;

	public UserApi(UserRepository userRepository, UserDetailRepository userDetailRepository,
			PQAccountDetailRepository pqAccountDetailRepository, PQAccountTypeRepository pqAccountTypeRepository,
			PQServiceRepository pqServiceRepository, PQTransactionRepository pqTransactionRepository,
			LoginLogRepository loginLogRepository, MpinLogRepository mpinLogRepository, PasswordEncoder passwordEncoder,
			IMailSenderApi mailSenderApi, ISMSSenderApi smsSenderApi, PQVersionRepository pqVersionRepository,
			InviteLogRepository inviteLogRepository, PGDetailsRepository pgDetailsRepository, ISessionApi sessionApi,
			VBankAccountDetailRepository vBankAccountDetailRepository,
			LocationDetailsRepository locationDetailsRepository, SecurityQuestionRepository securityQuestionRepository,
			SecurityAnswerDetailsRepository securityAnswerDetailsRepository,
			BTransferRequestLogRepository bTransferRequestLogRepository,
			DTHRequestLogRepository dthRequestLogRepository, ECityRequestLogRepository eCityRequestLogRepository,
			GasRequestLogRepository gasRequestLogRepository,
			InsuranceRequestLogRepository insuranceRequestLogRepository,
			LandlineRequestLogRepository landlineRequestLogRepository, LMRequestLogRepository lmRequestLogRepository,
			MRequestLogRepository mRequestLogRepository, MTRequestLogRepository mtRequestLogRepository,
			SMRequestLogRepository smRequestLogRepository, ServiceStatusRepository serviceStatusRepository,
			VisaMerchantRepository visaMerchantRepository, MerchantDetailsRepository merchantDetailsRepository,
			AuthUpdateLogRepository authUpdateLogRepository, RequestLogRepository requestLogRepository,
			PQCommissionRepository pqCommissionRepository, PQServiceTypeRepository pqServiceTypeRepository,
			IEmailLogApi emailLogApi, IMessageLogApi messageLogApi, IBalanceApi balanceApi,
			POSMerchantsRepository posMerchantsRepository, AccessLogRepository accessLogRepository,
			AgentDetailRepository agentDetailRepository, ICommissionApi commissionApi,
			PQOperatorRepository pqOperatorRepository, MpinHistoryRepository mpinHistoryRepository,
			AUserSignUpRepository aUserSignUpRepository, PasswordHistoryRepository passwordHistoryRepository,
			BankDetailRepository bankDetailRepository, VPQMerchantRepository vpqMerchantRepository,
			VersionLogsRepository versionLogsRepository,
			MerchantRefundRequestRepository merchantRefundRequestRepository, IStatusCheckApi statusCheckApi,
			IEBSRequestHandlerApi requestHandlerApi, IVnetStatusApi vnetStatusApi,
			WalletDetailRepository walletDetailRepository, YesBankPoolBalRepository yesBankPoolBalRepository,
			AutoReconWeeklyReportRepository autoReconWeeklyReportRepository,
			SendNotificationRepository sendNotificationRepository,
			UpiMerchantDetailRepository upiMerchantDetailRepository,
			UpiBscmMerTokenDetailRepository upiBscmMerTokenDetailRepository,
			UpiSdkMerchantDetailsRepository upiSdkMerchantDetailsRepository, BanksRepository banksRepository,
			OffersRepository offersRepository, DailySummaryRepository dailySummaryRepository,
			AadharDetailsRepository aadharDetailsRepository,UserMaxLimitRepository userMaxLimitRepository) {
		this.userRepository = userRepository;
		this.userDetailRepository = userDetailRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqAccountTypeRepository = pqAccountTypeRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.pqTransactionRepository = pqTransactionRepository;
		this.loginLogRepository = loginLogRepository;
		this.mpinLogRepository = mpinLogRepository;
		this.passwordEncoder = passwordEncoder;
		this.mailSenderApi = mailSenderApi;
		this.smsSenderApi = smsSenderApi;
		this.pqVersionRepository = pqVersionRepository;
		this.inviteLogRepository = inviteLogRepository;
		this.pgDetailsRepository = pgDetailsRepository;
		this.sessionApi = sessionApi;
		this.vBankAccountDetailRepository = vBankAccountDetailRepository;
		this.locationDetailsRepository = locationDetailsRepository;
		this.securityQuestionRepository = securityQuestionRepository;
		this.securityAnswerDetailsRepository = securityAnswerDetailsRepository;
		this.bTransferRequestLogRepository = bTransferRequestLogRepository;
		this.dthRequestLogRepository = dthRequestLogRepository;
		this.eCityRequestLogRepository = eCityRequestLogRepository;
		this.gasRequestLogRepository = gasRequestLogRepository;
		this.insuranceRequestLogRepository = insuranceRequestLogRepository;
		this.landlineRequestLogRepository = landlineRequestLogRepository;
		this.lmRequestLogRepository = lmRequestLogRepository;
		this.mRequestLogRepository = mRequestLogRepository;
		this.mtRequestLogRepository = mtRequestLogRepository;
		this.smRequestLogRepository = smRequestLogRepository;
		this.serviceStatusRepository = serviceStatusRepository;
		this.visaMerchantRepository = visaMerchantRepository;
		this.merchantDetailsRepository = merchantDetailsRepository;
		this.authUpdateLogRepository = authUpdateLogRepository;
		this.requestLogRepository = requestLogRepository;
		this.pqCommissionRepository = pqCommissionRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.emailLogApi = emailLogApi;
		this.messageLogApi = messageLogApi;
		this.balanceApi = balanceApi;
		this.posMerchantsRepository = posMerchantsRepository;
		this.accessLogRepository = accessLogRepository;
		this.agentDetailRepository = agentDetailRepository;
		this.commissionApi = commissionApi;
		this.pqOperatorRepository = pqOperatorRepository;
		this.mpinHistoryRepository = mpinHistoryRepository;
		this.aUserSignUpRepository = aUserSignUpRepository;
		this.passwordHistoryRepository = passwordHistoryRepository;
		this.bankDetailRepository = bankDetailRepository;
		this.vpqMerchantRepository = vpqMerchantRepository;
		this.versionLogsRepository = versionLogsRepository;
		this.merchantRefundRequestRepository = merchantRefundRequestRepository;
		this.statusCheckApi = statusCheckApi;
		this.requestHandlerApi = requestHandlerApi;
		this.vnetStatusApi = vnetStatusApi;
		this.autoReconWeeklyReportRepository = autoReconWeeklyReportRepository;
		this.walletDetailRepository = walletDetailRepository;
		this.yesBankPoolBalRepository = yesBankPoolBalRepository;
		this.sendNotificationRepository = sendNotificationRepository;
		this.upiMerchantDetailRepository = upiMerchantDetailRepository;
		this.upiBscmMerTokenDetailRepository = upiBscmMerTokenDetailRepository;
		this.upiSdkMerchantDetailsRepository = upiSdkMerchantDetailsRepository;
		this.banksRepository = banksRepository;
		this.offersRepository = offersRepository;
		this.dailySummaryRepository = dailySummaryRepository;
		this.aadharDetailsRepository = aadharDetailsRepository;
		this.userMaxLimitRepository=userMaxLimitRepository;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public void saveUser(RegisterDTO dto) throws ClientException {
		try {
			User user = new User();
			PasswordHistory history = new PasswordHistory();
			UserDetail userDetail = new UserDetail();
			SecurityAnswerDetails answerDetails = new SecurityAnswerDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setGender(dto.getGender());
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());
			userDetail.setFingerPrint(dto.getFingerPrint());

			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				}
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				user.setPassword(hashedPassword);
				history.setPassword(hashedPassword);
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				userDetail.setLocation(location);
				/*if (dto.isWeb()) {
					SecurityQuestion question = securityQuestionRepository
							.findSecurityQuestionByCode(dto.getSecQuestionCode());
					answerDetails.setAnswer(dto.getSecAnswer() == null ? "" : dto.getSecAnswer());
					answerDetails.setSecQuestion(question);
					answerDetails.setUser(user);
					securityAnswerDetailsRepository.save(answerDetails);
				}*/
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				history.setPassword(hashedPassword);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
				user.setOsName(dto.getDevice());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			pqAccountDetail.setVijayaAccountNumber(dto.getAccountNumber());
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
				history.setUser(user);
				passwordHistoryRepository.save(history);

			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
				history.setUser(user);
				passwordHistoryRepository.save(history);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public boolean isVBankAccountSaved(KycDTO dto, PQAccountDetail account) {
		boolean isSaved = false;
		try {
			String accountNumber = SecurityUtil.sha1(dto.getAccountNumber());
			VBankAccountDetail vBankAccount = new VBankAccountDetail();
			vBankAccount.setAccountNumber(accountNumber);
			vBankAccount.setMobileNumber(dto.getMobileNumber());
			vBankAccount.setStatus(Status.Inactive);
			vBankAccountDetailRepository.save(vBankAccount);
			account.setvBankAccount(vBankAccount);
			pqAccountDetailRepository.save(account);
			isSaved = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSaved;
	}

	@Override
	public boolean sendOTP(String mobileNumber, User user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		VBankAccountDetail vBankAccountDetail = user.getAccountDetail().getvBankAccount();
		vBankAccountDetail.setOtp(otp);
		vBankAccountDetailRepository.save(vBankAccountDetail);
		smsSenderApi.sendKYCSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE_KYC, user, mobileNumber, otp);
		return true;
	}

	@Override
	public boolean containsAccountAndMobile(String vbankAccount, String mobileNumber) {
		VBankAccountDetail account = vBankAccountDetailRepository.findByAccountNumber(vbankAccount);
		if (account != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void updateVBankAccountDetails(KYCResponse dto, VBankAccountDetail accountDetail, User user) {
		try {
			String accountNumber = SecurityUtil.sha1(dto.getAccountNo());
			accountDetail.setAccountNumber(accountNumber);
			accountDetail.setStatus(Status.Inactive);
			accountDetail.setMobileNumber(dto.getMobileNo());
			accountDetail.setAccountName(dto.getAccountName());
			vBankAccountDetailRepository.save(accountDetail);
			PQAccountDetail account = user.getAccountDetail();
			account.setvBankAccount(accountDetail);
			pqAccountDetailRepository.save(account);
			sendOTP(dto.getMobileNo(), user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveMerchant(MRegisterDTO dto) throws ClientException {
		try {
			String password = dto.getContactNo();
			User user = new User();
			UserDetail userDetail = new UserDetail();
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(" ");
			userDetail.setEmail(dto.getEmail());
			userDetail.setImage(dto.getImage());
			user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(password));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getEmail());
			user.setUserType(UserType.Merchant);
			user.setUserDetail(userDetail);
			PQAccountType kYCAccountType = pqAccountTypeRepository.findByCode("SUPERKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountType(kYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempMerchant = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Active);
			if (tempMerchant == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
			} else {
				userRepository.delete(tempMerchant);
				userDetailRepository.delete(tempMerchant.getUserDetail());
				user.setAccountDetail(tempMerchant.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
			}
			mailSenderApi.sendNoReplyMail("Verification Successful", MailTemplate.MERCHANT_REGISTRATION, user,
					password);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_SUCCESS, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public void editUser(RegisterDTO dto) {
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null) {
			UserDetail detail = user.getUserDetail();
			if (user.getEmailStatus() != Status.Active) {
				detail.setAddress(dto.getAddress());
				detail.setFirstName(dto.getFirstName());
				detail.setEmail(dto.getEmail());
				detail.setGender(dto.getGender());
				userDetailRepository.save(detail);
			} else {
				detail.setAddress(dto.getAddress());
				detail.setFirstName(dto.getFirstName());
				detail.setGender(dto.getGender());
				userDetailRepository.save(detail);
			}
		}
	}

	@Override
	public void editName(RegisterDTO dto) {
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null) {
			userDetailRepository.updateUserName(dto.getFirstName(), dto.getUsername());
		}
	}

	@Override
	public User findByUserName(String name) {
		User user = userRepository.findByUsername(name);
		return user;
	}

	@Override
	public User findByAccountId(PQAccountDetail id) {
		return userRepository.getByAccount(id);
	}

	@Override
	public User findById(long id) {
		User user = userRepository.findUserByUserId(id);
		return user;
	}

	@Override
	public List<UsernameListDTO> getAllAccessUsers() {
		List<UserType> userTypes = new ArrayList<>();
		userTypes.add(UserType.Admin);
		userTypes.add(UserType.SuperAdmin);
		// userTypes.add(UserType.Merchant);
		List<User> users = userRepository.getByUserType(userTypes);
		return ConvertUtil.convertList(users);
	}

	@Override
	public ResponseDTO updateServiceDetails(UpdateServiceDTO dto) {
		ResponseDTO result = new ResponseDTO();
		PQService service = pqServiceRepository.findServiceByCode(dto.getCode());
		if (service != null) {
			Map<String, Object> responseMap = new HashMap<>();
			service.setStatus(Status.valueOf(dto.getStatus()));
			service = pqServiceRepository.save(service);
			responseMap.put("service", ConvertUtil.getDTOFrom(service, null));
			if (dto.isUpdateCommission()) {
				PQCommission commission = pqCommissionRepository
						.findCommissionByIdentifier(dto.getCommissionIdentifier());
				if (commission != null) {
					commission.setFixed(dto.isFixed());
					commission.setValue(dto.getValue());
					String identifier = ConvertUtil.createCommissionIdentifier(commission);
					commission.setIdentifier(identifier);
					PQCommission exists = pqCommissionRepository.findCommissionByIdentifier(identifier);
					if (exists == null) {
						commission = pqCommissionRepository.save(commission);
						responseMap.put("commission", ConvertUtil.getCommissionDTO(commission));
					}
				}
			}
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Information Updated");
			result.setDetails(responseMap);
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Service Not available");
		}
		return result;
	}

	@Override
	public ResponseDTO getAllUserInfo(String username) {
		ResponseDTO result = new ResponseDTO();
		User user = findByUserName(username);
		if (user != null) {
			if (!(user.getAuthority().contains(Authorities.SPECIAL_USER))) {
				Map<String, Object> details = new HashMap<>();
				details.put("user", convertFromUser(user));
				PQAccountDetail account = user.getAccountDetail();
				details.put("account", getAccountFromEntity(account));

				List<PQTransaction> transactionList = pqTransactionRepository.findTransactionByAccount(account);
				details.put("transactions", ConvertUtil.convertFromEntityList(transactionList));
				List<LoginLog> loginList = loginLogRepository.findLogByUser(user);
				details.put("loginLogs", ConvertUtil.convertEntityFromLoginList(loginList));
				details.put("online", sessionApi.getUserOnlineStatus(user));
				details.put("totalDebit", String.format("%.2f", calculateDbCrForAccount(account, true)));
				details.put("totalCredit", String.format("%.2f", calculateDbCrForAccount(account, false)));
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("List");
				result.setDetails(details);
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("User not found");
		}
		return result;
	}

	@Override
	public ResponseDTO resendOTPUnregistered(User user) {
		ResponseDTO result = new ResponseDTO();
		String otp = CommonUtil.generateSixDigitNumericString();
		user.setMobileToken(otp);
		userRepository.save(user);
		result.setStatus(ResponseStatus.NEW_REGISTRATION_OTP);
		result.setMessage("OTP Sent to " + user.getUsername());
		result.setDetails("OTP Sent to " + user.getUsername());
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, user, null);
		return result;
	}

	@Override
	public ResponseDTO OTPForDirectPayment(User user) {
		ResponseDTO result = new ResponseDTO();
		String otp = CommonUtil.generateSixDigitNumericString();
		user.setMobileToken(otp);
		userRepository.save(user);
		result.setStatus(ResponseStatus.NEW_REGISTRATION_OTP);
		result.setMessage("OTP Sent to " + user.getUsername());
		result.setDetails("OTP Sent to " + user.getUsername());
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.TRANSACTION_OTP, user, null);
		return result;
	}

	@Override
	public List<TransactionListDTO> processFiltering(List<PQTransaction> transactionList, List<PQService> services) {
		List<TransactionListDTO> listDTOs = new ArrayList<>();
		System.err.println("inside filtering");
		for (PQTransaction transaction : transactionList) {
			PQService service = transaction.getService();
			if (services.contains(service)) {
				PQAccountDetail account = transaction.getAccount();
				double amount = transaction.getAmount();
				User user = userRepository.findByAccountDetails(account);
				if (!(user.getAuthority().contains(Authorities.LOCKED)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED))) {
					PQCommission commission = pqCommissionRepository.findCommissionByServiceAndAmount(service, amount);
					listDTOs.add(getTransactionListDTO(transaction, user, commission));
				}
			}
		}
		return listDTOs;
	}

	@Override
	public TransactionListDTO getTransactionListDTO(PQTransaction t, User u, PQCommission commission) {
		TransactionListDTO dto = new TransactionListDTO();
		dto.setId(t.getId());
		dto.setUsername((u.getUserDetail().getFirstName() != null) ? u.getUserDetail().getFirstName() : "");
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setDescription(t.getDescription());
		dto.setAmount(t.getAmount());
		dto.setCurrentBalance(t.getCurrentBalance());
		dto.setAuthority(u.getAuthority());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setDateOfTransaction(t.getCreated());
		dto.setImage((u.getUserDetail().getImage() != null) ? u.getUserDetail().getImage() : "");
		dto.setCommission(commission.getValue());
		dto.setDebit(t.isDebit());
		dto.setStatus(t.getStatus());
		dto.setServiceType(t.getService().getName());
		dto.setEmail((u.getUserDetail().getEmail() != null) ? u.getUserDetail().getEmail() : "");
		return dto;
	}

	@Override
	public List<TransactionListDTO> getTransactionsBetweenForServiceType(Date startDate, Date endDate,
			String serviceType) {
		List<TransactionListDTO> listDTOs = new ArrayList<>();
		try {
			PQServiceType type = pqServiceTypeRepository.findServiceTypeByName(serviceType);
			List<PQService> services = pqServiceRepository.findByServiceType(type);
			// List<PQTransaction> transactionList =
			// pqTransactionRepository.getTransactionBetween(TransactionType.DEFAULT,startDate,endDate);
			// listDTOs = processFiltering(transactionList,services);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<TransactionReport> getAllTransactions(Date startDate, Date endDate, String serviceType) {
		List<TransactionReport> listDTOs = new ArrayList<>();
		try {
			PQServiceType type = pqServiceTypeRepository.findServiceTypeByName(serviceType);
			List<PQService> services = pqServiceRepository.findByServiceType(type);
			List<PQTransaction> transactionList = null;
			if (serviceType.contains("BILL")) {
				transactionList = pqTransactionRepository.getTransactionBetweenForServices(TransactionType.DEFAULT, startDate,
						endDate, services);
			} else {
				transactionList = pqTransactionRepository.getTransactionBetweenForServices(TransactionType.DEFAULT, startDate,
						endDate, services);
			}
			listDTOs = ConvertUtil.getFromTransactionList(transactionList);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<TListDTO> getAllList(List<PQTransaction> transactions) {
		List<TListDTO> lists = new ArrayList<>();
		if (transactions != null && !transactions.isEmpty()) {
			for (PQTransaction t : transactions) {
				User u = userRepository.findByAccountDetails(t.getAccount());
				lists.add(ConvertUtil.getDTO(u, t));
			}
		}
		return lists;
	}

	@Override
	public List<TListDTO> getTodaysTransactions() {
		List<TListDTO> listDTOs = new ArrayList<>();
		String authority = Authorities.LOCKED + "," + Authorities.AUTHENTICATED;
		Pageable page = new PageRequest(0, 1000);
		try {
			List<PQAccountDetail> accounts = userRepository.getAccountsByAuthority(authority);
			Page<PQTransaction> todaysTransactions = pqTransactionRepository.findTodaysTransactions(page,
					TransactionType.DEFAULT, accounts);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(todaysTransactions.getContent());
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, todaysTransactions.getContent());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<TListDTO> getTransactionsBetween(Date startDate, Date endDate) {
		List<TListDTO> listDTOs = new ArrayList<>();
		String authority = Authorities.LOCKED + "," + Authorities.AUTHENTICATED;
		try {
			List<PQAccountDetail> accounts = userRepository.getAccountsByAuthority(authority);
			List<PQTransaction> transactionList = pqTransactionRepository.getTransactionBetween(TransactionType.DEFAULT,
					accounts, startDate, endDate);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, transactionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}
	
	@Override
	public List<TListDTO> getDailyTransactions(Date startDate, Date endDate,String transactionType) {
		List<TListDTO> listDTOs = new ArrayList<>();
		try {
			if (transactionType.equalsIgnoreCase("debit")) {
				List<PQTransaction> transactionList = pqTransactionRepository.getDailyTrasnactionBetweenDate(startDate,
						endDate, TransactionType.DEFAULT);
				List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
				List<User> users = userRepository.getUsersByAccount(accountList);
				listDTOs = ConvertUtil.getListDTOs(users, transactionList);
				return listDTOs;
			} else if (transactionType.equalsIgnoreCase("credit")) {
				List<PQTransaction> transactionList = pqTransactionRepository
						.getDailyTrasnactionBetweenDateForCredit(startDate, endDate, TransactionType.DEFAULT);
				List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
				List<User> users = userRepository.getUsersByAccount(accountList);
				listDTOs = ConvertUtil.getListDTOs(users, transactionList);
				return listDTOs;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<?> getSMSOrEmailLogs(String logType) {
		Pageable pageable = new PageRequest(0, 1000);
		List<?> logs = new ArrayList<>();
		switch (logType.toUpperCase()) {
		case "EMAIL":
			logs = emailLogApi.getAllEmailLogs(pageable).getContent();
			break;
		default:
			logs = messageLogApi.getAllSMSLogs(pageable).getContent();
		}
		return logs;

	}

	@Override
	public List<?> getSMSOrEmailLogsByFilter(Date start, Date end, String logType) {
		List<?> logs = new ArrayList<>();
		switch (logType.toUpperCase()) {
		case "EMAIL":
			logs = emailLogApi.getDailyEmailLogBetweeen(start, end);
			break;
		default:
			logs = messageLogApi.getDailyMessageLogBetweeen(start, end);
		}
		return logs;
	}

	@Override
	public List<ServiceListDTO> getAllServicesWithCommission() {
		List<PQService> services = (List<PQService>) pqServiceRepository.findAllServices();
		return ConvertUtil.getListForServices(services, pqCommissionRepository);
	}

	@Override
	public List<UserListDTO> getTodayUsers(String type) {
		List<UserListDTO> userList = new ArrayList<>();
		Page<User> users = null;
		Pageable page = new PageRequest(0, 1000);
		try {
			switch (type.toUpperCase()) {
			case "VERIFIED":
				users = userRepository.getTodaysUser(page, UserType.User, Status.Active);
				break;
			case "UNVERIFIED":
				users = userRepository.getTodaysUser(page, UserType.User, Status.Inactive);
				break;
			case "BLOCKED":
				String auth = Authorities.USER + "," + Authorities.BLOCKED;
				users = userRepository.getTodaysUser(page, UserType.User, auth);
				break;
			case "ONLINE":
				users = sessionApi.findOnlineUsers(page);
				break;
			case "LOCKED":
				String authority = Authorities.USER + "," + Authorities.LOCKED;
				users = userRepository.getTodaysUser(page, UserType.User, authority);
				break;
			case "MALE":
				users = userRepository.getTodaysUser(page, UserType.User, Gender.M);
				break;
			case "FEMALE":
				users = userRepository.getTodaysUser(page, UserType.User, Gender.F);
				break;
			case "KYC":
				PQAccountType accountType = pqAccountTypeRepository.findByCode("KYC");
				users = userRepository.getTodaysUser(page, UserType.User, accountType);
				break;
			default:
				users = userRepository.getTodaysUser(page, UserType.User);
				break;
			}
			userList = ConvertUtil.getUserAuthority(users.getContent());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;
	}

	@Override
	public List<UserListDTO> getUsersBetween(Date startDate, Date endDate, String type) {
		List<UserListDTO> list = new ArrayList<>();
		List<User> users = null;
		try {
			switch (type.toUpperCase()) {
			case "VERIFIED":
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, Status.Active);
				break;
			case "UNVERIFIED":
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, Status.Inactive);
				break;
			case "BLOCKED":
				String auth = Authorities.USER + "," + Authorities.BLOCKED;
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, auth);
				break;
			case "ONLINE":
				users = sessionApi.findOnlineUsers();
				break;
			case "LOCKED":
				String authority = Authorities.USER + "," + Authorities.LOCKED;
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, authority);
				break;
			case "MALE":
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, Gender.M);
				break;
			case "FEMALE":
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, Gender.F);
				break;
			case "KYC":
				PQAccountType accountType = pqAccountTypeRepository.findByCode("KYC");
				users = userRepository.getUserBetween(UserType.User, startDate, endDate, accountType);
				break;
			default:
				users = userRepository.getUserBetween(UserType.User, startDate, endDate);

			}

			list = ConvertUtil.getUserAuthority(users);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<UserDTO> findAllUser() {
		List<User> user = userRepository.findAllUser();
		if (user != null) {
			return ConvertUtil.convertUserList(user);
		}
		return null;
	}

	@Override
	public double getWalletBalance(User u) {
		return pqAccountDetailRepository.getUserWalletBalance(u.getAccountDetail().getId());
	}

	@Override
	public boolean isVerified(KycDTO dto, User u) {
		boolean valid = false;
		VBankAccountDetail bankAccount = u.getAccountDetail().getvBankAccount();
		String otp = dto.getOtp();
		String validOTP = bankAccount.getOtp();
		if (otp.equals(validOTP)) {
			valid = true;
			bankAccount.setOtp(null);
			bankAccount.setStatus(Status.Active);
			vBankAccountDetailRepository.save(bankAccount);
			updateAccountType(u.getUsername(), true);
			User newUser = findByUserName(u.getUsername());
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_KYC_SUCCESS, newUser, null);
		}
		return valid;
	}

	@Override
	public String getTransactionRequestInJSON(String serviceCode, String transactionRefNo) {
		String json = null;
		switch (serviceCode.toUpperCase()) {
		case "LMC":
			json = getLMRequest(transactionRefNo);
			break;
		case "SMR":
		case "SMU":
			json = getSendMoneyRequest(transactionRefNo);
			break;
		case "SMB":
			json = getBTransferRequest(transactionRefNo);
			break;
		case "VACP":
		case "VATP":
		case "VBVP":
		case "VBGP":
		case "VIDP":
		case "VMSP":
		case "VMMP":
		case "VMTP":
		case "VRGP":
		case "VTVP":
		case "VTMP":
		case "VTCP":
		case "VTSP":
		case "VTGP":
		case "VUSP":
		case "VUGP":
		case "VVSP":
		case "VVGP":
		case "VVFP":
		case "VACC":
		case "VATC":
		case "VBGC":
		case "VIDC":
		case "VMTC":
		case "VRGC":
		case "VTDC":
		case "VVFC":
			json = getTopupRequest(transactionRefNo);
			break;
		case "VATV":
		case "VDTV":
		case "VRTV":
		case "VSTV":
		case "VTTV":
		case "VVTV":
			json = getDTHRequest(transactionRefNo);
			break;
		case "VATL":
		case "VBGL":
		case "VMDL":
		case "VRGL":
		case "VTCL":
			json = getLandlineRequest(transactionRefNo);
			break;
		case "VTTE":
		case "VTPE":
		case "VNDE":
		case "VSTE":
		case "VSAE":
		case "VREE":
		case "VMPE":
		case "VDOE":
		case "VNUE":
		case "VMDE":
		case "VMME":
		case "VDRE":
		case "VJUE":
		case "VJRE":
		case "VIPE":
		case "VDNE":
		case "VDHE":
		case "VCCE":
		case "VCWE":
		case "VBYE":
		case "VBRE":
		case "VBME":
		case "VBBE":
		case "VAAE":
		case "VARE":
			json = getECityRequest(transactionRefNo);
			break;
		case "VIPG":
		case "VMMG":
		case "VGJG":
		case "VADG":
			json = getGasRequest(transactionRefNo);
			break;
		case "VIPI":
		case "VTAI":
		case "VILI":
		case "VBAI":
			json = getInsuranceRequest(transactionRefNo);
			break;
		default:
			json = null;
		}
		return json;
	}

	@Override
	public PGDetails findMerchantByMobile(String mobileNumber) {
		PGDetails pgDetails = null;
		List<PGDetails> pgList = (List<PGDetails>) pgDetailsRepository.findAll();
		for (PGDetails pg : pgList) {
			if (pg != null) {
				User merchant = pg.getUser();
				if (merchant != null) {
					String mobile = merchant.getUserDetail().getContactNo();
					if (mobileNumber.equals(mobile)) {
						pgDetails = pg;
						break;
					}
				}
			}
		}
		return pgDetails;
	}

	@Override
	public ArrayList<Long> getDailyTransactionsCountFromDate() {
		ArrayList<Long> fiveTransactions = new ArrayList<>();
		Calendar calender = Calendar.getInstance();
		for (int i = -1; i >= -5; i--) {
			calender.add(Calendar.DATE, i);
			Date date = new Date(calender.getTimeInMillis());
			fiveTransactions.add(pqTransactionRepository.countDailyTransactionByDate(date));
		}
		return fiveTransactions;
	}

	@Override
	public ArrayList<Double> getDailyTransactionsAmountFromDate() {
		ArrayList<Double> fiveTransactions = new ArrayList<>();
		Calendar calender = Calendar.getInstance();
		for (int i = -1; i >= -5; i--) {
			calender.add(Calendar.DATE, i);
			Date date = new Date(calender.getTimeInMillis());
			fiveTransactions.add(pqTransactionRepository.countDailyTransactionAmountByDate(date));
		}
		return fiveTransactions;
	}

	@Override
	public ResponseDTO requestOfflinePayment(PaymentRequestDTO dto, User merchant, User user) {
		ResponseDTO result = new ResponseDTO();
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		userRepository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.OFFLINE_VERIFICATION_MOBILE, user,
				merchant.getUserDetail().getFirstName());
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("OTP sent to ::" + user.getUsername());
		result.setDetails(dto.getAmount());
		return result;
	}

	@Override
	public void changePassword(ChangePasswordDTO dto, Long id) {
		User user = userRepository.findOne(id);
		user.setPassword(passwordEncoder.encode(dto.getPassword()));
		User u = userRepository.save(user);
		System.err.println(u);
	}

	@Override
	public void saveLMRequest(EBSRequest dto, String transactionRefNo) {
		LMRequestLog log = lmRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new LMRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setAmount(dto.getAmount());
			log.setAccount_id(dto.getAccount_id());
			log.setAddress(dto.getAddress());
			log.setChannel(dto.getChannel());
			log.setCity(dto.getCity());
			log.setCountry(dto.getCountry());
			log.setCurrency(dto.getCurrency());
			log.setDescription(dto.getDescription());
			log.setEmail(dto.getEmail());
			log.setMode(dto.getMode());
			log.setName(dto.getName());
			log.setPhone(dto.getPhone());
			log.setPostal_code(dto.getPostal_code());
			log.setState(dto.getState());
			log.setSecure_hash(dto.getSecure_hash());
			log.setReference_no(dto.getReference_no());
			log.setShip_address(dto.getShip_address());
			log.setShip_city(dto.getShip_city());
			log.setReturn_url(dto.getReturn_url());
			log.setShip_country(dto.getShip_country());
			log.setShip_name(dto.getShip_phone());
			log.setShip_phone(dto.getShip_phone());
			log.setShip_postal_code(dto.getShip_postal_code());
			lmRequestLogRepository.save(log);
		}
	}

	@Override
	public String getLMRequest(String transactionRefNo) {
		LMRequestLog log = lmRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveStoreRequest(PayStoreDTO dto, String transactionRefNo) {

	}

	@Override
	public String getStoreRequest(String transactionRefNo) {
		return null;
	}

	@Override
	public void saveBTransferRequest(SendMoneyBankDTO dto, String transactionRefNo) {
		BTransferRequestLog log = bTransferRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			try {
				log = new BTransferRequestLog();
				log.setAccountName(dto.getAccountName());
				log.setAccountNumber(dto.getAccountNumber());
				log.setAmount(dto.getAmount());
				log.setBankCode(dto.getBankCode());
				log.setIfscCode(dto.getIfscCode());
				log.setTransactionRefNo(transactionRefNo);
				bTransferRequestLogRepository.save(log);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getBTransferRequest(String transactionRefNo) {
		BTransferRequestLog log = bTransferRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public double calculateDbCrForAccount(PQAccountDetail accountDetail, boolean debit) {
		double amount = 0.0;
		try {
			amount = pqTransactionRepository.calculateTotalCrDb(TransactionType.DEFAULT, accountDetail, debit);
		} catch (Exception ex) {

		}
		return amount;
	}

	@Override
	public void saveSendMoneyRequest(SendMoneyMobileDTO dto, String transactionRefNo) {
		SMRequestLog log = smRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new SMRequestLog();
			log.setAmount(dto.getAmount());
			log.setTransactionRefNo(transactionRefNo);
			log.setMessage(dto.getMessage());
			log.setMobileNumber(dto.getMobileNumber());
			smRequestLogRepository.save(log);
		}
	}

	// send money to donatee

	@Override
	public void saveSendMoneyToDonateeRequest(SendMoneyDonateeDTO dto, String transactionRefNo) {
		SMRequestLog log = smRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new SMRequestLog();
			log.setAmount(dto.getAmount());
			log.setTransactionRefNo(transactionRefNo);
			log.setMessage(dto.getMessage());
			log.setMobileNumber(dto.getDonatee());
			smRequestLogRepository.save(log);
		}
	}

	@Override
	public String getSendMoneyRequest(String transactionRefNo) {
		String json = "";
		SMRequestLog log = smRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveTopupRequest(CommonRechargeDTO dto, String transactionRefNo) {
		MTRequestLog log = mtRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new MTRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setAmount(dto.getAmount());
			log.setArea(dto.getArea());
			log.setServiceProvider(dto.getServiceProvider());
			log.setMobileNo(dto.getMobileNo());
			log.setFromMdex(dto.isFromMdex());
			mtRequestLogRepository.save(log);
		}
	}

	@Override
	public String getTopupRequest(String transactionRefNo) {
		String json = "";
		MTRequestLog log = mtRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveDTHRequest(CommonRechargeDTO dto, String transactionRefNo) {
		DTHRequestLog log = dthRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new DTHRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setAmount(dto.getAmount());
			log.setDthNo(dto.getDthNo());
			log.setServiceProvider(dto.getServiceProvider());
			dthRequestLogRepository.save(log);
		}
	}

	@Override
	public String getDTHRequest(String transactionRefNo) {
		DTHRequestLog log = dthRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveLandlineRequest(CommonRechargeDTO dto, String transactionRefNo) {
		LandlineRequestLog log = landlineRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new LandlineRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setServiceProvider(dto.getServiceProvider());
			log.setAmount(dto.getAmount());
			log.setAccountNumber(dto.getAccountNumber());
			log.setLandlineNumber(dto.getLandlineNumber());
			log.setStdCode(dto.getStdCode());
			landlineRequestLogRepository.save(log);
		}
	}

	@Override
	public String getLandlineRequest(String transactionRefNo) {
		LandlineRequestLog log = landlineRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveECityRequest(CommonRechargeDTO dto, String transactionRefNo) {
		ECityRequestLog log = eCityRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new ECityRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setAmount(dto.getAmount());
			log.setAccountNumber(dto.getAccountNumber());
			log.setBillingUnit(dto.getBillingUnit());
			log.setCityName(dto.getCityName());
			log.setCycleNumber(dto.getCycleNumber());
			log.setServiceProvider(dto.getServiceProvider());
			log.setProcessingCycle(dto.getProcessingCycle());
			eCityRequestLogRepository.save(log);
		}
	}

	@Override
	public String getECityRequest(String transactionRefNo) {
		ECityRequestLog log = eCityRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveGasRequest(CommonRechargeDTO dto, String transactionRefNo) {
		GasRequestLog log = gasRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new GasRequestLog();
			log.setTransactionRefNo(transactionRefNo);
			log.setAmount(dto.getAmount());
			log.setServiceProvider(dto.getServiceProvider());
			log.setAccount(dto.getAccountNumber());
			gasRequestLogRepository.save(log);
		}
	}

	@Override
	public String getGasRequest(String transactionRefNo) {
		GasRequestLog log = gasRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public void saveInsuranceRequest(CommonRechargeDTO dto, String transactionRefNo) {
		InsuranceRequestLog log = insuranceRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		if (log == null) {
			log = new InsuranceRequestLog();
			log.setAmount(dto.getAmount());
			log.setTransactionRefNo(transactionRefNo);
			log.setServiceProvider(dto.getServiceProvider());
			log.setPolicyDate(dto.getPolicyDate());
			log.setPolicyNumber(dto.getPolicyNumber());
			insuranceRequestLogRepository.save(log);
		}
	}

	@Override
	public String getInsuranceRequest(String transactionRefNo) {
		InsuranceRequestLog log = insuranceRequestLogRepository.findByTransactionRefNo(transactionRefNo);
		String json = "";
		if (log != null) {
			ObjectWriter writer = new ObjectMapper().writer();
			try {
				json = writer.writeValueAsString(log);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@Override
	public boolean updateAccountType(String username, boolean kyc) {
		boolean isUpdated = false;
		PQAccountType updationType = null;
		String code = "";
		User user = userRepository.findByUsername(username);
		if (user != null) {
			PQAccountDetail account = user.getAccountDetail();
			if (account != null) {
				PQAccountType accountType = account.getAccountType();
				if (accountType != null) {
					if (kyc) {
						code = "KYC";
					} else {
						code = "NONKYC";
					}
					updationType = pqAccountTypeRepository.findByCode(code);
					account.setAccountType(updationType);
					pqAccountDetailRepository.save(account);
					isUpdated = true;
				}
			}
		}
		return isUpdated;
	}

	@Override
	public boolean checkMobileToken(String key, String mobileNumber) {
		User user = userRepository.findByMobileTokenAndStatus(key, mobileNumber, Status.Inactive);
		if (user == null) {
			return false;
		} else {
			user.setMobileStatus(Status.Active);
			user.setMobileToken(null);
			userRepository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_SUCCESS, user, null);
			return true;
		}
	}

	@Override
	public boolean checkMobileTokenAadhar(String key, String mobileNumber) {
		User user = userRepository.findByMobileTokenAndStatus(key, mobileNumber, Status.Active);
		if (user == null) {
			return false;
		} else {
			user.setMobileStatus(Status.Active);
			user.setMobileToken(null);
			userRepository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_SUCCESS, user, null);
			return true;
		}
	}

	@Override
	public boolean checkMobileTokenForDirectPayment(String key, String mobileNumber) {
		User user = userRepository.findByMobileTokenAndStatusForDirectPayment(key, mobileNumber, Status.Active);
		if (user == null) {
			return false;
		} else {
			user.setMobileStatus(Status.Active);
			user.setMobileToken(null);
			userRepository.save(user);
			return true;
		}
	}

	@Override
	public boolean resendMobileToken(String username) {
		User u = userRepository.findByUsernameAndStatus(username, Status.Inactive);
		if (u == null) {
			return false;
		} else {
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, u, null);
			return true;
		}
	}

	@Override
	public boolean resendMobileTokenNew(String username) {
		User u = userRepository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			return false;
		} else {
			u.setMobileToken(CommonUtil.generateSixDigitNumericString());
			userRepository.save(u);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, u, null);
			return true;
		}
	}
	
	@Override
	public boolean resendUserMobileToken(String username) {
		User u = userRepository.findByUsernameAndStatus(username, Status.Inactive);
		if (u == null) {
			return false;
		} else {
			u.setMobileToken(CommonUtil.generateSixDigitNumericString());
			userRepository.save(u);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, u, null);
			return true;
		}
	}

	@Override
	public boolean resendMobileTokenDeviceBinding(String username) {
		User u = userRepository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			return false;
		} else {
			u.setMobileToken(CommonUtil.generateSixDigitNumericString());
			userRepository.save(u);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, u, null);
			return true;
		}
	}

	@Override
	public boolean activateEmail(String key) {
		User user = userRepository.findByEmailTokenAndEmailStatusAndMobileStatus(key, Status.Inactive, Status.Active);
		if (user != null) {
			user.setEmailStatus(Status.Active);
			user.setEmailToken(null);
			userRepository.save(user);
			mailSenderApi.sendNoReplyMail("Email Verification Successful", MailTemplate.VERIFICATION_SUCCESS, user,
					null);
			return true;
		}
		return false;
	}

	@Override
	public void changePasswordRequest(User u) {
		u.setMobileToken(CommonUtil.generateSixDigitNumericString());
		userRepository.save(u);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.CHANGE_PASSWORD_REQUEST, u, null);
	}

	@Override
	public UserDTO checkPasswordToken(String key) {
		User user = userRepository.findByOTPAndStatus(key, Status.Active);
		if (user != null) {
			UserDTO dto = ConvertUtil.convertUser(user);
			return dto;
		}
		return null;
	}

	@Override
	public void revertAuthority() {
		List<User> users = userRepository.findAllBlockedUsers();
		Date now = new Date();
		String authority = Authorities.USER + "," + Authorities.AUTHENTICATED;
		for (User user : users) {
			Date last = loginLogRepository.findLastLoginDateOfUser(user);
			if (last != null) {
				long hours = (now.getTime() - last.getTime()) / (1000 * 60 * 60);
				if (hours >= 12) {
					updateUserAuthority(authority, user.getId());
				}
			}
		}
	}

	@Override
	public void renewPassword(ChangePasswordDTO dto) {
		User user = userRepository.findByMobileTokenAndUsername(dto.getKey(), dto.getUsername());
		if (user != null) {
			user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
			user.setMobileToken(null);
			userRepository.save(user);
			mailSenderApi.sendChangePasswordMail("Password Changed Successful", MailTemplate.CHANGE_PASSWORD_SUCCESS,
					user, null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.CHANGE_PASSWORD_SUCCESS, user, null);
		}
	}

	@Override
	public boolean agentRenewPassword(ChangePasswordDTO dto) {
		User user = userRepository.findByUsernameAndStatus(dto.getUsername(), Status.Active);
		if (user != null) {
			user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
			user.setMobileToken(null);
			userRepository.save(user);
			mailSenderApi.sendChangePasswordMail("Password Changed Successful", MailTemplate.CHANGE_PASSWORD_SUCCESS,
					user, null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.CHANGE_PASSWORD_SUCCESS, user, null);
			return true;
		}
		return false;
	}

	@Override
	public void renewPasswordFromAccount(ChangePasswordDTO dto) {
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null) {

			user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
			userRepository.save(user);
			mailSenderApi.sendChangePasswordMail("Password Changed Successful", MailTemplate.CHANGE_PASSWORD_SUCCESS,
					user, null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.CHANGE_PASSWORD_SUCCESS, user, null);

		}
	}

	@Override
	public void toggleStatus(String username, Status status) {
		User u = userRepository.findByUsername(username.toLowerCase());
		if (u != null) {
			u.setEmailStatus(status);
			userRepository.save(u);
		}
	}

	@Override
	public KYCResponse verifyByKycApi(KycDTO dto) {
		boolean valid = false;
		KYCResponse response = new KYCResponse();
		String stringResponse = "";
		try {
			String payload = PayQwikUtil.getInString(dto);
			String newPayload = URLEncoder.encode(payload);
			System.err.println("Payload::" + payload);
			WebResource resource = Client.create().resource(PayQwikUtil.KYC_URL).queryParam("INPUT", newPayload);
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			System.out.println("KYC Response is not 200::" + stringResponse);
			String[] responseParts = stringResponse.split("\\r?\\n");
			String jsonResponse = responseParts[0].trim();
			// String jsonResponse =
			// "{\"INDV_ACNT\":[{\"ACNO\":\"601900301000759\",\"ACNONAME\":\"MASHOOR
			// GULATI\",\"MOBNO\":\"9911389332\"}],\"MOBILE\":\"9911389332\",\"NOOFACNT\":\"1\",\"RESPONSECODE\":\"000\",\"RESPONSEDESC\":\"SUCCESS\"}";
			org.json.JSONObject json = new org.json.JSONObject(jsonResponse);
			if (json != null) {
				String responseCode = JSONParserUtil.getString(json, "RESPONSECODE");
				if (responseCode.equalsIgnoreCase("000")) {
					String noOfAccount = JSONParserUtil.getString(json, "NOOFACNT");
					int accounts = Integer.parseInt(noOfAccount);
					if (accounts > 0) {
						valid = true;
						System.err.println("valid is true");
						JSONArray accountArray = json.getJSONArray("INDV_ACNT");
						org.json.JSONObject accountOne = accountArray.getJSONObject(0);
						String accountNo = JSONParserUtil.getString(accountOne, "ACNO");
						String accountName = JSONParserUtil.getString(accountOne, "ACNONAME");
						String mobileNo = JSONParserUtil.getString(accountOne, "MOBNO");
						response.setAccountName(accountName);
						response.setMobileNo(mobileNo);
						response.setAccountNo(accountNo);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.setValid(valid);
		return response;
	}

	@Override
	public AmountDTO getAdminLoginValues() {
		User commission = findByUserName(StartupUtil.COMMISSION);
		User eTravos = findByUserName("ggupta@firstglobalmoney.com");
		User mvisa = findByUserName(StartupUtil.MVISA);
		User flight = findByUserName(StartupUtil.FLIGHT_BOOKING);
		User bus = findByUserName(StartupUtil.BUS_BOOKING);
		User mbank = findByUserName(StartupUtil.MBANK);
		PQAccountDetail commissionAccount = commission.getAccountDetail();
		double totalEBS = 0.0;
		double totalVNet = 0.0;
		double promobalance = 0.0;
		double totalPayable = 0.0;
		double iplbalance = 0.0;
		double totalUPI = 0.0;

		PQService loadMoneyService = pqServiceRepository.findServiceByCode("LMC");
		Double totalEBS1 = pqTransactionRepository.getValidAmountByService(loadMoneyService);
		if (totalEBS1 != null) {
			totalEBS = totalEBS1;
		}

		PQService upiService = pqServiceRepository.findServiceByCode("LMU");
		Double totalUPI1 = pqTransactionRepository.getValidAmountByService(upiService);
		if (totalUPI1 != null) {
			totalUPI = totalUPI1;
		}
		PQService vNetService = pqServiceRepository.findServiceByCode("LMB");
		Double totalVNet1 = pqTransactionRepository.getValidAmountByService(vNetService);
		if (totalVNet1 != null) {
			totalVNet = totalVNet1;
		}
		PQService promocodeService = pqServiceRepository.findServiceByCode("PPS");
		Double promobalance1 = pqTransactionRepository.getValidAmountByService(promocodeService);
		if (promobalance1 != null) {
			promobalance = promobalance1;
		}

		PQService iplService = pqServiceRepository.findServiceByCode("PAW");
		Double iplbalance1 = pqTransactionRepository.getValidAmountByService(iplService);
		if (iplbalance1 != null) {
			iplbalance = iplbalance1;
		}

		/*PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
		Double totalPayable1 = pqTransactionRepository.getValidAmountByTopUpService(serviceType);
		if (totalPayable1 != null) {
			totalPayable = totalPayable1;
		}*/

		PQService bankService = pqServiceRepository.findServiceByCode("SMB");
		double bankAmount = pqTransactionRepository.getValidAmountByBankService(bankService);

		double bankTransferCommission = pqTransactionRepository.getValidAmountByBankServiceCommission(bankService);
		double poolBalance = userRepository.findUsersWalletBalance();

		double totalEBSNow = 0;
		double topupTotal = 0;
		long totalUsers = 0;
		double totalVNetNow = 0;
		long totalMale = 0;
		long totalFemale = 0;
		long totalTransactions = 0;
		long onlineUsers = 0;
		String date = formatter.format(new Date());
		Date newDate = null;
		try {
			newDate = formatter.parse(date);
			totalTransactions = pqTransactionRepository.countTotalTransactionsByStatus(Status.Success);
			totalUsers = userRepository.getTotalUsersCount(UserType.User);
			totalMale = userDetailRepository.countUsersByGender(Gender.M);
			totalFemale = userDetailRepository.countUsersByGender(Gender.F);
			onlineUsers = sessionApi.countActiveSessions();
		} catch (Exception e) {
			e.printStackTrace();
		}

		AmountDTO amountDTO = new AmountDTO();
		amountDTO.setBankAmount(bankAmount);

		if (mbank != null) {
			double mBankAmount = mbank.getAccountDetail().getBalance();
			amountDTO.setmBankAmount(mBankAmount);
		}

		amountDTO.setWalletBalance(poolBalance);
		amountDTO.setTotalUsers(totalUsers);
		amountDTO.setTotalLoadMoneyEBS(totalEBS);
		amountDTO.setTotalLoadMoneyVNet(totalVNet);
		amountDTO.setTotalPayable(totalPayable);
		if (eTravos != null) {
			amountDTO.setMerchantPayable(eTravos.getAccountDetail().getBalance());
		}
		amountDTO.setMvisaTransaction(mvisa.getAccountDetail().getBalance());
		double busTransaction = bus.getAccountDetail().getBalance();
		double flightTrnsaction = flight.getAccountDetail().getBalance();
		double totalTransaction = busTransaction + flightTrnsaction;
		amountDTO.setTravelTransaction(totalTransaction);
		if (commission != null) {
			double commissionAmount = pqTransactionRepository.countTotalCommission(Status.Success,
					TransactionType.COMMISSION, commissionAccount);
			amountDTO.setTotalCommission(commissionAmount);
		}
		amountDTO.setTotalLoadMoneyEBSNow(totalEBSNow);
		amountDTO.setTotalLoadMoneyVNETNow(totalVNetNow);
		amountDTO.setTotalPayableNow(topupTotal);
		amountDTO.setPromoBalance(promobalance);
		amountDTO.setIplBalance(iplbalance);
		amountDTO.setFemaleUsers(totalFemale);
		amountDTO.setMaleUsers(totalMale);
		amountDTO.setTotalTransaction(totalTransactions);
		amountDTO.setDailyCounts(getDailyTransactionsCountFromDate());
		amountDTO.setDailyAmounts(getDailyTransactionsAmountFromDate());
		amountDTO.setBankTransferCommission(bankTransferCommission);
		amountDTO.setOnlineUsers(onlineUsers);
		amountDTO.setTotalLoadMoneyUPI(totalUPI);

		return amountDTO;
	}

	@Override
	public UserDTO getUserById(Long id) {
		User u = userRepository.findOne(id);
		if (u != null) {
			return ConvertUtil.convertUser(u);
		}
		return null;
	}

	@Override
	public List<User> getUserListByPage(int page, int perPage) {
		Pageable pageable = new PageRequest(page, perPage);
		List<User> result = userRepository.findWithPageable(pageable);
		return result;
	}

	@Override
	public long getUserCount() {
		return userRepository.getUserCount();
	}

	@Override
	public double getTotalWalletBalance() {
		double walletBalance = 0;
		double balance = 0;
		List<User> userList = userRepository.getTotalUsers(UserType.User);
		List<PGDetails> pgDetailsList = (List<PGDetails>) pgDetailsRepository.findAll();
		if (pgDetailsList != null && !(pgDetailsList.isEmpty())) {
			for (PGDetails pg : pgDetailsList) {
				userList.add(pg.getUser());
			}
		}
		for (User u : userList) {
			balance = u.getAccountDetail().getBalance();
			walletBalance += balance;
		}
		return walletBalance;
	}

	@Override
	public void updateUserAuthority(String authority, long id) {
		userRepository.updateUserAuthority(authority, id);
	}

	@Override
	public String handleLoginFailure(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		// String loginUsername = (String) authentication.getPrincipal();
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					LoginLog loginLog = new LoginLog();
					loginLog.setUser(user);
					loginLog.setRemoteAddress(ipAddress);
					loginLog.setServerIp(request.getRemoteAddr());
					loginLog.setStatus(Status.Failed);
					loginLogRepository.save(loginLog);

					List<LoginLog> llsFailed = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getLoginAttempts() - failedCount;
					if (remainingAttempts < 0 || failedCount == getLoginAttempts()) {
						if (user.getUserType() == UserType.Merchant) {
							String authority = Authorities.MERCHANT + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.Admin) {
							String authority = Authorities.ADMINISTRATOR + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						} else if (user.getUserType() == UserType.SuperAdmin) {
							String authority = Authorities.SUPER_ADMIN + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect password. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				LoginLog loginLog = new LoginLog();
				loginLog.setUser(user);
				loginLog.setRemoteAddress(ipAddress);
				loginLog.setServerIp(request.getRemoteAddr());
				loginLog.setStatus(Status.Success);
				loginLogRepository.save(loginLog);
				List<LoginLog> lls = loginLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
				for (LoginLog ll : lls) {
					loginLogRepository.deleteLoginLogForId(Status.Deleted, ll.getId());
				}
			}
		}
	}

	@Override
	public String handleMpinFailure(HttpServletRequest request, String loginUsername) {
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				if (user.getMobileStatus() == Status.Active) {
					if (user.getAuthority().contains(Authorities.BLOCKED)) {
						return "Your account is blocked! Please contact support.";
					}
					MpinLog mpinLog = new MpinLog();
					mpinLog.setUser(user);
					mpinLog.setServerIpAddress(request.getRemoteAddr());
					mpinLog.setStatus(Status.Failed);
					mpinLogRepository.save(mpinLog);
					List<MpinLog> llsFailed = mpinLogRepository.findTodayEntryForUserWithStatus(user, Status.Failed);
					int failedCount = llsFailed.size();
					int remainingAttempts = getMpinAttempts() - failedCount;
					if (failedCount == getMpinAttempts()) {
						if (user.getUserType() == UserType.User) {
							String authority = Authorities.USER + "," + Authorities.BLOCKED;
							updateUserAuthority(authority, user.getId());
						}
						return "Your account is blocked! Please try after 24 hrs.";
					}
					return "Incorrect mpin. Remaining attempts " + remainingAttempts;
				} else {
					return "User doesn't exists";
				}
			} else {
				return "User doesn't exists";
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public String handleMpinSuccess(HttpServletRequest request, String loginUsername, String ipAddress) {
		if (loginUsername != null) {
			User user = userRepository.findByUsername(loginUsername);
			if (user != null) {
				MpinLog mpinLog = new MpinLog();
				mpinLog.setUser(user);
				mpinLog.setServerIpAddress(request.getRemoteAddr());
				mpinLog.setStatus(Status.Success);
				mpinLogRepository.save(mpinLog);
			}
		}
		return "Authentication Failed. Please try again";
	}

	@Override
	public String authenticateVersion(String version) {
		if (version != null && version.contains(".")) {
			int mainVersion = 0;
			int subVersion = 0;
			PQVersion pqVersion = null;
			String[] versionParts = version.split("\\.");
			mainVersion = Integer.parseInt(versionParts[0]);
			subVersion = Integer.parseInt(versionParts[1]);
			if (mainVersion > 0) {
				pqVersion = pqVersionRepository.findByVersionNo(mainVersion, subVersion);
			}
			if (pqVersion != null) {
				if (pqVersion.getStatus().equals(Status.Active)) {
					return "true|Valid Version";
				} else if (pqVersion.getStatus().equals(Status.Inactive)) {
					PQVersion latestVersion = pqVersionRepository.findLatestVersion();
					return "false|Please Update Your app to version " + latestVersion.getVersionCode() + "."
							+ latestVersion.getSubversionCode();

				}
			}
		}
		return "Not a Valid Version Format";
	}

	@Override
	public int updatePoints(long points, PQAccountDetail account) {
		return pqAccountDetailRepository.updateUserPoints(points, account.getId());
	}

	@Override
	public int getLoginAttempts() {
		return 5;
	}

	@Override
	public int getMpinAttempts() {
		return 5;
	}

	@Override
	public int updateBalance(double balance, User user) {
		return pqAccountDetailRepository.updateUserBalance(balance, user.getAccountDetail().getId());
	}

	@Override
	public double dailyTransactionTotal(User user) {
		Calendar now = Calendar.getInstance();
		double total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getDailyTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), user.getAccountDetail());
			if (trans.size() != 0) {
				total = pqTransactionRepository.getDailyTransactionTotal(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), user.getAccountDetail());
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public void blockUser(String username) {
		User u = findByUserName(username);
		if (u != null) {
			String authority = u.getAuthority();
			if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
				authority = Authorities.USER + "," + Authorities.LOCKED;
				updateUserAuthority(authority, u.getId());
				sessionApi.clearAllSessionForUser(u);
			}
		}
	}

	@Override
	public double dailyDebitTransactionTotal(User user) {
		Calendar now = Calendar.getInstance();
		double total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getDailyDebitTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), user.getAccountDetail(), true);
			if (trans.size() != 0) {
				total = pqTransactionRepository.getDailyDebitTransactionTotal(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), user.getAccountDetail(), true);
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public double monthlyTransactionTotal(User user) {
		Calendar now = Calendar.getInstance();
		double total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getMonthlyTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
			if (trans.size() != 0) {
				total = pqTransactionRepository.getMonthlyTransactionTotal(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public long monthlyTransactionTotalBank(User user) {
		Calendar now = Calendar.getInstance();
		long total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getMonthlyTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
			if (trans.size() != 0) {
				total = pqTransactionRepository.getMonthlyTransactionTotalCountBank(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}
	
	@Override
	public long dailyTransactionTotalBank(User user) {
		Calendar now = Calendar.getInstance();
		long total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getDailyTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1),now.get(Calendar.DATE), user.getAccountDetail());
			if (trans.size() != 0) {
				total = pqTransactionRepository.getDailyTransactionTotalCountBank(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1),now.get(Calendar.DATE), user.getAccountDetail());
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}
	

	@Override
	public List<User> getUserListByVerifiedUsersPage(int page, int perPage) {
		Pageable pageable = new PageRequest(page, perPage);
		List<User> result = userRepository.findWithVerifiedUsersPageable(pageable);
		return result;
	}

	@Override
	public void saveUnregisteredUserSendMoney(RegisterDTO dto) throws ClientException {
		try {
			User user = new User();
			UserDetail userDetail = new UserDetail();
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				}
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
			} else {
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Inactive);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public User findByAccountDetail(PQAccountDetail accountDetail) {
		return userRepository.findByAccountDetails(accountDetail);
	}

	@Override
	public PQAccountDetail saveOrUpdateAccount(PQAccountDetail accountDetail) {
		return pqAccountDetailRepository.save(accountDetail);
	}

	@Override
	public boolean saveImage(User user, ImageDTO dto) {
		// SAVE IMAGE IN USERDETAIL
		UserDetail detail = user.getUserDetail();
		byte[] byteArr = Base64.getDecoder().decode(dto.getEncodedBytes());
		System.err.println("byteArr" + byteArr);
		detail.setImageContent(byteArr);
		detail.setImage(dto.getContentType());
		userDetailRepository.save(detail);
		return true;
	}

	@Override
	public int updateVersion(int versionCode, int subVersionCode) {
		PQVersion version = pqVersionRepository.findByVersionNo(versionCode, subVersionCode);
		int rowsUpdated = 0;
		if (version == null) {
			version = new PQVersion();
			version.setVersionCode(versionCode);
			version.setSubversionCode(subVersionCode);
			version.setStatus(Status.Active);
			pqVersionRepository.save(version);
			rowsUpdated = pqVersionRepository.disablePreviousVersionsBeforeID(version.getId());
		} else {
			rowsUpdated = pqVersionRepository.activateAllVersionsAfterID(version.getId());
		}
		return rowsUpdated;
	}

	@Override
	public List<PQTransaction> getUserTransactions(User user) {
		List<PQTransaction> listOfTransactions = pqTransactionRepository.getTotalTransactions(user.getAccountDetail());
		return listOfTransactions;
	}

	@Override
	public boolean setNewMpin(MpinDTO dto) {
		boolean updated = false;
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null && user.getUserDetail().getMpin() == null) {
			String mpin = dto.getNewMpin();
			try {
				String hashedMpin = SecurityUtil.sha512(mpin);
				userDetailRepository.updateUserMPIN(hashedMpin, user.getUsername());
				updated = true;
			} catch (Exception e) {
				updated = false;
			}
		}
		return updated;
	}

	@Override
	public ResponseDTO setMpinHistory(MpinDTO dto) throws Exception {
		ResponseDTO response = new ResponseDTO();
		User user = userRepository.findByUsername(dto.getUsername());
		List<MpinHistory> listMpin = mpinHistoryRepository.findByListUser(user);
		if (listMpin.size() == 0 && user.getUserDetail().getMpin() != null) {
			MpinHistory mpin = new MpinHistory();
			System.err.println();
			String mPIN = dto.getNewMpin();
			String hashedMpin = SecurityUtil.sha512(mPIN);
			mpin.setMpin(hashedMpin);
			mpin.setUser(user);
			mpinHistoryRepository.save(mpin);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setCode("S00");
			response.setMessage("MPIN Updated");
			response.setValid(true);
		} else if (listMpin != null && listMpin.size() < 5) {
			Map<String, Object> detail = new HashMap<String, Object>();
			int i = 0;
			for (MpinHistory mpinHistory : listMpin) {
				detail.put("mpin" + (i++), mpinHistory.getMpin());
			}
			if (!(detail.containsValue(SecurityUtil.sha512(dto.getNewMpin())))) {
				MpinHistory newMpin = new MpinHistory();
				String mPIN = dto.getNewMpin();
				String hashedMpin = SecurityUtil.sha512(mPIN);
				newMpin.setMpin(hashedMpin);
				newMpin.setUser(user);
				mpinHistoryRepository.save(newMpin);
				response.setStatus(ResponseStatus.SUCCESS);
				response.setCode("S01");
				response.setMessage("Mpin Saved Successfully");
				response.setValid(true);
			} else {
				response.setCode("F00");
				response.setMessage("Mpin can 't be same as Previous One");
				response.setValid(false);

			}
		} else if (listMpin != null && listMpin.size() == 5) {
			Map<String, Object> detail = new HashMap<String, Object>();
			int i = 0;
			for (MpinHistory mpinHistory : listMpin) {
				detail.put("mpin" + (i++), mpinHistory.getMpin());
			}
			if (detail.containsValue(SecurityUtil.sha512(dto.getNewMpin()))) {
				response.setCode("F00");
				response.setMessage("Mpin can't be same as Previous One");
				response.setValid(false);
			} else {
				List<MpinHistory> history = mpinHistoryRepository.updateUser(user);
				if (history != null) {
					if (history.size() > 0) {
						MpinHistory setHistory = (MpinHistory) history.get(0);
						String mPIN = dto.getNewMpin();
						String hashedMpin = SecurityUtil.sha512(mPIN);
						SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						Date now = new Date();
						String strDate = sdfDate.format(now);
						Date date = format.parse(strDate);
						setHistory.setCreated(date);
						setHistory.setMpin(hashedMpin);
						setHistory.setUser(user);
						mpinHistoryRepository.save(history);
						response.setStatus(ResponseStatus.SUCCESS);
						response.setCode("S02");
						response.setMessage("MPIN Updated Successfully");
						response.setValid(true);
					}
				}
			}
		}
		return response;
	}

	@Override
	public ResponseDTO setPasswordHistory(ChangePasswordDTO dto) throws Exception {
		ResponseDTO response = new ResponseDTO();
		String toBChangedPassword = dto.getNewPassword();
		String hashedPasswordChanged = passwordEncoder.encode(toBChangedPassword);
		User user = userRepository.findByUsername(dto.getUsername());
		if ((passwordEncoder.matches(dto.getNewPassword(), user.getPassword()))) {
			response.setCode("F00");
			response.setMessage("Please use a different password other than the previous one");
			response.setValid(false);
			response.setDetails("Please use a different password other than the previous one");
			return response;
		}
		List<PasswordHistory> listPassword = passwordHistoryRepository.findByListUser(user);
		if (listPassword.size() == 0 && user.getPassword() != null) {
			System.err.println("inside if condition");
			PasswordHistory pass = new PasswordHistory();
			pass.setPassword(hashedPasswordChanged);
			pass.setUser(user);
			passwordHistoryRepository.save(pass);
			response.setStatus(ResponseStatus.SUCCESS);
			response.setCode("S00");
			response.setMessage("Password Updated");
			response.setValid(true);
			response.setDetails("Password Updated");
		} else if (listPassword != null && listPassword.size() < 5) {
			System.err.println("inside elseif condition size <5");
			Map<String, Object> detail = new HashMap<String, Object>();
			int i = 0;
			for (PasswordHistory passwordHistory : listPassword) {
				if (passwordEncoder.matches(toBChangedPassword, passwordHistory.getPassword())) {
					detail.put("password" + (i++), passwordHistory.getPassword());
				}
			}
			if ((detail.isEmpty())) {
				PasswordHistory newPass = new PasswordHistory();
				// String pass=dto.getNewPassword();
				// String hashedPassword = passwordEncoder.encode(pass);
				newPass.setPassword(hashedPasswordChanged);
				newPass.setUser(user);
				passwordHistoryRepository.save(newPass);
				response.setStatus(ResponseStatus.SUCCESS);
				response.setCode("S01");
				response.setMessage("Password Saved Successfully");
				response.setValid(true);
				response.setDetails("Password Saved Successfully");
			} else {
				System.err.println("Password Already Exist");
				response.setCode("F00");
				response.setMessage(
						"Please enter new password.Your password must be different than last 5 used passwords");
				response.setValid(false);
				response.setDetails(
						"Please enter new password.Your password must be different than last 5 used passwords");

			}
		} else if (listPassword != null && listPassword.size() == 5) {
			System.err.println("inside elseif condition size ==5");
			Map<String, Object> detail = new HashMap<String, Object>();
			int i = 0;
			for (PasswordHistory passwordHistory : listPassword) {
				if (passwordEncoder.matches(toBChangedPassword, passwordHistory.getPassword())) {
					detail.put("mpin" + (i++), passwordHistory.getPassword());
				}
			}
			if (!(detail.isEmpty())) {
				System.err.println("PASSWORD ALREADY EXIST");
				response.setCode("F00");
				response.setMessage(
						"Please enter New Password.Your password must be different than last 5 used passwords");
				response.setValid(false);
				response.setDetails(
						"Please enter New Password.Your password must be different than last 5 used passwords");
			} else {
				System.err.println("inside else condition");
				List<PasswordHistory> history = passwordHistoryRepository.updateUser(user);
				if (history != null) {
					if (history.size() > 0) {
						PasswordHistory setHistory = (PasswordHistory) history.get(0);
						setHistory.setPassword(hashedPasswordChanged);
						SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						Date now = new Date();
						String strDate = sdfDate.format(now);
						Date date = format.parse(strDate);
						setHistory.setCreated(date);
						System.err.println("date ::" + setHistory.getCreated());
						setHistory.setUser(user);
						passwordHistoryRepository.save(setHistory);
						response.setStatus(ResponseStatus.SUCCESS);
						response.setCode("S02");
						response.setMessage("Password updated successfully");
						response.setValid(true);
						response.setDetails("Password updated successfully");
					}
				}
			}
		}
		return response;
	}

	@Override
	public boolean changeCurrentMpin(MpinChangeDTO dto) {
		boolean updated = false;
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null && user.getUserDetail().getMpin() != null) {
			String mpin = dto.getNewMpin();
			try {
				String hashedMpin = SecurityUtil.sha512(mpin);
				userDetailRepository.updateUserMPIN(hashedMpin, user.getUsername());
				updated = true;
			} catch (Exception e) {
				updated = false;
			}
		}
		return updated;
	}

	@Override
	public boolean deleteCurrentMpin(String username) {
		boolean deleted = false;
		User user = userRepository.findByUsername(username);
		if (user != null && user.getUserDetail().getMpin() != null) {
			userDetailRepository.deleteUserMPIN(username);
			deleted = true;
		}
		return deleted;
	}

	@Override
	public Page<User> getTotalUsers(Pageable pageable) {
		return userRepository.findAll(pageable);
	}

	@Override
	public double monthlyLoadMoneyTransactionTotal(User user) {
		// PQService loadMoneyService =
		// pqServiceRepository.findServiceByCode("LMC");
		Calendar now = Calendar.getInstance();
		double total = 0;
		try {
			total = pqTransactionRepository.getMonthlyTransactionTotal(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public int getTransactionTimeDifference() {
		return 1;
	}

	@Override
	public Page<User> getActiveUsers(Pageable pageable) {
		return userRepository.getActiveUsers(pageable);
	}

	@Override
	public Page<User> getInactiveUsers(Pageable pageable) {
		return userRepository.getInactiveUsers(pageable);
	}

	@Override
	public Page<User> getBlockedUsers(Pageable pageable) {
		return userRepository.getBlockedUsers(pageable);
	}

	@Override
	public Page<User> getLockedUsers(Pageable pageable) {
		return userRepository.getLockedUsers(pageable);
	}

	@Override
	public List<User> getAllUsers() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public boolean inviteByMobile(String number, String message, User u) {
		boolean valid = false;
		InviteLog exists = inviteLogRepository.getLogByUserAndMobile(u, number);
		if (exists == null) {
			valid = true;
			exists = new InviteLog();
			exists.setContactNo(number);
			exists.setUser(u);
			inviteLogRepository.save(exists);
			smsSenderApi.promotionalEmails(number, message);
		}
		return valid;
	}

	@Override
	public void inviteByEmail(String subject, String mailTemplate, User user) {
		mailSenderApi.sendNoReplyMail(subject, mailTemplate, user, null);
	}

	@Override
	public void inviteByEmailAddress(String subject, String mailTemplate, String email) {
		User user = new User();
		user.setUserDetail(new UserDetail());
		user.getUserDetail().setEmail(email);
		mailSenderApi.sendNoReplyMail(subject, mailTemplate, user, null);
	}

	@Override
	public void reSendEmailOTP(User user) {
		mailSenderApi.sendNoReplyMail("Email Verification", MailTemplate.VERIFICATION_EMAIL, user, null);
	}

	@Override
	public boolean redeemCode(User user, String promoCode) {
		user.getId();
		return false;
	}

	@Override
	public boolean changeEmail(User user, String email) {
		Status isActive = user.getEmailStatus();
		if (isActive.equals(Status.Inactive)) {
			userDetailRepository.updateChangeEmail(email, user.getUserDetail().getId());
			return true;
		}
		return false;
	}

	@Override
	public List<PQVersion> getAllVersions() {
		return pqVersionRepository.getAllVersions();
	}

	@Override
	public PQVersion getLatestVersion() {
		return pqVersionRepository.findLatestVersion();
	}

	@Override
	public int updateGcmId(String gcmId, String username) {
		int rowsUpdated = userRepository.updateGCMID(gcmId, username);
		return rowsUpdated;
	}

	@Override
	public User saveOrUpdateUser(User u) {
		return userRepository.save(u);
	}

	@Override
	public double getBalanceByUserAccount(PQAccountDetail accountDetail) {
		return pqAccountDetailRepository.getBalanceByUserAccount(accountDetail.getId());
	}

	@Override
	public List<LoginLog> getLastLoginOfUser(User u, Status status) {
		return loginLogRepository.findLastLoginOfUser(u, status);
	}

	@Override
	public boolean isValidLastLoginDevice(String device, User user) {
		boolean isValidDevice = false;
		List<LoginLog> loginLog = getLastLoginOfUser(user, Status.Success);
		if (loginLog != null && loginLog.size() > 0) {
			LoginLog lg = (LoginLog) loginLog.get(0);
			if (lg != null) {
				String deviceId = lg.getRemoteAddress();
				if (deviceId != null) {
					isValidDevice = deviceId.equalsIgnoreCase(device);
				}
			}
		}
		return isValidDevice;
	}

	@Override
	public void requestNewLoginDevice(User user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		userRepository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public void requestNewLoginDeviceForSuperAdmin(User user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		userRepository.save(user);
		smsSenderApi.sendAnotherUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.SUPERADMIN_VERIFICATION_MOBILE, user, null);
	}

	@Override
	public boolean isValidLoginToken(String otp, User user) {
		boolean isValidToken = false;
		String mobileToken = user.getMobileToken();
		if (mobileToken != null) {
			if (otp.equalsIgnoreCase(mobileToken)) {
				isValidToken = true;
				user.setMobileToken(null);
				userRepository.save(user);
			}
		}
		return isValidToken;
	}

	@Override
	public void merchantSignUp(RegisterDTO dto) throws ClientException {
		try {
			System.err.println("-------------------- INSIDE MERCHANT SIGN UP API ----------------------");
			User user = new User();
			UserDetail userDetail = new UserDetail();
			System.err.println("this is dto::" + dto);
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());

			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
					user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				}
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				userDetail.setLocation(location);
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			if (dto.isVbankCustomer()) {
				pqAccountDetail.setVijayaAccountNumber(dto.getBankAccountNo());
				pqAccountDetail.setBranchCode(dto.getIfscCode());
			}
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			MerchantDetails merchantDetails = new MerchantDetails();
			// merchantDetails.setAadharNo(dto.getAadharNo());
			merchantDetails.setPanCardNo(dto.getPanCardNo());
			merchantDetails.setBankAccountName(dto.getAccountName());
			merchantDetails.setBankAccountNo(dto.getBankAccountNo());
			merchantDetails.setBankName(dto.getBankName());
			merchantDetails.setBranchName(dto.getBranchName());
			merchantDetails.setIfscCode(dto.getIfscCode());
			user.setMerchantDetails(merchantDetails);
			// merchantDetails.setUser(user);

			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				System.err.println("this is mobile token::" + user.getMobileToken());
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				merchantDetailsRepository.save(merchantDetails);
				User u = userRepository.save(user);
				System.err.println(u);
			} else {
				System.err.println("this is mobile token temp usernt null:::" + user.getMobileToken());
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				merchantDetailsRepository.save(merchantDetails);
				userRepository.save(user);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public void registerMerchant(RegisterDTO dto) throws ClientException {
		try {
			System.err.println("-------------------- INSIDE MERCHANT SIGN UP API ----------------------");
			// User user = userRepository.findByUsername(dto.getUsername());
			// System.err.println("Merchant ID : " +user.getId());
			User user = new User();
			UserDetail userDetail = new UserDetail();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			userDetail.setLocation(location);
			user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			// user.setEmailToken("E" + System.currentTimeMillis());
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			if (dto.isVbankCustomer()) {
				pqAccountDetail.setVijayaAccountNumber(dto.getBankAccountNo());
				pqAccountDetail.setBranchCode(dto.getIfscCode());
			}
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			/*
			 * MerchantDetails merchantDetails = new MerchantDetails();
			 * merchantDetails.setAadharNo(dto.getAadharNo());
			 * merchantDetails.setPanCardNo(dto.getPanCardNo());
			 * merchantDetails.setBankAccountName(dto.getAccountName());
			 * merchantDetails.setBankAccountNo(dto.getBankAccountNo());
			 * merchantDetails.setBankName(dto.getBankName());
			 * merchantDetails.setBranchName(dto.getBranchName());
			 * merchantDetails.setIfscCode(dto.getIfscCode());
			 * merchantDetails.setUser(user);
			 */

			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				// merchantDetailsRepository.save(merchantDetails);
				userRepository.save(user);
			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				// merchantDetailsRepository.save(merchantDetails);
				userRepository.save(user);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public boolean checkMerchantMobileToken(String key, String mobileNumber) {
		UserDetail userDetail = userDetailRepository.findByContactNo(mobileNumber);
		if (userDetail == null) {
			return false;
		} else {
			mobileNumber = userDetail.getEmail();
			User user = userRepository.findByMobileTokenAndStatus(key, mobileNumber, Status.Inactive);
			if (user == null) {
				return false;
			} else {
				user.setMobileStatus(Status.Active);
				user.setMobileToken(null);
				userRepository.save(user);
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_SUCCESS, user, null);
				return true;
			}
		}
	}

	@Override
	public boolean resendMerchantMobileToken(String username) {
		UserDetail userDetail = userDetailRepository.findByContactNo(username);
		username = userDetail.getEmail();
		User u = userRepository.findByUsernameAndStatus(username, Status.Inactive);
		if (u == null) {
			return false;
		} else {
			// User user = new User();
			u.setMobileToken(CommonUtil.generateSixDigitNumericString());
			userRepository.save(u);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, u, null);
			return true;
		}
	}

	@Override
	public void unblockAdmins() {
		String adminAuthority = Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED;
		String superAdminAuthority = Authorities.SUPER_ADMIN + "," + Authorities.AUTHENTICATED;
		List<User> admins = userRepository.getAllBlockedAdmins(UserType.Admin, UserType.SuperAdmin);
		if (admins != null && !admins.isEmpty()) {
			for (User admin : admins) {
				String authority = admin.getAuthority();
				if (authority.contains(Authorities.BLOCKED)) {
					if (authority.contains(Authorities.ADMINISTRATOR)) {
						admin.setAuthority(adminAuthority);
						userRepository.save(admin);
					} else if (authority.contains(Authorities.SUPER_ADMIN)) {
						admin.setAuthority(superAdminAuthority);
						userRepository.save(admin);
					}
				}
			}
		}
	}

	@Override
	public void sendIpayBalanceAlert() {

		List<String> destinations = ConvertUtil.getAlertMobileList();
		BalanceRequest balanceRequest = new BalanceRequest();
		BalanceResponse response = balanceApi.request(balanceRequest);
		if (response.isSuccess()) {
			Balance balance = response.getBalance();
			String wallet = balance.getWallet();
			double bal = Double.parseDouble(wallet);
			if (bal <= InstantPayConstants.WALLET_THRESHOLD) {
				for (String destination : destinations) {
					smsSenderApi.sendAlertSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplate.IPAY_ALERT, destination,
							wallet);
				}
			}
		}
	}

	@Override
	public User createUsingKycResponse(KYCResponse response, POSMerchants posMerchant) {

		String accountNumber = ConvertUtil.convertToMerchant(response.getAccountNo());
		String username = ConvertUtil.convertToMerchant(response.getMobileNo());
		User user = findByUserName(username);
		PQAccountDetail account = null;
		UserDetail detail = null;
		if (user == null) {
			user = new User();
			account = new PQAccountDetail();
			detail = new UserDetail();
		} else {
			account = user.getAccountDetail();
			detail = user.getUserDetail();
		}
		MerchantDetails merchantDetails = merchantDetailsRepository.findByPanCardNo(posMerchant.getPanNo());
		if (merchantDetails == null) {
			merchantDetails = ConvertUtil.getFromPOS(posMerchant);
			merchantDetailsRepository.save(merchantDetails);
		}
		user.setUsername(username);
		user.setPassword(passwordEncoder.encode(response.getMobileNo()));
		user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
		user.setUserType(UserType.Merchant);
		user.setMobileStatus(Status.Inactive);
		user.setEmailStatus(Status.Active);
		detail.setFirstName(response.getAccountName());
		detail.setLastName(" ");
		detail.setContactNo(response.getMobileNo());
		detail.setEmail(posMerchant.getEmail());
		LocationDetails location = locationDetailsRepository.findLocationByPin(posMerchant.getPincode());
		detail.setLocation(location);
		userDetailRepository.save(detail);
		account.setVBankCustomer(true);
		account.setVijayaAccountNumber(accountNumber);
		PQAccountType accountType = pqAccountTypeRepository.findByCode(StartupUtil.KYC);
		account.setAccountType(accountType);
		VBankAccountDetail vbank = new VBankAccountDetail();
		vbank.setAccountName(response.getAccountName());
		vbank.setStatus(Status.Inactive);
		vbank.setMobileNumber(response.getMobileNo());
		try {
			vbank.setAccountNumber(SecurityUtil.sha1(accountNumber));
		} catch (Exception e) {
			vbank.setAccountNumber("NA");
		}
		String mobileToken = CommonUtil.generateSixDigitNumericString();
		vbank.setOtp(mobileToken);
		vBankAccountDetailRepository.save(vbank);
		account.setvBankAccount(vbank);
		account.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + detail.getId());
		account.setBalance(0);
		account.setPoints(0);
		pqAccountDetailRepository.save(account);
		user.setUserDetail(detail);
		user.setAccountDetail(account);
		System.err.println("merchant details" + merchantDetails);
		user.setMerchantDetails(merchantDetails);
		userRepository.save(user);
		smsSenderApi.sendKYCSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE_KYC, user,
				response.getMobileNo(), mobileToken);
		return user;
	}

	@Override
	public ResponseDTO verifyKYC(boolean request, MerchantRegisterDTO dto) {
		ResponseDTO result = new ResponseDTO();
		if (request) {
			// TODO verify using kyc api
			KycDTO kyc = ConvertUtil.convertFromMerchant(dto.getKycDTO());
			KYCResponse kycResponse = verifyByKycApi(kyc);
			if (kycResponse.isValid()) {
				// TODO create a merchant
				String accountNumber = kycResponse.getAccountNo();
				POSMerchants posMerchant = posMerchantsRepository.findByAccountNumber(accountNumber);
				if (posMerchant != null) {
					User user = createUsingKycResponse(kycResponse, posMerchant);
					dto.setFirstName(user.getUserDetail().getFirstName());
					dto.setMobileNumber(user.getUserDetail().getContactNo());
					result.setMessage("OTP sent to " + dto.getMobileNumber());
					result.setStatus(ResponseStatus.SUCCESS);
				} else {
					result.setStatus(ResponseStatus.NEW_REGISTRATION);
					result.setMessage("Not a Vijaya Bank Account");
				}
			} else {
				result.setStatus(ResponseStatus.NEW_REGISTRATION);
				result.setMessage("Not a Vijaya Bank Account");
			}
		} else {
			// TODO verify OTP and change status of mobile
			User user = findByUserName(dto.getKycDTO().getMobileNumber());
			if (user != null) {
				user.setMobileStatus(Status.Active);
				userRepository.save(user);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Verified Successfully");
				dto = ConvertUtil.convertToMerchantDTO(user);
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("User Not Exists");
			}
		}
		result.setDetails(dto);
		return result;
	}

	@Override
	public ResponseDTO createMerchantPassword(MerchantRegisterDTO dto) {
		ResponseDTO result = new ResponseDTO();
		User user = findByUserName(dto.getMobileNumber());
		if (user != null) {
			if (user.getUserType().equals(UserType.Merchant)) {
				long timeInMillis = user.getCreated().getTime();
				long currentMillis = System.currentTimeMillis();
				int mins = (int) (currentMillis - timeInMillis) / (1000 * 60);
				System.err.println("mins ::" + mins);
				if (mins <= 10) {
					user.setPassword(passwordEncoder.encode(dto.getPassword()));
					userRepository.save(user);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Password Generated Successfully");
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Request Timed Out");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not authorized to change password");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("User Not Found");
		}
		result.setDetails(dto);
		return result;
	}

	@Override
	public ResponseDTO changeAdminPassword(ChangePasswordDTO dto) {
		ResponseDTO result = new ResponseDTO();
		User admin = userRepository.findByUsername(dto.getUsername());
		if (admin != null) {
			if (dto.isRequest()) {
				String otp = CommonUtil.generateNDigitNumericString(6);
				admin.setMobileToken(otp);
				userRepository.save(admin);
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.CHANGE_PASSWORD_REQUEST, admin, null);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("OTP sent to " + admin.getUserDetail().getContactNo());
			} else {
				admin.setPassword(passwordEncoder.encode(dto.getNewPassword()));
				admin.setMobileToken(null);
				userRepository.save(admin);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Password Updated Successfully");
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.CHANGE_PASSWORD_SUCCESS, admin, null);
			}
		}
		return result;
	}

	@Override
	public Page<String> getGCMIDs(Pageable page) {
		return userRepository.getGCMIds(page, UserType.User);
	}

	@Override
	public ServiceStatus findByName(String name) {
		return serviceStatusRepository.findByName(name);
	}

	@Override
	public ServiceStatus setServiceStatusActive(ServiceStatus status, String name) {
		status.setStatus(Status.Active);
		serviceStatusRepository.save(status);
		status = serviceStatusRepository.findByName(name);
		return status;
	}

	@Override
	public boolean setServiceStatusInactive(String name) {
		ServiceStatus status = serviceStatusRepository.findByNameAndStatus(name, Status.Active);
		if (status == null) {
			return false;
		} else {
			status.setStatus(Status.Inactive);
			serviceStatusRepository.save(status);
			return true;
		}

	}

	@Override
	public List<ServiceStatusDTO> findAll() {
		return ConvertUtil.convertServiceList((List<ServiceStatus>) serviceStatusRepository.findAll());

	}

	@Override
	public List<PQAccountType> getAllAccountTypes() {
		return pqAccountTypeRepository.findAll();
	}

	@Override
	public ResponseDTO updateAccountType(KycLimitDTO dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			PQAccountType accountType = pqAccountTypeRepository.findByCode(dto.getAccountType());
			if (accountType != null) {
				accountType.setBalanceLimit(dto.getBalanceLimit());
				accountType.setDailyLimit(dto.getDailyLimit());
				accountType.setMonthlyLimit(dto.getMonthlyLimit());
				accountType.setTransactionLimit(dto.getTransactionLimit());
				PQAccountType updated = pqAccountTypeRepository.save(accountType);
				if (updated != null) {
					List<PQAccountType> status = getAllAccountTypes();
					result.setMessage("Updated Successfully");
					result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails(status);
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Account Type not found");
			}
		} catch (Exception ex) {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Bad Request");
		}
		return result;
	}

	public User getVisaMerchant(String entityId, String mVisaId) {
		VisaMerchant visaMerchant = visaMerchantRepository.findByMvisaIdEntityId(entityId, mVisaId);
		if (visaMerchant != null) {
			User user = visaMerchant.getUser();
			return user;
		}
		return null;
	}

	@Override
	public UserResponseDTO getUserByMobile(String username) {
		UserResponseDTO response = new UserResponseDTO();
		User user = userRepository.findByUsername(username);
		if (user != null) {
			response.setCreated(format.format(user.getCreated()));
			response.setAuthority(user.getAuthority());
			response.setEmail(user.getUserDetail().getEmail());
			response.setMobileStatus(user.getMobileStatus());
			response.setEmailStatus(user.getEmailStatus());
			response.setMobileToken(user.getMobileToken());
			response.setImage(user.getUserDetail().getImage());
			if (user.getUserDetail().getLocation() != null) {
				response.setPinCode(user.getUserDetail().getLocation().getPinCode());
				response.setCircleName(user.getUserDetail().getLocation().getCircleName());
			} else {
				response.setPinCode("");
				response.setCircleName("");
			}
			String accNo = user.getAccountDetail().getVijayaAccountNumber();
			if (accNo != null) {
				response.setVijayaBankAccount(accNo);
			} else {
				response.setVijayaBankAccount("");
			}
			response.setUserType(user.getUserType());
			response.setAccountType(user.getAccountDetail().getAccountType().getName());
			if (user.getUserDetail().getDateOfBirth() != null) {
				response.setDOB(formatter.format(user.getUserDetail().getDateOfBirth()));
			} else {
				response.setDOB("");
			}
			if (user.getUserDetail().getGender() != null) {
				response.setGender(user.getUserDetail().getGender());
			} else {
				response.setGender(Gender.NA);
			}
			response.setBalance(user.getAccountDetail().getBalance());
			response.setPoints(user.getAccountDetail().getPoints());
			response.setUsername(user.getUserDetail().getFirstName());
			response.setMobile(user.getUsername());
			response.setOsName(user.getOsName());
			response.setGcmId(user.getGcmId());
			response.setAccountTypeStatus(user.getAccountDetail().getAccountType().getCode());
			response.setAccountBalance(Double.parseDouble(String.format("%.2f", user.getAccountDetail().getBalance())));
		} else {
			response.setMsg("Not a Registered User");
		}
		return response;
	}

	@Override
	public ResponseDTO processUpdateAuth(AuthUpdateRequest request) {
		ResponseDTO result = new ResponseDTO();
		User user = findByUserName(request.getUsername());
		if (user != null) {
			AuthUpdateLog log = authUpdateLogRepository.getByUserAndRequest(user,
					RequestType.valueOf(request.getRequestType().toUpperCase()), Status.Processing);
			if (log == null) {
				log = new AuthUpdateLog();
				switch (request.getRequestType().toUpperCase()) {
				case "LOCK":
					log.setRequestType(RequestType.LOCK);
					break;
				case "UNLOCK":
					log.setRequestType(RequestType.UNLOCK);
					break;
				default:
					log.setRequestType(RequestType.LOCK);
				}
				log.setStatus(Status.Processing);
				log.setReason(request.getReason());
				log.setUser(user);
				log = authUpdateLogRepository.save(log);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Log Entered");
				result.setDetails(ConvertUtil.convertToDTO(log));
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Record Already Exists");
				result.setDetails(ConvertUtil.convertToDTO(log));
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("User not exists");
		}
		return result;
	}

	@Override
	public ResponseDTO getUpdateAuthLogById(String id) {
		ResponseDTO result = new ResponseDTO();
		long requestId = Long.parseLong(id);
		AuthUpdateLog log = authUpdateLogRepository.getById(requestId);
		if (log != null) {
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Log Details");
			result.setDetails(ConvertUtil.convertToDTO(log));
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Record not found");
		}
		return result;
	}

	@Override
	public ResponseDTO getAllAuthUpdateLogs() {
		ResponseDTO result = new ResponseDTO();
		List<AuthUpdateLog> authLogs = (List<AuthUpdateLog>) authUpdateLogRepository.findAll();
		List<LockUnlockDTO> logs = ConvertUtil.convertToList(authLogs);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Update Authority Logs");
		result.setDetails(logs);
		return result;
	}

	@Override
	public List<UsernameListDTO> getAllPossibilites(String subMobile) {
		List<User> userList = userRepository.getPossibilitiesByMobile(subMobile + "%");
		return ConvertUtil.convertList(userList);
	}

	@Override
	public void sendSingleSMS(String mobile, String content) {
		User user = findByUserName(mobile);
		if (user != null) {
			smsSenderApi.sendPromotionalSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplate.ADMIN_TEMPLATE, user, content);
		}
	}

	@Override
	public void sendBulkSMS(String content) {
		List<String> mobileList = userRepository.getAllActiveUsername(UserType.User);
		for (String mobile : mobileList) {
			sendSingleSMS(mobile, content);
		}
	}

	@Override
	public void sendSingleMail(String mailId, String content, String subject) {
		List<User> users = userRepository.getByMail(mailId);
		System.err.println(users);
		if (users != null && !users.isEmpty()) {
			for (User user : users) {
				mailSenderApi.sendNoReplyMail(subject, MailTemplate.ADMIN_TEMPLATE, user, content);
			}
		}
	}

	@Override
	public void sendBulkMail(String content, String subject) {
		List<String> mailList = userRepository.getMailsOfActiveUser(UserType.User);
		if (mailList != null && mailList.size() > 0) {
			for (String mail : mailList) {
				sendSingleMail(mail, content, subject);
			}
		}
	}

	/*
	 * @Override public AuthenticationError validateUserRequest(String request,
	 * User user, PQService service, long timeStamp) { String authority =
	 * Authorities.LOCKED + "" + Authorities.AUTHENTICATED; AuthenticationError
	 * error = new AuthenticationError(); try { RequestLog oldLog =
	 * requestLogRepository.getLastRequestOfUser(user); if (oldLog != null) {
	 * System.err.println("m here where old log is not null " +
	 * oldLog.getStatus()); System.err.println("now time " + timeStamp +
	 * " oldlogtime::" + oldLog.getTimeStamp()); long timeDiff = timeStamp -
	 * oldLog.getTimeStamp(); System.err.println("the time diff is:: " +
	 * timeDiff); long validTimeForTransaction = (long) (Math.pow(10, 9)) * 60 *
	 * 1; if (timeDiff > validTimeForTransaction) { System.err.println(
	 * "time diff is more den 1 min"); if
	 * (oldLog.getStatus().equals(Status.Processing)) {
	 * oldLog.setStatus(Status.Deleted); requestLogRepository.save(oldLog); }
	 * RequestLog newLog = new RequestLog(); error.setSuccess(true);
	 * error.setMessage("Valid Request"); newLog.setService(service);
	 * newLog.setTimeStamp(timeStamp); newLog.setRequest(request);
	 * newLog.setStatus(Status.Processing); newLog.setUser(user);
	 * requestLogRepository.save(newLog);
	 * 
	 * } else { System.err.println("m here where time diff is less then 1 min");
	 * if (oldLog.getStatus().equals(Status.Completed)) { RequestLog newLog =
	 * new RequestLog(); error.setSuccess(true); error.setMessage(
	 * "Valid Request"); newLog.setService(service);
	 * newLog.setTimeStamp(timeStamp); newLog.setRequest(request);
	 * newLog.setStatus(Status.Processing); newLog.setUser(user);
	 * requestLogRepository.save(newLog); } else {
	 * oldLog.setStatus(Status.Deleted); requestLogRepository.save(oldLog);
	 * RequestLog newLog = new RequestLog(); error.setSuccess(true);
	 * error.setMessage("Valid Request"); newLog.setService(service);
	 * newLog.setTimeStamp(timeStamp); newLog.setRequest(request);
	 * newLog.setStatus(Status.Processing); newLog.setUser(user);
	 * requestLogRepository.save(newLog);
	 * 
	 * } } } else { RequestLog newLog = new RequestLog();
	 * error.setSuccess(true); error.setMessage("Valid Request");
	 * newLog.setService(service); newLog.setTimeStamp(timeStamp);
	 * newLog.setRequest(request); newLog.setStatus(Status.Processing);
	 * newLog.setUser(user); requestLogRepository.save(newLog);
	 * 
	 * } } catch (Exception e) { e.printStackTrace();
	 * System.err.println("message::" + error); error.setMessage(
	 * "try again later"); }
	 * 
	 * return error; }
	 */

	@Override
	public AuthenticationError validateUserRequest(String request, User user, PQService service, long timeStamp) {
		String authority = Authorities.LOCKED + "," + Authorities.AUTHENTICATED;
		AuthenticationError error = new AuthenticationError();
		try {
			RequestLog newLogs = requestLogRepository.getLastRequestOfUser(user);
			if (newLogs != null) {
				long timeDiff = timeStamp - newLogs.getTimeStamp();
				long validTimeForTransaction = (long) (Math.pow(10, 9)) * 60 * 1;
				if (timeDiff > validTimeForTransaction) {
					if (newLogs.getStatus().equals(Status.Processing)) {
						newLogs.setStatus(Status.Deleted);
						requestLogRepository.save(newLogs);
					}
					RequestLog newLog = new RequestLog();
					error.setSuccess(true);
					error.setMessage("Valid Request");
					newLog.setService(service);
					newLog.setTimeStamp(timeStamp);
					newLog.setRequest(request);
					newLog.setStatus(Status.Processing);
					newLog.setUser(user);
					requestLogRepository.save(newLog);

				} else {
					if (newLogs.getStatus().equals(Status.Completed)) {
						RequestLog newLog = new RequestLog();
						error.setSuccess(true);
						error.setMessage("Valid Request");
						newLog.setService(service);
						newLog.setTimeStamp(timeStamp);
						newLog.setRequest(request);
						newLog.setStatus(Status.Processing);
						newLog.setUser(user);
						requestLogRepository.save(newLog);
					} else {
						error.setSuccess(false);
						user.setAuthority(authority);
						userRepository.save(user);
						error.setMessage("You're Locked,Please contact Customer Care");
					}
				}
			} else {
				RequestLog newLog = new RequestLog();
				error.setSuccess(true);
				error.setMessage("Valid Request");
				newLog.setService(service);
				newLog.setTimeStamp(timeStamp);
				newLog.setRequest(request);
				newLog.setStatus(Status.Processing);
				newLog.setUser(user);
				requestLogRepository.save(newLog);
			}

		} catch (Exception e) {
			e.printStackTrace();
			error.setMessage("Your request is not validated .");
		}
		return error;
	}

	@Override
	public void updateRequestLog(User u, long timeStamp) {
		RequestLog requestLog = requestLogRepository.findByUserRequestTimeStamp(u, timeStamp);
		if (requestLog != null) {
			requestLog.setStatus(Status.Completed);
			requestLogRepository.save(requestLog);
		}
	}

	@Override
	public void visaMerchantSignUp(com.thirdparty.model.request.VisaMerchantRequest dto) throws ClientException {
		try {
			System.err.println("-------------------- INSIDE MERCHANT SIGN UP API ----------------------");
			System.err.println("i am here....");
			User user = new User();
			UserDetail userDetail = new UserDetail();
			System.err.println("this is dto::" + dto);

			System.err.println("this is customerIdddd::::" + dto.getCustomerId());
			userDetail.setAddress(dto.getAddress1());
			userDetail.setContactNo(dto.getMobileNo().substring(3));
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName("");
			userDetail.setEmail(dto.getEmailAddress());

			if (dto.getUserType().equals(UserType.Merchant)) {
				user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			}
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getMobileNo());
			user.setUserType(UserType.Merchant);

			LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getPinCode());
			userDetail.setLocation(location);
			user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getMobileNo().substring(3));
			user.setUserType(UserType.Merchant);
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken("E" + System.currentTimeMillis());

			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(false);
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			MerchantDetails merchantDetails = merchantDetailsRepository.findByPanCardNo(dto.getPanNo());
			if (merchantDetails == null) {
				merchantDetails = new MerchantDetails();
				merchantDetails.setAadharNo(dto.getAadharNo());
				merchantDetails.setPanCardNo(dto.getPanNo());
				System.err.println("account Number" + dto.getBankAccountNo());
				merchantDetails.setBankAccountName(dto.getMerchantAccountName());
				merchantDetails.setBankAccountNo(dto.getMerchantAccountNumber());
				merchantDetails.setBankName(dto.getMerchantBankName());
				merchantDetails.setBranchName(dto.getMerchantBankLocation());
				merchantDetails.setIfscCode(dto.getMerchantBankIfscCode());
				merchantDetails.setEntityId(" ");
				merchantDetails.setmVisaId(" ");
				merchantDetails.setCustomerId(" ");
				merchantDetailsRepository.save(merchantDetails);
			}
			user.setMerchantDetails(merchantDetails);
			System.err.println("updated merchant with entity id:::" + merchantDetails);
			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				System.err.println("this is mobile token::" + user.getMobileToken());
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				User u = userRepository.save(user);
				System.err.println(u);
			} else {
				System.err.println("this is mobile token temp usernt null:::" + user.getMobileToken());
				//
				// userRepository.delete(tempUser);
				// user.setAccountDetail(tempUser.getAccountDetail());
				// userDetailRepository.save(userDetail);
				// userRepository.save(user);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	public void merchantSignUp(VisaMerchantRequest dto) throws ClientException {
		try {
			System.err.println("-------------------- INSIDE MERCHANT SIGN UP API ----------------------");
			User user = new User();
			UserDetail userDetail = new UserDetail();
			// System.err.println("this is dto::"+dto);
			userDetail.setAddress(dto.getAddress1());
			userDetail.setContactNo(dto.getMobileNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName("");
			userDetail.setEmail(dto.getEmailAddress());
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
					user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				}
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUserName().toLowerCase());
				user.setUserType(UserType.Merchant);
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getPinCode());
				userDetail.setLocation(location);
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUserName().toLowerCase());
				user.setUserType(UserType.Merchant);
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			// pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			/*
			 * if(dto.isVbankCustomer()){
			 * pqAccountDetail.setVijayaAccountNumber(dto.getBankAccountNo());
			 * pqAccountDetail.setBranchCode(dto.getIfscCode()); }
			 */ pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			MerchantDetails merchantDetails = new MerchantDetails();
			merchantDetails.setAadharNo(dto.getAadharNo());
			merchantDetails.setPanCardNo(dto.getPanNo());
			merchantDetails.setBankAccountName(dto.getMerchantAccountName());
			merchantDetails.setBankAccountNo(dto.getMerchantAccountNumber());
			merchantDetails.setBankName(dto.getMerchantBankName());
			merchantDetails.setBranchName("");
			merchantDetails.setIfscCode(dto.getMerchantBankIfscCode());
			user.setMerchantDetails(merchantDetails);
			// merchantDetails.setUser(user);

			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				System.err.println("this is mobile token::" + user.getMobileToken());
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				merchantDetailsRepository.save(merchantDetails);
				User u = userRepository.save(user);
				System.err.println(u);
			} else {
				System.err.println("this is mobile token temp usernt null:::" + user.getMobileToken());
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				merchantDetailsRepository.save(merchantDetails);
				userRepository.save(user);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public ResponseDTO kycUpdate(ApproveKycDTO request) {
		ResponseDTO result = new ResponseDTO();
		User user = userRepository.findByUsername(request.getUsername());
		if (user != null) {
			if (!request.isSuccess()) {
				PQAccountType kycAccountType = pqAccountTypeRepository.findByCode("KYC");
				PQAccountDetail account = user.getAccountDetail();
				account.setAccountType(kycAccountType);
				pqAccountDetailRepository.save(account);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("KYC request Approved successfully");
				result.setDetails("KYC request Approved successfully");
			} else {
				PQAccountType kycAccountType = pqAccountTypeRepository.findByCode("NONKYC");
				PQAccountDetail account = user.getAccountDetail();
				account.setAccountType(kycAccountType);
				pqAccountDetailRepository.save(account);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("NON-KYC request Approved successfully");
				result.setDetails("NON-KYC request Approved successfully");
			}
		}
		return result;
	}

	@Override
	public ResponseDTO nonKycUpdate(ApproveKycDTO request) {
		ResponseDTO result = new ResponseDTO();
		User user = userRepository.findByUsername(request.getUsername());
		if (user != null) {
			if (request.isSuccess()) {
				PQAccountType kycAccountType = pqAccountTypeRepository.findByCode("KYC");
				PQAccountDetail account = user.getAccountDetail();
				account.setAccountType(kycAccountType);
				pqAccountDetailRepository.save(account);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("KYC request Approved successfully");
				result.setDetails("KYC request Approved successfully");
			} else {
				PQAccountType kycAccountType = pqAccountTypeRepository.findByCode("NONKYC");
				PQAccountDetail account = user.getAccountDetail();
				account.setAccountType(kycAccountType);
				pqAccountDetailRepository.save(account);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("NON-KYC request Approved successfully");
				result.setDetails("NON-KYC request Approved successfully");
			}
		}
		return result;
	}

	/*
	 * public User findByUserName(String username){ return
	 * userRepository.checkForLogin(username); }
	 */
	@Override
	public void updateMerchantM2P(String customerId, String mVisaId, String rupayId, String username) {
		User u = userRepository.findByUsername(username.substring(3));
		MerchantDetails merchant = u.getMerchantDetails();
		if (merchant != null) {
			merchant.setCustomerId(customerId);
			merchant.setmVisaId(mVisaId);
			merchant.setRupayId(rupayId);
			merchantDetailsRepository.save(merchant);
		}
	}

	@Override
	public ResponseDTO updateAccess(AccessDTO dto) {
		ResponseDTO result = new ResponseDTO();
		User user = findByUserName(dto.getUsername());
		if (user != null) {
			AccessDetails access = accessLogRepository.getByUser(user);
			if (access == null) {
				access = new AccessDetails();
				access.setUser(user);
			}
			access = ConvertUtil.convertFromAccessDTO(access, dto);
			accessLogRepository.save(access);
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Updated Successfully");
			result.setDetails(ConvertUtil.convertFromAccessDetail(access));
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("User Not Found");
		}
		return result;
	}

	@Override
	public void updateQRcode(String username, String qrcode) {
		User u = userRepository.findByUsername(username.substring(3));
		MerchantDetails merchant = u.getMerchantDetails();
		if (merchant != null) {
			merchant.setQrCode(qrcode);
			merchantDetailsRepository.save(merchant);
		}
	}

	@Override
	public boolean checkMerchantMobileTokenNew(String key, String mobileNumber) {
		UserDetail userDetail = userDetailRepository.findByContactNo(mobileNumber);
		if (userDetail == null) {
			return false;
		} else {
			mobileNumber = userDetail.getContactNo();
			User user = userRepository.findByMobileTokenAndStatus(key, mobileNumber, Status.Inactive);
			if (user == null) {
				return false;
			} else {
				user.setMobileStatus(Status.Active);
				user.setMobileToken(null);
				userRepository.save(user);
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_SUCCESS, user, null);
				return true;
			}
		}
	}

	@Override
	public ResponseDTO getCityAndState(AgentRequest dto) throws Exception {

		ResponseDTO resp = new ResponseDTO();
		try {
			/*
			 * System.out.println("Inside Agent get city and state");
			 * System.err.println("pincode :: " + dto.getPinCode());
			 */
			LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getPinCode().trim());
			System.err.println("location ::" + location);
			if (location != null) {
				resp.setDetails(location.getDistrictName());
				resp.setDetails2(location.getStateName());
				resp.setStatus(ResponseStatus.SUCCESS);
				resp.setMessage("Successfully get city and state");
			} else {
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setMessage("Please enter valid pincode number");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
		return resp;
	}

	@Override
	public ResponseDTO getBankList() throws Exception {

		ResponseDTO resp = new ResponseDTO();

		try {
			List<Banks> banklist = banksRepository.getAllList();
			resp.setDetails(banklist);
			resp.setCode(ResponseStatus.SUCCESS.getValue());
			;
			resp.setMessage("Get Bank List");
			resp.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			System.out.println(e);
		}
		return resp;
	}

	@Override
	public void saveAgent(AgentRequest dto) throws Exception {
		// TODO Auto-generated method stub
		try {
			User user = new User();
			UserDetail userDetail = new UserDetail();
			userDetail.setAddress(dto.getAddress1());
			userDetail.setContactNo(dto.getMobileNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName("");
			userDetail.setEmail(dto.getEmailAddress());
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Agent)) {
					user.setAuthority(Authorities.AGENT + "," + Authorities.AUTHENTICATED);
					user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				}
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUserName());
				user.setUserType(UserType.Agent);
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getPinCode());
				userDetail.setLocation(location);
				user.setAuthority(Authorities.AGENT + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUserName());
				user.setUserType(UserType.Agent);
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("SUPERKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			AgentDetail agentDetails = new AgentDetail();
			agentDetails.setAgentName(dto.getAgencyName() !=null ? dto.getAgencyName() : "NA");
			agentDetails.setPanCardNo(dto.getPanNo() != null ? dto.getPanNo() : "NA");
			agentDetails.setBankAccountName(dto.getAgentAccountName() != null ? dto.getAgentAccountName() : "NA");
			agentDetails.setBankAccountNo(dto.getAgentAccountNumber() != null ? dto.getAgentAccountNumber() : "NA");
			agentDetails.setBankName(dto.getAgentBankName() != null ? dto.getAgentBankName() : "NA");
			agentDetails.setBranchName(dto.getAgentBranchName());
			agentDetails.setIfscCode(dto.getAgentBankIfscCode());
			agentDetails.setImage(dto.getContentType());
			byte[] byteArr = Base64.getDecoder().decode(dto.getEncodedBytes());
			agentDetails.setImageContent(byteArr);
			agentDetails.setMobileNumber(dto.getMobileNo());
			user.setAgentDetails(agentDetails);
			// merchantDetails.setUser(user);

			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			AgentDetail aga = agentDetailRepository.findUserbyAccountNumber(agentDetails.getBankAccountNo());
			AgentDetail agp = agentDetailRepository.findUserByPanCardNo(agentDetails.getPanCardNo());

			User auser = userRepository.findUserByAgentIdAndStatus(aga, Status.Inactive);
			User puser = userRepository.findUserByAgentIdAndStatus(agp, Status.Inactive);
			String tempusername = "";
			String ausername = "";
			String pusername = "";

			if (auser != null) {
				if (tempUser != null || puser != null) {
					if (tempUser != null && tempUser.getUsername().equalsIgnoreCase(auser.getUsername())) {
						auser = null;
					} else if (puser != null && puser.getUsername().equalsIgnoreCase(auser.getUsername())) {
						auser = null;
					}
				}
			}
			if (puser != null) {
				if (tempUser != null || auser != null) {
					if (tempUser != null && tempUser.getUsername().equalsIgnoreCase(puser.getUsername())) {
						puser = null;
					} else if (auser != null && auser.getUsername().equalsIgnoreCase(puser.getUsername())) {
						puser = null;
					}
				}
			}

			if (tempUser == null && auser == null && puser == null) {
				System.err.println("this is mobile token::" + user.getMobileToken());
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				agentDetailRepository.save(agentDetails);
				User u = userRepository.save(user);
				System.err.println(u);
			} else {
				System.err.println("this is mobile token temp usernt null:::" + user.getMobileToken());
				if (tempUser != null) {
					userRepository.delete(tempUser);
					userDetailRepository.delete(tempUser.getUserDetail());
					agentDetailRepository.delete(tempUser.getAgentDetails());
					user.setAccountDetail(tempUser.getAccountDetail());
				}
				if (auser != null) {
					userRepository.delete(auser);
					userDetailRepository.delete(auser.getUserDetail());
					agentDetailRepository.delete(auser.getAgentDetails());
					user.setAccountDetail(auser.getAccountDetail());
				}
				if (puser != null) {
					userRepository.delete(puser);
					userDetailRepository.delete(puser.getUserDetail());
					agentDetailRepository.delete(puser.getAgentDetails());
					user.setAccountDetail(puser.getAccountDetail());
				}
				userDetailRepository.save(userDetail);
				agentDetailRepository.save(agentDetails);
				userRepository.save(user);
			}
			mailSenderApi.sendNoReplyMail("Verification Successful", MailTemplate.AGENT_REGISTRATION, user,
					dto.getPassword());
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.AGENT_VERIFICATION_SUCCESS, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}

	}

	@Override
	public List<AgentDTO> getAllAgent(String role) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseDTO saveDonatee(DRegisterDTO dto) throws ClientException {
		ResponseDTO result = new ResponseDTO();
		try {
			String password = dto.getMobileNumber();
			User user = new User();
			String firstName = dto.getFirstName();
			int length = firstName.length();
			int i = 1;
			String serviceCode = (firstName.charAt(0) + "" + firstName.charAt(1) + "" + firstName.charAt(length - 1))
					.toUpperCase();
			PQService exists = pqServiceRepository.findServiceByOperatorCode(serviceCode);
			while ((exists != null) && (i <= length)) {
				serviceCode = (serviceCode + firstName.charAt(i)).toUpperCase();
				exists = pqServiceRepository.findServiceByOperatorCode(serviceCode);
				i++;
			}
			UserDetail userDetail = new UserDetail();
			userDetail.setContactNo(dto.getMobileNumber());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(" ");
			userDetail.setEmail(dto.getEmail());
			userDetail.setImage(dto.getImage());
			user.setAuthority(Authorities.DONATEE + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(password));
			user.setMobileStatus(Status.Active);
			user.setEmailStatus(Status.Active);
			user.setUsername("D" + dto.getMobileNumber());
			user.setUserType(UserType.Donatee);
			user.setUserDetail(userDetail);
			PQAccountType kYCAccountType = pqAccountTypeRepository.findByCode("SUPERKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountType(kYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempDonatee = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Active);
			if (tempDonatee == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
			} else {
				userRepository.delete(tempDonatee);
				userDetailRepository.delete(tempDonatee.getUserDetail());
				user.setAccountDetail(tempDonatee.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
			}
			PQService service = new PQService();
			PQOperator operator = pqOperatorRepository.findOperatorByName("VPayQwik");
			PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName(dto.getServiceName());
			if (serviceType != null) {
				service.setServiceType(serviceType);
			}
			if (operator != null) {
				service.setOperator(operator);
			}
			service.setCode("V" + serviceCode);
			service.setStatus(Status.Active);
			service.setOperatorCode(serviceCode);
			service.setName(firstName + " Payment");
			service.setDescription("Payment to " + firstName);
			service.setMinAmount(1);
			service.setMaxAmount(100000);
			pqServiceRepository.save(service);
			PQCommission commission = new PQCommission();
			commission.setMinAmount(1);
			commission.setMaxAmount(100000);
			commission.setFixed(dto.isFixed());
			commission.setType("POST");
			commission.setValue(dto.getValue());
			commission.setService(service);
			commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
			if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
				commissionApi.save(commission);
			}

			// mailSenderApi.sendNoReplyMail("Verification Successful",
			// MailTemplate.MERCHANT_REGISTRATION, user,password);
			// smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP,
			// SMSTemplate.VERIFICATION_SUCCESS, user,null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
		System.err.println("donatee saved.......");
		result.setMessage("Denotee saved...");
		result.setCode("S00");
		return result;
	}

	@Override
	public List<TListDTO> convertFromRawTransactions(List<PQTransaction> transactionList) {
		List<TListDTO> resultList = new ArrayList<>();
		for (PQTransaction t : transactionList) {
			User u = findByAccountDetail(t.getAccount());
			if (u != null) {
				resultList.add(ConvertUtil.getDTO(u, t));
			}
		}
		return resultList;
	}

	@Override
	public List<TListDTO> convertFromRawTransactionsForBillPay(List<PQTransaction> transactionList) {
		List<TListDTO> resultList = new ArrayList<>();
		for (PQTransaction t : transactionList) {
			User u = findByAccountDetail(t.getAccount());
			if (u != null) {
				resultList.add(ConvertUtil.getDTO(u, t));
			}
		}
		return resultList;
	}

	@Override
	public List<MTransactionResponseDTO> getDTOList(List<PQTransaction> transactionList,
			List<TPTransaction> transactions) {
		List<MTransactionResponseDTO> mTransactionLists = new ArrayList<>();
		for (PQTransaction transaction : transactionList) {
			TPTransaction transactionsList = pqTransactionRepository
					.findTransactionRefNo(transaction.getTransactionRefNo().substring(0, 13));
			User user = findByAccountDetail(transaction.getAccount());
			if (user != null) {
				if (transactionsList != null) {
					mTransactionLists.add(ConvertUtil.convert(user, transactionsList, transaction));
				}
			}
		}
		return mTransactionLists;
	}

	@Override
	public List<MTransactionResponseDTO> getDTOListForTreatCard(List<PQTransaction> transactionList) {
		List<MTransactionResponseDTO> mTransactionLists = new ArrayList<>();
		for (PQTransaction transaction : transactionList) {
			User user = findByAccountDetail(transaction.getAccount());
			if (user != null) {
				mTransactionLists.add(ConvertUtil.convertTreatCardUser(user, transaction));
			}
		}
		return mTransactionLists;
	}

	@Override
	public ResponseDTO getLocationByPin(String pincode) {
		ResponseDTO result = new ResponseDTO();
		LocationDetails details = locationDetailsRepository.findLocationByPin(pincode);
		if (details != null) {
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Location Details");
			result.setDetails(details);
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Location not found");
		}
		return result;
	}

	@Override
	public void saveAgentToUser(RegisterDTO dto, User user1) throws Exception {

		System.err.println("-------------------- INSIDE Agent SIGN UP API ----------------------");
		AUserSignUp ausignup = new AUserSignUp();
		try {
			User user = new User();
			UserDetail userDetail = new UserDetail();
			SecurityAnswerDetails answerDetails = new SecurityAnswerDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setGender(dto.getGender());
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				}
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				userDetail.setLocation(location);
				if (dto.isWeb()) {
					SecurityQuestion question = securityQuestionRepository
							.findSecurityQuestionByCode(dto.getSecQuestionCode());
					answerDetails.setAnswer(dto.getSecAnswer() == null ? "" : dto.getSecAnswer());
					answerDetails.setSecQuestion(question);
					answerDetails.setUser(user);
					securityAnswerDetailsRepository.save(answerDetails);
				}
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
			}
			user.setUserDetail(userDetail);
			ausignup.setAgent_id(user1);
			ausignup.setUser_id(user);

			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			pqAccountDetail.setVijayaAccountNumber(dto.getAccountNumber());
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
				aUserSignUpRepository.save(ausignup);

			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
				aUserSignUpRepository.save(ausignup);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}

	}

	@Override
	public String getcountofagentregister(User user) {
		// TODO Auto-generated method stub
		long d = aUserSignUpRepository.getUserCount(user);
		return String.valueOf(d);
	}

	@Override
	public String getCountOfAgentRegister(UserDTO user) {
		User u = userRepository.findByUser(user.getUsername());
		long d = aUserSignUpRepository.getUserCount(u);
		return String.valueOf(d);
	}

	@Override
	public List<User> getTotalAgents() {
		return userRepository.findAllAgent();
	}

	// bday Message
	@Override
	public void sendBdayMessageAlert() {
		SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
		String day = dayFormat.format(new Date());
		int dd = Integer.parseInt(day);
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		String month = monthFormat.format(new Date());
		int monthData = Integer.parseInt(month);
		List<UserDetail> userDetail = userDetailRepository.findUserBdayDate(monthData, dd);
		if (userDetail != null) {
			for (UserDetail detail : userDetail) {
				List<User> user = userRepository.findUserByUserDetails(detail);
				if (user != null) {
					for (User userList : user) {
						smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplate.BDAY_ALERT, userList,
								null);
					}
				}
			}
		}

	}

	@Override
	public UserBalanceDTO getUserTransaction(Date startDate, Date endDate, PQAccountDetail account) {
		UserBalanceDTO dto = new UserBalanceDTO();
		// double totalLoadMoney = 0;
		// double totaldebitsendMoney = 0;
		// double totaldebittsendMoney = 0;
		// double totalCreditsendMoney = 0;
		// double totalBankTransfer = 0;
		// double totalMvisaDebit = 0;
		// double totalgciDebit = 0;
		// double totalAdlabsDebit = 0;
		// double totalMeraEventsDebit = 0;
		// double totalsnowcityDebit = 0;
		// double totalBillpaymentDebit = 0;
		// List<PQTransaction> transaction =
		// pqTransactionRepository.getUserTransaction(startDate, endDate,
		// account);
		// if (transaction != null) {
		// for (int i = 0; i < transaction.size(); i++) {
		// PQTransaction openingbalance = transaction.get(i);
		// if (i == 0) {
		// dto.setOpeningBalance(openingbalance.getCurrentBalance());
		// }
		// if (i == transaction.size() - 1) {
		// dto.setClosingBalance(openingbalance.getCurrentBalance());
		// }
		// }
		// for (PQTransaction t : transaction) {
		// PQService loadMoneyService =
		// pqServiceRepository.findServiceByCode("LMC");
		// if (t.getService().equals(loadMoneyService)) {
		// totalLoadMoney =
		// pqTransactionRepository.getLoadMoneyCredit(startDate, endDate,
		// t.getAccount(),
		// loadMoneyService);
		// dto.setLoadMoneyCredit(totalLoadMoney);
		// }
		// // PQService sendMoneyDebitService =
		// // pqServiceRepository.findServiceByCode("SMU");
		// // if (t.getService().equals(sendMoneyDebitService)) {
		// // totaldebitsendMoney =
		// // pqTransactionRepository.getSendMoneydebit(startDate, endDate,
		// // t.getAccount(),
		// // sendMoneyDebitService);
		// // dto.setSendMoneyDebit(totaldebitsendMoney);
		// // }
		// // PQService sendMoneyDebittService =
		// // pqServiceRepository.findServiceByCode("SMR");
		// // if (t.getService().equals(sendMoneyDebittService)) {
		// // totaldebittsendMoney =
		// // pqTransactionRepository.getSendMoneyCredit(startDate,
		// // endDate, t.getAccount(),
		// // sendMoneyDebittService);
		// // dto.setSendMoneyDebit(totaldebittsendMoney);
		// // }
		// // PQService sendMoneyCreditService =
		// // pqServiceRepository.findServiceByCode("SMR");
		// // if (t.getService().equals(sendMoneyCreditService)) {
		// // totalCreditsendMoney =
		// // pqTransactionRepository.getSendMoneyCredit(startDate,
		// // endDate, t.getAccount(),
		// // sendMoneyCreditService);
		// // dto.setSendMoneyDebit(totalCreditsendMoney);
		// // }
		// // PQService sendMoneyDebittService =
		// // pqServiceRepository.findServiceByCode("SMR");
		// // if(t.getService().equals(sendMoneyDebittService)){
		// // totaldebitsendMoney =
		// // pqTransactionRepository.getSendMoneydebit(startDate,
		// // endDate,t.getAccount(), sendMoneyDebittService);
		// // dto.setSendMoneyDebit(totaldebitsendMoney);
		// // }
		// PQService sendMoneyBankService =
		// pqServiceRepository.findServiceByCode("SMB");
		// if (t.getService().equals(sendMoneyBankService)) {
		// totalBankTransfer =
		// pqTransactionRepository.getBankTransferdebit(startDate, endDate,
		// t.getAccount(),
		// sendMoneyBankService);
		// dto.setBankTransferDebit(totalBankTransfer);
		// }
		// PQService mvisaService =
		// pqServiceRepository.findServiceByCode("MVISA");
		// if (t.getService().equals(mvisaService)) {
		// totalMvisaDebit = pqTransactionRepository.getServiceDebit(startDate,
		// endDate, t.getAccount(),
		// mvisaService);
		// dto.setMvisaDebit(totalMvisaDebit);
		// }
		// PQService gciService =
		// pqServiceRepository.findServiceByCode("GCART");
		// if (t.getService().equals(gciService)) {
		// totalgciDebit = pqTransactionRepository.getServiceDebit(startDate,
		// endDate, t.getAccount(),
		// gciService);
		// dto.setGciDebit(totalgciDebit);
		// }
		// PQService snowcityService =
		// pqServiceRepository.findServiceByCode("VSNY");
		// if (t.getService().equals(snowcityService)) {
		// totalsnowcityDebit =
		// pqTransactionRepository.getServiceDebit(startDate, endDate,
		// t.getAccount(),
		// snowcityService);
		// dto.setSnowcityDebit(totalsnowcityDebit);
		// }
		// PQService meraeventsService =
		// pqServiceRepository.findServiceByCode("EVNT");
		// if (t.getService().equals(meraeventsService)) {
		// totalMeraEventsDebit =
		// pqTransactionRepository.getServiceDebit(startDate, endDate,
		// t.getAccount(),
		// meraeventsService);
		// dto.setMeraEventsDebit(totalMeraEventsDebit);
		// }
		//
		// PQService adlabsService =
		// pqServiceRepository.findServiceByCode("ADLABS");
		// if (t.getService().equals(adlabsService)) {
		// totalAdlabsDebit =
		// pqTransactionRepository.getBankTransferdebit(startDate, endDate,
		// t.getAccount(),
		// adlabsService);
		// dto.setAdlabsDebit(totalAdlabsDebit);
		// }
		// PQServiceType serviceType =
		// pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
		// // List<PQService> topupAndBillpayService =
		// // pqServiceRepository.findByServiceType(serviceType);
		// // for (PQService services : topupAndBillpayService) {
		// if (t.getService().getServiceType().equals(serviceType)) {
		// totalBillpaymentDebit =
		// pqTransactionRepository.getTopupAndBillPayment(startDate, endDate,
		// account);
		// dto.setTopUpAndBillPayDebit(totalBillpaymentDebit);
		// }
		// // }
		//
		// dto.setSendMoneyCredit(totaldebitsendMoney);
		// dto.setSendMoneyCredit(totalCreditsendMoney);
		// dto.setBankTransferDebit(totalBankTransfer);
		// Double totalCredit =
		// pqTransactionRepository.getTotalcredit(startDate, endDate, account);
		// if (totalCredit != null) {
		// dto.setCredit(totalCredit);
		// }
		// dto.setCredit(0);
		// Double totalDebit = pqTransactionRepository.getTotalDebit(startDate,
		// endDate, account);
		// if (totalDebit != null) {
		// dto.setDebit(totalDebit);
		// }
		// dto.setDebit(0);
		// dto.setTopUpAndBillPayDebit(totalBillpaymentDebit);
		// }
		// } else {
		// dto.setStatus(ResponseStatus.FAILURE);
		// dto.setMessage("No Transactions are Available.");
		// dto.setDetails("No Transactions are Available.");
		// }
		return dto;
	}

	@Override
	public void saveRegisterUser(RegisterDTO dto) throws ClientException {
		try {
			User user = new User();
			UserDetail userDetail = new UserDetail();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setGender(dto.getGender());
			userDetail.setMpin(SecurityUtil.sha512(dto.getMpin()));
			LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			userDetail.setLocation(location);
			user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
			user.setPassword(passwordEncoder.encode(dto.getPassword()));
			user.setMobileStatus(Status.Inactive);
			user.setEmailStatus(Status.Active);
			user.setUsername(dto.getUsername().toLowerCase());
			user.setUserType(dto.getUserType());
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			user.setEmailToken(null);
			user.setOtpGenerationTime(new Date());
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			pqAccountDetail.setVijayaAccountNumber(dto.getAccountNumber());
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}

	@Override
	public UserBalanceDTO getUserFilteredTransaction(PQAccountDetail account) {

		UserBalanceDTO dto = new UserBalanceDTO();
		Double totalCredit = pqTransactionRepository.getFilteredUserCreditTransaction(account);
		if (totalCredit != null) {
			dto.setCredit(totalCredit);
		}
		Double totalDebit = pqTransactionRepository.getFilteredUserDebitTransaction(account);
		if (totalDebit != null) {
			dto.setDebit(totalDebit);
		}

		return dto;
	}

	@Override
	public UserBalanceDTO getUserFilteredTransactionByDate(Date StartDate, Date endDate, PQAccountDetail account) {
		UserBalanceDTO dto = new UserBalanceDTO();
		Double totalCredit = pqTransactionRepository.getTotalcredit(StartDate, endDate, account);
		if (totalCredit != null) {
			dto.setCredit(totalCredit);
		}
		Double totalDebit = pqTransactionRepository.getTotalDebit(StartDate, endDate, account);
		if (totalDebit != null) {
			dto.setDebit(totalDebit);
		}

		return dto;
	}

	@Override
	public void saveVpqMerchant(VPQMerchantDTO dto) {

		System.err.println("-------------------- INSIDE MERCHANT SIGN UP API ----------------------");
		User user = new User();
		UserDetail userDetail = new UserDetail();
		// System.err.println("this is dto::"+dto);
		userDetail.setAddress(dto.getAddress());
		userDetail.setContactNo(dto.getContactNo());
		userDetail.setFirstName(dto.getMerchantName());
		userDetail.setLastName("");
		userDetail.setMiddleName("");
		userDetail.setEmail(dto.getEmail());
		if (dto.getUserType().equals(UserType.Merchant)) {
			user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
		}
		user.setPassword(passwordEncoder.encode(dto.getContactNo()));
		user.setMobileStatus(Status.Inactive);
		user.setEmailStatus(Status.Active);
		user.setUsername(dto.getEmail().toLowerCase());
		user.setUserType(UserType.Merchant);

		LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getPinCode());
		userDetail.setLocation(location);

		user.setUserDetail(userDetail);
		PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
		PQAccountDetail pqAccountDetail = new PQAccountDetail();
		pqAccountDetail.setBalance(0);
		// pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
		/*
		 * if(dto.isVbankCustomer()){
		 * pqAccountDetail.setVijayaAccountNumber(dto.getBankAccountNo());
		 * pqAccountDetail.setBranchCode(dto.getIfscCode()); }
		 */ pqAccountDetail.setAccountType(nonKYCAccountType);
		user.setAccountDetail(pqAccountDetail);

		VPQMerchant merchantDetails = new VPQMerchant();
		merchantDetails.setAdharNo(dto.getAdharNo());
		merchantDetails.setPanCardNo(dto.getPanCardNo());
		merchantDetails.setBankAccountNumber(dto.getBankAccountNumber());
		BankDetails detailBank = bankDetailRepository.findByBankIfscCode(dto.getIfscCode());
		if (detailBank != null) {
			merchantDetails.setBankDetails(detailBank);
		}
		int i = 1;
		String serviceCode = (dto.getMerchantName().charAt(0) + "" + dto.getMerchantName().charAt(1) + ""
				+ dto.getMerchantName().charAt(dto.getMerchantName().length() - 1)).toUpperCase();
		PQService exists = pqServiceRepository.findServiceByOperatorCode(serviceCode);
		while ((exists != null) && (i <= dto.getMerchantName().length())) {
			serviceCode = (serviceCode + dto.getMerchantName().charAt(i)).toUpperCase();
			exists = pqServiceRepository.findServiceByOperatorCode(serviceCode);
			i++;
		}

		PQService service = new PQService();
		PQOperator operator = pqOperatorRepository.findOperatorByName("VPayQwik");
		PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName(dto.getServiceName());
		if (serviceType != null) {
			service.setServiceType(serviceType);
		}
		if (operator != null) {
			service.setOperator(operator);
		}
		service.setCode("V" + serviceCode);
		service.setStatus(Status.Active);
		service.setOperatorCode(serviceCode);
		service.setName(dto.getMerchantName() + " Payment");
		service.setDescription("Payment to " + dto.getMerchantName());
		service.setMinAmount(1);
		service.setMaxAmount(100000);
		pqServiceRepository.save(service);
		PQCommission commission = new PQCommission();
		commission.setMinAmount(1.0);
		commission.setMaxAmount(1000000.0);
		commission.setFixed(true);
		commission.setType("POST");
		commission.setValue(0);
		commission.setService(service);
		commission.setIdentifier(commissionApi.createCommissionIdentifier(commission));
		if (commissionApi.findCommissionByIdentifier(commission.getIdentifier()) == null) {
			commissionApi.save(commission);
		}
		PGDetails pgDetails = new PGDetails();
		pgDetails.setService(service);
		pgDetails.setSuccessURL(dto.getSuccessURL());
		pgDetails.setReturnURL(dto.getReturnURL());
		pgDetails.setIpAddress(dto.getIpAddress());
		pgDetails.setPaymentGateway(dto.isPaymentGateway());
		pgDetails.setStore(dto.isPaymentGateway());
		try {
			pgDetails.setToken(SecurityUtil.md5(dto.getIpAddress() + "|" + dto.getContactNo()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
		if (tempUser == null) {
			System.err.println("this is mobile token::" + user.getMobileToken());
			userDetailRepository.save(userDetail);
			pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
			pqAccountDetailRepository.save(pqAccountDetail);

			User u = userRepository.save(user);
			User merchant = userRepository.findByUsername(dto.getEmail().toLowerCase());
			pgDetails.setUser(merchant);
			pgDetailsRepository.save(pgDetails);
			vpqMerchantRepository.save(merchantDetails);
			System.err.println(u);
		} else {
			System.err.println("this is mobile token temp usernt null:::" + user.getMobileToken());
			userRepository.delete(tempUser);
			userDetailRepository.delete(tempUser.getUserDetail());
			user.setAccountDetail(tempUser.getAccountDetail());
			userDetailRepository.save(userDetail);
			userRepository.save(user);
			User merchant = userRepository.findByUsername(dto.getEmail().toLowerCase());
			pgDetails.setUser(merchant);
			pgDetailsRepository.save(pgDetails);
			try {
				merchantDetails.setQrCode(SecurityUtil.md5(dto.getBankAccountNumber() + merchant.getId())
						+ dto.getMerchantName() + SecurityUtil.md5(dto.getContactNo()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			vpqMerchantRepository.save(merchantDetails);
		}
		// mailSenderApi.sendNoReplyMail("Email Verification",
		// MailTemplate.VERIFICATION_EMAIL, user,null);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public List<MTransactionResponseDTO> getDTOListMerchant(List<TPTransaction> transactions) {
		List<MTransactionResponseDTO> mTransactionLists = new ArrayList<>();
		for (TPTransaction tpTransaction : transactions) {
			MTransactionResponseDTO resp = new MTransactionResponseDTO();
			resp.setTransactionRefNo(tpTransaction.getTransactionRefNo());
			resp.setContactNo(tpTransaction.getUser().getUsername());
			resp.setEmail(tpTransaction.getUser().getUserDetail().getEmail());
			resp.setDate(tpTransaction.getCreated());
			resp.setOrderId(tpTransaction.getOrderId());
			resp.setAmount(tpTransaction.getAmount());
			resp.setStatus(tpTransaction.getStatus());
			mTransactionLists.add(resp);
		}
		return mTransactionLists;
	}

	@Override
	public List<MTransactionResponseDTO> getDTOList(List<PQTransaction> transactions) {
		List<MTransactionResponseDTO> mTransactionLists = new ArrayList<>();
		for (PQTransaction pqTransaction : transactions) {
			MTransactionResponseDTO resp = new MTransactionResponseDTO();
			String trans = pqTransaction.getTransactionRefNo();
			String transactionId = trans.substring(0, trans.length() - 1);
			User user = userRepository.getByAccount(pqTransaction.getAccount());
			if (user.getAuthority().contains(Authorities.MERCHANT)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				PQTransaction tr1 = pqTransactionRepository.findByTransactionRefNo(transactionId + "C");
				User us = userRepository.getByAccount(tr1.getAccount());
				resp.setContactNo(us.getUserDetail().getContactNo());
				resp.setEmail(us.getUserDetail().getEmail());
				resp.setCurrentBalance(pqTransaction.getCurrentBalance());
				resp.setAmount(pqTransaction.getAmount());
				resp.setDate(pqTransaction.getCreated());
				resp.setStatus(pqTransaction.getStatus());
				resp.setTransactionRefNo(pqTransaction.getTransactionRefNo());
				resp.setDescription(pqTransaction.getDescription());
				resp.setDebit(pqTransaction.isDebit());
				mTransactionLists.add(resp);
			} else {
				resp.setCurrentBalance(pqTransaction.getCurrentBalance());
				resp.setAmount(pqTransaction.getAmount());
				resp.setDate(pqTransaction.getCreated());
				resp.setStatus(pqTransaction.getStatus());
				resp.setTransactionRefNo(pqTransaction.getTransactionRefNo());
				resp.setDescription(pqTransaction.getDescription());
				resp.setDebit(pqTransaction.isDebit());
				resp.setContactNo(user.getUserDetail().getContactNo());
				resp.setEmail(user.getUserDetail().getEmail());
				mTransactionLists.add(resp);
			}
		}
		return mTransactionLists;
	}

	@Override
	public List<MTransactionResponseDTO> getDebitDTOList(List<PQTransaction> transactions) {
		List<MTransactionResponseDTO> mTransactionLists = new ArrayList<>();
		for (PQTransaction pqTransaction : transactions) {
			MTransactionResponseDTO resp = new MTransactionResponseDTO();
			User us = userRepository.getByAccount(pqTransaction.getAccount());
			resp.setContactNo(us.getUserDetail().getContactNo());
			resp.setEmail(us.getUserDetail().getEmail());
			resp.setCurrentBalance(pqTransaction.getCurrentBalance());
			resp.setAmount(pqTransaction.getAmount());
			resp.setDate(pqTransaction.getCreated());
			resp.setStatus(pqTransaction.getStatus());
			resp.setTransactionRefNo(pqTransaction.getTransactionRefNo());
			resp.setDescription(pqTransaction.getDescription());
			resp.setDebit(pqTransaction.isDebit());
			mTransactionLists.add(resp);
		}
		return mTransactionLists;
	}

	@Override
	public void senSMSforReversedTransactions() {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		String date = formatter.format(calendar.getTime());

		Date myDate = null;
		Date parsedDayBefore = null;
		try {
			myDate = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Date oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));
		String dayBefore = formatter.format(oneDayBefore);

		try {
			parsedDayBefore = formatter.parse(dayBefore);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<PQTransaction> listTransaction = pqTransactionRepository.getInitiatedTransaction(parsedDayBefore, myDate);
		for (PQTransaction pqTransaction : listTransaction) {
			User user = userRepository.findByAccountDetails(pqTransaction.getAccount());
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplate.TRANSACTIONAL_SMS_PROMOTION, user,
					pqTransaction.getDescription() + " " + "on" + " " + formatter.format(pqTransaction.getCreated()));
		}
	}

	@Override
	public List<VersionListDTO> getAllVersion() {
		List<VersionLogs> services = (List<VersionLogs>) versionLogsRepository.findAll();
		return ConvertUtil.getListForVersions(services);
	}

	@Override
	public ResponseDTO updateVersionDetails(UpdateServiceDTO dto) {
		ResponseDTO result = new ResponseDTO();
		VersionLogs versions = versionLogsRepository.getVersionByName(dto.getVersion());
		if (versions != null) {
			Map<String, Object> responseMap = new HashMap<>();
			versions.setStatus(dto.getStatus());
			versions = versionLogsRepository.save(versions);
			responseMap.put("service", ConvertUtil.convertListFromVersionList(versions));
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Information Updated");
			result.setDetails(responseMap);
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Service Not available");
		}
		return result;
	}

	@Override
	public List<UserBalanceDTO> getExpenseOfUser(Date startDate, Date endDate, PQAccountDetail account) {
		List<UserBalanceDTO> analyticsDTOs = new ArrayList<>();
		UserBalanceDTO dto = new UserBalanceDTO();
		PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
		List<PQService> service = pqServiceRepository.findByServiceType(serviceType);
		List<Object[]> transactionObjects = pqTransactionRepository.getAllTransactionsByServiceOfUser(startDate,
				endDate, account, service);
		if (transactionObjects.size() != 0) {
			for (Object[] objects : transactionObjects) {
				UserBalanceDTO dtos = new UserBalanceDTO();
				long count = (long) objects[0];
				double amt = (double) objects[1];
				PQService ser = (PQService) objects[2];
				dtos.setAmount(amt);
				dtos.setCount(count);
				dtos.setServiceName(ser.getName());
				dtos.setServiceCode(ser.getCode());
				dtos.setStatus(ResponseStatus.SUCCESS);
				dtos.setMessage("Receipts List.");
				dtos.setDetails("Receipts List.");
				analyticsDTOs.add(dtos);
			}
		} else {
			dto.setStatus(ResponseStatus.FAILURE);
			dto.setMessage("No Transaction Found.");
			dto.setDetails("No Transaction Found.");
			analyticsDTOs.add(dto);
		}

		return analyticsDTOs;
	}

	@Override
	public List<UserBalanceDTO> getExpenseOfTopUpUser(Date startDate, Date endDate, PQAccountDetail account) {
		List<UserBalanceDTO> analyticsDTOs = new ArrayList<>();
		UserBalanceDTO dto = new UserBalanceDTO();
		PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
		List<PQService> service = pqServiceRepository.findByServiceType(serviceType);
		List<Object[]> transactionObjects = pqTransactionRepository.getAllTopUpByServiceOfUser(startDate, endDate,
				account, service);
		if (transactionObjects.size() != 0) {
			for (Object[] objects : transactionObjects) {
				UserBalanceDTO dtos = new UserBalanceDTO();
				if (objects[1] != null) {
					long count = (long) objects[0];
					double amt = (double) objects[1];
					PQService ser = (PQService) objects[2];
					dtos.setAmount(amt);
					dtos.setCount(count);
					dtos.setServiceName(ser.getServiceType().getName());
					dtos.setStatus(ResponseStatus.SUCCESS);
					dtos.setMessage("Receipts List.");
					dtos.setDetails("Receipts List.");
					analyticsDTOs.add(dtos);
				} else {
					dto.setStatus(ResponseStatus.FAILURE);
					dto.setMessage("No Transactions Found.");
					dto.setDetails("No Transactions Found.");
					analyticsDTOs.add(dto);
				}
			}
		} else {
			dto.setStatus(ResponseStatus.FAILURE);
			dto.setMessage("No Transactions Found.");
			dto.setDetails("No Transactions Found.");
			analyticsDTOs.add(dto);
		}
		return analyticsDTOs;
	}

	@Override
	public List<UserBalanceDTO> getExpenseOfUserForCredit(Date startDate, Date endDate, PQAccountDetail account) {
		List<UserBalanceDTO> analyticsDTOs = new ArrayList<>();
		UserBalanceDTO dto = new UserBalanceDTO();
		List<Object[]> creditTransaction = pqTransactionRepository.getCreditTransactionsOfUser(startDate, endDate,
				account);
		if (creditTransaction.size() != 0) {
			for (Object[] object : creditTransaction) {
				UserBalanceDTO DTOs = new UserBalanceDTO();
				long count = (long) object[0];
				double amt = (double) object[1];
				PQService ser = (PQService) object[2];
				DTOs.setAmount(amt);
				DTOs.setCount(count);
				DTOs.setServiceName(ser.getName());
				DTOs.setServiceCode(ser.getCode());
				DTOs.setStatus(ResponseStatus.SUCCESS);
				DTOs.setMessage("Receipts List.");
				DTOs.setDetails("Receipts List.");
				analyticsDTOs.add(DTOs);
			}
		} else {
			dto.setStatus(ResponseStatus.FAILURE);
			dto.setMessage("No Transactions Found.");
			dto.setDetails("No Transactions Found.");
			analyticsDTOs.add(dto);
		}
		return analyticsDTOs;
	}

	public List<UserBalanceDTO> getBalanceOfUser(Date startDate, Date endDate, PQAccountDetail account) {
		UserBalanceDTO dto = new UserBalanceDTO();
		List<UserBalanceDTO> analyticsDTOs = new ArrayList<>();
		List<PQTransaction> transaction = pqTransactionRepository.getUserTransaction(startDate, endDate, account);
		if (transaction.size() != 0) {
			for (int i = 0; i < transaction.size(); i++) {
				PQTransaction openingbalance = transaction.get(i);
				if (i == 0) {
					dto.setOpeningBalance(openingbalance.getCurrentBalance());
					dto.setCode("S00");
					dto.setMessage("Receipts List.");
					dto.setDetails("Receipts List.");
					analyticsDTOs.add(dto);
				}
				if (i == transaction.size() - 1) {
					dto.setClosingBalance(openingbalance.getCurrentBalance());
					dto.setStatus(ResponseStatus.SUCCESS);
					dto.setMessage("Receipts List.");
					dto.setDetails("Receipts List.");
					analyticsDTOs.add(dto);
				}

			}
		} else {
			dto.setStatus(ResponseStatus.FAILURE);
			dto.setMessage("No Transactions Found.");
			dto.setDetails("No Transactions Found.");
			analyticsDTOs.add(dto);
		}
		return analyticsDTOs;
	}

	@Override
	public User saveUserForReferNEarn(RegisterDTO dto) throws ClientException {
		User userApp = null;
		try {

			User user = new User();
			PasswordHistory history = new PasswordHistory();
			UserDetail userDetail = new UserDetail();
			SecurityAnswerDetails answerDetails = new SecurityAnswerDetails();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			// userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setGender(dto.getGender());
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());
			userDetail.setFingerPrint(dto.getFingerPrint());

			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				}
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				user.setPassword(hashedPassword);
				history.setPassword(hashedPassword);
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				userDetail.setLocation(location);
				if (dto.isWeb()) {
					SecurityQuestion question = securityQuestionRepository
							.findSecurityQuestionByCode(dto.getSecQuestionCode());
					answerDetails.setAnswer(dto.getSecAnswer() == null ? "" : dto.getSecAnswer());
					answerDetails.setSecQuestion(question);
					answerDetails.setUser(user);
					securityAnswerDetailsRepository.save(answerDetails);
				}
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				history.setPassword(hashedPassword);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			pqAccountDetail.setVijayaAccountNumber(dto.getAccountNumber());
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userApp = userRepository.save(user);
				history.setUser(user);
				passwordHistoryRepository.save(history);

			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userApp = userRepository.save(user);
				history.setUser(user);
				passwordHistoryRepository.save(history);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendReferNearnMsg(SMSAccount.PAYQWIK_PROMOTIONAL_MESSAGE, SMSTemplate.REFER_EARN_VM, user,
					dto.getReferee());
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
		return userApp;
	}

	@Override
	public List<UserByLocationDTO> getAllUserByLocationCode(Date StartDate, Date endDate, String pinCode) {
		LocationDetails locationCode = locationDetailsRepository.findLocationByPin(pinCode);
		List<User> userListByLocationCode = userRepository.findUserByLocationCode(locationCode, StartDate, endDate);
		return ConvertUtil.getUserListByLocationCode(userListByLocationCode);
	}

	@Override
	public void sendBillPaymentAlert() {
		int page = 0;
		int size = 1000;
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(page, size, sort);
		Page<User> user = userRepository.findAuthenticateUser(pageable);
		while (page <= user.getTotalPages()) {
			for (User u : user) {
				// User u=userRepository.findByUsername("9760210393");
				PQAccountDetail account = u.getAccountDetail();
				List<PQTransaction> transaction = pqTransactionRepository.findAllBillPaymentTransaction(account);
				if (transaction.size() != 0) {
					for (PQTransaction successTransaction : transaction) {
						String transactionRefNo = successTransaction.getTransactionRefNo().substring(0,
								successTransaction.getTransactionRefNo().length() - 1);
						DTHRequestLog requestLog = dthRequestLogRepository.findByTransactionRefNo(transactionRefNo);
						if (requestLog != null) {
							if (requestLog.getTransactionRefNo().equalsIgnoreCase(transactionRefNo)) {
								PQTransaction t = pqTransactionRepository
										.findByTransactionRefNo(transactionRefNo + "D");
								if (t != null) {
									smsSenderApi.sendUserBillpaySMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
											SMSTemplate.REMAINDER_SMS, u, requestLog.getDthNo(),
											t.getService().getDescription());
								}
							}
						}
						// for postpaid
						if (successTransaction.getDescription().contains("Postpaid Topup to")) {
							MTRequestLog topupRequestLog = mtRequestLogRepository
									.findByTransactionRefNo(transactionRefNo);
							if (topupRequestLog != null) {
								if (topupRequestLog.getTransactionRefNo().equalsIgnoreCase(transactionRefNo)) {
									PQTransaction t = pqTransactionRepository
											.findByTransactionRefNo(transactionRefNo + "D");
									if (t != null) {
										smsSenderApi.sendUserBillpaySMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
												SMSTemplate.REMAINDER_SMS, u, topupRequestLog.getMobileNo(),
												t.getService().getDescription());
									}
								}
							}
						}

						// for electricity
						ECityRequestLog electricityRequestLog = eCityRequestLogRepository
								.findByTransactionRefNo(transactionRefNo);
						if (electricityRequestLog != null) {
							if (electricityRequestLog.getTransactionRefNo().equalsIgnoreCase(transactionRefNo)) {
								PQTransaction t = pqTransactionRepository
										.findByTransactionRefNo(transactionRefNo + "D");
								if (t != null) {
									smsSenderApi.sendUserBillpaySMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
											SMSTemplate.REMAINDER_SMS, u, electricityRequestLog.getAccountNumber(),
											t.getService().getDescription());
								}
							}
						}

						// for landline
						LandlineRequestLog landlineLog = landlineRequestLogRepository
								.findByTransactionRefNo(transactionRefNo);
						if (landlineLog != null) {
							if (landlineLog.getTransactionRefNo().equalsIgnoreCase(transactionRefNo)) {
								PQTransaction t = pqTransactionRepository
										.findByTransactionRefNo(transactionRefNo + "D");
								if (t != null) {
									smsSenderApi.sendUserBillpaySMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
											SMSTemplate.REMAINDER_SMS, u, landlineLog.getLandlineNumber(),
											t.getService().getDescription());
								}
							}
						}
						// for gas
						GasRequestLog gasLog = gasRequestLogRepository.findByTransactionRefNo(transactionRefNo);
						if (gasLog != null) {
							if (gasLog.getTransactionRefNo().equalsIgnoreCase(transactionRefNo)) {
								PQTransaction t = pqTransactionRepository
										.findByTransactionRefNo(transactionRefNo + "D");
								if (t != null) {
									smsSenderApi.sendUserBillpaySMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
											SMSTemplate.REMAINDER_SMS, u, gasLog.getAccount(),
											t.getService().getDescription());
								}
							}
						}
						// for insurance
						InsuranceRequestLog insuranceLog = insuranceRequestLogRepository
								.findByTransactionRefNo(transactionRefNo);
						if (insuranceLog != null) {
							if (insuranceLog.getTransactionRefNo().equalsIgnoreCase(transactionRefNo)) {
								PQTransaction t = pqTransactionRepository
										.findByTransactionRefNo(transactionRefNo + "D");
								if (t != null) {
									smsSenderApi.sendUserBillpaySMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
											SMSTemplate.REMAINDER_SMS, u, insuranceLog.getPolicyNumber(),
											t.getService().getDescription());
								}
							}
						}
					}
				}
			}
			page += 1;
			sort = new Sort(Sort.Direction.DESC, "id");
			pageable = new PageRequest(page, size, sort);
			user = userRepository.findAuthenticateUser(pageable);
		}
	}

	@Override
	public void saveMerchantDetails(NearByMerchantsDTO dto, long id) {
		UserDetail userDetail = userDetailRepository.findUserById(id);
		if (userDetail != null) {
			userDetail.setLatitude(String.valueOf(dto.getLatitude()));
			userDetail.setLongitude(String.valueOf(dto.getLongitude()));
			userDetailRepository.save(userDetail);
		}
	}

	@Override
	public boolean saveUserDetails(NearByMerchantsDTO dto, long id) {
		boolean valid = false;
		UserDetail userDetail = userDetailRepository.findUserById(id);
		if (userDetail != null) {
			userDetail.setLatitude(String.valueOf(dto.getLatitude()));
			userDetail.setLongitude(String.valueOf(dto.getLongitude()));
			userDetailRepository.save(userDetail);
			valid = true;
		}
		return valid;
	}

	@Override
	public List<NearMerchantDetailsDTO> calculateDistance(NearByMerchantsDTO dto) {
		List<NearMerchantDetailsDTO> merchantsList = new ArrayList<>();
		List<User> merchantList = userRepository.findAllMerchantList();
		logger.info("Merchant Size  -----------------------   : ", merchantList.size());
		if ((merchantList != null) && (!merchantList.isEmpty())) {
			for (User u : merchantList) {
				logger.info("Merchant Name : {} Merchant Id : {} ", new Object[] { u.getUsername(), u.getId() });
				UserDetail merchantdetail = u.getUserDetail();
				if (merchantdetail.getLatitude() != null && merchantdetail.getLongitude() != null) {
					String validDistance = isInValidDistance(dto, merchantdetail, dto.getRadius());
					if (validDistance != null && !validDistance.isEmpty()) {
						NearMerchantDetailsDTO merchantDTO = new NearMerchantDetailsDTO();
						merchantDTO.setAddress(merchantdetail.getAddress());
						merchantDTO.setMerchantName(merchantdetail.getFirstName() + " " + merchantdetail.getLastName());
						merchantDTO.setBetweenDistance(Double.parseDouble(validDistance));
						merchantDTO.setLatitude(Double.parseDouble(merchantdetail.getLatitude()));
						merchantDTO.setLongitude(Double.parseDouble(merchantdetail.getLongitude()));
						merchantsList.add(merchantDTO);
					}
				} else {
					logger.info("----------------------------NO Mechant Found--------------------------------- ");
				}
			}
		}
		return merchantsList;
	}

	public String isInValidDistance(NearByMerchantsDTO user, UserDetail merchant, double distance) {
		String valid = null;
		double distanceBW = CommonUtil.calculateDistance(user.getLatitude(), user.getLongitude(),
				Double.parseDouble(merchant.getLatitude()), Double.parseDouble(merchant.getLongitude()));
		logger.info("Calculated distance is : {} and input distance : {}", new Object[] { distanceBW, distance });
		if (distanceBW <= distance) {
			valid = String.valueOf(distanceBW);
		}
		return valid;
	}

	@Override
	public void createCsvForMonthlyTransaction() throws Exception {
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";
		String FILE_HEADER = "mobileNumber,firstName,openingbalance,closingbalance,debit,credit";
		String date = "2017-01-01";
		String date35 = "2017-01-20";

		String date1 = date + " 00:00";
		String transactionDate = date35 + " 23:59";
		UserMonthlyBalanceDTO dto = new UserMonthlyBalanceDTO();
		FileWriter writer = new FileWriter("D:\\VpayQwikApp latest\\WebContent\\WEB-INF\\startup\\monthlyreport06.csv");
		writer.append(FILE_HEADER);
		int page = 0;
		int size = 10000;
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(page, size, sort);
		Page<User> user = userRepository.findAuthenticateUser(pageable);
		while (page <= user.getTotalPages()) {
			for (User u : user) {
				// For Every User these values must be set to 0
				double openingBalance = 0;
				double closingBalance = 0;
				double totalCredit = 0;
				double totalDebit = 0;
				dto.setMobileNo(u.getUsername());
				writer.append(NEW_LINE_SEPARATOR);
				writer.append(dto.getMobileNo());
				writer.append(COMMA_DELIMITIER);
				dto.setFirstName(u.getUserDetail().getFirstName());
				writer.append(dto.getFirstName());
				writer.append(COMMA_DELIMITIER);
				// GET ALL TRANSACTIONS BETWEEN TWO DATES
				List<PQTransaction> transaction = pqTransactionRepository.findMonthlyUserTransaction(
						u.getAccountDetail(), dateFormat.parse(date1), dateFormat.parse(transactionDate));
				if (transaction != null && transaction.size() > 0) {
					System.err.println("inside if condition");
					for (int i = 0; i < transaction.size(); i++) {
						PQTransaction openingbalance = transaction.get(i);
						if (i == 0) {
							if (openingbalance.isDebit()) {
								openingBalance = openingbalance.getCurrentBalance() + openingbalance.getAmount();
							} else {
								openingBalance = openingbalance.getCurrentBalance() - openingbalance.getAmount();
							}
						}
						if (i == transaction.size() - 1) {
							closingBalance = openingbalance.getCurrentBalance();

						}
						if (openingbalance.isDebit()) {
							// Add total Debit
							totalDebit += openingbalance.getAmount();
						} else {
							// Add total credit
							totalCredit += openingbalance.getAmount();
						}
					}
				}
				dto.setOpeningBalance(openingBalance);
				writer.append(String.format("%.2f", dto.getOpeningBalance()));
				writer.append(COMMA_DELIMITIER);
				dto.setClosingBalance(closingBalance);
				writer.append(String.format("%.2f", dto.getClosingBalance()));
				writer.append(COMMA_DELIMITIER);
				dto.setDebitBalance(totalDebit);
				writer.append(String.format("%.2f", dto.getDebitBalance()));
				writer.append(COMMA_DELIMITIER);
				dto.setCreditBalance(totalCredit);
				writer.append(String.format("%.2f", dto.getCreditBalance()));
				writer.append(COMMA_DELIMITIER);

				// NO NEED FOR ELSE AS THESE VALUES ALREADY SET
			}
			page += 1;
			System.err.println("page ::" + page);
			sort = new Sort(Sort.Direction.DESC, "id");
			pageable = new PageRequest(page, size, sort);
			user = userRepository.findAuthenticateUser(pageable);
		}
		System.err.println("while loop closed .");
		writer.flush();
		writer.close();
	}

	@Override
	public User findByAccountDetails(PQAccountDetail accountDetail) {
		return userRepository.findByAccountDetails(accountDetail);
	}

	@Override
	public List<User> listUser() {
		return userRepository.findAllAuthenticateUserList();
	}

	// for single user notification

	@Override
	public String getGCMIdByUserName(String username) {
		return userRepository.getGCMIdsByUserName(UserType.User, username);
	}

	@Override
	public List<MTransactionResponseDTO> getPromoCodeTransactions() {
		List<MTransactionResponseDTO> listDTOs = new ArrayList<>();
		Pageable page = new PageRequest(0, 1000);
		try {
			PQService service = pqServiceRepository.findServiceByCode("PPS");
			Page<PQTransaction> transactionList = pqTransactionRepository.findPagewiseTransactionByServiceAndDebit(page,
					service, false);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList.getContent());
			List<User> userList = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getMerchantTransactions(transactionList.getContent(), userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<MTransactionResponseDTO> getPromoCodeTransactionsFiltered(Date startDate, Date endDate) {
		List<MTransactionResponseDTO> listDTOs = new ArrayList<>();
		try {
			PQService service = pqServiceRepository.findServiceByCode("PPS");
			List<PQTransaction> transactionList = pqTransactionRepository
					.findPagewiseTransactionByServiceAndDebitFiltered(service, false, startDate, endDate);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> userList = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getMerchantTransactions(transactionList, userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public ResponseDTO refundMerchantRequest(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String trnsactionRefNo = dto.getTransactionRefNo().substring(0, dto.getTransactionRefNo().length() - 1);
		PQTransaction transaction = pqTransactionRepository.findByTransactionRefNo(trnsactionRefNo + "D");
		PQTransaction merchantTransaction = pqTransactionRepository.findByTransactionRefNo(dto.getTransactionRefNo());
		if (transaction != null) {
			if (!merchantTransaction.getStatus().equals(Status.Processing)) {
				if (!merchantTransaction.getStatus().equals(Status.Refunded)) {
					MerchantRefundRequest refundRequest = new MerchantRefundRequest();
					User account = findByAccountDetail(transaction.getAccount());
					refundRequest.setOrderId(Long.parseLong(dto.getOrderId()));
					refundRequest.setTrnasactionRefNo(transaction.getTransactionRefNo());
					refundRequest.setUsername(account.getUsername());
					refundRequest.setAmount(transaction.getAmount());
					refundRequest.setRefundAmount(Double.parseDouble(dto.getRefundAmount()));
					refundRequest.setStatus(Status.Processing);
					merchantTransaction.setStatus(Status.Processing);
					pqTransactionRepository.save(merchantTransaction);
					merchantRefundRequestRepository.save(refundRequest);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Your request for Refund has been proccessed successfully.");
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("The request has been already refunded.");
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("You have already request for refund.");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction not exists");
		}
		return result;
	}

	/* lets manage */
	@Override
	public ResponseLetsDTO getListCount(String key) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		GetMainList dto = new GetMainList();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				Calendar now = Calendar.getInstance(); // Gets the current date
														// and time
				int prevyear1 = now.get(Calendar.YEAR) - 1;
				int prevyear2 = now.get(Calendar.YEAR) - 2;

				System.err.println(prevyear1);
				long userCount = userRepository.getCountUser();
				long transactionCount = pqTransactionRepository.getransactionCount();
				long rechargeCount = pqTransactionRepository.getAllRecharge(18);
				long loadMoneyCount = pqTransactionRepository.getAllRecharge(1);
				long billPayCount = pqTransactionRepository.getAllRecharge(18);
				long totalUsersprev1 = userRepository.getCountUseryear(prevyear1);
				long totalTransprev1 = pqTransactionRepository.getransactionCountyear(prevyear1);
				;
				long totalUsersprev2 = userRepository.getCountUseryear(prevyear2);
				long totalTransprev2 = pqTransactionRepository.getransactionCountyear(prevyear2);
				;
				System.err.println(loadMoneyCount);
				dto.setUserCount(userCount);
				dto.setTransactionCount(transactionCount);
				dto.setLoadMoneyCount(loadMoneyCount);
				dto.setRechargeCount(rechargeCount);
				dto.setBillPayCount(billPayCount);
				dto.setTotalUsersprev1(totalUsersprev1);
				dto.setTotalUsersprev2(totalUsersprev2);

				dto.setTotalTransprev1(totalTransprev1);
				dto.setTotalTransprev2(totalTransprev2);

				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get All transaction Type Details");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F00");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO getAllUserList(String key) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		List<UserTransactionListDTO> listDTO = new ArrayList<UserTransactionListDTO>();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				long creditTransaction = pqTransactionRepository.getdebitTransactioncount(false);
				long debitTransaction = pqTransactionRepository.getdebitTransactioncount(true);
				Pageable page = new PageRequest(0, 10);
				Page<PQTransaction> pqTransaction = pqTransactionRepository.findAllTransactionList(page);
				System.err.println(pqTransaction);
				GetAllUserList dto = new GetAllUserList();
				dto.setCreditCount(creditTransaction);
				dto.setDebitCount(debitTransaction);
				List<PQTransaction> pqTransaction1 = pqTransaction.getContent();
				System.err.println("list size::" + pqTransaction1.size());
				if (pqTransaction != null && pqTransaction1.size() > 0) {
					for (int i = 0; i < pqTransaction1.size(); i++) {
						UserTransactionListDTO list = new UserTransactionListDTO();
						list.setId(i + 1);
						PQAccountDetail pq = pqTransaction1.get(i).getAccount();
						long id = userRepository.getUser(pq);
						String uname = userDetailRepository.getUserNameByAccount(id);
						list.setName(uname);

						System.err.println(pqTransaction1.get(i).getTransactionRefNo());
						list.setTid(pqTransaction1.get(i).getTransactionRefNo());
						System.err.println(pqTransaction1.get(i).getCreated());
						list.setDate(pqTransaction1.get(i).getCreated().toString());
						list.setTransactionType(pqTransaction1.get(i).getTransactionType().toString());
						if (pqTransaction1.get(i).isDebit()) {
							list.setCredit(0);
							list.setDebit(pqTransaction1.get(i).getAmount());
						} else {
							list.setDebit(0);
							list.setCredit(pqTransaction1.get(i).getAmount());
						}
						list.setStatus(pqTransaction1.get(i).getStatus().getValue());
						listDTO.add(list);
					}
					dto.setListDTO(listDTO);
				}

				if (pqTransaction1.size() != 0) {
					Calendar now = Calendar.getInstance(); // Gets the current
															// date and time
					int year = now.get(Calendar.YEAR); // The current year
					int presentmonth = now.get(Calendar.MONTH) + 1;
					int currmonth = now.get(Calendar.MONTH);
					int prev1month = now.get(Calendar.MONTH) - 1;
					int prev2month = now.get(Calendar.MONTH) - 2;
					System.err.println("current month is:" + currmonth);
					long present = pqTransactionRepository.getCurrentTransaction(year, presentmonth);
					long current = pqTransactionRepository.getCurrentTransaction(year, currmonth);
					long prevmonth1 = pqTransactionRepository.getCurrentTransaction(year, prev1month);
					long prevmonth2 = pqTransactionRepository.getCurrentTransaction(year, prev2month);
					TransChartCount transCount = new TransChartCount();
					transCount.setPresent(present);
					transCount.setCurrent(current);
					;
					transCount.setPrevmonth1(prevmonth1);
					;
					transCount.setPrevmonth2(prevmonth2);
					dto.setTransCount(transCount);
				}
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get All list");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO getdebitTransactionsList(String key) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		List<DebitCreditListDTO> listDTO = new ArrayList<DebitCreditListDTO>();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				Pageable page = new PageRequest(0, 10);
				Page<PQTransaction> pqTransaction1 = pqTransactionRepository.findDebitTransactionList(page, true);
				System.err.println("List is:" + pqTransaction1);
				List<PQTransaction> pqTransaction = pqTransaction1.getContent();
				DebitDTO dto = new DebitDTO();
				if (pqTransaction != null && pqTransaction.size() != 0) {
					for (int i = 0; i < pqTransaction.size(); i++) {
						DebitCreditListDTO list = new DebitCreditListDTO();
						list.setId(i + 1);
						PQAccountDetail pq = pqTransaction.get(i).getAccount();
						long id = userRepository.getUser(pq);
						String uname = userDetailRepository.getUserNameByAccount(id);
						list.setName(uname);
						list.setTid(pqTransaction.get(i).getTransactionRefNo());
						list.setDate(pqTransaction.get(i).getCreated().toString());
						list.setTid(pqTransaction.get(i).getTransactionRefNo());
						list.setTransactionType(pqTransaction.get(i).getTransactionType().toString());
						list.setDebitCredit(pqTransaction.get(i).getAmount());
						list.setStatus(pqTransaction.get(i).getStatus().getValue());
						listDTO.add(list);
					}
					dto.setListDTO(listDTO);
				}
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get All Debit list");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public ResponseLetsDTO getServiceList(String key) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		GetSeviceListDTO dto = new GetSeviceListDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				long sendMoney = pqTransactionRepository.getAllRecharge(3);
				/*
				 * Double creditTransaction
				 * =pqTransactionRepository.getdebitTransactionSum(false);
				 * Double debitTransaction=pqTransactionRepository.
				 * getdebitTransactionSum(true); Double poolAccount=
				 * creditTransaction-debitTransaction;
				 */
				Double poolAccount = userRepository.findUsersWalletBalance();
				dto.setSendMoney(sendMoney);
				dto.setPoolAccount(poolAccount);
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get All transaction Type Details");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public List<TListDTO> getAllIpayTxns(Date startDate, Date endDate) {
		List<TListDTO> listDTOs = new ArrayList<>();
		try {
			PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
			System.err.println("serviceType -------------- ------------ " + serviceType);
			List<PQService> services = pqServiceRepository.findByServiceType(serviceType);
			List<PQTransaction> transactionList = pqTransactionRepository
					.getTransactionBetweenTopup(TransactionType.DEFAULT, startDate, endDate, services);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, transactionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<TListDTO> getAllIpayTxnsByStatus(Date startDate, Date endDate, Status status) {
		List<TListDTO> listDTOs = new ArrayList<>();
		try {
			PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
			List<PQService> services = pqServiceRepository.findByServiceType(serviceType);
			List<PQTransaction> transactionList = pqTransactionRepository
					.getTransactionBetweenTopupByStatus(TransactionType.DEFAULT, startDate, endDate, services, status);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, transactionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<TListDTO> getAllLoadMoneyTxns(Date startDate, Date endDate, String serviceCode) {
		List<TListDTO> listDTOs = new ArrayList<>();
		try {
			PQService service = pqServiceRepository.findByName(serviceCode);
			List<PQTransaction> transactionList = pqTransactionRepository.getAllLoadMoney(TransactionType.DEFAULT,
					startDate, endDate, service);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, transactionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<PQTransaction> getAllVijayaUPITxns(Date startDate, Date endDate) {
		List<PQTransaction> listDTOs = new ArrayList<>();
		try {
			listDTOs = pqTransactionRepository.getAllVijayaUpiTxns(TransactionType.VIJAYA_UPI, startDate, endDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public List<TListDTO> getLoadMoneyTxnsByStatus(Date startDate, Date endDate, Status status, String serviceCode) {
		List<TListDTO> listDTOs = new ArrayList<>();
		try {
			PQService service = pqServiceRepository.findByName(serviceCode);
			List<PQTransaction> transactionList = pqTransactionRepository.getLoadMoneyByStatus(TransactionType.DEFAULT,
					startDate, endDate, service, status);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, transactionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public void createCsvForWeeklyReconTransaction() throws Exception {

		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date startDate = cal.getTime();
		Date endDate = new Date();

		// String date = "2018-01-20";
		// String date35 = "2018-01-24";
		// String startDate = date + " 00:00";
		// String endDate = date35 + " 23:59";
		// System.err.println("Start Date : : " + startDate);
		// System.err.println("End Date : : " + endDate);

		// TODO Reconciling Report for EBS
		String EBS_FILE_HEADER = "Wallet Date,User Info,Ref No.,Description,Amount,Status,EBS Date, EBS Payment ID,EBS Status";
		String ebsReport = "EBS_Recon_from_" + dateForm.format(startDate) + "_to_" + dateForm.format(endDate) + ".csv";
		// String ebsReport = "EBS_Recon_from_"+date+"_to_"+date35+".csv";
		FileWriter ebs_Writer = new FileWriter(StartupUtil.FILE_PATH + ebsReport);
		ebs_Writer.append(EBS_FILE_HEADER);
		try {
			List<TListDTO> ebsTxns = getAllLoadMoneyTxns(startDate, endDate, EBSConstants.EBS_SERVICE_CODE);
			// List<TListDTO> ebsTxns =
			// getAllLoadMoneyTxns(dateFormat.parse(startDate),
			// dateFormat.parse(endDate), EBSConstants.EBS_SERVICE_CODE);
			System.out.println(ebsTxns.size());
			if (!ebsTxns.isEmpty()) {
				for (TListDTO t : ebsTxns) {
					String txnRef = ConvertUtil.removeLastString(t.getTransactionRefNo());
					System.err.println("Txn Ref No : : " + txnRef);
					EBSStatusRequest req = new EBSStatusRequest();
					req.setTxnRefNo(txnRef);
					EBSStatusResponse result = requestHandlerApi.statusApi(req);
					System.err.println(
							"EBS DATE ::-----------------------------------------      ::: " + result.getEbsDate());
					System.err.println(
							"EBS Status ::-----------------------------------------    ::: " + result.getEbsStatus());
					System.err.println(
							"EBS Paymet ID ::----------------------------------------- ::: " + result.getPaymentId());
					if (result.isValid()) {
						System.err.println(
								"Wallet status : " + t.getStatus() + " | EBS status : " + result.getEbsStatus());
						if (!t.getStatus().equals(result.getEbsStatus())) {
							System.err.println("Inside True");
							ebs_Writer.append(NEW_LINE_SEPARATOR);
							ebs_Writer.append(t.getDateOfTransaction());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(t.getUsername() + "|" + t.getContactNo() + "|" + t.getEmail());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(t.getTransactionRefNo());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(t.getDescription());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(t.getAmount());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(t.getStatus());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(result.getEbsDate());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(result.getPaymentId());
							ebs_Writer.append(COMMA_DELIMITIER);
							ebs_Writer.append(result.getEbsStatus());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.err.println("while loop closed .");
		ebs_Writer.flush();
		ebs_Writer.close();

		// TODO Reconciling Report for VNet
		String VNET_FILE_HEADER = "Wallet Date,User Info,Ref No.,Description,Amount,Status,VNET Status";
		String vnetReport = "VNET_Recon_from_" + dateForm.format(startDate) + "_to_" + dateForm.format(endDate)
				+ ".csv";
		// String vnetReport = "VNET_Recon_from_"+date+"_to_"+date35+".csv";
		FileWriter vnet_Writer = new FileWriter(StartupUtil.FILE_PATH + vnetReport);
		vnet_Writer.append(VNET_FILE_HEADER);
		try {
			List<TListDTO> vnetTxns = getAllLoadMoneyTxns(startDate, endDate, VNetUtils.VNET_SERVICE_CODE);
			// List<TListDTO> vnetTxns =
			// getAllLoadMoneyTxns(dateFormat.parse(startDate),
			// dateFormat.parse(endDate), VNetUtils.VNET_SERVICE_CODE);
			System.out.println(vnetTxns.size());
			if (!vnetTxns.isEmpty()) {
				for (TListDTO t : vnetTxns) {
					String txnRef = ConvertUtil.removeLastString(t.getTransactionRefNo());
					System.err.println("Txn Ref No : : " + txnRef);
					VNetStatusRequest req = new VNetStatusRequest();
					req.setAmount(Double.parseDouble(t.getAmount()));
					req.setTxnRef(txnRef);

					VNetStatusResponse result = vnetStatusApi.vnetVarification(req);
					System.err.println(
							"VNET Status ::-----------------------------------------    ::: " + result.getStatus());
					System.err.println("Wallet status : " + t.getStatus() + " | VNET status : " + result.getStatus());
					if (!t.getStatus().equals(result.getStatus())) {
						System.err.println("Inside True");
						vnet_Writer.append(NEW_LINE_SEPARATOR);
						vnet_Writer.append(t.getDateOfTransaction());
						vnet_Writer.append(COMMA_DELIMITIER);
						vnet_Writer.append(t.getUsername() + "|" + t.getContactNo() + "|" + t.getEmail());
						vnet_Writer.append(COMMA_DELIMITIER);
						vnet_Writer.append(t.getTransactionRefNo());
						vnet_Writer.append(COMMA_DELIMITIER);
						vnet_Writer.append(t.getDescription());
						vnet_Writer.append(COMMA_DELIMITIER);
						vnet_Writer.append(t.getAmount());
						vnet_Writer.append(COMMA_DELIMITIER);
						vnet_Writer.append(t.getStatus());
						vnet_Writer.append(COMMA_DELIMITIER);
						vnet_Writer.append(result.getStatus());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.err.println("while loop closed .");
		vnet_Writer.flush();
		vnet_Writer.close();

		// TODO Reconciling Report for Instant Pay
		String IPAY_FILE_HEADER = "Wallet Date,User Info,Ref No.,Description,Amount,Status,Ipay Date,Ipay Payment ID,Ipay Status";
		String ipayReport = "Ipay_Recon_from_" + dateForm.format(startDate) + "_to_" + dateForm.format(endDate)
				+ ".csv";
		// String ipayReport = "Ipay_Recon_from_"+date+"_to_"+date35+".csv";
		FileWriter ipay_Writer = new FileWriter(StartupUtil.FILE_PATH + ipayReport);

		ipay_Writer.append(IPAY_FILE_HEADER);
		try {
			List<TListDTO> ipayTxns = getAllIpayTxns(startDate, endDate);
			// List<TListDTO> ipayTxns =
			// getAllIpayTxns(dateFormat.parse(startDate),
			// dateFormat.parse(endDate));
			if (!ipayTxns.isEmpty()) {
				for (TListDTO t : ipayTxns) {
					String txnRef = ConvertUtil.removeLastString(t.getTransactionRefNo());
					System.err.println("Ipay Txn Ref No : : " + txnRef);
					StatusCheckRequest req = new StatusCheckRequest();
					req.setAgentId(txnRef);
					StatusCheckResponse result = statusCheckApi.request(req);
					System.err.println("Ipay DATE ::-----------------------------------------      ::: "
							+ result.getStatusCheck().getDatetime());
					System.err.println("Ipay Status ::-----------------------------------------    ::: "
							+ result.getStatusCheck().getStatus());
					System.err.println("Ipay Paymet ID ::----------------------------------------- ::: "
							+ result.getStatusCheck().getOprId());
					System.err.println("Wallet status : " + t.getStatus() + " | Ipay status : "
							+ result.getStatusCheck().getStatus());
					if (result.getStatusCheck().getStatus() != null) {
						if (!t.getStatus().equals(result.getStatusCheck().getStatus())) {
							System.err.println("Inside True");
							ipay_Writer.append(NEW_LINE_SEPARATOR);
							ipay_Writer.append(t.getDateOfTransaction());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(t.getUsername() + "|" + t.getContactNo() + "|" + t.getEmail());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(t.getTransactionRefNo());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(t.getDescription());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(t.getAmount());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(t.getStatus());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(result.getStatusCheck().getDatetime());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(result.getStatusCheck().getStatus());
							ipay_Writer.append(COMMA_DELIMITIER);
							ipay_Writer.append(result.getStatusCheck().getOprId());
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.err.println("while loop closed .");
		ipay_Writer.flush();
		ipay_Writer.close();

		// TODO Reconciling Report for UPI
		String UPI_FILE_HEADER = "Wallet Date,User Info,Ref No.,Description,Amount,Status,FSS Payment ID,FSS Status";
		String upiReport = "UPI_Recon_from_" + dateForm.format(startDate) + "_to_" + dateForm.format(endDate) + ".csv";
		// String upiReport = "UPI_Recon_from_"+date+"_to_"+date35+".csv";
		FileWriter upiWriter = new FileWriter(StartupUtil.FILE_PATH + upiReport);
		upiWriter.append(UPI_FILE_HEADER);
		try {
			List<TListDTO> upiTxns = getAllLoadMoneyTxns(startDate, endDate, UpiConstants.UPI_SERVICE_CODE);
			// List<TListDTO> upiTxns =
			// getAllLoadMoneyTxns(dateFormat.parse(startDate),
			// dateFormat.parse(endDate), UpiConstants.UPI_SERVICE_CODE);
			System.out.println(upiTxns.size());
			if (!upiTxns.isEmpty()) {
				for (TListDTO t : upiTxns) {
					String txnRef = ConvertUtil.removeLastString(t.getTransactionRefNo());
					System.err.println("UPI Txn Ref No : : " + txnRef);

					UpiRequest req = new UpiRequest();
					req.setMsgId(t.getUpiId());
					req.setMerchantVpa(UpiConstants.LIVE_MERCHANT_VPA);
					req.setSdk(t.isSkdStatus());
					UpiResponse response = converUPIstatusRequest(req);
					if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
						MerchantTxnStatusResponse result = upiStatus(req);
						System.err.println("FSS Status ::-----------------------------------------    ::: "
								+ result.getUpiStatus());
						System.err.println("FSS Paymet ID ::----------------------------------------- ::: "
								+ result.getReferenceNo());
						System.err.println(
								"Wallet status : " + t.getStatus() + " | FSS status : " + result.getUpiStatus());
						if (result.isValid()) {
							System.err.println(
									"Wallet status : " + t.getStatus() + " | FSS status : " + result.getUpiStatus());
							if (!t.getStatus().equals(result.getUpiStatus())) {
								System.err.println("Inside True");
								upiWriter.append(NEW_LINE_SEPARATOR);
								upiWriter.append(t.getDateOfTransaction());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(t.getUsername() + "|" + t.getContactNo() + "|" + t.getEmail());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(t.getTransactionRefNo());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(t.getDescription());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(t.getAmount());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(t.getStatus());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(result.getReferenceNo());
								upiWriter.append(COMMA_DELIMITIER);
								upiWriter.append(result.getStatus());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.err.println("while loop closed .");
		upiWriter.flush();
		upiWriter.close();

		// TODO Mail Sender for Reconciling Report
		mailSenderApi.weeklyReconReport(StartupUtil.FILE_PATH + ebsReport, StartupUtil.FILE_PATH + ipayReport,
				StartupUtil.FILE_PATH + vnetReport, StartupUtil.FILE_PATH + upiWriter);
	}

	// TODO Weekly PqyQwik Performance Report
	@Override
	public void createCsvForWeeklyPerformaceReport() throws Exception {

		SimpleDateFormat dFormate = new SimpleDateFormat("dd-MMMM-yy");
		FileWriter weekRprt_Writer = null;
		BufferedWriter out = null;
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date startDate = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);
		Date endDate = cal2.getTime();*/
		
		 String date = "2018-07-23";
		 String date35 = "2018-07-29";
		 String start = date + " 00:00";
		 String end = date35 + " 23:59";

		 Date startDate = dateFormat.parse(start);
		 Date endDate = dateFormat.parse(end);
		System.err.println("Start Date : : " + dateFormat.format(startDate));
		System.err.println("End Date   : : " + dateFormat.format(endDate));

		// TODO Reconciling Report for EBS
		String FILE_HEADER = "Week,Date From,Date To,Period in days,Customers,Percent Customers Growth over Prior Date,Total No. of Transactions, Total Amount of Transactions(Weekly),Avg Revenue per Customer in Canadian Dollar,Percent Transactions Growth over Prior Date,Transaction per Customer,Active Users,Comments";
		String weekRprt = "PayQwik Performance Tracking Sheet_from_" + dateForm.format(startDate) + "_to_"
				+ dateForm.format(endDate) + ".csv";

		File file = new File(StartupUtil.FILE_PATH + weekRprt);
		System.err.println("file ::" + file.exists());
		if (!file.exists()) {
			System.err.println("file doesnot exist");
			file.createNewFile();
			weekRprt_Writer = new FileWriter(file, true);
			out = new BufferedWriter(weekRprt_Writer);
			weekRprt_Writer.append(FILE_HEADER);
			try {
				List<AutoReconWeeklyReport> listRc = autoReconWeeklyReportRepository.findAllReconList();
				if (!listRc.isEmpty()) {
					AutoReconWeeklyReport reportExist = listRc.get(0);
					if (reportExist != null) {
						double amount = pqTransactionRepository.getSumOfAmountofTxnsByFromAndToDate(startDate, endDate);
						long countTxns = pqTransactionRepository.getCountOfTxnsByFromAndToDate(startDate, endDate);
						long countUser = userRepository.getCountUser(startDate, endDate);
						long countActiveUser = userRepository.getCountActiveUser(startDate, endDate);

						System.out.println("amount  " + amount);
						System.out.println("countTxns  " + countTxns);
						System.out.println("countUser  " + countUser);

						double existAmt = reportExist.getTotalAmount();
						System.out.println("existAmt  " + decimalFormatter.format(existAmt));

						long existTxn = Long.parseLong(reportExist.getTotalTxns());
						System.out.println("existTxn  " + existTxn);
						long exitsUser = Long.parseLong(reportExist.getCustomers());
						System.out.println("exitsUser  " + exitsUser);

						double finalAmt = amount + existAmt;
						System.out.println("finalAmt  " + decimalFormatter.format(finalAmt));
						long finalTxn = existTxn + countTxns;
						System.out.println("finalTxn  " + finalTxn);
						long finalUser = exitsUser + countUser;
						System.out.println("finalUser  " + finalUser);

						double percent = ConvertUtil.getPercent(exitsUser, finalUser);
						System.out.println("percent  " + percent);
						double avgRev = (finalAmt / finalUser);
						avgRev = avgRev * 0.02;
						System.out.println("avgRev  " + avgRev);

						double percentTxn = ConvertUtil.getPercent(existTxn, finalTxn);
						System.out.println("percentTxn  " + percentTxn);
						double netUser = finalUser;
						double txns = (finalTxn / netUser);
						txns = txns * 0.02;
						System.out.println("txns  " + txns);
						long week = Long.parseLong(reportExist.getWeek()) + 1;
						System.out.println("countTxns  " + week);

						AutoReconWeeklyReport newRecon = new AutoReconWeeklyReport();
						newRecon.setWeek(week + "");
						newRecon.setFromDate(startDate);
						newRecon.setToDate(endDate);

						// newRecon.setFromDate(dateFormat.parse(startDate));
						// newRecon.setToDate(dateFormat.parse(endDate));

						newRecon.setPeriodInDays("6");
						newRecon.setCustomers(finalUser + "");
						newRecon.setPercent(String.format("%.2f", percent));
						newRecon.setTotalTxns(finalTxn + "");
						newRecon.setTotalAmount(Double.parseDouble(String.format("%.2f", finalAmt)));
						newRecon.setAvgRevenue(String.format("%.2f", avgRev));
						newRecon.setPercentTxns(String.format("%.2f", percentTxn));
						newRecon.setTxnPerCustomer(String.format("%.5f", txns));
						newRecon.setActiveUsers(countActiveUser + "");
						autoReconWeeklyReportRepository.save(newRecon);

						List<AutoReconWeeklyReport> listRecon = autoReconWeeklyReportRepository.findAllReconList();
						System.err.println("size " + listRecon.size());
						for (AutoReconWeeklyReport w : listRecon) {
							System.err.println(w.getToDate() + " | " + decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(NEW_LINE_SEPARATOR);
							weekRprt_Writer.append(w.getWeek());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getFromDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getToDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPeriodInDays());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getCustomers());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercent() + "%");
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTotalTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getAvgRevenue());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercentTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTxnPerCustomer());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getActiveUsers());
						}
					}
				} else {
					System.err.println("List is Empty");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			weekRprt_Writer.flush();
			weekRprt_Writer.close();
		} else {
			System.err.println("file already exist");
		}
		// TODO Mail Sender for Reconciling Report
//		mailSenderApi.weeklyReconReport(StartupUtil.FILE_PATH + weekRprt);
	}
	
	
	@Override
	public void createCsvForWeeklyPerformaceReport1() throws Exception {

		SimpleDateFormat dFormate = new SimpleDateFormat("dd-MMMM-yy");
		FileWriter weekRprt_Writer = null;
		BufferedWriter out = null;
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date startDate = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);
		Date endDate = cal2.getTime();*/
		
		 String date = "2018-07-30";
		 String date35 = "2018-08-05";
		 String start = date + " 00:00";
		 String end = date35 + " 23:59";

		 Date startDate = dateFormat.parse(start);
		 Date endDate = dateFormat.parse(end);
		System.err.println("Start Date : : " + dateFormat.format(startDate));
		System.err.println("End Date   : : " + dateFormat.format(endDate));

		// TODO Reconciling Report for EBS
		String FILE_HEADER = "Week,Date From,Date To,Period in days,Customers,Percent Customers Growth over Prior Date,Total No. of Transactions, Total Amount of Transactions(Weekly),Avg Revenue per Customer in Canadian Dollar,Percent Transactions Growth over Prior Date,Transaction per Customer,Active Users,Comments";
		String weekRprt = "PayQwik Performance Tracking Sheet_from_" + dateForm.format(startDate) + "_to_"
				+ dateForm.format(endDate) + ".csv";

		File file = new File(StartupUtil.FILE_PATH + weekRprt);
		System.err.println("file ::" + file.exists());
		if (!file.exists()) {
			System.err.println("file doesnot exist");
			file.createNewFile();
			weekRprt_Writer = new FileWriter(file, true);
			out = new BufferedWriter(weekRprt_Writer);
			weekRprt_Writer.append(FILE_HEADER);
			try {
				List<AutoReconWeeklyReport> listRc = autoReconWeeklyReportRepository.findAllReconList();
				if (!listRc.isEmpty()) {
					AutoReconWeeklyReport reportExist = listRc.get(0);
					if (reportExist != null) {
						double amount = pqTransactionRepository.getSumOfAmountofTxnsByFromAndToDate(startDate, endDate);
						long countTxns = pqTransactionRepository.getCountOfTxnsByFromAndToDate(startDate, endDate);
						long countUser = userRepository.getCountUser(startDate, endDate);
						long countActiveUser = userRepository.getCountActiveUser(startDate, endDate);

						System.out.println("amount  " + amount);
						System.out.println("countTxns  " + countTxns);
						System.out.println("countUser  " + countUser);

						double existAmt = reportExist.getTotalAmount();
						System.out.println("existAmt  " + decimalFormatter.format(existAmt));

						long existTxn = Long.parseLong(reportExist.getTotalTxns());
						System.out.println("existTxn  " + existTxn);
						long exitsUser = Long.parseLong(reportExist.getCustomers());
						System.out.println("exitsUser  " + exitsUser);

						double finalAmt = amount + existAmt;
						System.out.println("finalAmt  " + decimalFormatter.format(finalAmt));
						long finalTxn = existTxn + countTxns;
						System.out.println("finalTxn  " + finalTxn);
						long finalUser = exitsUser + countUser;
						System.out.println("finalUser  " + finalUser);

						double percent = ConvertUtil.getPercent(exitsUser, finalUser);
						System.out.println("percent  " + percent);
						double avgRev = (finalAmt / finalUser);
						avgRev = avgRev * 0.02;
						System.out.println("avgRev  " + avgRev);

						double percentTxn = ConvertUtil.getPercent(existTxn, finalTxn);
						System.out.println("percentTxn  " + percentTxn);
						double netUser = finalUser;
						double txns = (finalTxn / netUser);
						txns = txns * 0.02;
						System.out.println("txns  " + txns);
						long week = Long.parseLong(reportExist.getWeek()) + 1;
						System.out.println("countTxns  " + week);

						AutoReconWeeklyReport newRecon = new AutoReconWeeklyReport();
						newRecon.setWeek(week + "");
						newRecon.setFromDate(startDate);
						newRecon.setToDate(endDate);

						// newRecon.setFromDate(dateFormat.parse(startDate));
						// newRecon.setToDate(dateFormat.parse(endDate));

						newRecon.setPeriodInDays("6");
						newRecon.setCustomers(finalUser + "");
						newRecon.setPercent(String.format("%.2f", percent));
						newRecon.setTotalTxns(finalTxn + "");
						newRecon.setTotalAmount(Double.parseDouble(String.format("%.2f", finalAmt)));
						newRecon.setAvgRevenue(String.format("%.2f", avgRev));
						newRecon.setPercentTxns(String.format("%.2f", percentTxn));
						newRecon.setTxnPerCustomer(String.format("%.5f", txns));
						newRecon.setActiveUsers(countActiveUser + "");
						autoReconWeeklyReportRepository.save(newRecon);

						List<AutoReconWeeklyReport> listRecon = autoReconWeeklyReportRepository.findAllReconList();
						System.err.println("size " + listRecon.size());
						for (AutoReconWeeklyReport w : listRecon) {
							System.err.println(w.getToDate() + " | " + decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(NEW_LINE_SEPARATOR);
							weekRprt_Writer.append(w.getWeek());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getFromDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getToDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPeriodInDays());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getCustomers());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercent() + "%");
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTotalTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getAvgRevenue());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercentTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTxnPerCustomer());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getActiveUsers());
						}
					}
				} else {
					System.err.println("List is Empty");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			weekRprt_Writer.flush();
			weekRprt_Writer.close();
		} else {
			System.err.println("file already exist");
		}
		// TODO Mail Sender for Reconciling Report
//		mailSenderApi.weeklyReconReport(StartupUtil.FILE_PATH + weekRprt);
	}
	
	@Override
	public void createCsvForWeeklyPerformaceReport2() throws Exception {

		SimpleDateFormat dFormate = new SimpleDateFormat("dd-MMMM-yy");
		FileWriter weekRprt_Writer = null;
		BufferedWriter out = null;
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date startDate = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);
		Date endDate = cal2.getTime();*/
		
		 String date = "2018-07-09";
		 String date35 = "2018-07-15";
		 String start = date + " 00:00";
		 String end = date35 + " 23:59";

		 Date startDate = dateFormat.parse(start);
		 Date endDate = dateFormat.parse(end);
		System.err.println("Start Date : : " + dateFormat.format(startDate));
		System.err.println("End Date   : : " + dateFormat.format(endDate));

		// TODO Reconciling Report for EBS
		String FILE_HEADER = "Week,Date From,Date To,Period in days,Customers,Percent Customers Growth over Prior Date,Total No. of Transactions, Total Amount of Transactions(Weekly),Avg Revenue per Customer in Canadian Dollar,Percent Transactions Growth over Prior Date,Transaction per Customer,Active Users,Comments";
		String weekRprt = "PayQwik Performance Tracking Sheet_from_" + dateForm.format(startDate) + "_to_"
				+ dateForm.format(endDate) + ".csv";

		File file = new File(StartupUtil.FILE_PATH + weekRprt);
		System.err.println("file ::" + file.exists());
		if (!file.exists()) {
			System.err.println("file doesnot exist");
			file.createNewFile();
			weekRprt_Writer = new FileWriter(file, true);
			out = new BufferedWriter(weekRprt_Writer);
			weekRprt_Writer.append(FILE_HEADER);
			try {
				List<AutoReconWeeklyReport> listRc = autoReconWeeklyReportRepository.findAllReconList();
				if (!listRc.isEmpty()) {
					AutoReconWeeklyReport reportExist = listRc.get(0);
					if (reportExist != null) {
						double amount = pqTransactionRepository.getSumOfAmountofTxnsByFromAndToDate(startDate, endDate);
						long countTxns = pqTransactionRepository.getCountOfTxnsByFromAndToDate(startDate, endDate);
						long countUser = userRepository.getCountUser(startDate, endDate);
						long countActiveUser = userRepository.getCountActiveUser(startDate, endDate);

						System.out.println("amount  " + amount);
						System.out.println("countTxns  " + countTxns);
						System.out.println("countUser  " + countUser);

						double existAmt = reportExist.getTotalAmount();
						System.out.println("existAmt  " + decimalFormatter.format(existAmt));

						long existTxn = Long.parseLong(reportExist.getTotalTxns());
						System.out.println("existTxn  " + existTxn);
						long exitsUser = Long.parseLong(reportExist.getCustomers());
						System.out.println("exitsUser  " + exitsUser);

						double finalAmt = amount + existAmt;
						System.out.println("finalAmt  " + decimalFormatter.format(finalAmt));
						long finalTxn = existTxn + countTxns;
						System.out.println("finalTxn  " + finalTxn);
						long finalUser = exitsUser + countUser;
						System.out.println("finalUser  " + finalUser);

						double percent = ConvertUtil.getPercent(exitsUser, finalUser);
						System.out.println("percent  " + percent);
						double avgRev = (finalAmt / finalUser);
						avgRev = avgRev * 0.02;
						System.out.println("avgRev  " + avgRev);

						double percentTxn = ConvertUtil.getPercent(existTxn, finalTxn);
						System.out.println("percentTxn  " + percentTxn);
						double netUser = finalUser;
						double txns = (finalTxn / netUser);
						txns = txns * 0.02;
						System.out.println("txns  " + txns);
						long week = Long.parseLong(reportExist.getWeek()) + 1;
						System.out.println("countTxns  " + week);

						AutoReconWeeklyReport newRecon = new AutoReconWeeklyReport();
						newRecon.setWeek(week + "");
						newRecon.setFromDate(startDate);
						newRecon.setToDate(endDate);

						// newRecon.setFromDate(dateFormat.parse(startDate));
						// newRecon.setToDate(dateFormat.parse(endDate));

						newRecon.setPeriodInDays("6");
						newRecon.setCustomers(finalUser + "");
						newRecon.setPercent(String.format("%.2f", percent));
						newRecon.setTotalTxns(finalTxn + "");
						newRecon.setTotalAmount(Double.parseDouble(String.format("%.2f", finalAmt)));
						newRecon.setAvgRevenue(String.format("%.2f", avgRev));
						newRecon.setPercentTxns(String.format("%.2f", percentTxn));
						newRecon.setTxnPerCustomer(String.format("%.5f", txns));
						newRecon.setActiveUsers(countActiveUser + "");
						autoReconWeeklyReportRepository.save(newRecon);

						List<AutoReconWeeklyReport> listRecon = autoReconWeeklyReportRepository.findAllReconList();
						System.err.println("size " + listRecon.size());
						for (AutoReconWeeklyReport w : listRecon) {
							System.err.println(w.getToDate() + " | " + decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(NEW_LINE_SEPARATOR);
							weekRprt_Writer.append(w.getWeek());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getFromDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getToDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPeriodInDays());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getCustomers());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercent() + "%");
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTotalTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getAvgRevenue());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercentTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTxnPerCustomer());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getActiveUsers());
						}
					}
				} else {
					System.err.println("List is Empty");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			weekRprt_Writer.flush();
			weekRprt_Writer.close();
		} else {
			System.err.println("file already exist");
		}
		// TODO Mail Sender for Reconciling Report
//		mailSenderApi.weeklyReconReport(StartupUtil.FILE_PATH + weekRprt);
	}
	
	@Override
	public void createCsvForWeeklyPerformaceReport3() throws Exception {

		SimpleDateFormat dFormate = new SimpleDateFormat("dd-MMMM-yy");
		FileWriter weekRprt_Writer = null;
		BufferedWriter out = null;
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date startDate = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);
		Date endDate = cal2.getTime();*/
		
		 String date = "2018-07-16";
		 String date35 = "2018-07-22";
		 String start = date + " 00:00";
		 String end = date35 + " 23:59";

		 Date startDate = dateFormat.parse(start);
		 Date endDate = dateFormat.parse(end);
		System.err.println("Start Date : : " + dateFormat.format(startDate));
		System.err.println("End Date   : : " + dateFormat.format(endDate));

		// TODO Reconciling Report for EBS
		String FILE_HEADER = "Week,Date From,Date To,Period in days,Customers,Percent Customers Growth over Prior Date,Total No. of Transactions, Total Amount of Transactions(Weekly),Avg Revenue per Customer in Canadian Dollar,Percent Transactions Growth over Prior Date,Transaction per Customer,Active Users,Comments";
		String weekRprt = "PayQwik Performance Tracking Sheet_from_" + dateForm.format(startDate) + "_to_"
				+ dateForm.format(endDate) + ".csv";

		File file = new File(StartupUtil.FILE_PATH + weekRprt);
		System.err.println("file ::" + file.exists());
		if (!file.exists()) {
			System.err.println("file doesnot exist");
			file.createNewFile();
			weekRprt_Writer = new FileWriter(file, true);
			out = new BufferedWriter(weekRprt_Writer);
			weekRprt_Writer.append(FILE_HEADER);
			try {
				List<AutoReconWeeklyReport> listRc = autoReconWeeklyReportRepository.findAllReconList();
				if (!listRc.isEmpty()) {
					AutoReconWeeklyReport reportExist = listRc.get(0);
					if (reportExist != null) {
						double amount = pqTransactionRepository.getSumOfAmountofTxnsByFromAndToDate(startDate, endDate);
						long countTxns = pqTransactionRepository.getCountOfTxnsByFromAndToDate(startDate, endDate);
						long countUser = userRepository.getCountUser(startDate, endDate);
						long countActiveUser = userRepository.getCountActiveUser(startDate, endDate);

						System.out.println("amount  " + amount);
						System.out.println("countTxns  " + countTxns);
						System.out.println("countUser  " + countUser);

						double existAmt = reportExist.getTotalAmount();
						System.out.println("existAmt  " + decimalFormatter.format(existAmt));

						long existTxn = Long.parseLong(reportExist.getTotalTxns());
						System.out.println("existTxn  " + existTxn);
						long exitsUser = Long.parseLong(reportExist.getCustomers());
						System.out.println("exitsUser  " + exitsUser);

						double finalAmt = amount + existAmt;
						System.out.println("finalAmt  " + decimalFormatter.format(finalAmt));
						long finalTxn = existTxn + countTxns;
						System.out.println("finalTxn  " + finalTxn);
						long finalUser = exitsUser + countUser;
						System.out.println("finalUser  " + finalUser);

						double percent = ConvertUtil.getPercent(exitsUser, finalUser);
						System.out.println("percent  " + percent);
						double avgRev = (finalAmt / finalUser);
						avgRev = avgRev * 0.02;
						System.out.println("avgRev  " + avgRev);

						double percentTxn = ConvertUtil.getPercent(existTxn, finalTxn);
						System.out.println("percentTxn  " + percentTxn);
						double netUser = finalUser;
						double txns = (finalTxn / netUser);
						txns = txns * 0.02;
						System.out.println("txns  " + txns);
						long week = Long.parseLong(reportExist.getWeek()) + 1;
						System.out.println("countTxns  " + week);

						AutoReconWeeklyReport newRecon = new AutoReconWeeklyReport();
						newRecon.setWeek(week + "");
						newRecon.setFromDate(startDate);
						newRecon.setToDate(endDate);

						// newRecon.setFromDate(dateFormat.parse(startDate));
						// newRecon.setToDate(dateFormat.parse(endDate));

						newRecon.setPeriodInDays("6");
						newRecon.setCustomers(finalUser + "");
						newRecon.setPercent(String.format("%.2f", percent));
						newRecon.setTotalTxns(finalTxn + "");
						newRecon.setTotalAmount(Double.parseDouble(String.format("%.2f", finalAmt)));
						newRecon.setAvgRevenue(String.format("%.2f", avgRev));
						newRecon.setPercentTxns(String.format("%.2f", percentTxn));
						newRecon.setTxnPerCustomer(String.format("%.5f", txns));
						newRecon.setActiveUsers(countActiveUser + "");
						autoReconWeeklyReportRepository.save(newRecon);

						List<AutoReconWeeklyReport> listRecon = autoReconWeeklyReportRepository.findAllReconList();
						System.err.println("size " + listRecon.size());
						for (AutoReconWeeklyReport w : listRecon) {
							System.err.println(w.getToDate() + " | " + decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(NEW_LINE_SEPARATOR);
							weekRprt_Writer.append(w.getWeek());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getFromDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(dFormate.format(w.getToDate()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPeriodInDays());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getCustomers());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercent() + "%");
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTotalTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(decimalFormatter.format(w.getTotalAmount()));
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getAvgRevenue());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getPercentTxns());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getTxnPerCustomer());
							weekRprt_Writer.append(COMMA_DELIMITIER);
							weekRprt_Writer.append(w.getActiveUsers());
						}
					}
				} else {
					System.err.println("List is Empty");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			weekRprt_Writer.flush();
			weekRprt_Writer.close();
		} else {
			System.err.println("file already exist");
		}
		// TODO Mail Sender for Reconciling Report
//		mailSenderApi.weeklyReconReport(StartupUtil.FILE_PATH + weekRprt);
	}

	@Override
	public ResponseLetsDTO getTransDTO(String key, GetTransDTO dtotrans) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		List<UserTransactionListDTO> listDTO = new ArrayList<UserTransactionListDTO>();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				Date from = new SimpleDateFormat("yyyy-MM-dd").parse(dtotrans.getFrom());
				Date to = new SimpleDateFormat("yyyy-MM-dd").parse(dtotrans.getTo());
				Pageable page = new PageRequest(0, 1000);
				Page<PQTransaction> pqTransaction = pqTransactionRepository.findAllTransactionListDateWise(page, from,
						to);
				System.err.println(pqTransaction);
				DateWiseTransDTO dto = new DateWiseTransDTO();
				List<PQTransaction> pqTransaction1 = pqTransaction.getContent();
				System.err.println("list size::" + pqTransaction1.size());
				if (pqTransaction != null && pqTransaction1.size() > 0) {
					for (int i = 0; i < pqTransaction1.size(); i++) {
						UserTransactionListDTO list = new UserTransactionListDTO();
						list.setId(i + 1);
						PQAccountDetail pq = pqTransaction1.get(i).getAccount();
						long id = userRepository.getUser(pq);
						String uname = userDetailRepository.getUserNameByAccount(id);
						list.setName(uname);

						System.err.println(pqTransaction1.get(i).getTransactionRefNo());
						list.setTid(pqTransaction1.get(i).getTransactionRefNo());
						System.err.println(pqTransaction1.get(i).getCreated());
						list.setDate(pqTransaction1.get(i).getCreated().toString());
						list.setTransactionType(pqTransaction1.get(i).getTransactionType().toString());
						if (pqTransaction1.get(i).isDebit()) {
							list.setCredit(0);
							list.setDebit(pqTransaction1.get(i).getAmount());
						} else {
							list.setDebit(0);
							list.setCredit(pqTransaction1.get(i).getAmount());
						}
						list.setStatus(pqTransaction1.get(i).getStatus().getValue());
						listDTO.add(list);
					}
					dto.setListDTO(listDTO);
				}

				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("DATEWISE TRANSACTION LIST");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public ResponseLetsDTO getcreditTransactionsList(String key) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		List<DebitCreditListDTO> listDTO = new ArrayList<DebitCreditListDTO>();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				Pageable page = new PageRequest(0, 10);
				Page<PQTransaction> pqTransaction1 = pqTransactionRepository.findDebitTransactionList(page, false);
				List<PQTransaction> pqTransaction = pqTransaction1.getContent();
				System.err.println(pqTransaction);
				DebitDTO dto = new DebitDTO();
				if (pqTransaction != null && pqTransaction.size() != 0) {
					for (int i = 0; i < pqTransaction.size(); i++) {
						DebitCreditListDTO list = new DebitCreditListDTO();
						list.setId(i + 1);
						PQAccountDetail pq = pqTransaction.get(i).getAccount();
						long id = userRepository.getUser(pq);
						String uname = userDetailRepository.getUserNameByAccount(id);
						list.setName(uname);
						list.setTid(pqTransaction.get(i).getTransactionRefNo());
						list.setDate(pqTransaction.get(i).getCreated().toString());
						list.setTid(pqTransaction.get(i).getTransactionRefNo());
						list.setTransactionType(pqTransaction.get(i).getTransactionType().toString());
						list.setDebitCredit(pqTransaction.get(i).getAmount());
						list.setStatus(pqTransaction.get(i).getStatus().getValue());
						listDTO.add(list);
					}
					dto.setListDTO(listDTO);
				}
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get All Credit list");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public ResponseLetsDTO getSuperAdminList(String key, UserTypeDto dto1) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		List<SuperAdminDTO> listDTO = new ArrayList<SuperAdminDTO>();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				Pageable page = new PageRequest(0, 20);
				Page<User> superList;
				List<User> superLists = null;
				System.err.println(dto1.getUserType());

				if (dto1.getUserType().equalsIgnoreCase("superadmin")) {
					superList = userRepository.findSuperAdminList(page);
					superLists = superList.getContent();
				} else if (dto1.getUserType().equalsIgnoreCase("admin")) {
					superList = userRepository.findAdminList(page);
					superLists = superList.getContent();

				}
				SuperAdminListDTO dto = new SuperAdminListDTO();
				if (superLists != null && superLists.size() != 0) {
					for (int i = 0; i < superLists.size(); i++) {
						SuperAdminDTO list = new SuperAdminDTO();
						list.setSuperName(superLists.get(i).getUsername());
						list.setEmail(superLists.get(i).getUserDetail().getEmail());
						list.setContactNo(superLists.get(i).getUserDetail().getContactNo());
						list.setAddress(superLists.get(i).getUserDetail().getAddress());
						list.setStatus(superLists.get(i).getAuthority());
						listDTO.add(list);
					}
					dto.setListDTO(listDTO);
				}
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get list");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public ResponseLetsDTO getSuperAdminBlockList(String key, UserTypeDto dto1) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		List<SuperAdminDTO> listDTO = new ArrayList<SuperAdminDTO>();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				Pageable page = new PageRequest(0, 20);
				Page<User> superList;
				List<User> superLists = null;
				System.err.println(dto1.getUserType());

				if (dto1.getUserType().equalsIgnoreCase("superadmin")) {
					superList = userRepository.findSuperAdminBlockList(page);
					superLists = superList.getContent();
				} else if (dto1.getUserType().equalsIgnoreCase("admin")) {
					superList = userRepository.findAdminBlockList(page);
					superLists = superList.getContent();

				}
				SuperAdminListDTO dto = new SuperAdminListDTO();
				if (superLists != null && superLists.size() != 0) {
					for (int i = 0; i < superLists.size(); i++) {
						SuperAdminDTO list = new SuperAdminDTO();
						list.setSuperName(superLists.get(i).getUsername());
						list.setEmail(superLists.get(i).getUserDetail().getEmail());
						list.setContactNo(superLists.get(i).getUserDetail().getContactNo());
						list.setAddress(superLists.get(i).getUserDetail().getAddress());
						list.setStatus(superLists.get(i).getAuthority());
						listDTO.add(list);
					}
					dto.setListDTO(listDTO);
				}
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("Get list");
				result.setDetails(dto);
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public ResponseLetsDTO addSuperAdmin(String key, SuperAdminAddDTO superdto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				User user1 = userRepository.checkSuperAdminUserName(superdto.getUserName());
				if (user1 == null) {
					System.err.println("here");
					UserDetail dto = new UserDetail();
					dto.setAddress(superdto.getAddress());
					dto.setContactNo(superdto.getContactNo());
					dto.setEmail(superdto.getEmail());
					dto.setFirstName(superdto.getFirstName());
					dto.setLastName(superdto.getLastName());
					User user = new User();
					user.setUserDetail(dto);
					user.setMobileStatus(Status.Active);
					user.setEmailStatus(Status.Active);
					user.setPassword(passwordEncoder.encode((superdto.getPassword())));
					String type = superdto.getUserType();
					if (type.equalsIgnoreCase("superadmin")) {
						user.setUserType(UserType.SuperAdmin);
					}
					if (type.equalsIgnoreCase("admin")) {
						user.setUserType(UserType.Admin);
					}
					user.setUsername(superdto.getUserName());
					if (type.equalsIgnoreCase("superadmin")) {
						user.setAuthority(Authorities.SUPER_ADMIN + "," + Authorities.AUTHENTICATED);
					} else if (type.equalsIgnoreCase("admin")) {
						user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
					}
					userDetailRepository.save(dto);
					userRepository.save(user);
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("Successfully Added");
					result.setDetails("Added");
				} else {
					result.setCode("S01");
					result.setStatus(ResponseStatusLetsMan.FAILURE);
					result.setMessage("userName exist change username");
					result.setDetails("change username");
				}
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public ResponseLetsDTO changePwdSuperAdmin(String key, PassWordSuperAdminDTO dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			System.err.println("here");
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				System.err.println("here");
				userRepository.changeSuperPwd(dto.getUserName(), passwordEncoder.encode(dto.getPassword()));
				result.setCode("S00");
				result.setStatus(ResponseStatusLetsMan.SUCCESS);
				result.setMessage("woooo!!!Password Successfully Changed");
				result.setDetails("woooo!!!password Changed");
				System.err.println("changed");

			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO blockSuperAdmin(String key, PassWordSuperAdminDTO dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				User user = userRepository.findUserByNameContact(dto.getUserName());
				if (user != null) {
					userRepository.blockSuperAdmin(dto.getUserName());
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!SuperAdmin Blocked");
					result.setDetails("woooo!!!SuperAdmin Blocked");
				} else {
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("SuperAdmin Does Not Exist");
					result.setDetails("OOOPS!!!! SuperAdmin nahi mila");
				}
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO blockSuperAdmin(String key, BlockDto dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				User user = null;
				User user1 = null;
				if (dto.getUserType().equalsIgnoreCase("superadmin")) {
					user = userRepository.findUserByNameContact(dto.getUserName());
				} else if (dto.getUserType().equalsIgnoreCase("admin")) {
					user1 = userRepository.findUserByNameContactb(dto.getUserName());
				}
				if (user != null) {
					System.err.println("block here");
					userRepository.blockSuperAdmin(dto.getUserName());
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!SuperAdmin Blocked");
					result.setDetails("woooo!!!SuperAdmin Blocked");
				} else if (user1 != null) {
					System.err.println("block here");
					userRepository.blockSuperAdminb(dto.getUserName());
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!Admin Blocked");
					result.setDetails("woooo!!!Admin Blocked");
				} else {
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage(" Does Not Exist");
					result.setDetails("OOOPS!!!! SuperAdmin nahi mila");
				}
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}
	/* Admin Panel Report */

	@Override
	public Page<WalletOutstandingDTO> getWalletOutsReportByDate(Pageable pageable, Date from, Date to) {

		Page<WalletOutstandingDTO> result = null;
		List<WalletOutstandingDTO> dictWallet = new ArrayList<>();
		double disBal = 0;
		try {
			Date toDate = formatter.parse(ConvertUtil.converCurToPastDate(to));
			List<Date> txnDates = pqTransactionRepository.getBeginDateOfAllTxns();
			// Date lastDate = txnDates.get(0);

			List<Date> d = userRepository.getBeginDateOfUsers();
			Date beginDate = d.get(0);
			Page<User> users = userRepository.findAllByDate(beginDate, to, pageable);
			for (User u : users) {

				// System.err.println("Userinfomation :: : : " + u.getUsername()
				// + " Email : : " + u.getUserDetail().getEmail());

				List<PQAccountDetail> listAcc = pqTransactionRepository.getTotalAccountByFromAndToDate(from, to);
				System.err.println("List Account : :  : " + listAcc.size());
				if (!listAcc.isEmpty()) {
					for (PQAccountDetail ac : listAcc) {
						List<PQTransaction> t = pqTransactionRepository.getTotalTxnsByStatusAndAccountId(ac, from, to);

						// System.out.println("User Account Id = " +
						// u.getAccountDetail().getId() + " | "+ t.get(t.size()
						// - 1).getAccount().getId() + " | " + t.get(t.size() -
						// 1).getId());

						if (u.getAccountDetail().getId() == t.get(t.size() - 1).getAccount().getId()) {
							PQTransaction txn = t.get(t.size() - 1);
							disBal = disBal + txn.getCurrentBalance();
							WalletOutstandingDTO dto = ConvertUtil.convertWalletOustReportWithTxn(u, txn);
							dictWallet.add(dto);
						} else {
							// System.out.println("Else Account Id = " +
							// u.getAccountDetail().getId() + " | "+
							// t.get(t.size() - 1).getAccount().getId() + " | "
							// + t.get(t.size() - 1).getId());
						}
					}
				}
			}

			result = new PageImpl<WalletOutstandingDTO>(dictWallet, pageable, dictWallet.size());
			double totalBal = 0;
			for (WalletOutstandingDTO w : result) {

				// System.out.println("Wallet Oustanding Username and Email :: :
				// : : " + w.getDate() + " | " + w.getName()+ " | " +
				// w.getEmail() + " | " + w.getMobile() + " | " +
				// w.getBalance());
				totalBal = totalBal + w.getBalance();
			}

			/*
			 * System.out.println("TotalBal : :  " + totalBal);
			 * System.out.println("Distinct Bal : : " + disBal);
			 * System.out.println("beginDate :: : : " + lastDate);
			 * System.out.println("toDate :  " + toDate);
			 */

			return result;
		} catch (ParseException e) {
			e.printStackTrace();
			return result;
		}
	}

	@Override
	public Page<WalletOutstandingDTO> getWalletOutsReport(Pageable pageable, Date from, Date to) {

		Page<User> users = userRepository.findAll(pageable);

		Page<WalletOutstandingDTO> result = null;
		List<WalletOutstandingDTO> lstWallet = new ArrayList<>();
		List<WalletOutstandingDTO> dictWallet = new ArrayList<>();
		List<WalletOutstandingDTO> mainWallet = new ArrayList<>();
		try {
			Date previousDate = formatter.parse(ConvertUtil.converCurToPastDate(new Date()));
			for (User u : users) {
				/*
				 * System.err.println( "Userinfomation :: : : " +
				 * u.getUsername() + " Email : :  " +
				 * u.getUserDetail().getEmail());
				 */
				List<PQAccountDetail> listAcc = pqTransactionRepository.getTotalAccountByFromDate(previousDate);
				System.err.println("List Account : :  : " + listAcc.size());
				if (!listAcc.isEmpty()) {
					for (PQAccountDetail ac : listAcc) {
						List<PQTransaction> t = pqTransactionRepository.getTotalTxnByStatusAndAccountId(ac);
						System.out.println(
								"User Account Id = " + u.getAccountDetail().getId() + " | " + t.get(0).getId());
						if (u.getAccountDetail().getId() == t.get(0).getAccount().getId()) {
							PQTransaction txn = t.get(0);
							System.out.println("Transaction Ref :: : : " + txn.getAuthReferenceNo());
							WalletOutstandingDTO dto = ConvertUtil.convertWalletOustReportWithTxn(u, txn);
							dictWallet.add(dto);
						} else {
							// System.out.println("User details sls s ; : : " +
							// lstWallet.size());
							/*
							 * if(lstWallet.isEmpty()){ WalletOutstandingDTO dto
							 * = ConvertUtil.convertWalletOustReport(u);
							 * System.out.println("User Deatils : " +
							 * lstWallet.contains(dto)); lstWallet.add(dto); }
							 */

							/*
							 * WalletOutstandingDTO dto =
							 * ConvertUtil.convertWalletOustReport(u);
							 * System.out.println("User Deatils : " +
							 * lstWallet.contains(dto)); lstWallet.add(dto);
							 */
						}
					}
				} else {
					// WalletOutstandingDTO dto =
					// ConvertUtil.convertWalletOustReport(u);
					// lstWallet.add(dto);
				}
			}

			for (User u : users) {
				WalletOutstandingDTO dto = ConvertUtil.convertWalletOustReport(u);
				lstWallet.add(dto);
			}

			for (WalletOutstandingDTO lW : lstWallet) {
				if (!dictWallet.isEmpty()) {
					for (WalletOutstandingDTO dW : dictWallet) {
						if (lW.getMobile().equals(dW.getMobile())) {
							System.out.println("Distinct : : : " + dW.getMobile() + " | " + dW.getBalance());
							mainWallet.add(dW);
						} /*
							 * else { // if(lW.getMobile().equals(anObject))
							 * mainWallet.add(lW); }
							 */
					}
				} else {
					mainWallet.add(lW);
				}

			}
			if (!dictWallet.isEmpty()) {
				System.err.println("Mobile No : : " + dictWallet.get(0).getMobile());
			}

			// for (WalletOutstandingDTO dW : dictWallet) {
			// for (WalletOutstandingDTO lW : lstWallet) {
			// if(!dW.getMobile().equals(lW.getMobile())){
			// System.out.println("LstMob : : " + dW.getMobile() + " | " +
			// lW.getMobile());
			//// System.out.println("Other : : : " + lW.getMobile() + " | " +
			// lW.getBalance());
			// mainWallet.add(lW);
			// }
			// }
			// }

			for (WalletOutstandingDTO lW : lstWallet) {
				// for (WalletOutstandingDTO dW : dictWallet) {
				// System.out.println("LstMob : : " + lW.getMobile() + " | " +
				// dW.getMobile());
				// }
				if (!dictWallet.isEmpty()) {
					if (!lW.getMobile().equals(dictWallet.get(0).getMobile())) {
						// System.out.println("Other : : : " + lW.getMobile() +
						// " | " + lW.getBalance());
						mainWallet.add(lW);
					}
				}
			}

			// dictWallet =
			// lstWallet.stream().distinct().collect(Collectors.toList());
			result = new PageImpl<WalletOutstandingDTO>(mainWallet, pageable, mainWallet.size());
			// results = new
			// PageImpl<WalletOutstandingDTO>((List<WalletOutstandingDTO>)
			// hsWwllet, pageable, hsWwllet.size());
			// hsWwllet.addAll(lstWallet);
			for (WalletOutstandingDTO w : result) {
				System.out.println("Wallet Oustanding Username and Email :: : : :  " + w.getDate() + " | " + w.getName()
						+ " | " + w.getEmail() + " | " + w.getMobile() + " | " + w.getBalance());
			}
			System.out.println("No of Element : :  " + result.getNumberOfElements());
			return result;
		} catch (ParseException e) {
			e.printStackTrace();
			return result;
		}
	}

	@Override
	public GetDailyAmountDTO getDailyValuesByDate(Date newDate) {
		User instantpay = findByUserName("instantpay@tripay.in");
		User commission = findByUserName("commission@tripay.in");

		double currCCAvenue = 0;
		double currBillDesk = 0;
		double currBankTxn = 0;

		double totalCurLoadMoneyNow = 0;
		double curBillLoadMoneyNow = 0;
		double curCCAvenueLoadMoneyNow = 0;
		long curCountBillLoadMoneyNow = 0;
		long curCountCCAvenueLoadMoneyNow = 0;
		long totalCurLoadMoneyCountNow = 0;

		double currTotalTopupNow = 0;
		long currTotalCountNow = 0;
		double currComTotalNow = 0;
		long topupComTotalCountNow = 0;
		double curBankTranferNow = 0;
		double curPromoCode = 0;
		double curMerchantRefund = 0;
		long curCountBankTranferNow = 0;
		long curCountPromoCode = 0;
		long curCountMerchantRefund = 0;
		double poolBalance = 0;
		double totalCCAvenue = 0.0;
		double totalBillDesk = 0;
		double totalLoadAmount = 0;
		double wallteOpBalance = 0;
		double totalSendMoneyCredit = 0;
		double totalSendMoneyDebit = 0;
		long countSendMoneyCredit = 0;
		long countSendMoneyDebit = 0;

		double totalMerchantAmout = 0;
		long countMerchantTxn = 0;

		User bank = findByUserName(StartupUtil.BANK);
		PQService loadMoneyService = pqServiceRepository.findServiceByCode("LMC");
		PQService billDeskService = pqServiceRepository.findServiceByCode("LMB");

		PQService smuService = pqServiceRepository.findServiceByCode("SMU");
		PQService smrService = pqServiceRepository.findServiceByCode("SMR");

		Date toDate = null;
		try {
			toDate = formatter.parse(ConvertUtil.converCurToPastDate(newDate));
			double curTxnBal = 0;
			List<Date> d = pqTransactionRepository.getLastDateOfLoadMoney();
			Date lastDate = d.get(0);

			List<Date> beginDate = pqTransactionRepository.getBeginDateOfAllTxns();
			lastDate = beginDate.get(0);

			WalletDetails wd = walletDetailRepository.getWalletDetailsByDate(toDate);
			if (wd != null) {
				curTxnBal = wd.getBalance();
			} else {
				List<PQAccountDetail> listAcc = pqTransactionRepository.getTotalTxnsAccount(lastDate, toDate);
				for (PQAccountDetail p : listAcc) {
					List<User> users = userRepository.getTotalUsers(UserType.User);
					for (User u : users) {
						List<PQTransaction> listTxn = pqTransactionRepository.getTotalTxnsByStatusAndAccountId(p,
								lastDate, toDate);
						if (listTxn.get(listTxn.size() - 1).getAccount().getId() == u.getAccountDetail().getId()) {
							curTxnBal = curTxnBal + listTxn.get(listTxn.size() - 1).getCurrentBalance();
						}
					}
				}
			}

			wallteOpBalance = /* totalLoadAmount + */curTxnBal;

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			totalSendMoneyCredit = pqTransactionRepository.getTotalSMUCreditByDate(newDate, smuService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			totalSendMoneyCredit = totalSendMoneyCredit
					+ pqTransactionRepository.getTotalSMRCreditByDate(newDate, smrService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			totalSendMoneyDebit = pqTransactionRepository.getTotalSMUDebitByDate(newDate, smuService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			totalSendMoneyDebit = totalSendMoneyDebit
					+ pqTransactionRepository.getTotalSMRDebitByDate(newDate, smrService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			countSendMoneyCredit = pqTransactionRepository.getCountSMUCreditByDate(newDate, smuService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			countSendMoneyCredit = countSendMoneyCredit
					+ pqTransactionRepository.getCountSMRCreditByDate(newDate, smrService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			countSendMoneyDebit = pqTransactionRepository.getCountSMUDebitByDate(newDate, smuService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			countSendMoneyDebit = countSendMoneyDebit
					+ pqTransactionRepository.getCountSMRDebitByDate(newDate, smrService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			Double totalCCAvenue1 = pqTransactionRepository.getValidAmountByService(loadMoneyService);
			if (totalCCAvenue1 != null) {
				totalCCAvenue = totalCCAvenue1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Double totalBillDesk1 = pqTransactionRepository.getValidAmountByService(billDeskService);
			if (totalBillDesk1 != null) {
				totalBillDesk = totalBillDesk1;
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}

		// double commissionAmount = Double.parseDouble(String.format("%.2f",
		// commission.getAccountDetail().getBalance()));
		// double totalPayable = Double.parseDouble(String.format("%.2f",
		// instantpay.getAccountDetail().getBalance()));
		// double bankAmount = bank.getAccountDetail().getBalance();

		// double wallteOpBalance = (totalCCAvenue + totalBillDesk) -
		// (commissionAmount + totalPayable + bankAmount);

		try {
			currCCAvenue = pqTransactionRepository.getValidAmountByService(loadMoneyService, newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			currBillDesk = pqTransactionRepository.getValidAmountByService(billDeskService, newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		PQService bankService = pqServiceRepository.findServiceByCode("SMB");
		try {
			currBankTxn = pqTransactionRepository.getValidDebitAmountByService(bankService, newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		PQService bankTransferService = pqServiceRepository.findServiceByCode("SMB");
		PQService promoService = pqServiceRepository.findServiceByCode("PPS");

		try {
			currTotalTopupNow = pqTransactionRepository.getValidTopupAmountForDate(instantpay.getAccountDetail(),
					newDate);
			currComTotalNow = pqTransactionRepository.getValidTopupAmountForDate(commission.getAccountDetail(),
					newDate);

			// currTotalTopupNow =
			// pqTransactionRepository.getValidTopupAmountByDateAndServicdes(newDate,
			// bankTransferService, smuService, smrService);

			System.err.println("TopupCom :: : " + currComTotalNow);
			System.err.println(currTotalTopupNow + currComTotalNow);
		} catch (Exception e) {
			e.printStackTrace();

		}

		try {
			currTotalCountNow = pqTransactionRepository.getCountValidTopupAmountForDate(instantpay.getAccountDetail(),
					newDate);
			// currTotalCountNow =
			// pqTransactionRepository.getCountAmountByDateAndServicdes(newDate,
			// bankTransferService, smuService, smrService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curCCAvenueLoadMoneyNow = pqTransactionRepository.getValidAmountByServiceForDate(loadMoneyService, newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curCountCCAvenueLoadMoneyNow = pqTransactionRepository.getValidCountByServiceForDate(loadMoneyService,
					newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curBillLoadMoneyNow = pqTransactionRepository.getValidAmountByServiceForDate(billDeskService, newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curCountBillLoadMoneyNow = pqTransactionRepository.getValidCountByServiceForDate(billDeskService, newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curBankTranferNow = pqTransactionRepository.getTotalBankAmountForDate(bank.getAccountDetail(), newDate);
			// curBankTranferNow =
			// pqTransactionRepository.getTotalAmountBankTransfer(newDate,
			// bankTransferService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curPromoCode = pqTransactionRepository.getTotalAmountPromoCodes(newDate, promoService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			// curMerchantRefund =
			// pqTransactionRepository.getTotalAmountMerchantRefunds(newDate,
			// bankTransferService);
			// curMerchantRefund =
			// pqTransactionRepository.getTotalAmountMerchantRefunds(newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curCountBankTranferNow = pqTransactionRepository.getCountValidTopupAmountForDate(bank.getAccountDetail(),
					newDate);
			// curCountBankTranferNow =
			// pqTransactionRepository.getCountBankTransfer(newDate,
			// bankTransferService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			curCountPromoCode = pqTransactionRepository.getCountPromoCodes(newDate, promoService);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			// curCountMerchantRefund =
			// pqTransactionRepository.getCountMerchantRefunds(newDate,
			// bankTransferService);
			// curCountMerchantRefund =
			// pqTransactionRepository.getCountMerchantRefunds(newDate);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			List<User> merchant = userRepository.findAllMerchants();
			if (merchant != null) {
				for (User m : merchant) {
					totalMerchantAmout = totalMerchantAmout
							+ pqTransactionRepository.getTotalMerchatAmountForDate(m.getAccountDetail(), newDate);
					countMerchantTxn = countMerchantTxn
							+ pqTransactionRepository.getCountMerchatAmountForDate(m.getAccountDetail(), newDate);
				}

			}
		} catch (Exception e) {
		}

		System.err.println("Currdate ----------    " + newDate);
		System.err.println("topupTotalNow :: " + currTotalTopupNow);
		System.err.println("topupTotalCountNow :: " + currTotalCountNow);
		System.err.println("totalBankTranferNow :: " + curBankTranferNow);
		System.err.println("totalPromoCode :: " + curPromoCode);
		System.err.println("totalMerchantRefund :: " + curMerchantRefund);
		System.err.println("totalCountBankTranferNow :: " + curCountBankTranferNow);
		System.err.println("totalCountPromoCode :: " + curCountPromoCode);
		System.err.println("totalCountMerchantRefund :: " + curCountMerchantRefund);
		System.err.println("Total Credit SM :: " + totalSendMoneyCredit);
		System.err.println("Total Debit SM :: " + totalSendMoneyDebit);
		System.err.println("totalMerchantAmout :: " + totalMerchantAmout);
		System.err.println("countMerchantTxn :: " + countMerchantTxn);

		totalCurLoadMoneyNow = curCCAvenueLoadMoneyNow + curBillLoadMoneyNow;

		// totalLoadMoneyNow = totalLoadMoneyNow - (totalPromoCode +
		// totalMerchantRefund);
		totalCurLoadMoneyCountNow = curCountCCAvenueLoadMoneyNow + curCountBillLoadMoneyNow;
		try {
			poolBalance = yesBankPoolBalRepository.getBalanceByDate(newDate).getBalance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// double currWallteOpBalance = (currCCAvenue + currBillDesk) -
		// (currComTotalNow + currTotalTopupNow + currBankTxn);
		// wallteOpBalance = wallteOpBalance - currWallteOpBalance;

		GetDailyAmountDTO amountDTO = new GetDailyAmountDTO();
		amountDTO.setTotalDailyLoadMoneyAmount(totalCurLoadMoneyNow);
		amountDTO.setTotalDailyLoadMoneyCount(totalCurLoadMoneyCountNow);
		amountDTO.setTotalDailyPayoutAmount(currTotalTopupNow + currComTotalNow + totalMerchantAmout);
		amountDTO.setTotalDailyPayoutCount(currTotalCountNow + countMerchantTxn);
		amountDTO.setPoolBalance(poolBalance);
		amountDTO.setWalletBalance(wallteOpBalance);
		amountDTO.setTotalBankTranferNow(curBankTranferNow);
		amountDTO.setTotalCountBankTranferNow(curCountBankTranferNow);
		amountDTO.setTotalCountPromoCode(curCountPromoCode);
		amountDTO.setTotalPromoCode(curPromoCode);
		amountDTO.setTotalCountMerchantRefund(curCountMerchantRefund);
		amountDTO.setTotalMerchantRefund(curMerchantRefund);
		amountDTO.setCountSendMoneyCredit(countSendMoneyCredit);
		amountDTO.setTotalSendMoneyCredit(totalSendMoneyCredit);
		amountDTO.setCountSendMoneyDebit(countSendMoneyDebit);
		amountDTO.setTotalSendMoneyDebit(totalSendMoneyDebit);
		System.err.println("Total Load Amount ********** ********************* **************************      "
				+ totalLoadAmount);
		return amountDTO;
	}

	@Override
	public MISReportDTO getMISReport() {
		MISReportDTO dto = new MISReportDTO();

		// User instantpay = findByUserName(StartupUtil.INSTANTPAY);
		User commission = findByUserName("commission@tripay.in");
		User bank = findByUserName(StartupUtil.BANK);
		Calendar fromCal = Calendar.getInstance();
		Date from = new Date(fromCal.getTimeInMillis());

		// PQAccountDetail instPayAcc = instantpay.getAccountDetail();
		PQAccountDetail bankAcc = bank.getAccountDetail();

		Calendar cal = Calendar.getInstance();
		// cal.add(Calendar.DATE, -7);
		Date to = cal.getTime();

		List<Object> loadMoney = new ArrayList<Object>();
		List<Object> spent = new ArrayList<Object>();
		List<Object> closing = new ArrayList<Object>();
		List<Object> created = new ArrayList<Object>();
		List<Object> wallet = new ArrayList<Object>();

		for (int i = 0; i < 7; i++) {
			double totalLoadMoney = 0;
			double totalSpent = 0;
			double totalCCAvenue = 0;
			double totalbillDesk = 0;
			double totalInstantPay = 0;
			double totalBank = 0;
			long totalWallet = 0;
			long totalCreated = 0;
			double totalColsing = 0;
			cal.add(Calendar.DATE, -(1));
			Date date = cal.getTime();
			Date today = null;
			Date frm = null;
			try {
				today = formatter.parse(ConvertUtil.converCurToNextDate(date));
				frm = formatter.parse(ConvertUtil.converCurToPastDate(date));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

			PQAccountType KYC = pqAccountTypeRepository.findByCode("KYC");
			PQAccountType NONKYC = pqAccountTypeRepository.findByCode("NONKYC");

			List<PQAccountDetail> allUserAcc = pqAccountDetailRepository.getAllAccountTillDate(date, KYC, NONKYC);
			if (allUserAcc.size() != 0) {

				totalCreated = pqAccountDetailRepository.getTotalCreated(date, KYC, NONKYC);
				totalWallet = allUserAcc.size();
			}

			WalletDetails wDetails = walletDetailRepository.getWalletDetailsByDate(date);
			if (wDetails != null) {
				totalColsing = wDetails.getBalance();
				totalLoadMoney = wDetails.getCredit();
				totalSpent = wDetails.getDebit();
			}

			System.err.println("totalLoad= " + totalLoadMoney);
			System.err.println("totalSpent= " + totalSpent);

			System.err.println("Date : : : : " + today);
			loadMoney.add(i, totalLoadMoney);
			spent.add(i, totalSpent);
			closing.add(i, totalColsing);
			created.add(i, totalCreated);
			wallet.add(i, totalWallet);
		}
		dto.setClosing(closing);
		dto.setCreated(created);
		dto.setLoadMoney(loadMoney);
		dto.setSpent(spent);
		dto.setWallet(wallet);
		return dto;
	}

	@Override
	public WalletLoadRepot getWalletLoadReport(Date date, Date endDate) {

		WalletLoadRepot dto = new WalletLoadRepot();

		// User instantpay = findByUserName(StartupUtil.INSTANTPAY);
		User commission = findByUserName("commission@vpajyQwik.in");
		User bank = findByUserName(StartupUtil.BANK);
		Calendar fromCal = Calendar.getInstance();
		Date from = new Date(fromCal.getTimeInMillis());
		SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		dateFilter.format(date);
		String fromDateTime = dateFilter.format(endDate);
		String a[] = fromDateTime.split(" ");
		a[1] = " 23:59";
		fromDateTime = a[0] + a[1];
		/* Calendar cal = Calendar.getInstance(); */

		Date date1 = null;

		try {
			date1 = dateFilter.parse(fromDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		PQService loadMoneyService = pqServiceRepository.findServiceByCode("LMC");

		PQService billDeskService = pqServiceRepository.findServiceByCode("LMB");
		List<PQTransaction> billdesk = pqTransactionRepository.getValidAmountByServiceForMIS(billDeskService, date);

		List<User> user = userRepository.getTotalUsers(UserType.User);

		List<PQTransaction> loadMoney = pqTransactionRepository.getallLoadMoney(date, date1, "LMC", "LMB");

		System.err.println(loadMoney);

		dto.setCcAvenueLoadMoney(loadMoney);
		dto.setUserList(user);

		return dto;

	}

	@Override
	public ResponseDTO saveGCMIDs(SendNotificationDTO dto) {
		ResponseDTO response = new ResponseDTO();
		SendNotification notification = new SendNotification();
		notification.setMessage(dto.getMessage());
		notification.setTitle(dto.getTitle());
		notification.setImage(dto.getImage());
		notification.setStatus(Status.Open);
		sendNotificationRepository.save(notification);
		response.setStatus(ResponseStatus.SUCCESS);
		response.setMessage("Notification Saved Successfully");
		return response;
	}

	@Override
	public GetValueDTO getBescomDashValue(User user) {
		// TODO Auto-generated method stub
		GetValueDTO dto = new GetValueDTO();
		double poolBalance = 0;
		long totalTxns = 0;
		long totalSuccessTxns = 0;
		long totalFailedTxns = 0;

		try {
			poolBalance = user.getAccountDetail().getBalance();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			totalTxns = pqTransactionRepository.getCountTxnsByAccount(user.getAccountDetail());
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			totalSuccessTxns = pqTransactionRepository.getCountTxnsByAccountAndStatus(user.getAccountDetail(),
					Status.Success);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			totalFailedTxns = pqTransactionRepository.getCountTxnsByAccountAndStatus(user.getAccountDetail(),
					Status.Failed);
		} catch (Exception e) {
			e.printStackTrace();
		}
		dto.setPoolBalance(poolBalance);
		dto.setTotalFailedTxns(totalFailedTxns);
		dto.setTotalSuccessTxns(totalSuccessTxns);
		dto.setTotalTxns(totalTxns);
		return dto;
	}

	@Override
	public List<TListDTO> getWooHooTransactionsBetween(Date startDate, Date endDate) {
		List<TListDTO> listDTOs = new ArrayList<>();
		String authority = Authorities.LOCKED + "," + Authorities.AUTHENTICATED;
		try {
			List<PQAccountDetail> accounts = userRepository.getAccountsByAuthority(authority);
			List<PQTransaction> transactionList = pqTransactionRepository.getTransactionBetween(TransactionType.DEFAULT,
					accounts, startDate, endDate);
			List<PQAccountDetail> accountList = ConvertUtil.getAccounts(transactionList);
			List<User> users = userRepository.getUsersByAccount(accountList);
			listDTOs = ConvertUtil.getListDTOs(users, transactionList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDTOs;
	}

	@Override
	public ResponseLetsDTO sendbulkmail(String key, BulkMailRequestDTO dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976"))
				System.err.println(dto.getUserType());
			System.err.println(UserType.User.toString());
			if (dto.getUserType().equalsIgnoreCase(UserType.User.toString())) {

				List<String> mailList = userRepository.getMailsOfActiveUser(UserType.User);
				System.err.println(mailList);
				if (mailList != null && mailList.size() > 0) {
					for (String mail : mailList) {
						sendSingleMail(mail, dto.getContent(), dto.getSubject());
					}
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!mail sent");
					result.setDetails("woooo!!!mail sent");
				}

			}

			else if (dto.getUserType().equalsIgnoreCase(UserType.SuperAdmin.toString())) {
				{
					List<String> mailList = userRepository.getMailsOfActiveUser(UserType.SuperAdmin);
					if (mailList != null && mailList.size() > 0) {
						for (String mail : mailList) {
							sendSingleMail(mail, dto.getContent(), dto.getSubject());
						}
						result.setStatus(ResponseStatusLetsMan.SUCCESS);
						result.setMessage("woooo!!!mail sent");
						result.setDetails("woooo!!!mail sent");
					}

				}

			} else if (dto.getUserType().equalsIgnoreCase(UserType.User.toString())) {
				List<String> mailList = userRepository.getMailsOfActiveUser(UserType.Admin);
				if (mailList != null && mailList.size() > 0) {
					for (String mail : mailList) {
						sendSingleMail(mail, dto.getContent(), dto.getSubject());
					}
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!mail sent");
					result.setDetails("woooo!!!mail sent");
				}

			}

			else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO sendmail(String key, MailRequestDTO dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				List<User> users = userRepository.getByMail(dto.getDestination());
				System.err.println(users);
				if (users != null && !users.isEmpty()) {
					for (User user : users) {
						mailSenderApi.sendNoReplyMail(dto.getSubject(), MailTemplate.ADMIN_TEMPLATE, user,
								dto.getContent());
					}
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!mail sent");
					result.setDetails("woooo!!!mail sent");
				} else {
					result.setCode("S01");
					result.setStatus(ResponseStatusLetsMan.FAILURE);
					result.setMessage("user Does Not Exist");
					result.setDetails("OOOPS!!!! email  add nahi mila");
				}
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO sendbulksms(String key, BulkSmsRequest dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				System.err.println(dto.getUserType());
				if (dto.getUserType().equalsIgnoreCase(UserType.User.toString())) {

					List<String> mobileList = userRepository.getAllActiveUsername(UserType.User);
					for (String mobile : mobileList) {
						sendSingleSMS(mobile, dto.getContent());
					}
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!sms sent");
					result.setDetails("woooo!!!sms sent");
				} else if (dto.getUserType().equalsIgnoreCase(UserType.SuperAdmin.toString())) {
					{
						List<String> mobileList = userRepository.getAllActiveUsername(UserType.SuperAdmin);
						for (String mobile : mobileList) {
							sendSingleSMS(mobile, dto.getContent());
						}
						result.setCode("S00");
						result.setStatus(ResponseStatusLetsMan.SUCCESS);
						result.setMessage("woooo!!!sms sent");
						result.setDetails("woooo!!!sms sent");
					}

				}

				else if (dto.getUserType().equalsIgnoreCase(UserType.Admin.toString())) {
					List<String> mobileList = userRepository.getAllActiveUsername(UserType.Admin);
					for (String mobile : mobileList) {
						sendSingleSMS(mobile, dto.getContent());
					}
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!sms sent");
					result.setDetails("woooo!!!sms sent");
				}

			}

			else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseLetsDTO sendsms(String key, SmsRequest dto) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		try {
			if (key.equals("5uChIt4TiW4R11510731296976")) {
				User user = userRepository.findByContactNo(dto.getMobile());
				if (user != null) {
					smsSenderApi.sendPromotionalSMS(SMSAccount.PAYQWIK_PROMOTIONAL, SMSTemplate.ADMIN_TEMPLATE, user,
							dto.getContent());
					result.setCode("S00");
					result.setStatus(ResponseStatusLetsMan.SUCCESS);
					result.setMessage("woooo!!!sms sent");
					result.setDetails("woooo!!!sms sent");
				}

				else {
					result.setCode("S01");
					result.setStatus(ResponseStatusLetsMan.FAILURE);
					result.setMessage("user Does Not Exist");
					result.setDetails("OOOPS!!!! contact does not exist");
				}
			} else {
				result.setCode("F00");
				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
				result.setMessage("Key does not mATCH");
			}
		} catch (Exception e) {
			result.setCode("F02");
			result.setStatus(ResponseStatusLetsMan.INTERNAL_SERVER_ERROR);
			result.setMessage("Service unavailable");
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public ResponseDTO saveAadhar(AadharDTO dto) {
		ResponseDTO result = new ResponseDTO();
		User user = userRepository.findByUsername(dto.getUsername());
		if (user != null) {
			UserDetail u = user.getUserDetail();
			if (u.getAadharNumber() == null) {
				String aadharNumber = SecurityUtil.encryptString(dto.getAadharNumber());
				u.setAadharNumber(aadharNumber);
				userDetailRepository.save(u);
				AadharDetails details = aadharDetailsRepository.findByAadharNumber(dto.getAadharNumber());
				if (details == null) {
					logger.info("inside aadhar details");
					details = new AadharDetails();
					details.setAadharName(dto.getCustomerName());
					details.setAadharAddress(dto.getCustomerAddress());
					details.setAadharNumber(dto.getAadharNumber());
					details.setUser(user);
					aadharDetailsRepository.save(details);
				}
				if (updateAccountType(u.getContactNo(), true)) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Your KYC updated successfully.");
					return result;
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("You have already a KYC");
				return result;
			}
		}
		return result;
	}

	@Override
	public void otpForAadhar(User user) {
		String otp = CommonUtil.generateNDigitNumericString(6);
		user.setMobileToken(otp);
		userRepository.save(user);
		smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
	}

	@Override
	public MerchantTxnStatusResponse bescomUpiStatus(UpiRequest request) {
		MerchantTxnStatusResponse response = new MerchantTxnStatusResponse();
		try {
			System.out.println("Merchant VPA :: ::: ::: " + request.getMerchantVpa());
			System.out.println("After Validate Token Repsonse is --------- ---------------- --------------  " + response.getCode());

			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UPIBescomConstants.URL_UPI_STATUS).queryParam(
					UPIBescomConstants.API_KEY_MSG_ID, (request.getMsgId() == null) ? "" : request.getMsgId());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				response.setValid(true);
				response.setCode(ResponseStatus.SUCCESS.getValue());
				response.setReferenceNo(jobj.getString("referenceNo"));
				response.setStatus(jobj.getString("upiStatus"));
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST.getValue());
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	@Override
	public void createCsvForWeeklyBescomReport() throws Exception {
		SimpleDateFormat dFormate = new SimpleDateFormat("dd-MMMM-yy");
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, -7);
		 Date startDate = cal.getTime();
		 Date endDate = new Date();

//		String date = "2017-10-26";
//		String date35 = "2018-01-01";
//		String startDate = date + " 00:00";
//		String endDate = date35 + " 23:59";

//		System.err.println("Start Date : : " + startDate);
//		System.err.println("End Date : : " + endDate);

		 System.err.println("Start Date : : " + dateFormat.format(startDate));
		// System.err.println("End Date : : " + dateFormat.format(endDate));

		// TODO Reconciling Report for EBS
		String UPI_FILE_HEADER = "Transaction Date,Transaction ID,Description,Amount,Status,FSS Payment ID,FSS Status";
		String bescomReport = "BESCOM_UPI_Rural_Recon_from_" + dateForm.format(startDate) + "_to_" + dateForm.format(endDate) + ".csv";
//		String bescomReport = "BESCOM_UPI_Rural_Recon_from_" + date + "_to_" + date35 + ".csv";
		FileWriter upiBescom_Writer = new FileWriter(StartupUtil.FILE_PATH + bescomReport);
		upiBescom_Writer.append(UPI_FILE_HEADER);
		try {
			 List<PQTransaction> upiTxns = getAllVijayaUPITxns(startDate, endDate);
//			List<PQTransaction> upiTxns = getAllBescomTxns(dateFormat.parse(startDate), dateFormat.parse(endDate),
//					UPIBescomConstants.BECOM_RURAL_MERCHANT);
			System.out.println(upiTxns.size());
			if (!upiTxns.isEmpty()) {
				for (PQTransaction t : upiTxns) {
					String txnRef = ConvertUtil.removeLastString(t.getTransactionRefNo());
					System.err.println("Txn Ref No : : " + txnRef);
					UpiRequest req = new UpiRequest();
					req.setMsgId(t.getUpiId());
					UpiResponse response = validateUpiMerchantTokenForMerchant(req);
					if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
						MerchantTxnStatusResponse result = bescomUpiStatus(req);
						System.err.println("EBS Status ::-----------------------------------------    ::: " + result.getStatus());
						System.err.println("EBS Paymet ID ::----------------------------------------- ::: "+ result.getReferenceNo());
						if (result.isValid()) {
							System.err.println("Wallet status : " + t.getStatus() + " | UPI status : " + result.getStatus());
							if (!t.getStatus().equals(result.getUpiStatus())) {
								System.err.println("Inside True");
								upiBescom_Writer.append(NEW_LINE_SEPARATOR);
								upiBescom_Writer.append(dFormate.format(t.getCreated()));
								upiBescom_Writer.append(COMMA_DELIMITIER);
								upiBescom_Writer.append(t.getTransactionRefNo());
								upiBescom_Writer.append(COMMA_DELIMITIER);
								upiBescom_Writer.append(t.getDescription());
								upiBescom_Writer.append(COMMA_DELIMITIER);
								upiBescom_Writer.append(decimalFormatter.format(t.getAmount()));
								upiBescom_Writer.append(COMMA_DELIMITIER);
								upiBescom_Writer.append(t.getStatus().getValue());
								upiBescom_Writer.append(COMMA_DELIMITIER);
								upiBescom_Writer.append(result.getReferenceNo());
								upiBescom_Writer.append(COMMA_DELIMITIER);
								upiBescom_Writer.append(result.getStatus());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.err.println("while loop closed .");
		upiBescom_Writer.flush();
		upiBescom_Writer.close();

		// TODO Mail Sender for Reconciling Report
		 mailSenderApi.weeklyBesconUPIReconReport(StartupUtil.FILE_PATH+bescomReport);
	}

	public UpiResponse converUPIstatusRequest(UpiRequest req) {
		UpiResponse resp = new UpiResponse();
		if (req.isSdk()) {
			resp = validateUpiSdkMerchantToken(req);
		} else {
			resp = validateUpiMerchantToken(req);
		}
		return resp;
	}

	public MerchantTxnStatusResponse upiStatus(UpiRequest request) {
		MerchantTxnStatusResponse response = new MerchantTxnStatusResponse();
		try {
			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UpiConstants.URL_UPI_STATUS)
					.queryParam(UpiConstants.API_KEY_MSG_ID, (request.getMsgId() == null) ? "" : request.getMsgId());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				response.setValid(true);
				response.setCode(jobj.getString("code"));
				response.setReferenceNo(jobj.getString("referenceNo"));
				response.setUpiStatus(jobj.getString("upiStatus"));
				response.setStatus(jobj.getString("status"));
				response.setMessage(jobj.getString("message"));
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST.getValue());
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	@Override
	public UpiResponse validateUpiMerchantToken(UpiRequest request) {
		// TODO Auto-generated method stub
		UpiResponse response = new UpiResponse();
		try {
			UpiMerchantDetail md = upiMerchantDetailRepository.getUpiMerchant(request.getMerchantVpa());
			if (md == null) {
				md = new UpiMerchantDetail();
				response = getMerchantResponse(request);
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					md.setExpTime(new Date());
					md.setMerchantVpa(request.getMerchantVpa());
					md.setMerchantToken(response.getMerchantToken());
					upiMerchantDetailRepository.save(md);
					response.setCode(ResponseStatus.SUCCESS);
					System.err.println("Merchant Details Saved First Time");
				}
			} else {
				Date date = md.getExpTime();
				if (date != null) {
					long lastTransactionTimeStamp = date.getTime();
					System.out.println("lastTransactionTimeStamp  ----  --------  " + lastTransactionTimeStamp);
					long currentTimeInMillis = System.currentTimeMillis();
					System.out.println("currentTimeInMillis  ----  --------  " + currentTimeInMillis);
					System.out.println("Diffe :: " + (currentTimeInMillis - lastTransactionTimeStamp));
					System.out.println(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp));
					if (!(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp))) {
						System.err.println("Time Expired...!! Call API");
						response = getMerchantResponse(request);
						if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							md.setExpTime(new Date());
							md.setMerchantVpa(request.getMerchantVpa());
							md.setMerchantToken(response.getMerchantToken());
							upiMerchantDetailRepository.save(md);
							response.setCode(ResponseStatus.SUCCESS);
						}
					} else {
						response.setCode(ResponseStatus.SUCCESS);
						response.setMerchantToken(md.getMerchantToken());
						System.err.println("You can use");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		System.err.println("Merchant Token thier : : " + response.getMerchantToken());
		return response;
	}

	@Override
	public UpiResponse validateUpiSdkMerchantToken(UpiRequest request) {
		// TODO Auto-generated method stub
		UpiResponse response = new UpiResponse();
		try {
			UpiSdkMerchantDetails md = upiSdkMerchantDetailsRepository.getUpiMerchant(request.getMerchantVpa());
			if (md == null) {
				md = new UpiSdkMerchantDetails();
				response = getMerchantResponse(request);
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					md.setExpTime(new Date());
					md.setMerchantVpa(request.getMerchantVpa());
					md.setMerchantToken(response.getMerchantToken());
					upiSdkMerchantDetailsRepository.save(md);
					response.setCode(ResponseStatus.SUCCESS);
					System.err.println("Merchant Details Saved First Time");
				}
			} else {
				Date date = md.getExpTime();
				if (date != null) {
					long lastTransactionTimeStamp = date.getTime();
					System.out.println("lastTransactionTimeStamp  ----  --------  " + lastTransactionTimeStamp);
					long currentTimeInMillis = System.currentTimeMillis();
					System.out.println("currentTimeInMillis  ----  --------  " + currentTimeInMillis);
					System.out.println("Diffe :: " + (currentTimeInMillis - lastTransactionTimeStamp));
					System.out.println(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp));
					if (!(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp))) {
						System.err.println("Time Expired...!! Call API");
						response = getMerchantResponse(request);
						if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							md.setExpTime(new Date());
							md.setMerchantVpa(request.getMerchantVpa());
							md.setMerchantToken(response.getMerchantToken());
							upiSdkMerchantDetailsRepository.save(md);
							response.setCode(ResponseStatus.SUCCESS);
						}
					} else {
						response.setCode(ResponseStatus.SUCCESS);
						response.setMerchantToken(md.getMerchantToken());
						System.err.println("You can use");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		System.err.println("Merchant Token thier : : " + response.getMerchantToken());
		return response;
	}

	UpiResponse getMerchantResponse(UpiRequest request) {
		UpiResponse response = new UpiResponse();
		try {
			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UpiConstants.URL_MERCHANT_TOKEN);
			ClientResponse clientResponse = resource.header("sdk", request.isSdk())
					.header("deviceId", request.getDeviceId()).get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				if (jobj.getString("code").equals(ResponseStatus.SUCCESS.getValue())) {
					response.setCode(ResponseStatus.SUCCESS);
					response.setMessage(jobj.getString("message"));
					response.setMerchantToken(jobj.getString("token"));
				} else {
					response.setCode(ResponseStatus.FAILURE);
					response.setMessage(jobj.getString("message"));
				}
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST);
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	@Override
	public UpiResponse validateUpiMerchantTokenForMerchant(UpiRequest request) {
		// TODO Auto-generated method stub
		UpiResponse response = new UpiResponse();
		try {
			UpiBscmMerTokenDetails md = upiBscmMerTokenDetailRepository.getUpiMerchant(request.getMerchantVpa());
			if (md == null) {
				md = new UpiBscmMerTokenDetails();
				response = getMerchantResponseMerchant();
				if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
					md.setExpTime(new Date());
					md.setMerchantVpa(request.getMerchantVpa());
					md.setMerchantToken(response.getMerchantToken());
					upiBscmMerTokenDetailRepository.save(md);
					response.setCode(ResponseStatus.SUCCESS);
					System.err.println("Merchant Details Saved First Time");
				}
			} else {
				Date date = md.getExpTime();
				if (date != null) {
					long lastTransactionTimeStamp = date.getTime();
					System.out.println("lastTransactionTimeStamp  ----  --------  " + lastTransactionTimeStamp);
					long currentTimeInMillis = System.currentTimeMillis();
					System.out.println("currentTimeInMillis  ----  --------  " + currentTimeInMillis);
					System.out.println("Diffe :: " + (currentTimeInMillis - lastTransactionTimeStamp));
					System.out.println(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp));
					if (!(1000 * 60 * 60 * 5 >= (currentTimeInMillis - lastTransactionTimeStamp))) {
						System.err.println("Time Expired...!! Call API");
						response = getMerchantResponseMerchant();
						if (response.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							md.setExpTime(new Date());
							md.setMerchantVpa(request.getMerchantVpa());
							md.setMerchantToken(response.getMerchantToken());
							upiBscmMerTokenDetailRepository.save(md);
							response.setCode(ResponseStatus.SUCCESS);
						}
					} else {
						response.setCode(ResponseStatus.SUCCESS);
						System.err.println("You can use");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	UpiResponse getMerchantResponseMerchant() {
		UpiResponse response = new UpiResponse();
		try {
			String stringResponse = "";
			Client colMoney = new Client();
			colMoney.addFilter(new LoggingFilter(System.out));
			WebResource resource = colMoney.resource(UPIBescomConstants.URL_MERCHANT_TOKEN);
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				JSONObject jobj = new JSONObject(stringResponse);
				if (jobj.getString("code").equals(ResponseStatus.SUCCESS.getValue())) {
					response.setCode(ResponseStatus.SUCCESS);
					response.setMessage(jobj.getString("message"));
					response.setMerchantToken(jobj.getString("token"));
				} else {
					response.setCode(ResponseStatus.FAILURE);
					response.setMessage(jobj.getString("message"));
				}
			} else {
				response.setCode(ResponseStatus.BAD_REQUEST);
				response.setMessage("Bad Request");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.INTERNAL_SERVER_ERROR);
			response.setMessage("Internal Server Error");
		}
		return response;
	}

	@Override
	public void uploadGCMNotification() {
		int page = 0;
		int size = 1000;
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(page, size, sort);
		List<GCMDto> gcmIds = new ArrayList<>();
		Page<String> gcmList = getGCMIDs(pageable);
		if (gcmList != null) {
			long totalPages = gcmList.getTotalPages();
			List<SendNotification> sendNotifications = sendNotificationRepository
					.findListNotificationByStatus(Status.Open);
			if (sendNotifications != null && !sendNotifications.isEmpty()) {
				for (SendNotification sendNotification : sendNotifications) {
					long count = 0;
					while (page <= totalPages) {
						List<String> pages = gcmList.getContent();
						for (int i = 0; i < pages.size(); i++) {
							GCMDto dto = new GCMDto();
							dto.setGcmId(pages.get(i));
							gcmIds.add(dto);
						}

						NotificationDTO notificationDTO = new NotificationDTO();
						notificationDTO.setImage(sendNotification.getImage());
						notificationDTO.setTitle(sendNotification.getTitle());
						notificationDTO.setMessage(sendNotification.getMessage());
						notificationDTO.setGcmIds(gcmIds);
						ResponseDTO resp = sendNotification(notificationDTO);
						gcmIds.clear();
						page += 1;
						count += resp.getCount();
						CommonUtil.sleep(5 * 1000);
						System.err.println("page ::" + page);
						sort = new Sort(Sort.Direction.DESC, "id");
						pageable = new PageRequest(page, size, sort);
						gcmList = getGCMIDs(pageable);
					}
					sendNotification.setDeliveredCount(count);
					updateGcmNotification(sendNotification);
				}
			} else {
				logger.info("There is no open notification.");
			}
		}

	}

	private void updateGcmNotification(SendNotification sendNotification) {
		sendNotification.setStatus(Status.Close);
		sendNotificationRepository.save(sendNotification);
	}

	@Override
	public ResponseDTO sendNotification(NotificationDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			String str = getNotificationJson(dto);
			System.out.println(str);
			WebResource webResource = client.resource(SEND_NOTIFICATION_URL);
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, str);
			String strResponse = response.getEntity(String.class);
			System.err.println("response of gcm ::" + strResponse);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			if (jobj != null) {
				if (jobj.getInt("success") >= 1) {
					resp.setCode("S00");
					resp.setValid(true);
					resp.setCount(jobj.getInt("success"));
					System.err.println("success gcm");
				} else {
					resp.setCode("F00");
					resp.setValid(false);
					System.err.println("failed gcm");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	public String getNotificationJson(NotificationDTO dto) {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		jsonBuilder.add("title", dto.getTitle() != null ? dto.getTitle() : "");
		jsonBuilder.add("message", dto.getMessage() != null ? dto.getMessage() : "");
		jsonBuilder.add("image", dto.getImage() != null ? dto.getImage() : "");
		jsonBuilder.add("imageGCM", dto.getImage() != null ? true : false);
		jsonBuilder.add("gcmIds", getGcmIdsJson(dto.getGcmIds()));
		JsonObject empObj = jsonBuilder.build();
		StringWriter jsnReqStr = new StringWriter();
		JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		jsonWtr.writeObject(empObj);
		jsonWtr.close();
		return jsnReqStr.toString();
	}

	private JsonArrayBuilder getGcmIdsJson(List<GCMDto> gcmIds) {
		if (gcmIds != null && !gcmIds.isEmpty()) {
			JsonArrayBuilder gcmIdsList = Json.createArrayBuilder();
			for (GCMDto gcmId : gcmIds) {
				JsonObjectBuilder productJson = Json.createObjectBuilder();
				productJson.add("gcmId", gcmId.getGcmId() != null ? gcmId.getGcmId() : "");
				gcmIdsList.add(productJson.build());
			}
			return gcmIdsList;
		}
		return null;
	}

	@Override
	public List<User> getUserByAccount(List<PQAccountDetail> accounts) {
		return userRepository.getUsersByAccount(accounts);
	}

	public void findActiveUserByPQTransaction() throws IOException {
		int page = 0;
		int size = 10000;
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";
		String FILE_HEADER = "mobileNumber";
		UserMonthlyBalanceDTO dto = new UserMonthlyBalanceDTO();
		FileWriter writer = new FileWriter(
				"D:\\OFFICE PROJECTS\\office project\\APP\\APP\\WebContent\\WEB-INF\\startup\\userReport06.csv");
		writer.append(FILE_HEADER);

		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(page, size, sort);
		Page<User> user = userRepository.findAuthenticateUser(pageable);
		while (page <= user.getTotalPages()) {
			for (User u : user) {
				List<PQTransaction> pqacc = pqTransactionRepository.findUserByAccId(u.getAccountDetail());
				if (pqacc.size() > 0) {
					// count++;
					dto.setMobileNo(u.getUsername());
					writer.append(NEW_LINE_SEPARATOR);
					writer.append(dto.getMobileNo());
					writer.append(COMMA_DELIMITIER);

				}
			}
			page += 1;
			// System.out.println("findActiveUserByPQTransaction count=" +
			// count);
			sort = new Sort(Sort.Direction.DESC, "id");
			pageable = new PageRequest(page, size, sort);
			user = userRepository.findAuthenticateUser(pageable);
		}
		System.err.println("the while loop closed .");
	}

	@Override
	public void updatePasswordHistory() {
		int page = 0;
		int size = 10000;
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(page, size, sort);
		Page<User> user = userRepository.findAuthenticateUser(pageable);
		while (page <= user.getTotalPages()) {
			for (User u : user) {
				PasswordHistory password = passwordHistoryRepository.findPasswordNull(u);
				if (password != null) {
					if (password.getPassword() == null || password.getPassword().isEmpty()) {
						password.setPassword(u.getPassword());
						passwordHistoryRepository.save(password);
					} else {
						System.out.println("Password field is not null");
					}
				}
			}
			page += 1;
			sort = new Sort(Sort.Direction.DESC, "id");
			pageable = new PageRequest(page, size, sort);
			user = userRepository.findAuthenticateUser(pageable);
		}
	}

	@Override
	public void updateRequestLog() {
		List<RequestLog> requestLog = requestLogRepository.findProcessingTransactions(Status.Processing);
		if (requestLog != null) {
			for (RequestLog log : requestLog) {
				log.setStatus(Status.Completed);
				requestLogRepository.save(log);
			}
		}
	}

	@Override
	public List<RefundDTO> getAllMerchantRefundRequest() {
		List<MerchantRefundRequest> request = merchantRefundRequestRepository.findAllRefundRequest();
		if (request != null && request.size() != 0) {
			List<RefundDTO> plansDTOs = ConvertUtil.convertRefundRequest(request);
			return plansDTOs;
		}
		return null;
	}

	@Override
	public void password() {
		String password = "7906415598";
		System.err.println("password ::" + passwordEncoder.encode(password));
	}

	@Override
	public List<OffersDTO> getAllOffers() {
		List<Offers> request = offersRepository.findAllOffers();
		if (request != null && request.size() != 0) {
			List<OffersDTO> offerDTOs = ConvertUtil.convertOfferRequest(request);
			return offerDTOs;
		}
		return null;
	}

	@Override
	public Offers AddOffers(OffersDTO dto) {
		Offers offer = new Offers();
		offer.setActiveOffer(Status.Active);
		offer.setOffers(dto.getOffers());
		if (dto.getServices().equalsIgnoreCase("Prepaid") || dto.getServices().equalsIgnoreCase("Postpaid")
				|| dto.getServices().equalsIgnoreCase("Datacard")) {
			offer.setServices("Topup-" + dto.getServices());
		} else if (dto.getServices().equalsIgnoreCase("Electricity") || dto.getServices().equalsIgnoreCase("Landline")
				|| dto.getServices().equalsIgnoreCase("Dth") || dto.getServices().equalsIgnoreCase("Insurance")
				|| dto.getServices().equalsIgnoreCase("Gas")) {
			offer.setServices("BillPayment-" + dto.getServices());
		} else {
			offer.setServices(dto.getServices());
		}
		offersRepository.save(offer);
		return offer;
	}

	@Override
	public boolean checkUserExist(String username) {
		User user = userRepository.findByUsername(username);
		if (user != null) {
			String otp = CommonUtil.generateNDigitNumericString(6);
			user.setMobileToken(otp);
			userRepository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
			return true;
		}
		return false;
	}

	@Override
	public List<NearAgentsDetailsDTO> nearByAgentDetails(NearByAgentsDTO dto) {
		List<NearAgentsDetailsDTO> agentDTO = new ArrayList<>();
		List<User> agentList = userRepository.findAllAgent();
		if (agentList != null && !agentList.isEmpty()) {
			for (User user : agentList) {
				UserDetail agentDetails = user.getUserDetail();
				if (agentDetails.getLatitude() != null && agentDetails.getLongitude() != null) {
					dto.setAgentName(agentDetails.getFirstName() + " " + agentDetails.getLastName());
					boolean validateDistance = validateDistance(dto, agentDetails, dto.getRadius());
					if (validateDistance) {
						NearAgentsDetailsDTO nearByAgent = new NearAgentsDetailsDTO();
						nearByAgent.setAgentName(dto.getAgentName());
						nearByAgent.setLatitude(dto.getLatitude());
						nearByAgent.setLongitude(dto.getLongitude());
						nearByAgent.setAddress(agentDetails.getAddress());
						agentDTO.add(nearByAgent);
					}
				}
			}
		}
		return agentDTO;
	}

	public boolean validateDistance(NearByAgentsDTO user, UserDetail agent, double distance) {
		boolean valid = false;
		double distanceBW = CommonUtil.calculateDistance(user.getLatitude(), user.getLongitude(),
				Double.parseDouble(agent.getLatitude()), Double.parseDouble(agent.getLongitude()));
		System.err.println("distance bw ::" + distanceBW);
		System.err.println("distance ::" + distance);
		if (distanceBW <= distance) {
			valid = true;
		}
		return valid;
	}

	@Override
	public void createCsvForMonthlyTransactionReport() {
		YearMonth thisMonth = YearMonth.now();
		YearMonth lastMonth = thisMonth.minusMonths(1);
		DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMMM yyyy");
		UserMonthlyBalanceDTO dto = new UserMonthlyBalanceDTO();
		dto.setDate(lastMonth.format(monthYearFormatter));
		Date[] dates = getDateArray();
		int page = 0;
		int size = 10000;
		Pageable pageable = new PageRequest(page, size);
		Page<User> user = userRepository.findAuthenticateUser(pageable);
		List<PQService> creditService = pqServiceRepository.findServicesByCodes();
		while (page <= user.getTotalPages()) {
			for (User u : user) {
//				if (isValidUser(u.getUsername())) {
					try {
						dto.setEmail(u.getUserDetail().getEmail());
						dto.setMobileNo(u.getUsername());
						Double openingBalance = null, closingBalance = null, totalCredit = null, totalDebit = null;
						List<PQTransaction> txnList = pqTransactionRepository.getOpeningBalance(u.getAccountDetail(),
								dates[0], dates[1]);
						PQTransaction txnDetails = null;
						if (txnList != null && txnList.size() > 0) {
							txnDetails = txnList.get(0);
							if (txnDetails.isDebit()) {
								openingBalance = txnDetails.getAmount() + txnDetails.getCurrentBalance();
							} else if (isCredit(txnDetails)) {
								openingBalance = txnDetails.getCurrentBalance() - txnDetails.getAmount();
							}
							closingBalance = txnList.get(txnList.size() - 1).getCurrentBalance();
							totalDebit = pqTransactionRepository.getTotalDebitAmount(u.getAccountDetail(), dates[0],
									dates[1]);
							totalCredit = pqTransactionRepository.getTotalCreditAmount(u.getAccountDetail(), dates[0],
									dates[1], creditService);
							dto.setOpeningBalance(openingBalance != null ?  Double.parseDouble(String.format("%.2f", openingBalance)) : 0);
							dto.setClosingBalance(closingBalance != null ?  Double.parseDouble(String.format("%.2f", closingBalance)) : 0);
							dto.setDebitBalance(totalDebit != null ? Double.parseDouble(String.format("%.2f", totalDebit)) : 0);
							dto.setCreditBalance(totalCredit != null ? Double.parseDouble(String.format("%.2f", totalCredit)) : 0);
							mailSenderApi.sendMonthlyTransactionTesting("VPayQwik Cash Summary for " + dto.getDate(),
									MailTemplate.VERIFICATION_EMAIL_FOR_MONTHLY_TRANSACTION, dto, u, null);
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.getMessage());
					}
//				}
			}
			page++;
			System.err.println("page ::" + page);
			pageable = new PageRequest(page, size);
			user = userRepository.findAuthenticateUser(pageable);
		}
	}

	private boolean isValidUser(String string) {
		String[] users = getUserNames();
		for (int i = 0; i < users.length; i++) {
			if (users[i].equals(string))
				return false;
		}
		return true;
	}

	private static Date[] getDateArray() {
		Date[] dates = new Date[2];
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, -1);
		aCalendar.set(Calendar.DATE, 1);
		Calendar cal = setTimeToBeginningOfDay(aCalendar);
		Date firstDateOfPreviousMonth = cal.getTime();
		aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Calendar cal1 = setTimeToEndofDay(aCalendar);
		Date lastDateOfPreviousMonth = cal1.getTime();
		dates[0] = firstDateOfPreviousMonth;
		dates[1] = lastDateOfPreviousMonth;
		return dates;
	}

	private static Calendar setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	private static Calendar setTimeToEndofDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar;
	}

	private boolean isCredit(PQTransaction txnDetails) {
		if (!txnDetails.isDebit() && (("LMC".equalsIgnoreCase(txnDetails.getService().getCode()))
				|| ("SMR".equalsIgnoreCase(txnDetails.getService().getCode()))
				|| ("LMB".equalsIgnoreCase(txnDetails.getService().getCode()))
				|| ("LMU".equalsIgnoreCase(txnDetails.getService().getCode()))
				|| ("PPS".equalsIgnoreCase(txnDetails.getService().getCode())))) {
			return true;
		}
		return false;
	}

	private String[] getUserNames() {
		String[] unames = { "9900242369", "9941576628", "9480491824", "8123398279", "9496642836", "9986629696",
				"9742976660", "7204773105", "8197891618", "9480658678", "9844461741" };
		return unames;
	}

	@Override
	public void updateOperatingSystem(User user) {
		userRepository.save(user);
	}

	@Override
	public void getDailyDetails() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
		SummaryReportDTO summaryReportDTO = new SummaryReportDTO();
		PQService service1 = pqServiceRepository.findServiceByCode("SMR");
		PQService service2 = pqServiceRepository.findServiceByCode("SMB");
		PQService service3 = pqServiceRepository.findServiceByCode("SMU");
		
		String authority = Authorities.LOCKED + "," + Authorities.AUTHENTICATED;
		String authority1 = Authorities.MERCHANT + "," + Authorities.AUTHENTICATED;
		try {
			List<PQAccountDetail> accounts = userRepository.getAccountsByAuthorities(authority,authority1);
			User mbank = findByUserName(StartupUtil.MBANK);

		DailySummaryDetails summaryDetails = dailySummaryRepository.getLastDetails(CommonUtil.getDateBeforeYesterday());
		if (summaryDetails == null) {
			Date fromDate = CommonUtil.getStartTimeOfDateBeforeYesterday(new Date());
			logger.info("date ::" + fromDate);
			Date toDate = CommonUtil.getEndTimeOfDateBeforeYesterday(new Date());
			logger.info("date ::" + toDate);
			Long totalUser = userRepository.findAllAuthenticateUserByDate(fromDate, toDate);
			logger.info("total users ::" + totalUser);
			Long totalNoOfTransaction = pqTransactionRepository.getTotalTransactionByDate(fromDate, toDate,accounts,mbank.getAccountDetail());
			logger.info("total noOfTransactions ::" + totalNoOfTransaction);
			Double totalDebitTransaction = pqTransactionRepository.getTotalDebitTransactionByDate(fromDate, toDate,
					service1, service2, service3);
			logger.info("total DebitTransaction ::" + totalDebitTransaction);
			Double totalCreditTransaction = pqTransactionRepository.getTotalCreditTransactionByDate(fromDate, toDate);
			logger.info("total CreditTransaction ::" + totalCreditTransaction);
			PQService ebs = pqServiceRepository.findServiceByCode("LMC");
			Double totalEbsCreditTransaction = pqTransactionRepository.getTotalEBSCreditTransactionByDate(fromDate,
					toDate, ebs);
			logger.info("total EBS CreditTransaction ::" + totalEbsCreditTransaction);
			PQService vnet = pqServiceRepository.findServiceByCode("LMB");
			Double totalVnetCreditTransaction = pqTransactionRepository.getTotalVNETCreditTransactionByDate(fromDate,
					toDate, vnet);
			logger.info("total VNET CreditTransaction ::" + totalVnetCreditTransaction);
			PQService upi = pqServiceRepository.findServiceByCode("LMU");
			Double totalUPICreditTransaction = pqTransactionRepository.getTotalUPICreditTransactionByDate(fromDate,
					toDate, upi);
			logger.info("total EBS CreditTransaction ::" + totalUPICreditTransaction);
			Long totalNoOfInitiatedTransaction = pqTransactionRepository.getTotalInitiatedTransactionByDate();
			logger.info("totalNoOf Initiated Transaction ::" + totalNoOfInitiatedTransaction);
			Long totalNoOfFailedTransaction = pqTransactionRepository.getTotalFailedTransactionByDate(fromDate, toDate);
			logger.info("totalNoOf Failed Transaction ::" + totalNoOfFailedTransaction);
			if (totalDebitTransaction == null) {
				totalDebitTransaction = 0.0;
			}
			if (totalCreditTransaction == null) {
				totalCreditTransaction = 0.0;
			}

			if (totalVnetCreditTransaction == null) {
				totalVnetCreditTransaction = 0.0;
			}
			if (totalEbsCreditTransaction == null) {
				totalEbsCreditTransaction = 0.0;
			}
			if (totalUPICreditTransaction == null) {
				totalUPICreditTransaction = 0.0;
			}
			summaryDetails = new DailySummaryDetails();
			summaryDetails.setDate(fromDate);
			summaryDetails.setNoOfUsers(totalUser);
			summaryDetails.setNoOfTransactions(totalNoOfTransaction);
			summaryDetails.setTotalDebitTransaction(totalDebitTransaction);
			summaryDetails.setTotalCreditTransaction(totalCreditTransaction);
			summaryDetails.setNoOfInitiatedTransactions(totalNoOfInitiatedTransaction);
			summaryDetails.setTotalEBSCreditTrasnaction(totalEbsCreditTransaction);
			summaryDetails.setTotalVnetCreditTransaction(totalVnetCreditTransaction);
			summaryDetails.setTotalUpiCreditTransaction(totalUPICreditTransaction);
			summaryDetails.setNoOfFailedTransactions(totalNoOfFailedTransaction);
			dailySummaryRepository.save(summaryDetails);
		}
		Date fromDate = CommonUtil.getStartTimeOfYesterDayDate(new Date());
		logger.info("date ::" + fromDate);
		Date toDate = CommonUtil.getEndTimeOfYesterDayDate(new Date());
		logger.info("date ::" + toDate);
		String dateString = sdf.format(fromDate);
		Long totalUser = userRepository.findAllAuthenticateUserByDate(fromDate, toDate);
		logger.info("total users ::" + totalUser);
		Long totalNoOfTransaction = pqTransactionRepository.getTotalTransactionByDate(fromDate, toDate,accounts,mbank.getAccountDetail());
		logger.info("total noOfTransactions ::" + totalNoOfTransaction);
		Double totalDebitTransaction = pqTransactionRepository.getTotalDebitTransactionByDate(fromDate, toDate,
				service1, service2, service3);
		logger.info("total DebitTransaction ::" + totalDebitTransaction);
		Double totalCreditTransaction = pqTransactionRepository.getTotalCreditTransactionByDate(fromDate, toDate);
		logger.info("total CreditTransaction ::" + totalCreditTransaction);
		PQService ebs = pqServiceRepository.findServiceByCode("LMC");
		Double totalEbsCreditTransaction = pqTransactionRepository.getTotalEBSCreditTransactionByDate(fromDate, toDate,
				ebs);
		logger.info("total EBS CreditTransaction ::" + totalEbsCreditTransaction);
		PQService vnet = pqServiceRepository.findServiceByCode("LMB");
		Double totalVnetCreditTransaction = pqTransactionRepository.getTotalVNETCreditTransactionByDate(fromDate,
				toDate, vnet);
		logger.info("total VNET CreditTransaction ::" + totalVnetCreditTransaction);
		PQService upi = pqServiceRepository.findServiceByCode("LMU");
		Double totalUPICreditTransaction = pqTransactionRepository.getTotalUPICreditTransactionByDate(fromDate, toDate,
				upi);
		logger.info("total UPI CreditTransaction ::" + totalUPICreditTransaction);
		Long totalNoOfInitiatedTransaction = pqTransactionRepository.getTotalInitiatedTransactionByDate();
		logger.info("totalNoOf Initiated Transaction ::" + totalNoOfInitiatedTransaction);
		Long totalNoOfFailedTransaction = pqTransactionRepository.getTotalFailedTransactionByDate(fromDate, toDate);
		logger.info("totalNoOf Failed Transaction ::" + totalNoOfFailedTransaction);

		if (totalDebitTransaction == null) {
			totalDebitTransaction = 0.0;
		}

		if (totalCreditTransaction == null) {
			totalCreditTransaction = 0.0;
		}

		if (totalVnetCreditTransaction == null) {
			totalVnetCreditTransaction = 0.0;
		}
		if (totalEbsCreditTransaction == null) {
			totalEbsCreditTransaction = 0.0;
		}
		if (totalUPICreditTransaction == null) {
			totalUPICreditTransaction = 0.0;
		}

		summaryReportDTO.setTotalUser(totalUser);
		summaryReportDTO.setTotalNoOfTransaction(totalNoOfTransaction);
		summaryReportDTO.setTotalDebitTransaction(totalDebitTransaction);
		summaryReportDTO.setTotalCreditTransaction(totalCreditTransaction);
		summaryReportDTO.setDate(dateString);
		summaryReportDTO.setTotalNoOfInitiatedTransaction(totalNoOfInitiatedTransaction);
		summaryReportDTO.setTotalNoOfFailedTransaction(totalNoOfFailedTransaction);
		summaryReportDTO.setTotalEBSCreditTrasnaction(totalEbsCreditTransaction);
		summaryReportDTO.setTotalVnetCreditTransaction(totalVnetCreditTransaction);
		summaryReportDTO.setTotalUpiCreditTransaction(totalUPICreditTransaction);

		Long userGrowth = 0l;
		Long noOfTransactionGrowth = 0l;
		Long totalDebitGrowth = 0l;
		Long totalCreditGrowth = 0l;
		Long totalEBSCreditGrowth = 0l;
		Long totalVnetCreditGrowth = 0l;
		Long totalUPICreditGrowth = 0l;
		Long noOfInitiatedTransactionGrowth = 0l;
		Long noOfFailedTransactionGrowth = 0l;

		String userGrowthString = "";
		String noOfTransactionGrowthString = "";
		String totalDebitGrowthString = "";
		String totalCreditGrowthString = "";
		String totalEBSCreditGrowthString = "";
		String totalVnetCreditGrowthString = "";
		String totalUPICreditGrowthString = "";
		String noOfInitiatedTransactionGrowthString = "";
		String noOfFailedTransactionGrowthString = "";

		if (summaryDetails.getNoOfUsers() == 0 && totalUser == 0) {
			userGrowth = 0l;
		} else if (summaryDetails.getNoOfUsers() == 0) {
			userGrowth = ((totalUser - summaryDetails.getNoOfUsers()) * 100) / totalUser;
		} else {
			userGrowth = ((totalUser - summaryDetails.getNoOfUsers()) * 100) / summaryDetails.getNoOfUsers();
		}

		if (userGrowth > 0) {
			userGrowthString = "+" + userGrowth.toString();
		} else {
			userGrowthString = userGrowth.toString();
		}

		if (summaryDetails.getNoOfTransactions() == 0 && totalNoOfTransaction == 0) {
			noOfTransactionGrowth = 0l;
		} else if (summaryDetails.getNoOfTransactions() == 0) {
			noOfTransactionGrowth = ((totalNoOfTransaction - summaryDetails.getNoOfTransactions()) * 100)
					/ totalNoOfTransaction;
		} else {
			noOfTransactionGrowth = ((totalNoOfTransaction - summaryDetails.getNoOfTransactions()) * 100)
					/ summaryDetails.getNoOfTransactions();
		}

		if (noOfTransactionGrowth > 0) {
			noOfTransactionGrowthString = "+" + noOfTransactionGrowth.toString();
		} else {
			noOfTransactionGrowthString = noOfTransactionGrowth.toString();
		}

		if (summaryDetails.getTotalDebitTransaction() == 0 && totalDebitTransaction == 0) {
			totalDebitGrowth = 0l;
		} else if (summaryDetails.getTotalDebitTransaction() == 0) {
			totalDebitGrowth = (long) (((totalDebitTransaction - summaryDetails.getTotalDebitTransaction()) * 100)
					/ totalDebitTransaction);
		} else {
			totalDebitGrowth = (long) (((totalDebitTransaction - summaryDetails.getTotalDebitTransaction()) * 100)
					/ summaryDetails.getTotalDebitTransaction());
		}

		if (totalDebitGrowth > 0) {
			totalDebitGrowthString = "+" + totalDebitGrowth.toString();
		} else {
			totalDebitGrowthString = totalDebitGrowth.toString();
		}

		if (summaryDetails.getTotalCreditTransaction() == 0 && totalCreditTransaction == 0) {
			totalCreditGrowth = 0l;
		} else if (summaryDetails.getTotalCreditTransaction() == 0) {
			totalCreditGrowth = (long) (((totalCreditTransaction - summaryDetails.getTotalCreditTransaction()) * 100)
					/ totalCreditTransaction);
		} else {
			totalCreditGrowth = (long) (((totalCreditTransaction - summaryDetails.getTotalCreditTransaction()) * 100)
					/ summaryDetails.getTotalCreditTransaction());
		}

		if (totalCreditGrowth > 0) {
			totalCreditGrowthString = "+" + totalCreditGrowth.toString();
		} else {
			totalCreditGrowthString = totalCreditGrowth.toString();
		}

		if (summaryDetails.getTotalEBSCreditTrasnaction() == 0 && totalEbsCreditTransaction == 0) {
			totalEBSCreditGrowth = 0l;
		} else if (summaryDetails.getTotalEBSCreditTrasnaction() == 0) {
			totalEBSCreditGrowth = (long) (((totalEbsCreditTransaction - summaryDetails.getTotalEBSCreditTrasnaction())
					* 100) / totalEbsCreditTransaction);
		} else {
			totalEBSCreditGrowth = (long) (((totalEbsCreditTransaction - summaryDetails.getTotalEBSCreditTrasnaction())
					* 100) / summaryDetails.getTotalEBSCreditTrasnaction());
		}

		if (totalEBSCreditGrowth > 0) {
			totalEBSCreditGrowthString = "+" + totalEBSCreditGrowth.toString();
		} else {
			totalEBSCreditGrowthString = totalEBSCreditGrowth.toString();
		}

		if (summaryDetails.getTotalVnetCreditTransaction() == 0 && totalVnetCreditTransaction == 0) {
			totalVnetCreditGrowth = 0l;
		} else if (summaryDetails.getTotalVnetCreditTransaction() == 0) {
			totalVnetCreditGrowth = (long) (((totalVnetCreditTransaction
					- summaryDetails.getTotalVnetCreditTransaction()) * 100) / totalVnetCreditTransaction);
		} else {
			totalVnetCreditGrowth = (long) (((totalVnetCreditTransaction
					- summaryDetails.getTotalVnetCreditTransaction()) * 100)
					/ summaryDetails.getTotalVnetCreditTransaction());
		}

		if (totalVnetCreditGrowth > 0) {
			totalVnetCreditGrowthString = "+" + totalVnetCreditGrowth.toString();
		} else {
			totalVnetCreditGrowthString = totalVnetCreditGrowth.toString();
		}

		if (summaryDetails.getTotalUpiCreditTransaction() == 0 && totalUPICreditTransaction == 0) {
			totalUPICreditGrowth = 0l;
		} else if (summaryDetails.getTotalUpiCreditTransaction() == 0) {
			totalUPICreditGrowth = (long) (((totalUPICreditTransaction - summaryDetails.getTotalUpiCreditTransaction())
					* 100) / totalCreditTransaction);
		} else {
			totalUPICreditGrowth = (long) (((totalUPICreditTransaction - summaryDetails.getTotalUpiCreditTransaction())
					* 100) / summaryDetails.getTotalUpiCreditTransaction());
		}

		if (totalUPICreditGrowth > 0) {
			totalUPICreditGrowthString = "+" + totalUPICreditGrowth.toString();
		} else {
			totalUPICreditGrowthString = totalUPICreditGrowth.toString();
		}

		if (summaryDetails.getNoOfInitiatedTransactions() == 0 && totalNoOfInitiatedTransaction == 0) {
			noOfInitiatedTransactionGrowth = 0l;
		} else if (summaryDetails.getNoOfInitiatedTransactions() == 0) {
			noOfInitiatedTransactionGrowth = ((totalNoOfInitiatedTransaction
					- summaryDetails.getNoOfInitiatedTransactions()) * 100) / totalNoOfInitiatedTransaction;
		} else {
			noOfInitiatedTransactionGrowth = ((totalNoOfInitiatedTransaction
					- summaryDetails.getNoOfInitiatedTransactions()) * 100)
					/ summaryDetails.getNoOfInitiatedTransactions();
		}

		if (noOfInitiatedTransactionGrowth > 0) {
			noOfInitiatedTransactionGrowthString = "+" + noOfInitiatedTransactionGrowth.toString();
		} else {
			noOfInitiatedTransactionGrowthString = noOfInitiatedTransactionGrowth.toString();
		}

		if (summaryDetails.getNoOfFailedTransactions() == 0 && totalNoOfFailedTransaction == 0) {
			noOfFailedTransactionGrowth = 0l;
		} else if (summaryDetails.getNoOfFailedTransactions() == 0) {
			noOfFailedTransactionGrowth = ((totalNoOfFailedTransaction - summaryDetails.getNoOfFailedTransactions())
					* 100) / totalNoOfFailedTransaction;
		} else {
			noOfFailedTransactionGrowth = ((totalNoOfFailedTransaction - summaryDetails.getNoOfFailedTransactions())
					* 100) / summaryDetails.getNoOfFailedTransactions();
		}

		if (noOfFailedTransactionGrowth > 0) {
			noOfFailedTransactionGrowthString = "+" + noOfFailedTransactionGrowth.toString();
		} else {
			noOfFailedTransactionGrowthString = noOfFailedTransactionGrowth.toString();
		}

		DailySummaryDetails details = new DailySummaryDetails();
		details.setDate(fromDate);
		details.setNoOfTransactions(totalNoOfTransaction);
		details.setNoOfUsers(totalUser);
		details.setTotalCreditTransaction(totalCreditTransaction);
		details.setTotalDebitTransaction(totalDebitTransaction);
		details.setNoOfInitiatedTransactions(totalNoOfInitiatedTransaction);
		details.setTotalEBSCreditTrasnaction(totalEbsCreditTransaction);
		details.setTotalVnetCreditTransaction(totalVnetCreditTransaction);
		details.setTotalUpiCreditTransaction(totalUPICreditTransaction);
		details.setNoOfFailedTransactions(totalNoOfFailedTransaction);
		dailySummaryRepository.save(details);

		summaryReportDTO.setUserGrowth(userGrowth);
		summaryReportDTO.setUserGrowthString(userGrowthString);
		summaryReportDTO.setNoOfTransactionGrowth(noOfTransactionGrowth);
		summaryReportDTO.setNoOfTransactionGrowthString(noOfTransactionGrowthString);
		summaryReportDTO.setTotalDebitGrowth(totalDebitGrowth);
		summaryReportDTO.setTotalDebitGrowthString(totalDebitGrowthString);
		summaryReportDTO.setTotalCreditGrowth(totalCreditGrowth);
		summaryReportDTO.setTotalCreditGrowthString(totalCreditGrowthString);
		summaryReportDTO.setTotalEBSCreditGrowth(totalEBSCreditGrowth);
		summaryReportDTO.setTotalEBSCreditGrowthString(totalEBSCreditGrowthString);
		summaryReportDTO.setTotalVentCreditGrowth(totalVnetCreditGrowth);
		summaryReportDTO.setTotalVnetCreditGrowthString(totalVnetCreditGrowthString);
		summaryReportDTO.setTotalUPICreditGrowth(totalUPICreditGrowth);
		summaryReportDTO.setTotalUPICreditGrowthString(totalUPICreditGrowthString);
		summaryReportDTO.setNoOfInitiatedTransactionGrowth(noOfInitiatedTransactionGrowth);
		summaryReportDTO.setNoOfInitiatedTransactionGrowthString(noOfInitiatedTransactionGrowthString);
		summaryReportDTO.setTotalNoOfFailedTransactionGrowth(noOfFailedTransactionGrowth);
		summaryReportDTO.setNoOfFailedTransactionGrowthString(noOfFailedTransactionGrowthString);
		mailSenderApi.sendSummaryMail(MailTemplate.SUMMARY_EMAIL, summaryReportDTO);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public Page<SendNotification> getGcmList(Pageable pageable) {
		return sendNotificationRepository.getGcmList(pageable);
	}
	@Override
	public ResponseDTO saveServices(AddServicesDTO dto) {

		ResponseDTO result = new ResponseDTO();
		try {
			logger.info("operator ::" + dto.getOperator());
			PQOperator operator = pqOperatorRepository.findOperatorByName(dto.getOperator().trim());
			logger.info("Got operator : " + operator);
			if (operator != null) {
				PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName(dto.getServiceType());
				logger.info("Got Service Type : : " + serviceType);
				if (serviceType != null) {
					logger.info("code ::" + dto.getCode());
					PQService service = pqServiceRepository.findServiceByCode(dto.getCode());
					logger.info("Service Adding " + service);
					if (service == null) {
						service = new PQService();
						service.setCode(dto.getCode());
						if (dto.getServiceType().equalsIgnoreCase("Bill Payment")) {
							service.setOperatorCode(dto.getCode().substring(1));
						} else {
							service.setOperatorCode(dto.getCode());
						}
						service.setName(dto.getName());
						service.setDescription(dto.getDescription());
						service.setServiceType(serviceType);
						service.setOperator(operator);
						service.setStatus(Status.Active);
						service.setMinAmount(Double.parseDouble(dto.getMinAmount()));
						service.setMaxAmount(Double.parseDouble(dto.getMaxAmount()));
						service.setBbpsEnabled(dto.isBbpsEnabled());
						pqServiceRepository.save(service);
						PQCommission commission = pqCommissionRepository.findOneCommissionByService(service);
						if (commission == null) {
							commission = new PQCommission();
							commission.setType(dto.getType());
							commission.setMinAmount(Double.parseDouble(dto.getMinAmount()));
							commission.setMaxAmount(Double.parseDouble(dto.getMaxAmount()));
							commission.setIdentifier(getIdentifier(dto));
							commission.setFixed(dto.isFixed());
							commission.setValue(Double.parseDouble(dto.getValue()));
							commission.setService(service);
							pqCommissionRepository.save(commission);
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Commission has already added");
							result.setDetails("Commission has already added");
						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Service has been added successfully");
						result.setDetails("Service has been added successfully");
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Service has already added");
						result.setDetails("Service has already added");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Please select valid service type");
					result.setDetails("Please select valid service type");
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Please select valid operator");
				result.setDetails("Please select valid operator");
			}
		} catch (Exception e) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Internal server error");
			result.setDetails("Internal server error");
		}
		return result;
	}
	
	@Override
	public List<AadharDetailsDTO> getAllAadharDetails() {
		List<AadharDetails> details = (List<AadharDetails>) aadharDetailsRepository.findAllAadharDetails();
		return ConvertUtil.getListForAadharDetails(details,userRepository);
	}
	
	@Override
	public ResponseDTO saveServiceType(AddServicesDTO dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName(dto.getName());
			if (serviceType == null) {
				serviceType = new PQServiceType();
				serviceType.setName(dto.getName());
				serviceType.setDescription(dto.getDescription());
				pqServiceTypeRepository.save(serviceType);
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("ServiceType has been added successfully");
				result.setDetails("ServiceType has been added successfully");
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("ServiceType has already added");
				result.setDetails("ServiceType has already added");
			}
		} catch (Exception e) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Internal server error");
			result.setDetails("Internal server error");
		}
		return result;

	}
	
	private String getIdentifier(AddServicesDTO req) {
		String identifier = req.getType() + "|" + req.getValue() + "|" + req.getMinAmount() + "|"
				+ req.getMaxAmount() + "|" + req.getType() + "|" + req.getCode();
		return identifier;
   }
	
	@Override
	public AgentDetailsDTO getAgentDetailsById(String agentId) {
		AgentDetailsDTO dto = new AgentDetailsDTO();
		try {
			AgentDetail agent = agentDetailRepository.findById(Long.parseLong(agentId));
			if (agent != null) {
				dto.setAgentId(agent.getId().toString());
				dto.setAgentName(agent.getAgentName());
				dto.setBankAccountName(agent.getBankAccountName());
				dto.setBranchName(agent.getBranchName());
				dto.setIfscCode(agent.getIfscCode());
				dto.setPanCardNo(agent.getPanCardNo());
				dto.setImage(agent.getImage());
				dto.setImageContent(agent.getImageContent());
				User user = userRepository.findByUser("A" + agent.getMobileNumber());
				if (user != null) {
					UserDetail detail = userDetailRepository.findUserById(user.getUserDetail().getId());
					if (detail != null) {
						dto.setFirstName(detail.getFirstName());
						dto.setLastName(detail.getLastName());
						dto.setAddress(detail.getAddress());
						dto.setMobileNumber(detail.getContactNo());
						dto.setLocationId(detail.getLocation().getId());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dto;
	}
	
	
	@Override
	public User saveUnregisteredSdkUser(RegisterDTO dto,String device) throws ClientException {
		User user = new User();
		UserDetail userDetail = new UserDetail();
		try {
			userDetail.setContactNo(dto.getContactNo());
			userDetail.setFirstName(dto.getFirstName());
			userDetail.setLastName(dto.getLastName());
			userDetail.setMiddleName(dto.getMiddleName());
			userDetail.setEmail(dto.getEmail());
			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				}
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setOsName(device);
			} else {
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Inactive);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);

			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
		return user;
	}

	@Override
	public LoginLog getRemoteAddress(User user) {
		List<LoginLog> logs=loginLogRepository.getUserLogById(user);
		if(logs!=null && logs.size()>0){
			return logs.get(0);
		}
		return null;
	}

	@Override
	public ResponseDTO updateUserMaxLimit(UserMaxLimitDto dto, User user) {
		ResponseDTO responseDto = new ResponseDTO();
		try {
			PQAccountDetail accountDetail = user.getAccountDetail();
			UserMaxLimit maxLimit = accountDetail.getUserMaxLimit() != null ? accountDetail.getUserMaxLimit()
					: new UserMaxLimit();
			if (maxLimit != null && maxLimit.getId() == null) {
				maxLimit = getDefaultValues(maxLimit, accountDetail);
			}
			if (!CommonValidation.isNull(dto.getTransactionType())) {
				switch (dto.getTransactionType().toUpperCase()) {
				case "MERCHANT":
					if (!CommonValidation.isNull(dto.getDailyAmountOfTxn()))
						maxLimit.setMerchantDailyAmountOfTxn(Double.parseDouble(dto.getDailyAmountOfTxn()));
					if (!CommonValidation.isNull(dto.getMonthlyAmountOfTxn()))
						maxLimit.setMerchantMonthlyAmountOfTxn(Double.parseDouble(dto.getMonthlyAmountOfTxn()));
					if (!CommonValidation.isNull(dto.getDailyNoOfTxn()))
						maxLimit.setMerchantDailyNoOfTxn(Long.parseLong(dto.getDailyNoOfTxn()));
					if (!CommonValidation.isNull(dto.getMonthlyNoOfTxn()))
						maxLimit.setMerchantMonthlyNoOfTxn(Long.parseLong(dto.getMonthlyNoOfTxn()));
					break;
				case "USER":
					if (!CommonValidation.isNull(dto.getDailyAmountOfTxn()))
						maxLimit.setUserDailyAmountOfTxn(Double.parseDouble(dto.getDailyAmountOfTxn()));
					if (!CommonValidation.isNull(dto.getMonthlyAmountOfTxn()))
						maxLimit.setUserMonthlyAmountOfTxn(Double.parseDouble(dto.getMonthlyAmountOfTxn()));
					if (!CommonValidation.isNull(dto.getDailyNoOfTxn()))
						maxLimit.setUserDailyNoOfTxn(Long.parseLong(dto.getDailyNoOfTxn()));
					if (!CommonValidation.isNull(dto.getMonthlyNoOfTxn()))
						maxLimit.setUserMonthlyNoOfTxn(Long.parseLong(dto.getMonthlyNoOfTxn()));

					break;
				case "BANKTRANSFER":
					if (!CommonValidation.isNull(dto.getDailyAmountOfTxn()))
						maxLimit.setUserDailyAmountOfBT(Double.parseDouble(dto.getDailyAmountOfTxn()));
					if (!CommonValidation.isNull(dto.getMonthlyAmountOfTxn()))
						maxLimit.setUserMonthlyAmountOfBT(Double.parseDouble(dto.getMonthlyAmountOfTxn()));
					if (!CommonValidation.isNull(dto.getDailyNoOfTxn()))
						maxLimit.setUserDailyNoOfBT(Long.parseLong(dto.getDailyNoOfTxn()));
					if (!CommonValidation.isNull(dto.getMonthlyNoOfTxn()))
						maxLimit.setUserMonthlyNoOfBT(Long.parseLong(dto.getMonthlyNoOfTxn()));
					break;
				default:
					responseDto.setCode(ResponseStatus.FAILURE.getValue());
					responseDto.setMessage("Transaction type miss match ");
					return responseDto;
				}
				userMaxLimitRepository.save(maxLimit);
				accountDetail.setUserMaxLimit(maxLimit);
				user.setMobileToken(null);
				userRepository.save(user);
				pqAccountDetailRepository.save(accountDetail);
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.SUCCESS_PAYMENT_LIMIT, user, null);
				
				responseDto.setCode(ResponseStatus.SUCCESS.getValue());
				responseDto.setMessage(dto.getTransactionType()+" payment limit has been updated");
			} else {
				responseDto.setCode(ResponseStatus.FAILURE.getValue());
				responseDto.setMessage("Transaction type should not be empty");
			}
		} catch (Exception e) {
			responseDto.setMessage("Error in updating payment limit");
			responseDto.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			e.printStackTrace();
		}
		return responseDto;
	}

	private UserMaxLimit getDefaultValues(UserMaxLimit maxLimit, PQAccountDetail accountDetail) {
			maxLimit.setMerchantDailyAmountOfTxn(accountDetail.getAccountType().getDailyLimit());
			maxLimit.setMerchantMonthlyAmountOfTxn(accountDetail.getAccountType().getMonthlyLimit());
			maxLimit.setMerchantDailyNoOfTxn(5000);
			maxLimit.setMerchantMonthlyNoOfTxn(5000);
			maxLimit.setUserDailyAmountOfTxn(accountDetail.getAccountType().getDailyLimit());
			maxLimit.setUserMonthlyAmountOfTxn(accountDetail.getAccountType().getMonthlyLimit());
			maxLimit.setUserDailyNoOfTxn(5000);
			maxLimit.setUserMonthlyNoOfTxn(5000);
			maxLimit.setUserDailyAmountOfBT(5000);
			maxLimit.setUserMonthlyAmountOfBT(15000);
			maxLimit.setUserDailyNoOfBT(1);
			maxLimit.setUserMonthlyNoOfBT(3);
		return maxLimit;
	}

	@Override
	public ResponseDTO getUserMaxLimit(UserMaxLimitDto dto, User user) {
		UserMaxLimitDto maxLimitDto=new UserMaxLimitDto();
		ResponseDTO responseDto= new ResponseDTO();
		try {
			UserMaxLimit maxLimit= user.getAccountDetail().getUserMaxLimit();
			if (!CommonValidation.isNull(dto.getTransactionType())) {
				if(maxLimit == null) {
					maxLimit= getDefaultValues(new UserMaxLimit(),user.getAccountDetail());
				}
				switch (dto.getTransactionType().toUpperCase()) {
				case "MERCHANT":
					maxLimitDto.setDailyAmountOfTxn(String.format("%.2f", maxLimit.getMerchantDailyAmountOfTxn()));
					maxLimitDto.setDailyNoOfTxn(maxLimit.getMerchantDailyNoOfTxn()+"");
					maxLimitDto.setMonthlyAmountOfTxn(String.format("%.2f", maxLimit.getMerchantMonthlyAmountOfTxn()));
					maxLimitDto.setMonthlyNoOfTxn(maxLimit.getMerchantMonthlyNoOfTxn()+"");
					break;
				case "USER":
					maxLimitDto.setDailyAmountOfTxn(String.format("%.2f", maxLimit.getUserDailyAmountOfTxn()));
					maxLimitDto.setDailyNoOfTxn(maxLimit.getUserDailyNoOfTxn()+"");
					maxLimitDto.setMonthlyAmountOfTxn(String.format("%.2f", maxLimit.getUserMonthlyAmountOfTxn()));
					maxLimitDto.setMonthlyNoOfTxn(maxLimit.getUserMonthlyNoOfTxn()+"");
					break;
				case "BANKTRANSFER":
					maxLimitDto.setDailyAmountOfTxn(String.format("%.2f", maxLimit.getUserDailyAmountOfBT()));
					maxLimitDto.setDailyNoOfTxn(maxLimit.getUserDailyNoOfBT()+"");
					maxLimitDto.setMonthlyAmountOfTxn(String.format("%.2f", maxLimit.getUserMonthlyAmountOfBT()));
					maxLimitDto.setMonthlyNoOfTxn(maxLimit.getUserMonthlyNoOfBT()+"");
					break;
				default :
					responseDto.setCode(ResponseStatus.FAILURE.getValue());
					responseDto.setMessage("Transaction type miss match ");
					return responseDto;
				}
				responseDto.setCode(ResponseStatus.SUCCESS.getValue());
				responseDto.setMessage("Get user payment limit amount");
				responseDto.setDetails(maxLimitDto);
			}else {
				responseDto.setCode(ResponseStatus.FAILURE.getValue());
				responseDto.setMessage("Error in fetching payment limit data");
			}
		} catch (Exception e) {
			responseDto.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			responseDto.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			e.printStackTrace();
		}
		return responseDto;
	}

	@Override
	public long getDailyTransactionCount(User user) {
		Calendar now = Calendar.getInstance();
		long total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getDailyTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1),now.get(Calendar.DATE), user.getAccountDetail());
			if (trans.size() != 0) {
				total = pqTransactionRepository.getDailyTransactionCount(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1),now.get(Calendar.DATE), user.getAccountDetail());
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public long getMonthlyTransactionCount(User user) {
		Calendar now = Calendar.getInstance();
		long total = 0;
		try {
			List<PQTransaction> trans = pqTransactionRepository.getDailyTransaction(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1),now.get(Calendar.DATE), user.getAccountDetail());
			if (trans.size() != 0) {
				total = pqTransactionRepository.getMonthlyTransactionCount(now.get(Calendar.YEAR),
						(now.get(Calendar.MONTH) + 1), user.getAccountDetail());
			} else {
				total = 0;
			}
		} catch (NullPointerException e) {
			total = 0;
		} catch (Exception e) {
			total = -1;
			e.printStackTrace();
		}
		return total;
	}

	@Override
	public ResponseDTO sendMaxLimitOtp(User user) {
		ResponseDTO dto= new ResponseDTO();
		try {
			user.setMobileToken(CommonUtil.generateSixDigitNumericString());
			userRepository.save(user);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.UPDATE_MAX_LIMIT, user, null);
			dto.setCode(ResponseStatus.SUCCESS.getValue());
			dto.setMessage("Please enter otp for updating your payment limit");
		} catch (Exception e) {
			dto.setCode(ResponseStatus.FAILURE.getValue());
			dto.setMessage("Error in sending Otp, please try later.");
			e.printStackTrace();
		}
		return dto;
	}

	@Override
	public boolean validateUserPassword(LoginDTO dto, String password) {
		if(password!=null && dto.getPassword()!=null && passwordEncoder.matches(dto.getPassword(),password))
			return true;
		return false;
	}
	
}
