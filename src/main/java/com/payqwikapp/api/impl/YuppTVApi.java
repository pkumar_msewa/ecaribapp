package com.payqwikapp.api.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IYuppTVApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.SavaariBooking;
import com.payqwikapp.entity.YTVAccess;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.YupTVDTO;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.YupTvRepository;
import com.payqwikapp.util.StartupUtil;

public class YuppTVApi implements IYuppTVApi {
	
	 private ITransactionApi transactionApi;
	 private final YupTvRepository yupTvRepository;
	 private final PQTransactionRepository pqTransactionRepository;
	 private final PQAccountDetailRepository pqAccountDetailRepository;
	 private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	public YuppTVApi(ITransactionApi transactionApi,YupTvRepository yupTvRepository,PQTransactionRepository pqTransactionRepository,PQAccountDetailRepository pqAccountDetailRepository) {
		this.transactionApi=transactionApi;
		this.yupTvRepository=yupTvRepository;
		this.pqTransactionRepository=pqTransactionRepository;
		this.pqAccountDetailRepository=pqAccountDetailRepository;
	}

	@Override
	public YTVAccess isYupTVAccountSaved(YupTVDTO dto, PQAccountDetail account) throws ParseException {
		YTVAccess access = new YTVAccess();
		access.setUserId(dto.getUserId());
		access.setDescription(dto.getMessage());
		access.setPartnerId(dto.getPartnerId());
		access.setStatus(dto.getStatus());
		access.setExpiryDate(formatter.parse(dto.getExpiryDate()));
		access.setToken(dto.getToken());
		access.setUniqueId(dto.getUnique_id());
		access.setAccount(account);
		yupTvRepository.save(access);
		processPayment(dto.getTransactionRefNo());
		return access;
	}

	@Override
	public boolean checkRegister(YupTVDTO dto, PQAccountDetail account) {
		YTVAccess access = yupTvRepository.findByUniqueId(dto.getUnique_id());
		if (access != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean checkExistUser(YupTVDTO dto, PQAccountDetail account) {
		YTVAccess access = yupTvRepository.findByUniqueId(dto.getUserId());
		if (access != null) {
			return true;
		}
		return false;
	}

	@Override
	public YTVAccess findByExpiryDate(Date expiryDate) throws ParseException {
		YTVAccess access = yupTvRepository.findByExpiryDate(expiryDate);
		return access;
	}

	@Override
	public YTVAccess findByUniqueId(String id) {
		YTVAccess access = yupTvRepository.findByUniqueId(id);
		return access;
	}

	@Override
	public YTVAccess updateYtvAccess(YupTVDTO dto) {
		YTVAccess access = yupTvRepository.findByUserId(dto.getUserId());
		if (access != null) {
			try {
				access.setUserId(dto.getUserId());
				access.setDescription(dto.getMessage());
				access.setPartnerId(dto.getPartnerId());
				access.setStatus(dto.getStatus());
				access.setExpiryDate(formatter.parse(dto.getExpiryDate()));
				access.setToken(dto.getToken());
				access.setUniqueId(dto.getUnique_id());
				yupTvRepository.save(access);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return access;
		}
		return null;
	}
	@Override
	public String processPayment(String transactionRefNo) {
			transactionApi.successBankTransfer(transactionRefNo);
			return null;	
		}

	@Override
	public PQTransaction placeSubscriptionForYuppTV(YupTVDTO order, PQService service, String senderUsername) {
		  String transactionRefNo = String.valueOf(System.currentTimeMillis());
		  System.err.println("transactionrefNo::" + transactionRefNo);
		PQTransaction valid = initiateTransaction(transactionRefNo,Double.parseDouble(order.getAmount()),service,senderUsername);
		return valid;
	}
	
	@Override
	public PQTransaction reverseSubscriptionForYuppTV(YupTVDTO order, PQService service, String senderUsername) {
		System.err.println("transaction no::" + order.getTransactionRefNo());
		PQTransaction transaction = transactionApi.getTransactionByRefNo(order.getTransactionRefNo());
		System.err.println("transaction ::" + transaction.getTransactionRefNo());
		System.err.println("transaction ::" + transaction);
		if(transaction!=null && (transaction.getStatus().equals(Status.Initiated))){
			System.err.println("inside if condition");
			PQTransaction pqTransaction = new PQTransaction();
			pqTransaction.setCurrentBalance(transaction.getCurrentBalance()+Double.parseDouble(order.getAmount()));
			pqTransaction.setTransactionRefNo(order.getTransactionRefNo());
			pqTransaction.setService(transaction.getService());
			pqTransaction.setAmount(transaction.getAmount());
			pqTransaction.setCommissionIdentifier(transaction.getCommissionIdentifier());
			pqTransaction.setAccount(transaction.getAccount());
			transaction.setStatus(Status.Reversed);
			pqTransaction.setStatus(Status.Success);
			pqTransaction.setDebit(true);
			pqTransaction.setDescription("Reverse Of YuppTv of Rs."+Double.parseDouble(order.getAmount()));
			pqTransactionRepository.save(transaction);
			pqTransactionRepository.save(pqTransaction);
			PQAccountDetail pqAccountDetail= transaction.getAccount();
			System.err.println("account ::" + pqAccountDetail.getId());
			if(pqAccountDetail!=null){
				double balance = pqAccountDetail.getBalance();
				balance += transaction.getAmount();
				System.err.println("amount ::" + pqTransaction.getCurrentBalance());
				pqAccountDetail.setBalance(balance);
				pqAccountDetailRepository.save(pqAccountDetail);
			}
			return transaction;
		}
		return null;
	}

	@Override
	public PQTransaction initiateTransaction(String transactionRefNo, double amount, PQService service,
			String senderUsername) {
		String description = "Subscription Of YuppTv of Rs."+amount;
        return transactionApi.initiateYuppTvPayment(amount,description,service,transactionRefNo,senderUsername, StartupUtil.YUPPTV);
	}

	

}
