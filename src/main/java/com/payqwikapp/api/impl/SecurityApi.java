package com.payqwikapp.api.impl;

import com.ebs.model.EBSRedirectResponse;
import com.ebs.model.EBSRequest;
import com.payqwikapp.api.ISecurityApi;
import com.payqwikapp.model.*;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.util.SecurityUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.IOException;

public class SecurityApi implements ISecurityApi {

    @Override
    public LoginDTO getLoginDTO(ERequestDTO dto) {
        LoginDTO login = null;
        String json =  SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                login = mapper.readValue(json,LoginDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return login;
    }

    @Override
    public SessionDTO getSessionDTO(ERequestDTO dto) {
        SessionDTO session = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                session = mapper.readValue(json,SessionDTO.class);
            } catch(IOException e){
                e.printStackTrace();
            }
        }
        return session;
    }

    @Override
    public EResponseDTO getEResponse(ResponseDTO dto) {
        EResponseDTO result = new EResponseDTO();
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer();
        try {
            String json = writer.writeValueAsString(dto);
            result.setEncryptedText(SecurityUtil.encryptString(json));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public RegisterDTO getRegisterDTO(ERequestDTO dto) {
        RegisterDTO newDTO = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try{
                newDTO = mapper.readValue(json,RegisterDTO.class);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return newDTO;
    }

    @Override
    public ChangePasswordDTO getChangePasswordDTO(ERequestDTO dto) {
        ChangePasswordDTO passwordDTO = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                passwordDTO = mapper.readValue(json,ChangePasswordDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return passwordDTO;
    }

    @Override
    public UserRequestDTO getUserRequestDTO(ERequestDTO dto) {
        UserRequestDTO user = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                user = mapper.readValue(json,UserRequestDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    public RefundDTO getRefundDTO(ERequestDTO dto) {
        RefundDTO refund = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                refund = mapper.readValue(json,RefundDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return refund;
    }

    @Override
    public GetTransactionDTO getTransactionDTO(ERequestDTO dto) {
        GetTransactionDTO transaction = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                transaction = mapper.readValue(json,GetTransactionDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return transaction;
    }

    @Override
    public PagingDTO getPagingDTO(ERequestDTO dto) {
        PagingDTO paging = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
                ObjectMapper mapper = new ObjectMapper();
            try {
                paging = mapper.readValue(json,PagingDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return paging;
    }

    @Override
    public MRegisterDTO getMRegisterDTO(ERequestDTO dto) {
        MRegisterDTO register = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                register = mapper.readValue(json,MRegisterDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return register;
    }

    @Override
    public PromoCodeDTO getPromoCodeDTO(ERequestDTO dto) {
        PromoCodeDTO promo = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                promo = mapper.readValue(json,PromoCodeDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return promo;
    }

    @Override
    public CallBackDTO getCallBackDTO(ERequestDTO dto) {
        CallBackDTO callBack = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                callBack = mapper.readValue(json,CallBackDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return callBack;
    }

    @Override
    public DTHBillPaymentDTO getDTHBillPaymentDTO(ERequestDTO dto) {
        DTHBillPaymentDTO dth = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                dth = mapper.readValue(json,DTHBillPaymentDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dth;
    }

    @Override
    public LandlineBillPaymentDTO getLandlineBillPaymentDTO(ERequestDTO dto) {
        LandlineBillPaymentDTO landline = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                landline = mapper.readValue(json,LandlineBillPaymentDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return landline;
    }

    @Override
    public ElectricityBillPaymentDTO getElectricityBillPaymentDTO(ERequestDTO dto) {
        ElectricityBillPaymentDTO electricity = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                electricity = mapper.readValue(json,ElectricityBillPaymentDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return electricity;
    }

    @Override
    public GasBillPaymentDTO getGasBillPaymentDTO(ERequestDTO dto) {
        GasBillPaymentDTO gas = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                gas = mapper.readValue(json,GasBillPaymentDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return gas;
    }

    @Override
    public InsuranceBillPaymentDTO getInsuranceBillPaymentDTO(ERequestDTO dto) {
        InsuranceBillPaymentDTO insurance = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                insurance = mapper.readValue(json,InsuranceBillPaymentDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return insurance;
    }

    @Override
    public TelcoDTO getTelcoDTO(ERequestDTO dto) {
        TelcoDTO telco = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try{
                telco = mapper.readValue(json,TelcoDTO.class);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return telco;
    }

    @Override
    public PlanDTO getPlanDTO(ERequestDTO dto) {
        PlanDTO plan = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                plan = mapper.readValue(json,PlanDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return plan;
    }

    @Override
    public String encryptText(String rawText) {
        String encryptedText = SecurityUtil.encryptString(rawText);
        return encryptedText;
    }

    @Override
    public CallBackRequest getCallBackRequest(ERequestDTO dto) {
        CallBackRequest callBack = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                callBack  = mapper.readValue(json,CallBackRequest.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return callBack;
    }

    @Override
    public CouponDTO getCouponDTO(ERequestDTO dto) {
        CouponDTO coupon = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                coupon = mapper.readValue(json,CouponDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return coupon;
    }

    @Override
    public VNetDTO getVNetDTO(ERequestDTO dto) {
        VNetDTO vnet = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                vnet = mapper.readValue(json,VNetDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return vnet;
    }

    @Override
    public VNetResponse getVNetResponse(ERequestDTO dto) {
        VNetResponse vnet = null;
        String json  = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                vnet = mapper.readValue(json,VNetResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return vnet;
    }

    @Override
    public EBSRequest getEBSRequest(ERequestDTO dto) {
        EBSRequest ebs  = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                ebs = mapper.readValue(json,EBSRequest.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ebs;
    }

    @Override
    public EBSRedirectResponse getEBSRedirectResponse(ERequestDTO dto) {
        EBSRedirectResponse ebs = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                ebs = mapper.readValue(json,EBSRedirectResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ebs;
    }

    @Override
    public PayAtStoreDTO getPayStoreDTO(ERequestDTO dto) {
        PayAtStoreDTO pay = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                pay = mapper.readValue(json,PayAtStoreDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return pay;
    }

    @Override
    public RedeemDTO getRedeemDTO(ERequestDTO dto) {
        RedeemDTO redeem = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null){
            ObjectMapper mapper = new ObjectMapper();
            try {
                redeem = mapper.readValue(json,RedeemDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return redeem;
    }

    @Override
    public MpinDTO getMpinDTO(ERequestDTO dto) {
        MpinDTO mpin = null;
        String json =  SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                mpin = mapper.readValue(json,MpinDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mpin;
    }

    @Override
    public MpinChangeDTO getMpinChangeDTO(ERequestDTO dto) {
        MpinChangeDTO mpinChange = null;
        String json = SecurityUtil.decryptData(dto.getEncryptedData());
        if(json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                mpinChange = mapper.readValue(json,MpinChangeDTO.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mpinChange;
    }
}
