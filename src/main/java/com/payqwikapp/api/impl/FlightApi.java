package com.payqwikapp.api.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
/*
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfWriter;*/
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IFlightApi;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.FlightTravellers;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.FlightDetailsDTO;
import com.payqwikapp.model.FlightPaymentRequest;
import com.payqwikapp.model.FlightPaymentResponse;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.FlightResponseEmail;
import com.payqwikapp.model.travel.FlightTicketdtoForname;
import com.payqwikapp.model.travel.TicketDetailsDTOFlight;
import com.payqwikapp.model.travel.flight.CompareCountry;
import com.payqwikapp.model.travel.flight.FlghtTicketResp;
import com.payqwikapp.model.travel.flight.FlightPayment;
import com.payqwikapp.model.travel.flight.TicketsResp;
import com.payqwikapp.repositories.FlightDetailsRepository;
import com.payqwikapp.repositories.FlightListRepository;
import com.payqwikapp.repositories.FlightTicketRepository;
import com.payqwikapp.repositories.FlightTravellerDetailsRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.StartupUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FlightApi implements IFlightApi {
	private ITransactionApi transactionApi;
	private FlightDetailsRepository flightDetailsRepository;
	private PQTransactionRepository pqTransactionRepository;
	private PQServiceRepository pqServiceRepository;
	private UserRepository userRepository;
	private final IMailSenderApi mailSenderApi;
	private final ISMSSenderApi smsSenderApi;

	private FlightTicketRepository flightTicketRepository;
	private FlightTravellerDetailsRepository flightTravellerDetailsRepository;
	private FlightListRepository flightListRepository;
	private final ICommissionApi commissionApi;

	private final SimpleDateFormat sdf = new SimpleDateFormat("DD-mm-YYYY HH:mm:ss");
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private static final String FLIGHT_CITY_LIST =DeploymentConstants.WEB_URL+"/Api/v1/User/Android/en/Treval/Flight/SaveAirLineInDbByCron";

//	private static final String pdfUrlLive="/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/classes/HelloWorld.pdf";
	
	private static final String pdfUrlLive="/usr/local/tomcat1/webapps/ROOT/WEB-INF/classes/HelloWorld.pdf";
//	private static final String pdfUrlLive="D:/All App/payqwikapp/src/main/resources/HelloWorld.pdf";
	
	public FlightApi(ITransactionApi transactionApi, FlightDetailsRepository flightDetailsRepository,
			PQTransactionRepository pqTransactionRepository, PQServiceRepository pqServiceRepository,
			UserRepository userRepository, IMailSenderApi mailSenderApi, ISMSSenderApi smsSenderApi,
			FlightTicketRepository flightTicketRepository,FlightTravellerDetailsRepository flightTravellerDetailsRepository,
			FlightListRepository flightListRepository,ICommissionApi commissionApi) {
		super();
		this.transactionApi = transactionApi;
		this.flightDetailsRepository = flightDetailsRepository;
		this.pqTransactionRepository = pqTransactionRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.userRepository = userRepository;
		this.mailSenderApi = mailSenderApi;
		this.smsSenderApi = smsSenderApi;
		this.flightTicketRepository=flightTicketRepository;
		this.flightTravellerDetailsRepository=flightTravellerDetailsRepository;
		this.flightListRepository=flightListRepository;
		this.commissionApi=commissionApi;
	}


	@Override
	public FlightPaymentResponse placeOrder(FlightPaymentRequest order, PQService service, User senderUser) {
		/*String transactionRefNo = String.valueOf(System.currentTimeMillis());
		String pnr = getUniquePnrNo();*/
		FlightPaymentResponse response = new FlightPaymentResponse();
/*		PQTransaction valid = initiateTransaction(transactionRefNo, order.getPaymentAmount(), service, senderUser);
		if (valid != null) {
			FlightDetails orders = saveFlightBooking(order, valid, senderUser);
			if (orders != null) {
				response.setTransactionRefNo(transactionRefNo);
				response.setValid(true);
				response.setMessage("Transaction Initiated");
			} else {
				response.setValid(false);
				response.setMessage("Order not saved,Try again later");
			}
		} else {
			response.setValid(false);
			response.setMessage("Transaction  not saved,Try again later");
		}*/
		return response;
	}

	@Override
	public PQTransaction initiateTransaction(String transactionRefNo, double amount, PQService service,
			User senderUser,String convenienceFee,String baseFare) {
		String description = "Booking of Flight of Rs." + amount;
		return transactionApi.initiateFlightPayment(amount, description, service, transactionRefNo, senderUser,
				StartupUtil.FLIGHT_BOOKING,convenienceFee,baseFare);
	}

	@Override
	public FlightPaymentResponse processPayment(FlightPaymentRequest dto, User user) throws Exception {
		FlightPaymentResponse response = new FlightPaymentResponse();
		if (dto.isSuccess()) {
			PQTransaction senderTransaction = transactionApi.successFlightPayment(dto.getTransactionRefno());
			if (senderTransaction != null) {
				if (senderTransaction.getStatus().equals(Status.Success)) {
					FlightDetails orders = flightDetailsRepository.getByTransaction(senderTransaction);
					if (orders != null) {
						orders.setFirstName(dto.getFirstName());
						orders.setPaymentAmount(dto.getPaymentAmount());
						orders.setFlightStatus(dto.getStatusflight());
						orders.setPaymentstatus(dto.getPaymentstatus());
						orders.setPaymentmethod(dto.getPaymentmethod());
						orders.setTicketDetails(dto.getTicketDetails());
						orders.setBookingRefId(dto.getBookingRefId());
						orders.setTicketNumber(dto.getTicketNumber());
						orders.setTransactionRefNomdex(dto.getTransactionRefno());
						orders.setUser(user);
						orders.setTransaction(senderTransaction);
						flightDetailsRepository.save(orders);
						response.setValid(true);
						response.setMessage("Transaction Successful");

						JSONObject obj = new JSONObject(dto.getTicketDetails());
						String	ticketobj = null;
						if(obj!=null){
							ticketobj = JSONParserUtil.getString(obj, "Tickets");

						}
						String unescape = StringEscapeUtils.unescapeJava(ticketobj);
						System.out.println(unescape);
						boolean roundway=false;
						JSONObject obj1 = new JSONObject(unescape);
						JSONArray legs = JSONParserUtil.getArray(obj1, "Oneway");
						JSONArray legs1 =  JSONParserUtil.getArray(obj1, "Roundway");

						List<FlightResponseEmail> flightresponse = new ArrayList<FlightResponseEmail>();
						List<FlightResponseEmail> flightresponsearrreturn = new ArrayList<FlightResponseEmail>();
						List<FlightTicketdtoForname> nameandticket = new ArrayList<FlightTicketdtoForname>();

						flightresponsearrreturn.clear();
						flightresponse.clear();
						for (int k = 0; k < legs.length(); k++) {
							FlightResponseEmail flightresponseobj = new FlightResponseEmail();
							//flightresponseobj.setJourneyTime("" + legs.getJSONObject(k).getString("journeyTime"));
							flightresponseobj.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
							flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
							flightresponseobj.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
							flightresponseobj.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
							flightresponseobj.setCabin("" + legs.getJSONObject(k).getString("cabin"));
							flightresponseobj.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
							flightresponseobj.setDestination("" + legs.getJSONObject(k).getString("destination"));
							flightresponseobj.setDuration("" + legs.getJSONObject(k).getString("duration"));
							flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setOrigin("" + legs.getJSONObject(k).getString("origin"));
							flightresponseobj.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
							flightresponseobj.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
							flightresponse.add(flightresponseobj);
						}
						for (int k = 0; k < legs1.length(); k++) {
							System.err.println(k);
							roundway=true;
							FlightResponseEmail flightresponseobj = new FlightResponseEmail();
							//flightresponseobj
							//.setJourneyTimereturn("" + legs1.getJSONObject(k).getString("journeyTime"));
							flightresponseobj
							.setDepartureDatereturn("" + legs1.getJSONObject(k).getString("departureDate"));
							flightresponseobj
							.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
							flightresponseobj
							.setArrivalDatereturn("" + legs1.getJSONObject(k).getString("arrivalDate"));
							flightresponseobj
							.setAirlineNamereturn("" + legs1.getJSONObject(k).getString("airlineName"));
							flightresponseobj
							.setArrivalTimereturn("" + legs1.getJSONObject(k).getString("arrivalTime"));
							flightresponseobj.setCabinreturn("" + legs1.getJSONObject(k).getString("cabin"));
							flightresponseobj
							.setDepartureTimereturn("" + legs1.getJSONObject(k).getString("departureTime"));
							flightresponseobj
							.setDestinationreturn("" + legs1.getJSONObject(k).getString("destination"));
							flightresponseobj.setDurationreturn("" + legs1.getJSONObject(k).getString("duration"));
							flightresponseobj
							.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setOriginreturn("" + legs1.getJSONObject(k).getString("origin"));
							flightresponseobj
							.setBaggageUnitreturn("" + legs1.getJSONObject(k).getString("baggageUnit"));
							flightresponseobj
							.setBaggageWeightreturn("" + legs1.getJSONObject(k).getString("baggageWeight"));
							flightresponsearrreturn.add(flightresponseobj);
						}

						String ticketnumber[] = dto.getTicketNumber().split("~");
						String firstName[] = dto.getFirstName().split("~");

						for (int i = 0; i < firstName.length; i++) {
							FlightTicketdtoForname firstNameticket=new FlightTicketdtoForname();
							firstNameticket.setFname(firstName[i]);
							firstNameticket.setTicketnumbernumber(ticketnumber[i]);
							nameandticket.add(firstNameticket);
						}


						TicketDetailsDTOFlight additionalInfo = new TicketDetailsDTOFlight();
						additionalInfo.setBookingRefId(dto.getBookingRefId());
						additionalInfo.setFlightresponse(flightresponse);
						additionalInfo.setFlightresponsearrreturn(flightresponsearrreturn);
						additionalInfo.setNameandticket(nameandticket);
						additionalInfo.setPaymentAmount(dto.getPaymentAmount());


						user.getUserDetail().setEmail(dto.getEmail());
						user.setUsername(dto.getMobile());
						

						mailSenderApi.sendFlightTicketMail("VPayQwik Flight Ticket Booking",
								MailTemplate.FLIGHTBOOKING_SUCCESS, user, senderTransaction, additionalInfo);

						smsSenderApi.sendFlightTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FLIGHTTICKET_SUCCESS, user, senderTransaction, orders);

					} else {
						response.setValid(false);
						response.setMessage("Order Details not found");
					}
				} else {
					response.setValid(false);
					response.setMessage("Transaction is " + senderTransaction.getStatus());
				}
			} else {
				response.setValid(false);
				response.setMessage("Transaction not found with this reference no");
			}
		} else {
			PQTransaction senderTransaction=transactionApi.getTransactionByRefNo(dto.getTransactionRefno()+"D");
			transactionApi.failedFlightPayment(dto.getTransactionRefno());
			FlightDetails orders = flightDetailsRepository.getByTransaction(senderTransaction);
			if (orders != null) {
				orders.setFirstName(dto.getFirstName());
				orders.setPaymentAmount(dto.getPaymentAmount());
				orders.setFlightStatus(dto.getStatusflight());
				orders.setPaymentstatus(dto.getPaymentstatus());
				orders.setPaymentmethod(dto.getPaymentmethod());
				flightDetailsRepository.save(orders);
			}
			response.setValid(false);
			response.setMessage("Transaction Failed");
		}
		return response;
	}

	@Override
	public FlightDetails saveFlightBooking(FlightPaymentRequest order, PQTransaction transaction, User user) {
		FlightDetails orders = flightDetailsRepository.getByTransaction(transaction);
		if (orders == null) {
			orders = new FlightDetails();
			orders.setFirstName(order.getFirstName());
			orders.setPaymentAmount(order.getPaymentAmount());
			orders.setFlightStatus(Status.Processing);
			orders.setPaymentstatus(Status.Initiated);
			orders.setPaymentmethod(order.getPaymentmethod());
			orders.setTicketDetails(order.getTicketDetails());
			orders.setTransaction(transaction);
			orders.setUser(user);
			flightDetailsRepository.save(orders);
		}
		return orders;
	}

	public static String getUniquePnrNo() {
		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder((100000 + rnd.nextInt(900000)) + "-");
		for (int i = 0; i < 5; i++)
			sb.append(chars[rnd.nextInt(chars.length)]);

		return sb.toString();

	}

	@Override
	public List<FlightDetailsDTO> getFlightDetailsForAdmin() {
		List<FlightDetailsDTO> flightDetailsList = new ArrayList<>();
		PQService sevice = pqServiceRepository.findServiceByCode(StartupUtil.FLIGHT_CODE);
		List<PQTransaction> list = pqTransactionRepository.findByServiceFlight(sevice);
		if (list != null) {
			for (PQTransaction pqTransaction : list) {
				FlightDetails details = flightDetailsRepository.getByTransaction(pqTransaction);
				User user = userRepository.getByAccount(pqTransaction.getAccount());
				if (details != null) {
					FlightDetailsDTO dto = new FlightDetailsDTO();
					dto.setAmount(details.getPaymentAmount());
					dto.setBookingStatus(details.getFlightStatus().getValue());
					if (Status.Booked.getValue().equalsIgnoreCase(details.getFlightStatus().getValue())) {
						dto.setBookingRefNo(details.getBookingRefId());
						dto.setTicketNumber(details.getTicketNumber());
					} else {
						dto.setBookingRefNo("NA");
						dto.setTicketNumber("NA");
					}
					dto.setContactNo(user.getUsername());

					dto.setTransactionRefNo(pqTransaction.getTransactionRefNo());
					dto.setTransactionStatus(pqTransaction.getStatus().getValue());
					dto.setUsername(user.getUserDetail().getFirstName());
					dto.setDate(dateFormat.format(details.getCreated()));
					flightDetailsList.add(dto);
				}
			}
		}
		return flightDetailsList;
	}

	@Override
	public void processPaymentGateWaySuccess(FlightPaymentRequest dto, User u) throws JSONException {
		PQTransaction senderTransaction = pqTransactionRepository
				.findByTransactionRefNo("FL" + dto.getTransactionRefno() + "D");
		if (senderTransaction != null) {
			if (senderTransaction.getStatus().equals(Status.Booked)) {
				FlightDetails orders = flightDetailsRepository.getByTransaction(senderTransaction);
				if (orders == null) {
					orders = new FlightDetails();
					orders.setFirstName(dto.getFirstName());
					orders.setPaymentAmount(dto.getPaymentAmount());
					orders.setFlightStatus(dto.getStatusflight());
					orders.setPaymentstatus(dto.getPaymentstatus());
					orders.setPaymentmethod(dto.getPaymentmethod());
					orders.setTicketDetails(dto.getTicketDetails());
					orders.setBookingRefId(dto.getBookingRefId());
					orders.setTicketNumber(dto.getTicketNumber());
					orders.setTransactionRefNomdex(dto.getTransactionRefno());
					orders.setUser(u);
					orders.setTransaction(senderTransaction);
					flightDetailsRepository.save(orders);

					JSONObject obj = new JSONObject(dto.getTicketDetails());
					String	ticketobj = null;
					if(obj!=null){
						ticketobj = JSONParserUtil.getString(obj, "Tickets");

					}
					String unescape = StringEscapeUtils.unescapeJava(ticketobj);
					System.out.println(unescape);
					JSONObject obj1 = new JSONObject(unescape);
					JSONArray legs = JSONParserUtil.getArray(obj1, "Oneway");
					JSONArray legs1 =  JSONParserUtil.getArray(obj1, "Roundway");
					List<FlightResponseEmail> flightresponse = new ArrayList<FlightResponseEmail>();
					List<FlightResponseEmail> flightresponsearrreturn = new ArrayList<FlightResponseEmail>();
					List<FlightTicketdtoForname> nameandticket = new ArrayList<FlightTicketdtoForname>();

					flightresponsearrreturn.clear();
					flightresponse.clear();
					for (int k = 0; k < legs.length(); k++) {
						FlightResponseEmail flightresponseobj = new FlightResponseEmail();
						//flightresponseobj.setJourneyTime("" + legs.getJSONObject(k).getString("journeyTime"));
						flightresponseobj.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
						flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
						flightresponseobj.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
						flightresponseobj.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
						flightresponseobj.setCabin("" + legs.getJSONObject(k).getString("cabin"));
						flightresponseobj.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
						flightresponseobj.setDestination("" + legs.getJSONObject(k).getString("destination"));
						flightresponseobj.setDuration("" + legs.getJSONObject(k).getString("duration"));
						flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setOrigin("" + legs.getJSONObject(k).getString("origin"));
						flightresponseobj.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
						flightresponseobj.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
						flightresponse.add(flightresponseobj);
					}
					for (int k = 0; k < legs1.length(); k++) {
						System.err.println(k);
						FlightResponseEmail flightresponseobj = new FlightResponseEmail();
						//flightresponseobj
						//.setJourneyTimereturn("" + legs1.getJSONObject(k).getString("journeyTime"));
						flightresponseobj
						.setDepartureDatereturn("" + legs1.getJSONObject(k).getString("departureDate"));
						flightresponseobj
						.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
						flightresponseobj
						.setArrivalDatereturn("" + legs1.getJSONObject(k).getString("arrivalDate"));
						flightresponseobj
						.setAirlineNamereturn("" + legs1.getJSONObject(k).getString("airlineName"));
						flightresponseobj
						.setArrivalTimereturn("" + legs1.getJSONObject(k).getString("arrivalTime"));
						flightresponseobj.setCabinreturn("" + legs1.getJSONObject(k).getString("cabin"));
						flightresponseobj
						.setDepartureTimereturn("" + legs1.getJSONObject(k).getString("departureTime"));
						flightresponseobj
						.setDestinationreturn("" + legs1.getJSONObject(k).getString("destination"));
						flightresponseobj.setDurationreturn("" + legs1.getJSONObject(k).getString("duration"));
						flightresponseobj
						.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setOriginreturn("" + legs1.getJSONObject(k).getString("origin"));
						flightresponseobj
						.setBaggageUnitreturn("" + legs1.getJSONObject(k).getString("baggageUnit"));
						flightresponseobj
						.setBaggageWeightreturn("" + legs1.getJSONObject(k).getString("baggageWeight"));
						flightresponsearrreturn.add(flightresponseobj);
					}

					String ticketnumber[] = dto.getTicketNumber().split("~");
					String firstName[] = dto.getFirstName().split("~");
					for (int i = 0; i < firstName.length; i++) {
						FlightTicketdtoForname firstNameticket=new FlightTicketdtoForname();
						firstNameticket.setFname(firstName[i]);
						firstNameticket.setTicketnumbernumber(ticketnumber[i]);
						nameandticket.add(firstNameticket);
					}

					TicketDetailsDTOFlight additionalInfo = new TicketDetailsDTOFlight();
					additionalInfo.setBookingRefId(dto.getBookingRefId());
					additionalInfo.setFlightresponse(flightresponse);
					additionalInfo.setFlightresponsearrreturn(flightresponsearrreturn);
					additionalInfo.setNameandticket(nameandticket);
					additionalInfo.setPaymentAmount(dto.getPaymentAmount());


					u.getUserDetail().setEmail(dto.getEmail());
					u.setUsername(dto.getMobile());

					System.err.println("Ticket Email:: "+u.getUserDetail().getEmail());
					System.err.println("Ticket Mobile:: "+u.getUsername());

					mailSenderApi.sendFlightTicketMail("VPayQwik Flight Ticket Booking",
							MailTemplate.FLIGHTBOOKING_SUCCESS, u, senderTransaction, additionalInfo);

					smsSenderApi.sendFlightTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FLIGHTTICKET_SUCCESS, u, senderTransaction, orders);



					/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FLIGHT_LOADMONEY_SUCCESS, u, senderTransaction, null);*/
				}
			}
		}
	}


	@Override
	public void processPaymentGateWayFailure(FlightPaymentRequest dto, User u) {
		PQTransaction senderTransaction = pqTransactionRepository
				.findByTransactionRefNo("FL" + dto.getTransactionRefno() + "D");
		if (senderTransaction != null) {
			if (senderTransaction.getStatus().equals(Status.Booked)) {
				FlightDetails orders = flightDetailsRepository.getByTransaction(senderTransaction);
				if (orders == null) {
					orders = new FlightDetails();
					orders.setFirstName(dto.getFirstName());
					orders.setPaymentAmount(dto.getPaymentAmount());
					orders.setFlightStatus(dto.getStatusflight());
					orders.setPaymentstatus(dto.getPaymentstatus());
					orders.setPaymentmethod(dto.getPaymentmethod());
					orders.setTicketDetails(dto.getTicketDetails());
					orders.setTicketNumber(dto.getTicketNumber());
					orders.setBookingRefId(dto.getBookingRefId());
					orders.setTransactionRefNomdex(dto.getTransactionRefno());
					orders.setUser(u);
					orders.setTransaction(senderTransaction);

					flightDetailsRepository.save(orders);
				}
			}
		}
	}


	/* Code Done By Rohit End */


	/*##############################################################################################################*/

	/* Fresh Code Started By Subir */

	@Override
	public FlightPaymentResponse flightInit(FlightPayment order, PQService service, User senderUser) {
		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		FlightPaymentResponse response = new FlightPaymentResponse();
		PQTransaction valid = initiateTransaction(transactionRefNo, order.getPaymentAmount(), service, senderUser,
				order.getConvenienceFee(),order.getBaseFare());
		if (valid != null) {
			FlightTicket orders = saveFlight(order, valid, senderUser,order.getBaseFare());
			if (orders != null) {
				response.setTransactionRefNo(transactionRefNo);
				response.setValid(true);
				response.setMessage("Transaction Initiated");

			} else {
				response.setValid(false);
				response.setMessage("Order not saved,Try again later");
			}
		} else {
			response.setValid(false);
			response.setMessage("Transaction  not saved,Try again later");
		}
		return response;
	}
	@Override
	public FlightTicket saveFlight(FlightPayment order, PQTransaction transaction, User user,String baseFare) {
		FlightTicket orders = flightTicketRepository.getByTransaction(transaction);
		Date ageOfTraveller=null;
		if (orders == null) {
			double comAmt=Double.parseDouble(baseFare);
			PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(transaction.getService(), comAmt);
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, comAmt);
			orders = new FlightTicket();
			orders.setPaymentAmount(order.getPaymentAmount());
			orders.setFlightStatus(Status.Processing);
			orders.setPaymentStatus(Status.Initiated);
			orders.setPaymentMethod(order.getPaymentMethod());
			orders.setTicketDetails(order.getTicketDetails());
			String firstAirlineName="";
			String secondAirlineName="";
			String tripType="";
			try {
				if (order.getTicketDetails()!=null && !order.getTicketDetails().isEmpty() &&  !order.getTicketDetails().equalsIgnoreCase("null")) {
				JSONObject obj = new JSONObject(order.getTicketDetails());
				JSONObject ticketDetails=obj.getJSONObject("Tickets");
				JSONArray oneWay=ticketDetails.getJSONArray("Oneway");
				JSONArray roundWay=ticketDetails.getJSONArray("Roundway");
				if (roundWay.length()==0) {
					tripType="OneWay";
				}
				for (int i = 0; i < oneWay.length(); i++) {
					JSONObject objd=oneWay.getJSONObject(i);
					if (i==0) {
						firstAirlineName=firstAirlineName+objd.get("airlineName");
					}
					else{
						firstAirlineName=firstAirlineName+" | "+objd.get("airlineName");
					}
				}
				for (int i = 0; i < roundWay.length(); i++) {
					tripType="RoundTrip";
					JSONObject objd=roundWay.getJSONObject(i);
					if (i==0) {
						secondAirlineName=secondAirlineName+objd.get("airlineName");
					}
					else{
						secondAirlineName=secondAirlineName+" | "+objd.get("airlineName");
					}
				}
			}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			orders.setTransaction(transaction);
			orders.setMobile(order.getMobile());
			orders.setEmail(order.getEmail());
			orders.setCommissionAmt(netCommissionValue+200);
			orders.setUser(user);
			orders.setFlightNumberOnward(firstAirlineName);
			orders.setFlightNumberReturn(secondAirlineName);
			orders.setTripType(tripType);
			orders.setBaseFare(Double.parseDouble(baseFare));
			flightTicketRepository.save(orders);
			try{
			if (order.getTravellerDetails()!=null) {
				for (int i = 0; i < order.getTravellerDetails().size(); i++) {
					FlightTravellers travellerDetails=new FlightTravellers();
					travellerDetails.setfName(order.getTravellerDetails().get(i).getfName());
					travellerDetails.setlName(order.getTravellerDetails().get(i).getlName());
					if(order.getTravellerDetails().get(i).getAge()!=null && 
							!order.getTravellerDetails().get(i).getAge().isEmpty()){
					ageOfTraveller=format.parse(order.getTravellerDetails().get(i).getAge());
					travellerDetails.setAge(String.valueOf(CommonUtil.calculateAge(ageOfTraveller)));
					}
					travellerDetails.setGender(order.getTravellerDetails().get(i).getGender());
					travellerDetails.setFare(order.getTravellerDetails().get(i).getFare());
					travellerDetails.setType(order.getTravellerDetails().get(i).getTravellerType());
					travellerDetails.setTicketNo(order.getTravellerDetails().get(i).getTicketNo());
					travellerDetails.setFlightTicket(orders);
					flightTravellerDetailsRepository.save(travellerDetails);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		return orders;
	}

	@Override
	public FlightPaymentResponse flightPayment(FlightPayment dto, User user) throws Exception {
		FlightPaymentResponse response = new FlightPaymentResponse();
		if (dto.isSuccess()) {
			PQTransaction senderTransaction = transactionApi.successFlightPayment(dto.getTxnRefno());
			if (senderTransaction != null) {
				if (senderTransaction.getStatus().equals(Status.Success)) {
					FlightTicket orders = flightTicketRepository.getByTransaction(senderTransaction);
					if (orders != null) {
						orders.setPaymentAmount(dto.getPaymentAmount());
						if (dto.getFlightStatus()!=null) {
							orders.setFlightStatus(dto.getFlightStatus());
						}
						orders.setPaymentStatus(Status.Success);
						orders.setPaymentMethod(dto.getPaymentMethod());
						orders.setTicketDetails(dto.getTicketDetails());
						orders.setBookingRefId(dto.getBookingRefId());
						orders.setMdexTxnRefNo(dto.getMdexTxnRefNo());
						orders.setMobile(dto.getMobile());
						orders.setEmail(dto.getEmail());
						orders.setUser(user);
						orders.setTransaction(senderTransaction);
						flightTicketRepository.save(orders);
						List<FlightTravellers> travellerDetails=flightTravellerDetailsRepository.getTravellersByTicket(orders);
						for (int i = 0; i < dto.getTravellerDetails().size(); i++) {
							travellerDetails.get(i).setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
							flightTravellerDetailsRepository.save(travellerDetails.get(i));
						}
						response.setValid(true);
						response.setMessage("Transaction Successful");
						JSONObject obj = new JSONObject(dto.getTicketDetails());
						String	ticketobj = null;
						if(obj!=null){
							ticketobj = JSONParserUtil.getString(obj, "Tickets");
						}
						String unescape = StringEscapeUtils.unescapeJava(ticketobj);
						JSONObject obj1 = new JSONObject(unescape);
						JSONArray legs = JSONParserUtil.getArray(obj1, "Oneway");
						JSONArray legs1 =  JSONParserUtil.getArray(obj1, "Roundway");
						List<FlightResponseEmail> flightresponse = new ArrayList<FlightResponseEmail>();
						List<FlightResponseEmail> flightresponsearrreturn = new ArrayList<FlightResponseEmail>();
						List<FlightTicketdtoForname> nameandticket = new ArrayList<FlightTicketdtoForname>();
						flightresponsearrreturn.clear();
						flightresponse.clear();
						for (int k = 0; k < legs.length(); k++) {
							FlightResponseEmail flightresponseobj = new FlightResponseEmail();
							flightresponseobj.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
							flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
							flightresponseobj.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
							flightresponseobj.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
							flightresponseobj.setCabin("" + legs.getJSONObject(k).getString("cabin"));
							flightresponseobj.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
							flightresponseobj.setDestination("" + legs.getJSONObject(k).getString("destination"));
							flightresponseobj.setDuration("" + legs.getJSONObject(k).getString("duration"));
							flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setOrigin("" + legs.getJSONObject(k).getString("origin"));
							flightresponseobj.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
							flightresponseobj.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
							flightresponse.add(flightresponseobj);
						}
						for (int k = 0; k < legs1.length(); k++) {
							System.err.println(k);
							FlightResponseEmail flightresponseobj = new FlightResponseEmail();
							flightresponseobj
							.setDepartureDatereturn("" + legs1.getJSONObject(k).getString("departureDate"));
							flightresponseobj
							.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
							flightresponseobj
							.setArrivalDatereturn("" + legs1.getJSONObject(k).getString("arrivalDate"));
							flightresponseobj
							.setAirlineNamereturn("" + legs1.getJSONObject(k).getString("airlineName"));
							flightresponseobj
							.setArrivalTimereturn("" + legs1.getJSONObject(k).getString("arrivalTime"));
							flightresponseobj.setCabinreturn("" + legs1.getJSONObject(k).getString("cabin"));
							flightresponseobj
							.setDepartureTimereturn("" + legs1.getJSONObject(k).getString("departureTime"));
							flightresponseobj
							.setDestinationreturn("" + legs1.getJSONObject(k).getString("destination"));
							flightresponseobj.setDurationreturn("" + legs1.getJSONObject(k).getString("duration"));
							flightresponseobj
							.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
							flightresponseobj.setOriginreturn("" + legs1.getJSONObject(k).getString("origin"));
							flightresponseobj
							.setBaggageUnitreturn("" + legs1.getJSONObject(k).getString("baggageUnit"));
							flightresponseobj
							.setBaggageWeightreturn("" + legs1.getJSONObject(k).getString("baggageWeight"));
							flightresponsearrreturn.add(flightresponseobj);
						}
						TicketDetailsDTOFlight additionalInfo = new TicketDetailsDTOFlight();
						additionalInfo.setBookingRefId(dto.getBookingRefId());
						additionalInfo.setFlightresponse(flightresponse);
						FlightAirLineList airlineList = flightListRepository.getCityByCode(additionalInfo.getFlightresponse().get(0).getOrigin());
						if(airlineList!=null){
							additionalInfo.setOriginName(airlineList.getCityName());
						}
						FlightAirLineList airlineList1 = flightListRepository.getCityByCode(additionalInfo.getFlightresponse().get(0).getDestination());
						if(airlineList1!=null){
							additionalInfo.setDestinationName(airlineList.getCityName());
						}
						additionalInfo.setFlightresponsearrreturn(flightresponsearrreturn);
						additionalInfo.setNameandticket(nameandticket);
						additionalInfo.setPaymentAmount(dto.getPaymentAmount());
						additionalInfo.setTravellerDetails(travellerDetails);
						additionalInfo.setPnrNo(dto.getPnrNo());
						user.getUserDetail().setEmail(dto.getEmail());
						user.setUsername(dto.getMobile());
						smsSenderApi.sendFlightTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FLIGHTTICKET_SUCCESS, user, senderTransaction, additionalInfo);
						mailSenderApi.sendFlightTicketMail("VPayQwik Flight Ticket Booking",
								MailTemplate.FLIGHTBOOKING_SUCCESS, user, senderTransaction, additionalInfo);
						List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(orders);
						test(list);
					} else {
						response.setValid(false);
						response.setMessage("Order Details not found");
					}
				} else {
					response.setValid(false);
					response.setMessage("Transaction is " + senderTransaction.getStatus());
				}
			} else {
				response.setValid(false);
				response.setMessage("Transaction not found with this reference no");
			}
		} else {
			try{
				PQTransaction senderTransaction=transactionApi.getTransactionByRefNo(dto.getTxnRefno()+"D");
				transactionApi.failedFlightPayment(dto.getTxnRefno());
				FlightDetails orders = flightDetailsRepository.getByTransaction(senderTransaction);
				if (orders != null) {
					orders.setPaymentAmount(dto.getPaymentAmount());
					orders.setFlightStatus(dto.getFlightStatus());
					orders.setPaymentstatus(dto.getPaymentStatus());
					orders.setPaymentmethod(dto.getPaymentMethod());
					flightDetailsRepository.save(orders);
				}
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("Unable to reversed amount");
			}
			response.setValid(false);
			response.setMessage("Transaction Failed");
		}
		return response;
	}

	

	@Override
	public FlightPaymentResponse paymentGateWaySuccess(FlightPayment dto, User user) throws JSONException {

		FlightPaymentResponse response = new FlightPaymentResponse();


		PQTransaction senderTransaction = pqTransactionRepository
				.findByTransactionRefNo("FL" + dto.getMerchantRefNo() + "D");
		if (senderTransaction != null) {

			if (senderTransaction.getStatus().equals(Status.Booked)) {
				FlightTicket orders = flightTicketRepository.getByTransaction(senderTransaction);
				if (orders != null) {
					orders.setPaymentAmount(dto.getPaymentAmount());
					if (dto.getFlightStatus()!=null) {
						orders.setFlightStatus(dto.getFlightStatus());
					}
					orders.setPaymentStatus(Status.Success);
					orders.setPaymentMethod(dto.getPaymentMethod());
					orders.setTicketDetails(dto.getTicketDetails());
					orders.setBookingRefId(dto.getBookingRefId());
					orders.setMdexTxnRefNo(dto.getMdexTxnRefNo());
					orders.setMobile(dto.getMobile());
					orders.setEmail(dto.getEmail());
					JSONObject obj = new JSONObject(dto.getTicketDetails());
					String	ticketobj = null;
					if(obj!=null){
						ticketobj = JSONParserUtil.getString(obj, "Tickets");
					}
					String unescape = StringEscapeUtils.unescapeJava(ticketobj);
					System.out.println(unescape);
					JSONObject obj1 = new JSONObject(unescape);
					JSONArray oneway = JSONParserUtil.getArray(obj1, "Oneway");
					JSONArray roundway =  JSONParserUtil.getArray(obj1, "Roundway");
					
					String firstName="";
					String secoundName="";
					String tripType="";
					try {
						if (roundway.length()==0) {
							tripType="OneWay";
						}
						for (int i = 0; i < oneway.length(); i++) {
							JSONObject objd=oneway.getJSONObject(i);
							if (i==0) {
								firstName=firstName+objd.get("airlineName");
							}
							else{
								firstName=firstName+" | "+objd.get("airlineName");
							}
						}
						for (int i = 0; i < roundway.length(); i++) {
							tripType="RoundTrip";
							JSONObject objd=roundway.getJSONObject(i);
							if (i==0) {
								secoundName=secoundName+objd.get("airlineName");
							}
							else{
								secoundName=secoundName+" | "+objd.get("airlineName");
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					orders.setFlightNumberOnward(firstName);
					orders.setFlightNumberReturn(secoundName);
					orders.setTripType(tripType);
					orders.setUser(user);
					orders.setTransaction(senderTransaction);

					flightTicketRepository.save(orders);

					List<FlightTravellers> travellerDetailsArr=new ArrayList<>();
					List<FlightTravellers> travellerDetail=flightTravellerDetailsRepository.getTravellersByTicket(orders);
					if (!travellerDetail.isEmpty()) {

						for (int i = 0; i < dto.getTravellerDetails().size(); i++) {

							travellerDetail.get(i).setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
							travellerDetailsArr.add(travellerDetail.get(i));
							flightTravellerDetailsRepository.save(travellerDetail.get(i));
						}
					}
					else{
						if (dto.getTravellerDetails()!=null) {
							for (int i = 0; i < dto.getTravellerDetails().size(); i++) {
								FlightTravellers travellerDetails=new FlightTravellers();
								travellerDetails.setfName(dto.getTravellerDetails().get(i).getfName());
								travellerDetails.setlName(dto.getTravellerDetails().get(i).getlName());
								travellerDetails.setAge(dto.getTravellerDetails().get(i).getAge());
								travellerDetails.setGender(dto.getTravellerDetails().get(i).getGender());
								travellerDetails.setFare(dto.getTravellerDetails().get(i).getFare());
								travellerDetails.setType(dto.getTravellerDetails().get(i).getTravellerType());
								travellerDetails.setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
								travellerDetails.setFlightTicket(orders);
								travellerDetailsArr.add(travellerDetails);
								flightTravellerDetailsRepository.save(travellerDetails);
							}
						}
					}
					response.setValid(true);
					response.setMessage("Transaction Successful");

					

					List<FlightResponseEmail> flightresponse = new ArrayList<FlightResponseEmail>();
					List<FlightResponseEmail> flightresponsearrreturn = new ArrayList<FlightResponseEmail>();
					List<FlightTicketdtoForname> nameandticket = new ArrayList<FlightTicketdtoForname>();

					flightresponsearrreturn.clear();
					flightresponse.clear();
					for (int k = 0; k < oneway.length(); k++) {
						FlightResponseEmail flightresponseobj = new FlightResponseEmail();
						//flightresponseobj.setJourneyTime("" + legs.getJSONObject(k).getString("journeyTime"));
						flightresponseobj.setDepartureDate("" + oneway.getJSONObject(k).getString("departureDate"));
						flightresponseobj.setFlightNumber("" + oneway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setArrivalDate("" + oneway.getJSONObject(k).getString("arrivalDate"));
						flightresponseobj.setAirlineName("" + oneway.getJSONObject(k).getString("airlineName"));
						flightresponseobj.setArrivalTime("" + oneway.getJSONObject(k).getString("arrivalTime"));
						flightresponseobj.setCabin("" + oneway.getJSONObject(k).getString("cabin"));
						flightresponseobj.setDepartureTime("" + oneway.getJSONObject(k).getString("departureTime"));
						flightresponseobj.setDestination("" + oneway.getJSONObject(k).getString("destination"));
						flightresponseobj.setDuration("" + oneway.getJSONObject(k).getString("duration"));
						flightresponseobj.setFlightNumber("" + oneway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setOrigin("" + oneway.getJSONObject(k).getString("origin"));
						flightresponseobj.setBaggageUnit("" + oneway.getJSONObject(k).getString("baggageUnit"));
						flightresponseobj.setBaggageWeight("" + oneway.getJSONObject(k).getString("baggageWeight"));
						flightresponse.add(flightresponseobj);
					}
					for (int k = 0; k < roundway.length(); k++) {
						System.err.println(k);
						FlightResponseEmail flightresponseobj = new FlightResponseEmail();
						//flightresponseobj.setJourneyTimereturn("" + legs1.getJSONObject(k).getString("journeyTime"));
						flightresponseobj
						.setDepartureDatereturn("" + roundway.getJSONObject(k).getString("departureDate"));
						flightresponseobj
						.setFlightNumberreturn("" + roundway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj
						.setArrivalDatereturn("" + roundway.getJSONObject(k).getString("arrivalDate"));
						flightresponseobj
						.setAirlineNamereturn("" + roundway.getJSONObject(k).getString("airlineName"));
						flightresponseobj
						.setArrivalTimereturn("" + roundway.getJSONObject(k).getString("arrivalTime"));
						flightresponseobj.setCabinreturn("" + roundway.getJSONObject(k).getString("cabin"));
						flightresponseobj
						.setDepartureTimereturn("" + roundway.getJSONObject(k).getString("departureTime"));
						flightresponseobj
						.setDestinationreturn("" + roundway.getJSONObject(k).getString("destination"));
						flightresponseobj.setDurationreturn("" + roundway.getJSONObject(k).getString("duration"));
						flightresponseobj
						.setFlightNumberreturn("" + roundway.getJSONObject(k).getString("flightNumber"));
						flightresponseobj.setOriginreturn("" + roundway.getJSONObject(k).getString("origin"));
						flightresponseobj
						.setBaggageUnitreturn("" + roundway.getJSONObject(k).getString("baggageUnit"));
						flightresponseobj
						.setBaggageWeightreturn("" + roundway.getJSONObject(k).getString("baggageWeight"));
						flightresponsearrreturn.add(flightresponseobj);
					}

					TicketDetailsDTOFlight additionalInfo = new TicketDetailsDTOFlight();
					additionalInfo.setBookingRefId(dto.getBookingRefId());
					additionalInfo.setFlightresponse(flightresponse);
					additionalInfo.setFlightresponsearrreturn(flightresponsearrreturn);
					additionalInfo.setNameandticket(nameandticket);
					additionalInfo.setPaymentAmount(dto.getPaymentAmount());
					additionalInfo.setTravellerDetails(travellerDetailsArr);
					additionalInfo.setPnrNo(dto.getPnrNo());
					
					user.getUserDetail().setEmail(dto.getEmail());
					user.setUsername(dto.getMobile());

					System.err.println("Ticket Email:: "+user.getUserDetail().getEmail());
					System.err.println("Ticket Mobile:: "+user.getUsername());

					List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(orders);
					test(list);

					mailSenderApi.sendFlightTicketMail("VPayQwik Flight Ticket Booking",
							MailTemplate.FLIGHTBOOKING_SUCCESS, user, senderTransaction, additionalInfo);

					smsSenderApi.sendFlightTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FLIGHTTICKET_SUCCESS, user, senderTransaction, additionalInfo);

				} else {
					response.setValid(false);
					response.setMessage("Order Details not found");
				}
			} else {
				response.setValid(false);
				response.setMessage("Transaction is " + senderTransaction.getStatus());
			}
		}
		return response;
	}



	@Override
	public void paymentGateWayFailure(FlightPayment dto, User u) {
		PQTransaction senderTransaction = pqTransactionRepository
				.findByTransactionRefNo("FL" + dto.getMerchantRefNo() + "D");
		if (senderTransaction != null) {
			if (senderTransaction.getStatus().equals(Status.Booked)) {
				FlightTicket orders = flightTicketRepository.getByTransaction(senderTransaction);
				if (orders != null) {
					orders.setPaymentAmount(dto.getPaymentAmount());
					orders.setFlightStatus(dto.getFlightStatus());
					orders.setPaymentStatus(Status.Success);
					orders.setPaymentMethod(dto.getPaymentMethod());
					orders.setTicketDetails(dto.getTicketDetails());
					orders.setBookingRefId(dto.getBookingRefId());
					orders.setMdexTxnRefNo(dto.getMdexTxnRefNo());
					orders.setTicketDetails(dto.getTicketDetails());
					orders.setMobile(dto.getMobile());
					orders.setEmail(dto.getEmail());
					orders.setUser(u);
					orders.setTransaction(senderTransaction);

					flightTicketRepository.save(orders);

					List<FlightTravellers> travellerDetailsArr=new ArrayList<>();

					if (dto.getTravellerDetails()!=null) {
						for (int i = 0; i < dto.getTravellerDetails().size(); i++) {
							FlightTravellers travellerDetails=new FlightTravellers();
							travellerDetails.setfName(dto.getTravellerDetails().get(i).getfName());
							travellerDetails.setlName(dto.getTravellerDetails().get(i).getlName());
							travellerDetails.setAge(dto.getTravellerDetails().get(i).getAge());
							travellerDetails.setGender(dto.getTravellerDetails().get(i).getGender());
							travellerDetails.setFare(dto.getTravellerDetails().get(i).getFare());
							travellerDetails.setType(dto.getTravellerDetails().get(i).getTravellerType());
							travellerDetails.setTicketNo(dto.getTravellerDetails().get(i).getTicketNo());
							travellerDetails.setFlightTicket(orders);
							travellerDetailsArr.add(travellerDetails);
							flightTravellerDetailsRepository.save(travellerDetails);
						}
					}
				}
			}
		}
	}

	@Override
	public List<FlghtTicketResp> getAllTickets(String userName) {

		List<FlghtTicketResp> list=new ArrayList<>();
		User user=userRepository.findByUsername(userName);
		PQAccountDetail accountDetail=user.getAccountDetail();
		List<FlightTicket> flightTickets=flightTicketRepository.getByStatusAndUser(Status.Initiated,user);
		for (int i = 0; i < flightTickets.size(); i++) {
			FlghtTicketResp resp=new FlghtTicketResp();
			FlightTicket flightTicketDB=flightTickets.get(i);
			List<FlightTravellers> travellerDetails2=flightTravellerDetailsRepository.getTravellersByTicket(flightTicketDB);
			resp.setFlightTicket(flightTicketDB);
			resp.setTravellerDetails(travellerDetails2);
			list.add(resp);
		}
		return list;
	}

	@Override
	public List<FlightTicket> getFlightDetailForAdmin() {
		List<FlightTicket> flightTickets=(List<FlightTicket>)flightTicketRepository.getAllTicket();
		return flightTickets;
	}

	@Override
	public void cronforSaveCityList() {
		int size=0;
		org.codehaus.jettison.json.JSONObject payload = new org.codehaus.jettison.json.JSONObject();
		try{
			payload.put("sessionId", "12345");
			Client client = Client.create();
			WebResource webResource = client.resource(FLIGHT_CITY_LIST);
			ClientResponse response = webResource.accept("application/json").type("application/json").header("hash", payload.toString()).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			JSONObject jobj=null;
			jobj = new JSONObject(strResponse);
			String code=null;
			code = jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails = null;
				strDetails = jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList = null;

					cList = jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {

						FlightAirLineList dto=new FlightAirLineList();

						dto.setCityCode((cList.getJSONObject(i).getString("cityCode")));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						dto.setAirportName(cList.getJSONObject(i).getString("airportName"));
						dto.setCountry(cList.getJSONObject(i).getString("country"));
						flightListRepository.save(dto);

						size++;
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public List<FlightAirLineList> getAllAirLineList() {

		List<FlightAirLineList> airLineLists=(List<FlightAirLineList>)flightListRepository.findAll();

		return airLineLists;
	}


	@Override
	public List<FlightTravellers> getFlightTravellersForAdmin(long flightTicketId) {

		FlightTicket flightTicket=flightTicketRepository.findOne(flightTicketId);

		List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(flightTicket);

		test(list);
		//pdfTest();
		return list;

	}
	
	@Override
	public TicketsResp getFlightTravellersNameForAdmin(long flightTicketId) {

		FlightTicket flightTicket=flightTicketRepository.findOne(flightTicketId);
		List<FlightTravellers> list=flightTravellerDetailsRepository.getTravellersByTicket(flightTicket);
	//	for(int i=0;i<list.size();i++){
		TicketsResp	 ticketDeatilsDTO= new TicketsResp(); 
			FlightTicket tickets=list.get(0).getFlightTicket();
			String ticketsStr = tickets.getTicketDetails();
			
			if (!ticketsStr.equalsIgnoreCase("null")) {

				ObjectMapper mapper = new ObjectMapper();
				try {
					ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.err.println("ticketDeatilsDTO:: "+ticketDeatilsDTO);
		//		flightTicketResp.setTicketsResp(ticketDeatilsDTO);
			
			    long timeDiff=0;
			    for(int j=0;j<ticketDeatilsDTO.getTickets().getOneway().size();j++){
			    	   String origin = ticketDeatilsDTO.getTickets().getOneway().get(j).getOrigin();
					    String desti = ticketDeatilsDTO.getTickets().getOneway().get(j).getDestination();
			
					FlightAirLineList originName=flightTicketRepository.getCityByCode(origin);
					FlightAirLineList destNmae =flightTicketRepository.getCityByCode(desti);
					ticketDeatilsDTO.getTickets().getOneway().get(j).setDestinationName(destNmae.getCityName());
					ticketDeatilsDTO.getTickets().getOneway().get(j).setSourceName(originName.getCityName());
			    }
		    }
		
	//   }
		//test(list);
		//pdfTest();
		return ticketDeatilsDTO;
	}

	


	public void test(List<FlightTravellers> list)
	{

		Font blueFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, new CMYKColor(255, 0, 0, 0));
		Font redFont = FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, new CMYKColor(0, 255, 0, 0));
		Font yellowFont = FontFactory.getFont(FontFactory.COURIER, 14, Font.BOLD, new CMYKColor(0, 0, 255, 0));
		Document document = new Document();

		try
		{
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfUrlLive));
			document.open();

			String img="https://www.vpayqwik.com/resources/images/vijayalogo.png";
			Image image2 = Image.getInstance(new URL(img));
			//image2.setAbsolutePosition(1f, 1f);
			image2.setAlignment(Image.RIGHT);
			image2.scaleAbsolute(50, 50);

			FlightTicket flightTicket= list.get(0).getFlightTicket();


			String ticktStr=flightTicket.getTicketDetails();

			JSONObject ticket=new JSONObject(ticktStr);
			ticket=ticket.getJSONObject("Tickets");
			JSONArray oneway=ticket.getJSONArray("Oneway");
			JSONArray roundway=ticket.getJSONArray("Roundway");
			String org=oneway.getJSONObject(0).getString("origin");
			String dest=oneway.getJSONObject(oneway.length()-1).getString("destination");

			FlightAirLineList origin=flightListRepository.getCityByCode(org);
			FlightAirLineList destination=flightListRepository.getCityByCode(dest);
			String bk=list.get(0).getFlightTicket().getBookingRefId();
			String bkd=list.get(0).getFlightTicket().getCreated().toString();
			String arr[]=bkd.split(" ");
			bkd=arr[0];
			PdfPTable table = new PdfPTable(2);

			table.setWidthPercentage(100);
			table.addCell(getCell("E-TICKET", PdfPCell.ALIGN_LEFT));
			table.addCell(image2);
			document.add(table);

			PdfPTable table2 = new PdfPTable(2);
			table2.setWidthPercentage(100);
			table2.addCell(getCell("Booking Ref Id: "+bk, PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("Customer Care: +918025011300", PdfPCell.ALIGN_LEFT));
			document.add(table2);

			PdfPTable table3= new PdfPTable(1);
			table3.setWidthPercentage(100);
			table3.addCell(getCell("Booked on: "+bkd, PdfPCell.ALIGN_LEFT));
			document.add(table3);

			document.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
			document.add(new Paragraph("Flight Details"));
			document.add(new Paragraph(origin.getCityName() +" to "+destination.getCityName()));

			PdfPTable table4= new PdfPTable(1);
			table4.addCell(getCell("Journey Date: "+oneway.getJSONObject(0).getString("departureDate"), PdfPCell.ALIGN_RIGHT));
			document.add(table4);
			document.add(new Paragraph(""));
			document.add( Chunk.NEWLINE );

			PdfPTable table5= new PdfPTable(2);


			for (int i = 0; i < oneway.length(); i++) {
				table5.addCell(getCell(oneway.getJSONObject(i).getString("origin"), PdfPCell.ALIGN_LEFT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("destination"),PdfPCell.ALIGN_RIGHT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("departureDate"), PdfPCell.ALIGN_LEFT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("departureDate"),PdfPCell.ALIGN_RIGHT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("departureTime"), PdfPCell.ALIGN_LEFT));
				table5.addCell(getCell(oneway.getJSONObject(i).getString("arrivalTime"), PdfPCell.ALIGN_RIGHT));
				table5.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
				table5.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			}

			document.add(table5);
			document.add(Chunk.NEWLINE);
			if (roundway.length()>0) {
				document.add(new Paragraph("Return"));
				document.add(Chunk.NEWLINE);
			}

			PdfPTable table0= new PdfPTable(2);

			for (int i = 0; i < roundway.length(); i++) {
				table0.addCell(getCell(roundway.getJSONObject(i).getString("origin"), PdfPCell.ALIGN_LEFT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("destination"),PdfPCell.ALIGN_RIGHT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("departureDate"), PdfPCell.ALIGN_LEFT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("departureDate"),PdfPCell.ALIGN_RIGHT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("departureTime"), PdfPCell.ALIGN_RIGHT));
				table0.addCell(getCell(roundway.getJSONObject(i).getString("arrivalTime"), PdfPCell.ALIGN_LEFT));
				table0.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
				table0.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
			}

			document.add(table0);
			document.add(Chunk.NEWLINE);

			document.add(new Paragraph("Traveller Details: "));

			document.add(Chunk.NEWLINE);

			PdfPTable table6= new PdfPTable(6);

			table6.addCell("SL No");
			table6.addCell("First Name");
			table6.addCell("Last Name");
			table6.addCell("Gender");
			table6.addCell("Ticket No");
			table6.addCell("Passenger Type");

			for (int i = 0; i < list.size(); i++) {

				table6.addCell(i+1+"");
				table6.addCell(list.get(i).getfName());
				table6.addCell(list.get(i).getlName());
				table6.addCell(list.get(i).getGender());
				table6.addCell(list.get(i).getTicketNo());
				table6.addCell(list.get(i).getType());

			}
			document.add(table6);
			document.close();
			writer.close();

		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	@Override
	public List<FlightTicket> getFlightDetailForAdminByDate(Date from, Date to) {
		return flightTicketRepository.getAllTicketByDate(from, to);
	}

	@Override
	public boolean comCountry(CompareCountry dto) {
		String orgCountry=flightListRepository.getCountryByCode(dto.getOrg());
		String destCountry=flightListRepository.getCountryByCode(dto.getDest());
		boolean international=false;
		if(orgCountry!=null && destCountry!=null) {
		if (orgCountry.equalsIgnoreCase(destCountry)) {
			international=false;
		}else {
			international=true;
		}
		return international;
	}
		return international;
	}

}


