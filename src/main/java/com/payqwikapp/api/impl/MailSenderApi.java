package com.payqwikapp.api.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//import javax.activation.DataHandler;
//import javax.activation.DataSource;
//import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.entity.EmailLog;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.WoohooCardDetails;
import com.payqwikapp.mail.util.MailConstants;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.SummaryReportDTO;
import com.payqwikapp.model.TreatCardDTO;
import com.payqwikapp.model.UserMonthlyBalanceDTO;
import com.payqwikapp.model.WoohooTransactionResponse;
import com.payqwikapp.model.travel.AgentTicketDetailsDTOFlight;
import com.payqwikapp.model.travel.TicketDetailsDTOFlight;
import com.payqwikapp.model.travel.bus.AgentTicketDetailsDTO;
import com.payqwikapp.model.travel.bus.TicketDetailsDTO;
import com.payqwikapp.repositories.EmailLogRepository;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.PayQwikUtil;

public class MailSenderApi implements IMailSenderApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;
	private JavaMailSender mailSender;
	private VelocityEngine velocityEngine;
	private EmailLogRepository emailLogRepository;

	public void setMailSender(JavaMailSender mailSender, VelocityEngine velocityEngine,
			EmailLogRepository emailLogRepository) {
		this.mailSender = mailSender;
		this.velocityEngine = velocityEngine;
		this.emailLogRepository = emailLogRepository;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setEmailLogRepository(EmailLogRepository emailLogRepository) {
		this.emailLogRepository = emailLogRepository;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public void sendMail(final String subject, final String mailTemplate, final User user,final String additionalInfo) throws MailSendException {
		sendVijayaBankEmail(subject, mailTemplate, user, null, additionalInfo,null);

	}

	@Override
	public void sendNoReplyMail(String subject, String mailTemplate, User user, String additionalInfo) throws MailSendException {
		sendVijayaBankNoReplyAgentEmail(subject, mailTemplate, user, null, additionalInfo);
	}

	@Override
	public void sendChangePasswordMail(final String subject, final String mailTemplate, final User user,final String additionalInfo)
			throws MailSendException {
		if (user.getEmailStatus().equals(Status.Active)) {
			sendChangePasswordMail(subject, mailTemplate, user, null, null,null);
		}
	}

	@Override
	public void sendTransactionMail(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,final String additionalInfo,String ccEmail) throws MailSendException {
		if(user.getEmailStatus().equals(Status.Active)) {
			sendVijayaBankEmailNew(subject, mailTemplate, user, transaction, additionalInfo,ccEmail);
		}
	}
	@Override
	public void sendTransactionFailTKMail(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,final String additionalInfo,String ccEmail,TKOrderDetail orderDetail) throws MailSendException {
		if(user.getEmailStatus().equals(Status.Active)) {
			sendFailTKEmail(subject, mailTemplate, user, transaction, additionalInfo,ccEmail,orderDetail);
		}
	}

	@Override
	public void saveLog(String subject, String mailTemplate, User user,String sender,Status status) {
		EmailLog email = new EmailLog();
		email.setDestination(user.getUserDetail().getEmail());
		email.setExcutionTime(new Date());
		email.setMailTemplate(mailTemplate);
		email.setStatus(status);
		email.setSender(sender);
		emailLogRepository.save(email);
	}
	
	@Override
	public void saveLogForMonthlyTrasnaction(String subject, String mailTemplate, UserMonthlyBalanceDTO emailOfUser,String sender,Status status) {
		EmailLog email = new EmailLog();
		email.setDestination(emailOfUser.getEmail());
		email.setExcutionTime(new Date());
		email.setMailTemplate(mailTemplate);
		email.setStatus(status);
		email.setSender(sender);
		emailLogRepository.save(email);
	}

	@Override
	public void sendVijayaBankEmail(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo,final String ccEmail)
			throws MailSendException {
		try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							if(ccEmail != null) {
								helper.setCc(new InternetAddress(ccEmail));
							}
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
				//			helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}
	
	@Override
	public void sendVijayaBankEmailNew(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo,final String ccEmail)
			throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							if(ccEmail != null) {
								helper.setCc(new InternetAddress(ccEmail));
							}
							String created=String.valueOf(transaction.getCreated());
							double amount=BigDecimal.valueOf(transaction.getCurrentBalance()).setScale(2, RoundingMode.HALF_UP).doubleValue();
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("created",created.substring(0,19));
							model.put("updatedBalance",amount);
							model.put("info",additionalInfo);
							model.put("ecariblogo",DeploymentConstants.getWebUrlForImages()+"/resources/images/mail/vijayapaylogo.png");
							model.put("msewa_image",DeploymentConstants.getWebUrlForImages()+"/resources/images/mail/mlogo.png");
							
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}
	
	
	@Override
	public void sendChangePasswordMail(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo,final String ccEmail)
			throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							if(ccEmail != null) {
								helper.setCc(new InternetAddress(ccEmail));
							}
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("tkhanalogo",new ClassPathResource("tkhana.jpg"));
//							helper.addInline("nikilogo",new ClassPathResource("niki.jpg"));
//							helper.addInline("treatcardlogo",new ClassPathResource("treatcard.jpg"));
//							helper.addInline("patanjalilogo",new ClassPathResource("patanjali.png"));
//							helper.addInline("rechargelogo",new ClassPathResource("recharge.png"));
//							helper.addInline("utilitylogo",new ClassPathResource("utility.png"));
//							helper.addInline("trasnferlogo",new ClassPathResource("transfer.png"));
//							helper.addInline("msewa_image",new ClassPathResource("mlogo.png"));
//							helper.addInline("shoppinglogo",new ClassPathResource("shopping.png"));
//							helper.addInline("travellogo",new ClassPathResource("travel.png"));
//							helper.addInline("giftlogo",new ClassPathResource("gift.png"));
//							helper.addInline("fblogo",new ClassPathResource("fb.png"));
//							helper.addInline("twlogo",new ClassPathResource("tw.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}
	
	@Override
	public void senderReceiverBankTransfer(final String subject, final String mailTemplate, final AgentSendMoneyBankDTO dto,String emailId)
			throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				@SuppressWarnings("unchecked")
				public void run() {
					try {
						 Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						 try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
						//	String created=String.valueOf(transaction.getCreated());
							double amount=BigDecimal.valueOf(Double.parseDouble(dto.getAmount())).setScale(2, RoundingMode.HALF_UP).doubleValue();
							Map model = new HashMap();
							model.put("amount",amount);
							model.put("senderName",dto.getSenderName());
					//		model.put("created",created.substring(0,19));
							model.put("receiverName",dto.getAccountName());
							model.put("senderMobileNo",dto.getSenderMobileNo());
							model.put("receiverMobileNo",dto.getReceiverMobileNo());
							model.put("senderEmailId",dto.getSenderEmailId());
							model.put("receiverEmailId",dto.getReceiverEmailId());
							model.put("transactionRefNo",dto.getTransactionRef());
							model.put("BankName",dto.getBankName());
							model.put("accountNumber",dto.getAccountNumber());
							model.put("ifscCode",dto.getIfscCode());
							model.put("transactionRefNo",dto.getTransactionRef());
							model.put("bankName",dto.getBankName());
							model.put("accountNumber",dto.getAccountNumber());
							model.put("ifscCode",dto.getIfscCode());
							model.put("amount",dto.getAmount());
							model.put("transactionId",dto.getTransactionRef());
							helper.setTo(new InternetAddress(emailId));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("tkhanalogo",new ClassPathResource("tkhana.jpg"));
//							helper.addInline("nikilogo",new ClassPathResource("niki.jpg"));
//							helper.addInline("treatcardlogo",new ClassPathResource("treatcard.jpg"));
//							helper.addInline("patanjalilogo",new ClassPathResource("patanjali.png"));
//							helper.addInline("rechargelogo",new ClassPathResource("recharge.png"));
//							helper.addInline("utilitylogo",new ClassPathResource("utility.png"));
//							helper.addInline("trasnferlogo",new ClassPathResource("transfer.png"));
//							helper.addInline("msewa_image",new ClassPathResource("mlogo.png"));
//							helper.addInline("shoppinglogo",new ClassPathResource("shopping.png"));
//							helper.addInline("travellogo",new ClassPathResource("travel.png"));
//							helper.addInline("giftlogo",new ClassPathResource("gift.png"));
//							helper.addInline("fblogo",new ClassPathResource("fb.png"));
//							helper.addInline("twlogo",new ClassPathResource("tw.png"));
							sendAsyncVijayaEmail(message);
						//	saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
						//	saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					//	saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		//	saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}

	@Override
	public void sendFailTKEmail(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo,final String ccEmail,TKOrderDetail orderDetail)
			throws MailSendException {
		try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							if(ccEmail != null) {
								helper.setCc(new InternetAddress(ccEmail));
							}
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							model.put("orderDetail", orderDetail);
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
				//			helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}
	
	@Override
	public void sendVijayaBankNoReplyEmail(final String subject, final String mailTemplate, final User user, final PQTransaction transaction, final String additionalInfo) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							String email = user.getUserDetail().getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
				//			helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
							sendAsyncVijayaNoReplyEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL_NO_REPLY,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL_NO_REPLY,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}

	}

	@Override
	public void sendVijayaBankNoReplyAgentEmail(final String subject, final String mailTemplate, final User user, final PQTransaction transaction, final String additionalInfo) throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							String email = user.getUserDetail().getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("gift",new ClassPathResource("gift.png"));
//							helper.addInline("niki",new ClassPathResource("niki.jpg"));
//							helper.addInline("patanjali",new ClassPathResource("patanjali.png"));
//							helper.addInline("recharge",new ClassPathResource("recharge.png"));
//							helper.addInline("shopping",new ClassPathResource("shopping.png"));
//							helper.addInline("tkhana",new ClassPathResource("tkhana.jpg"));
//							helper.addInline("transfer",new ClassPathResource("transfer.png"));
//							helper.addInline("travel",new ClassPathResource("travel.png"));
//							helper.addInline("utility",new ClassPathResource("utility.png"));
//							helper.addInline("treatcard",new ClassPathResource("treatcard.jpg"));
							sendAsyncVijayaNoReplyEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL_NO_REPLY,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL_NO_REPLY,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}

	}

	
	@Override
	public void sendAsync(final JavaMailSender mailSender, final MimeMessagePreparator preparator) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					mailSender.send(preparator);
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void sendAsyncVijayaEmail(final MimeMessage message) {
		final String password = MailConstants.PASSWORD;
		final String userId = MailConstants.USER_ID;
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
                        Transport.send(message, userId, password);
						//Transport.send(message);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void sendAsyncVijayaEmailForBankTransfer(final MimeMessage message) {
		final String password = MailConstants.PASSWORD;
		final String userId = MailConstants.USER_ID;
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
                        Transport.send(message, message.getAllRecipients(), userId, password);
						//Transport.send(message);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendAsyncVijayaNoReplyEmail(final MimeMessage message) {
		final String password = MailConstants.PASSWORD;
		final String userId = MailConstants.USER_ID;
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Transport.send(message,userId,password);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Override
	public void sendgciMail(String subject, String mailTemplate,ProcessGiftCardDTO giftCard, GiftCardTransactionResponse dto,final User user) throws MailSendException

	{
		sendgiftcardmail(subject, mailTemplate,giftCard, dto, user);
		//sendgiftcardmail(subject, mailTemplate, dto);
		//saveLog(subject, mailTemplate, dto);

	}

	@Override
	public void sendgiftcardmail(String subject, String mailTemplate,ProcessGiftCardDTO giftCard, GiftCardTransactionResponse dto,final User user)
			throws MailSendException {

		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map model = new HashMap();
							double amount=BigDecimal.valueOf(user.getAccountDetail().getBalance()).setScale(2, RoundingMode.HALF_UP).doubleValue();
							model.put("expiryDate", dto.getExpiryDate());
							model.put("brandName",dto.getBrandName());
							model.put("amount", giftCard.getAmount());
							model.put("voucherNumber",dto.getVoucherNumber());
							model.put("voucherPin", dto.getVoucherPin()!=null ? dto.getVoucherPin() :"NA");
							model.put("updatedBalance",amount);
							String email = giftCard.getReceiversEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("tkhanalogo",new ClassPathResource("tkhana.jpg"));
//							helper.addInline("nikilogo",new ClassPathResource("niki.jpg"));
//							helper.addInline("treatcardlogo",new ClassPathResource("treatcard.jpg"));
//							helper.addInline("patanjalilogo",new ClassPathResource("patanjali.png"));
//							helper.addInline("rechargelogo",new ClassPathResource("recharge.png"));
//							helper.addInline("utilitylogo",new ClassPathResource("utility.png"));
//							helper.addInline("trasnferlogo",new ClassPathResource("transfer.png"));
//							helper.addInline("msewa_image",new ClassPathResource("mlogo.png"));
//							helper.addInline("shoppinglogo",new ClassPathResource("shopping.png"));
//							helper.addInline("travellogo",new ClassPathResource("travel.png"));
//							helper.addInline("giftlogo",new ClassPathResource("gift.png"));
//							helper.addInline("fblogo",new ClassPathResource("fb.png"));
//							helper.addInline("twlogo",new ClassPathResource("tw.png"));
							sendAsyncVijayaNoReplyEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}

	@Override
	public void saveLog(String subject, String mailTemplate, User user) {
		EmailLog email = new EmailLog();
		email.setDestination(user.getUserDetail().getEmail());
		email.setExcutionTime(new Date());
		email.setMailTemplate(mailTemplate);
		//email.setResponse(null);
		email.setStatus(Status.Inactive);
		emailLogRepository.save(email);
	}

	@Override
	public void sendQwikrPayMail(String subject, String mailTemplate, QwikrPayRequest dto, User user)
	{

		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.CC_MAIL));
							Map model = new HashMap();
							model.put("OrderId", dto.getOrderId());
							model.put("TransactionRefNo", dto.getTransactionRefNo());
							model.put("amount", dto.getAmount());
							String email = user.getUserDetail().getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
					//		helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
							sendAsyncVijayaNoReplyEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}

	}

	
	@Override
	public void sendBusTicketMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			TicketDetailsDTO additionalInfo) throws MailSendException {

		try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							helper.setTo(new InternetAddress(additionalInfo.getUserEmail()));
							helper.setCc(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("pointlogo",new ClassPathResource("point.png"));
//							helper.addInline("buslogo",new ClassPathResource("Bus.png"));
//							helper.addInline("buslogo1",new ClassPathResource("Bus1.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
		
	}	

	@Override
	public void sendBusTicketMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			AgentTicketDetailsDTO additionalInfo) throws MailSendException {

		try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							helper.setTo(new InternetAddress(additionalInfo.getUserEmail()));
							helper.setCc(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("pointlogo",new ClassPathResource("point.png"));
//							helper.addInline("buslogo",new ClassPathResource("Bus.png"));
//							helper.addInline("buslogo1",new ClassPathResource("Bus1.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
		
	}	


	@Override
	public void sendFlightTicketMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			TicketDetailsDTOFlight additionalInfo) throws MailSendException {
		
		try {

			Thread t = new Thread(new Runnable() {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							model.put("trip",additionalInfo.getFlightresponse());
							model.put("tripReturn",additionalInfo.getFlightresponsearrreturn());
							if (additionalInfo.getFlightresponsearrreturn().size()>0) {
								model.put("return","Return");
							}
							else {
								model.put("return","");
							}
							System.err.println("Passenger Size:: "+additionalInfo.getNameandticket().size());
							System.err.println("Trip Size:: "+additionalInfo.getFlightresponse().size());
						
							for (int i = 0; i < additionalInfo.getFlightresponse().size(); i++) {
								System.err.println("Origin:: "+additionalInfo.getFlightresponse().get(i).getOrigin());
								System.err.println("Destination:: "+additionalInfo.getFlightresponse().get(i).getDestination());
							}
							System.err.println("User Email is:: "+user.getUserDetail().getEmail());
//							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							helper.setCc(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("pdf",new ClassPathResource("HelloWorld.pdf"));
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("msewalogo",new ClassPathResource("logo.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}
	
	@Override
	public void sendFlightTicketMailNew(String subject, String mailTemplate, User user, PQTransaction transaction,
			TicketDetailsDTOFlight additionalInfo) throws MailSendException {
		
		try {

			Thread t = new Thread(new Runnable() {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							model.put("trip",additionalInfo.getFlightresponse());
							model.put("tripReturn",additionalInfo.getFlightresponsearrreturn());
							if (additionalInfo.getFlightresponsearrreturn().size()>0) {
								model.put("return","Return");
							}
							else {
								model.put("return","");
							}
							System.err.println("Passenger Size:: "+additionalInfo.getNameandticket().size());
							System.err.println("Trip Size:: "+additionalInfo.getFlightresponse().size());
						
							for (int i = 0; i < additionalInfo.getFlightresponse().size(); i++) {
								System.err.println("Origin:: "+additionalInfo.getFlightresponse().get(i).getOrigin());
								System.err.println("Destination:: "+additionalInfo.getFlightresponse().get(i).getDestination());
							}
							System.err.println("User Email is:: "+user.getUserDetail().getEmail());
//							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							helper.setCc(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("pdf",new ClassPathResource("HelloWorld.pdf"));
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("msewalogo",new ClassPathResource("logo.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}
	
  @Override
	public void sendRedeemPointsMail(String subject, String mailTemplate, User user, long point, double balance,PQTransaction transaction) throws MailSendException {

		try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
						//	helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map model = new HashMap();
							model.put("user", user);
						    model.put("point",point);
						    model.put("balance",balance);
						    model.put("transaction", transaction);
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("msewalogo",new ClassPathResource("logo.png"));
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
		
	}
	
  @Override
	public void sendTransactionMailFoodOrder(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,final String additionalInfo,String ccEmail,long userOrderId,TKOrderDetail orderDetail) throws MailSendException {
		if(user.getEmailStatus().equals(Status.Active)) {
			sendVijayaBankEmailTK(subject, mailTemplate, user, transaction, additionalInfo,ccEmail,userOrderId,orderDetail);
		}
	}
	
  @Override
	public void sendVijayaBankEmailTK(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo,
			  final String ccEmail,long userOrderId,TKOrderDetail orderDetail)
			throws MailSendException {
		try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							if(ccEmail != null) {
								helper.setCc(new InternetAddress(ccEmail));
							}
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							model.put("userOrderId",userOrderId);
							model.put("orderDetail",orderDetail);
							
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("mlogo",new ClassPathResource("mlogo.png"));
//							helper.addInline("Train",new ClassPathResource("Train.png"));
//							helper.addInline("station",new ClassPathResource("station.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}

  //refund merchant
  @Override
	public void refundRequestMerchant(String filedetails,String merchantName,String merchantId) throws MailSendException {
	  try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							message.setSubject("Merchant Refund Request ID#"+merchantId);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							helper.setCc(new InternetAddress("kamal@msewa.com"));
							helper.setCc(new InternetAddress("abhijitp@msewa.com"));
							helper.setTo(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							Multipart multipart = new MimeMultipart();
					        BodyPart messageBodyPart = new MimeBodyPart();
					        messageBodyPart.setText("Process the refund request from merchant("+merchantName+") at earliest");
					        multipart.addBodyPart(messageBodyPart);
					        messageBodyPart = new MimeBodyPart();
//					        DataSource source = new FileDataSource(filedetails);
//					        messageBodyPart.setDataHandler(new DataHandler(source));
//					        messageBodyPart.setFileName(source.getName());
					        multipart.addBodyPart(messageBodyPart);
					        message.setContent(multipart);
							sendAsyncVijayaEmail(message);
						} catch (MessagingException ex) {
							ex.printStackTrace();
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		}
  
  @Override
 	public void sendMerchantRefundTransaction(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo)
 			throws MailSendException {
 		try {

 			Thread t = new Thread(new Runnable() {
 				public void run() {
 					try {
 						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
 						try {
 							MimeMessage message = new MimeMessage(session);
 							MimeMessageHelper helper = new MimeMessageHelper(message,true);
 							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
 							Map model = new HashMap();
 							model.put("user", user);
 							model.put("transaction", transaction);
 							model.put("info",additionalInfo);
 							helper.setTo(new InternetAddress(MailConstants.CC_MAIL_LIVE));
 							helper.setCc(new InternetAddress("kamal@msewa.com"));
 							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
 									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
 							helper.setSubject(subject);
 							helper.setText(mailBody, true);
 					//		helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
 							sendAsyncVijayaEmail(message);
 							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
 						} catch (MessagingException ex) {
 							ex.printStackTrace();
 							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
 						}
 					} catch (Exception ee) {
 						ee.printStackTrace();
 						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
 					}
 				}
 			});
 			t.start();
 		} catch (Exception e) {
 			e.printStackTrace();
 			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
 		}
 	}
  
  @Override
 	public void weeklyReconReport(String ipayFileDeatils,String ebsFileDetails,String vnetFileDetails,String upiFileDetails) throws MailSendException {
 	  
 	  try {

 			Thread t = new Thread(new Runnable() {
 				public void run() {
 					try {
 						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
 						try {
 							MimeMessage message = new MimeMessage(session);
 							message.setSubject("Weekly Reconciling Report");
 							 String[] cc={"ggupta@msewa.com","amanalath@msewa.com","kamal@msewa.com","aadeeb@msewa.com","prashant@msewa.com","kgeorge@msewa.com","mamtha@msewa.com"};
 							
							 InternetAddress[] ccAddress = new InternetAddress[cc.length];
					            
					            // To get the array of ccaddresses
					            for( int i = 0; i < cc.length; i++ ) {
					                ccAddress[i] = new InternetAddress(cc[i]);
					            }
					            
					            // Set cc: header field of the header.
					            for( int i = 0; i < ccAddress.length; i++) {
					                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
					            }
 							MimeMessageHelper helper = new MimeMessageHelper(message,true);
 							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
 							helper.setTo(new InternetAddress("pkumar@msewa.com"));
// 							helper.setTo(new InternetAddress(MailConstants.CC_MAIL_LIVE));
 							
 							Multipart multipart = new MimeMultipart();
 					        BodyPart messageBodyPart = new MimeBodyPart();
 					        messageBodyPart.setText("PFA InstantPay, EBS and VNet reconciling report");
 					        multipart.addBodyPart(messageBodyPart);

 					        messageBodyPart = new MimeBodyPart();
// 					        DataSource source = new FileDataSource(ipayFileDeatils);
// 					        messageBodyPart.setDataHandler(new DataHandler(source));
// 					        messageBodyPart.setFileName(source.getName());
// 					        multipart.addBodyPart(messageBodyPart);
// 					        
// 					        messageBodyPart = new MimeBodyPart();
// 					        DataSource source1 = new FileDataSource(ebsFileDetails);
// 					        messageBodyPart.setDataHandler(new DataHandler(source1));
// 					        messageBodyPart.setFileName(source1.getName());
 					        multipart.addBodyPart(messageBodyPart);
 					        
 					        messageBodyPart = new MimeBodyPart();
//					        DataSource source2 = new FileDataSource(vnetFileDetails);
//					        messageBodyPart.setDataHandler(new DataHandler(source2));
//					        messageBodyPart.setFileName(source2.getName());
//					        multipart.addBodyPart(messageBodyPart);
//					        
//					        messageBodyPart = new MimeBodyPart();
//					        DataSource source3 = new FileDataSource(upiFileDetails);
//					        messageBodyPart.setDataHandler(new DataHandler(source3));
//					        messageBodyPart.setFileName(source3.getName());
					        multipart.addBodyPart(messageBodyPart);
 					        
					        
 					        message.setContent(multipart);
 					       sendAsyncVijayaEmailForBankTransfer(message);
 						} catch (MessagingException ex) {
 							ex.printStackTrace();
 						}
 					} catch (Exception ee) {
 						ee.printStackTrace();
 					}
 				}
 			});
 			t.start();
 		} catch (Exception e) {
 			e.printStackTrace();
 		}

 		}
  
  @Override
	public void weeklyReconReport(String ipayFileDeatils) throws MailSendException {
	  try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
 							message.setSubject("Performance Tracking Weekly Report");
 							 String[] cc={"ggupta@msewa.com","amanalath@msewa.com","kamal@msewa.com","prashant@msewa.com","aadeeb@msewa.com","spawar@msewa.com","mamtha@msewa.com"};
 							
							 InternetAddress[] ccAddress = new InternetAddress[cc.length];
					            
					            // To get the array of ccaddresses
					            for( int i = 0; i < cc.length; i++ ) {
					                ccAddress[i] = new InternetAddress(cc[i]);
					            }
					            
					            // Set cc: header field of the header.
					            for( int i = 0; i < ccAddress.length; i++) {
					                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
					            }
 							MimeMessageHelper helper = new MimeMessageHelper(message,true);
 							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
 							helper.setTo(new InternetAddress("pkumar@msewa.com"));
							
					        Multipart multipart = new MimeMultipart();
					        BodyPart messageBodyPart = new MimeBodyPart();
					        multipart.addBodyPart(messageBodyPart);
					        
					        
					        String messageforBody = "<h4>Hello Sir,</h4><br>";
					        messageforBody += "<i>PFA PayQwik Performance Tracking Weekly Report</i><br><br>";
					        messageforBody += "Thanks & Regards,<br><br>";
					        messageforBody += "<b>VpayQwik<b>";
					        message.setContent(messageforBody, "text/html");    
					        messageBodyPart.setContent(messageforBody, "text/html");

					        messageBodyPart = new MimeBodyPart();
//					        DataSource source = new FileDataSource(ipayFileDeatils);
//					        messageBodyPart.setDataHandler(new DataHandler(source));
//					        messageBodyPart.setFileName(source.getName());
					        multipart.addBodyPart(messageBodyPart);
					        
					        message.setContent(multipart);
							sendAsyncVijayaEmail(message);
						} catch (MessagingException ex) {
							ex.printStackTrace();
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		}
  
  
  @Override
	public void dailyBankTrasnferReport(String bankTrasnferFileDeatils,String dateFormat,int fileSize,long amount) throws MailSendException {
	  try {

			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							message.setSubject("Daily BankTransfer Report");
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							 String[] cc={ "sunithan@vijayabank.co.in","ABPSTeam@vijayabank.co.in","kamal@msewa.com","pkumar@msewa.com","aadeeb@msewa.com","ggupta@msewa.com"};
//							 String[] cc={ "kamal@msewa.com","pkumar@msewa.com"};
							 InternetAddress[] ccAddress = new InternetAddress[cc.length];
					            
					            // To get the array of ccaddresses
					            for( int i = 0; i < cc.length; i++ ) {
					                ccAddress[i] = new InternetAddress(cc[i]);
					            }
					            
					            // Set cc: header field of the header.
					            for( int i = 0; i < ccAddress.length; i++) {
					                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
					            }
							
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							helper.setTo(new InternetAddress("rubalmishra@vijayabank.co.in"));
//							helper.setTo(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							
							Multipart multipart = new MimeMultipart();
					        BodyPart messageBodyPart = new MimeBodyPart();
					        multipart.addBodyPart(messageBodyPart);
					        
					        
					        String messageforBody = "<h4>Dear Team,</h4><br>";
					        messageforBody += "<i>We are sending the file for bank Transactions made on : "+dateFormat+" </i><br><br>";
					        messageforBody += "<i> Number of Transaction(s)	:"+ fileSize +"</i><br><br>";
					        messageforBody += "<i> Amount(in Rupees) : "+amount +"</i><br><br>"; 
					        messageforBody += "<i>kindly upload the attached file to NPCI.</i><br><br>";
					        
					        messageforBody += "Thanks & Regards,<br><br>";
					        messageforBody += "<b>VpayQwik<b>";
					        message.setContent(messageforBody, "text/html");    
					        messageBodyPart.setContent(messageforBody, "text/html");

					        messageBodyPart = new MimeBodyPart();
//					        DataSource source = new FileDataSource(bankTrasnferFileDeatils);
//					        messageBodyPart.setDataHandler(new DataHandler(source));
//					        messageBodyPart.setFileName(source.getName());
					        multipart.addBodyPart(messageBodyPart);
					        message.setContent(multipart);
					        sendAsyncVijayaEmailForBankTransfer(message);
						} catch (MessagingException ex) {
							ex.printStackTrace();
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		}
	
  @Override
public void sendwoohoogiftcardmail(String subject, String mailTemplate, Object object,
		WoohooTransactionResponse dto, User user) {
	  try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map model = new HashMap();
							model.put("expiryDate", dto.getExpiry_date());
							model.put("brandName",dto.getCardName());
							model.put("amount", dto.getCard_price());
							model.put("voucherNumber",dto.getCardnumber());
							model.put("pin", dto.getPin_or_url());
							model.put("user", user.getUserDetail().getFirstName());
							model.put("brand", dto.getCardName());
							model.put("terms", dto.getTermsAndConditions());
							String email = dto.getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("mvpaylogo",new ClassPathResource("mlogo.png"));
//							helper.addInline("giftcard",new ClassPathResource("giftcard.png"));
//							helper.addInline("logo1",new ClassPathResource("logo1.png"));
//							helper.addInline("QwikcilverLogo",new ClassPathResource("QwikcilverLogo.png"));
							sendAsyncVijayaNoReplyEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		 }
	
     }
  
  
  @Override
public void sendwoohoogiftcardmailNew(String subject, String mailTemplate, Object object,
		OrderPlaceResponse dto, User user) {
	  try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map model = new HashMap();
							model.put("expiryDate", dto.getExpiry_date());
							model.put("brandName",dto.getCardName());
							model.put("amount", dto.getCard_price());
							model.put("voucherNumber",dto.getCardnumber());
							model.put("pin", dto.getPin_or_url());
							model.put("user", dto.getName());
							model.put("brand", dto.getCardName());
							model.put("terms", dto.getTermsAndConditions());
							String email = dto.getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("mvpaylogo",new ClassPathResource("mlogo.png"));
//							helper.addInline("giftcard",new ClassPathResource("giftcard.png"));
//							helper.addInline("logo1",new ClassPathResource("logo1.png"));
//							helper.addInline("QwikcilverLogo",new ClassPathResource("QwikcilverLogo.png"));
							sendAsyncVijayaNoReplyEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.CC_MAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		 }
	
     }
  
  @Override
	public void treatCardEmail(final String subject, final String mailTemplate, final User user) throws MailSendException {
	    System.err.println("inside mailsender api");
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
							Map model = new HashMap();
							model.put("user", user);
							String email = user.getUserDetail().getEmail();
							helper.setTo(new InternetAddress(email));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("treat_1",new ClassPathResource("1.jpg"));
//							helper.addInline("msewa_image",new ClassPathResource("msslogo.png"));
//							helper.addInline("treat_2",new ClassPathResource("2.png"));
							sendAsyncVijayaNoReplyEmail(message);
                      } catch (MessagingException ex) {
							ex.printStackTrace();
                      }
					} catch (Exception ee) {
						ee.printStackTrace();
                  }

				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
  
  // TODO Refund Request for Bescom Merchant
  @Override
	public void refundRequestBescomMerchant(String filedetails,String merchantName,String merchantId) throws MailSendException {
	  try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							message.setSubject("Merchant Refund Request ID#"+merchantId);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							helper.setCc(new InternetAddress("kamal@msewa.com"));
							helper.setCc(new InternetAddress("abhijitp@msewa.com"));
							helper.setTo(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							
//							 String[] cc={"kamal@msewa.com","pkumar@msewa.com","prashant@msewa.com","ggupta@msewa.com" };
							 String[] cc={ "prashant@msewa.com"};
							 InternetAddress[] ccAddress = new InternetAddress[cc.length];
					            
					            // To get the array of ccaddresses
					            for( int i = 0; i < cc.length; i++ ) {
					                ccAddress[i] = new InternetAddress(cc[i]);
					            }
					            
					            // Set cc: header field of the header.
					            for( int i = 0; i < ccAddress.length; i++) {
					                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
					            }
							
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							helper.setTo(new InternetAddress("prashant@msewa.com"));
							
							Multipart multipart = new MimeMultipart();
					        BodyPart messageBodyPart = new MimeBodyPart();
					        messageBodyPart.setText("Process the refund request from merchant("+merchantName+") at earliest");
					        multipart.addBodyPart(messageBodyPart);
					        messageBodyPart = new MimeBodyPart();
//					        DataSource source = new FileDataSource(filedetails);
//					        messageBodyPart.setDataHandler(new DataHandler(source));
					//        messageBodyPart.setFileName(source.getName());
					        multipart.addBodyPart(messageBodyPart);
					        message.setContent(multipart);
							sendAsyncVijayaEmail(message);
						} catch (MessagingException ex) {
							ex.printStackTrace();
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		}  
  
	@Override
	public void sendBulkUploadMail(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,final String additionalInfo,String ccEmail) throws MailSendException {
		if(user.getEmailStatus().equals(Status.Active)) {
			sendVijayaBankEmailForBulkUpload(subject, mailTemplate, user, transaction, additionalInfo,ccEmail);
		}
	}
	
	
	@Override
	public void sendVijayaBankEmailForBulkUpload(final String subject, final String mailTemplate, final User user, final PQTransaction transaction,final String additionalInfo,final String ccEmail)
			throws MailSendException {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							LocalDate currentDate = LocalDate.now();
						    Month m = currentDate.getMonth();
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							if(ccEmail != null) {
								helper.setCc(new InternetAddress(ccEmail));
							}
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",m);
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
						//	helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}

	@Override
	public void weeklyBesconUPIReconReport(String filedetails) throws MailSendException {
		  try {
				Thread t = new Thread(new Runnable() {
					public void run() {
						try {
							Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
							try {
								MimeMessage message = new MimeMessage(session);
	 							message.setSubject("Performance Tracking Weekly Report");
	 							 String[] cc={"ggupta@msewa.com","amanalath@msewa.com","kamal@msewa.com","prashant@msewa.com","kgeorge@msewa.com","mamtha@msewa.com"};
	 							
								 InternetAddress[] ccAddress = new InternetAddress[cc.length];
						            
						            // To get the array of ccaddresses
						            for( int i = 0; i < cc.length; i++ ) {
						                ccAddress[i] = new InternetAddress(cc[i]);
						            }
						            
						            // Set cc: header field of the header.
						            for( int i = 0; i < ccAddress.length; i++) {
						                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
						            }
	 							MimeMessageHelper helper = new MimeMessageHelper(message,true);
	 							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
	 							helper.setTo(new InternetAddress("pkumar@msewa.com"));
								
								Multipart multipart = new MimeMultipart();
						        BodyPart messageBodyPart = new MimeBodyPart();
						        messageBodyPart.setText("PFA PayQwik Performance Tracking Weekly Report");
						        multipart.addBodyPart(messageBodyPart);
						        
						        message.setContent(multipart);
								sendAsyncVijayaEmail(message);
							} catch (MessagingException ex) {
								ex.printStackTrace();
							}
						} catch (Exception ee) {
							ee.printStackTrace();
						}
					}
				});
				t.start();
			} catch (Exception e) {
				e.printStackTrace();
			}

			}

	@Override
	public void sendFlightTicketMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			AgentTicketDetailsDTOFlight additionalInfo) throws MailSendException {
		
		try {
			Thread t = new Thread(new Runnable() {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				public void run() {
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
							helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
							
							Map model = new HashMap();
							model.put("user", user);
							model.put("transaction", transaction);
							model.put("info",additionalInfo);
							model.put("trip",additionalInfo.getFlightresponse());
							model.put("tripReturn",additionalInfo.getFlightresponsearrreturn());
							if (additionalInfo.getFlightresponsearrreturn().size()>0) {
								model.put("return","Return");
							}
							else {
								model.put("return","");
							}
							System.err.println("Passenger Size:: "+additionalInfo.getNameandticket().size());
							System.err.println("Trip Size:: "+additionalInfo.getFlightresponse().size());
						
							for (int i = 0; i < additionalInfo.getFlightresponse().size(); i++) {
								System.err.println("Origin:: "+additionalInfo.getFlightresponse().get(i).getOrigin());
								System.err.println("Destination:: "+additionalInfo.getFlightresponse().get(i).getDestination());
							}
							System.err.println("User Email is:: "+user.getUserDetail().getEmail());
//							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							helper.setTo(new InternetAddress(user.getUserDetail().getEmail()));
							helper.setCc(new InternetAddress(MailConstants.CC_MAIL_LIVE));
							
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("pdf",new ClassPathResource("HelloWorld.pdf"));
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("msewalogo",new ClassPathResource("logo.png"));
							sendAsyncVijayaEmail(message);
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Success);
						} catch (MessagingException ex) {
							ex.printStackTrace();
							saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
						saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
			saveLog(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}
	}

	@Override
	public void senderReceiverBankTransferMail(String string, String mailTemplate,
			AgentSendMoneyBankDTO dto,String emailId) {
		
		senderReceiverBankTransfer(string, mailTemplate, dto,emailId);
	}

	
	@Override
	public void sendMonthlyTransactionTesting(final String subject, final String mailTemplate, final UserMonthlyBalanceDTO user,final User u, final String additionalInfo) throws MailSendException {
		try {
						try {
							Session session = Session.getDefaultInstance(MailConstants.getBulkEmailProperties(), null);
							try {
								MimeMessage message = new MimeMessage(session);
								MimeMessageHelper helper = new MimeMessageHelper(message,true);
								helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL_NO_REPLY));
								Map model = new HashMap();
								model.put("user", user);
								model.put("info",additionalInfo);
								String email = user.getEmail();
								helper.setTo(new InternetAddress(email));
								String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
										PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
								helper.setSubject(subject);
								helper.setText(mailBody, true);
//								helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//								helper.addInline("wallet_image",new ClassPathResource("wallet.png"));
//								helper.addInline("android_image",new ClassPathResource("android.png"));
//								helper.addInline("apple_image",new ClassPathResource("apple.png"));
//								helper.addInline("msewa_image",new ClassPathResource("mlogo.png"));
								sendAsyncVijayaNoReplyEmail(message);

								saveLogForMonthlyTrasnaction(subject,mailTemplate,user,MailConstants.SENDER_EMAIL_NO_REPLY,Status.Success);
	                        } catch (MessagingException ex) {
								ex.printStackTrace();
								saveLogForMonthlyTrasnaction(subject,mailTemplate,user,MailConstants.SENDER_EMAIL_NO_REPLY,Status.Failed);
	                        }
						} catch (Exception ee) {
							ee.printStackTrace();
							saveLogForMonthlyTrasnaction(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
	                    }
		} catch (Exception e) {
			e.printStackTrace();
			saveLogForMonthlyTrasnaction(subject,mailTemplate,user,MailConstants.SENDER_EMAIL,Status.Failed);
		}

	}
	
	@Override
	 public void sendSummaryMail(String mailTemplate, SummaryReportDTO summaryReportDTO) {
		try {
			     String subject="VPayQwik | Daily summary statement for "+ summaryReportDTO.getDate();
					try {
						Session session = Session.getDefaultInstance(MailConstants.getEmailProperties(), null);
						try {
							MimeMessage message = new MimeMessage(session);
							MimeMessageHelper helper = new MimeMessageHelper(message,true);
 							 String[] cc=getUserMails();
							 InternetAddress[] ccAddress = new InternetAddress[cc.length];
					            // To get the array of ccaddresses
					            for( int i = 0; i < cc.length; i++ ) {
					                ccAddress[i] = new InternetAddress(cc[i]);
					            }
					            // Set cc: header field of the header.
					            for( int i = 0; i < ccAddress.length; i++) {
					                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
					            }
					            helper.setFrom(new InternetAddress(MailConstants.SENDER_EMAIL));
								helper.setTo(new InternetAddress("pkumar@msewa.com"));
								
							Map model = new HashMap();
	                        model.put("user", summaryReportDTO);
							String mailBody = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
									PayQwikUtil.MAIL_TEMPLATE + mailTemplate, model);
							helper.setSubject(subject);
							helper.setText(mailBody, true);
//							helper.addInline("vpaylogo",new ClassPathResource("vijaya pay logo.png"));
//							helper.addInline("msewa_image",new ClassPathResource("mlogo.png"));
//							helper.addInline("fblogo",new ClassPathResource("fb.png"));
//							helper.addInline("twlogo",new ClassPathResource("tw.png"));
							sendAsyncVijayaEmail(message);
						} catch (MessagingException ex) {
							ex.printStackTrace();
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String[] getUserMails() {
		String[] userMails = { "kamal@msewa.com", "ggupta@msewa.com", "aadeeb@msewa.com", "spawar@msewa.com","vjain@msewa.com",
				"mamtha@msewa.com", "akothari@msewa.com", "spati@msewa.com", "prashant@msewa.com","balphonse@msewa.com" };
		return userMails;
	}
}