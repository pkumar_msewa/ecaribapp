package com.payqwikapp.api.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.payqwikapp.api.IDataConfigApi;
import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.admin.UpdateServiceDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.DataConfigRepository;
import com.payqwikapp.util.ConvertUtil;

public class DataConfigApi implements IDataConfigApi{

	private final DataConfigRepository dataConfigRepository;

	public DataConfigApi(DataConfigRepository dataConfigRepository) {

		this.dataConfigRepository = dataConfigRepository;
	}

	@Override
	public DataConfig getDataConfigByType(String type, Status status) {
		return dataConfigRepository.getByConfigType(type, status);
	}

	@Override
	public List<DataConfig> getAllConfig() {
		return dataConfigRepository.getAllDataConfig();
	}
	
	@Override
	public ResponseDTO updateMdexSwitch(UpdateServiceDTO dto) {
		ResponseDTO result = new ResponseDTO();
		DataConfig data = dataConfigRepository.findServiceByCode(dto.getCode());
		if (data != null) {
			data.setStatus(Status.valueOf(dto.getStatus()));
			if(dto.getStatus().equalsIgnoreCase("Active")){
				data.setMdex("Active");
			}else{
				data.setMdex("Inactive");
			}
			data = dataConfigRepository.save(data);
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Information Updated");
			result.setDetails(data);
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Service Not available");
		}
		return result;
	}




	
}