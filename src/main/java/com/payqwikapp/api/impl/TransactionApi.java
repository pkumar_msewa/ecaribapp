package com.payqwikapp.api.impl;

import java.io.FileWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.ws.rs.core.MediaType;

//import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ebs.util.EBSConstants;
import com.instantpay.api.ITransactionApi;
import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.BulkFile;
import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.entity.MerchantRefundRequest;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PredictAndWinPayment;
import com.payqwikapp.entity.PromoCode;
import com.payqwikapp.entity.SharePointsLog;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.Visa;
import com.payqwikapp.entity.Voucher;
import com.payqwikapp.entity.WoohooCardDetails;
import com.payqwikapp.mail.util.MailConstants;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.AddVoucherDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.LoadMoneyRequest;
import com.payqwikapp.model.MicroPaymentServiceStatus;
import com.payqwikapp.model.OnePayResponse;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionListDTO;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.UpiMobileRedirectReq;
import com.payqwikapp.model.UserDetailDTO;
import com.payqwikapp.model.VNetStatusResponse;
import com.payqwikapp.model.WoohooCardDetailsDTO;
import com.payqwikapp.model.WoohooTransactionRequest;
import com.payqwikapp.model.WoohooTransactionResponse;
import com.payqwikapp.model.admin.EBSStatusResponseDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.BulkFileRepository;
import com.payqwikapp.repositories.DataConfigRepository;
import com.payqwikapp.repositories.MerchantRefundRequestRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.PredictAndWinRepository;
import com.payqwikapp.repositories.SavaariBookingRepository;
import com.payqwikapp.repositories.SharePointsLogRepository;
import com.payqwikapp.repositories.TPTransactionRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.VisaTransactionRepository;
import com.payqwikapp.repositories.VoucherRepository;
import com.payqwikapp.repositories.WoohooCardDetailsRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.util.VNetUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.thirdparty.util.MeraEventsUtil;
import com.upi.model.MerchantTxnStatusResponse;
import com.upi.model.UpiRequest;
import com.upi.model.UpiResponse;
import com.upi.util.UpiConstants;

public class TransactionApi implements com.payqwikapp.api.ITransactionApi, MessageSourceAware {


	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;
	private final SimpleDateFormat dateOnly = new SimpleDateFormat("yyyy-MM-dd");
	private final IUserApi userApi;
	private final PQTransactionRepository transactionRepository;
	private final ISMSSenderApi smsSenderApi;
	private final IMailSenderApi mailSenderApi;
	private final ICommissionApi commissionApi;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQServiceRepository pqServiceRepository;
	private final SharePointsLogRepository sharePointsLogRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final VisaTransactionRepository visaTransactionRepository;
	private final UserRepository userRepository;
	private final SavaariBookingRepository savaariBookingRepository;
	private final TPTransactionRepository tPTransactionRepository;
	private final BulkFileRepository bulkFileRepository;
	private final WoohooCardDetailsRepository woohooCardDetailsRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final MerchantRefundRequestRepository merchantRefundRequestRepository;
	private final ITransactionApi ipayTransactionApi;
	private final DataConfigRepository dataConfigRepository;
	private final PredictAndWinRepository predictAndWinRepository;
	private final VoucherRepository voucherRepository;
	 
	public TransactionApi(IUserApi userApi, PQTransactionRepository transactionRepository, ISMSSenderApi smsSenderApi,
			IMailSenderApi mailSenderApi, ICommissionApi commissionApi,
			PQAccountDetailRepository pqAccountDetailRepository, PQServiceRepository pqServiceRepository,
			SharePointsLogRepository sharePointsLogRepository,PGDetailsRepository pgDetailsRepository, VisaTransactionRepository visaTransactionRepository,UserRepository userRepository,SavaariBookingRepository savaariBookingRepository,TPTransactionRepository tPTransactionRepository,BulkFileRepository bulkFileRepository
			,WoohooCardDetailsRepository woohooCardDetailsRepository,PQServiceTypeRepository pqServiceTypeRepository,
			MerchantRefundRequestRepository merchantRefundRequestRepository,ITransactionApi ipayTransactionApi,
			DataConfigRepository dataConfigRepository,PredictAndWinRepository predictAndWinRepository,VoucherRepository voucherRepository) {
		this.userApi = userApi;
		this.transactionRepository = transactionRepository;
		this.smsSenderApi = smsSenderApi;
		this.mailSenderApi = mailSenderApi;
		this.commissionApi = commissionApi;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.sharePointsLogRepository = sharePointsLogRepository;
		this.pgDetailsRepository = pgDetailsRepository;
		this.visaTransactionRepository = visaTransactionRepository;
		this.userRepository=userRepository;
		this.savaariBookingRepository=savaariBookingRepository;
		this.tPTransactionRepository=tPTransactionRepository;
		this.bulkFileRepository=bulkFileRepository;
		this.woohooCardDetailsRepository=woohooCardDetailsRepository;
		this.pqServiceTypeRepository=pqServiceTypeRepository;
		this.merchantRefundRequestRepository=merchantRefundRequestRepository;
		this.ipayTransactionApi = ipayTransactionApi;
		this.dataConfigRepository = dataConfigRepository;
		this.predictAndWinRepository=predictAndWinRepository;
		this.voucherRepository = voucherRepository;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public PQTransaction findByTransactionRefNoAndStatus(String transactionReferenceNumber, Status status) {
		return transactionRepository.findByTransactionRefNoAndStatus(transactionReferenceNumber, status);
	}

	@Override
	public List<PQTransaction> getMonthlyTransaction(int month) {
		List<PQTransaction> pq = (List<PQTransaction>) transactionRepository.getMonthlyTransaction(month);
		return (List<PQTransaction>) pq;
	}

	@Override
	public List<PQTransaction> getDailyTransactionBetweeen(Date from, Date to) {
		List<PQTransaction> pq = (List<PQTransaction>) transactionRepository.getDailyTransactionBetween(from, to);
		return (List<PQTransaction>) pq;
	}

	@Override
	public List<PQTransaction> getDailyTransactionBetweenForAccount(Date from, Date to, PQAccountDetail account) {
		List<PQTransaction> pq = (List<PQTransaction>) transactionRepository.getDailyTransactionBetweenForAccount(from,
				to, account);
		return (List<PQTransaction>) pq;
	}

	@Override
	public List<PQTransaction> getDailyTransactionByDate(Date from) {
		List<PQTransaction> pq = (List<PQTransaction>) transactionRepository.getDailyTransactionByDate(from);
		return (List<PQTransaction>) pq;
	}

	@Override
	public List<PQTransaction> getDailyTransactionByDateForAccount(Date from, PQAccountDetail account) {
		List<PQTransaction> pq = (List<PQTransaction>) transactionRepository.getDailyTransactionByDateForAccount(from,
				account);
		return (List<PQTransaction>) pq;
	}

	@Override
	public List<PQTransaction> listTransactionByPaging(PagingDTO dto) {
		List<PQTransaction> transactionList = null;
		Date startDate = dto.getStartDate();
		Date endDate = dto.getEndDate();
		if((startDate!= null) && (endDate != null)){
			transactionList = transactionRepository.getTransactionBetween(startDate,endDate);
		}else {
			transactionList = (List<PQTransaction>)transactionRepository.findAll();
		}
		return transactionList;
	}

	@Override
	public List<PQTransaction> listTransactionByServiceAndDate(Date from, Date to, PQServiceType serviceType, boolean debit, TransactionType transactionType, Status status) {
		return transactionRepository.findTransactionByServiceAndDate(from,to,debit,transactionType,status);
	}

//	for nikkiChat 
	
	@Override
	public List<PQTransaction> listTransactionByDate(boolean debit,Date from, Date to, PQService service, Status status) {
		return transactionRepository.findTransactionByDate(debit,from,to,service,status);
	}
	
	
//	for debit transaction
	
	@Override
	public List<PQTransaction> listDebitTransaction(boolean debit,Date from, Date to, Status status) {
		return transactionRepository.findDebitTransactionByDate(debit,from,to,status);
	}
	
	
	
	// for credit transaction
	
	@Override
	public List<PQTransaction> listCreditTransaction(boolean debit,Date from, Date to, Status status) {
		return transactionRepository.findCreditTransactionByDate(debit,from,to,status);
	}
	
	
	
	@Override
	public List<PQTransaction> listCommissionTransactionByServiceAndDate(Date from, Date to, PQServiceType serviceType, boolean debit, TransactionType transactionType, Status status) {
		return transactionRepository.findCommissionTransactionByServiceAndDate(from,to,debit,transactionType,status);
	}

	@Override
	public Long countDailyTransactionByDate(Date from) {
		return transactionRepository.countDailyTransactionByDate(from);
	}
	//for tx id clickable
	@Override
	public PQTransaction transactionIdClickable(String transactionRefNo){
		return transactionRepository.findByTransactionRefNo(transactionRefNo);
	}

	@Override
	public double countTotalReceivedEBS() {
		double totalEBS = 0.0;
		try {
			PQService service = pqServiceRepository.findServiceByCode("LMC");
			Double totalEBS1 = transactionRepository.getValidAmountByService(service);
			if(totalEBS1!=null){
				totalEBS = totalEBS1;
			}
			return totalEBS;
		}catch(Exception ex){
			return totalEBS;
		}
	}

	@Override
	public double countTotalReceivedEBSNow() {
		try {
			PQService service = pqServiceRepository.findServiceByCode("LMC");
			String date = dateOnly.format(new Date());
			Date newDate = null;
			try {
				newDate = dateOnly.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			double totalEBS = transactionRepository.getValidAmountByServiceForDate(service, newDate);
			return totalEBS;
		}catch(Exception e){
			return 0;
		}
	}

	@Override
	public double countTotalReceivedVNET() {
		double totalVNet = 0.0;
		try {
			PQService service = pqServiceRepository.findServiceByCode("LMB");
			System.err.print(service);
			Double totalVNet1 = transactionRepository.getValidAmountByService(service);
			if(totalVNet1!=null){
				totalVNet = totalVNet1;
			}
			return totalVNet;
		}catch(Exception e){
			return totalVNet;
		}
	}

	@Override
	public double countTotalReceivedVNETNow() {
		try {
			PQService service = pqServiceRepository.findServiceByCode("LMB");
			String date = dateOnly.format(new Date());
			Date newDate = null;
			try {
				newDate = dateOnly.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			double totalVNet = transactionRepository.getValidAmountByServiceForDate(service, newDate);
			return totalVNet;
		}catch(Exception e){
			return 0;
		}
	}

	@Override
	public double countTotalTopupNow() {
		User user = userApi.findByUserName("instantpay@payqwik.in");
		double topupTotal = 0;
		String date = dateOnly.format(new Date());
		Date newDate = null;
		try {
			newDate = dateOnly.parse(date);
			topupTotal = transactionRepository.getValidTopupAmountForDate(user.getAccountDetail(),newDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return topupTotal;
	}

	@Override
	public List<PQTransaction> getLoadMoneyTransactions(Status status) {
		PQService ebs  = pqServiceRepository.findServiceByOperatorCode("LMC");
		PQService vnet = pqServiceRepository.findServiceByOperatorCode("LMB");
		List<PQTransaction> transactionList = transactionRepository.getLoadMoneyTransactions(ebs,vnet,status);
		return transactionList;
	}

	@Override
	public double countTotalPay() {
		List<PQTransaction> filteredTransactions = getTotalTransactions();
		double amount = 0;
		for (PQTransaction t : filteredTransactions) {
			amount += t.getAmount();
		}
		return amount;
	}

	@Override
	public PQTransaction getLastReverseTransactionOfUser(PQAccountDetail account) {
		List<PQTransaction> trans = transactionRepository.getReverseTransactionsOfUser(account);
		if (trans != null && !trans.isEmpty()) {
			return trans.get(trans.size() - 1);
		}
		return null;
	}

	@Override
	public Long countDailyTransactionByDateForAccount(Date from, PQAccountDetail account) {
		return transactionRepository.countDailyTransactionByDateForAccount(from, account);
	}

	@Override
	public PQTransaction getTransactionByRefNo(String transactionRefNo) {
		return transactionRepository.findByTransactionRefNo(transactionRefNo);
	}

	@Override
	public Date getLastTranasactionTimeStamp(PQAccountDetail account) {
		return transactionRepository.getTimeStampOfLastTransaction(account);
	}

	@Override
	public Date getLastTranasactionTimeStampByStatus(PQAccountDetail account, Status status) {
		return transactionRepository.getTimeStampOfLastTransactionByStatus(account,status);
	}

	@Override
	public Page<PQTransaction> getTotalTransactions(Pageable page) {
		return transactionRepository.findAll(page);
	}

	@Override
	public Page<PQTransaction> getTotalTransactionsFiltered(Pageable page,Date from,Date to) {
		return transactionRepository.findAllByFiltered(page,from,to);
	}

	@Override
	public PQTransaction processRefundTransaction(String transactionRefNo) {
		PQTransaction transaction = getTransactionByRefNo(transactionRefNo);
		if(transaction != null) {
			Status status = transaction.getStatus();
			if(status.equals(Status.Success)) {
				transaction.setStatus(Status.Refunded);
				transaction = saveOrUpdate(transaction);
			}
		}
		return transaction;
	}

	@Override
	public List<TransactionListDTO> getmVisaTransaction(){
		List<PQTransaction> transaction= transactionRepository.findVisaMerchantTransactions();
		List<TransactionListDTO> resultList=new ArrayList<>();

		for (PQTransaction pqTransaction : transaction) {
			TransactionListDTO dto=new TransactionListDTO();
			User user=userRepository.getByAccount(pqTransaction.getAccount());
			dto.setUsername(user.getUserDetail().getFirstName());
			dto.setEmail(user.getUserDetail().getEmail());
			dto.setAuthority(user.getAuthority());
			dto.setContactNo(user.getUsername());
			dto.setTransactionRefNo(pqTransaction.getTransactionRefNo());
			dto.setDateOfTransaction(pqTransaction.getCreated());
			dto.setAmount(pqTransaction.getAmount());
			dto.setStatus(pqTransaction.getStatus());
			dto.setAuthReferenceNo((pqTransaction.getAuthReferenceNo() != null) ? pqTransaction.getAuthReferenceNo() : "NA");
			if(pqTransaction.getRetrivalReferenceNo()!=null){
				dto.setRetrivalReferenceNo(pqTransaction.getRetrivalReferenceNo());
			}else{
				dto.setRetrivalReferenceNo("NA");
			}
			dto.setDescription(pqTransaction.getDescription());
			resultList.add(dto);
		}
		return resultList;
	}
	
	
	@Override
	public List<TransactionListDTO> getTreatCardTransaction(PQService service){
		List<PQTransaction> transaction= transactionRepository.findTreatCardTransactions(service);
		List<TransactionListDTO> resultList=new ArrayList<>();
		for (PQTransaction pqTransaction : transaction) {
			TransactionListDTO dto=new TransactionListDTO();
			User user=userRepository.getByAccount(pqTransaction.getAccount());
			dto.setUsername(user.getUserDetail().getFirstName());
			dto.setEmail(user.getUserDetail().getEmail());
			dto.setAuthority(user.getAuthority());
			dto.setContactNo(user.getUsername());
			dto.setTransactionRefNo(pqTransaction.getTransactionRefNo());
			dto.setDateOfTransaction(pqTransaction.getCreated());
			dto.setAmount(pqTransaction.getAmount());
			dto.setStatus(pqTransaction.getStatus());
			dto.setDescription(pqTransaction.getDescription());
			resultList.add(dto);
		}
		return resultList;
	}


	@Override
	public List<TransactionListDTO> getmVisaTransactionFilter(Date from,Date to){
		List<PQTransaction> transaction= transactionRepository.findVisaMerchantTransactionsFilter(from,to);
		List<TransactionListDTO> resultList=new ArrayList<>();

		for (PQTransaction pqTransaction : transaction) {
			TransactionListDTO dto=new TransactionListDTO();
			User user=userRepository.getByAccount(pqTransaction.getAccount());
			dto.setUsername(user.getUserDetail().getFirstName());
			dto.setEmail(user.getUserDetail().getEmail());
			dto.setAuthority(user.getAuthority());
			dto.setContactNo(user.getUsername());
			dto.setTransactionRefNo(pqTransaction.getTransactionRefNo());
			dto.setDateOfTransaction(pqTransaction.getCreated());
			dto.setAmount(pqTransaction.getAmount());
			dto.setStatus(pqTransaction.getStatus());
			dto.setAuthReferenceNo((pqTransaction.getAuthReferenceNo() != null) ? pqTransaction.getAuthReferenceNo() : "NA");
			if(pqTransaction.getRetrivalReferenceNo()!=null){
				dto.setRetrivalReferenceNo(pqTransaction.getRetrivalReferenceNo());
			}else{
				dto.setRetrivalReferenceNo("NA");
			}
			dto.setDescription(pqTransaction.getDescription());
			resultList.add(dto);
		}
		return resultList;
	}

	@Override
	public Page<PQTransaction> getTotalTransactionsOfUser(Pageable page, String username) {
		User user = userApi.findByUserName(username);
		PQAccountDetail account = user.getAccountDetail();
		return transactionRepository.findAllTransactionByUser(page, account);
	}

	@Override
	public Page<PQTransaction> getTotalTransactionsOfUserFilter(Pageable page,Date StartDate,Date endDate, String username) {
		User user = userApi.findByUserName(username);
		PQAccountDetail account = user.getAccountDetail();
		return transactionRepository.findAllTransactionByUserFilter(page,StartDate,endDate, account);
	}

	@Override
	public List<PQTransaction> transactionListByService(PQService service) {
		return transactionRepository.findAllTransactionForMerchant(service);
	}


	@Override
	public Page<PQTransaction> transactionListByServicePage(Pageable page,PQService service) {
		return transactionRepository.findMerchantTransactions(page,service,TransactionType.DEFAULT);
	}
	
	@Override
	public Page<PQTransaction> last20transactionListByServicePage(Pageable page,PQService service) {
		return transactionRepository.findlast20MerchantTransactions(page,service,TransactionType.DEFAULT);
	}
	
	@Override
	public Page<PQTransaction> findDebittransactionListByServicePage(Pageable page,PQAccountDetail account) {
		return transactionRepository.findDebitMerchantTransactions(page,account,TransactionType.DEFAULT);
	}

	@Override
	public Page<PQTransaction> transactionListServicePage(Pageable page,PQService service) {
		return transactionRepository.findSuccessMerchantTransactions(page,service,TransactionType.DEFAULT);
	}
	
	@Override
	public Page<PQTransaction> transactionListServiceBescom(Pageable page,PQService service) {
		return transactionRepository.findBescomMerchantTransactions(page,service,TransactionType.DEFAULT);
	}
	
	
	@Override
	public Page<PQTransaction> transactionListServicePageForTreatCard(Pageable page,PQService service) {
		return transactionRepository.findSuccessMerchantTransactionsForTreatCard(page,service,TransactionType.DEFAULT);
	}

	@Override
	public Page<PQTransaction> transactionListByService(Pageable page,PQService service) {
		return transactionRepository.findSuccessMerchantTransactions(page,service,TransactionType.DEFAULT);
	}

	@Override
	public Page<PQTransaction> transactionListByServicePageFiltered(Pageable page,PQService service,Date from,Date to) {
		return transactionRepository.findMerchantTransactionsFiltered(page,service,TransactionType.DEFAULT,from,to);
	}

	@Override
	public Page<PQTransaction> transactionListByServicePageCommssion(Pageable page,PQService service) {
		return transactionRepository.findMerchantTransactions(page,service,TransactionType.COMMISSION);
	}
	

	@Override
	public List<PQTransaction> filterListByTypeAndStatus(List<PQService> services, Date startDate, Date endDate, Status status) {
		List<PQTransaction> transactionBetween = transactionRepository.getTransactionBetween(services, startDate, endDate, TransactionType.DEFAULT, status);
		return transactionBetween;
	}
	
	@Override
	public List<PQTransaction> filterListByTypeAndStatusDebit(List<PQService> services, Date startDate, Date endDate, Status status) {
		List<PQTransaction> transactionBetween = transactionRepository.getTransactionBetweenDebit(services, startDate, endDate, TransactionType.DEFAULT, status);
		return transactionBetween;
	}
	
	@Override
	public List<PQTransaction> filterListByTypeAndStatusAndRefNo(Date startDate, Date endDate,String trasnactionRefNo) {
		List<PQTransaction> transactionBetween = transactionRepository.getTransactionBetweenDateAndRefNo(startDate, endDate, TransactionType.DEFAULT,trasnactionRefNo);
		return transactionBetween;
	}



	@Override
	public EBSStatusResponseDTO checkLoadMoneyStatus(String transactionRefNo,String serviceCode) {
		EBSStatusResponseDTO result = new EBSStatusResponseDTO();
		Client client = Client.create();
		WebResource webResource = null;
		if(serviceCode.equalsIgnoreCase("LMC")) {
			System.err.println("transaction ref no");
			webResource = client.resource(DeploymentConstants.WEB_URL+"/ws/api/LoadMoney/"+transactionRefNo+"/Status");
			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
			String resp = response.getEntity(String.class);
			System.err.println(resp);
			if(response.getStatus() == 200) {
				try {
					JSONObject json = new JSONObject(resp);
					result.setStatus(JSONParserUtil.getString(json,"status"));
					result.setSuccess(json.getBoolean("success"));
					result.setMessage(json.getString("message"));
					result.setErrorMessage(json.getString("error"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}else {

		}
		return result;
	}

	@Override
	public List<PQTransaction> transactionListByServiceAndDebit(PQService service, boolean debit) {
		return transactionRepository.findTransactionByServiceAndDebit(service,debit);
	}

	@Override
	public List<PQTransaction> getTotalSuccessfulTransactionsOfUser(String user) {
		User u = userApi.findByUserName(user);
		PQAccountDetail account = u.getAccountDetail();
		return transactionRepository.findAllTransactionByUserAndStatus(account, Status.Success);
	}

	@Override
	public PQTransaction getLastTransactionsOfUser(User user) {
		List<PQTransaction> trans = transactionRepository.getTotalTransactions(user.getAccountDetail());
		if (trans != null && !trans.isEmpty()) {
			return trans.get(trans.size() - 1);
		}
		return null;
	}

	@Override
	public void revertSendMoneyOperations() {
		Date now = new Date();
		PQService service = pqServiceRepository.findServiceByCode("SMU");
		List<PQTransaction> sendMoneyTransactions = transactionRepository.findTransactionByService(service);
		for (PQTransaction pq : sendMoneyTransactions) {
			String transactionRefNo = pq.getTransactionRefNo();
			if (transactionRefNo != null) {
				transactionRefNo = transactionRefNo.replaceAll("[^0-9]", "");
			}
			PQTransaction receiverTransaction = findByTransactionRefNoAndStatus(transactionRefNo + "C", Status.Success);
			if (receiverTransaction != null) {
				Date d = pq.getCreated();
				long daysSpent = (now.getTime() - d.getTime()) / (1000 * 60 * 60 * 24);
				if (daysSpent >= 18) {
					User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
					if (receiver.getMobileStatus() == Status.Inactive) {
						reverseTransaction(transactionRefNo);
					}
				}
			}
		}
	}

	@Override
	public int updateFavouriteTransaction(String transactionRefNo, boolean favourite) {
		return transactionRepository.updateFavouriteTransaction(transactionRefNo, favourite);
	}

	@Override
	public void initiateSendMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = senderAccount.getBalance();
		/*TODO calculate transaction amount commission */
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		/*TODO create a transaction log for sender */
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Initiated);
			/*TODO debit amount from sender */
			senderAccount.setBalance(senderCurrentBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		User receiver = userApi.findByUserName(receiverUsername);
		PQTransaction receiverTransaction = new PQTransaction();
		PQTransaction receiverTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		PQAccountDetail receiverAccount = receiver.getAccountDetail();
		/*TODO create transaction log for receiver*/
		if (receiverTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = receiverAccount.getBalance();
			receiverTransaction.setCurrentBalance(receiverCurrentBalance);
			receiverTransaction.setAmount(receiverTransactionAmount);
			receiverTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			receiverTransaction.setDescription(description);
			receiverTransaction.setService(service);
			receiverTransaction.setAccount(receiverAccount);
			receiverTransaction.setTransactionRefNo(transactionRefNo + "C");
			receiverTransaction.setDebit(false);
			receiverTransaction.setStatus(Status.Initiated);
			transactionRepository.save(receiverTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		/*TODO create commission log */
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successSendMoney(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String senderUsername = "";
		String receiverUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_SENDER, sender, senderTransaction, receiverUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
							sender, senderTransaction, receiverUsername,null);
				}
			}
		}


		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				/*TODO credit amount to receiver */
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				receiverAccount.setBalance(receiverCurrentBalance);
				PQAccountDetail updatedDetails = pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					if (serviceCode.equalsIgnoreCase("SMU")) {
						smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendTransactionMail("VPayQwik Transaction",
								MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
								senderUsername,null);
					} else {
						smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendTransactionMail("VPayQwik Transaction",
								MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
								senderUsername,null);
					}
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				/*TODO credit amount to commission account */
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				transactionRepository.save(commissionTransaction);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				PQAccountDetail commissionUpdatedDetails = pqAccountDetailRepository.save(commissionAccountDetail);
			}
		}
	}

	@Override
	public void failedSendMoney(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				/*TODO credit amount back to sender */
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}
	
	@Override
	public void transferMoneyToWallet(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		/*double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);*/
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = senderAccount.getBalance();
		/*TODO calculate transaction amount commission */
		/*TODO create a transaction log for sender */
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", senderCurrentBalance)));
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			/*TODO debit amount from sender */
			senderAccount.setBalance(Double.parseDouble(String.format("%.2f", senderCurrentBalance)));
			long accountNumber = senderTransaction.getAccount().getAccountNumber();
			long points = calculatePoints(netTransactionAmount);
			pqAccountDetailRepository.addUserPoints(points, accountNumber);
			pqAccountDetailRepository.save(senderAccount);
			PQTransaction t = transactionRepository.save(senderTransaction);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
						SMSTemplate.FUNDTRANSFER_SUCCESS_SENDER, sender, senderTransaction, receiverUsername);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
						sender, senderTransaction, receiverUsername,null);
			}
		}

		User receiver = userApi.findByUserName(receiverUsername);
		PQTransaction receiverTransaction = new PQTransaction();
		PQTransaction receiverTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		PQAccountDetail receiverAccount = receiver.getAccountDetail();
		
		/*TODO create transaction log for receiver*/
		if (receiverTransactionExists == null) {
			double receiverTransactionAmount = 0;
			double receiverCurrentBalance = receiverAccount.getBalance();
			receiverTransactionAmount = receiverCurrentBalance +netTransactionAmount;
			receiverTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", receiverTransactionAmount)));
			receiverTransaction.setAmount(netTransactionAmount);
			receiverTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			receiverTransaction.setDescription(description);
			receiverTransaction.setService(service);
			receiverTransaction.setAccount(receiverAccount);
			receiverTransaction.setTransactionRefNo(transactionRefNo + "C");
			receiverTransaction.setDebit(false);
			receiverTransaction.setStatus(Status.Success);
			receiverAccount.setBalance(Double.parseDouble(String.format("%.2f", receiverTransactionAmount)));
			pqAccountDetailRepository.save(receiverAccount);
			PQTransaction t = transactionRepository.save(receiverTransaction);
			if (t != null) {
				String serviceCode = t.getService().getCode();
				if (serviceCode.equalsIgnoreCase("SMU")) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
							senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
							senderUsername,null);
				} else {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
							senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
							senderUsername,null);
				}
			}
		}
	}


	@Override
	public void initiateSharePoints(long points, String description, String transactionRefNo, String senderUsername,
			String receiverUsername) {
		User sender = userApi.findByUserName(senderUsername);
		long senderPoints = sender.getAccountDetail().getPoints();
		SharePointsLog exists = sharePointsLogRepository.getByTransactionRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderPoints -= points;
			exists = new SharePointsLog();
			exists.setPoints(points);
			exists.setDescription(description);
			exists.setTransactionRefNo(transactionRefNo + "D");
			exists.setAccount(sender.getAccountDetail());
			exists.setStatus(Status.Initiated);
			sharePointsLogRepository.save(exists);
			userApi.updatePoints(senderPoints, sender.getAccountDetail());
		}

		User receiver = userApi.findByUserName(receiverUsername);
		SharePointsLog receiverLog = sharePointsLogRepository.getByTransactionRefNo(transactionRefNo + "C");
		if (receiverLog == null) {
			receiverLog = new SharePointsLog();
			receiverLog.setPoints(points);
			receiverLog.setDescription(description);
			receiverLog.setAccount(receiver.getAccountDetail());
			receiverLog.setTransactionRefNo(transactionRefNo + "C");
			receiverLog.setStatus(Status.Initiated);
			sharePointsLogRepository.save(receiverLog);
		}

	}

	@Override
	public void successSharePoints(String transactionRefNo) {

		long receiverCurrentPoints = 0;
		SharePointsLog senderLog = sharePointsLogRepository.getByTransactionRefNo(transactionRefNo + "D");
		SharePointsLog receiverLog = sharePointsLogRepository.getByTransactionRefNo(transactionRefNo + "C");
		if (senderLog != null) {
			User sender = userApi.findByAccountDetail(senderLog.getAccount());
			User receiver = userApi.findByAccountDetail(receiverLog.getAccount());
			if ((senderLog.getStatus().equals(Status.Initiated))) {
				senderLog.setStatus(Status.Success);
				senderLog.setLastModified(new Date());
				sharePointsLogRepository.save(senderLog);
			}
		}

		if (receiverLog != null) {
			if ((receiverLog.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverLog.getAccount());
				receiverCurrentPoints = receiver.getAccountDetail().getPoints();
				receiverCurrentPoints += receiverLog.getPoints();
				receiverLog.setStatus(Status.Success);
				receiverLog.setLastModified(new Date());
				sharePointsLogRepository.save(receiverLog);
				userApi.updatePoints(receiverCurrentPoints, receiver.getAccountDetail());
			}

		}

	}

	@Override
	public void initiateMerchantPayment(double amount,String description,String remarks,PQService service,String transactionRefNo,
			String senderUsername, String receiverUsername, boolean isPG) {
		User sender = userApi.findByUserName(senderUsername);
		PQCommission receiverCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(receiverCommission, amount);
		System.err.println("commission value ::" + netCommissionValue);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double roundSenderCurrentBalance=0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (receiverCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount - netCommissionValue;
		}

		if(isPG){
			User  stateGST = userApi.findByUserName(StartupUtil.STATE_GST);
			PQAccountDetail stateAccount = stateGST.getAccountDetail();
			PQService sgstService = pqServiceRepository.findServiceByCode("SGST");
			PQCommission sgstCommission = commissionApi.findCommissionByServiceAndAmount(sgstService,netCommissionValue);
			double sgstValue = commissionApi.getCommissionValue(sgstCommission,netCommissionValue);
			netTransactionAmount = netTransactionAmount - sgstValue;

			//  create transaction for sgst

			PQTransaction sgstTransaction = getTransactionByRefNo(transactionRefNo+"SGST");
			if(sgstTransaction == null) {
				sgstTransaction = new PQTransaction();
				sgstTransaction.setDescription(description);
				sgstTransaction.setStatus(Status.Initiated);
				sgstTransaction.setTransactionType(TransactionType.SGST);
				sgstTransaction.setService(service);
				sgstTransaction.setAmount(sgstValue);
				sgstTransaction.setAccount(stateAccount);
				sgstTransaction.setTransactionRefNo(transactionRefNo + "SGST");
				sgstTransaction.setDebit(false);
				sgstTransaction.setCurrentBalance(stateAccount.getBalance());
				transactionRepository.save(sgstTransaction);
			}

			User cgstUser = userApi.findByUserName(StartupUtil.CENTRAL_GST);
			PQAccountDetail cgstAccount = cgstUser.getAccountDetail();
			PQService cgstService = pqServiceRepository.findServiceByCode("CGST");
			PQCommission cgstCommission = commissionApi.findCommissionByServiceAndAmount(cgstService,netCommissionValue);
			double cgstValue = commissionApi.getCommissionValue(cgstCommission,netCommissionValue);
			netTransactionAmount = netTransactionAmount - cgstValue;

			//  create transaction for cgst

			PQTransaction cgstTransaction = getTransactionByRefNo(transactionRefNo+"CGST");
			if(cgstTransaction == null) {
				cgstTransaction = new PQTransaction();
				cgstTransaction.setDescription(description);
				cgstTransaction.setStatus(Status.Initiated);
				cgstTransaction.setTransactionType(TransactionType.CGST);
				cgstTransaction.setService(service);
				cgstTransaction.setAmount(cgstValue);
				cgstTransaction.setAccount(cgstAccount);
				cgstTransaction.setTransactionRefNo(transactionRefNo + "CGST");
				cgstTransaction.setDebit(false);
				cgstTransaction.setCurrentBalance(cgstAccount.getBalance());
				transactionRepository.save(cgstTransaction);
			}
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderCurrentBalance = senderUserBalance - amount;
			roundSenderCurrentBalance=BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue();
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setRemarks(remarks);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(roundSenderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccount.setBalance(roundSenderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}
		User merchant = userApi.findByUserName(receiverUsername);
		PQTransaction merchantTransaction = new PQTransaction();
		PQTransaction merchantTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (merchantTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = merchant.getAccountDetail().getBalance();
			double roundReceiverCurrentBalance=BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue();
			merchantTransaction.setCurrentBalance(roundReceiverCurrentBalance);
			merchantTransaction.setAmount(receiverTransactionAmount);
			merchantTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			merchantTransaction.setDescription(description);
			merchantTransaction.setService(service);
			merchantTransaction.setAccount(merchant.getAccountDetail());
			merchantTransaction.setTransactionRefNo(transactionRefNo + "C");
			merchantTransaction.setDebit(false);
			merchantTransaction.setStatus(Status.Initiated);
			transactionRepository.save(merchantTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successMerchantPayment(String transactionRefNo,boolean isPG) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String senderUsername = "";
		String receiverUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			String receiverName = receiver.getUserDetail().getFirstName();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.MERCHANT_SENDER,
							sender, senderTransaction,receiverName);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.MERCHANT_SENDER, sender,
							senderTransaction,receiverName, MailConstants.CC_MAIL);
				}
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);

				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.MERCHANT_RECEIVER,
							receiver, receiverTransaction,senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.MERCHANT_RECEIVER,
							receiver, receiverTransaction,senderUsername,MailConstants.CC_MAIL);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				//				userApi.updateBalance(commissionCurrentBalance, commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}

		if(isPG) {
			/*            PQTransaction vatTransaction = getTransactionByRefNo(transactionRefNo+"VAT");
            if(vatTransaction != null) {
                if(vatTransaction.getStatus().equals(Status.Initiated)) {
                    User vatAccount =  userApi.findByAccountDetail(vatTransaction.getAccount());
                    PQAccountDetail vatAccountDetail = vatAccount.getAccountDetail();
                    double vatCurrentBalance = vatAccountDetail.getBalance() + vatTransaction.getAmount();
                    vatAccountDetail.setBalance(vatCurrentBalance);
                    vatTransaction.setStatus(Status.Success);
                    vatTransaction.setCurrentBalance(vatCurrentBalance);
                    pqAccountDetailRepository.save(vatAccountDetail);
                    transactionRepository.save(vatTransaction);
                }
            }


            PQTransaction sbcTransaction = getTransactionByRefNo(transactionRefNo+"SBC");
            if(sbcTransaction != null) {
                if(sbcTransaction.getStatus().equals(Status.Initiated)) {
                    User sbcAccount =  userApi.findByAccountDetail(sbcTransaction.getAccount());
                    PQAccountDetail sbcAccountDetail = sbcAccount.getAccountDetail();
                    double sbcCurrentBalance = sbcAccountDetail.getBalance() + sbcTransaction.getAmount();
                    sbcAccountDetail.setBalance(sbcCurrentBalance);
                    sbcTransaction.setStatus(Status.Success);
                    sbcTransaction.setCurrentBalance(sbcCurrentBalance);
                    pqAccountDetailRepository.save(sbcAccountDetail);
                    transactionRepository.save(sbcTransaction);
                }
            }

            PQTransaction kkcTransaction = getTransactionByRefNo(transactionRefNo+"KKC");
            if(kkcTransaction != null) {
                if(kkcTransaction.getStatus().equals(Status.Initiated)) {
                    User kkcAccount =  userApi.findByAccountDetail(kkcTransaction.getAccount());
                    PQAccountDetail kkcAccountDetail = kkcAccount.getAccountDetail();
                    double kkcCurrentBalance = kkcAccountDetail.getBalance() + kkcTransaction.getAmount();
                    kkcAccountDetail.setBalance(kkcCurrentBalance);
                    kkcTransaction.setStatus(Status.Success);
                    kkcTransaction.setCurrentBalance(kkcCurrentBalance);
                    pqAccountDetailRepository.save(kkcAccountDetail);
                    transactionRepository.save(kkcTransaction);
                }
            }
			 */            PQTransaction sgstTransaction = getTransactionByRefNo(transactionRefNo+"SGST");
			 if(sgstTransaction != null) {
				 if(sgstTransaction.getStatus().equals(Status.Initiated)) {
					 User sgstAccount =  userApi.findByAccountDetail(sgstTransaction.getAccount());
					 PQAccountDetail sgstAccountDetail = sgstAccount.getAccountDetail();
					 double sgstCurrentBalance = sgstAccountDetail.getBalance() + sgstTransaction.getAmount();
					 sgstAccountDetail.setBalance(sgstCurrentBalance);
					 sgstTransaction.setStatus(Status.Success);
					 sgstTransaction.setLastModified(new Date());
					 sgstTransaction.setCurrentBalance(sgstCurrentBalance);
					 pqAccountDetailRepository.save(sgstAccountDetail);
					 transactionRepository.save(sgstTransaction);
				 }
			 }
			 PQTransaction cgstTransaction = getTransactionByRefNo(transactionRefNo+"CGST");
			 if(cgstTransaction != null) {
				 if(cgstTransaction.getStatus().equals(Status.Initiated)) {
					 User cgstAccount =  userApi.findByAccountDetail(cgstTransaction.getAccount());
					 PQAccountDetail cgstAccountDetail = cgstAccount.getAccountDetail();
					 double cgstCurrentBalance = cgstAccountDetail.getBalance() + cgstTransaction.getAmount();
					 cgstAccountDetail.setBalance(cgstCurrentBalance);
					 cgstTransaction.setStatus(Status.Success);
					 cgstTransaction.setLastModified(new Date());
					 cgstTransaction.setCurrentBalance(cgstCurrentBalance);
					 pqAccountDetailRepository.save(cgstAccountDetail);
					 transactionRepository.save(cgstTransaction);
				 }
			 }

		}


	}

	@Override
	public void failedMerchantPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,MailConstants.CC_MAIL);
			}
		}

		//		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		//		if (debitSettlementTransaction != null) {
		//			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
		//			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
		//			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
		//			String debitSettlementDescription = debitSettlementTransaction.getDescription();
		//			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
		//			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
		//			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
		//			PQTransaction debitSettlementTransaction1 = new PQTransaction();
		//			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
		//			if (settlementTransactionExists == null) {
		//				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
		//				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
		//				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
		//				debitSettlementTransaction1.setDescription(debitSettlementDescription);
		//				debitSettlementTransaction1.setService(senderService);
		//				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
		//				debitSettlementTransaction1.setDebit(false);
		//				debitSettlementTransaction1.setAccount(accountDetail);
		//				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
		//				debitSettlementTransaction1.setStatus(Status.Success);
		//				accountDetail.setBalance(debitSettlementCurrentBalance);
		//				pqAccountDetailRepository.save(accountDetail);
		//				//				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
		//				transactionRepository.save(debitSettlementTransaction1);
		//			}
		//		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,MailConstants.CC_MAIL);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public OnePayResponse getTransaction(String transactionRefNo,User u) {
		OnePayResponse result = new OnePayResponse();
		PQTransaction transaction = getTransactionByRefNo(transactionRefNo);
		long account = u.getAccountDetail().getAccountNumber();
		if(transaction != null){
			boolean debit = transaction.isDebit();
			if(debit) {
				long transactionAccount = transaction.getAccount().getAccountNumber();
				if (account == transactionAccount) {
					String serviceCode = transaction.getService().getCode();
					String completeRefNo = transaction.getTransactionRefNo();
					completeRefNo = completeRefNo.substring(0, completeRefNo.length() - 1);
					String json = userApi.getTransactionRequestInJSON(serviceCode, completeRefNo);
					if(json != null) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setServiceCode(transaction.getService().getCode());
						result.setServiceName(transaction.getService().getName());
						result.settStatus(transaction.getStatus());
						result.setJson(json);
					}else  {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Qwik Pay not available for this transaction");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Transaction not belong to this account");
				}
			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Qwik Pay is valid for debit transactions only");
			}
		}else  {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction Not Found");
		}
		return result;
	}

	@Override
	public void initiateVisaPayment(double amount, String description, PQService service, String transactionRefNo, String senderUserName, String receiverUserName) {

		User sender = userApi.findByUserName(senderUserName);
		PQCommission receiverCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(receiverCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (receiverCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount - netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderCurrentBalance = senderUserBalance - amount;
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			//			senderTransaction.setRequest(json);
			logger.error("Merchant payment Initiated");
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		Visa visaMerchant = new Visa();
		visaMerchant.setFirstName(sender.getUserDetail().getFirstName());
		visaMerchant.setLastName(sender.getUserDetail().getLastName());
		visaMerchant.setContactNo(sender.getUsername());
		visaMerchant.setAmount(netTransactionAmount);
		visaMerchant.setBillRefNo("");
		visaMerchant.setBusiness("VIJAYA");
		visaMerchant.setDescription("MVISA TRANSACTION");
		visaMerchant.setExternalTransactionId(transactionRefNo);
		visaMerchant.setFromEntityId(sender.getAccountDetail().getAccountNumber()+"");
		visaMerchant.setMerchantData("");
		visaMerchant.setProductId("GENERAL");
		visaMerchant.setSenderCardNo(sender.getAccountDetail().getAccountNumber()+"");
		visaMerchant.setToEntityId("");
		visaMerchant.setTransaction(senderTransaction);
		visaMerchant.setTransactionType("PURCHASE");
		visaTransactionRepository.save(visaMerchant);

		//		User visa = userApi.findByUserName(StartupUtil.MVISA); // changes to merchant dynamically
		User visa = userApi.findByUserName(receiverUserName);
		PQTransaction visaTransaction = new PQTransaction();
		PQTransaction visaTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (visaTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = visa.getAccountDetail().getBalance();
			visaTransaction.setCurrentBalance(receiverCurrentBalance);
			visaTransaction.setAmount(receiverTransactionAmount);
			visaTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			visaTransaction.setDescription(description);
			visaTransaction.setService(service);
			visaTransaction.setAccount(visa.getAccountDetail());
			visaTransaction.setTransactionRefNo(transactionRefNo + "C");
			visaTransaction.setDebit(false);
			visaTransaction.setStatus(Status.Initiated);
			transactionRepository.save(visaTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}

	}
	
	@Override
	public void initiateVisaMerchantPayment(double amount, String description, PQService service, String transactionRefNo, String senderUserName, String receiverUserName,String merchantUserName) {

		User sender = userApi.findByUserName(senderUserName);
		PQCommission receiverCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(receiverCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (receiverCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount - netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderCurrentBalance = senderUserBalance - amount;
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			//			senderTransaction.setRequest(json);
			logger.error("Merchant payment Initiated");
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		Visa visaMerchant = new Visa();
		visaMerchant.setFirstName(sender.getUserDetail().getFirstName());
		visaMerchant.setLastName(sender.getUserDetail().getLastName());
		visaMerchant.setContactNo(sender.getUsername());
		visaMerchant.setAmount(netTransactionAmount);
		visaMerchant.setBillRefNo("");
		visaMerchant.setBusiness("VIJAYA");
		visaMerchant.setDescription("MVISA TRANSACTION");
		visaMerchant.setExternalTransactionId(transactionRefNo);
		visaMerchant.setFromEntityId(sender.getAccountDetail().getAccountNumber()+"");
		visaMerchant.setMerchantData("");
		visaMerchant.setProductId("GENERAL");
		visaMerchant.setSenderCardNo(sender.getAccountDetail().getAccountNumber()+"");
		visaMerchant.setToEntityId("");
		visaMerchant.setTransaction(senderTransaction);
		visaMerchant.setTransactionType("PURCHASE");
		visaTransactionRepository.save(visaMerchant);

		User visa = userApi.findByUserName(receiverUserName);
		PQTransaction visaTransaction = new PQTransaction();
		PQTransaction visaTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (visaTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = visa.getAccountDetail().getBalance();
			visaTransaction.setAmount(receiverTransactionAmount);
			visaTransaction.setCurrentBalance(receiverCurrentBalance);
			visaTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			visaTransaction.setDescription(description);
			visaTransaction.setService(service);
			visaTransaction.setAccount(visa.getAccountDetail());
			visaTransaction.setTransactionRefNo(transactionRefNo + "C");
			visaTransaction.setDebit(false);
			visaTransaction.setStatus(Status.Initiated);
			transactionRepository.save(visaTransaction);
		}
		User visaMerchants = userApi.findByUserName(merchantUserName);
		PQTransaction visaTransactionMerchant = new PQTransaction();
		String newTransactionRefNo=transactionRefNo.substring(2);
		PQTransaction visaMerchantTransactionExists = getTransactionByRefNo(newTransactionRefNo + "C");
		if (visaMerchantTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = visaMerchants.getAccountDetail().getBalance();
			visaTransactionMerchant.setCurrentBalance(receiverCurrentBalance);
			visaTransactionMerchant.setAmount(receiverTransactionAmount);
			visaTransactionMerchant.setCommissionIdentifier(receiverCommission.getIdentifier());
			visaTransactionMerchant.setDescription(description);
			visaTransactionMerchant.setService(service);
			visaTransactionMerchant.setAccount(visaMerchants.getAccountDetail());
			visaTransactionMerchant.setTransactionRefNo(newTransactionRefNo + "C");
			visaTransactionMerchant.setDebit(false);
			visaTransactionMerchant.setStatus(Status.Initiated);
			transactionRepository.save(visaTransactionMerchant);
		}
		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}

	}

	@Override
	public void successVisaPayment(String transactionRefNo, String authReferenceNo, String retrivalReferenceNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String newtransactionRefNo = transactionRefNo.substring(2);
		String receiverUsername = "";
		String senderUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiveMerchantTransaction = getTransactionByRefNo(newtransactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			String receiverName = receiver.getUserDetail().getFirstName();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setAuthReferenceNo(authReferenceNo);
				senderTransaction.setRetrivalReferenceNo(retrivalReferenceNo);
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.BHARATQR_SUCCESS,
							sender, senderTransaction,receiverName);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.BHARATQR_SUCCESS, sender,
							senderTransaction,receiverName, MailConstants.CC_MAIL);
				}
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User visa = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				PQAccountDetail receiverAccount = visa.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.MERCHANT_RECEIVER,
							visa, receiverTransaction,senderUsername,MailConstants.CC_MAIL);
				}
			}
		}
		
		if (receiveMerchantTransaction != null) {
			logger.info("inside vijayabank merchant transactions");
			if ((receiveMerchantTransaction.getStatus().equals(Status.Initiated))) {
				logger.info("inside initiated vijayabank merchant transactions");
				User visa = userApi.findByAccountDetail(receiveMerchantTransaction.getAccount());
				double receiverCurrentBalance = receiveMerchantTransaction.getCurrentBalance();
				double amount = receiveMerchantTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiveMerchantTransaction.setStatus(Status.Success);
				receiveMerchantTransaction.setLastModified(new Date());
				receiveMerchantTransaction.setCurrentBalance(receiverCurrentBalance);
				PQAccountDetail receiverAccount = visa.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				transactionRepository.save(receiveMerchantTransaction);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void failedVisaPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance +  senderTransaction.getAmount();
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				PQAccountDetail senderAccount = senderTransaction.getAccount();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				//                userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,MailConstants.CC_MAIL);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				accountDetail.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				//				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				//                smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
				//                        receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,MailConstants.CC_MAIL);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	private void revertUserSendMoney(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			PQAccountDetail senderAccount = senderTransaction.getAccount();
			User sender = userApi.findByAccountDetail(senderAccount);
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Success))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				//				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				transactionRepository.save(debitSettlementTransaction1);
				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success))) {
				PQAccountDetail receiverAccount = receiverTransaction.getAccount();
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double reducedAmount = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverTransaction.setStatus(Status.Failed);
				receiverAccount.setBalance(reducedAmount);
				pqAccountDetailRepository.save(receiverAccount);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}

	}

	@Override
	public void initiateBillPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername,PQCommission senderCommission,double netCommissionValue) {
		User sender = userApi.findByUserName(senderUsername);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);
		}
		User instantpayAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (instantpayTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = instantpayAccount.getAccountDetail().getBalance();
			instantpayTransaction.setCurrentBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			instantpayTransaction.setAmount(receiverTransactionAmount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(instantpayAccount.getAccountDetail());
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Initiated);
			transactionRepository.save(instantpayTransaction);
		}
		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(BigDecimal.valueOf(commissionCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}
	
	@Override
	public TransactionResponse successBillPayment(String transactionRefNo,PQCommission senderCommission,double netCommissionValue,boolean suspicious,TransactionResponse response) {
		String receiverMobileNumber = null;
		double netTransactionAmount = 0;
		netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setSuspicious(suspicious);
				senderTransaction.setLastModified(new Date());
				response.setRemainingBalance(senderTransaction.getCurrentBalance());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction, receiverMobileNumber,null);
				}
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
				receiverAccount.setBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
				pqAccountDetailRepository.save(receiverAccount);
				transactionRepository.save(receiverTransaction);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(BigDecimal.valueOf(commissionCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
				commissionAccountDetail.setBalance(BigDecimal.valueOf(commissionCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
				pqAccountDetailRepository.save(commissionAccountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
		return response;
	}

	@Override
	public void revertFailedBillPayment() {

	}

	@Override
	public List<PQTransaction> getTotalTransactions() {
		List<PQTransaction> totalTransactions = transactionRepository.getTotalTransactions();
		List<PQTransaction> filteredTransactions = new ArrayList<>();
		for (PQTransaction t : totalTransactions) {
			if (t.getStatus().equals(Status.Success) && t.isDebit()) {
				if ((t.getService().getServiceType().equals("Bill Payment"))) {
					filteredTransactions.add(t);
				}
			}
		}
		return totalTransactions;
	}

	@Override
	public void failedBillPayment(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}

			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void initiateLoadMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername) {
		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setFavourite(false);
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public void successLoadMoney(String transactionRefNo, String retrievalRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setRetrivalReferenceNo(retrievalRefNo);
			senderTransaction.setLastModified(new Date());
			//			senderTransaction.setUpiId(upiId);
			PQTransaction t = transactionRepository.save(senderTransaction);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			pqAccountDetailRepository.save(senderAccount);
			//			int i = userApi.updateBalance(senderCurrentBalance, sender);
			//			logger.error("Updated Load money " + i);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null,null);
			}
		}
	}

	@Override
	public void failedLoadMoney(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
		}
	}

	@Override
	public void initiateUPILoadMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, boolean isSdk) {
		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(UpiConstants.RES_DESCRIPTION + " of Rs." + amount);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setFavourite(false);
			if(isSdk){
				senderTransaction.setSdkRequest(true);
			}else{
				senderTransaction.setSdkRequest(false);
			}
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Sent);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public void successUPILoadMoney(String transactionRefNo) {
		// TODO Auto-generated method stub
		System.out.println("Success ------ --------- ---------- " + transactionRefNo);
//		PQTransaction senderTransaction = getTransactionByRetrivalReferenceNo(retrivalReferenceNo);
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Sent))) {

//			PQTransaction approvedTxn = new PQTransaction();
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			PQTransaction t = transactionRepository.save(senderTransaction);

//			approvedTxn.setCommissionIdentifier(senderTransaction.getCommissionIdentifier());
//			approvedTxn.setAmount(senderTransaction.getAmount());
//			approvedTxn.setDescription(UpiConstants.RES_DESCRIPTION + " of Rs." + senderTransaction.getAmount());
//			approvedTxn.setService(senderTransaction.getService());
//			approvedTxn.setTransactionRefNo(System.currentTimeMillis() + "C");
//			approvedTxn.setDebit(false);
//			approvedTxn.setFavourite(false);
//			approvedTxn.setAuthReferenceNo(senderTransaction.getRetrivalReferenceNo());
//			approvedTxn.setCurrentBalance(senderCurrentBalance);
//			approvedTxn.setRetrivalReferenceNo(senderTransaction.getTransactionRefNo());
//			approvedTxn.setAccount(sender.getAccountDetail());
////			approvedTxn.setUpiId(senderTransaction.getUpiId());
//			approvedTxn.setStatus(Status.Success);
//			PQTransaction t = transactionRepository.save(approvedTxn);

			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			pqAccountDetailRepository.save(senderAccount);

			//			int i = userApi.updateBalance(senderCurrentBalance, sender);
			//			logger.error("Updated Load money " + i);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null,null);
			}
		}
	}

	@Override
	public void failedUPILoadMoney(String retrivalReferenceNo) {
		PQTransaction senderTransaction = getTransactionByRetrivalReferenceNo(retrivalReferenceNo);
		if ((senderTransaction.getStatus().equals(Status.Sent))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
		}
	}
	
	@Override
	public void successUPISdkLoadMoney(UpiMobileRedirectReq dto) {
		// TODO Auto-generated method stub
		PQTransaction senderTransaction = getTransactionByRefNo(dto.getTxnRefNo()+"C");
		if ((senderTransaction.getStatus().equals(Status.Sent))) {
//			PQTransaction approvedTxn = new PQTransaction();
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setRetrivalReferenceNo(dto.getRefId());
			senderTransaction.setUpiId(dto.getUpiId());
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			PQTransaction t = transactionRepository.save(senderTransaction);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null,null);
			}
		}
	}

	@Override
	public void failedUPISdkLoadMoney(UpiMobileRedirectReq dto) {
		PQTransaction senderTransaction = getTransactionByRefNo(dto.getTxnRefNo()+"C");
		if ((senderTransaction.getStatus().equals(Status.Sent))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setRetrivalReferenceNo(dto.getRefId());
			senderTransaction.setUpiId(dto.getUpiId());
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public void failedUPILoadMoneyWithTxnRef(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Sent))) {
//			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
		}
	}
	
	@Override
	public void updateUpiTrasaction(String transactionRefNo, String upiId, String referenceId) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		senderTransaction.setRetrivalReferenceNo(referenceId);
		senderTransaction.setUpiId(upiId);
		transactionRepository.save(senderTransaction);

	}

	@Override
	public void initiateBankTransfer(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction(); 
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(Double.parseDouble(String.format("%.2f",senderCurrentBalance)));
			pqAccountDetailRepository.save(senderAccount);
			//			userApi.updateBalance(senderCurrentBalance, sender);
		}
		User bankAccount = userApi.findByUserName(receiverUsername);
		PQTransaction bankTransaction = new PQTransaction();
		PQTransaction bankTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (bankTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			PQAccountDetail bankAccountDetail = bankAccount.getAccountDetail();
			double receiverCurrentBalance = bankAccount.getAccountDetail().getBalance();
			bankTransaction.setCurrentBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			bankTransaction.setAmount(receiverTransactionAmount);
			bankTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			bankTransaction.setDescription(description);
			bankTransaction.setService(service);
			bankTransaction.setAccount(bankAccount.getAccountDetail());
			bankTransaction.setTransactionRefNo(transactionRefNo + "C");
			bankTransaction.setDebit(false);
			bankTransaction.setStatus(Status.Initiated);
			transactionRepository.save(bankTransaction);
			bankAccountDetail.setBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionAccountDetail.getBalance();
			commissionTransaction.setCurrentBalance(BigDecimal.valueOf(commissionCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);

		}
	}

	@Override
	public void successBankTransfer(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			netTransactionAmount = senderTransaction.getAmount();
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				senderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f",senderTransaction.getCurrentBalance())));
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				//				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction, null,null);
				}
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				senderCommission = commissionApi.findCommissionByIdentifier(receiverTransaction.getCommissionIdentifier());
				netCommissionValue = commissionApi.getCommissionValue(senderCommission, receiverTransaction.getAmount());
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + receiverTransaction.getAmount();
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverTransaction.setAmount(receiverTransaction.getAmount());
				PQTransaction t = transactionRepository.save(receiverTransaction);
				PQAccountDetail bankAccount = receiver.getAccountDetail();
				bankAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(bankAccount);
				if (t != null) {
					/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null,null);*/
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				transactionRepository.save(commissionTransaction);
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
			}
		}
	}

	@Override
	public void failedBankTransfer(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				double	senderCurrentBalance = senderUserBalance + senderTransaction.getAmount();
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void initiatePromoCodeOld(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, String json) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();

		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setRequest(json);
			transactionRepository.save(senderTransaction);
			userApi.updateBalance(senderCurrentBalance, sender);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			transactionRepository.save(settlementTransaction);
			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
		}

		User instantpayAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (instantpayTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = instantpayAccount.getAccountDetail().getBalance();
			instantpayTransaction.setCurrentBalance(receiverCurrentBalance);
			instantpayTransaction.setAmount(receiverTransactionAmount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(instantpayAccount.getAccountDetail());
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Initiated);
			transactionRepository.save(instantpayTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successPromoCodeOld(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				transactionRepository.save(debitSettlementTransaction1);
				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}

	}

	@Override
	public void failedPromoCodeOld(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				transactionRepository.save(debitSettlementTransaction1);
				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void initiatePromoCode(double amount, String description, String transactionRefNo, PQService service,
			PromoCode promoCode, String senderUsername, String receiverUsername) {
		User sender = userApi.findByUserName(senderUsername);
		User promo = userApi.findByUserName(receiverUsername);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f",senderCurrentBalance)));
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			PQTransaction saved = transactionRepository.save(senderTransaction);
		}

		PQTransaction promoCodeTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo+"D");
		if(exists == null){
			PQAccountDetail promoCodeAccount = promo.getAccountDetail();
			double transactionAmount = amount;
			double senderUserBalance = promoCodeAccount.getBalance();
			double senderCurrentBalance = senderUserBalance;
			promoCodeTransaction.setAmount(transactionAmount);
			promoCodeTransaction.setDescription(description);
			promoCodeTransaction.setService(service);
			promoCodeTransaction.setTransactionRefNo(transactionRefNo + "D");
			promoCodeTransaction.setDebit(true);
			promoCodeTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f",senderCurrentBalance)));
			promoCodeTransaction.setAccount(promoCodeAccount);
			promoCodeTransaction.setStatus(Status.Initiated);
			PQTransaction saved = transactionRepository.save(promoCodeTransaction);
		}
	}

	@Override
	public void successPromoCode(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction promoTransaction = getTransactionByRefNo(transactionRefNo+ "D");
		double senderCurrentBalance = 0.0;
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = senderTransaction.getAccount();
			User sender = userApi.findByAccountDetail(accountDetail);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = accountDetail.getBalance();
			senderCurrentBalance = senderUserBalance;
			senderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f",senderCurrentBalance)));
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(Double.parseDouble(String.format("%.2f",senderCurrentBalance)));
			pqAccountDetailRepository.save(senderAccount);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.PROMO_SUCCESS, sender,
					senderTransaction, null);
			mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.PROMO_SUCCESS, sender,
					senderTransaction, null,null);
		}

		if ((promoTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = promoTransaction.getAccount();
			User promo = userApi.findByAccountDetail(accountDetail);
			PQAccountDetail senderAccount = promo.getAccountDetail();
			double senderUserBalance = accountDetail.getBalance();
			senderCurrentBalance = senderUserBalance;
			promoTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f",senderCurrentBalance)));
			promoTransaction.setStatus(Status.Success);
			promoTransaction.setLastModified(new Date());
			transactionRepository.save(promoTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
		}
	}

	@Override
	public void failedPromoCode(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = senderTransaction.getAccount();
			User sender = userApi.findByAccountDetail(accountDetail);
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED, sender,
					senderTransaction, null);
			mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
					senderTransaction, null,null);
		}
	}

	@Override
	public void initiateMobileBanking(double amount, String description, PQService service, String transactionRefNo, String receiverName) {
		User sender = userApi.findByUserName(receiverName);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(EBSConstants.DESCRIPTION + " of "+PayQwikUtil.CURRENCY+ amount);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setFavourite(false);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public void successMobileBanking(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			PQTransaction t = transactionRepository.save(senderTransaction);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			//			int i = userApi.updateBalance(senderCurrentBalance, sender);
			//			logger.error("Updated Load money " + i);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null,null);
			}
		}
	}

	@Override
	public void failedMobileBanking(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public PQTransaction saveOrUpdate(PQTransaction transaction) {
		return transactionRepository.save(transaction);
	}

	@Override
	public List<PQTransaction> findTransactionByAccount(Date from, Date to, PQAccountDetail account, double amount) {
		List<PQTransaction> trans = transactionRepository.findTransactionByAccount(from, to, account, amount);
		return trans;
	}

	@Override
	public List<PQTransaction> findTransactionByAccount(PQAccountDetail account) {
		List<PQTransaction> trans = transactionRepository.findTransactionByAccount(account);
		return trans;
	}

	@Override
	public Page<PQTransaction> findByAccount(Pageable page,PQAccountDetail account) {
		return transactionRepository.findAllByAccount(page,account);
	}

	@Override
	public Page<PQTransaction> findByService(Pageable page, PQService service) {
		return null;
	}

	@Override
	public List<PQTransaction> findTransactionByAccountAndAmount(Date from, Date to, PQAccountDetail account,double amount) {
		List<PQTransaction> trans = transactionRepository.findTransactionByAccountAndAmount(from, to, account, amount);
		return trans;
	}

	@Override
	public List<PQTransaction> getDailyDebitTransaction(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		List<PQTransaction> trans = transactionRepository.getDailyDebitTransaction(now.get(Calendar.YEAR),
				(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account, true);
		return trans;
	}

	@Override
	public List<PQTransaction> getMonthlyDebitTransaction(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		List<PQTransaction> trans = transactionRepository.getMonthlyDebitTransaction(now.get(Calendar.YEAR),
				(now.get(Calendar.MONTH) + 1), account);
		return trans;
	}

	@Override
	public List<PQTransaction> getDailyCreditAndDebitTransation(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		List<PQTransaction> trans = transactionRepository.getDailyCreditAndDebitTransation(now.get(Calendar.YEAR),
				(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
		return trans;
	}

	@Override
	public List<PQTransaction> getMonthlyCreditAndDebitTransation(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		List<PQTransaction> trans = transactionRepository.getMonthlyCreditAndDebitTransation(now.get(Calendar.YEAR),
				(now.get(Calendar.MONTH) + 1), account);
		return trans;
	}

	@Override
	public List<PQTransaction> getDailyCreditTransation(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		List<PQTransaction> trans = transactionRepository.getDailyCreditTransation(now.get(Calendar.YEAR),
				(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
		return trans;
	}

	@Override
	public List<PQTransaction> getMonthlyCreditTransation(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		List<PQTransaction> trans = transactionRepository.getMonthlyCreditTransation(now.get(Calendar.YEAR),
				(now.get(Calendar.MONTH) + 1), account);
		return trans;
	}

	@Override
	public double getDailyDebitTransactionTotalAmount(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getDailyDebitTransactionTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getDailyDebitTransactionTotalAmountBank(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getDailyDebitTransactionTotalAmountBank(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}
	@Override
	public double getMonthlyDebitTransactionTotalAmount(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getMonthlyDebitTransactionTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getMonthlyDebitTransactionTotalAmountBank(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getMonthlyDebitTransactionTotalAmountBank(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getDailyCreditAndDebitTransationTotalAmount(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getDailyCreditAndDebitTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getMonthlyCreditAndDebitTransationTotalAmount(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getMonthlyCreditAndDebitTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getDailyCreditTransationTotalAmount(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getDailyCreditTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), now.get(Calendar.DATE), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getMonthlyCreditTransationTotalAmount(PQAccountDetail account) {
		Calendar now = Calendar.getInstance();
		double amount = 0.0;
		try{
			amount = transactionRepository.getMonthlyCreditTransationTotalAmount(now.get(Calendar.YEAR),
					(now.get(Calendar.MONTH) + 1), account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		}
		catch(NullPointerException e){
			return amount;
		}
		return amount;
	}

	@Override
	public double getLastSuccessTransaction(PQAccountDetail account) {
		List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(account);
		PQTransaction lastTrans = null;
		if (lastTransList != null && !lastTransList.isEmpty()) {
			lastTrans = lastTransList.get(lastTransList.size() - 1);
			System.err.println("last Transaction Balance ::" + lastTrans.getCurrentBalance());
			return lastTrans.getCurrentBalance();
		}
		return 0.0;
	}

	@Override
	public void reverseTransaction(String transactionRefNo) {
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			if(commissionTransaction != null) {
				senderCommission = commissionApi.createByIdentifier(commissionTransaction.getCommissionIdentifier(), senderService);
			}else {
				senderCommission.setType("POST");
				senderCommission.setIdentifier(" ");
			}
			double netCommissionValue = senderCommission.getValue();
			//double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");
			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , Send Money Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				//				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}

		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , Send Money Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
				receiverAccount.setBalance(currentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS_SENDER	,receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
	}

	@Override
	public void processRefundBillPayment(String transactionRefNo) {
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");
			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setLastModified(new Date());
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setLastModified(new Date());
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
			}
		}

		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Send Money transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);
				refundCommissionTransaction.setLastModified(new Date());
				commissionTransaction.setStatus(Status.Refunded);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}
	}
	
	@Override
	public void processRefundTravelWallet(String transactionRefNo) {
		System.err.print(transactionRefNo);
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");

			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS_SENDER	,receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Send Money transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);

				commissionTransaction.setStatus(Status.Refunded);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}
	}
	
	
	@Override
	public void processRefundMerchantPayment(String transactionRefNo) {
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");

			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setLastModified(new Date());
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);*/
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Processing)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setLastModified(new Date());
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
				receiverAccount.setBalance(currentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS_SENDER	,receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Send Money transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);
                refundCommissionTransaction.setLastModified(new Date());
				commissionTransaction.setStatus(Status.Refunded);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}

		PQTransaction receiverCGSTTransaction = getTransactionByRefNo(transactionRefNo + "CGST");
		PQTransaction receiverCGSTRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "DCGST");
		if (receiverCGSTTransaction != null) {
			if ((receiverCGSTTransaction.getStatus().equals(Status.Success)) && (receiverCGSTRefundTransaction == null)) {
				User receiverSGST = userApi.findByAccountDetail(receiverCGSTTransaction.getAccount());
				PQAccountDetail receiverAccountSGST = receiverSGST.getAccountDetail();
				double currentBalance = receiverAccountSGST.getBalance() - receiverCGSTTransaction.getAmount();
				receiverCGSTRefundTransaction = new PQTransaction();
				receiverCGSTRefundTransaction.setAmount(receiverCGSTTransaction.getAmount());
				receiverCGSTRefundTransaction.setService(receiverCGSTTransaction.getService());
				receiverCGSTRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverCGSTRefundTransaction.setTransactionRefNo(newTransactionRefNo + "DCGST");
				receiverCGSTRefundTransaction.setDebit(true);
				receiverCGSTRefundTransaction.setDescription("Refund of Amount "+receiverCGSTTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverCGSTRefundTransaction.setAccount(receiverAccountSGST);
				receiverCGSTRefundTransaction.setCommissionIdentifier(receiverCGSTTransaction.getCommissionIdentifier());
				receiverCGSTRefundTransaction.setStatus(Status.Success);
				receiverCGSTRefundTransaction.setLastModified(new Date());
				receiverCGSTRefundTransaction.setCurrentBalance(currentBalance);
				receiverCGSTTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverCGSTTransaction);
				transactionRepository.save(receiverCGSTRefundTransaction);
			}
		}
		PQTransaction receiverSGSTTransaction = getTransactionByRefNo(transactionRefNo + "SGST");
		PQTransaction receiverSGSTRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "DSGST");
		if (receiverSGSTTransaction != null) {
			if ((receiverSGSTTransaction.getStatus().equals(Status.Success)) && (receiverSGSTRefundTransaction == null)) {
				User receiverSGST = userApi.findByAccountDetail(receiverSGSTTransaction.getAccount());
				PQAccountDetail receiverAccountSGST = receiverSGST.getAccountDetail();
				double currentBalance = receiverAccountSGST.getBalance() - receiverSGSTTransaction.getAmount();
				receiverSGSTRefundTransaction = new PQTransaction();
				receiverSGSTRefundTransaction.setAmount(receiverSGSTTransaction.getAmount());
				receiverSGSTRefundTransaction.setService(receiverSGSTTransaction.getService());
				receiverSGSTRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverSGSTRefundTransaction.setTransactionRefNo(newTransactionRefNo + "DCGST");
				receiverSGSTRefundTransaction.setDebit(true);
				receiverSGSTRefundTransaction.setDescription("Refund of Amount "+receiverSGSTTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverSGSTRefundTransaction.setAccount(receiverAccountSGST);
				receiverSGSTRefundTransaction.setCommissionIdentifier(receiverSGSTTransaction.getCommissionIdentifier());
				receiverSGSTRefundTransaction.setStatus(Status.Success);
				receiverSGSTRefundTransaction.setLastModified(new Date());
				receiverSGSTRefundTransaction.setCurrentBalance(currentBalance);
				receiverSGSTTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverSGSTTransaction);
				transactionRepository.save(receiverSGSTRefundTransaction);
			}
		}
		MerchantRefundRequest request=merchantRefundRequestRepository.findTrnasactionRefNo(transactionRefNo+"D");
		if(request!=null){
			if(request.getStatus().equals(Status.Processing)){
				request.setStatus(Status.Refunded);
				merchantRefundRequestRepository.save(request);
			}
		}
	}


	@Override
	public void processRefundLoadMoney(String transactionRefNo) {
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");
			if ((senderTransaction.getStatus().equals(Status.Initiated)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.LOADMONEY_REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setLastModified(new Date());
				senderRefundTransaction.setDescription("Credit  of Amount "+netTransactionAmount+" on behalf of Load Money Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
	}
	
	@Override
	public void processRefundUPILoadMoney(String transactionRefNo) {
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");
			if ((senderTransaction.getStatus().equals(Status.Failed)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.LOADMONEY_REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setLastModified(new Date());
				senderRefundTransaction.setDescription("Credit of Amount "+netTransactionAmount+" on behalf of UPI Load Money Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
	}

	public void successBillPaymentNew(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String senderUsername = "";
		String receiverUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.BILLPAYMENT_SUCCESS, sender, senderTransaction, receiverUsername);
					mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, receiverUsername,null);
				}
			}
		}

		PQTransaction settlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransaction != null) {
			User settlement = userApi.findByAccountDetail(settlementTransaction.getAccount());
			PQAccountDetail accountDetail = settlementTransaction.getAccount();
			double settlementTransactionAmount = settlementTransaction.getAmount();
			String settlementDescription = settlementTransaction.getDescription();
			double settlementCurrentBalance = settlementTransaction.getCurrentBalance();
			PQTransaction settlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				settlementTransaction1.setAmount(settlementTransactionAmount);
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setDescription(settlementDescription);
				settlementTransaction1.setService(senderService);
				settlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				settlementTransaction1.setDebit(false);
				settlementTransaction1.setAccount(accountDetail);
				settlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				settlementTransaction1.setStatus(Status.Success);
				transactionRepository.save(settlementTransaction1);
				userApi.updateBalance(settlementCurrentBalance, settlement);
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				userApi.updateBalance(receiverCurrentBalance, receiver);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction,null);
					mailSenderApi.sendTransactionMail("VPayqwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction,receiverUsername,null);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				transactionRepository.save(commissionTransaction);
				userApi.updateBalance(commissionCurrentBalance, commissionAccount);
			}
		}

	}

	@Override
	public void failedBillPaymentNew(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))/*||(senderTransaction.getStatus().equals(Status.Success))*/) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}

		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				transactionRepository.save(debitSettlementTransaction1);
				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	private long calculatePoints(double amount) {
		long points = 0;
		if (amount >= 100) {
			points = (long) amount / 100;
		}
		return points;

	}

	@Override
	public void initiateBusBooking(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, String json) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();

		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setRequest(json);
			// userApi.updateBalance(senderCurrentBalance, sender);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);
			User temp = userApi.findByUserName(senderUsername);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "D");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			// userApi.updateBalance(settlementCurrentBalance,
			// settlementAccount);
			transactionRepository.save(settlementTransaction);
		}

		User instantpayAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (instantpayTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = instantpayAccount.getAccountDetail().getBalance();

			instantpayTransaction.setCurrentBalance(receiverCurrentBalance);
			instantpayTransaction.setAmount(receiverTransactionAmount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(instantpayAccount.getAccountDetail());
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Initiated);
			transactionRepository.save(instantpayTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}


	@Override
	public void successTravelBus(String transactionRefNo) {

		String senderUsername = null;
		String receiverUsername = null;
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			/* netTransactionAmount = senderTransaction.getAmount(); */
			netTransactionAmount = 0.0;
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction, receiverUsername,null);
				}
			}
		}


		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverUsername = receiver.getUsername();
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				// userApi.updateBalance(receiverCurrentBalance, receiver);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, receiver,
							receiverTransaction, receiverUsername,null);
				}
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				transactionRepository.save(commissionTransaction);
				// userApi.updateBalance(commissionCurrentBalance,
				// commissionAccount);
			}
		}
	}

	@Override
	public List<PQTransaction> getAllDefaultTransactions() {
		return transactionRepository.getTotalDefaultTransactions(TransactionType.DEFAULT);
	}

	@Override
	public void initiateMBankTransfer(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername, String json) {

		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();

		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setRequest(json);
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			//			userApi.updateBalance(senderCurrentBalance, sender);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			PQAccountDetail settleAccount = settlementAccount.getAccountDetail();
			double settlementUserBalance = settleAccount.getBalance();
			double settlementCurrentBalance = 0;
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			transactionRepository.save(settlementTransaction);
			settleAccount.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settleAccount);
			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
		}

		User bankAccount = userApi.findByUserName(receiverUsername);
		PQTransaction bankTransaction = new PQTransaction();
		PQTransaction bankTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (bankTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			PQAccountDetail bankAccountDetail = bankAccount.getAccountDetail();
			double receiverCurrentBalance = bankAccount.getAccountDetail().getBalance();
			bankTransaction.setCurrentBalance(receiverCurrentBalance);
			bankTransaction.setAmount(receiverTransactionAmount);
			bankTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			bankTransaction.setDescription(description);
			bankTransaction.setService(service);
			bankTransaction.setAccount(bankAccount.getAccountDetail());
			bankTransaction.setTransactionRefNo(transactionRefNo + "C");
			bankTransaction.setDebit(false);
			bankTransaction.setStatus(Status.Initiated);
			transactionRepository.save(bankTransaction);
			receiverCurrentBalance = receiverCurrentBalance + receiverTransactionAmount;
			bankAccountDetail.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(bankAccountDetail);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionAccountDetail.getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void initiateABankTransfer(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername, String json) {

		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();

		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setRequest(json);
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			//			userApi.updateBalance(senderCurrentBalance, sender);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			PQAccountDetail settleAccount = settlementAccount.getAccountDetail();
			double settlementUserBalance = settleAccount.getBalance();
			double settlementCurrentBalance = 0;
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			transactionRepository.save(settlementTransaction);
			settleAccount.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settleAccount);
			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
		}

		User bankAccount = userApi.findByUserName(receiverUsername);
		PQTransaction bankTransaction = new PQTransaction();
		PQTransaction bankTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (bankTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			PQAccountDetail bankAccountDetail = bankAccount.getAccountDetail();
			double receiverCurrentBalance = bankAccount.getAccountDetail().getBalance();
			bankTransaction.setCurrentBalance(receiverCurrentBalance);
			bankTransaction.setAmount(receiverTransactionAmount);
			bankTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			bankTransaction.setDescription(description);
			bankTransaction.setService(service);
			bankTransaction.setAccount(bankAccount.getAccountDetail());
			bankTransaction.setTransactionRefNo(transactionRefNo + "C");
			bankTransaction.setDebit(false);
			bankTransaction.setStatus(Status.Initiated);
			transactionRepository.save(bankTransaction);
			receiverCurrentBalance = receiverCurrentBalance + receiverTransactionAmount;
			bankAccountDetail.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(bankAccountDetail);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionAccountDetail.getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}
	
	@Override
	public List<PQTransaction> getTotalTransactionsOfMerchant(String merchantEmail) {
		User user = userApi.findByUserName(merchantEmail);
		PQAccountDetail account = user.getAccountDetail();
		return transactionRepository.findAllTransactionByMerchant(account);
	}


	@Override
	public Page<PQTransaction> getTotalTransactionsOfMerchantService(String merchantEmail,PQService service,Pageable pageable) {
		return transactionRepository.findAllTransactionByMerchantService(service,pageable);
	}

	@Override
	public List<PQTransaction> getDailyTransactionByDateStatus(Date from,Date to,Status status,PQService service) {
		return (List<PQTransaction>) transactionRepository.findByTranRefNoAndStatus(from,to,status,service);
	}

	@Override
	public void initiateEventPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		// comment line for commission  transaction
		//		if (senderCommission.getType().equalsIgnoreCase("POST")) {
		//			netTransactionAmount = netTransactionAmount + netCommissionValue;
		//		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);

			//			int update = userApi.updateBalance(senderCurrentBalance, sender);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);

			//			System.err.println("*****rows updated*******" + update);
			User temp = userApi.findByUserName(senderUsername);

		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			//			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
			transactionRepository.save(settlementTransaction);
		}

		User meraEvent = userApi.findByUserName(receiverUsername);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}

			double receiverCurrentBalance = meraEvent.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(receiverCurrentBalance);
			eventTransaction.setAmount(receiverTransactionAmount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(meraEvent.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public ResponseDTO getBookingStatus(String sessionId, String transactionRefNo, String orderID, String accessToken) {
		ResponseDTO resp = new ResponseDTO();
		try {
			System.err.println("--------------- INSIDE BOOKING STATUS API ----------------------");
			org.codehaus.jettison.json.JSONObject payload = new org.codehaus.jettison.json.JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("transactionId", transactionRefNo);
			payload.put("orderId", orderID);
			payload.put("access_token", accessToken);
			System.out.println("payload : " +payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(MeraEventsUtil.URL+"offlineBooking");
			System.err.println(MeraEventsUtil.URL+"offlineBooking");
			ClientResponse response = webResource.accept("application/json").type("application/json").header("hash", payload.toString())
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setMessage("Ticket booking failure");
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					System.err.println("--------------OFFLINE STATUS CALL -------------------: " +jsonObj);
					boolean status = jsonObj.getBoolean("status");
					int code = jsonObj.getInt("statusCode");
					if(code == 200) {
						resp.setStatus(ResponseStatus.SUCCESS);
						resp.setMessage("Event booked");
					}
				} else {
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setMessage("Ticket booking was failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public void successEventPayment(String transactionRefNo) {
		System.out.println("---------------------------- INSIDE SUCCESS EVENT PAYMENT API -----------------------");
		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderMobileNumber = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			org.json.JSONObject json = null;
			String mobileNumber = null;
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS_MERAEVENT,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction," ",null);
				}
			}
		}

		PQTransaction settlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransaction != null) {
			User settlement = userApi.findByAccountDetail(settlementTransaction.getAccount());
			PQAccountDetail accountDetail = settlementTransaction.getAccount();
			double settlementTransactionAmount = settlementTransaction.getAmount();
			String settlementDescription = settlementTransaction.getDescription();
			double settlementCurrentBalance = settlementTransaction.getCurrentBalance();
			PQTransaction settlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail settlementAccountDetail = settlement.getAccountDetail();
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				settlementTransaction1.setAmount(settlementTransactionAmount);
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setDescription(settlementDescription);
				settlementTransaction1.setService(senderService);
				settlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				settlementTransaction1.setDebit(false);
				settlementTransaction1.setAccount(accountDetail);
				settlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				settlementTransaction1.setStatus(Status.Success);
				settlementAccountDetail.setBalance(settlementCurrentBalance);
				pqAccountDetailRepository.save(settlementAccountDetail);
				//				userApi.updateBalance(settlementCurrentBalance, settlement);
				transactionRepository.save(settlementTransaction1);

			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				//				receiverCurrentBalance = receiverCurrentBalance - netTransactionAmount;
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				//				userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);

				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, senderMobileNumber,null);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				//				userApi.updateBalance(commissionCurrentBalance, commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void failedEventPayment(String transactionRefNo) {
		System.err.println("---------------------------- INSIDE FAILURE EVENT PAYMENT API -----------------------");
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				//				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				//				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void initiateQwikrPayPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername,String orderId) {
		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = sender.getAccountDetail().getBalance();
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderTransaction.setRetrivalReferenceNo(orderId);

			//			int update = userApi.updateBalance(senderCurrentBalance, sender);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);

			//			System.err.println("*****rows updated*******" + update);
			User temp = userApi.findByUserName(senderUsername);

		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			//			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
			transactionRepository.save(settlementTransaction);
		}

		User qwikPay = userApi.findByUserName(StartupUtil.QWIKR_PAY);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = qwikPay.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(receiverCurrentBalance);
			eventTransaction.setAmount(receiverTransactionAmount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(qwikPay.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successQwikrPayPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			System.err.println("username::"+sender.getUsername());
			senderMobileNumber = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			org.json.JSONObject json = null;
			String mobileNumber = null;
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					/* smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.QWIK_PAY,
                            sender, senderTransaction, null);
                    mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.QWIK_PAY, sender,
                            senderTransaction," ",null);
					 */               
					}
			}


		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				//				userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);

				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, senderMobileNumber,null);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				//				userApi.updateBalance(commissionCurrentBalance, commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public void failedQwikrPayPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();

			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();

				System.err.println(senderCurrentBalance);
			
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}
	@Override
	public void initiateSendMoneyToPredictWinner(double amount, String description, String transactionRefNo, PQService service,
			PromoCode promoCode, String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = senderAccount.getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Initiated);
			senderAccount.setBalance(senderCurrentBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
			//			userApi.updateBalance(senderCurrentBalance, sender);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		PQAccountDetail settleAccount = settlementAccount.getAccountDetail();
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settleAccount.getBalance();
			double settlementCurrentBalance = 0;
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settleAccount);
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(settleAccount);
			transactionRepository.save(settlementTransaction);
			//			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
		}

		User instantpayAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		PQAccountDetail ipayAccount = instantpayAccount.getAccountDetail();
		if (instantpayTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = ipayAccount.getBalance();
			instantpayTransaction.setCurrentBalance(receiverCurrentBalance);
			instantpayTransaction.setAmount(receiverTransactionAmount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(ipayAccount);
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Initiated);
			transactionRepository.save(instantpayTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successSendMoneyToPredictWinner(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String senderUsername = "";
		String receiverUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_SENDER, sender, senderTransaction, receiverUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
							sender, senderTransaction, receiverUsername,null);
				}
			}
		}

		PQTransaction settlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransaction != null) {
			User settlement = userApi.findByAccountDetail(settlementTransaction.getAccount());
			PQAccountDetail accountDetail = settlementTransaction.getAccount();
			PQAccountDetail settlementAccountDetail = settlement.getAccountDetail();
			double settlementTransactionAmount = settlementTransaction.getAmount();
			String settlementDescription = settlementTransaction.getDescription();
			double settlementCurrentBalance = settlementTransaction.getCurrentBalance();
			PQTransaction settlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				settlementTransaction1.setAmount(settlementTransactionAmount);
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setDescription(settlementDescription);
				settlementTransaction1.setService(senderService);
				settlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				settlementTransaction1.setDebit(false);
				settlementTransaction1.setAccount(accountDetail);
				settlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				settlementTransaction1.setStatus(Status.Success);
				logger.error("Debit settlement 2 success ");
				settlementAccountDetail.setBalance(settlementCurrentBalance);
				pqAccountDetailRepository.save(settlementAccountDetail);
				transactionRepository.save(settlementTransaction1);
				// userApi.updateBalance(settlementCurrentBalance, settlement);
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				receiverAccount.setBalance(receiverCurrentBalance);
				PQAccountDetail updatedDetails = pqAccountDetailRepository.save(receiverAccount);
				//				userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					if (serviceCode.equalsIgnoreCase("SMU")) {
						smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendTransactionMail("VPayQwik Transaction",
								MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
								senderUsername,null);
					} else {
						smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendTransactionMail("VPayQwik Transaction",
								MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
								senderUsername,null);
					}
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				transactionRepository.save(commissionTransaction);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				PQAccountDetail commissionUpdatedDetails = pqAccountDetailRepository.save(commissionAccountDetail);
				//				userApi.updateBalance(commissionCurrentBalance, commissionAccount);
			}
		}
	}
	@Override
	public void initiateIpltransaction(double amount, String description, String transactionRefNo, PQService service,
			String senderUsername, String receiverUsername) {
		User sender = userApi.findByUserName(receiverUsername);
		User ipl = userApi.findByUserName(senderUsername);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			PQTransaction saved = transactionRepository.save(senderTransaction);
		}

		PQTransaction promoCodeTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo+"D");
		if(exists == null){
			PQAccountDetail promoCodeAccount = ipl.getAccountDetail();
			double transactionAmount = amount;
			double senderUserBalance = promoCodeAccount.getBalance();
			double senderCurrentBalance = senderUserBalance;
			promoCodeTransaction.setAmount(transactionAmount);
			promoCodeTransaction.setDescription(description);
			promoCodeTransaction.setService(service);
			promoCodeTransaction.setTransactionRefNo(transactionRefNo + "D");
			promoCodeTransaction.setDebit(true);
			promoCodeTransaction.setCurrentBalance(senderCurrentBalance);
			promoCodeTransaction.setAccount(promoCodeAccount);
			promoCodeTransaction.setStatus(Status.Initiated);
			PQTransaction saved = transactionRepository.save(promoCodeTransaction);
		}
	}
	@Override
	public void successPredictAndWin(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction promoTransaction = getTransactionByRefNo(transactionRefNo+ "D");
		double senderCurrentBalance = 0.0;
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = senderTransaction.getAccount();
			User sender = userApi.findByAccountDetail(accountDetail);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = accountDetail.getBalance();
			senderCurrentBalance = senderUserBalance;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.PREDICT_SUCCESS, sender,
					senderTransaction, null);
			//		mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.PREDICT_SUCCESS, sender,
			//				senderTransaction, null,null);
		}

		if ((promoTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = promoTransaction.getAccount();
			User promo = userApi.findByAccountDetail(accountDetail);
			PQAccountDetail senderAccount = promo.getAccountDetail();
			double senderUserBalance = accountDetail.getBalance();
			senderCurrentBalance = senderUserBalance;
			promoTransaction.setCurrentBalance(senderCurrentBalance);
			promoTransaction.setStatus(Status.Success);
			transactionRepository.save(promoTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
		}
	}

	@Override
	public void initiateAgentSendMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {
		// TODO Auto-generated method stub
		User sender = userApi.findByUserName(senderUsername);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = senderAccount.getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Initiated);
			senderAccount.setBalance(senderCurrentBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
			// userApi.updateBalance(senderCurrentBalance, sender);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		PQAccountDetail settleAccount = settlementAccount.getAccountDetail();
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settleAccount.getBalance();
			double settlementCurrentBalance = 0;
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settleAccount);
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(settleAccount);
			transactionRepository.save(settlementTransaction);
			// userApi.updateBalance(settlementCurrentBalance,
			// settlementAccount);
		}

		User instantpayAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		PQAccountDetail ipayAccount = instantpayAccount.getAccountDetail();
		if (instantpayTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = ipayAccount.getBalance();
			instantpayTransaction.setCurrentBalance(receiverCurrentBalance);
			instantpayTransaction.setAmount(receiverTransactionAmount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(ipayAccount);
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Initiated);
			transactionRepository.save(instantpayTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successAgentSendMoney(String transactionRefNo) {
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String senderUsername = "";
		String receiverUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_SENDER, sender, senderTransaction, receiverUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
							sender, senderTransaction, receiverUsername, null);
				}
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				receiverAccount.setBalance(receiverCurrentBalance);
				PQAccountDetail updatedDetails = pqAccountDetailRepository.save(receiverAccount);
				// userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					if (serviceCode.equalsIgnoreCase("SMU")) {
						smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendTransactionMail("VPayQwik Transaction",
								MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiver, receiverTransaction,
								senderUsername, null);
					} else {
						smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendTransactionMail("VPayQwik Transaction",
								MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiver, receiverTransaction,
								senderUsername, null);
					}
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				transactionRepository.save(commissionTransaction);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				PQAccountDetail commissionUpdatedDetails = pqAccountDetailRepository.save(commissionAccountDetail);
				// userApi.updateBalance(commissionCurrentBalance,
				// commissionAccount);
			}
		}
	}

	@Override
	public void failedAgentSendMoney(String transactionRefNo) {
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
				// userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null, null);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				accountDetail.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(debitSettlementTransaction1);
				// userApi.updateBalance(debitSettlementCurrentBalance,
				// debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null, null);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}

	}


	@Override
	public ResponseDTO processSendMoneyRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		if(dto.getTransactionRefNo() != null) {
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(dto.getTransactionRefNo());
			if(transaction != null){
				if(transaction.getStatus().equals(Status.Success)) {
					String serviceCode = transaction.getService().getCode();
					if (serviceCode.equalsIgnoreCase("SMU") || serviceCode.equalsIgnoreCase("SMR")) {
						String newRefNo = dto.getTransactionRefNo().substring(0, dto.getTransactionRefNo().length() - 1);
						reverseTransaction(newRefNo);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Refunded Successfully");
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Not a send money transaction");
					}
				} else{
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Transaction was not SUCCESS");
				}
			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not available");
			}
		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Enter Transaction Ref No.");
		}
		return result;
	}

	@Override
	public ResponseDTO processLoadMoneyRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
			if(transaction != null){
				String serviceCode = transaction.getService().getCode();
				if(serviceCode.equalsIgnoreCase("LMC")) {
					if(transaction.getStatus().equals(Status.Initiated)){
						String transactionId = transactionRef.substring(0, transactionRef.length() - 1);
						//TODO check the transaction status from EBS
						EBSStatusResponseDTO status = checkLoadMoneyStatus(transactionId,serviceCode);
						if(status.isSuccess()) {
							// TODO credit into user wallet account
							processRefundLoadMoney(transactionId);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Credited Successfully");
						}else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(status.getMessage()+" "+status.getErrorMessage());
						}
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction already refunded");
					}
				}else if(serviceCode.equalsIgnoreCase("LMB")) {
					if(transaction.getStatus().equals(Status.Initiated)){
						String transactionId = transactionRef.substring(0, transactionRef.length() - 1);
//						VNetStatusResponse status = vnetVarification(transactionId,serviceCode,transaction.getAmount());
//						if(status.isSuccess()) {
							// TODO credit into user wallet account
							processRefundLoadMoney(transactionId);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Credited Successfully");
						/*}else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(status.getMessage());
						}*/
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction already refunded");
					}
				}else if(serviceCode.equalsIgnoreCase("LMU")) {
					if(transaction.getStatus().equals(Status.Failed)){
						String transactionId = transactionRef.substring(0, transactionRef.length() - 1);
						UpiRequest req = new UpiRequest();
						req.setMsgId(transaction.getUpiId());
						req.setMerchantVpa(UpiConstants.LIVE_MERCHANT_VPA);
						req.setSdk(transaction.isSdkRequest());
						UpiResponse response = userApi.converUPIstatusRequest(req);
						if(response.getCode().equals(ResponseStatus.SUCCESS.getValue())){
							MerchantTxnStatusResponse statusResponse = userApi.upiStatus(req);
							if(statusResponse.getCode().equals(ResponseStatus.SUCCESS.getValue())){
								if(statusResponse.getUpiStatus().equalsIgnoreCase("SUCCESS")){
									processRefundUPILoadMoney(transactionId);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Credited Successfully");
								}else{
									// TODO UPI Status not success
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Transaction is already processed. FSS status is " + statusResponse.getUpiStatus());
								}
							}else{
								// TODO Failed/Not Transaction fount at FSS
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(statusResponse.getMessage());
							}
						}else{
							// TODO Failed/Bad Response
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("No status is found from FSS");
						}
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction  already refunded");
					}
				}  else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a LOAD MONEY transaction");
				}

			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction doesn't exists");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}
		return result;
	}

	@Override
	public ResponseDTO processBillPaymentRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
			if(transaction != null){
				String serviceType = transaction.getService().getServiceType().getName();
				if(serviceType.equalsIgnoreCase("Bill Payment")) {
					if(transaction.getStatus().equals(Status.Success)){
						// TODO credit into user wallet account
						String transactionId = transactionRef.substring(0,transactionRef.length()-1);
						processRefundBillPayment(transactionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Credited Successfully");
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction was "+transaction.getStatus());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a TOPUP/BILL PAYMENT transaction");
				}

			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not exists");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}

		return result;
	}
	
	@Override
	public ResponseDTO processTravelEBSRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
			if(transaction != null){
				String serviceCode = transaction.getService().getCode();
				if(serviceCode.equalsIgnoreCase("EMTF")) {
					if(transaction.getStatus().equals(Status.Processing)){
						String transactionId = transactionRef.substring(0, transactionRef.length() - 1);
						//TODO check the transaction status from EBS
						EBSStatusResponseDTO status = checkLoadMoneyStatus(transactionId,serviceCode);
						System.err.println("dto"+status.toString());
						if(status.isSuccess()) {
							// TODO credit into user wallet account
							processRefundLoadMoney(transactionId);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Credited Successfully");
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction was "+transaction.getStatus());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a TOPUP/BILL PAYMENT transaction");
				 }
				}

			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not exists");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}

		return result;
	}
	
	@Override
	public ResponseDTO processTravelWalletRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
			if(transaction != null){
				String serviceType = transaction.getService().getServiceType().getName();
				if(serviceType.equalsIgnoreCase("Flight_VPQ payment in VPayQwik")) {
					if(transaction.getStatus().equals(Status.Success)){
						// TODO credit into user wallet account
						String transactionId = transactionRef.substring(0,transactionRef.length()-1);
						processRefundTravelWallet(transactionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Credited Successfully");
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction was "+transaction.getStatus());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a TOPUP/BILL PAYMENT transaction");
				}

			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not exists");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}

		return result;
	}
	
	
	@Override
	public ResponseDTO processMerchantPaymentRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
			if(transaction != null){
				String serviceType = transaction.getService().getServiceType().getName();
				if(serviceType.equalsIgnoreCase("Merchant Payment")) {
					if(transaction.getStatus().equals(Status.Success)){
						// TODO credit into user wallet account
						String transactionId = transactionRef.substring(0,transactionRef.length()-1);
						processRefundMerchantPayment(transactionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Credited Successfully");
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction was "+transaction.getStatus());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a Merchant PAYMENT transaction");
				}

			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not exists");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}

		return result;
	}

	@Override
	public List<PQTransaction> getTotalinitateAgentTransactions(Pageable page, String servicecode) {
		// TODO Auto-generated method stub
		PQService service=transactionRepository.findBypqservicetype(servicecode);

		return transactionRepository.getAgentRequestLoadMoney(service);
	}

	@Override
	public void initiateNikkiChatPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String username, String authReferenceNo) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission receiverCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(receiverCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (receiverCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount - netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderCurrentBalance = senderUserBalance - amount;
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setAuthReferenceNo(authReferenceNo);
			senderTransaction.setStatus(Status.Initiated);
			// senderTransaction.setRequest(json);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			transactionRepository.save(settlementTransaction);
		}

		User visa = userApi.findByUserName(username);
		PQTransaction visaTransaction = new PQTransaction();
		PQTransaction visaTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (visaTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = visa.getAccountDetail().getBalance();
			visaTransaction.setCurrentBalance(receiverCurrentBalance);
			visaTransaction.setAmount(receiverTransactionAmount);
			visaTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			visaTransaction.setDescription(description);
			visaTransaction.setService(service);
			visaTransaction.setAccount(visa.getAccountDetail());
			visaTransaction.setTransactionRefNo(transactionRefNo + "C");
			visaTransaction.setDebit(false);
			visaTransaction.setStatus(Status.Initiated);
			transactionRepository.save(visaTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successNikkiChatPayment(String transactionRefNo ,String retrivalReferenceNumber) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String receiverUsername = "";
		String senderUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			String receiverName = receiver.getUserDetail().getFirstName();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			System.out.println("Sender commision && netTransactionAmount :: " + senderCommission + netTransactionAmount);
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setRetrivalReferenceNo(retrivalReferenceNumber);
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.NIKKI_CHAT_SUCCESS,
							sender, senderTransaction, receiverName);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.NIKKI_CHAT_SUCCESS, sender,
							senderTransaction, receiverName, MailConstants.CC_MAIL);
				}
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User visa = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				PQAccountDetail receiverAccount = visa.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					// smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
					// SMSTemplate.MERCHANT_RECEIVER,
					// visa, receiverTransaction,senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, visa,
							receiverTransaction, senderUsername, MailConstants.CC_MAIL);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				// userApi.updateBalance(commissionCurrentBalance,
				// commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}

	}

	@Override
	public void failedNikkiChatPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + senderTransaction.getAmount();
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				PQAccountDetail senderAccount = senderTransaction.getAccount();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				// userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null, MailConstants.CC_MAIL);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				accountDetail.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				// userApi.updateBalance(debitSettlementCurrentBalance,
				// debitSettelment);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				// smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
				// SMSTemplate.TRANSACTION_FAILED,
				// receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null, MailConstants.CC_MAIL);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	//	@Override
	//	public Date getLastTranasactionTimeByStatus(String bookingId, Status status) {
	//		 TODO Auto-generated method stub
	//		retur\n null;
	//	}



	@Override
	public void initiatePayloPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String username, String authReferenceNo) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission receiverCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(receiverCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (receiverCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount - netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderCurrentBalance = senderUserBalance - amount;
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setAuthReferenceNo(authReferenceNo);
			senderTransaction.setStatus(Status.Initiated);
			// senderTransaction.setRequest(json);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			transactionRepository.save(settlementTransaction);
		}

		User visa = userApi.findByUserName(username);
		PQTransaction visaTransaction = new PQTransaction();
		PQTransaction visaTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (visaTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = visa.getAccountDetail().getBalance();
			visaTransaction.setCurrentBalance(receiverCurrentBalance);
			visaTransaction.setAmount(receiverTransactionAmount);
			visaTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			visaTransaction.setDescription(description);
			visaTransaction.setService(service);
			visaTransaction.setAccount(visa.getAccountDetail());
			visaTransaction.setTransactionRefNo(transactionRefNo + "C");
			visaTransaction.setDebit(false);
			visaTransaction.setStatus(Status.Initiated);
			transactionRepository.save(visaTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}




	@Override
	public void successPayloPayment(String transactionRefNo, String retrivalReferenceNumber) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String receiverUsername = "";
		String senderUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			String receiverName = receiver.getUserDetail().getFirstName();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			System.out
			.println("Sender commision && netTransactionAmount :: " + senderCommission + netTransactionAmount);
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setRetrivalReferenceNo(retrivalReferenceNumber);
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setLastModified(new Date());
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.NIKKI_CHAT_SUCCESS,
							sender, senderTransaction, receiverName);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.NIKKI_CHAT_SUCCESS, sender,
							senderTransaction, receiverName, MailConstants.CC_MAIL);
				}
			}
		}

		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User visa = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setLastModified(new Date());
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				PQAccountDetail receiverAccount = visa.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					// smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
					// SMSTemplate.MERCHANT_RECEIVER,
					// visa, receiverTransaction,senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, visa,
							receiverTransaction, senderUsername, MailConstants.CC_MAIL);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setLastModified(new Date());
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				// userApi.updateBalance(commissionCurrentBalance,
				// commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}

	}

	@Override
	public void failedPayloPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + senderTransaction.getAmount();
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				transactionRepository.save(senderTransaction);
				PQAccountDetail senderAccount = senderTransaction.getAccount();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				// userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null, MailConstants.CC_MAIL);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				accountDetail.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				// userApi.updateBalance(debitSettlementCurrentBalance,
				// debitSettelment);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				// smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
				// SMSTemplate.TRANSACTION_FAILED,
				// receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null, MailConstants.CC_MAIL);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}


	@Override
	public double getTotalTransactionsOfAgent(String username) {
		// TODO Auto-generated method stub
		User user = userApi.findByUserName(username);
		PQAccountDetail account = user.getAccountDetail();
		double amount = 0.0;
		try {
			amount = transactionRepository.getAllTransactionDebitByAgent(account);
			String v = String.format("%.2f", amount);
			amount = Double.parseDouble(v);
		} catch (NullPointerException e) {

			return amount;
		}
		return amount;
	}


	///                       for ADLABS                             /////////



	@Override
	public void initiateAdlabsPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			System.err.println("senderCurrentBalance current balnce after ticket amount deduction "+senderCurrentBalance);
			String v = String.format("%.2f", senderCurrentBalance);

			senderCurrentBalance = Double.parseDouble(v);
			System.err.println("senderCurrentBalance current balnce after ticket amount deduction in right format "+senderCurrentBalance);

			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);

			//			int update = userApi.updateBalance(senderCurrentBalance, sender);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);

			//			System.err.println("*****rows updated*******" + update);
			User temp = userApi.findByUserName(senderUsername);

		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {

			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			//			userApi.updateBalance(settlementCurrentBalance, settlementAccount);
			transactionRepository.save(settlementTransaction);
			System.err.println("Settlement executed");
		}

		User Gci = userApi.findByUserName(receiverUsername);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = Gci.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(receiverCurrentBalance);
			eventTransaction.setAmount(receiverTransactionAmount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(Gci.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
			System.err.println("Receiver intiated executed");
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
			System.err.println("Commission Initiated");
		}
	}
	@Override
	public void successAdlabsPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		String senderUsername = null;
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderMobileNumber = sender.getUsername();
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);

			System.err.println(" Net Commision :::::::::::::::::::::"+netCommissionValue);
			org.json.JSONObject json = null;
			String mobileNumber = null;
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction," ",null);
				}
			}
		}


		PQTransaction settlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransaction != null) {
			User settlement = userApi.findByAccountDetail(settlementTransaction.getAccount());
			PQAccountDetail accountDetail = settlementTransaction.getAccount();
			double settlementTransactionAmount = settlementTransaction.getAmount();
			String settlementDescription = settlementTransaction.getDescription();
			double settlementCurrentBalance = settlementTransaction.getCurrentBalance();
			PQTransaction settlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				settlementTransaction1.setAmount(settlementTransactionAmount);
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setDescription(settlementDescription);
				settlementTransaction1.setService(senderService);
				settlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				settlementTransaction1.setDebit(false);
				settlementTransaction1.setAccount(accountDetail);
				settlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				settlementTransaction1.setStatus(Status.Success);
				accountDetail.setBalance(settlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				// userApi.updateBalance(settlementCurrentBalance, settlement);
				transactionRepository.save(settlementTransaction1);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User visa = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				PQAccountDetail receiverAccount = visa.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					// smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
					// SMSTemplate.MERCHANT_RECEIVER,
					// visa, receiverTransaction,senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, visa,
							receiverTransaction, senderUsername, MailConstants.CC_MAIL);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				// userApi.updateBalance(commissionCurrentBalance,
				// commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}


	}

	@Override
	public void failedAdlabsPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();

			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();

				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);

				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}



	@Override
	public  PQTransaction initiateImagicaPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {
		
		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);
		}
		User imagica = userApi.findByUserName(receiverUsername);
		PQTransaction orderTransaction = new PQTransaction();
		PQTransaction orderTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (orderTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = imagica.getAccountDetail().getBalance();
			orderTransaction.setCurrentBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			orderTransaction.setAmount(receiverTransactionAmount);
			orderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			orderTransaction.setDescription(description);
			orderTransaction.setService(service);
			orderTransaction.setAccount(imagica.getAccountDetail());
			orderTransaction.setTransactionRefNo(transactionRefNo + "C");
			orderTransaction.setDebit(false);
			orderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(orderTransaction);
		}
		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(BigDecimal.valueOf(commissionCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		return senderTransaction;
	}

	@Override
	public PQTransaction successImagicaPayment(String transactionRefNo) {
		String senderUsername = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			PQCommission senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction," ",null);
				}
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User imagica = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				PQAccountDetail receiverAccount = imagica.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, imagica,
							receiverTransaction, senderUsername, MailConstants.CC_MAIL);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
		return senderTransaction;

	}

	@Override
	public void failedImagicaPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = senderAccount.getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Reversed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Reversed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getTotalTransactionsByServices(Date from,Date to){
		List<Object[]> listAnalytics=transactionRepository.getAllTransactionsByService(from, to);
		return listAnalytics;
	}


	@Override
	public Page<TPTransaction> transactionListByMerchantPageFiltered(Pageable page,User merchant,Date from,Date to) {
		return tPTransactionRepository.findMerchantTransactionsFiltered(page,merchant,from,to);
	}

	@Override
	public Page<TPTransaction> transactionListByMerchantPage(Pageable page,User merchant) {
		return tPTransactionRepository.findMerchantTransactions(page,merchant);
	}

	@Override
	public List<TPTransaction> transactionListMerchantPage(User merchant) {
		return tPTransactionRepository.findTransactions(merchant);
	}
	@Override
	public List<TPTransaction> transactionListByMerchant(User merchant) {
		return tPTransactionRepository.findMerchantTransactionsByOrderId(merchant);
	}

	@Override
	public List<Object[]> getTotalTransactionsByServicesCredit(Date from,Date to){
		List<Object[]> listAnalytics=transactionRepository.getCreditTransactions(from, to);
		return listAnalytics;
	}

	@Override
	public void initiateGiftCartPayment(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername,PQCommission senderCommission,double netCommissionValue) {
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);

		}

		User Gci = userApi.findByUserName(StartupUtil.Giftcart);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = Gci.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(receiverCurrentBalance);
			eventTransaction.setAmount(amount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(Gci.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successGiftCartPayment(String transactionRefNo,String receiptNo,PQCommission senderCommission,double netCommissionValue,GiftCardTransactionResponse response,User u,ProcessGiftCardDTO dto) {
		// TODO Auto-generated method stub
		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderMobileNumber = sender.getUsername();
			senderService = senderTransaction.getService();
			netTransactionAmount = senderTransaction.getAmount();
			org.json.JSONObject json = null;
			String mobileNumber = null;
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setRetrivalReferenceNo(receiptNo);
				PQTransaction t= transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendgciMail("VPayQwik Gift Card", MailTemplate.GCI_SENDER,dto, response, u);
					smsSenderApi.sendUserGCISMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.GCI_SMS,response, u,  dto);
					mailSenderApi.sendTransactionMail("VPayQwik GiftCard Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction," ",null);
				}
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				//					userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);

				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, senderMobileNumber,null);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				//					userApi.updateBalance(commissionCurrentBalance, commissionAccount);
				transactionRepository.save(commissionTransaction);
			}

		}
	}

	@Override
	public void failedGiftCartPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}


	@Override
	public  PQTransaction initiateYuppTvPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);
		}

		User settlementAccount = userApi.findByUserName("settlement@vpayqwik.com");
		PQTransaction settlementTransaction = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransactionExists == null) {
			double settlementUserBalance = settlementAccount.getAccountDetail().getBalance();
			double settlementCurrentBalance = 0;
			PQAccountDetail settlementAccountDetail = settlementAccount.getAccountDetail();
			settlementCurrentBalance = settlementUserBalance + netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			settlementTransaction.setAmount(netTransactionAmount);
			settlementTransaction.setDescription(description);
			settlementTransaction.setService(service);
			settlementTransaction.setTransactionRefNo(transactionRefNo + "CS");
			settlementTransaction.setDebit(false);
			settlementTransaction.setCurrentBalance(settlementCurrentBalance);
			settlementTransaction.setAccount(settlementAccount.getAccountDetail());
			settlementTransaction.setTransactionType(TransactionType.SETTLEMENT);
			settlementTransaction.setStatus(Status.Success);
			settlementAccountDetail.setBalance(settlementCurrentBalance);
			pqAccountDetailRepository.save(settlementAccountDetail);
			transactionRepository.save(settlementTransaction);
		}

		User imagica = userApi.findByUserName(receiverUsername);
		PQTransaction orderTransaction = new PQTransaction();
		PQTransaction orderTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (orderTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = imagica.getAccountDetail().getBalance();
			orderTransaction.setCurrentBalance(receiverCurrentBalance);
			orderTransaction.setAmount(receiverTransactionAmount);
			orderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			orderTransaction.setDescription(description);
			orderTransaction.setService(service);
			orderTransaction.setAccount(imagica.getAccountDetail());
			orderTransaction.setTransactionRefNo(transactionRefNo + "C");
			orderTransaction.setDebit(false);
			orderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(orderTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		return senderTransaction;
	}

	@Override
	public PQTransaction successYuppTVPayment(String transactionRefNo) {
		String senderUsername = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			PQCommission senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction," ",null);
				}
			}
		}


		PQTransaction settlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (settlementTransaction != null) {
			User settlement = userApi.findByAccountDetail(settlementTransaction.getAccount());
			PQAccountDetail accountDetail = settlementTransaction.getAccount();
			double settlementTransactionAmount = settlementTransaction.getAmount();
			String settlementDescription = settlementTransaction.getDescription();
			double settlementCurrentBalance = settlementTransaction.getCurrentBalance();
			PQTransaction settlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setAmount(settlementTransactionAmount);
				settlementTransaction1.setCurrentBalance(settlementCurrentBalance);
				settlementTransaction1.setDescription(settlementDescription);
				settlementTransaction1.setService(senderService);
				settlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				settlementTransaction1.setDebit(false);
				settlementTransaction1.setAccount(accountDetail);
				settlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				settlementTransaction1.setStatus(Status.Success);
				accountDetail.setBalance(settlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(settlementTransaction1);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User imagica = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				PQAccountDetail receiverAccount = imagica.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, imagica,
							receiverTransaction, senderUsername, MailConstants.CC_MAIL);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
		return senderTransaction;

	}

	@Override
	public void failedYuppTVPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = senderAccount.getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Reversed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Reversed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	@Override
	public long getDailyTransactionCountBetweeen(Date from, Date to) {
		return transactionRepository.getDailyTransactionCountBetween(from, to);
	}

	@Override
	public long getDailyTransactionCountBetweenForAccount(Date from, Date to,PQAccountDetail account) {
		return transactionRepository.getDailyTransactionCountBetweenForAccount(from, to,account);
	}

	//flight initiate for Wallet

	@Override
	public  PQTransaction initiateFlightPayment(double netTransactionAmount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername,String convenienceFee,String baseFare) {
		 double baseFareAmount=Double.parseDouble(baseFare);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, baseFareAmount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, baseFareAmount);
		/*convience fees fixed 200*/
		netCommissionValue= netCommissionValue + 200;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(Double.parseDouble(String.format("%.2f", netTransactionAmount)));
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", senderCurrentBalance)));
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);
		}
		User flight = userApi.findByUserName(receiverUsername);
		PQTransaction orderTransaction = new PQTransaction();
		PQTransaction orderTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (orderTransactionExists == null) {
			double receiverTransactionAmount = baseFareAmount;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = baseFareAmount - netCommissionValue;
			}
			double receiverCurrentBalance = flight.getAccountDetail().getBalance();
			orderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", receiverCurrentBalance)));
			orderTransaction.setAmount(Double.parseDouble(String.format("%.2f", receiverTransactionAmount)));
			orderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			orderTransaction.setDescription(description);
			orderTransaction.setService(service);
			orderTransaction.setAccount(flight.getAccountDetail());
			orderTransaction.setTransactionRefNo(transactionRefNo + "C");
			orderTransaction.setDebit(false);
			orderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(orderTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", commissionCurrentBalance)));
			commissionTransaction.setAmount(Double.parseDouble(String.format("%.2f", netCommissionValue)));
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		return senderTransaction;
	}

	@Override
	public PQTransaction successFlightPayment(String transactionRefNo) {
		String senderUsername = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction," ",null);*/
				}
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User flight = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", receiverCurrentBalance)));
				PQAccountDetail receiverAccount = flight.getAccountDetail();
				receiverAccount.setBalance(Double.parseDouble(String.format("%.2f", receiverCurrentBalance)));
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				/*if (t != null) {
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, flight,
							receiverTransaction, senderUsername, MailConstants.CC_MAIL);
				}*/
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + commissionTransaction.getAmount();
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", commissionCurrentBalance)));
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(Double.parseDouble(String.format("%.2f", commissionCurrentBalance)));
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
		return senderTransaction;

	}

	@Override
	public void failedFlightPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = senderAccount.getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
				/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);*/
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Reversed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Reversed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}


	//refer n earn


	@Override
	public void initiatiateRefernEarn(double amount, String description, String transactionRefNo, PQService service,
			String senderUsername, String receiverUsername) {
		User sender = userApi.findByUserName(senderUsername);
		User promo = userApi.findByUserName(receiverUsername);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			PQTransaction saved = transactionRepository.save(senderTransaction);
		}

		PQTransaction promoCodeTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo+"D");
		if(exists == null){
			PQAccountDetail promoCodeAccount = promo.getAccountDetail();
			double transactionAmount = amount;
			double senderUserBalance = promoCodeAccount.getBalance();
			double senderCurrentBalance = senderUserBalance;
			promoCodeTransaction.setAmount(transactionAmount);
			promoCodeTransaction.setDescription(description);
			promoCodeTransaction.setService(service);
			promoCodeTransaction.setTransactionRefNo(transactionRefNo + "D");
			promoCodeTransaction.setDebit(true);
			promoCodeTransaction.setCurrentBalance(senderCurrentBalance);
			promoCodeTransaction.setAccount(promoCodeAccount);
			promoCodeTransaction.setStatus(Status.Initiated);
			PQTransaction saved = transactionRepository.save(promoCodeTransaction);
		}
	}

	@Override
	public void successRefernEarn(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction promoTransaction = getTransactionByRefNo(transactionRefNo+ "D");
		double senderCurrentBalance = 0.0;
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = senderTransaction.getAccount();
			User sender = userApi.findByAccountDetail(accountDetail);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = accountDetail.getBalance()+senderTransaction.getAmount();
			senderCurrentBalance = senderUserBalance;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			/* smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.PROMO_SUCCESS, sender,
		                    senderTransaction, null);
		            mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.PROMO_SUCCESS, sender,
		                    senderTransaction, null,null);*/
		}

		if ((promoTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = promoTransaction.getAccount();
			User promo = userApi.findByAccountDetail(accountDetail);
			PQAccountDetail senderAccount = promo.getAccountDetail();
			double senderUserBalance = accountDetail.getBalance()-promoTransaction.getAmount();
			senderCurrentBalance = senderUserBalance;
			promoTransaction.setCurrentBalance(senderCurrentBalance);
			promoTransaction.setStatus(Status.Success);
			transactionRepository.save(promoTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
		}
	}

	@Override
	public void failedRefernEarn(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			PQAccountDetail accountDetail = senderTransaction.getAccount();
			User sender = userApi.findByAccountDetail(accountDetail);
			senderTransaction.setStatus(Status.Failed);
			transactionRepository.save(senderTransaction);
			/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED, sender,
		                    senderTransaction, null);
		            mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
		                    senderTransaction, null,null);*/

		}
	}

	@Override
	public UserDetailDTO getUserDataForGenericValidation(String username,int currentDay,int currentMonth,int currentYear) {
		// TODO Auto-generated method stub
		return transactionRepository.getUserDataForValidation(username,currentDay,currentMonth,currentYear);
	}


	@Override
	public UserDetailDTO getUserDataForP2PValidation(String username,int currentDay,int currentMonth,int currentYear) {
		// TODO Auto-generated method stub
		return transactionRepository.getUserDataForP2PValidation(username,currentDay,currentMonth,currentYear);
	}



	@Override
	public void sendMoneyNew(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = senderAccount.getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Success);
			senderAccount.setBalance(senderCurrentBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
			PQTransaction t = transactionRepository.save(senderTransaction);
			long accountNumber = senderTransaction.getAccount().getAccountNumber();
			long points = calculatePoints(netTransactionAmount);
			pqAccountDetailRepository.addUserPoints(points, accountNumber);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
						SMSTemplate.FUNDTRANSFER_SUCCESS_SENDER, sender, senderTransaction, receiverUsername);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
						sender, senderTransaction, receiverUsername,null);
			}
		}


		User receiverAccount = userApi.findByUserName(receiverUsername);
		PQTransaction instantpayTransaction = new PQTransaction();
		PQTransaction instantpayTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		PQAccountDetail ipayAccount = receiverAccount.getAccountDetail();
		if (instantpayTransactionExists == null) {
			double receiverCurrentBalance =  ipayAccount.getBalance() + netTransactionAmount;
			instantpayTransaction.setCurrentBalance(receiverCurrentBalance);
			instantpayTransaction.setAmount(amount);
			instantpayTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			instantpayTransaction.setDescription(description);
			instantpayTransaction.setService(service);
			instantpayTransaction.setAccount(ipayAccount);
			instantpayTransaction.setTransactionRefNo(transactionRefNo + "C");
			instantpayTransaction.setDebit(false);
			instantpayTransaction.setStatus(Status.Success);
			PQTransaction t = transactionRepository.save(instantpayTransaction);
			ipayAccount.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(ipayAccount);
			//					userApi.updateBalance(receiverCurrentBalance, receiver);
			if (t != null) {
				if (service.getCode().equalsIgnoreCase("SMU")) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiverAccount, t,
							senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED, receiverAccount, t,
							senderUsername,null);
				} else {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiverAccount, t,
							senderUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction",
							MailTemplate.FUNDTRANSFER_SUCCESS_RECEIVER, receiverAccount, t,
							senderUsername,null);
				}
			}
		}

	}

	/* Travel Bus started */

	@Override
	public PQTransaction initiateBusPayment(double amount, String description, PQService service,
			String transactionRefNo, String senderUsername, String receiverUsername) {

		// TODO Auto-generated method stub
		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(Double.parseDouble(String.format("%.2f", netTransactionAmount)));
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", senderCurrentBalance)));
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);
		}

		User bus = userApi.findByUserName(receiverUsername);
		PQTransaction orderTransaction = new PQTransaction();
		PQTransaction orderTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (orderTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = bus.getAccountDetail().getBalance();
			orderTransaction.setCurrentBalance(receiverCurrentBalance);
			orderTransaction.setAmount(receiverTransactionAmount);
			orderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			orderTransaction.setDescription(description);
			orderTransaction.setService(service);
			orderTransaction.setAccount(bus.getAccountDetail());
			orderTransaction.setTransactionRefNo(transactionRefNo + "C");
			orderTransaction.setDebit(false);
			orderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(orderTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		return senderTransaction;

	}


	@Override
	public PQTransaction successBusPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		String senderUsername = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		System.err.println("senderTransaction of Bus is:: "+senderTransaction);
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderUsername = sender.getUsername();
			senderService = senderTransaction.getService();
			PQCommission senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			System.err.println("PQCommission of Bus is::"+senderCommission);
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);

			}
		}


		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User bus = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", receiverCurrentBalance)));
				PQAccountDetail receiverAccount = bus.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);

			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
		return senderTransaction;
	}




	@Override
	public void failedBusPayment(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = senderAccount.getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);

			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Reversed);
				transactionRepository.save(receiverTransaction);

			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Reversed);
				transactionRepository.save(commissionTransaction);
			}
		}


	}


	@Override
	public String cancelBusTicket(String transactionRefNo,double refundedAmt) {

		System.err.print(transactionRefNo);
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");

		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");

			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + refundedAmt;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);

			}
		}


		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo+ "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {

				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverTransaction.getAccount().getBalance()-refundedAmt;
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(refundedAmt);
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+refundedAmt+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);


			}
		}


		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Bus Ticket cancel transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);

				commissionTransaction.setStatus(Status.Refunded);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}
		return newTransactionRefNo;
	}


	/* Travel Bus ended */

	//flight payment thru gateway

	@Override
	public PQTransaction initiateFlightPaymentEBS(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername,String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		double bkAmt=amount-200;
		
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, bkAmt);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, bkAmt);
		netCommissionValue=netCommissionValue+200;
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo("FL"+ transactionRefNo + "D");
		if (transactionExists == null) {
			double transactionAmount = amount;
			senderUserBalance = sender.getAccountDetail().getBalance();
			senderCurrentBalance = senderUserBalance;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(EBSConstants.DESCRIPTION_FLIGHT + " of Rs." + amount);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo("FL"+ transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setFavourite(false);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Processing);
			senderTransaction.setTransactionType(TransactionType.FLIGHT);
			transactionRepository.save(senderTransaction);
		}

		User flight = userApi.findByUserName(receiverUsername);
		PQTransaction orderTransaction = new PQTransaction();
		PQTransaction orderTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (orderTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = flight.getAccountDetail().getBalance();
			orderTransaction.setCurrentBalance(receiverCurrentBalance);
			orderTransaction.setAmount(receiverTransactionAmount);
			orderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			orderTransaction.setDescription(EBSConstants.DESCRIPTION_FLIGHT + " of Rs." + amount);
			orderTransaction.setService(service);
			orderTransaction.setAccount(flight.getAccountDetail());
			orderTransaction.setTransactionRefNo(transactionRefNo + "C");
			orderTransaction.setDebit(false);
			orderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(orderTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(EBSConstants.DESCRIPTION_FLIGHT + " of Rs." + amount);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		return senderTransaction;
	}


	//flight payment success thru gateway

	@Override
	public void successFlightPaymentEBS(String transactionRefNo,String paymentId) {

		PQTransaction senderTransaction = getTransactionByRefNo("FL" +transactionRefNo + "D");

		double netTransactionAmount = 0;
		double netCommissionValue = 0;

		if (senderTransaction!=null) {
			if ((senderTransaction.getStatus().equals(Status.Processing))) {
				User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
				double transactionAmount = senderTransaction.getAmount();
				double senderUserBalance = sender.getAccountDetail().getBalance();
				double senderCurrentBalance = senderUserBalance;

				PQCommission senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
				netTransactionAmount = senderTransaction.getAmount();
				netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount-200);

				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Booked);
				senderTransaction.setRetrivalReferenceNo(paymentId);
				PQTransaction t = transactionRepository.save(senderTransaction);
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				/*int i = userApi.updateBalance(senderCurrentBalance, sender);
				logger.error("Updated Load money " + i);*/
				if (t != null) {
					/*  smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.FLIGHT_LOADMONEY_SUCCESS, sender,
	                        senderTransaction, null);*/
					/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FLIGHT_LOADMONEY_SUCCESS, sender,
	                        senderTransaction, null,null);*/
				}
			}
			
			PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
			if (receiverTransaction != null) {
				if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
					User flight = userApi.findByAccountDetail(receiverTransaction.getAccount());
					double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
					double amount = receiverTransaction.getAmount();
					receiverCurrentBalance = receiverCurrentBalance + amount;
					receiverTransaction.setStatus(Status.Success);
					receiverTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", receiverCurrentBalance)));
					PQAccountDetail receiverAccount = flight.getAccountDetail();
					receiverAccount.setBalance(receiverCurrentBalance);
					pqAccountDetailRepository.save(receiverAccount);
					transactionRepository.save(receiverTransaction);
				}
			}

			PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
			if (commissionTransaction != null) {
				if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
					User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
					double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
					commissionCurrentBalance = commissionCurrentBalance + netCommissionValue+200;
					commissionTransaction.setStatus(Status.Success);
					commissionTransaction.setCurrentBalance(Double.parseDouble(String.format("%.2f", commissionCurrentBalance)));
					PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
					accountDetail.setBalance(commissionCurrentBalance);
					pqAccountDetailRepository.save(accountDetail);
					transactionRepository.save(commissionTransaction);
				}
			}
		}
		}


			    @Override
				public void successTKPayment(String transactionRefNo,long userOrderId,PQService service,double amount,TKOrderDetail orderDetail) {
					// TODO Auto-generated method stub
					String senderMobileNumber = null;
					String receiverMobileNumber = null;
					String senderUsername = null;
					double senderCurrentBalance=0;
					PQCommission senderCommission = new PQCommission();
					PQService senderService = new PQService();
					double netTransactionAmount = amount;
					double netCommissionValue = 0;
					PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
				try{
					if (senderTransaction != null) {
						User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
						senderMobileNumber = sender.getUsername();
						senderUsername = sender.getUsername();
						
												/**************/
						senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
					    netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
						double senderUserbalance = sender.getAccountDetail().getBalance();
						if (senderCommission.getType().equalsIgnoreCase("POST")) {
							netTransactionAmount = netTransactionAmount + netCommissionValue;
						}
						senderCurrentBalance = senderUserbalance - netTransactionAmount;
						System.err.println("senderCurrentBalance current balnce after food ordered amount deduction "+senderCurrentBalance);
						String v = String.format("%.2f", senderCurrentBalance);

						senderCurrentBalance = Double.parseDouble(v);
						System.err.println("senderCurrentBalance current balnce after ticket amount deduction in right format "+senderCurrentBalance);

						PQAccountDetail senderAccountDetail = sender.getAccountDetail();
						senderAccountDetail.setBalance(senderCurrentBalance);
						
												/**************/
						
						senderService = senderTransaction.getService();
						senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
						netTransactionAmount = senderTransaction.getAmount();
						netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);

						System.err.println(" Net Commision :::::::::::::::::::::"+netCommissionValue);
						org.json.JSONObject json = null;
						String mobileNumber = null;
						if ((senderTransaction.getStatus().equals(Status.Initiated))) {
							senderTransaction.setStatus(Status.Success);
							senderTransaction.setCurrentBalance(senderCurrentBalance);
							PQTransaction t = transactionRepository.save(senderTransaction);
							long accountNumber = senderTransaction.getAccount().getAccountNumber();
							long points = calculatePoints(netTransactionAmount);
							pqAccountDetailRepository.addUserPoints(points, accountNumber);
							pqAccountDetailRepository.save(senderAccountDetail);
							
							if (t != null) {
								smsSenderApi.sendTransactionSMSOrderFood(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TK_BILLPAYMENT_SUCCESS,
										sender, senderTransaction, null,userOrderId,orderDetail);
								mailSenderApi.sendTransactionMailFoodOrder(" Order Registration : Your order has been placed at VPayQwik.com", MailTemplate.TK_BILLPAYMENT_SUCCESS, sender,
										senderTransaction," ",null,userOrderId,orderDetail);
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
					failedTKPayment(transactionRefNo,orderDetail);      //call in case failed payment
				}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User flight = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount2 = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount2;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				PQAccountDetail receiverAccount = flight.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				/*if (t != null) {
						mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, flight,
								receiverTransaction, senderUsername, MailConstants.CC_MAIL);
					}*/
			}
		}


		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue+200;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}

	}



	@Override
	public void failedFlightEBS(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo("FL"+ transactionRefNo + "D");
		if ((senderTransaction.getStatus().equals(Status.Processing))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
		}
	}

	@Override
	public PQTransaction getTransactionByRetrivalReferenceNo(String retrivalReferenceNo) {
		return transactionRepository.findByTransactionRetivalReferenceNo(retrivalReferenceNo);
	}		

	//flight payment thru gateway
	@Override
	public PQTransaction initiateFlightPaymentVnet(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {
		User sender = userApi.findByUserName(senderUsername);
		
		double bkAmt=amount-200;
		
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, bkAmt);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, bkAmt);
		netCommissionValue=netCommissionValue+200;
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo("FL"+ transactionRefNo + "D");
		if (transactionExists == null) {
			double transactionAmount = amount;
			senderUserBalance = sender.getAccountDetail().getBalance();
		    senderCurrentBalance = senderUserBalance;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo("FL"+ transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setFavourite(false);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Processing);
			senderTransaction.setTransactionType(TransactionType.FLIGHT);
			transactionRepository.save(senderTransaction);
		}
		
		User flight = userApi.findByUserName(receiverUsername);
		PQTransaction orderTransaction = new PQTransaction();
		PQTransaction orderTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (orderTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = flight.getAccountDetail().getBalance();
			orderTransaction.setCurrentBalance(receiverCurrentBalance);
			orderTransaction.setAmount(receiverTransactionAmount);
			orderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			orderTransaction.setDescription(description);
			orderTransaction.setService(service);
			orderTransaction.setAccount(flight.getAccountDetail());
			orderTransaction.setTransactionRefNo(transactionRefNo + "C");
			orderTransaction.setDebit(false);
			orderTransaction.setStatus(Status.Initiated);
			transactionRepository.save(orderTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		
		return senderTransaction;
	}


	//flight payment success thru gateway

	@Override
	public void successFlightPaymentVnet(String transactionRefNo,String paymentId) {
		
		PQTransaction senderTransaction = getTransactionByRefNo("FL" +transactionRefNo + "D");
		
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		
		if (senderTransaction != null) {
		if ((senderTransaction.getStatus().equals(Status.Processing))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			
			PQCommission senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount-200);
			
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Booked);
			senderTransaction.setRetrivalReferenceNo(paymentId);
			PQTransaction t = transactionRepository.save(senderTransaction);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			/*int i = userApi.updateBalance(senderCurrentBalance, sender);
			logger.error("Updated Load money " + i);*/
			
			if (t != null) {
				/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.LOADMONEY_SUCCESS, sender,
						senderTransaction, null,null);*/
			}
		}
	}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User flight = userApi.findByAccountDetail(receiverTransaction.getAccount());
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				double amount = receiverTransaction.getAmount();
				receiverCurrentBalance = receiverCurrentBalance + amount;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				PQAccountDetail receiverAccount = flight.getAccountDetail();
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				/*if (t != null) {
						mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.MERCHANT_RECEIVER, flight,
								receiverTransaction, senderUsername, MailConstants.CC_MAIL);
					}*/
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue+200;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
				accountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
		
	}


	@Override
	public void failedFlightVnet(String transactionRefNo) {
		PQTransaction senderTransaction = getTransactionByRefNo("FL"+ transactionRefNo + "D");
		if ((senderTransaction.getStatus().equals(Status.Processing))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
		}
	}


	@Override
	public ResponseDTO processNikiChatRefund(MicroPaymentServiceStatus dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef + "D");
			if(transaction != null){
				if(transaction.getStatus().equals(Status.Success)){
					// TODO credit into user wallet account
					processRefundNikkiTransaction(transactionRef);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Credited Successfully");
					result.setDetails("Credited Successfully");
				}else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Transaction was "+transaction.getStatus());
				}
			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not exists");
			}
		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}

		return result;
	}

	@Override
	public void processRefundNikkiTransaction(String transactionRefNo) {
		System.err.print(transactionRefNo);
		ResponseDTO result = new ResponseDTO();
		String newTransactionRefNo = ""+System.nanoTime();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount + netCommissionValue;
			}
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");

			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettlement = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(newTransactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(true);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				transactionRepository.save(debitSettlementTransaction1);
				accountDetail.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(accountDetail);
				//				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS_SENDER	,receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Nikki chat transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);

				commissionTransaction.setStatus(Status.Refunded);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}
	}




	/* PIYUSH SUCCESS_REDEEM_POINT */
	@Override
	public void successRedeemMoney(double amount, String description, PQService service,String receiverUserName,long point) {     //amount==100/50
		User sender = userApi.findByUserName(PayQwikUtil.PROMOCODE_NUMBER);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommision = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommision, amount);
		double senderCurrentBalance = senderAccount.getBalance();
		double netTransactionAmount=amount;
		long transactionRefNo= System.currentTimeMillis();
		//double senderUserBalance = senderAccount.getBalance();
		if(senderCommision.getType().equalsIgnoreCase("POST")){
			netTransactionAmount = netTransactionAmount+netCommissionValue;    
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction  exists = getTransactionByRefNo(transactionRefNo + "D");
		if(exists==null){
			senderCurrentBalance=senderCurrentBalance-netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommision.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo +"D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Success);

			senderAccount.setBalance(senderCurrentBalance);
			PQAccountDetail updateDetails=pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		User receiverUsername = userApi.findByUserName(receiverUserName); 
		PQTransaction receiveTransaction = new PQTransaction();
		PQCommission receiverCommision = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue2 = commissionApi.getCommissionValue(receiverCommision, amount);
		PQTransaction receiveTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		double receiverTransactionAmount;
		if (receiverCommision.getType().equalsIgnoreCase("POST")) {
			receiverTransactionAmount = netTransactionAmount - netCommissionValue2;
		}
		if (receiveTransactionExists == null) {
			double receiverCurrentBalance = receiverUsername.getAccountDetail().getBalance();
			receiverCurrentBalance=receiverCurrentBalance+netTransactionAmount;

			receiveTransaction.setCurrentBalance(receiverCurrentBalance);
			receiveTransaction.setAmount(amount);
			receiveTransaction.setCommissionIdentifier(receiverCommision.getIdentifier());
			receiveTransaction.setDescription(description);
			receiveTransaction.setService(service);
			receiveTransaction.setAccount(receiverUsername.getAccountDetail());
			receiveTransaction.setTransactionRefNo(transactionRefNo + "C");
			receiveTransaction.setDebit(false);
			receiveTransaction.setStatus(Status.Success);
			transactionRepository.save(receiveTransaction);
			smsSenderApi.sendRedeemPointsSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REDEEMPOINTS_SUCCESS,receiverUsername,amount,point,receiveTransaction);	
			String desc = point+" points Successfully redeemed  to cash ";
			mailSenderApi.sendRedeemPointsMail(desc, MailTemplate.REDEEMPOINTS_CASH_SUCCESS,receiverUsername,point,amount,receiveTransaction);
		} 

	}

	@Override
	public Page<PQTransaction> getCommissionOfMerchantServicePage(PQService service ,Pageable pageable) {
		return transactionRepository.findAllCommissionByMerchantServicePage(service,pageable);
	}

	@Override
	public List<PQTransaction> getCommissionOfMerchantService(PQService service ) {
		return transactionRepository.findAllCommissionByMerchantService(service);
	}
	
	@Override
	public Page<PQTransaction> getCommissionOfTKService(Pageable pageable,PQService service ) {
		return transactionRepository.findAllCommissionByTKService(pageable,service);
	}

	//TRAVELKAHAN PLACE ORDER 


	@Override
	public void initiateTKPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername) {
		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
	//		senderCurrentBalance = senderUserBalance - netTransactionAmount;
			/*System.err.println("senderCurrentBalance current balnce after food ordered amount deduction "+senderCurrentBalance);
			String v = String.format("%.2f", senderCurrentBalance);

			senderCurrentBalance = Double.parseDouble(v);
			System.err.println("senderCurrentBalance current balnce after ticket amount deduction in right format "+senderCurrentBalance);
*/
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderUserBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);

			//			int update = userApi.updateBalance(senderCurrentBalance, sender);
//			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);

			System.err.println("*****rows updated*******" + updatedDetails.getTransactionRefNo());
			System.out.println("BALANCE="+senderUserBalance);
			System.out.println("CURRENT BALANCE="+sender.getAccountDetail().getBalance());
			User temp = userApi.findByUserName(senderUsername);
		}

		User receiver = userApi.findByUserName(StartupUtil.TRAVELKHANA);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = receiver.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(receiverCurrentBalance);
			eventTransaction.setAmount(receiverTransactionAmount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(receiver.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
			System.err.println("Receiver intiated executed");
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
			System.err.println("Commission Initiated");
		}
	}

	@Override
	public void failedTKPayment(String transactionRefNo,TKOrderDetail orderDetail) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			System.out.println("FAILED SENDER BALANCE:"+senderUserBalance);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
		//		senderCurrentBalance = senderUserBalance + netTransactionAmount;   MODIFY
				senderCurrentBalance = senderUserBalance;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();
	//			senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionFailTKMail(" Order Registration : Your order has been failed at VPayQwik.com", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null,orderDetail);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}
	
	
	@Override
	public void initiateTreatCardPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername,PQCommission senderCommission,double netCommissionValue) {

		User sender = userApi.findByUserName(senderUsername);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);
			User temp = userApi.findByUserName(senderUsername);
		}
		User tretaCard = userApi.findByUserName(receiverUsername);
		PQTransaction treatCardTransaction = new PQTransaction();
		PQTransaction treatCardTrasnactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (treatCardTrasnactionExists == null) {
			double receiverTransactionAmount = 0;
			
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = tretaCard.getAccountDetail().getBalance();
			treatCardTransaction.setCurrentBalance(receiverCurrentBalance);
			treatCardTransaction.setAmount(receiverTransactionAmount);
			treatCardTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			treatCardTransaction.setDescription(description);
			treatCardTransaction.setService(service);
			treatCardTransaction.setAccount(tretaCard.getAccountDetail());
			treatCardTransaction.setTransactionRefNo(transactionRefNo + "C");
			treatCardTransaction.setDebit(false);
			treatCardTransaction.setStatus(Status.Initiated);
			transactionRepository.save(treatCardTransaction);
		}
		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successTreatCardPayment(String transactionRefNo,PQCommission senderCommission,double netCommissionValue) {
		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		 netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderMobileNumber = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							sender, senderTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS, sender,
							senderTransaction, receiverMobileNumber,null);
				}
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				//				userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, senderMobileNumber,null);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				transactionRepository.save(commissionTransaction);
			}
		}
	}
	
	@Override
	public void failedTreatCardPayment(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			PQAccountDetail senderAccount = sender.getAccountDetail();
			double senderUserBalance = senderAccount.getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Reversed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Reversed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}

	
//	HouseJoy Api
	
	@Override
	public PQTransaction initiateHouseJoyPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {

		User sender = userApi.findByUserName(senderUsername);
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = sender.getAccountDetail().getBalance();
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);

			//int update = userApi.updateBalance(senderCurrentBalance, sender);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			PQTransaction updatedDetails = transactionRepository.save(senderTransaction);

			//System.err.println("*****rows updated*******" + update);
			User temp = userApi.findByUserName(senderUsername);

		}

		

		User qwikPay = userApi.findByUserName(StartupUtil.HOUSE_JOY);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount +netCommissionValue;
			}
			else {
				receiverTransactionAmount = netTransactionAmount -netCommissionValue;
			}
			double receiverCurrentBalance = qwikPay.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(receiverCurrentBalance);
			eventTransaction.setAmount(receiverTransactionAmount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(qwikPay.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
		return senderTransaction;
	}
	
	@Override
	public void successHouseJoyPayment(String transactionRefNo,String jobId) {

		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			System.err.println("username::"+sender.getUsername());
			senderMobileNumber = sender.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			org.json.JSONObject json = null;
			String mobileNumber = null;
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				senderTransaction.setRetrivalReferenceNo(jobId);
				
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					 smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.HOUSEJOY_PAYMENT_SUCCESS,
                            sender, senderTransaction, null);
                    mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.HOUSEJOY_SUCCESS, sender,
                            senderTransaction," ",null);
				}
			}

		}
		

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				//userApi.updateBalance(receiverCurrentBalance, receiver);
				PQTransaction t = transactionRepository.save(receiverTransaction);

				if (t != null) {
					/*smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, senderMobileNumber,null);*/
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				//userApi.updateBalance(commissionCurrentBalance, commissionAccount);
				transactionRepository.save(commissionTransaction);
			}
		}
	}
	
	@Override
	public void failedHouseJoyPayment(String transactionRefNo) {

		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();

			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			System.err.println(senderCurrentBalance+"\n"+netCommissionValue);
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();

				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);

				System.err.println(senderCurrentBalance);
				System.err.println(senderCurrentBalance);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}
		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
		if (debitSettlementTransaction != null) {
			User debitSettelment = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
			PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
			double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
			String debitSettlementDescription = debitSettlementTransaction.getDescription();
			double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
			debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
			debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
			PQTransaction debitSettlementTransaction1 = new PQTransaction();
			PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
			if (settlementTransactionExists == null) {
				PQAccountDetail debitSettlementAccount = accountDetail;
				debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
				debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
				debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
				debitSettlementTransaction1.setDescription(debitSettlementDescription);
				debitSettlementTransaction1.setService(senderService);
				debitSettlementTransaction1.setTransactionRefNo(transactionRefNo + "DS");
				debitSettlementTransaction1.setDebit(false);
				debitSettlementTransaction1.setAccount(accountDetail);
				debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
				debitSettlementTransaction1.setStatus(Status.Success);
				debitSettlementAccount.setBalance(debitSettlementCurrentBalance);
				pqAccountDetailRepository.save(debitSettlementAccount);
				transactionRepository.save(debitSettlementTransaction1);
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
	}
	
	
	@Override
	public PQTransaction initiateMerchantUPIPayent(double amount, String description, PQService service, String transactionRefNo,
			User sender, String authRefNo) {
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction transactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (transactionExists == null) {
			double transactionAmount = amount;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(transactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "C");
			senderTransaction.setDebit(false);
			senderTransaction.setFavourite(false);
			senderTransaction.setTransactionType(TransactionType.VIJAYA_UPI);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setAuthReferenceNo(authRefNo);
			senderTransaction.setStatus(Status.Sent);
			return transactionRepository.save(senderTransaction);
		}
		return transactionExists;
	}

	@Override
	public void successMerchantUPIPayent(String retrivalReferenceNo, String consumerId, String orgRefId) {
		// TODO Auto-generated method stub
		PQTransaction senderTransaction = getTransactionByRetrivalReferenceNo(retrivalReferenceNo);
		if ((senderTransaction.getStatus().equals(Status.Sent))) {

//			PQTransaction approvedTxn = new PQTransaction();
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			double transactionAmount = senderTransaction.getAmount();
			double senderUserBalance = sender.getAccountDetail().getBalance();
			double senderCurrentBalance = senderUserBalance + transactionAmount;

//			approvedTxn.setCommissionIdentifier(senderTransaction.getCommissionIdentifier());
//			approvedTxn.setAmount(senderTransaction.getAmount());
//			approvedTxn.setDescription(senderTransaction.getDescription());
//			approvedTxn.setService(senderTransaction.getService());
//			approvedTxn.setTransactionRefNo(newRefNo + "C");
//			approvedTxn.setDebit(false);
//			approvedTxn.setFavourite(false);
//			approvedTxn.setCurrentBalance(senderCurrentBalance);
//			approvedTxn.setRetrivalReferenceNo(senderTransaction.getTransactionRefNo());
//			approvedTxn.setAccount(sender.getAccountDetail());
//			approvedTxn.setStatus(Status.Success);
//			PQTransaction t = transactionRepository.save(approvedTxn);
			
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setOrgRefId(orgRefId);
		 	PQTransaction t = transactionRepository.save(senderTransaction);
			
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);

			int i = userApi.updateBalance(senderCurrentBalance, sender);
			logger.error("Updated Load money " + i);
			if (t != null) {
				smsSenderApi.sendBescomTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BESCOM_UPI_SUCCESS, sender,
						senderTransaction, consumerId);
//				mailSenderApi.sendTransactionMail("Vijaya Bescom Transaction", MailTemplate.LOADMONEY_SUCCESS, sender,
//						senderTransaction, null,null);
			}
		}
	}

	@Override
	public void failedMerchantUPIPayent(String retrivalReferenceNo, String orgRefId) {
		PQTransaction senderTransaction = getTransactionByRetrivalReferenceNo(retrivalReferenceNo);
		if ((senderTransaction.getStatus().equals(Status.Sent))) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderTransaction.setStatus(Status.Failed);
			senderTransaction.setOrgRefId(orgRefId);
			transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public void updateMerchantUPIPayentTrasaction(String transactionRefNo, String upiId, String referenceId) {
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "C");
		senderTransaction.setRetrivalReferenceNo(referenceId);
		senderTransaction.setUpiId(upiId);
		transactionRepository.save(senderTransaction);
	}
	
	@Override
	public void successSendMoneyForBulkFile(String transactionRefNo) {

		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String senderUsername = "";
		String receiverUsername = "";
		double netTransactionAmount = 0;
		double netCommissionValue = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			senderUsername = sender.getUsername();
			receiverUsername = receiver.getUsername();
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
			netTransactionAmount = senderTransaction.getAmount();
			netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t = transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				if (t != null) {
					/*smsSenderApi.sendBulkFileSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
							SMSTemplate.BULKUPLOAD_PAYMENT_SUCCESS, sender, senderTransaction, receiverUsername);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.FUNDTRANSFER_SUCCESS_SENDER,
							sender, senderTransaction, receiverUsername,null);*/
				}
			}
		}
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				BulkFile file=bulkFileRepository.findByMobileNumberWithSatus(receiver.getUsername(),Status.Approved);
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				/*TODO credit amount to receiver */
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount;
				receiverTransaction.setStatus(Status.Success);
				file.setStatus(Status.Close);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				String serviceCode = receiverTransaction.getService().getCode();
				receiverAccount.setBalance(receiverCurrentBalance);
				PQAccountDetail updatedDetails = pqAccountDetailRepository.save(receiverAccount);
				bulkFileRepository.save(file);
				PQTransaction t = transactionRepository.save(receiverTransaction);
				if (t != null) {
					if (serviceCode.equalsIgnoreCase("SMU")) {
						smsSenderApi.sendBulkFileSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.BULKUPLOAD_PAYMENT_SUCCESS, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendBulkUploadMail("VPayQwik Transaction",
								MailTemplate.BULKUPLOAD_PAYMENT_SUCCESS, receiver, receiverTransaction,
								senderUsername,null);
					} else {
						smsSenderApi.sendBulkFileSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,
								SMSTemplate.BULKUPLOAD_PAYMENT_SUCCESS, receiver, receiverTransaction,
								senderUsername);
						mailSenderApi.sendBulkUploadMail("VPayQwik Transaction",
								MailTemplate.BULKUPLOAD_PAYMENT_SUCCESS, receiver, receiverTransaction,
								senderUsername,null);
					}
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				/*TODO credit amount to commission account */
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				transactionRepository.save(commissionTransaction);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				PQAccountDetail commissionUpdatedDetails = pqAccountDetailRepository.save(commissionAccountDetail);
				//				userApi.updateBalance(commissionCurrentBalance, commissionAccount);
			}
		}
	}
	
	
	
	//WOOHOO

	@Override
	public void initiateWoohooPayment(WoohooTransactionRequest dto, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername,PQCommission senderCommission,double netCommissionValue) {
		double amount=dto.getPrice();
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			PQAccountDetail senderAccountDetail = sender.getAccountDetail();
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Initiated);
			senderAccountDetail.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccountDetail);
			transactionRepository.save(senderTransaction);
			WoohooCardDetails cardDetails=new WoohooCardDetails();
			cardDetails.setTransactionRefNo(transactionRefNo);
			cardDetails.setFirstName(dto.getFirstname());
			cardDetails.setLastName(dto.getLastname());
			cardDetails.setEmailId(dto.getEmail());
			cardDetails.setContanctNo(dto.getTelephone());
			cardDetails.setUser(sender);
			cardDetails.setStatus("Processing");
			cardDetails.setTransaction(senderTransaction);
			woohooCardDetailsRepository.save(cardDetails);

		}

		User Gci = userApi.findByUserName(StartupUtil.WOOHOO);
		PQTransaction eventTransaction = new PQTransaction();
		PQTransaction eventTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (eventTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("PRE")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}
			double receiverCurrentBalance = Gci.getAccountDetail().getBalance();
			eventTransaction.setCurrentBalance(BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			eventTransaction.setAmount(receiverTransactionAmount);
			eventTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			eventTransaction.setDescription(description);
			eventTransaction.setService(service);
			eventTransaction.setAccount(Gci.getAccountDetail());
			eventTransaction.setTransactionRefNo(transactionRefNo + "C");
			eventTransaction.setDebit(false);
			eventTransaction.setStatus(Status.Initiated);
			transactionRepository.save(eventTransaction);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance() + netCommissionValue;
			commissionTransaction.setCurrentBalance(BigDecimal.valueOf(commissionCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Initiated);
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void successWoohooPayment(String transactionRefNo,PQCommission senderCommission,double netCommissionValue,User u,WoohooTransactionResponse response) {
		// TODO Auto-generated method stub
		String senderMobileNumber = null;
		String receiverMobileNumber = null;
		PQService senderService = new PQService();
		double netTransactionAmount = 0;
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderMobileNumber = sender.getUsername();
			senderService = senderTransaction.getService();
			netTransactionAmount = senderTransaction.getAmount();
			org.json.JSONObject json = null;
			String mobileNumber = null;
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				senderTransaction.setStatus(Status.Success);
				PQTransaction t= transactionRepository.save(senderTransaction);
				long accountNumber = senderTransaction.getAccount().getAccountNumber();
				long points = calculatePoints(netTransactionAmount);
				pqAccountDetailRepository.addUserPoints(points, accountNumber);
				WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
				if(cardDetails!=null){
					response.setEmail(cardDetails.getEmailId());
					response.setName(cardDetails.getFirstName());
					cardDetails.setCard_price(response.getCard_price());
					cardDetails.setCardName(response.getCardName());
					cardDetails.setCardnumber(response.getCardnumber());
					cardDetails.setExpiry_date(response.getExpiry_date());
					cardDetails.setPin_or_url(response.getPin_or_url());
					cardDetails.setOrderId(response.getOrderId());
					cardDetails.setRetrivalRefNo(response.getRetrivalRefNo());
					cardDetails.setStatus("Success");
					smsSenderApi.sendUserWoohooSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,SMSTemplate.GCI_SMS,cardDetails,u);
					woohooCardDetailsRepository.save(cardDetails);
				}
				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,sender, senderTransaction, null);
					if(response.getTermsAndConditions()!=null){
					mailSenderApi.sendwoohoogiftcardmail("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER,null, response, u);
					}else{
						mailSenderApi.sendwoohoogiftcardmail("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER_WITHOUT_TC,null, response, u);
					}
				}
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
				receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
				receiverTransaction.setStatus(Status.Success);
				receiverTransaction.setCurrentBalance(receiverCurrentBalance);
				receiverAccount.setBalance(receiverCurrentBalance);
				pqAccountDetailRepository.save(receiverAccount);
				PQTransaction t = transactionRepository.save(receiverTransaction);

				if (t != null) {
					smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, null);
					mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
							receiver, receiverTransaction, senderMobileNumber,null);
				}
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
				double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
				commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
				commissionTransaction.setStatus(Status.Success);
				commissionTransaction.setCurrentBalance(commissionCurrentBalance);
				commissionAccountDetail.setBalance(commissionCurrentBalance);
				pqAccountDetailRepository.save(commissionAccountDetail);
				//					userApi.updateBalance(commissionCurrentBalance, commissionAccount);
				transactionRepository.save(commissionTransaction);
			}

		}
	}

	@Override
	public void failedWoohooPayment(String transactionRefNo) {
		// TODO Auto-generated method stub
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				netTransactionAmount = netTransactionAmount;
			}
			if ((senderTransaction.getStatus().equals(Status.Initiated))) {
				List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
				PQTransaction lastTrans = null;
				if (lastTransList != null && !lastTransList.isEmpty()) {
					lastTrans = lastTransList.get(lastTransList.size() - 1);
					senderCurrentBalance =  lastTrans.getCurrentBalance();
				}
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Reversed);
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
//				userApi.updateBalance(senderCurrentBalance, sender);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
						sender, senderTransaction, null);
				/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);*/
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				receiverTransaction.setStatus(Status.Failed);
				transactionRepository.save(receiverTransaction);
//				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
//						receiver, receiverTransaction, null);
				/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
	*/		}
		}
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
				PQAccountDetail senderAccount = commissionTransaction.getAccount();
				double balance=commissionTransaction.getAmount();
				double accountBalnce=senderAccount.getBalance();
				commissionTransaction.setStatus(Status.Failed);
				transactionRepository.save(commissionTransaction);
			}
		}
		WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
		if(cardDetails!=null){
			cardDetails.setStatus("Failure");
			woohooCardDetailsRepository.save(cardDetails);
		}
	}
	
	@Override
	public List<WoohooCardDetailsDTO> getallCards() {
		List<WoohooCardDetails> cardList=woohooCardDetailsRepository.findAllCards();
		List<WoohooCardDetailsDTO> resultList=new ArrayList<>();
		for (WoohooCardDetails card : cardList) {
			WoohooCardDetailsDTO dto=new WoohooCardDetailsDTO();
			dto.setCard_price((card.getCard_price() == null) ? "NA" : card.getCard_price());
			dto.setCardName((card.getCardName() == null) ? "NA" : card.getCardName());
			dto.setCardnumber((card.getCardnumber() == null) ? "NA" : card.getCardnumber());
			dto.setContanctNo((card.getContanctNo() == null) ? "NA" : card.getContanctNo());
			dto.setEmailId((card.getEmailId() == null) ? "NA" : card.getEmailId());
			dto.setExpiry_date((card.getExpiry_date() == null) ? "NA" : card.getExpiry_date());
			dto.setPin_or_url((card.getPin_or_url() == null) ? "NA" : card.getPin_or_url());
			dto.setTransactionRefNo((card.getTransactionRefNo() == null) ? "NA" : card.getTransactionRefNo());
			dto.setOrderId((card.getOrderId() == null) ? "NA" : card.getOrderId());
			dto.setRetrivalRefNo((card.getRetrivalRefNo() == null) ? "NA" : card.getRetrivalRefNo());
			dto.setStatus((card.getStatus() == null) ? "NA" : card.getStatus());
			PQTransaction transaction= transactionRepository.findByTransactionRefNo(dto.getTransactionRefNo()+"D");
			if(transaction !=null){
				dto.setDescription(transaction.getDescription());
				String dateOfTransaction=String.valueOf(transaction.getCreated());
				dto.setDateOfTrasnaction(dateOfTransaction.substring(0, dateOfTransaction.length() - 2));
				User u=	userApi.findByAccountDetail(transaction.getAccount());
				if(u!=null){
					dto.setUserFirstName(u.getUserDetail().getFirstName());
					dto.setUserPhoneNumber(u.getUsername());
				}
			}
			resultList.add(dto);
		}
		return resultList;
	}
	@Override
	public VNetStatusResponse vnetVarification(String transactionRefNo,String serviceCode,double amount) {
		VNetStatusResponse result = new VNetStatusResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = null;
		if(serviceCode.equalsIgnoreCase("LMB")) {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("PID", VNetUtils.VNET_PID);
			formData.add("PRN", transactionRefNo);
			formData.add("ITC", transactionRefNo);
			formData.add("AMT", amount);
			WebResource resource = client.resource(VNetUtils.VNET_STATUS);
			ClientResponse response = resource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			if(strResponse != null){
				Document html = Jsoup.parse(strResponse);
				String h4 = html.body().getElementsByTag("h4").text();
				if(h4.equals("Your Payment is Successful")){
					result.setSuccess(true);
					result.setStatus("Success");
					result.setMessage(h4);
				}else{
					result.setSuccess(false);
					result.setStatus("Failed");
					result.setMessage(h4);
				}
			}
		}
		return result;
	}

	
	@Override
	public List<PQTransaction> iPayRefundedTxnList(PagingDTO dto) {
		List<PQTransaction> transactionList = null;
		Date startDate = dto.getStartDate();
		Date endDate = dto.getEndDate();
		PQServiceType service=pqServiceTypeRepository.findServiceTypeByName("Bill Payment");
		if ((startDate != null) && (endDate != null)) {
//			transactionList = transactionRepository.getIPayRefundedTnxBetween(startDate, endDate);
		} else {
//			transactionList = (List<PQTransaction>) transactionRepository.getAllRefundedInstanPayTxn(service);
		}
		return transactionList;
	}
	
	@Override
	public ResponseDTO processBankTransferRefund(RefundDTO dto) {
		ResponseDTO result = new ResponseDTO();
		String transactionRef = dto.getTransactionRefNo();
		if(transactionRef != null){
			PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
			if(transaction != null){
				String serviceType = transaction.getService().getServiceType().getName();
				if(serviceType.equalsIgnoreCase("Fund Transfer Bank")) {
					if(transaction.getStatus().equals(Status.Success)){
						// TODO credit into user wallet account
						String transactionId = transactionRef.substring(0,transactionRef.length()-1);
						processRefundBankTransferPayment(transactionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Credited Successfully");
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction was "+transaction.getStatus());
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Not a Bank Transfer transaction");
				}

			}else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not exists");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction ID must not be null");
		}

		return result;
	}
	
	
	@Override
	public void processRefundBankTransferPayment(String transactionRefNo) {
		System.err.print(transactionRefNo);
		ResponseDTO result = new ResponseDTO();
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			double netTransactionAmount = senderTransaction.getAmount();
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();
			PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");
			if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderRefundTransaction = new PQTransaction();
				senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
				senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
				senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
				senderRefundTransaction.setDebit(false);
				senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
				senderRefundTransaction.setAmount(netTransactionAmount);
				senderRefundTransaction.setAccount(sender.getAccountDetail());
				senderRefundTransaction.setTransactionType(TransactionType.REFUND);
				senderRefundTransaction.setService(senderService);
				senderRefundTransaction.setStatus(Status.Success);
				senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				senderTransaction.setStatus(Status.Refunded);
				transactionRepository.save(senderTransaction);
				transactionRepository.save(senderRefundTransaction);
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				//				userApi.updateBalance(senderCurrentBalance, sender);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);
			}
		}

		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS_SENDER	,receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
						receiverTransaction, null,null);
			}
		}

		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Send Money transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);

				commissionTransaction.setStatus(Status.Refunded);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}
	}
	
	
	@Override
	public List<PQTransaction> listWooHooTransactionByDate(boolean debit, Date startDate, Date endDate, PQService type,Status success) {
		return transactionRepository.findTransactionByDate(debit,startDate,endDate,type,success);
	}
	
@Override
	public OrderPlaceResponse pendingWoohooPaymentNew(String transactionRefNo, PQCommission commission, double netCommissionValue,
			User u, OrderPlaceResponse response) {
	OrderPlaceResponse resp= new OrderPlaceResponse();
	String senderMobileNumber = null;
	String receiverMobileNumber = null;
	PQService senderService = new PQService();
	double netTransactionAmount = 0;
	PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
	if (senderTransaction != null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		senderMobileNumber = sender.getUsername();
		senderService = senderTransaction.getService();
		netTransactionAmount = senderTransaction.getAmount();
		org.json.JSONObject json = null;
		String mobileNumber = null;
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setRetrivalReferenceNo(response.getRetrivalRefNo());
			senderTransaction.setCheckStatus(true);
			resp.setRemainingBalance(senderTransaction.getCurrentBalance());
			PQTransaction t= transactionRepository.save(senderTransaction);
			long accountNumber = senderTransaction.getAccount().getAccountNumber();
			long points = calculatePoints(netTransactionAmount);
			pqAccountDetailRepository.addUserPoints(points, accountNumber);
			WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
			if(cardDetails!=null){
				response.setEmail(cardDetails.getEmailId());
				response.setName(cardDetails.getFirstName());
				cardDetails.setCard_price(response.getCard_price());
				cardDetails.setCardName(response.getCardName());
				cardDetails.setCardnumber(response.getCardnumber());
				cardDetails.setExpiry_date(response.getExpiry_date());
				cardDetails.setPin_or_url(response.getPin_or_url());
				cardDetails.setOrderId(response.getOrderNumber());
				cardDetails.setRetrivalRefNo(response.getRetrivalRefNo());
				woohooCardDetailsRepository.save(cardDetails);
			}
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.WOOHOO_GIFTCARD_PENDING_SMS,
						sender, senderTransaction, null);
			}
		}
	}

	PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
	if (receiverTransaction != null) {
		if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			PQAccountDetail receiverAccount = receiver.getAccountDetail();
			double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
			receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
			receiverTransaction.setStatus(Status.Success);
			receiverTransaction.setCurrentBalance(receiverCurrentBalance);
			receiverAccount.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(receiverAccount);
			PQTransaction t = transactionRepository.save(receiverTransaction);

			/*if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
						receiver, receiverTransaction, senderMobileNumber,null);
			}*/
		}
	}

	PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
	if (commissionTransaction != null) {
		if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
			User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
			commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
			commissionTransaction.setStatus(Status.Success);
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionAccountDetail.setBalance(commissionCurrentBalance);
			pqAccountDetailRepository.save(commissionAccountDetail);
			//					userApi.updateBalance(commissionCurrentBalance, commissionAccount);
			transactionRepository.save(commissionTransaction);
		}

	}
	return resp;
		
	}


@Override
public void failedStatusWoohooPayment(String transactionRefNo) {
	PQCommission senderCommission = new PQCommission();
	PQService senderService = new PQService();
	PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
	if (senderTransaction != null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		senderService = senderTransaction.getService();
		senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
				senderTransaction.getAmount());
		double netTransactionAmount = senderTransaction.getAmount();
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount;
		}
		if ((senderTransaction.getStatus().equals(Status.Success)) &&(senderTransaction.isCheckStatus()) ) {
			List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
			PQTransaction lastTrans = null;
			if (lastTransList != null && !lastTransList.isEmpty()) {
				lastTrans = lastTransList.get(lastTransList.size() - 1);
				senderCurrentBalance =  lastTrans.getCurrentBalance();
			}
			senderCurrentBalance = senderUserBalance + netTransactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Reversed);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
//			userApi.updateBalance(senderCurrentBalance, sender);
			transactionRepository.save(senderTransaction);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
					sender, senderTransaction, null);
			/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
					senderTransaction, null,null);*/
		}
	}
	PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
	if (receiverTransaction != null) {
		double receiverCurrentBalance = 0;
		if ((receiverTransaction.getStatus().equals(Status.Success))) {
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			PQAccountDetail receiverAccount=receiverTransaction.getAccount();
			receiverCurrentBalance= receiverAccount.getBalance()-receiverTransaction.getAmount();
			receiverAccount.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(receiverAccount);
			receiverTransaction.setStatus(Status.Failed);
			transactionRepository.save(receiverTransaction);
//			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
//					receiver, receiverTransaction, null);
			/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
					receiverTransaction, null,null);
*/		}
	}
	PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
	if (commissionTransaction != null) {
		if ((commissionTransaction.getStatus().equals(Status.Success))) {
			PQAccountDetail senderAccount = commissionTransaction.getAccount();
			double balance=commissionTransaction.getAmount();
			double accountBalnce=senderAccount.getBalance();
			double senderCurrentBalance = accountBalnce - balance;
			commissionTransaction.setStatus(Status.Failed);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(commissionTransaction);
		}
	}
	WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
	if(cardDetails!=null){
		cardDetails.setStatus("Failure");
		woohooCardDetailsRepository.save(cardDetails);
	}
	
}

@Override
public void successStatusWoohooPayment(WoohooTransactionResponse dto,PQTransaction transaction) {
	PQTransaction senderTransaction = getTransactionByRefNo(dto.getTransactionRefNo());
	if (senderTransaction != null) {
		senderTransaction.setCheckStatus(false);;
		transactionRepository.save(senderTransaction);
	}
	WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransaction(transaction);
	if(cardDetails!=null){
		dto.setEmail(cardDetails.getEmailId());
		dto.setName(cardDetails.getFirstName());
		cardDetails.setCard_price(dto.getCard_price());
		cardDetails.setCardName(dto.getCardName());
		cardDetails.setCardnumber(dto.getCardnumber());
		cardDetails.setExpiry_date(dto.getExpiry_date());
		cardDetails.setPin_or_url(dto.getPin_or_url());
		cardDetails.setOrderId(dto.getOrderId());
		cardDetails.setRetrivalRefNo(dto.getRetrivalRefNo());
		cardDetails.setStatus("Success");
		woohooCardDetailsRepository.save(cardDetails);
	}
	if (senderTransaction!= null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
				sender, senderTransaction, null);
		if(dto.getTermsAndConditions()!=null){
		mailSenderApi.sendwoohoogiftcardmail("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER,null, dto, sender);
		}
		else{
			mailSenderApi.sendwoohoogiftcardmail("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER_WITHOUT_TC,null, dto, sender);
		}
	}
}

@Override
public OrderPlaceResponse successWoohooPaymentNew(String transactionRefNo, PQCommission commission, double netCommissionValue,
		User user, OrderPlaceResponse response) {
	OrderPlaceResponse resp= new OrderPlaceResponse();
	// TODO Auto-generated method stub
	
	String senderMobileNumber = null;
	String receiverMobileNumber = null;
	PQService senderService = new PQService();
	double netTransactionAmount = 0;
	PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
	if (senderTransaction != null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		senderMobileNumber = sender.getUsername();
		senderService = senderTransaction.getService();
		netTransactionAmount = senderTransaction.getAmount();
		org.json.JSONObject json = null;
		String mobileNumber = null;
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			PQTransaction t= transactionRepository.save(senderTransaction);
			resp.setRemainingBalance(senderTransaction.getCurrentBalance());
			long accountNumber = senderTransaction.getAccount().getAccountNumber();
			long points = calculatePoints(netTransactionAmount);
			pqAccountDetailRepository.addUserPoints(points, accountNumber);
			WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
			if(cardDetails!=null){
				response.setEmail(cardDetails.getEmailId());
				response.setName(cardDetails.getFirstName());
				cardDetails.setCard_price(response.getCard_price());
				cardDetails.setCardName(response.getCardName());
				cardDetails.setCardnumber(response.getCardnumber());
				cardDetails.setExpiry_date(response.getExpiry_date());
				cardDetails.setPin_or_url(response.getPin_or_url());
				cardDetails.setOrderId(response.getOrderNumber());
				cardDetails.setRetrivalRefNo(response.getRetrivalRefNo());
				cardDetails.setStatus("Success");
				smsSenderApi.sendUserWoohooSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,SMSTemplate.WOOHOO_GIFTCARD_SMS,cardDetails,user);
				woohooCardDetailsRepository.save(cardDetails);
			}
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,sender, senderTransaction, null);
				if(response.getTermsAndConditions()!=null){
				mailSenderApi.sendwoohoogiftcardmailNew("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER,null, response, user);
				}else{
					mailSenderApi.sendwoohoogiftcardmailNew("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER_WITHOUT_TC,null, response, user);
				}
			}
		}
	}

	PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
	if (receiverTransaction != null) {
		if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			PQAccountDetail receiverAccount = receiver.getAccountDetail();
			double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
			receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
			receiverTransaction.setStatus(Status.Success);
			receiverTransaction.setLastModified(new Date());
			receiverTransaction.setCurrentBalance(receiverCurrentBalance);
			receiverAccount.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(receiverAccount);
			PQTransaction t = transactionRepository.save(receiverTransaction);

			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
						receiver, receiverTransaction, senderMobileNumber,null);
			}
		}
	}

	PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
	if (commissionTransaction != null) {
		if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
			User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
			commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
			commissionTransaction.setStatus(Status.Success);
			commissionTransaction.setLastModified(new Date());
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionAccountDetail.setBalance(commissionCurrentBalance);
			pqAccountDetailRepository.save(commissionAccountDetail);
			//					userApi.updateBalance(commissionCurrentBalance, commissionAccount);
			transactionRepository.save(commissionTransaction);
		}

	}
	return resp;
}
	

@Override
public void successStatusWoohooPaymentNew(OrderPlaceResponse dto, PQTransaction transaction) {
	// TODO Auto-generated method stub
	
	PQTransaction senderTransaction = getTransactionByRefNo(dto.getTransactionRefNo());
	if (senderTransaction != null) {
		senderTransaction.setCheckStatus(false);;
		transactionRepository.save(senderTransaction);
	}
	WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransaction(transaction);
	if(cardDetails!=null){
		dto.setEmail(cardDetails.getEmailId());
		dto.setName(cardDetails.getFirstName());
		cardDetails.setCard_price(dto.getCard_price());
		cardDetails.setCardName(dto.getCardName());
		cardDetails.setCardnumber(dto.getCardnumber());
		cardDetails.setExpiry_date(dto.getExpiry_date());
		cardDetails.setPin_or_url(dto.getPin_or_url());
		cardDetails.setOrderId(dto.getOrderNumber());
		cardDetails.setRetrivalRefNo(dto.getRetrivalRefNo());
		cardDetails.setStatus("Success");
		woohooCardDetailsRepository.save(cardDetails);
	}
	if (senderTransaction!= null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
				sender, senderTransaction, null);
		if(dto.getTermsAndConditions()!=null){
		mailSenderApi.sendwoohoogiftcardmailNew("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER,null, dto, sender);
		}
		else{
			mailSenderApi.sendwoohoogiftcardmailNew("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER_WITHOUT_TC,null, dto, sender);
		}
	}
	
}

@Override
public void pendingWoohooPayment(String transactionRefNo, PQCommission commission, double netCommissionValue, User user,
		WoohooTransactionResponse response) {
	// TODO Auto-generated method stub
	String senderMobileNumber = null;
	String receiverMobileNumber = null;
	PQService senderService = new PQService();
	double netTransactionAmount = 0;
	PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
	if (senderTransaction != null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		senderMobileNumber = sender.getUsername();
		senderService = senderTransaction.getService();
		netTransactionAmount = senderTransaction.getAmount();
		org.json.JSONObject json = null;
		String mobileNumber = null;
		if ((senderTransaction.getStatus().equals(Status.Initiated))) {
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setRetrivalReferenceNo(response.getRetrivalRefNo());
			senderTransaction.setCheckStatus(true);
			PQTransaction t= transactionRepository.save(senderTransaction);
			long accountNumber = senderTransaction.getAccount().getAccountNumber();
			long points = calculatePoints(netTransactionAmount);
			pqAccountDetailRepository.addUserPoints(points, accountNumber);
			WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
			if(cardDetails!=null){
				/*response.setEmail(cardDetails.getEmailId());
				response.setName(cardDetails.getFirstName());*/
				cardDetails.setCard_price(response.getCard_price());
				cardDetails.setCardName(response.getCardName());
				cardDetails.setCardnumber(response.getCardnumber());
				cardDetails.setExpiry_date(response.getExpiry_date());
				cardDetails.setPin_or_url(response.getPin_or_url());
				cardDetails.setOrderId(response.getOrderId());
				cardDetails.setRetrivalRefNo(response.getRetrivalRefNo());
				cardDetails.setStatus("Success");
				woohooCardDetailsRepository.save(cardDetails);
			}
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
						sender, senderTransaction, null);
			/*	if(response.getTermsAndConditions()!=null){
				mailSenderApi.sendwoohoogiftcardmail("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER,null, response, u);
				}
				else{
					mailSenderApi.sendwoohoogiftcardmail("VPayQwik Gift Card", MailTemplate.WOOHOO_SENDER_WITHOUT_TC,null, response, u);
				}*/
			}
		}
	}

	PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
	if (receiverTransaction != null) {
		if ((receiverTransaction.getStatus().equals(Status.Initiated))) {
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			PQAccountDetail receiverAccount = receiver.getAccountDetail();
			double receiverCurrentBalance = receiverTransaction.getCurrentBalance();
			receiverCurrentBalance = receiverCurrentBalance + netTransactionAmount - netCommissionValue;
			receiverTransaction.setStatus(Status.Success);
			receiverTransaction.setCurrentBalance(receiverCurrentBalance);
			receiverAccount.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(receiverAccount);
			PQTransaction t = transactionRepository.save(receiverTransaction);

			/*if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BILLPAYMENT_SUCCESS,
						receiver, receiverTransaction, null);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.BILLPAYMENT_SUCCESS,
						receiver, receiverTransaction, senderMobileNumber,null);
			}*/
		}
	}

	PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
	if (commissionTransaction != null) {
		if ((commissionTransaction.getStatus().equals(Status.Initiated))) {
			User commissionAccount = userApi.findByAccountDetail(commissionTransaction.getAccount());
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionTransaction.getCurrentBalance();
			commissionCurrentBalance = commissionCurrentBalance + netCommissionValue;
			commissionTransaction.setStatus(Status.Success);
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionAccountDetail.setBalance(commissionCurrentBalance);
			pqAccountDetailRepository.save(commissionAccountDetail);
			//					userApi.updateBalance(commissionCurrentBalance, commissionAccount);
			transactionRepository.save(commissionTransaction);
		}

	  }
	
   }

@Override
public void failedWoohooPaymentNew(String transactionRefNo) {
	PQCommission senderCommission = new PQCommission();
	PQService senderService = new PQService();
	PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
	if (senderTransaction != null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		senderService = senderTransaction.getService();
		senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
				senderTransaction.getAmount());
		double netTransactionAmount = senderTransaction.getAmount();
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount;
		}
		if ((senderTransaction.getStatus().equals(Status.Success)) &&(senderTransaction.isCheckStatus()) ) {
			List<PQTransaction> lastTransList = transactionRepository.getTotalSuccessTransactions(senderTransaction.getAccount());
			PQTransaction lastTrans = null;
			if (lastTransList != null && !lastTransList.isEmpty()) {
				lastTrans = lastTransList.get(lastTransList.size() - 1);
				senderCurrentBalance =  lastTrans.getCurrentBalance();
			}
			senderCurrentBalance = senderUserBalance + netTransactionAmount;
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setStatus(Status.Reversed);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
//			userApi.updateBalance(senderCurrentBalance, sender);
			transactionRepository.save(senderTransaction);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
					sender, senderTransaction, null);
			/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
					senderTransaction, null,null);*/
		}
	}
	PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
	if (receiverTransaction != null) {
		double receiverCurrentBalance = 0;
		if ((receiverTransaction.getStatus().equals(Status.Success))) {
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			PQAccountDetail receiverAccount=receiverTransaction.getAccount();
			receiverCurrentBalance= receiverAccount.getBalance()-receiverTransaction.getAmount();
			receiverAccount.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(receiverAccount);
			receiverTransaction.setStatus(Status.Failed);
			transactionRepository.save(receiverTransaction);
//			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.TRANSACTION_FAILED,
//					receiver, receiverTransaction, null);
			/*mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
					receiverTransaction, null,null);
*/		}
	}
	PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
	if (commissionTransaction != null) {
		if ((commissionTransaction.getStatus().equals(Status.Success))) {
			PQAccountDetail senderAccount = commissionTransaction.getAccount();
			double balance=commissionTransaction.getAmount();
			double accountBalnce=senderAccount.getBalance();
			double senderCurrentBalance = accountBalnce - balance;
			commissionTransaction.setStatus(Status.Failed);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(commissionTransaction);
		}
	}
	WoohooCardDetails cardDetails=woohooCardDetailsRepository.findByTransactionRefNo(transactionRefNo);
	if(cardDetails!=null){
		cardDetails.setStatus("Failure");
		woohooCardDetailsRepository.save(cardDetails);
	}
	
 }

   @Override
   public ResponseDTO processWoohooRefund(RefundDTO dto) {
	ResponseDTO result = new ResponseDTO();
	String transactionRef = dto.getTransactionRefNo();
	if(transactionRef != null){
		PQTransaction transaction = transactionRepository.findByTransactionRefNo(transactionRef);
		if(transaction != null){
			String serviceType = transaction.getService().getServiceType().getName();
			if(serviceType.equalsIgnoreCase("woohoo Gift")) {
				if(transaction.getStatus().equals(Status.Success)){
					// TODO credit into user wallet account
					String transactionId = transactionRef.substring(0,transactionRef.length()-1);
					processRefundWoohoo(transactionId);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Credited Successfully");
				}else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Transaction was "+transaction.getStatus());
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Not a TOPUP/BILL PAYMENT transaction");
			}

		}else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction not exists");
		}

	}else {
		result.setStatus(ResponseStatus.FAILURE);
		result.setMessage("Transaction ID must not be null");
	}

	return result;
}

   @Override
   public void processRefundWoohoo(String transactionRefNo) {
	ResponseDTO result = new ResponseDTO();
	String newTransactionRefNo = ""+System.currentTimeMillis();
	PQCommission senderCommission = new PQCommission();
	PQService senderService = new PQService();
	PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo+"D");
	PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
	if (senderTransaction != null) {
		User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
		senderService = senderTransaction.getService();
		double netTransactionAmount = senderTransaction.getAmount();
		senderCommission = commissionApi.findCommissionByIdentifier(senderTransaction.getCommissionIdentifier());
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		PQTransaction senderRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "C");
		if ((senderTransaction.getStatus().equals(Status.Success)) && (senderRefundTransaction == null)) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderRefundTransaction = new PQTransaction();
			senderCurrentBalance = senderAccount.getBalance() + netTransactionAmount;
			senderRefundTransaction.setCurrentBalance(senderCurrentBalance);
			senderRefundTransaction.setTransactionRefNo(newTransactionRefNo+"C");
			senderRefundTransaction.setDebit(false);
			senderRefundTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderRefundTransaction.setAmount(netTransactionAmount);
			senderRefundTransaction.setAccount(sender.getAccountDetail());
			senderRefundTransaction.setTransactionType(TransactionType.REFUND);
			senderRefundTransaction.setService(senderService);
			senderRefundTransaction.setStatus(Status.Success);
			senderRefundTransaction.setDescription("Refund of Amount "+netTransactionAmount+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
			senderTransaction.setCheckStatus(false);
			senderTransaction.setStatus(Status.Refunded);
			transactionRepository.save(senderTransaction);
			transactionRepository.save(senderRefundTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			//				userApi.updateBalance(senderCurrentBalance, sender);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
					sender, senderTransaction, null);
			mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
					senderTransaction, null,null);
		}
	}

/*		PQTransaction debitSettlementTransaction = getTransactionByRefNo(transactionRefNo + "CS");
	if (debitSettlementTransaction != null) {
		User debitSettlement = userApi.findByAccountDetail(debitSettlementTransaction.getAccount());
		PQAccountDetail accountDetail = debitSettlementTransaction.getAccount();
		double debitSettlementTransactionAmount = debitSettlementTransaction.getAmount();
		String debitSettlementDescription = debitSettlementTransaction.getDescription();
		double debitSettlementCurrentBalance = debitSettlementTransaction.getCurrentBalance();
		debitSettlementCurrentBalance = debitSettlementCurrentBalance - debitSettlementTransactionAmount;
		debitSettlementTransactionAmount = debitSettlementTransactionAmount - debitSettlementTransactionAmount;
		PQTransaction debitSettlementTransaction1 = new PQTransaction();
		PQTransaction settlementTransactionExists = getTransactionByRefNo(transactionRefNo + "DS");
		if (settlementTransactionExists == null) {
			debitSettlementTransaction1.setCommissionIdentifier(senderCommission.getIdentifier());
			debitSettlementTransaction1.setAmount(debitSettlementTransactionAmount);
			debitSettlementTransaction1.setCurrentBalance(debitSettlementCurrentBalance);
			debitSettlementTransaction1.setDescription(debitSettlementDescription);
			debitSettlementTransaction1.setService(senderService);
			debitSettlementTransaction1.setTransactionRefNo(newTransactionRefNo + "DS");
			debitSettlementTransaction1.setDebit(true);
			debitSettlementTransaction1.setAccount(accountDetail);
			debitSettlementTransaction1.setTransactionType(TransactionType.SETTLEMENT);
			debitSettlementTransaction1.setStatus(Status.Success);
			transactionRepository.save(debitSettlementTransaction1);
			accountDetail.setBalance(debitSettlementCurrentBalance);
			pqAccountDetailRepository.save(accountDetail);
			//				userApi.updateBalance(debitSettlementCurrentBalance, debitSettelment);
		}
	}*/

	PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
	PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
	if (receiverTransaction != null) {
		if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
			User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
			PQAccountDetail receiverAccount = receiver.getAccountDetail();
			double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
			receiverRefundTransaction = new PQTransaction();
			receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
			receiverRefundTransaction.setService(receiverTransaction.getService());
			receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
			receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
			receiverRefundTransaction.setDebit(true);
			receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
			receiverRefundTransaction.setAccount(receiverAccount);
			receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
			receiverRefundTransaction.setStatus(Status.Success);
			receiverRefundTransaction.setCurrentBalance(currentBalance);
			receiverTransaction.setStatus(Status.Refunded);
			transactionRepository.save(receiverTransaction);
			transactionRepository.save(receiverRefundTransaction);
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS_SENDER	,receiver, receiverTransaction, null);
			mailSenderApi.sendTransactionMail("VPayQwik Transaction", MailTemplate.TRANSACTION_FAILED, receiver,
					receiverTransaction, null,null);
		}
	}

	PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
	if (commissionTransaction != null) {
		if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
			refundCommissionTransaction = new PQTransaction();
			User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
			PQAccountDetail commissionAccount = commission.getAccountDetail();
			double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
			refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
			refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
			refundCommissionTransaction.setDescription("Commission Debited due to Refund of Send Money transaction with ID " + transactionRefNo);
			refundCommissionTransaction.setCurrentBalance(commissionBalance);
			refundCommissionTransaction.setAccount(commissionAccount);
			refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
			refundCommissionTransaction.setDebit(true);
			refundCommissionTransaction.setService(commissionTransaction.getService());
			refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
			refundCommissionTransaction.setStatus(Status.Success);

			commissionTransaction.setStatus(Status.Refunded);
			transactionRepository.save(commissionTransaction);
			transactionRepository.save(refundCommissionTransaction);
			commissionAccount.setBalance(commissionBalance);
			pqAccountDetailRepository.save(commissionAccount);
		}
	}
}
  
   @Override
	public Page<PQTransaction> getUPITxn(Pageable pageable, PQService upiService) {
		return transactionRepository.getUPITxn(pageable,upiService);
	}

	@Override
	public List<PQTransaction> getUPITxnList(PQService upiService, Date fromDate, Date toDate) {
		return transactionRepository.getUPITxn(upiService, fromDate, toDate);
	}

	@Override
	public User getAccountByAuthority(PQAccountDetail detail) {
		return userRepository.findByAccountDetails(detail);
	}
	
	
	@Override
	public List<PQTransaction> getSuspisiousTxn(Status success) {
		return transactionRepository.getSuspisiousTxn(success);
	}

	@Override
	public void updateSuspiciousTransaction() {
		DataConfig access = dataConfigRepository.getByStatus(Status.Active);
		if(access!=null){
		List<PQTransaction> txnList = getSuspisiousTxn(Status.Success);
		if (txnList != null && !txnList.isEmpty()) {
			for (PQTransaction pqTransaction : txnList) {
				try {
					String refNo = pqTransaction.getTransactionRefNo().substring(0,
								pqTransaction.getTransactionRefNo().length() - 1);
						TransactionResponse response = ipayTransactionApi.checkMdexStatus(refNo);
						if (response.isSuccess() && !response.isSuspicious()) {
							successSuspisiouseTxn(refNo);
						} else if(!response.isSuccess() && !response.isSuspicious()){
							failedSuspisiousTxn(refNo);
						}
				} catch (Exception e) {
					System.err.println("Error in walnut cron: " + e.getMessage());
				}
			}
		 }
	   }
	}
	
	
	
	@Override
	public void successSuspisiouseTxn(String refNo) {
		PQTransaction senderTransaction = getTransactionByRefNo(refNo + "D");
		if ((senderTransaction.getStatus().equals(Status.Success)) && senderTransaction.isSuspicious()) {
			senderTransaction.setSuspicious(false);
			PQTransaction t = transactionRepository.save(senderTransaction);
		}
	}

	@Override
	public void failedSuspisiousTxn(String transactionRefNo) {
		PQCommission senderCommission = new PQCommission();
		PQService senderService = new PQService();
		String newTransactionRefNo = ""+System.currentTimeMillis();
		PQTransaction senderTransaction = getTransactionByRefNo(transactionRefNo + "D");
		if (senderTransaction != null) {
			User sender = userApi.findByAccountDetail(senderTransaction.getAccount());
			senderService = senderTransaction.getService();
			senderCommission = commissionApi.findCommissionByServiceAndAmount(senderService,
					senderTransaction.getAmount());
			double netTransactionAmount = senderTransaction.getAmount();
			double netCommissionValue = commissionApi.getCommissionValue(senderCommission, netTransactionAmount);
			double senderCurrentBalance = 0;
			double senderUserBalance = sender.getAccountDetail().getBalance();

			if ((senderTransaction.getStatus().equals(Status.Success)) && senderTransaction.isSuspicious()) {
				senderCurrentBalance = senderUserBalance + netTransactionAmount;
				senderTransaction.setCurrentBalance(senderCurrentBalance);
				senderTransaction.setStatus(Status.Refunded);
				senderTransaction.setSuspicious(false);
				PQAccountDetail senderAccount = sender.getAccountDetail();
				senderAccount.setBalance(senderCurrentBalance);
				pqAccountDetailRepository.save(senderAccount);
				transactionRepository.save(senderTransaction);
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.REFUND_SUCCESS,
						sender, senderTransaction, null);
				mailSenderApi.sendTransactionMail("Vpayqwik Transaction", MailTemplate.TRANSACTION_FAILED, sender,
						senderTransaction, null,null);

				// Insert new Row.
				try {
					PQTransaction failedTxn = new PQTransaction();
					failedTxn.setCommissionIdentifier(senderCommission.getIdentifier());
					failedTxn.setAmount(netTransactionAmount);
					failedTxn.setDescription("Topup refund of transaction ref: "+senderTransaction.getTransactionRefNo());
					failedTxn.setService(senderTransaction.getService());
					failedTxn.setTransactionRefNo(newTransactionRefNo + "C");
					failedTxn.setDebit(false);
					failedTxn.setTransactionType(TransactionType.REFUND);
					failedTxn.setCurrentBalance(senderCurrentBalance);
					failedTxn.setAccount(sender.getAccountDetail());
					failedTxn.setStatus(Status.Success);
					failedTxn.setLastModified(new Date());
					failedTxn.setRetrivalReferenceNo(senderTransaction.getTransactionRefNo());
					failedTxn.setSuspicious(false);
					transactionRepository.save(failedTxn);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		PQTransaction receiverTransaction = getTransactionByRefNo(transactionRefNo + "C");
		PQTransaction receiverRefundTransaction = getTransactionByRefNo(newTransactionRefNo + "D");
		if (receiverTransaction != null) {
			if ((receiverTransaction.getStatus().equals(Status.Success)) && (receiverRefundTransaction == null)) {
				User receiver = userApi.findByAccountDetail(receiverTransaction.getAccount());
				PQAccountDetail receiverAccount = receiver.getAccountDetail();
				double currentBalance = receiverAccount.getBalance() - receiverTransaction.getAmount();
				receiverRefundTransaction = new PQTransaction();
				receiverRefundTransaction.setAmount(receiverTransaction.getAmount());
				receiverRefundTransaction.setService(receiverTransaction.getService());
				receiverRefundTransaction.setTransactionType(TransactionType.REFUND);
				receiverRefundTransaction.setTransactionRefNo(newTransactionRefNo + "D");
				receiverRefundTransaction.setDebit(true);
				receiverRefundTransaction.setDescription("Refund of Amount "+receiverTransaction.getAmount()+" , "+ senderService.getDescription() +" Transaction ID "+transactionRefNo);
				receiverRefundTransaction.setAccount(receiverAccount);
				receiverRefundTransaction.setSuspicious(false);
				receiverRefundTransaction.setCommissionIdentifier(receiverTransaction.getCommissionIdentifier());
				receiverRefundTransaction.setStatus(Status.Success);
				receiverRefundTransaction.setLastModified(new Date());
				receiverRefundTransaction.setCurrentBalance(currentBalance);
				receiverTransaction.setStatus(Status.Refunded);
				receiverTransaction.setSuspicious(false);
				transactionRepository.save(receiverTransaction);
				transactionRepository.save(receiverRefundTransaction);
				receiverAccount.setBalance(currentBalance);
				pqAccountDetailRepository.save(receiverAccount);
			}
		}

		PQTransaction commissionTransaction = getTransactionByRefNo(transactionRefNo + "CC");
		PQTransaction refundCommissionTransaction = getTransactionByRefNo(newTransactionRefNo + "DC");
		if (commissionTransaction != null) {
			if ((commissionTransaction.getStatus().equals(Status.Success)) && (refundCommissionTransaction == null)) {
				refundCommissionTransaction = new PQTransaction();
				User commission = userApi.findByAccountDetail(commissionTransaction.getAccount());
				PQAccountDetail commissionAccount = commission.getAccountDetail();
				double commissionBalance = commissionAccount.getBalance() - commissionTransaction.getAmount() ;
				refundCommissionTransaction.setCommissionIdentifier(commissionTransaction.getCommissionIdentifier());
				refundCommissionTransaction.setAmount(commissionTransaction.getAmount());
				refundCommissionTransaction.setDescription("Commission Debited due to Refund of Send Money transaction with ID " + transactionRefNo);
				refundCommissionTransaction.setCurrentBalance(commissionBalance);
				refundCommissionTransaction.setAccount(commissionAccount);
				refundCommissionTransaction.setTransactionRefNo(newTransactionRefNo + "DC");
				refundCommissionTransaction.setDebit(true);
				refundCommissionTransaction.setSuspicious(false);
				refundCommissionTransaction.setService(commissionTransaction.getService());
				refundCommissionTransaction.setTransactionType(TransactionType.COMMISSION);
				refundCommissionTransaction.setStatus(Status.Success);
				refundCommissionTransaction.setLastModified(new Date());
				commissionTransaction.setStatus(Status.Refunded);
				commissionTransaction.setSuspicious(false);
				transactionRepository.save(commissionTransaction);
				transactionRepository.save(refundCommissionTransaction);
				commissionAccount.setBalance(commissionBalance);
				pqAccountDetailRepository.save(commissionAccount);
			}
		}
	}
	
	
	@Override
	public void successCanteenMerchantPayment(double amount,String description,String remarks,PQService service,String transactionRefNo,
			String senderUsername, String receiverUsername,boolean isPG) {
		User sender = userApi.findByUserName(senderUsername);
		User merchant = userApi.findByUserName(receiverUsername);
		User commissionAccount = userApi.findByUserName("commission@vpayqwik.com");
		PQCommission receiverCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(receiverCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double roundSenderCurrentBalance=0;
		double senderUserBalance = sender.getAccountDetail().getBalance();
		if (receiverCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount - netCommissionValue;
		}
		
		if(isPG){
			User  stateGST = userApi.findByUserName(StartupUtil.STATE_GST);
			PQAccountDetail stateAccount = stateGST.getAccountDetail();
			PQService sgstService = pqServiceRepository.findServiceByCode("SGST");
			PQCommission sgstCommission = commissionApi.findCommissionByServiceAndAmount(sgstService,netCommissionValue);
			double sgstValue = commissionApi.getCommissionValue(sgstCommission,netCommissionValue);
			netTransactionAmount = netTransactionAmount - sgstValue;

			//  create transaction for sgst

			PQTransaction sgstTransaction = getTransactionByRefNo(transactionRefNo+"SGST");
			if(sgstTransaction == null) {
				sgstTransaction = new PQTransaction();
				sgstTransaction.setDescription(description);
				sgstTransaction.setStatus(Status.Success);
				sgstTransaction.setLastModified(new Date());
				sgstTransaction.setTransactionType(TransactionType.SGST);
				sgstTransaction.setService(service);
				sgstTransaction.setAmount(sgstValue);
				sgstTransaction.setAccount(stateAccount);
				sgstTransaction.setTransactionRefNo(transactionRefNo + "SGST");
				sgstTransaction.setDebit(false);
				sgstTransaction.setCurrentBalance(stateAccount.getBalance() + sgstValue);
				transactionRepository.save(sgstTransaction);
			}

			User cgstUser = userApi.findByUserName(StartupUtil.CENTRAL_GST);
			PQAccountDetail cgstAccount = cgstUser.getAccountDetail();
			PQService cgstService = pqServiceRepository.findServiceByCode("CGST");
			PQCommission cgstCommission = commissionApi.findCommissionByServiceAndAmount(cgstService,netCommissionValue);
			double cgstValue = commissionApi.getCommissionValue(cgstCommission,netCommissionValue);
			netTransactionAmount = netTransactionAmount - cgstValue;

			//  create transaction for cgst

			PQTransaction cgstTransaction = getTransactionByRefNo(transactionRefNo+"CGST");
			if(cgstTransaction == null) {
				cgstTransaction = new PQTransaction();
				cgstTransaction.setDescription(description);
				cgstTransaction.setStatus(Status.Success);
				cgstTransaction.setLastModified(new Date());
				cgstTransaction.setTransactionType(TransactionType.CGST);
				cgstTransaction.setService(service);
				cgstTransaction.setAmount(cgstValue);
				cgstTransaction.setAccount(cgstAccount);
				cgstTransaction.setTransactionRefNo(transactionRefNo + "CGST");
				cgstTransaction.setDebit(false);
				cgstTransaction.setCurrentBalance(cgstAccount.getBalance() + cgstValue);
				transactionRepository.save(cgstTransaction);
			}
		}
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderCurrentBalance = senderUserBalance - amount;
			roundSenderCurrentBalance=BigDecimal.valueOf(senderCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue();
			senderTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setRemarks(remarks);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(roundSenderCurrentBalance);
			senderTransaction.setAccount(sender.getAccountDetail());
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			senderAccount.setBalance(roundSenderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
			PQTransaction t = transactionRepository.save(senderTransaction);
			long accountNumber = senderTransaction.getAccount().getAccountNumber();
			long points = calculatePoints(netTransactionAmount);
			pqAccountDetailRepository.addUserPoints(points, accountNumber);
			if (t != null) {
				smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,SMSTemplate.MERCHANT_SENDER,sender, senderTransaction,receiverUsername);
				mailSenderApi.sendTransactionMail("VPayQwik Transaction",MailTemplate.MERCHANT_SENDER_CANTEEN, sender,senderTransaction,receiverUsername, MailConstants.CC_MAIL);
			}
		}
		
		PQTransaction merchantTransaction = new PQTransaction();
		PQTransaction merchantTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (merchantTransactionExists == null) {
			double receiverTransactionAmount = netTransactionAmount;
			double receiverCurrentBalance = merchant.getAccountDetail().getBalance();
			double roundReceiverCurrentBalance=BigDecimal.valueOf(receiverCurrentBalance).setScale(2, RoundingMode.HALF_UP).doubleValue();
			double updateCurrentBalance=roundReceiverCurrentBalance+receiverTransactionAmount;
			merchantTransaction.setCurrentBalance(updateCurrentBalance);
			merchantTransaction.setAmount(receiverTransactionAmount);
			merchantTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			merchantTransaction.setDescription(description);
			merchantTransaction.setService(service);
			merchantTransaction.setAccount(merchant.getAccountDetail());
			merchantTransaction.setTransactionRefNo(transactionRefNo + "C");
			merchantTransaction.setDebit(false);
			merchantTransaction.setStatus(Status.Success);
			merchantTransaction.setLastModified(new Date());
			transactionRepository.save(merchantTransaction);
			PQAccountDetail receiverAccount = merchant.getAccountDetail();
			receiverAccount.setBalance(updateCurrentBalance);
			pqAccountDetailRepository.save(receiverAccount);
		}

		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commissionAccount.getAccountDetail().getBalance();
			double commissionAmount=commissionCurrentBalance + netCommissionValue;
			commissionTransaction.setCurrentBalance(BigDecimal.valueOf(commissionAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(receiverCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Success);
			commissionTransaction.setLastModified(new Date());
			transactionRepository.save(commissionTransaction);
			PQAccountDetail accountDetail = commissionAccount.getAccountDetail();
			accountDetail.setBalance(commissionCurrentBalance);
			pqAccountDetailRepository.save(accountDetail);
		}
	}
	
	
	@Override
	public void successMBankTransfer(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername, String json) {

		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = sender.getAccountDetail().getBalance();

		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}

		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(netTransactionAmount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			PQAccountDetail senderAccount = sender.getAccountDetail();
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			senderTransaction.setRequest(json);
			transactionRepository.save(senderTransaction);
			senderAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(senderAccount);
		}
		User bankAccount = userApi.findByUserName(receiverUsername);
		PQTransaction bankTransaction = new PQTransaction();
		PQTransaction bankTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		if (bankTransactionExists == null) {
			double receiverTransactionAmount = amount;
			PQAccountDetail bankAccountDetail = bankAccount.getAccountDetail();
			double receiverCurrentBalance = bankAccount.getAccountDetail().getBalance();
			bankTransaction.setCurrentBalance(receiverCurrentBalance + receiverTransactionAmount);
			bankTransaction.setAmount(receiverTransactionAmount);
			bankTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			bankTransaction.setDescription(description);
			bankTransaction.setService(service);
			bankTransaction.setAccount(bankAccount.getAccountDetail());
			bankTransaction.setTransactionRefNo(transactionRefNo + "C");
			bankTransaction.setDebit(false);
			bankTransaction.setStatus(Status.Success);
			bankTransaction.setLastModified(new Date());
			transactionRepository.save(bankTransaction);
			receiverCurrentBalance = receiverCurrentBalance + receiverTransactionAmount;
			bankAccountDetail.setBalance(receiverCurrentBalance);
			pqAccountDetailRepository.save(bankAccountDetail);
		}

		User commissionAccount = userApi.findByUserName(StartupUtil.COMMISSION);
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		if (commissionTransactionExists == null) {
			PQAccountDetail commissionAccountDetail = commissionAccount.getAccountDetail();
			double commissionCurrentBalance = commissionAccountDetail.getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commissionAccount.getAccountDetail());
			commissionTransaction.setStatus(Status.Success);
			commissionTransaction.setLastModified(new Date());
			transactionRepository.save(commissionTransaction);
		}
	}

	@Override
	public void findTotalCommisionByMonthWise() throws Exception {
	   String  date = "2016/09/16";
	   String date2 = "2016/09/31";
	   Date from = new Date();
	   from = dateOnly.parse(date);
	  
	   Date to = new Date();
	   to = dateOnly.parse(date2);
	   
	   List<PQServiceType>pqServiceType = (List<PQServiceType>) pqServiceTypeRepository.findAll();
	   for(int i=0;i<pqServiceType.size();i++){
		   List<PQService>pqService = pqServiceRepository.findServiceByServiceTypeID(pqServiceType.get(i).getId());
		   System.out.println("pqService size="+pqService.size());
		   
		   Double amount  = transactionRepository.getTransactionServiceWiseMonthly(pqService,from,to,TransactionType.COMMISSION,Status.Success);
		   System.out.println("amount=="+amount);
	   }
	}

	
	@Override
	public void findTotalUserMonthWise() throws Exception{
		 String  date = "2017-12-01";
		   String date2 = "2017-12-31";
		   Date from = new Date();
		   from = dateOnly.parse(date);
		   
		   Date to = new Date();
		   to = dateOnly.parse(date2);
		   String COMMA_DELIMITIER = ",";
		   String NEW_LINE_SEPARATOR = "\n";
		   String FILE_HEADER = "UserName,Total_Transaction";
		   FileWriter writer = new FileWriter("C:\\Users\\Piyush\\Desktop\\UserReport\\2017-12-01-To-2017-12-31.csv");
		   writer.append(FILE_HEADER);
		   
		   List<User>totalUser = userRepository.findActiveUsersByDate(from, to);
		   System.out.println("TOTAL Active USER=="+totalUser.size());
		   for(int i=0;i<totalUser.size();i++){
			   List<PQTransaction>totalTransaction = transactionRepository.findAllTransactionByUserAndStatusAndType(totalUser.get(i).getAccountDetail(), Status.Success);
			//   System.out.println((i+1)+" Total Transaction=="+totalTransaction.size()+" of Username="+totalUser.get(i).getUsername()+" Name="+totalUser.get(i).getUserDetail().getName());
		   	
		   	   writer.append(NEW_LINE_SEPARATOR);
			   writer.append(totalUser.get(i).getUsername());
			   writer.append(COMMA_DELIMITIER);
			   if(totalTransaction!=null){
				   writer.append(""+totalTransaction.size());
			   }
			   else{
				   writer.append("NA");
			   }
		   }
		   writer.flush();
		   writer.close();
	}
	@Override
	public void SendMoneyPredictUsers(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername) {
		User sender = userApi.findByUserName(senderUsername);
		PQAccountDetail senderAccount = sender.getAccountDetail();
		PQCommission senderCommission = commissionApi.findCommissionByServiceAndAmount(service, amount);
		double netCommissionValue = commissionApi.getCommissionValue(senderCommission, amount);
		double netTransactionAmount = amount;
		double senderCurrentBalance = 0;
		double senderUserBalance = senderAccount.getBalance();
		/*TODO calculate transaction amount commission */
		if (senderCommission.getType().equalsIgnoreCase("POST")) {
			netTransactionAmount = netTransactionAmount + netCommissionValue;
		}
		/*TODO create a transaction log for sender */
		PQTransaction senderTransaction = new PQTransaction();
		PQTransaction exists = getTransactionByRefNo(transactionRefNo + "D");
		if (exists == null) {
			senderCurrentBalance = senderUserBalance - netTransactionAmount;
			senderTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			senderTransaction.setAmount(amount);
			senderTransaction.setDescription(description);
			senderTransaction.setService(service);
			senderTransaction.setTransactionRefNo(transactionRefNo + "D");
			senderTransaction.setDebit(true);
			senderTransaction.setCurrentBalance(senderCurrentBalance);
			senderTransaction.setAccount(senderAccount);
			senderTransaction.setStatus(Status.Success);
			senderTransaction.setLastModified(new Date());
			/*TODO debit amount from sender */
			senderAccount.setBalance(senderCurrentBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(senderAccount);
			transactionRepository.save(senderTransaction);
		}

		User receiver = userApi.findByUserName(receiverUsername);
		PQTransaction receiverTransaction = new PQTransaction();
		PQTransaction receiverTransactionExists = getTransactionByRefNo(transactionRefNo + "C");
		PQAccountDetail receiverAccount = receiver.getAccountDetail();
		PredictAndWinPayment file=predictAndWinRepository.findByMobileNumberWithSatus(receiver.getUsername(),Status.Open);
		/*TODO create transaction log for receiver*/
		if (receiverTransactionExists == null) {
			double receiverTransactionAmount = 0;
			if (senderCommission.getType().equalsIgnoreCase("POST")) {
				receiverTransactionAmount = netTransactionAmount - netCommissionValue;
			}else{
				receiverTransactionAmount = netTransactionAmount;
			}
			double receiverCurrentBalance = receiverAccount.getBalance();
			double receiverTotalBalance = receiverCurrentBalance + receiverTransactionAmount;
			receiverTransaction.setCurrentBalance(receiverTotalBalance);
			receiverTransaction.setAmount(receiverTransactionAmount);
			receiverTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			receiverTransaction.setDescription(description);
			receiverTransaction.setService(service);
			receiverTransaction.setAccount(receiverAccount);
			receiverTransaction.setTransactionRefNo(transactionRefNo + "C");
			receiverTransaction.setDebit(false);
			receiverTransaction.setStatus(Status.Success);
			receiverTransaction.setLastModified(new Date());
			file.setStatus(Status.Close);
			predictAndWinRepository.save(file);
			PQTransaction t =transactionRepository.save(receiverTransaction);
			receiverAccount.setBalance(receiverTotalBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(receiverAccount);
			if (t != null) {
					smsSenderApi.sendBulkFileSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,SMSTemplate.PREDICT_PAYMENT_SUCCESS, receiver, receiverTransaction,
							senderUsername);
					/*mailSenderApi.sendBulkUploadMail("VPayQwik Transaction",
							MailTemplate.BULKUPLOAD_PAYMENT_SUCCESS, receiver, receiverTransaction,
							senderUsername,null);*/
			}
		}

		User commission = userApi.findByUserName(StartupUtil.COMMISSION);
		/*TODO create commission log */
		PQTransaction commissionTransaction = new PQTransaction();
		PQTransaction commissionTransactionExists = getTransactionByRefNo(transactionRefNo + "CC");
		PQAccountDetail commissionAccount = commission.getAccountDetail();
		if (commissionTransactionExists == null) {
			double commissionCurrentBalance = commission.getAccountDetail().getBalance();
			commissionTransaction.setCurrentBalance(commissionCurrentBalance);
			commissionTransaction.setAmount(netCommissionValue);
			commissionTransaction.setCommissionIdentifier(senderCommission.getIdentifier());
			commissionTransaction.setDescription(description);
			commissionTransaction.setService(service);
			commissionTransaction.setTransactionRefNo(transactionRefNo + "CC");
			commissionTransaction.setDebit(false);
			commissionTransaction.setTransactionType(TransactionType.COMMISSION);
			commissionTransaction.setAccount(commission.getAccountDetail());
			commissionTransaction.setStatus(Status.Success);
			commissionTransaction.setLastModified(new Date());
			transactionRepository.save(commissionTransaction);
			commissionAccount.setBalance(commissionCurrentBalance);
			PQAccountDetail updatedDetails = pqAccountDetailRepository.save(receiverAccount);
		 }
	}
	
	@Override
	public int addVouchers(AddVoucherDTO dto) {
		int count=0;
		Date expDate=null;
		try {
			expDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getExpiryDate());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		try{
			for (int i = 0; i < dto.getVoucherQty(); i++) {
				long voucherNo=System.currentTimeMillis();
				Random random=new Random();
				int x = random.nextInt(900) + 100;
				String str=voucherNo+""+x;
				logger.info("X is:: "+x);

				MessageDigest m = MessageDigest.getInstance("MD5");

				byte[] data = str.getBytes();

				m.update(data, 0, data.length);

				BigInteger j = new BigInteger(1, m.digest());

				String hash = String.format("%1$032X", j);

				logger.info("Hash is:: "+hash.length());
				hash=hash.substring(0, 16);
				logger.info("Hash is:: "+hash);
				if (hash.length()==16) {
					Voucher voucher=new Voucher();
					voucher.setAmount(dto.getVoucherAmount());
					voucher.setExpiryDate(expDate);
					voucher.setVoucherNo(hash);
					voucherRepository.save(voucher);
					count++;	
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public Voucher getVoucher(String voucherNo) {
		Voucher voucher=voucherRepository.getVoucherByNumber(voucherNo);
		return voucher;
	}

	@Override
	public ResponseDTO loadMoneyProcess(LoadMoneyRequest dto,User user, PQService service) {
		ResponseDTO result=new ResponseDTO();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();
		Date todayWithZeroTime = null;
		try {
			todayWithZeroTime = formatter.parse(formatter.format(today));
		Voucher voucher=voucherRepository.checkVoucherIsExpired(dto.getVoucherNumber());
		if(voucher!=null){
		Date voucherExp = null;
		try {
			voucherExp = formatter.parse(formatter.format(voucher.getExpiryDate()));
		} catch (ParseException e) {
			e.printStackTrace();
			result.setMessage("Date parsing exception");
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
			return result;
		}
		logger.info("voucher expiry--"+voucherExp);
		logger.info("current date--"+todayWithZeroTime);
		if(voucherExp!=null && todayWithZeroTime!=null){
		if (todayWithZeroTime.compareTo(voucherExp) > 0) {
			result.setMessage("Voucher is expired");
			result.setDetails("Voucher is expired");
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
			voucher.setExpired(true);
			voucherRepository.save(voucher);
			return result;
		}
		if (!voucher.isExpired() && !voucher.isRedeemed()) {
			PQAccountDetail pqActiveAccount=pqAccountDetailRepository.findAccount(user.getAccountDetail().getId());
			double senderUserBalance = pqActiveAccount.getBalance();
			double senderCurrentBalance = senderUserBalance + voucher.getAmount();
			pqActiveAccount.setBalance(senderCurrentBalance);
			pqAccountDetailRepository.save(pqActiveAccount);

			PQTransaction receiverTransaction=new PQTransaction();
			receiverTransaction.setTransactionRefNo(System.currentTimeMillis()+"C");
			receiverTransaction.setService(service);
			receiverTransaction.setAccount(pqActiveAccount);
			receiverTransaction.setDescription("Load Money using Voucher");
			receiverTransaction.setDebit(false);
			receiverTransaction.setAmount(voucher.getAmount());
			receiverTransaction.setCurrentBalance(senderCurrentBalance);
			receiverTransaction.setStatus(Status.Success);
			logger.error("Load Money Success And Updated Balance " + senderCurrentBalance);
			PQTransaction t = transactionRepository.save(receiverTransaction);

			voucher.setRedeemed(true);
			voucher.setExpired(true);
			voucher.setPqTransaction(t);
			voucherRepository.save(voucher);
			
			smsSenderApi.sendTransactionSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.LOADMONEY_SUCCESS, user,
					receiverTransaction, null);
			mailSenderApi.sendTransactionMail("eCarib Transaction", MailTemplate.LOADMONEY_SUCCESS, user,
					receiverTransaction, null,null);
			
			result.setMessage("Load Money successfully");
			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setDetails(voucher.getAmount());
			result.setCode(ResponseStatus.SUCCESS.getValue());
		}else {
			result.setMessage("Voucher is expired");
			result.setDetails(voucher.getAmount());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
		}
		}else{
			result.setMessage("Invalid voucher expiry");
			result.setDetails(voucher.getAmount());
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
		}
		}else{
			result.setMessage("Invalid voucher");
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
		}
		} catch (ParseException e) {
			e.printStackTrace();
			result.setMessage("Unable to process loadMoney through voucher");
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
		}
		return result;
	}

	
	@Override
	public Page<Voucher> getAllVouchers(Pageable pageable) {
		Page<Voucher> voucher= voucherRepository.findAll(pageable);
		return voucher;
	}
	
}

