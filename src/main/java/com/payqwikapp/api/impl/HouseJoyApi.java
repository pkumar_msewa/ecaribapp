package com.payqwikapp.api.impl;

import java.util.Date;
import java.util.List;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.IHouseJoyApi;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.HouseJoy;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.HouseJoyRequest;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.HouseJoyRepository;
import com.payqwikapp.util.StartupUtil;

public class HouseJoyApi implements IHouseJoyApi{

	private final ITransactionApi transactionApi;
	private final IMailSenderApi mailSenderApi;
	private final ISMSSenderApi smsSenderApi;
	private final HouseJoyRepository houseJoyRepository;

	public HouseJoyApi(ITransactionApi transactionApi, IMailSenderApi mailSenderApi,
			ISMSSenderApi smsSenderApi,HouseJoyRepository houseJoyRepository) {
		super();
		this.transactionApi = transactionApi;
		this.mailSenderApi = mailSenderApi;
		this.smsSenderApi = smsSenderApi;
		this.houseJoyRepository=houseJoyRepository;
	}

	@Override
	public String initiateHouseJoyPayment(HouseJoyRequest dto, User user, PQService service) {
		String transactionRefNo = System.currentTimeMillis() + "";
		PQTransaction pqTransaction=transactionApi.initiateHouseJoyPayment(Double.parseDouble(dto.getAmount()),
				" HouseJoy of Rs " + dto.getAmount()  , service, transactionRefNo, user.getUsername(),
				StartupUtil.HOUSE_JOY);
		HouseJoy joy=new HouseJoy();

		if (dto.getAmount()!=null && !dto.getAmount().isEmpty()) {

			joy.setAmount(Double.parseDouble(dto.getAmount()));
		}
		joy.setServiceName(dto.getServiceName());
		joy.setTransaction(pqTransaction);
		joy.setUser(user);
		
		houseJoyRepository.save(joy);

		return transactionRefNo;
	}

	@Override
	public TransactionResponse successHouseJoyPayment(HouseJoyRequest dto, PQService service,
			User user) {
		
		TransactionResponse resp=new TransactionResponse();
		HouseJoy joy=new HouseJoy();
		
		if (dto.isPaymentOffline()) {

			PQTransaction transaction= transactionApi.getTransactionByRefNo(dto.getTransactionRefNo()+"D");

			if (transaction!=null && transaction.getStatus().getValue().equals(Status.Initiated.getValue())) {

				if(dto.getCode().equalsIgnoreCase("S00")){
					transactionApi.successHouseJoyPayment(dto.getTransactionRefNo(),dto.getJobId());
					//mailSenderApi.sendQwikrPayMail("VPayQwik Gift Card", MailTemplate.QWIK_PAY, dto, user);
					//smsSenderApi.sendUserQWIKRSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.QWIK_PAY, user,  dto);
					resp.setSuccess(true);
					joy=houseJoyRepository.getByTransaction(transaction);
					
					
					joy.setServiceName(dto.getServiceName());
					joy.setHjtxnRefNo(dto.getHjtxnRefNo());
					joy.setJobId(dto.getJobId());
					joy.setCustomerName(dto.getCustomerName());
					joy.setDate(dto.getDate());
					joy.setMobile(dto.getMobile());
					joy.setPaymentType("Pay by wallet");
					joy.setTime(dto.getTime());
					joy.setPqtxnRefNo(dto.getTransactionRefNo());
					joy.setUser(user);
					houseJoyRepository.save(joy);
					
				} else {
					transactionApi.failedQwikrPayPayment(dto.getTransactionRefNo());
					resp.setSuccess(false);
					resp.setDetails("Payment failed");
				}
			}
			else {
				resp.setSuccess(false);
				resp.setDetails("Transaction not found");
			}
		}
		else {
			
			if (dto.getAmount()!=null && !dto.getAmount().isEmpty()) {

				joy.setAmount(Double.parseDouble(dto.getAmount()));
			}
			joy.setServiceName(dto.getServiceName());
			joy.setHjtxnRefNo(dto.getHjtxnRefNo());
			joy.setJobId(dto.getJobId());
			joy.setCustomerName(dto.getCustomerName());
			joy.setDate(dto.getDate());
			joy.setMobile(dto.getMobile());
			joy.setPaymentType("Pay by cash");
			joy.setTime(dto.getTime());
			joy.setUser(user);
			
			houseJoyRepository.save(joy);
			resp.setSuccess(true);
		}
		return resp;
	}

	
	@Override
	public TransactionResponse cancelHouseJoyPayment(String txnRefNo) {
		TransactionResponse resp=new TransactionResponse();
		
		transactionApi.failedQwikrPayPayment(txnRefNo);
		resp.setSuccess(true);
		resp.setDetails("Payment Canceld");
		return resp;
	}
	
	
	
	@Override
	public List<HouseJoy> getHouseJoyForAdminByDate(Date from, Date to) {
		
		List<HouseJoy> houseJoyDetails=(List<HouseJoy>)houseJoyRepository.getAllHouseJoyDetailsByDate(from, to);
		return houseJoyDetails;
	}
	
	@Override
	public List<HouseJoy> getHouseJoyForAdmin() {
		
		List<HouseJoy> houseJoyDetails=(List<HouseJoy>)houseJoyRepository.getAllHouseJoyDetails();
		return houseJoyDetails;
	}
	
	
	
}
