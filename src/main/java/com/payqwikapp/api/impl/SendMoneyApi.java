package com.payqwikapp.api.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.ABankTransfer;
import com.payqwikapp.entity.BankDetails;
import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.Banks;
import com.payqwikapp.entity.MBankTransfer;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PPIFiles;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.SenderBankTransferInfo;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.NeftRequestDTO;
import com.payqwikapp.model.PPIRequestDTO;
import com.payqwikapp.model.PayAtStoreDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyDonateeDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.SendMoneyResponse;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UploadResponse;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.ABankTransferRepository;
import com.payqwikapp.repositories.BankDetailRepository;
import com.payqwikapp.repositories.BankTransferRepository;
import com.payqwikapp.repositories.BanksRepository;
import com.payqwikapp.repositories.MBankTransferRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PPIFilesRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.SenderBankTransferInfoRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ClientException;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.FTPUtil;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.StartupUtil;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;
import com.thirdparty.util.FRMConstants;

public class SendMoneyApi implements ISendMoneyApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final static SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//	private final String dir = "D:/OFFICE PROJECTS/office project/APP/APP/WebContent/WEB-INF/banktransfer/";
	private final String dir = StartupUtil.SEND_MONEY_FILE;
	private final String path = dir ;
	private MessageSource messageSource;
	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final BankTransferRepository bankTransferRepository;
	private final BanksRepository banksRepository;
	private final BankDetailRepository bankDetailRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final PQServiceRepository pqServiceRepository;
	private final MBankTransferRepository mBankTransferRepository;
	private final ABankTransferRepository aBankTransferRepository;
	private final SenderBankTransferInfoRepository senderBankTransferInfoRepository;
	private final PQTransactionRepository pqTransactionRepository;
	private final IMailSenderApi mailSenderApi;
	private final ISMSSenderApi smsSenderApi;
	private final PPIFilesRepository pPIFilesRepository;
	private final UserRepository userRepository;
	private final IFRMApi frmApi;

	public SendMoneyApi(ITransactionApi transactionApi, IUserApi userApi, BankTransferRepository bankTransferRepository,
			BanksRepository banksRepository, BankDetailRepository bankDetailRepository,
			PGDetailsRepository pgDetailsRepository, PQServiceRepository pqServiceRepository,
			MBankTransferRepository mBankTransferRepository, ABankTransferRepository aBankTransferRepository,
			SenderBankTransferInfoRepository senderBankTransferInfoRepository,
			PQTransactionRepository pqTransactionRepository, IMailSenderApi mailSenderApi, ISMSSenderApi smsSenderApi,
			PPIFilesRepository pPIFilesRepository, UserRepository userRepository, IFRMApi frmApi) {
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.bankTransferRepository = bankTransferRepository;
		this.banksRepository = banksRepository;
		this.bankDetailRepository = bankDetailRepository;
		this.pgDetailsRepository = pgDetailsRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.mBankTransferRepository = mBankTransferRepository;
		this.aBankTransferRepository = aBankTransferRepository;
		this.senderBankTransferInfoRepository = senderBankTransferInfoRepository;
		this.pqTransactionRepository = pqTransactionRepository;
		this.mailSenderApi = mailSenderApi;
		this.smsSenderApi = smsSenderApi;
		this.pPIFilesRepository = pPIFilesRepository;
		this.userRepository = userRepository;
		this.frmApi = frmApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public ResponseDTO sendMoneyMobile(SendMoneyMobileDTO dto, String username, PQService service) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		ResponseDTO result=new ResponseDTO();
		try {
			String transactionRefNo = System.currentTimeMillis() + "";
			frmResponse.setSuccess(true);
			dto.setTransactionrefno(transactionRefNo);
			dto.setServiceCode(service.getCode());
			User u = userApi.findByUserName(username);
			if (DeploymentConstants.PRODUCTION) {
				logger.info("frm api calling from app server");
				frmResponse = frmApi.decideFundTransferWallet(dto, u);
				logger.info(" success response ::" + frmResponse.toString());
			}
				if (frmResponse.isSuccess()) {
					transactionApi.transferMoneyToWallet(Double.parseDouble(dto.getAmount()),
							"Send Money Rs " + dto.getAmount() + " to " + dto.getMobileNumber() + " from " + username
									+ "|" + dto.getMessage(),
							service, transactionRefNo, username, dto.getMobileNumber());
					userApi.saveSendMoneyRequest(dto, transactionRefNo);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Sent money to mobile successful. Money Sent to "+ dto.getMobileNumber());
					result.setDetails("Money Sent to " + dto.getMobileNumber());
					result.setBalance(userApi.getWalletBalance(u));
				}else{
					logger.info("failure response ::" + frmResponse.toString());
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Cause of some security reason,Your Transaction has been blocked.");
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public String sendMoneyMobileBulkFile(SendMoneyMobileDTO dto, String username, PQService service) {
		
		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.initiateSendMoney(
				Double.parseDouble(dto.getAmount()), "Canteen allowance Rs " + dto.getAmount() + " to " + dto.getMobileNumber()
						+ "|" + dto.getMessage(),
				service, transactionRefNo, username, dto.getMobileNumber());
		userApi.saveSendMoneyRequest(dto, transactionRefNo);
		transactionApi.successSendMoneyForBulkFile(transactionRefNo);
		return null;
	}
	
	//send money to Donatee
	
	@Override
	public String sendMoneyMobile(SendMoneyDonateeDTO dto, String username, PQService service) {
		String transactionRefNo = System.currentTimeMillis() + "";

		transactionApi.initiateSendMoney(
				Double.parseDouble(dto.getAmount()), "Send Money Rs " + dto.getAmount() + " to " + dto.getDonatee()
						+ " from " + username + "|" + dto.getMessage(),
				service, transactionRefNo, username, dto.getDonatee());
		userApi.saveSendMoneyToDonateeRequest(dto, transactionRefNo);
		transactionApi.successSendMoney(transactionRefNo);
		return null;
	}
	
	
	
	@Override
	 public String sendMoneyAgentMobile(SendMoneyMobileDTO dto, String username, PQService service) {
	  // TODO Auto-generated method stub
	  String transactionRefNo = System.currentTimeMillis() + "";
	  transactionApi.initiateAgentSendMoney(
	    Double.parseDouble(dto.getAmount()), "Send Money Rs " + dto.getAmount() + " to " + dto.getMobileNumber()
	      + " from " + username + "|" + dto.getMessage(),
	    service, transactionRefNo, username, dto.getMobileNumber());
	  userApi.saveSendMoneyRequest(dto, transactionRefNo);
	  transactionApi.successAgentSendMoney(transactionRefNo);
	  return null;
	 }

	@Override
	public boolean sendMoneyBankInitiate(SendMoneyBankDTO dto, String username, PQService service) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		try {
			String transactionRefNo = System.currentTimeMillis() + "";
			transactionApi.initiateBankTransfer(Double.parseDouble(dto.getAmount()),
					"Send money of " + dto.getAmount() + " to bank", service, transactionRefNo, username,
					StartupUtil.BANK);
			userApi.saveBTransferRequest(dto, transactionRefNo);
			Banks bank = banksRepository.findByCode(dto.getBankCode());
			BankDetails details = bankDetailRepository.findByIfscCode(dto.getIfscCode(), bank);
			User u = userApi.findByUserName(username);
			BankTransfer transfer = ConvertUtil.bankTransfer(dto, transactionRefNo, details, u);
			bankTransferRepository.save(transfer);
			dto.setTransactionRefNo(transactionRefNo);
			dto.setServiceCode(service.getCode());
			frmResponse.setSuccess(true);
			if(DeploymentConstants.PRODUCTION){
			frmResponse = frmApi.decideFundTransfer(dto, u);
			if (frmResponse.isSuccess()) {
				sendMoneyBankSuccess(transactionRefNo);
				frmApi.saveFrmDetails(u.getUserDetail().getContactNo(), frmResponse.getDecideResp(),
						frmResponse.isEnqueueResp(), transactionRefNo, FRMConstants.funTr);
			} else {
				sendMoneyBankFailed(transactionRefNo);
				frmApi.saveFrmDetails(u.getUserDetail().getContactNo(), frmResponse.getDecideResp(),
						frmResponse.isEnqueueResp(), transactionRefNo, FRMConstants.funTr);
			}
		}else{
			sendMoneyBankSuccess(transactionRefNo);
		}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frmResponse.isSuccess();
	}
	
	@Override
	public String sendMoneyBankFailed(String transactionRefNo) {
		transactionApi.failedBankTransfer(transactionRefNo);
		return null;
	}

	@Override
	public String sendMoneyBankSuccess(String transactionRefNo) {
		transactionApi.successBankTransfer(transactionRefNo);
		return null;
	}

	@Override
	public String sendMoneyStore(PayAtStoreDTO dto, String username, PQService service) {
		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.initiateMerchantPayment(Double.parseDouble(dto.getAmount()),
				"Send Money Rs " + dto.getAmount() + " to " + dto.getServiceProvider() + " from " + username,dto.getRemarks(), service,
				transactionRefNo, username, dto.getServiceProvider(), false);
		transactionApi.successSendMoney(transactionRefNo);
		return null;
	}

	@Override
	public String prepareSendMoney(String username) {
		User receiverUser = userApi.findByUserName(username);
		String serviceCode = "SMR";
		if ((receiverUser == null) || (receiverUser.getMobileStatus().equals(Status.Inactive))) {
			serviceCode = "SMU";
		} else {
			serviceCode = "SMR";
		}
		if (receiverUser == null) {
			RegisterDTO dto = new RegisterDTO();
			dto.setFirstName(" ");
			dto.setLastName(" ");
			dto.setMiddleName(" ");
			dto.setEmail("");
			dto.setContactNo(username);
			dto.setAddress("");
			dto.setLocationCode("");
			dto.setUserType(UserType.User);
			dto.setUsername(username);
			dto.setPassword("");
			dto.setConfirmPassword("");
			try {
				userApi.saveUnregisteredUserSendMoney(dto);
			} catch (ClientException e) {
				e.printStackTrace();
			}
		}
		return serviceCode;
	}

	//for sending money to donatee
	
	@Override
	public String prepareSendMoneyToDonatee(String donatee) {
		User receiverUser = userApi.findByUserName(donatee);
		String serviceCode = null;
		if ((receiverUser != null) && (receiverUser.getMobileStatus().equals(Status.Active))) {
			serviceCode = "SMR";
		} 
		
		return serviceCode;
	}
	
	@Override
	public ResponseDTO preparePayStore(PayStoreDTO dto, User u) {
		ResponseDTO result = new ResponseDTO();
		User merchant = userRepository.findUserByUserId(dto.getId());
		if (merchant != null) {
			String authority = merchant.getAuthority();
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					PGDetails merchantDetails = pgDetailsRepository.findByUser(merchant);
					if (merchantDetails != null) {
						boolean usingStore = merchantDetails.isStore();
						if (usingStore) {
							PQService merchantService = merchantDetails.getService();
							if (merchantService != null) {
								UserDetail merchantDetail = merchant.getUserDetail();
								double minAmt=merchantService.getMinAmount();
								if(dto.getNetAmount()>=minAmt){
								if (merchantDetail != null) {
									String transactionRefNo = System.currentTimeMillis() + "";
									String description = "Payment of " + dto.getNetAmount() + " to "+ merchantDetail.getFirstName();
									transactionApi.successCanteenMerchantPayment(dto.getNetAmount(), description,dto.getRemarks(),
											merchantService, transactionRefNo, u.getUsername(), merchant.getUsername(),false);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Payment Successful");
									result.setDetails(dto.getNetAmount() + " successfully transferred to "
											+ merchantDetail.getFirstName() + " \n VPayQwik Transaction ID : "
											+ transactionRefNo);
									return result;
							  }
							}else{
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("The minimum amount to pay "+merchant.getUsername()+" is "+minAmt);
							}
								}else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Service not defined for this merchant");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Not authorized to use as store");
						}
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Not a Valid VPayQwik QR");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Authority is undefined");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Not a Valid VPayQwik QR.");
			result.setDetails("Merchant Not Found.");
		}
		
		return result;
	}

	@Override
	public ResponseDTO refundMoneyToAccount(RefundDTO dto, User u) {
		ResponseDTO result = new ResponseDTO();
		PQTransaction transaction = transactionApi.processRefundTransaction(dto.getTransactionRefNo());
		if (transaction != null) {
			if (transaction.getStatus().equals(Status.Refunded)) {
				PQAccountDetail account = u.getAccountDetail();
				double balance = account.getBalance();
				if (account != null) {
					if (balance >= dto.getNetAmount()) {
						PQService service = pqServiceRepository.findServiceByCode("RMU");
						if (service != null) {
							refundMoney(dto, u, service);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Amount successfully refunded");
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Service Not Found");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Balance is " + balance + " which is less than " + dto.getNetAmount());
					}
				} 
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Please try again later");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Transaction Not Found");
		}

		return result;
	}

	@Override
	public void refundMoney(RefundDTO dto, User u, PQService service) {
		String refundUser = "refund@msewa.com";
		String transactionRefNo = System.currentTimeMillis() + "";
		String description = "Refund from " + u.getUsername() + " to " + refundUser;
		transactionApi.initiateSendMoney(dto.getNetAmount(), description, service, transactionRefNo, u.getUsername(),
				refundUser);
		transactionApi.successSendMoney(transactionRefNo);
	}

	@Override
	public SendMoneyResponse sendMoneyMBankInitiate(SendMoneyBankDTO dto, User user, PQService service) {
		SendMoneyResponse resp = new SendMoneyResponse();
		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.initiateMBankTransfer(Double.parseDouble(dto.getAmount()), "NEFT to Merchant A/C", service,
				transactionRefNo, user, StartupUtil.MBANK, dto.toJSON().toString());
		Banks bank = banksRepository.findByCode(dto.getBankCode());
		BankDetails details = bankDetailRepository.findByIfscCode(dto.getIfscCode(), bank);
		MBankTransfer transfer = ConvertUtil.mBankTransfer(dto, transactionRefNo, details, user);
		mBankTransferRepository.save(transfer);
		sendMoneyBankSuccess(transactionRefNo);
		resp.setValid(true);
		return resp;
	}
	
	
	@Override
	public SendMoneyResponse sendMoneyMBankInitiateNew(SendMoneyBankDTO dto, User user, PQService service) {
		SendMoneyResponse resp = new SendMoneyResponse();
		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.successMBankTransfer(Double.parseDouble(dto.getAmount()), "NEFT to Merchant A/C", service,
				transactionRefNo, user, StartupUtil.MBANK, dto.toJSON().toString());
		Banks bank = banksRepository.findByCode(dto.getBankCode());
		BankDetails details = bankDetailRepository.findByIfscCode(dto.getIfscCode(), bank);
		MBankTransfer transfer = ConvertUtil.mBankTransfer(dto, transactionRefNo, details, user);
		mBankTransferRepository.save(transfer);
		resp.setValid(true);
		return resp;
	}
	
	@Override
	public SendMoneyResponse sendMoneyABankInitiate(AgentSendMoneyBankDTO dto, User user, PQService service) {
		SendMoneyResponse resp = new SendMoneyResponse();
		String transactionRefNo = System.currentTimeMillis() + "";
		SenderBankTransferInfo senderInfo = ConvertUtil.senderBankTransferInfo(dto);
		senderBankTransferInfoRepository.save(senderInfo);
		transactionApi.initiateABankTransfer(Double.parseDouble(dto.getAmount()), "NEFT to Agent A/C", service,
				transactionRefNo, user, StartupUtil.ABANK, dto.toJSON().toString());
		Banks bank = banksRepository.findByCode(dto.getBankCode());
		BankDetails details = bankDetailRepository.findByIfscCode(dto.getIfscCode(), bank);
		ABankTransfer transfer = ConvertUtil.aBankTransfer(dto, transactionRefNo, details, user);
		transfer.setSenderInfo(senderInfo);
		aBankTransferRepository.save(transfer);
		sendMoneyBankSuccess(transactionRefNo);
		dto.setTransactionRef(transactionRefNo);
		smsSenderApi.senderBankTransferSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.AGENT_SENDER_BANK_TRANSFER,dto,dto.getSenderMobileNo());
		smsSenderApi.senderBankTransferSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.AGENT_RECEIVER_BANK_TRANSFER,dto,dto.getReceiverMobileNo());
		mailSenderApi.senderReceiverBankTransferMail("VPayQwik Transaction", MailTemplate.AGENT_BANK_TRANSFER_SENDER,dto,dto.getSenderEmailId());
		mailSenderApi.senderReceiverBankTransferMail("VPayQwik Transaction", MailTemplate.AGENT_BANK_TRANSFER_RECEIVER,dto,dto.getReceiverEmailId());
		resp.setValid(true);
		return resp;
	}
	
	@Override
	public List<NeftRequestDTO> getNeftRequestFile() throws IOException {
		try {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		String date = dateFormat.format(calendar.getTime());
		Date myDate = null;
		myDate = dateFormat.parse(date);
		Date oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));
		String result = dateFormat.format(oneDayBefore);
			List<BankTransfer> bankTransfer = bankTransferRepository.findAllNeftTransactionByDate(dateFormat.parse(result));
			if (bankTransfer != null && bankTransfer.size() != 0) {
				List<NeftRequestDTO> requestDTOs = convertRequestList(bankTransfer);
				if(requestDTOs.size() != 0){
				writeToFile(requestDTOs,path, requestDTOs.size());
				}else{
					System.err.println("There is no record in the db");
				}
				return requestDTOs;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<NeftRequestDTO> convertRequestList(List<BankTransfer> transfers) {
		List<NeftRequestDTO> dtoList = new ArrayList<NeftRequestDTO>();
		for (BankTransfer transfer : transfers) {
			PQTransaction transaction = pqTransactionRepository.findByTransactionRefNo(transfer.getTransactionRefNo());
			if (transaction.getStatus().equals(Status.Success)) {
				dtoList.add(convertBankTransfer(transfer));
			}
		}
		return dtoList;
	}

	@Override
	public NeftRequestDTO convertTransfer(MBankTransfer transfer) {
		NeftRequestDTO dto = new NeftRequestDTO();
		dto.setAmount(transfer.getAmount());
		dto.setBeneficiaryName(transfer.getBeneficiaryName());
		dto.setBeneficiaryAccountNumber(transfer.getBeneficiaryAccountNumber());
		dto.setTransactionRefNo(transfer.getTransactionRefNo());
		dto.setIfscCode(transfer.getBankDetails().getIfscCode());
		String userdescription = description(transfer.getTransactionRefNo());
		dto.setDescription(userdescription);
		return dto;
	}
	
	@Override
	public NeftRequestDTO convertBankTransfer(BankTransfer transfer) {
		NeftRequestDTO dto = new NeftRequestDTO();
		dto.setAmount(transfer.getAmount());
		dto.setBeneficiaryName(transfer.getBeneficiaryName());
		dto.setBeneficiaryAccountNumber(transfer.getBeneficiaryAccountNumber());
		dto.setTransactionRefNo(transfer.getTransactionRefNo());
		dto.setIfscCode(transfer.getBankDetails().getIfscCode());
		String userdescription = description(transfer.getTransactionRefNo());
		dto.setDescription(userdescription);
		return dto;
	}

	@Override
	public String description(String transactionRefNo) {
		return pqTransactionRepository.findByTransactionRefNo(transactionRefNo).getDescription();

	}

	private void writeToFile(List<NeftRequestDTO> list, String path, int size) throws IOException {
		FileWriter fw = null;
		BufferedWriter out = null;
		try {
			String fileName = ConvertUtil.generateFileName();
			String filePath = path + "ACH-CR-VIJB-VIJB24875" + "-" + format.format(new Date()) + "-" + "9027" + fileName
					+ "-" + "INP" + ".txt";
			File file = new File(path + "ACH-CR-VIJB-VIJB24875" + "-" + format.format(new Date()) + "-" + "9027"
					+ fileName + "-" + "INP" + ".txt");
			if (!file.exists())
				file.createNewFile();
			fw = new FileWriter(file, true);
			out = new BufferedWriter(fw);
			out.write(formatTransferHeaderString());
			out.newLine();
			for (NeftRequestDTO transfer : list) {
				out.write(formatTransferString(transfer));
				out.newLine();
			}
			out.flush();
			fw.flush();
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			String result = dateFormat.format(new Date(cal.getTimeInMillis()));
			mailSenderApi.dailyBankTrasnferReport(filePath, result, list.size(), amountCalculateForMail(list));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public static long amountCalculate(List<NeftRequestDTO> list) {
		long total = 0;
		for (NeftRequestDTO dto : list) {
			long amt = (long) (dto.getAmount() * 100);
			String amount = String.format("%013d", amt);
			long l = Long.parseLong(amount);
			total += l;

		}
		return total;
	}
	
	public static long amountCalculateForMail(List<NeftRequestDTO> list) {
		long total = 0;
		for (NeftRequestDTO dto : list) {
			long amt = (long) (dto.getAmount());
			total += amt;

		}
		return total;
	}

	// 2 - 9-18 -40length-16space-20 length-13 space-40 space
	public String formatTransferString(NeftRequestDTO transfer) {
		StringBuilder line = new StringBuilder();
		long amount = (long) (transfer.getAmount() * 100);
		String amt = String.format("%013d", amount);
		line.append(String.format("%2s", "23") + insertSpace(9));
		line.append(String.format("%2s", "10") + insertSpace(18));
		line.append(String.format("%-40s", transfer.getBeneficiaryName()) + insertSpace(16));
		line.append(String.format("%-20s", transfer.getDescription()).substring(0, 20) + insertSpace(13));
		line.append(String.format("%-13s", amt) + insertSpace(23));
		line.append(String.format("%-46s", transfer.getIfscCode() + transfer.getBeneficiaryAccountNumber()));
		line.append(String.format("%-11s", "VIJB0001331"));
		line.append(String.format("%-18s", "NACH00000000006474"));
		line.append(String.format("%-30s", transfer.getTransactionRefNo().substring(0, 13)));
		line.append(String.format("%-45s", "10 "));
		return line.toString();
	}

	public String formatTransferHeaderString() {
		try {
			StringBuilder line = new StringBuilder();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			String date = dateFormat.format(calendar.getTime());
			Date myDate = dateFormat.parse(date);
			Date oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));
			String result = dateFormat.format(oneDayBefore);
			List<BankTransfer> bankTransfer = bankTransferRepository
					.findAllNeftTransactionByDate(dateFormat.parse(result));
			List<NeftRequestDTO> requestDTOs = convertRequestList(bankTransfer);
			String totalAmount = String.format("%013d", amountCalculate(requestDTOs));
			String total = totalAmount + format.format(new Date());
			String join = "VPM" + String.format("%013d", 0);
			line.append(String.format("%2s", "12") + insertSpace(7));
			line.append(String.format("%-40s", "VPAYQWIK User") + insertSpace(47));
			line.append(String.format("%-16s", join));
			line.append(String.format("%-21s", total) + insertSpace(23));
			line.append(String.format("%-18s", "NACH00000000006474"));
			line.append(String.format("%-18s", "ACH"));
			line.append(String.format("%-11s", "VIJB0001331") + insertSpace(35));
			line.append(String.format("%09d", requestDTOs.size()) + insertSpace(59));
			return line.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String insertSpace(int n) {
		return String.format("%" + n + "s", " ");
	}

	@Override
	public SendMoneyResponse checkInputParameters(SendMoneyBankDTO dto) {
		SendMoneyResponse resp = new SendMoneyResponse();
		double amount = Double.parseDouble(dto.getAmount());
		String message = "";
		Banks bank = banksRepository.findByCode(dto.getBankCode());
		boolean valid = true;
		if (amount < 500) {
			message = "You cannot transfer amount below 500/-";
			valid = false;
		}
		if (bank == null) {
			valid = false;
			message = "Not a valid Bank";

		} else {
			BankDetails details = bankDetailRepository.findByIfscCode(dto.getIfscCode(), bank);
			if (details == null) {
				message = "Not a valid IFSC Code of Bank " + bank.getName();
				valid = false;
			}
		}
		resp.setValid(valid);
		resp.setMessage(message);
		return resp;
	}

	@Override
	public String getBankCode(SendMoneyBankDTO dto) {
		
		Banks bank = banksRepository.getBankCode(dto.getBankName());
		return bank.getCode();
	}
	
	@Override
	public SendMoneyResponse checkAInputParameters(AgentSendMoneyBankDTO dto) {
		SendMoneyResponse resp = new SendMoneyResponse();
		double amount = Double.parseDouble(dto.getAmount());
		String message = "";
		Banks bank = banksRepository.findByCode(dto.getBankCode());
		boolean valid = true;
		if (amount < 500) {
			message = "You cannot transfer amount below 500/-";
			valid = false;
		}
		if (bank == null) {
			valid = false;
			message = "Not a valid Bank";

		} else {
			BankDetails details = bankDetailRepository.findByIfscCode(dto.getIfscCode(), bank);
			if (details == null) {
				message = "Not a valid IFSC Code of Bank " + bank.getName();
				valid = false;
			}
		}
		resp.setValid(valid);
		resp.setMessage(message);
		return resp;
	}

	@Override
	public String getAgentBankCode(AgentSendMoneyBankDTO dto) {
		
		Banks bank = banksRepository.getBankCode(dto.getBankName());
		return bank.getCode();
	}

	@Override
	public String requestMoneyAgentMobile(SendMoneyMobileDTO dto, String username, PQService service) {
		// TODO Auto-generated method stub
		String transactionRefNo = System.currentTimeMillis() + "";
//		transactionApi.initiateRequestAgentSendMoney(
//				Double.parseDouble(dto.getAmount()), "Load Money Request Rs " + dto.getAmount() ,
//				service, transactionRefNo, "superagent@msewa.com", username);
//		userApi.saveSendMoneyRequest(dto, transactionRefNo);
//		transactionApi.successRequestAgentSendMoney(transactionRefNo);
		return null;
	}

	@Override
	public String agentRequestmoneysuccess(String transactionRefNo) {
		// TODO Auto-generated method stub

//		transactionApi.successRequestAgentSendMoney(transactionRefNo);
		return null;
	}
	
	@Override
	public String sendMoneyMobileNew(SendMoneyMobileDTO dto, String username, PQService service) {
		String transactionRefNo = System.currentTimeMillis() + "";
		transactionApi.sendMoneyNew(
				Double.parseDouble(dto.getAmount()), "Send Money Rs " + dto.getAmount() + " to " + dto.getMobileNumber()
						+ " from " + username + "|" + dto.getMessage(),
				service, transactionRefNo, username, dto.getMobileNumber());
		userApi.saveSendMoneyRequest(dto, transactionRefNo);
		return null;
	}
	
	
	
	@Override
	public List<NeftRequestDTO> generatePPIFile() throws IOException {
		try {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		String date = dateFormat.format(calendar.getTime());
		Date myDate = null;
		myDate = dateFormat.parse(date);
		Date oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));
		String result = dateFormat.format(oneDayBefore);
		String parseDate=format.format(oneDayBefore);
			List<BankTransfer> bankTransfer = bankTransferRepository.findAllNeftTransactionByDate(dateFormat.parse(result));
			if (bankTransfer != null && bankTransfer.size() != 0) {
				List<NeftRequestDTO> requestDTOs = convertRequestListFile(bankTransfer);
				if(requestDTOs.size() != 0){
					createFileForPPI(requestDTOs,parseDate);
				}else{
					System.err.println("There is no record in the db");
				}
				return requestDTOs;
			}
			/*double countOfTransaction=pqTransactionRepository.findCountOfTransactionForPPI(dateFormat.parse(result));
			double amountOfTransaction=pqTransactionRepository.findAmountOfTransactionForPPI(dateFormat.parse(result));*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<NeftRequestDTO> convertRequestListFile(List<BankTransfer> transfers) {
		Map<String, NeftRequestDTO> neftReqMap = new LinkedHashMap<String, NeftRequestDTO>();
		NeftRequestDTO nDto;
			for (BankTransfer transfer : transfers) {
				nDto = convertBankTransferPPI(transfer);
				if( neftReqMap.containsKey(nDto.getIfscCode()) ){
					nDto.setAmount(neftReqMap.get(nDto.getIfscCode()).getAmount() + nDto.getAmount());
					nDto.setTxnVolume(neftReqMap.get(nDto.getIfscCode()).getTxnVolume() + nDto.getTxnVolume());
				} 
				neftReqMap.put(nDto.getIfscCode(), nDto);
                 
			}
		List<NeftRequestDTO> dtoList = new ArrayList<NeftRequestDTO>(neftReqMap.values());
	
		return dtoList;
	}
	
	@Override
	public NeftRequestDTO convertBankTransferPPI(BankTransfer transfer) {
		NeftRequestDTO dto = new NeftRequestDTO();
		dto.setAmount(transfer.getAmount());
		dto.setBeneficiaryName(transfer.getBeneficiaryName());
		dto.setBeneficiaryAccountNumber(transfer.getBeneficiaryAccountNumber());
		dto.setTransactionRefNo(transfer.getTransactionRefNo());
		dto.setIfscCode(transfer.getBankDetails().getIfscCode());
		dto.setTxnVolume(1);
		String userdescription = description(transfer.getTransactionRefNo());
		dto.setDescription(userdescription);
		return dto;
	}
	
	public void createFileForPPI(List<NeftRequestDTO> requestDTOs, String result) throws Exception {
		FileWriter writer = null;
		BufferedWriter out = null;
		PPIFiles ppiFile = new PPIFiles();
		String COMMA_DELIMITIER = ",";
		String NEW_LINE_SEPARATOR = "\n";
		String FILE_HEADER = "RemitterIFSCCode,RecipientIFSCCode,TxnValue,TxnVolume,TxnSubCategoryCode,others";
		String fileName = StartupUtil.PPI_FILE_PATH;
		File file = new File(fileName + PayQwikUtil.getPPIFileName(result));
		ppiFile.setName(PayQwikUtil.getPPIFileName(result));
		try {
			if (!file.exists())
				System.err.println("file doesnot exist");
			file.createNewFile();
			writer = new FileWriter(file, true);
			out = new BufferedWriter(writer);
			PPIRequestDTO dto = new PPIRequestDTO();
			writer.append(FILE_HEADER);
			for (NeftRequestDTO req : requestDTOs) {
				dto.setRemitterIFSCCode("");
				writer.append(NEW_LINE_SEPARATOR);
				writer.append(dto.getRemitterIFSCCode());
				writer.append(COMMA_DELIMITIER);
				dto.setRecipientIFSCCode(req.getIfscCode());
				writer.append(dto.getRecipientIFSCCode());
				writer.append(COMMA_DELIMITIER);
				dto.setTxnValue(String.valueOf(req.getAmount()));
				writer.append(dto.getTxnValue());
				writer.append(COMMA_DELIMITIER);
				writer.append(String.valueOf(req.getTxnVolume()).substring(0, 1));
				writer.append(COMMA_DELIMITIER);
				dto.setTxnSubCategoryCode("1");
				writer.append(dto.getTxnSubCategoryCode());
				writer.append(COMMA_DELIMITIER);
				dto.setOthers("");
				writer.append(dto.getOthers());
				writer.append(COMMA_DELIMITIER);
			}
			PPIFiles files = pPIFilesRepository.findByName(ppiFile.getName());
			if (files == null) {
				files = new PPIFiles();
				files.setName(ppiFile.getName());
				files.setPath(file.getAbsolutePath());
				files.setStatus(Status.Generated);
				pPIFilesRepository.save(files);
			}
			out.flush();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	 @Override
	    public void uploadPPIFile() throws ParseException {
		 Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			String date = dateFormat.format(calendar.getTime());
			Date myDate = null;
			myDate = dateFormat.parse(date);
			Date oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));
			String parseDate=format.format(oneDayBefore);
		    List<PPIFiles> files = pPIFilesRepository.getPPIFiles(Status.Generated);
	        System.err.println(files);
		    if(files != null && !files.isEmpty()) {
	            for(PPIFiles file: files){
	                UploadResponse response = FTPUtil.uploadPPIFile(file,parseDate);
	                if(response.isSuccess()){
	                    file.setStatus(Status.Uploaded);
	                } else {
	                    file.setStatus(Status.Dropped);
	                }
	                file.setRemarks(response.getMessage());
	                pPIFilesRepository.save(file);
	            }
	        }
	    }
		@Override
		public void sendMoneyToWinners(SendMoneyMobileDTO dto, String username, PQService service) {
			String transactionRefNo = System.currentTimeMillis() + "";
			transactionApi.SendMoneyPredictUsers(
					Double.parseDouble(dto.getAmount()), "Predict winner of Rs " + dto.getAmount() + " to " + dto.getMobileNumber()
							+ "|" + dto.getMessage(),service, transactionRefNo, username, dto.getMobileNumber());
			userApi.saveSendMoneyRequest(dto, transactionRefNo);
		}

}
