package com.payqwikapp.api.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import com.payqwikapp.api.IUserApi;

import com.payqwikapp.util.StartupUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.servlet.ModelAndView;

import com.ebs.api.IEBSRequestHandlerApi;
import com.ebs.model.EBSRedirectResponse;
import com.ebs.model.EBSRequest;
import com.payqwikapp.api.IEBSApi;
import com.payqwikapp.api.IFlightApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.FrmLoadMoneyDTO;
import com.payqwikapp.model.RazorPayRequest;
import com.payqwikapp.model.RazorPayResponse;
import com.payqwikapp.model.RefundResponseDTO;
import com.payqwikapp.model.RefundStatusDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;

import ch.qos.logback.core.pattern.ConverterUtil;

public class EBSApi implements IEBSApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String dir = StartupUtil.CSV_FILE_LOADMONEY;
	private final SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
	private final IEBSRequestHandlerApi ebsRequestHandlerApi;
	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final JavaMailSender mailSender;
	private final IFlightApi flightApi;
	private final IFRMApi frmApi;
	public EBSApi(IEBSRequestHandlerApi ebsRequestHandlerApi, ITransactionApi transactionApi,IUserApi userApi,JavaMailSender mailSender
			,IFlightApi flightApi,IFRMApi frmApi) {
		this.ebsRequestHandlerApi = ebsRequestHandlerApi;
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.mailSender=mailSender;
		this.flightApi = flightApi;
		this.frmApi=frmApi;
	}
	
	@Override
	public EBSRequest requestHandler(EBSRequest request, String username, PQService service) {
		String transactionRefNo = "" + System.currentTimeMillis();
		double amount = Double.parseDouble(request.getAmount());
		request.setReference_no(transactionRefNo);
		request = ebsRequestHandlerApi.request(request);
		String description = "Load Money through EBS";
		transactionApi.initiateLoadMoney(amount, description, service, transactionRefNo, username);
		userApi.saveLMRequest(request,transactionRefNo);
		return request;
	}

	@Override
	public ModelAndView responseHandlerView(HttpServletRequest request, String username) {
		ModelAndView mv = new ModelAndView();
		boolean isSuccess = false;
		String transactionRefNo = null;
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			String paramValue = request.getParameter(paramName);
			mv.addObject(paramName, paramValue);
			if (paramName.equalsIgnoreCase("ResponseCode") && paramValue.equalsIgnoreCase("0")) {
				isSuccess = true;
			}
			if (paramName.equalsIgnoreCase("MerchantRefNo")) {
				transactionRefNo = paramValue;
			}
		}
		if (isSuccess) {
			transactionApi.successLoadMoney(transactionRefNo, "");
		} else {
			transactionApi.failedLoadMoney(transactionRefNo);
		}
		return mv;
	}
	
	@Override
	public ResponseDTO responseHandler(EBSRedirectResponse redirectResponse,PQService service,String sessionId,User u) {
		ResponseDTO result = new ResponseDTO();
		FRMResponseDTO frmResp = new FRMResponseDTO();
		FrmLoadMoneyDTO loadmoney= new FrmLoadMoneyDTO();
		String responseCode = redirectResponse.getResponseCode();
		String transactionRefNo = redirectResponse.getMerchantRefNo();
		loadmoney.setTransactionRefNo(transactionRefNo);
		loadmoney.setServiceCode(service.getCode());
        loadmoney.setAmount(Double.parseDouble(redirectResponse.getAmount()));
		frmResp.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
		 frmResp= frmApi.decideLoadMoney(loadmoney,u);
		}
		logger.info("frm response ::" + frmResp.isSuccess());
		if (frmResp.isSuccess() && responseCode.equalsIgnoreCase("0")) {
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Success");
			result.setDetails(redirectResponse);
			result.setSessionId(sessionId);
			transactionApi.successLoadMoney(transactionRefNo, "");
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Failure");
			result.setDetails(redirectResponse);
			transactionApi.failedLoadMoney(transactionRefNo);
		}
		return result;
	}
	@Override
	public void responseFromWeb() {
		File fileOut=null;
		FileOutputStream out=null;
		try{
		
			Client client = Client.create();
			WebResource webResource = client
					.resource(DeploymentConstants.WEB_URL+"/Api/v1/User/Website/En/LoadMoney/refundEBS");
			ClientResponse response = webResource.accept("application/json").type("application/json").get(ClientResponse.class);

			String output = response.getEntity(String.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			
			
			String fileName=ConvertUtil.generateFileName();
			
			
			fileOut = new File(dir +"/"+"EbsRefundMoney"+ format.format(new Date())+"-"+(fileName) +".xls");
			
			
			
			JSONArray jsonarray=new JSONArray(output);
			List<RefundResponseDTO> list=new ArrayList<RefundResponseDTO>();
			for (int i = 0; i < jsonarray.length(); i++) {
				RefundResponseDTO dto=new RefundResponseDTO();
			org.json.JSONObject jsonObject=jsonarray.getJSONObject(i);
			String status=jsonObject.getString("status");
			String transactionRefNo=jsonObject.getString("transactionRefNo");
//			String amount=jsonObject.getString("amount");
			String created=jsonObject.getString("created");
			if(!status.equalsIgnoreCase("null") && !status.isEmpty()){
				dto.setStatus(status);
			}
		    if(!transactionRefNo.equalsIgnoreCase("null") && !transactionRefNo.isEmpty()){
				dto.setTransactionRefNo(transactionRefNo);
				}
		    if(!created.equalsIgnoreCase("null") && !created.isEmpty()){
				dto.setCreated(created);
				}
			list.add(dto);
			}
        HSSFWorkbook workbook  = new HSSFWorkbook();
        HSSFSheet excelSheet = workbook.createSheet("EbsRefundMoney");
        setExcelHeader(excelSheet);
        setExcelRows(excelSheet,list);
        
        	out=new FileOutputStream(fileOut);
        	if (!fileOut.exists())
        	fileOut.createNewFile();
			workbook.write(out);
			out.flush();
			sendMail(""+fileOut);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        finally{
        	try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}
	
	public static void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("created");
		excelHeader.createCell(1).setCellValue("status");
		excelHeader.createCell(2).setCellValue("transactionRefNo");
		

	}
	public void setExcelRows(HSSFSheet excelSheet, List<RefundResponseDTO> refundDTOs) {
		int record = 1;
		for (RefundResponseDTO dto : refundDTOs) {
			HSSFRow cell= excelSheet.createRow(record++);
			cell.createCell(0).setCellValue(dto.getCreated());
			cell.createCell(1).setCellValue(dto.getStatus());
			cell.createCell(2).setCellValue(dto.getTransactionRefNo());
			}
		}
		
		
	
	public void sendMail(String fileOut) throws AddressException{
		
		String msg="Hi team,"
				+ "\n"
				+ "Please find the attached sheet of refund Load Money Request.";
		
		String [] cc={
				
				"kamal@msewa.com",
				"abhijitp@msewa.com",
				"manjunath@msewa.com",
		};
		
		 try{  
		        MimeMessage message = mailSender.createMimeMessage();  
		        MimeMessageHelper helper = new MimeMessageHelper(message, true);  
		        //to be changed while pushing to production
		        helper.setFrom("vpayqwikcare@vijayabank.co.in");  
		        helper.setTo("kamala.satapathy@gmail.com");  
		        helper.setCc(cc);
		      //  helper.setBcc("wel.abhijit@gmail.com");
		        helper.setSubject("Load Money Refund");  
		        helper.setText(msg);  
		  
		        // attach the file  
		        FileSystemResource file = new FileSystemResource(fileOut);  
		      helper.addAttachment(file.getFilename(), file);
		      
		        mailSender.send(message);  
		        }
		 catch(javax.mail.MessagingException e){
			 e.printStackTrace();
		    }  
	}

	
	//flight payment gateway apis
	
	
	@Override
	public EBSRequest requestHandlerFLight(EBSRequest request, String username, PQService service) {
		String transactionRefNo = "" + System.currentTimeMillis();
		double amount = Double.parseDouble(request.getAmount());
		request.setReference_no(transactionRefNo);
		request = ebsRequestHandlerApi.request(request);
		PQTransaction pqTransaction=transactionApi.initiateFlightPaymentEBS(amount, request.getDescription(), service, transactionRefNo, username,StartupUtil.FLIGHT_BOOKING);
		request.setPqTransaction(pqTransaction);
		userApi.saveLMRequest(request,transactionRefNo);
		return request;
	}
	
	@Override
	public ResponseDTO responseHandlerFlight(EBSRedirectResponse redirectResponse) {
		ResponseDTO result = new ResponseDTO();
		boolean isSuccess = false;
		String responseCode = redirectResponse.getResponseCode();
		String transactionRefNo=redirectResponse.getMerchantRefNo();
		if (responseCode.equalsIgnoreCase("0")) {
				isSuccess = true;
		}
		if (isSuccess) {
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Success");
			result.setDetails(redirectResponse);
			transactionApi.successFlightPaymentEBS(transactionRefNo,redirectResponse.getPaymentId());
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Failure");
			result.setDetails(redirectResponse);
			transactionApi.failedFlightEBS(transactionRefNo);
		}
		return result;
	}
	
	@Override
	public RazorPayRequest requestRazorPay(RazorPayRequest request, String username, PQService service) {
		String transactionRefNo = "" + System.currentTimeMillis();
		request.setTransactionId(transactionRefNo);
		double amount = request.getAmount();
		String description = "Load Money through RazorPay";
		transactionApi.initiateLoadMoney(amount, description, service, transactionRefNo, username);
		return request;
	}
	
	@Override
	public ResponseDTO successRazorPay(RazorPayResponse redirectResponse) {
		ResponseDTO result = new ResponseDTO();
		if (redirectResponse.isSuccess()) {
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Success");
			result.setDetails(redirectResponse);
			transactionApi.successLoadMoney(redirectResponse.getTransactionId(),redirectResponse.getReferenceId());
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Failure");
			result.setDetails(redirectResponse);
			transactionApi.failedLoadMoney(redirectResponse.getTransactionId());
		}
		return result;
	}
}
