package com.payqwikapp.api.impl;

import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.payqwikapp.api.IABusApi;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.AgentBusTicket;
import com.payqwikapp.entity.AgentTravellerDetails;
import com.payqwikapp.entity.BusCityList;
import com.payqwikapp.entity.BusResponseOfMdex;
import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TravellerDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.bus.AgentBusTicketResp;
import com.payqwikapp.model.travel.bus.AgentTicketDetailsDTO;
import com.payqwikapp.model.travel.bus.BusCityListResponse;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.model.travel.bus.BusPaymentReq;
import com.payqwikapp.model.travel.bus.BusPaymentResponse;
import com.payqwikapp.model.travel.bus.BusTicketResp;
import com.payqwikapp.model.travel.bus.TicketDetailsDTO;
import com.payqwikapp.repositories.AgentBusTicketRepository;
import com.payqwikapp.repositories.AgentTravellerDetailsRepository;
import com.payqwikapp.repositories.BusCityListRepository;
import com.payqwikapp.repositories.BusJsonRepository;
import com.payqwikapp.repositories.BusTicketRepository;
import com.payqwikapp.repositories.TravellerDetailsRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.StartupUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class AgentBusApi implements IABusApi{
	private ITransactionApi transactionApi;
	private AgentBusTicketRepository agentBusTicketRepository;
	private BusCityListRepository busCityListRepository;
	private final IMailSenderApi mailSenderApi;
	private AgentTravellerDetailsRepository agentTravellerDetailsRepository;
	private final UserRepository userRepository;
	private final ISMSSenderApi smsSenderApi;
	private BusJsonRepository busJsonRepository;
	private static final String BUS_CITY_LIST =DeploymentConstants.WEB_URL+"/Api/v1/User/Android/en/Treval/Bus/GetAllCityListCron";

	//private static final String pdfUrlLive="D:/All App/payqwikapp/src/main/resources/HelloWorld.pdf";
	private static final String pdfUrlLive="/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/classes/HelloWorld.pdf";
//	private static final String pdfUrlLive = "D:/Piyush/Software/VPayQwik-app/payqwikapp/src/main/resources/HelloWorld.pdf";
	public AgentBusApi(ITransactionApi transactionApi,AgentBusTicketRepository agentBusTicketRepository,
			BusCityListRepository busCityListRepository,IMailSenderApi mailSenderApi,
			AgentTravellerDetailsRepository agentTravellerDetailsRepository,UserRepository userRepository,ISMSSenderApi smsSenderApi,BusJsonRepository busJsonRepository) {
		super();
		this.transactionApi = transactionApi;
		this.agentBusTicketRepository=agentBusTicketRepository;
		this.busCityListRepository=busCityListRepository;
		this.mailSenderApi=mailSenderApi;
		this.agentTravellerDetailsRepository=agentTravellerDetailsRepository;
		this.userRepository=userRepository;
		this.smsSenderApi=smsSenderApi;
		this.busJsonRepository=busJsonRepository;
	}

	@Override
	public BusPaymentResponse placeOrder(BusPaymentInit order, PQService service,String senderUsername,User user) {

		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		BusPaymentResponse response =  new BusPaymentResponse();
		PQTransaction valid = initiateTransaction(transactionRefNo,order.getTotalFare(),service,senderUsername);
		if(valid != null) {
			BusResponseOfMdex mdexresp=new BusResponseOfMdex();
			//mdexresp.setGetTxnIdResp(order.getGetMdexTxnIdResp());
			mdexresp.setPqTransaction(valid);
			busJsonRepository.save(mdexresp);
			saveBusBooking(order,valid,user);
			response.setTransactionRefNo(transactionRefNo);
			response.setValid(true);
			response.setMessage("Transaction Initiated");

		}else {
			response.setValid(false);
			response.setMessage("Transaction  not saved,Try again later");
		}
		return response;
	}


	@Override
	public PQTransaction initiateTransaction(String transactionRefNo,double amount,PQService service, String senderUsername) {
		String description = "Booking of Bus of Rs."+amount;
		return transactionApi.initiateBusPayment(amount,description,service,transactionRefNo,senderUsername, StartupUtil.BUS_BOOKING);
	}

	@Override
	public BusPaymentResponse processPayment(BusPaymentReq dto,User user) {

		BusPaymentResponse response = new BusPaymentResponse();
		PQTransaction txn = transactionApi.successBusPayment(dto.getpQTxnId());

		BusResponseOfMdex mdexresp=new BusResponseOfMdex();
		mdexresp.setBookingResp(dto.getMdexBookingResp());
		mdexresp.setGetTxnIdResp("");
		mdexresp.setPqTransaction(txn);
		busJsonRepository.save(mdexresp);

		if(dto.isSuccess()) {
			PQTransaction senderTransaction = transactionApi.successBusPayment(dto.getpQTxnId());
			if(senderTransaction != null) {
				if(senderTransaction.getStatus().equals(Status.Success)) {
					AgentBusTicket ticket = agentBusTicketRepository.getByTransaction(senderTransaction);
					if(ticket != null ) {
						ticket.setTicketPnr(dto.getTicketPnr());
						ticket.setOperatorPnr(dto.getOperatorPnr());
						ticket.setStatus(Status.Booked);
						ticket.setUser(user);
						agentBusTicketRepository.save(ticket);
						response.setValid(true);
						response.setMessage("Transaction Successful");

						List<AgentTravellerDetails> travellerList=agentTravellerDetailsRepository.getTicketByBusId(ticket);

						AgentTicketDetailsDTO additionalInfo=new AgentTicketDetailsDTO();
						additionalInfo.setUserMobile(dto.getUserMobile());
						additionalInfo.setTicketPnr(dto.getTicketPnr());
						additionalInfo.setEmtTxnId(ticket.getEmtTxnId());
						additionalInfo.setArrTime(ticket.getArrTime());
						additionalInfo.setDeptTime(ticket.getDeptTime());
						additionalInfo.setBoardingAddress(ticket.getBoardingAddress());
						additionalInfo.setBusOperator(ticket.getBusOperator());
						additionalInfo.setDestination(ticket.getDestination());
						additionalInfo.setJourneyDate(ticket.getJourneyDate());
						additionalInfo.setOperatorPnr(ticket.getOperatorPnr());
						additionalInfo.setSource(ticket.getSource());
						additionalInfo.setTotalFare(ticket.getTotalFare());
						additionalInfo.setUserEmail(ticket.getUserEmail());
						additionalInfo.setBoardTime(ticket.getBoardTime());

						additionalInfo.setTravellerList(travellerList);
						user.getUserDetail().setFirstName(travellerList.get(0).getfName());
						user.setUsername(dto.getUserMobile());
						mailSenderApi.sendBusTicketMail("VPayQwik Bus Ticket Booking", MailTemplate.BUSBOOKING_SUCCESS,user,senderTransaction, additionalInfo);

						smsSenderApi.sendBusTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BUSTICKET_SUCCESS,
								user, senderTransaction, additionalInfo);

					} else {
						response.setValid(false);
						response.setMessage("Order Details not found");
					}
				} else {
					response.setValid(false);
					response.setMessage("Transaction is "+senderTransaction.getStatus());
				}
			}else {
				response.setValid(false);
				response.setMessage("Transaction not found with this reference no");
			}
		} else {
			transactionApi.failedBusPayment(dto.getpQTxnId());
			response.setValid(false);
			response.setMessage("Transaction Failed");
		}
		return response;
	}

	@Override
	public void saveBusBooking(BusPaymentInit dto,PQTransaction transaction,User user) {

		AgentBusTicket booking =null;
		if (dto.getTripId()!=null && !dto.getTripId().isEmpty() && !dto.getTripId().equalsIgnoreCase("null")) {
			
			booking= agentBusTicketRepository.getTicketBytripId(dto.getTripId());
		}
		else{
		booking= new AgentBusTicket();
		booking.setTripId(dto.getSeatDetailsId());
		}
		
		if (booking==null) {
			booking= new AgentBusTicket();
		}
		
		booking.setBusId(dto.getBusId());
		booking.setTotalFare(dto.getTotalFare());
		booking.setEmtTxnId(dto.getEmtTxnId());
		booking.setUserMobile(dto.getUserMobile());
		booking.setTransaction(transaction);
		booking.setStatus(Status.Initiated);
		booking.setArrTime(dto.getArrTime());
		booking.setDeptTime(dto.getDeptTime());
		booking.setBusOperator(dto.getTravelName());
		booking.setEmtTransactionScreenId(dto.getEmtTransactionScreenId());
		booking.setUserEmail(dto.getUserEmail());
		booking.setBoardingAddress(dto.getBoardName());
		booking.setSource(dto.getSource());
		booking.setDestination(dto.getDestination());
		booking.setJourneyDate(dto.getJourneyDate());
		booking.setSeatHoldId(dto.getSeatHoldId());
		booking.setBoardTime(dto.getBoardTime());
		booking.setUser(user);
		agentBusTicketRepository.save(booking);

		if (dto.getTravellers()!=null) {

			for (int i = 0; i < dto.getTravellers().size(); i++) {
			AgentTravellerDetails travellerDetails=new AgentTravellerDetails();
			travellerDetails.setfName(dto.getTravellers().get(i).getfName());
			travellerDetails.setlName(dto.getTravellers().get(i).getlName());
			travellerDetails.setAge(dto.getTravellers().get(i).getAge());
			travellerDetails.setGender(dto.getTravellers().get(i).getGender());
			travellerDetails.setSeatNo(dto.getTravellers().get(i).getSeatNo());
			travellerDetails.setSeatType(dto.getTravellers().get(i).getSeatType());
			travellerDetails.setFare(dto.getTravellers().get(i).getFare());
			travellerDetails.setBusTicketId(booking);
			agentTravellerDetailsRepository.save(travellerDetails);
		}
	}
	}

	public static String getUniquePnrNo(){

		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder((100000 + rnd.nextInt(900000)) + "-");
		for (int i = 0; i < 5; i++)
			sb.append(chars[rnd.nextInt(chars.length)]);

		return sb.toString();

	}

	@Override
	public void failPayment(String transaction) {

		transactionApi.failedBusPayment(transaction);

	}



	@Override
	public void cronforGetAllCityList() {
		List<BusCityList> list=new ArrayList<>();
		List<BusCityList> busCityListFromDB=busCityListRepository.getAllCityList();
		int size=0;
		String stringResponse = "";
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", "12345");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Client vpqClient = Client.create();
		vpqClient.addFilter(new LoggingFilter(System.out));
		WebResource vpqWebResource = vpqClient.resource(BUS_CITY_LIST);
		ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
				.header("hash", "123456").post(ClientResponse.class, payload);
		String strResponse = vpqresponse.getEntity(String.class);

		System.out.println(strResponse);
		JSONObject jobj=null;
		try {
			jobj = new JSONObject(strResponse);
		} catch (Exception e) {

			e.printStackTrace();
		}
		String code=null;
		try {
			code = jobj.getString("code");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if ("S00".equalsIgnoreCase(code)) {
			String strDetails = null;
			try {
				strDetails = jobj.getString("details");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (strDetails!=null) {
				JSONArray cList = null;
				try {
					cList = jobj.getJSONArray("details");
				} catch (JSONException e) {
					e.printStackTrace();
				}

				for (int i = 0; i < cList.length(); i++) {
					BusCityList dto=new BusCityList();

					long cId=0;
					try {
						cId = cList.getJSONObject(i).getLong("cityId");
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					try {
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
					} catch (JSONException e) {
						e.printStackTrace();
					}

					busCityListRepository.save(dto);
					size++;
				}
			}
		}
		System.err.println("City Size is:: "+size);
	}


	@Override
	public List<BusCityList> getAllCityList() {
		BusCityListResponse response=new BusCityListResponse();
		List<BusCityList> busCityLists=null;
		try {
			busCityLists=busCityListRepository.getAllCityList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return busCityLists;
	}	


	@Override
	public List<AgentBusTicketResp> getAllTickets(String userName) {

		List<AgentBusTicketResp> list=new ArrayList<>();
		User user=userRepository.findByUsername(userName);
		PQAccountDetail accountDetail=user.getAccountDetail();
		List<AgentBusTicket> busTickets=agentBusTicketRepository.getByStatusAndAccount(Status.Initiated,accountDetail);
		for (int i = 0; i < busTickets.size(); i++) {
			AgentBusTicketResp resp=new AgentBusTicketResp();
			AgentBusTicket busTicketDB=busTickets.get(i);
			List<AgentTravellerDetails> travellerDetails2=agentTravellerDetailsRepository.getTicketByBusId(busTicketDB);
			resp.setBusTicket(busTicketDB);
			resp.setTravellerDetails(travellerDetails2);
			list.add(resp);
		}

		return list;
	}

	@Override
	public List<AgentTravellerDetails> getSingleTicketTravellerDetails(String emtTxnId) {
		List<AgentBusTicketResp> list=new ArrayList<>();
		AgentBusTicket busTicket=agentBusTicketRepository.getTicketByTxnId(emtTxnId);
		List<AgentTravellerDetails> travellerDetails=agentTravellerDetailsRepository.getTicketByBusId(busTicket);
		return travellerDetails;
	}

	@Override
	public List<AgentBusTicket> getBusDetailsForAdmin() {

		List<AgentBusTicket> busTickets=agentBusTicketRepository.getByStatus();

		return busTickets;
	}

	@Override
	public ResponseDTO cancelBookedTicket(String transactionRefNo,double refundedAmt) {

		ResponseDTO result=new ResponseDTO();
		try {
			String newTransactionRefNo=transactionApi.cancelBusTicket(transactionRefNo, refundedAmt);
			PQTransaction transaction=transactionApi.getTransactionByRefNo(transactionRefNo+"D");
			PQTransaction newTransaction=transactionApi.getTransactionByRefNo(newTransactionRefNo+"C");
			AgentBusTicket busTicket=agentBusTicketRepository.getByTransaction(transaction);
			busTicket.setStatus(Status.Cancelled);
			busTicket.setTransaction(newTransaction);
			agentBusTicketRepository.save(busTicket);
			result.setStatus(ResponseStatus.SUCCESS.getKey());
			result.setCode(ResponseStatus.SUCCESS.getValue());
			result.setMessage("Bus Ticket Cancel Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Unable To Cancel Bus Ticket");
		}

		return result;
	}


	@Override
	public List<AgentBusTicket> getBusDetailsForAdminByDate(Date from, Date to) {
		return agentBusTicketRepository.getBusTicketByDate(from, to);
	}






	//  Changes for re price

	@Override
	public BusPaymentResponse saveGetTxnId(BusPaymentInit order, PQService service, String senderUsername, User user) {

		BusPaymentResponse response =  new BusPaymentResponse();
		BusResponseOfMdex mdexresp=new BusResponseOfMdex();

		try {
			JSONObject getTxnIdResp=new JSONObject(order.getGetMdexTxnIdResp());
			System.err.println(getTxnIdResp);
			mdexresp.setGetTxnIdResp(order.getError());

			mdexresp.setSeatHoldId(order.getSeatHoldId());
			mdexresp.setEmtTxnId(order.getEmtTxnId());	
			busJsonRepository.save(mdexresp);
			saveBusBooking(order,null,user);

			response.setCode(ResponseStatus.SUCCESS.getValue());
			response.setEmtTxnId(order.getEmtTxnId());
			response.setValid(true);
			response.setMessage("Transaction Initiated");

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setValid(true);
			response.setMessage("Get Txn Id details not saved");
		}

		return response;
	}


	@Override
	public BusPaymentResponse bookTicketPayment(BusPaymentReq dto,PQService service,String senderUsername,User user) {

		BusPaymentResponse response = new BusPaymentResponse();

		PQTransaction valid = transactionApi.findByTransactionRefNoAndStatus(dto.getpQTxnId()+"D", Status.Initiated);
		if(valid != null) {

			BusResponseOfMdex mdexresp=busJsonRepository.getTxnBySeatHoldId(dto.getSeatHoldId());
			if (mdexresp!=null) {
				mdexresp.setBookingResp(dto.getMdexBookingResp());
				mdexresp.setPqTransaction(valid);
				busJsonRepository.save(mdexresp);
			}

			if(dto.isSuccess()) {
				PQTransaction senderTransaction = transactionApi.successBusPayment(dto.getpQTxnId());
				if(senderTransaction != null) {
					if(senderTransaction.getStatus().equals(Status.Success)) {
						AgentBusTicket ticket = agentBusTicketRepository.getByTransaction(valid);
						if(ticket != null ) {
							ticket.setTicketPnr(dto.getTicketPnr());
							ticket.setOperatorPnr(dto.getOperatorPnr());
							ticket.setStatus(Status.Booked);
							ticket.setUser(user);
							agentBusTicketRepository.save(ticket);
							response.setValid(true);
							response.setMessage("Transaction Successful");

							List<AgentTravellerDetails> travellerList=agentTravellerDetailsRepository.getTicketByBusId(ticket);

							AgentTicketDetailsDTO additionalInfo=new AgentTicketDetailsDTO();
							additionalInfo.setUserMobile(dto.getUserMobile());
							additionalInfo.setTicketPnr(dto.getTicketPnr());
							additionalInfo.setEmtTxnId(ticket.getEmtTxnId());
							additionalInfo.setArrTime(ticket.getArrTime());
							additionalInfo.setDeptTime(ticket.getDeptTime());
							additionalInfo.setBoardingAddress(ticket.getBoardingAddress());
							additionalInfo.setBusOperator(ticket.getBusOperator());
							additionalInfo.setDestination(ticket.getDestination());
							additionalInfo.setJourneyDate(ticket.getJourneyDate());
							additionalInfo.setOperatorPnr(ticket.getOperatorPnr());
							additionalInfo.setSource(ticket.getSource());
							additionalInfo.setTotalFare(Double.valueOf(ticket.getPriceRecheckTotalFare()));
							additionalInfo.setUserEmail(ticket.getUserEmail());
							additionalInfo.setBoardTime(ticket.getBoardTime());

							additionalInfo.setTravellerList(travellerList);
							
							user.getUserDetail().setFirstName(travellerList.get(0).getfName());
							user.setUsername(dto.getUserMobile());
							
							mailSenderApi.sendBusTicketMail("VPayQwik Bus Ticket Booking", MailTemplate.BUSBOOKING_SUCCESS,user,senderTransaction, additionalInfo);

							smsSenderApi.sendBusTicketSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.BUSTICKET_SUCCESS,
									user, senderTransaction, additionalInfo);

						} else {
							response.setValid(false);
							response.setMessage("Order Details not found");
						}
					} else {
						response.setValid(false);
						response.setMessage("Transaction is "+senderTransaction.getStatus());
					}
				}else {
					response.setValid(false);
					response.setMessage("Transaction not found with this reference no");
				}
			} else {
				transactionApi.failedBusPayment(dto.getpQTxnId());
				response.setValid(false);
				response.setMessage("Transaction Failed");
			}
		}else {
			response.setValid(false);
			response.setMessage("Transaction  not saved,Try again later");
		}
		return response;
	}



	@Override
	public BusPaymentResponse initBusPayment(BusPaymentInit order, PQService service, String senderUsername,
			User user) {

		String transactionRefNo = String.valueOf(System.currentTimeMillis());
		PQTransaction valid = initiateTransaction(transactionRefNo,order.getPriceRecheckAmt(),service,senderUsername);

		BusPaymentResponse response =  new BusPaymentResponse();

		BusResponseOfMdex mdexresp=busJsonRepository.getTxnBySeatHoldId(order.getSeatHoldId());

		if (mdexresp!=null) {
			mdexresp.setPqTransaction(valid);
			busJsonRepository.save(mdexresp);
		}

		AgentBusTicket busTicket=agentBusTicketRepository.getTicketBySeatHoldId(order.getSeatHoldId());
		if (busTicket!=null) {
			busTicket.setTransaction(valid);
			busTicket.setPriceRecheckTotalFare(order.getPriceRecheckAmt()+"");
			agentBusTicketRepository.save(busTicket);
		}

		response.setTransactionRefNo(transactionRefNo);
		response.setEmtTxnId(order.getEmtTxnId());
		response.setSeatHoldId(order.getSeatHoldId());
		response.setValid(true);
		response.setCode(ResponseStatus.SUCCESS.getValue());
		response.setMessage("Transaction Initiated");

		return response;
	}


	@Override
	public List<AgentTravellerDetails> createSingleTicketPdf(String emtTxnId) {
		AgentBusTicket busTicket=agentBusTicketRepository.getTicketByTxnId(emtTxnId);
		List<AgentTravellerDetails> travellerDetails=agentTravellerDetailsRepository.getTicketByBusId(busTicket);
		ticketPdf(travellerDetails);
		return travellerDetails;
	}


	public void ticketPdf(List<AgentTravellerDetails> list)
	{

		Font black = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.NORMAL, BaseColor.BLACK);
		Font redFont = FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, new CMYKColor(0, 255, 0, 0));
		Font yellowFont = FontFactory.getFont(FontFactory.COURIER, 14, Font.BOLD, new CMYKColor(0, 0, 255, 0));
		Font red = FontFactory.getFont(FontFactory.COURIER, 14, Font.BOLD, BaseColor.RED);
		 
		Document document = new Document();

		try
		{
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfUrlLive));
			document.open();

			String img="https://www.vpayqwik.com/resources/images/vijayalogo.png";
			Image image2 = Image.getInstance(new URL(img));
			image2.setAbsolutePosition(1f, 1f);
			image2.setAlignment(Image.RIGHT);
			image2.scaleAbsolute(50, 50);

			AgentBusTicket busTicket= list.get(0).getBusTicketId();

			String origin=busTicket.getSource();
			String destination=busTicket.getDestination();
			String bk=busTicket.getTicketPnr();
			String bkd=busTicket.getCreated().toString();
			String arr[]=bkd.split(" ");
			bkd=arr[0];
			if (bk==null) {
				bk="NA";
			} 
					
					
			PdfPTable table = new PdfPTable(2);

			table.setWidthPercentage(100);
			table.addCell(getCell("E-TICKET", PdfPCell.ALIGN_LEFT));
			table.addCell(image2);
			document.add(table);

			PdfPTable table2 = new PdfPTable(2);
			table2.setWidthPercentage(100);
			table2.addCell(getCell("Ticket Pnr: "+bk, PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("Customer Care: +918025011300", PdfPCell.ALIGN_LEFT));
			document.add(table2);

			document.add(new Paragraph(" "));
			PdfPTable table3= new PdfPTable(1);
			table3.setWidthPercentage(100);
			table3.addCell(getCell("Booked on: "+bkd, PdfPCell.ALIGN_LEFT));
			document.add(table3);
			
			document.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
			
			document.add(new Paragraph("Bus Details:",black));
			document.add(new Paragraph(" "));
//			document.add(new Paragraph(origin +" to "+destination));

			PdfPTable table4= new PdfPTable(2);
			table4.addCell(getCell(busTicket.getBusOperator(), PdfPCell.ALIGN_LEFT));
			table4.addCell(getCell("Journey Date: "+busTicket.getJourneyDate(), PdfPCell.ALIGN_RIGHT));
			document.add(table4);
			document.add(new Paragraph(" "));
//			document.add( Chunk.NEWLINE );
	
			document.add(new Paragraph("Boarding Point Details: ",black));
			document.add(new Paragraph(" "));
			
			PdfPTable table5= new PdfPTable(4);

			table5.addCell(getCell("Source", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell("Destination", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell("Boarding Point", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell("Boarding time", PdfPCell.ALIGN_LEFT));
			
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			
			table5.addCell(getCell(origin, PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(destination,PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(busTicket.getBoardingAddress(), PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(busTicket.getBoardTime(), PdfPCell.ALIGN_LEFT));

			document.add(table5);
			document.add(Chunk.NEWLINE);

			document.add(new Paragraph("Traveller Details: ",black));

//			document.add(Chunk.NEWLINE);

			document.add(new Paragraph(" "));
			
			PdfPTable table6= new PdfPTable(7);

			table6.addCell("SL No");
			table6.addCell("First Name");
			table6.addCell("Last Name");
			table6.addCell("Age");
			table6.addCell("Gender");
			table6.addCell("Seat No");
			table6.addCell("Fare");

			for (int i = 0; i < list.size(); i++) {

				table6.addCell(i+1+"");
				table6.addCell(list.get(i).getfName());
				table6.addCell(list.get(i).getlName());
				table6.addCell(list.get(i).getAge());
				table6.addCell(list.get(i).getGender());
				table6.addCell(list.get(i).getSeatNo());
				table6.addCell(list.get(i).getFare());

			}

			document.add(table6);
			
			document.add(Chunk.NEWLINE);
			if (busTicket.getStatus().getValue().equalsIgnoreCase("Booked")) {
				
				document.add(new Paragraph("Ticket Status: "+busTicket.getStatus(),red));
			}else {
				document.add(new Paragraph("Ticket Status: "+"Not Booked",red));
			}
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph("Total Fare: "+busTicket.getTotalFare(),red));
			
			
			document.close();
			writer.close();

		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
//		cell.setBorderColor(r);
		return cell;
	}


	
	@Override
	public BusPaymentResponse saveSeatDetails(BusPaymentInit order, PQService service, String senderUsername,
			User user) {

		BusPaymentResponse response =  new BusPaymentResponse();

		try {
		
			saveBusBooking(order,null,user);

			response.setCode(ResponseStatus.SUCCESS.getValue());
			response.setTripId(order.getSeatDetailsId());
			response.setValid(true);
			response.setMessage("Seat Details save");

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setValid(true);
			response.setMessage("Seat details not saved");
		}

		return response;
	}
	
}
