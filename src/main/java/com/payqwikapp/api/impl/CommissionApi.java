package com.payqwikapp.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.repositories.PQCommissionRepository;

public class CommissionApi implements ICommissionApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQCommissionRepository pqCommissionRepository;

	public CommissionApi(PQCommissionRepository pqCommissionRepository) {
		this.pqCommissionRepository = pqCommissionRepository;
	}

	@Override
	public PQCommission save(PQCommission commission) {
		return pqCommissionRepository.save(commission);
	}

	@Override
	public PQCommission findCommissionByIdentifier(String identifier) {
		PQCommission commission = new PQCommission();
		commission = pqCommissionRepository.findCommissionByIdentifier(identifier);
		return commission;
	}

	@Override
	public PQCommission findCommissionByType(String type) {
		PQCommission commission = new PQCommission();
		commission = pqCommissionRepository.findCommissionByType(type);
		return commission;
	}

	@Override
	public List<PQCommission> findCommissionByService(PQService service) {
		List<PQCommission> commission = new ArrayList<PQCommission>();
		commission = pqCommissionRepository.findCommissionByService(service);
		return commission;
	}

	@Override
	public PQCommission findCommissionByServiceAndAmount(PQService service, double amount) {
		PQCommission commission = pqCommissionRepository.findCommissionByServiceAndAmount(service, amount);
		if (commission == null) {
			commission = new PQCommission();
			commission.setMinAmount(1);
			commission.setMaxAmount(1000000);
			commission.setType("PRE");
			commission.setValue(0);
			commission.setFixed(true);
			commission.setService(service);
			commission.setIdentifier(createCommissionIdentifier(commission));
		}
		return commission;
	}

	@Override
	public double getCommissionValue(PQCommission senderCommission, double amount) {
		double netCommissionValue = 0;
		if (senderCommission.isFixed()) {
			netCommissionValue = senderCommission.getValue();
		} else {
			netCommissionValue = (senderCommission.getValue() * amount) / 100;
		}
		String v = String.format("%.2f", netCommissionValue);
		netCommissionValue = Double.parseDouble(v);
		return netCommissionValue;
	}

	@Override
	public String createCommissionIdentifier(PQCommission commission) {
		String identifier = commission.getType() + "|" + commission.getValue() + "|" + commission.getMinAmount() + "|"
				+ commission.getMaxAmount() + "|" + commission.isFixed() + "|" + commission.getService().getCode();
		return identifier;
	}

	@Override
	public PQCommission createByIdentifier(String identifier,PQService service) {
		PQCommission commission = new PQCommission();
		String []values = identifier.split("\\|");
		commission.setType(values[0]);
		commission.setValue(Double.parseDouble(values[1]));
		commission.setMinAmount(Double.parseDouble(values[2]));
		commission.setMaxAmount(Double.parseDouble(values[3]));
		commission.setFixed(Boolean.parseBoolean(values[4]));
		commission.setService(service);
		return commission;
	}

	@Override
	public PQCommission findOneCommissionByService(PQService service) {
		return pqCommissionRepository.findOneCommissionByService(service);
	}
	
}
