package com.payqwikapp.api.impl;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.payqwikapp.api.IAdminApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.entity.ABankTransfer;
import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.BulkFile;
import com.payqwikapp.entity.EmailLog;
import com.payqwikapp.entity.MBankTransfer;
import com.payqwikapp.entity.MessageLog;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PredictAndWinPayment;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.AnalyticsDTO;
import com.payqwikapp.model.BulkFileDTO;
import com.payqwikapp.model.BulkUploadDTO;
import com.payqwikapp.model.Gender;
import com.payqwikapp.model.PromoCodeDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionReport;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.UserAnalytics;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.UserResponseDTO;
import com.payqwikapp.repositories.ABankTransferRepository;
import com.payqwikapp.repositories.BankTransferRepository;
import com.payqwikapp.repositories.BulkFileRepository;
import com.payqwikapp.repositories.EmailLogRepository;
import com.payqwikapp.repositories.MBankTransferRepository;
import com.payqwikapp.repositories.MessageLogRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.PredictAndWinRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.ConvertUtil;

public class AdminApi implements IAdminApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private final SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private static final String SENDER = "9999999123";
	private static final String IPL_SENDER = "predictAndWin@msewa.com";
	private final MessageLogRepository messageLogRepository;
	private final EmailLogRepository emailLogRepository;
	private final BankTransferRepository bankTransferRepository;
	private final PQTransactionRepository pqTransactionRepository;
	private final MBankTransferRepository mBankTransferRepository;
	private final ABankTransferRepository aBankTransferRepository;
	private final UserRepository userRepository;
	private final PQServiceRepository pqServiceRepository;
	private ISendMoneyApi sendMoneyApi;
	private final BulkFileRepository bulkFileRepository;
	private final PredictAndWinRepository predictAndWinRepository;
	private final ISMSSenderApi smsSenderApi;

	public AdminApi(MessageLogRepository messageLogRepository, EmailLogRepository emailLogRepository,
			BankTransferRepository bankTransferRepository, PQTransactionRepository pqTransactionRepository,
			MBankTransferRepository mBankTransferRepository,ABankTransferRepository aBankTransferRepository, UserRepository userRepository,
			PQServiceRepository pqServiceRepository, ISendMoneyApi sendMoneyApi,
			BulkFileRepository bulkFileRepository,PredictAndWinRepository predictAndWinRepository,ISMSSenderApi smsSenderApi) {
		this.messageLogRepository = messageLogRepository;
		this.emailLogRepository = emailLogRepository;
		this.bankTransferRepository = bankTransferRepository;
		this.pqTransactionRepository = pqTransactionRepository;
		this.mBankTransferRepository = mBankTransferRepository;
		this.aBankTransferRepository = aBankTransferRepository;
		this.userRepository = userRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.sendMoneyApi = sendMoneyApi;
		this.bulkFileRepository = bulkFileRepository;
		this.predictAndWinRepository=predictAndWinRepository;
		this.smsSenderApi=smsSenderApi;
	}

	@Override
	public Page<MessageLog> getMessageLogs(Pageable pageable) {
		return messageLogRepository.getAllMessage(pageable);
	}

	@Override
	public Page<EmailLog> getEmailLogs(Pageable pageable) {
		return emailLogRepository.getAllEmails(pageable);
	}

	@Override
	public Page<User> getOnlineUsers(Pageable pagable) {
		return null;
	}

	@Override
	public Page<User> getActiveUsers(Pageable pageable) {
		return null;
	}

	@Override
	public Page<User> getBlockedUsers(Pageable pageable) {
		return null;
	}

	@Override
	public Page<User> getInactiveUsers(Pageable pageable) {
		return null;
	}

	@Override
	public List<BankTransfer> getAllTransferReports() {
		return (List<BankTransfer>) bankTransferRepository.findAll();
	}

	@Override
	public List<BankTransfer> getAllTransferReportsLimit() {
		Pageable page = new PageRequest(0, 500);
		return (bankTransferRepository.getListByPage(page)).getContent();
	}

	@Override
	public List<BankTransfer> getAllTransferReportsByDate(Date startDate, Date endDate) {
		return bankTransferRepository.getListByDate(startDate, endDate);
	}

	@Override
	public Page<BankTransfer> getAllTransferReportsByDateandPageWise(Pageable pageable, Date startDate, Date endDate) {
		return bankTransferRepository.getListByDateAndPageWise(pageable, startDate, endDate);
	}

	@Override
	public PQTransaction getTransactionByRefNo(String transactionRefNo) {
		return pqTransactionRepository.findByTransactionRefNo(transactionRefNo);
	}

	@Override
	public void saveMerchant(RegisterDTO user) {
		// merchantApi.addMerchant(user);
	}

	@Override
	public void savePromoCode(PromoCodeDTO request) {

	}

	@Override
	public List<MBankTransfer> getAllMBankTransferReports() {
		return (List<MBankTransfer>) mBankTransferRepository.findAllNeftTransaction();
	}

	@Override
	public List<ABankTransfer> getAllABankTransferReports() {
		return (List<ABankTransfer>) aBankTransferRepository.findAllNeftTransaction();
	}

	@Override
	public List<TransactionReport> getTransactionsByDate(Date from, Date to) {
		List<TransactionReport> list = new ArrayList<TransactionReport>();
		List<PQTransaction> trx = pqTransactionRepository.findTransactionsByDate(from, to);
		for (PQTransaction pqTransaction : trx) {
			list.add(convertList(pqTransaction));
		}
		return list;

	}

	// for nikki
	@Override
	public List<TransactionReport> getNikkiTransactionsByDate(Date from, Date to) {

		List<TransactionReport> list = new ArrayList<TransactionReport>();
		// ResponseDTO dto=new ResponseDTO();
		List<PQTransaction> trx = pqTransactionRepository.findTransactionsByDate(from, to);
		for (PQTransaction pqTransaction : trx) {
			list.add(convertList(pqTransaction));

		}
		return list;

	}

	public TransactionReport convertList(PQTransaction t) {
		TransactionReport dto = new TransactionReport();
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setDebit(t.isDebit());
		dto.setDescription(t.getDescription());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setDate(dateTimeFormat.format(t.getCreated()));
		dto.setService(t.getService().getDescription());
		dto.setAccount_id(t.getAccount().getId());
		User u = userRepository.getUserFromAccountId(t.getAccount());
		dto.setUserName(u.getUsername());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setBalance(u.getAccountDetail().getBalance());
		dto.setFirstName(u.getUserDetail().getFirstName());
		return dto;
	}

	@Override
	public List<UserResponseDTO> getUsersByDate(Date from, Date to) {

		List<UserResponseDTO> list = new ArrayList<UserResponseDTO>();
		List<User> userList = userRepository.findUsersByDate(from, to);
		for (User user : userList) {
			list.add(convertUserList(user));

		}
		return list;
	}

	public UserResponseDTO convertUserList(User t) {
		UserResponseDTO response = new UserResponseDTO();
		if (t != null) {
			response.setCreated(format.format(t.getCreated()));
			response.setAuthority(t.getAuthority());
			response.setEmail(t.getUserDetail().getEmail());
			response.setMobileStatus(t.getMobileStatus());
			response.setEmailStatus(t.getEmailStatus());
			response.setMobileToken(t.getMobileToken());
			response.setImage(t.getUserDetail().getImage());
			if (t.getUserDetail().getLocation() != null) {
				response.setPinCode(t.getUserDetail().getLocation().getPinCode());
				response.setCircleName(t.getUserDetail().getLocation().getCircleName());
			} else {
				response.setPinCode("");
				response.setCircleName("");
			}
			String accNo = t.getAccountDetail().getVijayaAccountNumber();
			if (accNo != null) {
				response.setVijayaBankAccount(accNo);
			} else {
				response.setVijayaBankAccount("");
			}
			response.setUserType(t.getUserType());
			response.setAccountType(t.getAccountDetail().getAccountType().getName());
			if (t.getUserDetail().getDateOfBirth() != null) {
				response.setDOB(dateFormater.format(t.getUserDetail().getDateOfBirth()));
			} else {
				response.setDOB("");
			}
			if (t.getUserDetail().getGender() != null) {
				response.setGender(t.getUserDetail().getGender());
			} else {
				response.setGender(Gender.NA);
			}
			response.setBalance(t.getAccountDetail().getBalance());
			response.setPoints(t.getAccountDetail().getPoints());
			response.setUsername(t.getUserDetail().getFirstName());
			response.setMobile(t.getUsername());

		}
		return response;
	}

	@Override
	public List<User> getAllLockedAccountsLists() {

		return userRepository.findAllLockedAccounts();

	}

	public ResponseDTO getLockedAccountTransactions(String user, Date from, Date to) {
		User lockedUser = userRepository.checkForLogin(user);
		List<PQTransaction> transactions = pqTransactionRepository.getTotalTransactions(lockedUser.getAccountDetail());
		for (PQTransaction pqTransaction : transactions) {
			PQService service = pqTransaction.getService();
		}
		return null;
	}

	@Override
	public List<UserAnalytics> getUserListForAnalytics(AnalyticsDTO dto) {
		List<UserAnalytics> list = new ArrayList<>();
		ResponseDTO resp = new ResponseDTO();
		System.err.println("m here1");
		PQService service = pqServiceRepository.findByName(dto.getServiceName());
		System.err.println(service);
		Date parsedTo = null;
		Date parsedFrom = null;

		try {
			parsedTo = dateFormater.parse(dto.getTo());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			parsedFrom = dateFormater.parse(dto.getFrom());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println(parsedFrom + " " + parsedTo);
		List<PQTransaction> transaction = pqTransactionRepository.getUserListAnalytics(parsedTo, parsedFrom, service);
		if (transaction != null) {
			for (PQTransaction pqTransaction : transaction) {

				System.err.println("m herererehudsfnjjl");
				User user = userRepository.findByAccountDetails(pqTransaction.getAccount());
				System.err.println(user);
				UserAnalytics analytics = new UserAnalytics();
				analytics.setCreated(format.format(pqTransaction.getCreated()));
				analytics.setDescription(pqTransaction.getDescription());
				analytics.setStatus(pqTransaction.getStatus().getValue());
				analytics.setTransactionRefNo(pqTransaction.getTransactionRefNo());
				analytics.setUsername(user.getUsername());
				analytics.setAmount(Double.toString(pqTransaction.getAmount()));
				System.err.println("this is" + analytics);
				list.add(analytics);

			}
		} else {
			System.err.println("transaction null");
		}
		resp.setDetails(list);
		return list;
	}

	@Override
	public ResponseDTO getTransactionsCountByMonthWise() {
		ResponseDTO resp = new ResponseDTO();
		Map<String, String> result = new LinkedHashMap<String, String>();
		try {
			YearMonth thisMonth1 = YearMonth.now();
			YearMonth thisMonth = thisMonth1.minusMonths(5);
			for (int i = 7; i > 1; i--) {
				YearMonth lastMonth = thisMonth.plusMonths(1);
				/*
				 * List<User>
				 * user=userRepository.findAllAuthenticateUsers(dateTimeFormat.
				 * parse(thisMonth + "-01 00:00"),
				 * dateTimeFormat.parse(lastMonth + "-01 00:00")); for(User u :
				 * user){
				 */
				long count = pqTransactionRepository.findCountByMonth(dateTimeFormat.parse(thisMonth + "-01 00:00"),
						dateTimeFormat.parse(lastMonth + "-01 00:00"), TransactionType.DEFAULT);
				DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MM");
				int ss = Integer.valueOf(thisMonth.format(monthYearFormatter));
				String Month = new DateFormatSymbols().getMonths()[ss - 1];
				result.put(Month, count + "");
				thisMonth = lastMonth;
			}
			resp.setDetails(result);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public ResponseDTO getUserCountByMonthWise() {
		ResponseDTO resp = new ResponseDTO();
		Map<String, String> result = new LinkedHashMap<String, String>();
		try {
			YearMonth thisMonth1 = YearMonth.now();
			YearMonth thisMonth = thisMonth1.minusMonths(5);
			for (int i = 7; i > 1; i--) {
				YearMonth lastMonth = thisMonth.plusMonths(1);
				long count = userRepository.findUserCountByMonth(dateTimeFormat.parse(thisMonth + "-01 00:00"),
						dateTimeFormat.parse(lastMonth + "-01 00:00"));
				DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MM");
				int ss = Integer.valueOf(thisMonth.format(monthYearFormatter));
				String Month = new DateFormatSymbols().getMonths()[ss - 1];
				result.put(Month, count + "");
				thisMonth = lastMonth;
			}
			resp.setDetails(result);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public ResponseDTO getTransactionsCountByMonthWiseSingleUser(String username) {
		ResponseDTO resp = new ResponseDTO();
		Map<String, String> result = new LinkedHashMap<String, String>();
		try {
			YearMonth thisMonth1 = YearMonth.now();
			YearMonth thisMonth = thisMonth1.minusMonths(5);
			for (int i = 7; i > 1; i--) {
				YearMonth lastMonth = thisMonth.plusMonths(1);
				User u = userRepository.findByUsername(username);
				long count = pqTransactionRepository.findCountByMonthUser(dateTimeFormat.parse(thisMonth + "-01 00:00"),
						dateTimeFormat.parse(lastMonth + "-01 00:00"), u.getAccountDetail());
				DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MM");
				int ss = Integer.valueOf(thisMonth.format(monthYearFormatter));
				String Month = new DateFormatSymbols().getMonths()[ss - 1];
				result.put(Month, count + "");
				thisMonth = lastMonth;
			}
			resp.setDetails(result);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public void uploadBulkFile(BulkUploadDTO bulkUpload) {
		/* TODO switch file name here */
		Iterator<SendMoneyMobileDTO> iterator = bulkUpload.getMobileDTO().iterator();
		if (bulkUpload.getMobileDTO() != null && !bulkUpload.getMobileDTO().isEmpty()) {
			while (iterator.hasNext()) {
				SendMoneyMobileDTO dto = iterator.next();
				String serviceCode = sendMoneyApi.prepareSendMoney(dto.getMobileNumber());
				PQService service = pqServiceRepository.findServiceByCode(serviceCode);
				if (service != null && Double.parseDouble(dto.getAmount()) > 0)
					sendMoneyApi.sendMoneyMobile(dto, SENDER, service);
			}
		}
	}

	@Override
	public void saveBulkFile(BulkUploadDTO bulkUpload) {
		/* TODO switch file name here */
		Iterator<SendMoneyMobileDTO> iterator = bulkUpload.getMobileDTO().iterator();
		if (bulkUpload.getMobileDTO() != null && !bulkUpload.getMobileDTO().isEmpty()) {
			while (iterator.hasNext()) {
				SendMoneyMobileDTO dto = iterator.next();
				BulkFile file = bulkFileRepository.findByMobileNumber(dto.getMobileNumber());
				if (file == null) {
					file = new BulkFile();
					file.setMobileNumber(dto.getMobileNumber());
					file.setAmount(dto.getAmount());
					file.setMessage(dto.getMessage());
					file.setStatus(Status.Open);
					bulkFileRepository.save(file);
				} else if (file.getStatus() == Status.Close) {
					file = new BulkFile();
					file.setMobileNumber(dto.getMobileNumber());
					file.setAmount(dto.getAmount());
					file.setMessage(dto.getMessage());
					file.setStatus(Status.Open);
					bulkFileRepository.save(file);
				}
			}
		}
	}

	@Override
	public List<BulkFileDTO> getBulkUploadFiles() {
		List<BulkFile> users = bulkFileRepository.findAllFiles(Status.Open);
		return ConvertUtil.convertFileList(users);
	}

	@Override
	public ResponseDTO saveBulkRecord(BulkUploadDTO bulkUpload) {
		/* TODO switch file name here */
		ResponseDTO result = new ResponseDTO();
		List<SendMoneyMobileDTO> responseList = new ArrayList<>();
		SendMoneyMobileDTO dtoList = new SendMoneyMobileDTO();
		Iterator<SendMoneyMobileDTO> iterator = bulkUpload.getMobileDTO().iterator();
		if (bulkUpload.getMobileDTO() != null && !bulkUpload.getMobileDTO().isEmpty()) {
			while (iterator.hasNext()) {
				SendMoneyMobileDTO dto = iterator.next();
				BulkFile file = bulkFileRepository.findByMobileNumberWithSatus(dto.getMobileNumber(), Status.Open);
				if (file != null && file.getStatus() == Status.Open) {
					file.setStatus(Status.Approved);
					bulkFileRepository.save(file);
				}
			}
		}
		List<BulkFile> file = bulkFileRepository.findAllFiles(Status.Approved);
		for (BulkFile files : file) {
			dtoList.setAmount(files.getAmount());
			dtoList.setMobileNumber(files.getMobileNumber());
			dtoList.setStatus(files.getStatus());
			responseList.add(dtoList);
		}
		result.setDetails(responseList);
		return result;
	}

	@Override
	public void uploadBulkFiles() {
		List<BulkFile> fileList = bulkFileRepository.findAllFiles(Status.Approved);
		SendMoneyMobileDTO mobiledto = new SendMoneyMobileDTO();
		Iterator<BulkFile> iterator = fileList.iterator();
		if (fileList != null && !fileList.isEmpty()) {
			while (iterator.hasNext()) {
				BulkFile dto = iterator.next();
				String serviceCode = sendMoneyApi.prepareSendMoney(dto.getMobileNumber());
				PQService service = pqServiceRepository.findServiceByCode(serviceCode);
				if (service != null && Double.parseDouble(dto.getAmount()) > 0)
					mobiledto = new SendMoneyMobileDTO();
				mobiledto.setAmount(dto.getAmount());
				mobiledto.setMessage(dto.getMessage());
				mobiledto.setMobileNumber(dto.getMobileNumber());
				sendMoneyApi.sendMoneyMobileBulkFile(mobiledto, SENDER, service);
			}
		}
		System.err.println("no approved files are there to load money .");

	}

	@Override
	public List<MBankTransfer> getAllMerchantTransferReportsByDate(Date startDate, Date endDate) {
		return mBankTransferRepository.getListByDate(startDate, endDate);
	}

	@Override
	public List<ABankTransfer> getAllAgentTransferReportsByDate(Date startDate, Date endDate) {
		return aBankTransferRepository.getListByDate(startDate, endDate);
	}
	@Override
	public void savePredictAndWinFile(BulkUploadDTO bulkUpload) {
		/* TODO switch file name here */
		System.err.println("inside predict and win api");
		Iterator<SendMoneyMobileDTO> iterator = bulkUpload.getMobileDTO().iterator();
		if (bulkUpload.getMobileDTO() != null && !bulkUpload.getMobileDTO().isEmpty()) {
			while (iterator.hasNext()) {
				SendMoneyMobileDTO dto = iterator.next();
				PredictAndWinPayment file = predictAndWinRepository.findByMobileNumber(dto.getMobileNumber());
				if (file == null) {
					file = new PredictAndWinPayment();
					file.setMobileNumber(dto.getMobileNumber());
					file.setAmount(dto.getAmount());
					file.setMessage(dto.getMessage());
					file.setStatus(Status.Open);
					predictAndWinRepository.save(file);
				}else if(file.getStatus()==Status.Close){
					file = new PredictAndWinPayment();
					file.setMobileNumber(dto.getMobileNumber());
					file.setAmount(dto.getAmount());
					file.setMessage(dto.getMessage());
					file.setStatus(Status.Open);
					predictAndWinRepository.save(file);
				}
			}
		}
	}
	

	@Override
	public void sendMoneyToWinners() {
		User u = userRepository.findByUsername(IPL_SENDER);
		PQAccountDetail pq = u.getAccountDetail();
		if (pq.getBalance() >= 0) {
			List<PredictAndWinPayment> fileList = predictAndWinRepository.findAllFiles(Status.Open);
			SendMoneyMobileDTO mobiledto = new SendMoneyMobileDTO();
			Iterator<PredictAndWinPayment> iterator = fileList.iterator();
			if (fileList != null && !fileList.isEmpty()) {
				while (iterator.hasNext()) {
					PredictAndWinPayment dto = iterator.next();
					PQService service = pqServiceRepository.findServiceByCode("PAW");
					if (service != null && Double.parseDouble(dto.getAmount()) > 0) {
						mobiledto = new SendMoneyMobileDTO();
						mobiledto.setAmount(dto.getAmount());
						mobiledto.setMessage(dto.getMessage());
						mobiledto.setMobileNumber(dto.getMobileNumber());
						sendMoneyApi.sendMoneyToWinners(mobiledto, IPL_SENDER, service);
					} else {
						System.err.println("service is null.amount must be greater than 0");
					}
				}
			} else {
				System.err.println("no approved files are there to send Money to Predict winner.");
			}
		} else {
			User user = userRepository.findByUsername("9066165729");
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.LESS_BALANCE_ALERT, user, null);
		}
	}
}
