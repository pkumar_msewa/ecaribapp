package com.payqwikapp.api.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import com.payqwikapp.api.ISavaariApi;
import com.payqwikapp.api.ISavaariBookingApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.SavaariAuth;
import com.payqwikapp.entity.SavaariBooking;
import com.payqwikapp.entity.SavaariTripDetails;
import com.payqwikapp.entity.SavaariUserDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.BookSavaariDTO;
import com.payqwikapp.model.SavaariTokenDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.SavaariAuthRepository;
import com.payqwikapp.repositories.SavaariBookingRepository;
import com.payqwikapp.repositories.SavaariTripDetailsRepository;
import com.payqwikapp.repositories.SavaariUserDetailsRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.CommonValidation;

public class SavaariApi implements ISavaariApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
	 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private MessageSource messageSource;

	private final UserRepository userRepository;
	private final SavaariAuthRepository savaariAuthRepository;
	private final SavaariTripDetailsRepository savaariTripDetailsRepository;
	private final SavaariUserDetailsRepository savaariUserDetailsRepository;
	private final SavaariBookingRepository savaariBookingRepository;
	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final ISavaariBookingApi bookingApi;

	public SavaariApi(UserRepository userRepository, SavaariAuthRepository savaariAuthRepository,
			SavaariTripDetailsRepository savaariTripDetailsRepository,
			SavaariUserDetailsRepository savaariUserDetailsRepository,
			SavaariBookingRepository savaariBookingRepository, IUserApi userApi, ITransactionApi transactionApi,
			ISavaariBookingApi bookingApi) {
		this.userRepository = userRepository;
		this.savaariAuthRepository = savaariAuthRepository;
		this.savaariTripDetailsRepository = savaariTripDetailsRepository;
		this.savaariUserDetailsRepository = savaariUserDetailsRepository;
		this.savaariBookingRepository = savaariBookingRepository;
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.bookingApi = bookingApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public ResponseDTO saveAccessToken(SavaariTokenDTO request, String username) {
		ResponseDTO resp = new ResponseDTO();
		try {
			User user = userRepository.findByUsername(username);
			SavaariAuth exist = savaariAuthRepository.findByAccountDetails(user.getAccountDetail());
			if (exist == null) {
				exist = new SavaariAuth();
				exist.setSavaariToken(request.getAccessToken());
				exist.setAccount(user.getAccountDetail());
				savaariAuthRepository.save(exist);
				resp.setStatus(ResponseStatus.SUCCESS);
				resp.setMessage("Access token saved");
			} else {
				exist.setSavaariToken(request.getAccessToken());
				savaariAuthRepository.save(exist);
				resp.setStatus(ResponseStatus.SUCCESS);
				resp.setMessage("Access token saved");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public ResponseDTO getAccessToken(SavaariTokenDTO request, String username) {
		ResponseDTO resp = new ResponseDTO();
		User user = userRepository.findByUsername(username);
		SavaariAuth exist = savaariAuthRepository.findByAccountDetails(user.getAccountDetail());
		if (exist != null) {
			resp.setDetails(exist.getSavaariToken());
			resp.setStatus(ResponseStatus.SUCCESS);
			resp.setMessage("Get Access Token");
		} else {
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setMessage("Could Not Found Token");
		}
		return resp;
	}

	@Override
	public void saveSavaariTicket(BookSavaariDTO req, User user, PQService service) {

		try {
			 Date d1=simpleDateFormat.parse(req.getPickUpDateTime());
		      String dbDateTime= format.format(d1);
		     Date d2= format.parse(dbDateTime);
			SavaariTripDetails tripDetail = new SavaariTripDetails();
			tripDetail.setLocallity(req.getLocalityId());
			tripDetail.setDestinationCity(req.getDestinationCity());
			tripDetail.setSourceCity(req.getSourceCity());
			tripDetail.setPickupDateTime(d2);
			System.err.println("time ::"+ tripDetail.getPickupDateTime());
			savaariTripDetailsRepository.save(tripDetail);

			SavaariUserDetails userDetail = new SavaariUserDetails();
			userDetail.setAddress(req.getPickupAddress());
			userDetail.setEmailId(req.getCustomerEmail());
			userDetail.setMobileNo(req.getCustomerMobile());
			userDetail.setName(req.getCustomerName());
			savaariUserDetailsRepository.save(userDetail);

			// analysis

			// BusResponseEntity busrequest = new BusResponseEntity();
			// busrequest.setIPAddress(req.getIpAddress());
			// busrequest.setDate(req.getBookingDate());
			// busrequest.setRequest(req.toJSON().toString());
			// busrequest.setBus(busDetail);
			// busResponseRepository.save(busrequest);
			// analysis
			double amount = Double.parseDouble(req.getPrePayment());
			String transactionRefNo = req.getTransactionRefNo();
			bookingApi.initiateSavaariBooking(amount,
					"Savaari Ticket Initiated Rs " + req.getPrePayment() + " from " + req.getSourceCity()
							+ " to RefNo. " + req.getTransactionRefNo(),
					service, transactionRefNo, user.getUsername(), StartupUtil.SAVAARI, "", "");

			PQTransaction transaction = transactionApi.getTransactionByRefNo(transactionRefNo + "D");
			if (transaction != null) {
				String busTransactionRefNo = req.getTransactionRefNo();
				SavaariBooking busTransaction = new SavaariBooking();
				busTransaction.setAmount(req.getPrePayment());
				busTransaction.setCarType(req.getCarType());
				busTransaction.setTransctionRefNo(busTransactionRefNo);
				busTransaction.setStatus(Status.Initiated);
				busTransaction.setDescription(transaction.getDescription());
				busTransaction.setPickupDateTime(d2);
				busTransaction.setSavaariDetails(userDetail);
				busTransaction.setUserDetails(user);
				busTransaction.setAccount(user.getAccountDetail());
				busTransaction.setTrip(tripDetail);
				savaariBookingRepository.save(busTransaction);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void bookSavaariTicket(BookSavaariDTO dto, User user) throws ParseException {
		System.err.println("----------------------");

		PQTransaction transaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo() + "D");
		System.err.println("Trasaction :: " + transaction);
		if (transaction != null) {
			System.err.println("TRANSation failure " + transaction.getTransactionRefNo());
			bookingApi.successSavaariBooking(dto.getTransactionRefNo(),
					"Savaari Ticket Booked Rs " + dto.getPrePayment() + " from " + dto.getSourceCity()
							+ " to bookingId " + dto.getBookingId() + " AND reservationId." + dto.getReservationId());
		}

		SavaariBooking bookingTransaction = savaariBookingRepository.findByTransactionRefNo(dto.getTransactionRefNo());
		if (bookingTransaction != null) {
			if ((bookingTransaction.getStatus().equals(Status.Initiated))) {
				System.err.println("inside this");
				System.err.println("status ::" + bookingTransaction.getStatus());
				SavaariTripDetails tripDetail = bookingTransaction.getTrip();
					Date date = new Date(dto.getReturnDate());
					String parsedate = formatDate.format(date);
				Date newDate = formatDate.parse(parsedate);
				tripDetail.setReturnDateTime(newDate);
				tripDetail.setSourceCity(dto.getSourceCity());
				tripDetail.setLocallity(dto.getLocalityId());
				tripDetail.setPickUpLocation(dto.getPickUpLocation());
				savaariTripDetailsRepository.save(tripDetail);
				PQTransaction updateTransaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo() + "D");
				bookingTransaction.setDescription(updateTransaction.getDescription());
				bookingTransaction.setCarType(dto.getCarType());
				bookingTransaction.setCarName(dto.getCarName());
				bookingTransaction.setBookingId(dto.getBookingId());
				bookingTransaction.setReservationId(dto.getReservationId());
				bookingTransaction.setStatus(Status.Booked);
				savaariBookingRepository.save(bookingTransaction);
			}

		}
	}

	@Override
	public void failBookSavaariTicket(BookSavaariDTO req) {
		PQTransaction transaction = transactionApi.getTransactionByRefNo(req.getTransactionRefNo() + "D");
		if (transaction != null) {
			bookingApi.failedSavaariBooking(req.getTransactionRefNo());
		}
		SavaariBooking bookingTransaction = savaariBookingRepository.findByTransactionRefNo(req.getTransactionRefNo());
		if (bookingTransaction != null) {
			bookingTransaction.setStatus(Status.Failed);
			savaariBookingRepository.save(bookingTransaction);
		}
	}
	
	@Override
	public void CancelBookSavaariTicket(BookSavaariDTO dto) {
		PQTransaction transaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo() + "D");
		double amount=Double.parseDouble(dto.getPrePayment());
		System.err.println("amount ::" + amount);
		System.err.println("transaction ::" + transaction);
		if (transaction != null) {
			System.err.println("inside this");
			bookingApi.CancelledSavaariBooking(dto.getTransactionRefNo(),amount,"Savaari Ticket Cancelled Rs " + dto.getPrePayment() + " from " + dto.getSourceCity()
			+ " to bookingId " + dto.getBookingId() + " AND reservationId." + dto.getReservationId());
		}
		SavaariBooking bookingTransaction = savaariBookingRepository.findByTransactionRefNo(dto.getTransactionRefNo());
		if (bookingTransaction != null) {
			PQTransaction updateTransaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo() + "D");
			bookingTransaction.setDescription(updateTransaction.getDescription());
			bookingTransaction.setStatus(Status.Cancelled);
			savaariBookingRepository.save(bookingTransaction);
		}
	}

	@Override
	public boolean checkBalance(User user, BookSavaariDTO req) {
		boolean valid = false;
		double amount = Double.parseDouble(req.getPrePayment());
		if (CommonValidation.balanceCheck(user.getAccountDetail().getBalance(), amount)) {
			valid = true;
		}
		return valid;
	}

	/* To Do checkbookingId */
	@Override
	public boolean checkBookingId(User user, BookSavaariDTO dto) {
		SavaariBooking booking = savaariBookingRepository.findByBookingId(dto.getBookingId(),Status.Booked,user);
		if (booking == null) {
			return false;
		}
		return true;

	}

	@Override
	public boolean checkUserBookingId(User u, BookSavaariDTO dto) {
		User user = userApi.findByUserName(u.getUsername());
		if (user != null) {
			SavaariBooking booking = savaariBookingRepository.findByBookingId(dto.getBookingId(),Status.Booked,user);
			if (booking == null) {
				return false;
			}
		} else {
			return false;
		}
		return true;

	}
	
    @Override
    public List<SavaariBooking> getBookingList(User u,Status status) {
    	User user=userRepository.findByUsername(u.getUsername());
    	if(user!=null){
        List<SavaariBooking> transactionList = savaariBookingRepository.getTransactions(user,status);
        return transactionList;
    	}
		return null;
    }
    
  //Savaari
    @Override
    public Date getLastTranasactionTimeByStatus(String bookingId, Status status) {
        return savaariBookingRepository.getTimeStampOfLastTransactionByStatus(bookingId,status);
    }
	
}
