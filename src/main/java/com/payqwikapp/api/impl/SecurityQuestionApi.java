package com.payqwikapp.api.impl;

import java.util.List;

import com.payqwikapp.api.ISecurityQuestionApi;
import com.payqwikapp.entity.SecurityQuestion;
import com.payqwikapp.model.SecurityQuestionDTO;
import com.payqwikapp.repositories.SecurityQuestionRepository;
import com.payqwikapp.util.ConvertUtil;

public class SecurityQuestionApi implements ISecurityQuestionApi {

	private final SecurityQuestionRepository securityQuestionRepository;

	public SecurityQuestionApi(SecurityQuestionRepository securityQuestionRepository) {
		this.securityQuestionRepository = securityQuestionRepository;
	}


	@Override
	public List<SecurityQuestionDTO> getAllQuestionDTO() {
		List<SecurityQuestion> securityQuestions = securityQuestionRepository.findAllQuestions();
		if (securityQuestions != null && securityQuestions.size() != 0) {
			List<SecurityQuestionDTO> secuirtyQuestionDTOs = ConvertUtil.convertSecurityQuestionList(securityQuestions);
			return secuirtyQuestionDTOs;
		}
		return null;
	}

}
