package com.payqwikapp.api.impl;
import com.instantpay.model.request.AdlabsOrderRequest;
import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.IAdlabsApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.AdlabsProduct;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.repositories.AdlabsProductRepository;
import com.payqwikapp.util.StartupUtil;

public class AdlabsApi implements IAdlabsApi {

	private final AdlabsProductRepository adlabsProductRepository;
	private final ITransactionApi transactionApi;

	public AdlabsApi(AdlabsProductRepository adlabsProductRepository, ITransactionApi transactionApi) {

		this.adlabsProductRepository = adlabsProductRepository;
		this.transactionApi = transactionApi;
	}

	@Override
	public String adlabspaymentInitiate(AdlabsOrderRequest dto, String senderUser, PQService service, String receiverUser) {
		
		System.err.println(" UserName:::::::::::::::"+senderUser);
		String transactionRefNo = System.currentTimeMillis() + "";

		transactionApi.initiateAdlabsPayment(Double.parseDouble(dto.getAmount()),
				"Adlabs Ticket Rs " + dto.getAmount() , service, transactionRefNo, senderUser, receiverUser);
		
		
		

		return transactionRefNo;
	}
	@Override
	public TransactionResponse adlabspaymentSuccess(AdlabsOrderRequest dto, String username, PQService service,
			User u) {
		if (dto.getCode().equalsIgnoreCase("S00"))
		{
			System.err.println("####################################################################################");
			transactionApi.successAdlabsPayment(dto.getTransactionRefNo());
			System.err.println("####################################################################################");
			AdlabsProduct req = new AdlabsProduct();
			req.setAmount(dto.getAmount());
			req.setUser(dto.getUser());
			req.setSuccess_payment(dto.getSuccess_payment());
			req.setSuccess_profile(dto.getSuccess_profile());
			adlabsProductRepository.save(req);
			System.err.println("####################################################################################");

			

			
		}
			  else {
					System.err.println("####################################################################################");

					System.err.println("####################################################################################");

					System.err.println("####################################################################################");

					System.err.println("####################################################################################");

					System.err.println("####################################################################################");

			transactionApi.failedAdlabsPayment(dto.getTransactionRefNo());
		}

		return null;
	}

}