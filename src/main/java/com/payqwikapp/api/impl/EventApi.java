package com.payqwikapp.api.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import com.instantpay.model.request.TransactionRequest;
import com.instantpay.model.response.TransactionResponse;
import com.instantpay.util.IPayConvertUtil;
import com.instantpay.util.InstantPayConstants;
import com.payqwikapp.api.IEventApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.EventAccessToken;
import com.payqwikapp.entity.EventDetails;
import com.payqwikapp.entity.EventTicketDetails;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.EventDetailsDTO;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.EventAccessTokenRepository;
import com.payqwikapp.repositories.EventDetailsRepository;
import com.payqwikapp.repositories.EventTicketDetailsRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.util.StartupUtil;
import com.thirdparty.util.EventsType;

public class EventApi implements IEventApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;
	
	private final UserRepository userRepository;
	private final EventDetailsRepository eventDetailsRepository;
	private final EventTicketDetailsRepository eventTicketDetailsRepository;
	private final EventAccessTokenRepository eventAccessTokenRepository;
	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	
	public EventApi(UserRepository userRepository, EventDetailsRepository eventDetailsRepository,
							EventTicketDetailsRepository eventTicketDetailsRepository,
							EventAccessTokenRepository eventAccessTokenRepository, IUserApi userApi,
							ITransactionApi transactionApi) {
		this.userRepository = userRepository;
		this.eventDetailsRepository = eventDetailsRepository;
		this.eventTicketDetailsRepository = eventTicketDetailsRepository;
		this.eventAccessTokenRepository = eventAccessTokenRepository;
		this.userApi = userApi;
		this.transactionApi = transactionApi;
	}
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	@Override
	public ResponseDTO saveAccessToken(EventDetailsDTO request, String contactNo) {
		ResponseDTO resp = new ResponseDTO();
		try {
		User user = userRepository.findByUsername(contactNo);
		EventAccessToken exist = eventAccessTokenRepository.findByAccountDetails(user.getAccountDetail());
		if(exist == null) {
			exist = new EventAccessToken();
			exist.setAccessToken(request.getAccessToken());
			exist.setAccountDetail(user.getAccountDetail());
			eventAccessTokenRepository.save(exist);
			resp.setStatus(ResponseStatus.SUCCESS);
			resp.setMessage("Access token saved");
			System.out.println("-----ACCESS TOKEN SAVED-----");
			System.out.println("Access Token Saved for the 1st time");
		}
		else {
			exist.setAccessToken(request.getAccessToken());
			eventAccessTokenRepository.save(exist);
			resp.setStatus(ResponseStatus.SUCCESS);
			resp.setMessage("Access token saved");
			System.out.println("-----ACCESS TOKEN SAVED-----");
			System.out.println("Access Token updated");
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public ResponseDTO saveEventDetails(EventDetailsDTO request, String contactNo) {
		ResponseDTO resp = new ResponseDTO();
		System.err.println("----------------------INSIDE SAVE EVENT DETAILS API IN APP-------------");
		try {
		User user = userRepository.findByUsername(contactNo);
		EventDetails e = new EventDetails();
		/*	EventDetails exist = eventDetailsRepository.findByUser(user);
		if(exist == null) {
			System.err.println("EVENT DETAILS COULD NOT SAVED, ACCESS TOKEN NOT SAVED");
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setMessage("Event Details couldn't saved");
		} else {
			System.out.println("orderId :" +request.getOrderId());
			System.out.println("eventSignUpId :" +request.getEventSignUpId());
			System.out.println("city :" +request.getCity());
			System.out.println("state :" +request.getState());
			System.out.println("country :" +request.getCountry());
			System.out.println("category :" +request.getCategory());
			System.out.println("eventId :" +request.getEventId());
			System.out.println("eventName :" +request.getEventName());
			System.out.println("eventVanue :" +request.getEventVanue());
			System.out.println("noOfAttendees :" +request.getNoOfAttendees());
			System.out.println("netAmount :" +request.getNetAmount());
			System.out.println("ticketId :" +request.getTicketId());
			System.out.println("ticketType :" +request.getTicketType());
			System.out.println("ticketName :" +request.getTicketName());
			System.out.println("quantity :" +request.getQuantity());
			System.out.println("price :" +request.getPrice());
			
>>>>>>> origin/as_3.0
			EventTicketDetails ticket = new EventTicketDetails();
			ticket.setTicketId(request.getTicketId());
			ticket.setTicketType(request.getTicketType());
			ticket.setTicketName(request.getTicketName());
			ticket.setQuantity(request.getQuantity());
			ticket.setPrice(request.getPrice());
			ticket.setDescription(request.getDescription());
			eventTicketDetailsRepository.save(ticket);
<<<<<<< HEAD
			exist.setEventId(request.getEventId());
			exist.setEventType(EventsType.Paid);
			exist.setEventName(request.getEventName());
			exist.setCity(request.getCity());
			exist.setState(request.getState());
			exist.setCountry(request.getCountry());
			exist.setCategory(request.getCategory());
			exist.setVanue(request.getEventVanue());
			exist.setTotalQuantity(request.getNoOfAttendees());
			exist.setTotalAmount(request.getNetAmount());
			exist.setOrderId(request.getOrderId());
			exist.setEventSignUpId(request.getEventSignUpId());
			exist.setTicketDetails(ticket);
			eventDetailsRepository.save(exist);
			resp.setStatus(ResponseStatus.SUCCESS);
			resp.setMessage("Event Details saved");
			System.err.println("EVENT DETAILS SAVED SUCCESSFULLY, ACCESS TOKEN FOUND");
		} else {
			System.err.println("EVENT DETAILS COULD NOT SAVED, ACCESS TOKEN NOT SAVED");
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setMessage("Event Details couldn't saved");
		}
=======
			
			e.setEventId(request.getEventId());
			e.setEventType(EventsType.Paid);
			e.setEventName(request.getEventName());
			e.setCity(request.getCity());
			e.setState(request.getState());
			e.setCountry(request.getCountry());
			e.setCategory(request.getCategory());
			e.setVanue(request.getEventVanue());
			e.setTotalQuantity(request.getNoOfAttendees());
			e.setTotalAmount(request.getNetAmount());
			e.setOrderId(request.getOrderId());
			e.setEventSignUpId(request.getEventSignUpId());
			e.setTicketDetails(ticket);
			eventDetailsRepository.save(e);
			resp.setStatus(ResponseStatus.SUCCESS);
			resp.setMessage("Event Details saved");
			System.err.println("EVENT DETAILS SAVED SUCCESSFULLY, ACCESS TOKEN FOUND");
			}*/
		
		EventTicketDetails ticket = new EventTicketDetails();
		ticket.setTicketId(request.getTicketId());
		ticket.setTicketType(request.getTicketType());
		ticket.setTicketName(request.getTicketName());
		ticket.setQuantity(request.getQuantity());
		ticket.setPrice(request.getNetAmount());
		ticket.setDescription(request.getDescription());
		eventTicketDetailsRepository.save(ticket);
		
		e.setAccountDetail(user.getAccountDetail());
		e.setEventId(request.getEventId());
		e.setEventType(EventsType.Paid);
		e.setEventName(request.getEventName());
		e.setCity(request.getCity());
		e.setState(request.getState());
		e.setCountry(request.getCountry());
		e.setCategory(request.getCategory());
		e.setVanue(request.getEventVanue());
		e.setTotalQuantity(request.getNoOfAttendees());
		e.setTotalAmount(request.getNetAmount());
		e.setOrderId(request.getOrderId());
		e.setEventSignUpId(request.getEventSignUpId());
		e.setTicketDetails(ticket);
		eventDetailsRepository.save(e);
		resp.setStatus(ResponseStatus.SUCCESS);
		resp.setMessage("Event Details saved");
		System.err.println("EVENT DETAILS SAVED SUCCESSFULLY, ACCESS TOKEN FOUND");
		} catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public ResponseDTO eventPayment(EventDetailsDTO dto, String username,String status) {
		User user = userRepository.findByUsername(username);
		
		System.err.println("STATUS==========+++++++++++"+status);
		if(status.equalsIgnoreCase("true")) {
			
			
			System.err.println("-------------------- CALLING BOOKING STATUS -----------------------");
			System.err.println("Access Token " +dto.getAccessToken());
				System.err.println("---------------------------CALLING SUCCESS EVENT PAYMENT API IN APP -------------------------");
				transactionApi.successEventPayment(dto.getTransactionRefNo());
		} else {
			
			System.err.println("---------------------------CALLING FAILURE EVENT PAYMENT API IN APP -------------------------");
			transactionApi.failedEventPayment(dto.getTransactionRefNo());
		}
	return null;
	}
	@Override
	
	public String eventPaymentinitate(EventDetailsDTO dto, String username, PQService service, String status)
	
	{
		
		User user = userRepository.findByUsername(username);
		 final String transactionRefNo = System.currentTimeMillis() + "";
		
		transactionApi.initiateEventPayment(dto.getNetAmount(),
				"Payment of Rs." + dto.getNetAmount() + " from" + user.getUsername()+ " was successful your transaction id is ." + transactionRefNo,
				service, transactionRefNo, username,
				StartupUtil.MERA_EVENTS);
		
		return transactionRefNo;
	}
}