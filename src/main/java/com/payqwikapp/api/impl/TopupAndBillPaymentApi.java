package com.payqwikapp.api.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import com.instantpay.model.request.TransactionRequest;
import com.instantpay.model.response.TransactionResponse;
import com.instantpay.util.IPayConvertUtil;
import com.instantpay.util.InstantPayConstants;
import com.payqwikapp.api.IDataConfigApi;
import com.payqwikapp.api.ITopupAndBillPaymentApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.CallBackRequest;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TopupType;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.PayQwikUtil;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;

public class TopupAndBillPaymentApi implements ITopupAndBillPaymentApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final com.instantpay.api.ITransactionApi ipayTransactionApi;
	private final com.instantpay.api.IValidationApi ipayValidationApi;
	private final ITransactionApi transactionApi;
	private final IUserApi userApi;
	private final IDataConfigApi dataConfigApi;
	private final IFRMApi frmApi;

	public TopupAndBillPaymentApi(com.instantpay.api.ITransactionApi ipayTransactionApi,
			com.instantpay.api.IValidationApi ipayValidationApi, ITransactionApi transactionApi,IUserApi userApi,IDataConfigApi dataConfigApi,IFRMApi frmApi) {
		this.ipayTransactionApi = ipayTransactionApi;
		this.ipayValidationApi = ipayValidationApi;
		this.transactionApi = transactionApi;
		this.userApi = userApi;
		this.dataConfigApi = dataConfigApi;
		this.frmApi = frmApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public TransactionResponse prepaidTopup(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue,DataConfig config,String description) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.prepaidRequest(dto);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),description,service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission, netCommissionValue);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = dto.getServiceProvider().substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			userApi.saveTopupRequest(dto,transactionRefNo);
			request.setMobileNo(dto.getMobileNo());
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setArea(dto.getArea());
			request.setTopupType(dto.getTopupType());
			request.setAdditionalInfo("Requested by ::" + username);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
			if (request.getSpKey().equalsIgnoreCase("VATP")) {
				request.setOptional5(InstantPayConstants.OUTLET_ID);
			}
			userApi.saveTopupRequest(dto,transactionRefNo);
			 response = ipayTransactionApi.request(request, service);
			 logger.info("success or failure response from instantpay ::" + response.isSuccess());
		}
		frmResponse.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
			User u = userApi.findByUserName(username);
		frmResponse = frmApi.decideRecharge(dto, u);
		}
		if (frmResponse.isSuccess() && response.isSuccess()) {
			transactionApi.successBillPayment(transactionRefNo,commission, netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}



	@Override
	public TransactionResponse dthBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.dthRequest(dto);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"DTH bill payment of "+service.getDescription()+" to "+ dto.getDthNo(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = serviceProvider.substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			userApi.saveDTHRequest(dto,transactionRefNo);
			request.setDthNo(dto.getDthNo());
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setTopupType(TopupType.Dth);
			request.setAdditionalInfo("Requested by ::" + username);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
			if (request.getSpKey().equalsIgnoreCase("VATP")) {
				request.setOptional5(InstantPayConstants.OUTLET_ID);
			}
			userApi.saveDTHRequest(dto,transactionRefNo);
			 response = ipayTransactionApi.request(request, service);
		}
		frmResponse.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
			User u = userApi.findByUserName(username);
		frmResponse = frmApi.decideRecharge(dto, u);
		}
		if (frmResponse.isSuccess() && response.isSuccess()) {
			transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}

	@Override
	public TransactionResponse landlineBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.landlineRequest(dto);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"Landline bill payment of "+PayQwikUtil.CURRENCY+" "+dto.getAmount() + " to " + dto.getStdCode() + dto.getLandlineNumber(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = serviceProvider.substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setTopupType(TopupType.Landline);
			request.setLandlineNumber(dto.getLandlineNumber());
			request.setStdCode(dto.getStdCode());
			request.setAccountNumber(dto.getAccountNumber());
			request.setAdditionalInfo("Requested by ::" + username);
			userApi.saveLandlineRequest(dto,transactionRefNo);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
		userApi.saveLandlineRequest(dto,transactionRefNo);
		if (request.getSpKey().equalsIgnoreCase("VBGL")) {
			request.setOptional2(dto.getAccountNumber());
			request.setOptional3("LLI");
		}
		if (request.getSpKey().equalsIgnoreCase("VATP")) {
			request.setOptional5(InstantPayConstants.OUTLET_ID);
		}
		response = ipayTransactionApi.request(request, service);
		}
		frmResponse.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
			User u = userApi.findByUserName(username);
		frmResponse = frmApi.decideRecharge(dto, u);
		}
		if (frmResponse.isSuccess() && response.isSuccess()) {
			response = transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}

	@Override
	public TransactionResponse electricityBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.electricityRequest(dto,service.isBbpsEnabled(),username);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"Electricity bill payment of "+PayQwikUtil.CURRENCY +" "+dto.getAmount() + " to " + dto.getAccountNumber(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = serviceProvider.substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setTopupType(TopupType.Electricity);
			request.setAccountNumber(dto.getAccountNumber());
			request.setBillingUnit(dto.getBillingUnit());
			request.setCityName(dto.getCityName());
			request.setCycleNumber(dto.getCycleNumber());
			request.setProcessingCycle(dto.getProcessingCycle());
			request.setAdditionalInfo("Requested by ::" + username);
			userApi.saveECityRequest(dto,transactionRefNo);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
		userApi.saveECityRequest(dto,transactionRefNo);
		 response = ipayTransactionApi.request(request, service);
		}
		frmResponse.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
			User u = userApi.findByUserName(username);
		frmResponse = frmApi.decideRecharge(dto, u);
		}
		if (frmResponse.isSuccess() && response.isSuccess()) {
			response = transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}
	
	
	@Override
	public TransactionResponse gasBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.gasRequest(dto,service.isBbpsEnabled(),username);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"Gas bill payment of "+PayQwikUtil.CURRENCY +" "+dto.getAmount() + " to " + dto.getAccountNumber(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = serviceProvider.substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setTopupType(TopupType.Gas);
			request.setAccountNumber(dto.getAccountNumber());
			request.setAdditionalInfo("Requested by ::" + username);
			userApi.saveGasRequest(dto,transactionRefNo);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
		userApi.saveGasRequest(dto,transactionRefNo);
		 response = ipayTransactionApi.request(request, service);
		}
		if (response.isSuccess()) {
			response = transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}

/*	@Override
	public TransactionResponse gasBillPayment(GasBillPaymentDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		TransactionRequest request = IPayConvertUtil.gasRequest(dto);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"Gas bill payment of "+PayQwikUtil.CURRENCY +" "+dto.getAmount() + " to " + dto.getAccountNumber(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		userApi.saveGasRequest(dto,transactionRefNo);
		TransactionResponse response = ipayTransactionApi.request(request, service);
		if (response.isSuccess()) {
			response = transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}*/

	@Override
	public TransactionResponse insuranceBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.insuranceRequest(dto);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"Insurance bill payment of "+PayQwikUtil.CURRENCY +" "+dto.getAmount() + " to " + dto.getPolicyNumber(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = serviceProvider.substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setTopupType(TopupType.Insurance);
			request.setAccountNumber(dto.getPolicyNumber());
			request.setPolicyDate(dto.getPolicyDate());
			request.setAdditionalInfo("Requested by ::" + username);
			userApi.saveInsuranceRequest(dto,transactionRefNo);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
			userApi.saveInsuranceRequest(dto,transactionRefNo);
		 response = ipayTransactionApi.request(request, service);
		}
		frmResponse.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
			User u = userApi.findByUserName(username);
		frmResponse = frmApi.decideRecharge(dto, u);
		}
		if (frmResponse.isSuccess() && response.isSuccess()) {
			response = transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}
	
	
	@Override
	public TransactionResponse waterBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue) {
		FRMResponseDTO frmResponse = new FRMResponseDTO();
		TransactionRequest request = IPayConvertUtil.insuranceRequest(dto);
		String transactionRefNo = System.currentTimeMillis() + "";
		request.setAgentId(transactionRefNo);
		transactionApi.initiateBillPayment(Double.parseDouble(dto.getAmount()),
				"Insurance bill payment of "+PayQwikUtil.CURRENCY +" "+dto.getAmount() + " to " + dto.getPolicyNumber(),
				service, transactionRefNo, username,
				InstantPayConstants.USERNAME,commission,netCommissionValue);
		DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
		TransactionResponse response = null;
		if(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex())) {
			String serviceProvider =dto.getServiceProvider();
			String serviceProvider1 = serviceProvider.substring(1, serviceProvider.length())+"IN";
			dto.setFromMdex(true);
			request.setAmount(dto.getAmount());
			request.setTransactionId(transactionRefNo);
			request.setServiceProvider(serviceProvider1);
			request.setTopupType(TopupType.Insurance);
			request.setAccountNumber(dto.getPolicyNumber());
			request.setPolicyDate(dto.getPolicyDate());
			request.setAdditionalInfo("Requested by ::" + username);
			userApi.saveInsuranceRequest(dto,transactionRefNo);
			response = ipayTransactionApi.rechargeRequestToMdex(request);
		}else {
			userApi.saveInsuranceRequest(dto,transactionRefNo);
		 response = ipayTransactionApi.request(request, service);
		}
		frmResponse.setSuccess(true);
		if(DeploymentConstants.PRODUCTION){
			User u = userApi.findByUserName(username);
		frmResponse = frmApi.decideRecharge(dto, u);
		}
		if (frmResponse.isSuccess() && response.isSuccess()) {
			response = transactionApi.successBillPayment(transactionRefNo,commission,netCommissionValue,response.isSuspicious(),response);
		} else {
			transactionApi.failedBillPayment(transactionRefNo);
		}
		return response;
	}
		
	@Override
	public ResponseDTO callbackHandler(CallBackRequest dto) {
		ResponseDTO result = new ResponseDTO();
		if (dto.getRes_code().equalsIgnoreCase("TXN")) {
			transactionApi.successBillPayment(dto.getAgent_id(),null,0,false,new TransactionResponse());
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Transaction successful.");
		} else {
			transactionApi.failedBillPayment(dto.getAgent_id());
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Failed, Please try again later.");
		}
		return result;
	}

}
