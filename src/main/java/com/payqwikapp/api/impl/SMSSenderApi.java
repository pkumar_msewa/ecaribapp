package com.payqwikapp.api.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.MessageLog;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.entity.WoohooCardDetails;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.model.travel.AgentTicketDetailsDTOFlight;
import com.payqwikapp.model.travel.TicketDetailsDTOFlight;
import com.payqwikapp.model.travel.bus.AgentTicketDetailsDTO;
import com.payqwikapp.model.travel.bus.TicketDetailsDTO;
import com.payqwikapp.repositories.MessageLogRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.PayQwikUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SMSSenderApi implements ISMSSenderApi, MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	
	private static String SMSURL_ROUTE = DeploymentConstants.WEB_URL + "/SendSMS"; // FGM
																					// TEST
																					// Server
	private static String SMSURL_VBANK = DeploymentConstants.WEB_URL + "/SendSMS/Now";
	
	public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private MessageLogRepository messageLogRepository;
	private VelocityEngine velocityEngine;

	public void setSmsSender(VelocityEngine velocityEngine, MessageLogRepository messageLogRepository) {
		this.velocityEngine = velocityEngine;
		this.messageLogRepository = messageLogRepository;
	}

	public void setMessageLogRepository(MessageLogRepository messageLogRepository) {
		this.messageLogRepository = messageLogRepository;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public void sendUserSMS(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendUserSMSNew(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendAnotherUserSMS(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
			sendAnotherUserSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
			sendAnotherUserSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendAlertSMS(SMSAccount smsAccount, String smsTemplate, String destination, String additionalInfo) {
		Map model = new HashMap();
		model.put("info", additionalInfo);
		System.err.print(smsTemplate);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			alertSMSNew(smsAccount, destination, smsMessage, smsTemplate);
		} else {
			alertSMS(smsAccount, destination, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendMerchantSMS(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendPromotionalSMS(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo) {
		Map model = new HashMap();
		model.put("info", additionalInfo);
		model.put("user", user);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendTransactionSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			String additionalInfo) {
		double amount=BigDecimal.valueOf(transaction.getCurrentBalance()).setScale(2, RoundingMode.HALF_UP).doubleValue();
		String created=dateFormate.format(transaction.getCreated());
		Map model = new HashMap();
		model.put("user", user);
		model.put("transaction", transaction);
		model.put("created",created);
		model.put("currentBalance", amount);
		model.put("info", additionalInfo);
		if (transaction.getService() != null && transaction.getService().getCode() != null) {
			if (transaction.getService().getCode().equals("SMM") || transaction.getService().getCode().equals("SMU")) {
				String[] parts = transaction.getDescription().split(" ");
				model.put("sender", parts[4]);
			}
		}
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}
	
	@Override
	public void sendBescomTransactionSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			String consumerId) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("transaction", transaction);
		model.put("info", consumerId);
		
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	private void sendSMS(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", destination.getUserDetail().getContactNo());
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSMSNew(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", destination.getUserDetail().getContactNo());
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	private void sendAnotherUserSMS(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						String[]usernames= getUserNames();
						for(int user=0; user<usernames.length; user++){
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", usernames[user]);
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					  }
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendAnotherUserSMSNew(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						String[]usernames= getUserNames();
						for(int user=0; user<usernames.length; user++){
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber",usernames[user]);
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					  }
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendGCISMS(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate, final ProcessGiftCardDTO giftCard) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", giftCard.getReceiversMobileNumber());
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendGCISMSNew(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate, final ProcessGiftCardDTO giftCard) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", giftCard.getReceiversMobileNumber());
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void alertSMS(final SMSAccount smsAccount, final String destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", destination);
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveAlertLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void alertSMSNew(final SMSAccount smsAccount, final String destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", destination);
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveAlertLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSMSGenericNew(final SMSAccount smsAccount, final User destination, final String mobileNumber,
			final String smsMessage, final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", mobileNumber);
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSMSGeneric(final SMSAccount smsAccount, final User destination, final String mobileNumber,
			final String smsMessage, final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", mobileNumber);
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveLog(User user, String smsTemplate, String smsMessage, String smsResponse, SMSAccount smsAccount) {
		MessageLog mgslog = new MessageLog();
		mgslog.setExecutionTime(new Date());
		mgslog.setDestination(user.getUsername());
		mgslog.setMessage(smsMessage);
		mgslog.setTemplate(smsTemplate);
		mgslog.setResponse(smsResponse);
		if(DeploymentConstants.PRODUCTION){
			mgslog.setSender("VIJBNK");
			}else{
				mgslog.setSender(smsAccount.getUsername());	
			}
		messageLogRepository.save(mgslog);
	}

	private void saveAlertLog(String destination, String smsTemplate, String smsMessage, String smsResponse,
			SMSAccount smsAccount) {
		MessageLog mgslog = new MessageLog();
		mgslog.setExecutionTime(new Date());
		mgslog.setDestination(destination);
		mgslog.setMessage(smsMessage);
		mgslog.setTemplate(smsTemplate);
		mgslog.setResponse(smsResponse);
		if(DeploymentConstants.PRODUCTION){
			mgslog.setSender("VIJBNK");
			}else{
				mgslog.setSender(smsAccount.getUsername());	
			}
		messageLogRepository.save(mgslog);
	}

	@Override
	public void promotionalEmails(String number, String message) {
		Map model = new HashMap();
		model.put("model", message);
		User user = new User();
		user.setUsername(number);
		UserDetail userDetaills = new UserDetail();
		userDetaills.setContactNo(number);
		user.setUserDetail(userDetaills);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + SMSTemplate.PROMOTIONAL_SMS, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(SMSAccount.PAYQWIK_PROMOTIONAL, user, smsMessage, SMSTemplate.PROMOTIONAL_SMS);
		} else {
			sendSMS(SMSAccount.PAYQWIK_PROMOTIONAL, user, smsMessage, SMSTemplate.PROMOTIONAL_SMS);
		}
	}

	@Override
	public void sendKYCSMS(SMSAccount smsAccount, String smsTemplate, User user, String mobileNumber,
			String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("info", additionalInfo);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSGenericNew(smsAccount, user, mobileNumber, smsMessage, smsTemplate);
		} else {
			sendSMSGeneric(smsAccount, user, mobileNumber, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendUserGCISMS(SMSAccount smsAccount, String smsTemplate, GiftCardTransactionResponse response, User u,
			ProcessGiftCardDTO dto) {
		// TODO Auto-generated method stub

		Map model = new HashMap();
		model.put("userName", dto.getReceiversName());
		model.put("expiryDate", response.getExpiryDate());
		model.put("brandName", dto.getBrandName());
		model.put("amount", dto.getAmount());
		model.put("voucherNumber", response.getVoucherNumber());
		model.put("voucherPin", response.getVoucherPin());
		model.put("receiversName", dto.getReceiversName());
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendGCISMSNew(smsAccount, u, smsMessage, smsTemplate, dto);
		} else {
			sendGCISMS(smsAccount, u, smsMessage, smsTemplate, dto);
		}

	}

	@Override
	public void sendUserQWIKRSMS(SMSAccount smsAccount, String smsTemplate, User user, QwikrPayRequest dto) {

		Map model = new HashMap();
		model.put("OrderId", dto.getOrderId());
		model.put("TransactionRefNo", dto.getTransactionRefNo());
		model.put("Amount", dto.getAmount());
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		}

	}

	@Override
	public boolean sendUserBillpaySMS(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo,
			String description) {
		boolean valid = false;
		try {
			Map model = new HashMap();
			model.put("user", user);
			model.put("info", additionalInfo);
			model.put("description", description);
			String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
					PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
			if (DeploymentConstants.PRODUCTION) {
				sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
			} else {
				sendSMS(smsAccount, user, smsMessage, smsTemplate);
			}
			valid = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valid;
	}

	@Override
	public void sendReferNearnMsg(SMSAccount smsAccount, String smsTemplate, User user, String additionalInfo) {
		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("info", additionalInfo);
		model.put("description", additionalInfo);

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendBusTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			TicketDetailsDTO additionalInfo) {

		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForBusNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForBus(smsAccount, user, smsMessage, smsTemplate);
		}

	}
	
	@Override
	public void sendBusTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			AgentTicketDetailsDTO additionalInfo) {

		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForBusNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForBus(smsAccount, user, smsMessage, smsTemplate);
		}

	}

	private void sendSMSForBusNew(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", destination.getUsername());
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSMSForBus(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						System.err.println("User Mobile for sms:: " + destination.getUsername());

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", destination.getUsername());
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendFlightTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			FlightDetails additionalInfo) {

		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForBusNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForBus(smsAccount, user, smsMessage, smsTemplate);
		}

	}

	@Override
	public void sendFlightTicketPaymentGatewaySMS(SMSAccount smsAccount, String smsTemplate, User user,
			String transaction, FlightDetails additionalInfo) {

		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForBusNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForBus(smsAccount, user, smsMessage, smsTemplate);
		}

	}

	@Override
	public void sendRedeemPointsSMS(SMSAccount smsAccount, String smsTemplate, User user, double balance, long point,
			PQTransaction transaction) {

		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("balance", balance);
		model.put("point", point);
		model.put("transaction", transaction);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForRedeemPointNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForRedeemPoint(smsAccount, user, smsMessage, smsTemplate);
		}

	}

	private void sendSMSForRedeemPointNew(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", destination.getUsername());
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSMSForRedeemPoint(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						System.err.println("User Mobile for sms:: " + destination.getUsername());

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", destination.getUsername());
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void sendFlightTicketSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			TicketDetailsDTOFlight additionalInfo) {
		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);
		model.put("detDate", additionalInfo.getFlightresponse().get(0).getDepartureDate());
		model.put("src", additionalInfo.getFlightresponse().get(0).getOrigin());
		model.put("dest",
				additionalInfo.getFlightresponse().get(additionalInfo.getFlightresponse().size() - 1).getDestination());

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForBusNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForBus(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	// Travelkhana
	@Override
	public void sendTransactionSMSOrderFood(SMSAccount smsAccount, String smsTemplate, User user,
			PQTransaction transaction, String additionalInfo, long userOrderId, TKOrderDetail orderDetail) {
		Map model = new HashMap();
		model.put("user", user);
		model.put("transaction", transaction);
		model.put("info", additionalInfo);
		model.put("userOrderId", userOrderId);
		model.put("orderDetail", orderDetail);
		if (transaction.getService() != null && transaction.getService().getCode() != null) {
			if (transaction.getService().getCode().equals("SMM") || transaction.getService().getCode().equals("SMU")) {
				String[] parts = transaction.getDescription().split(" ");
				model.put("sender", parts[4]);
			}
		}
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}

	@Override
	public void sendBulkFileSMS(SMSAccount smsAccount, String smsTemplate, User user, PQTransaction transaction,
			String additionalInfo) {
		Map model = new HashMap();
		LocalDate currentDate = LocalDate.now();
	    Month m = currentDate.getMonth();
		model.put("user", user);
		model.put("transaction", transaction);
		model.put("info", m);
		if (transaction.getService() != null && transaction.getService().getCode() != null) {
			if (transaction.getService().getCode().equals("SMM") || transaction.getService().getCode().equals("SMU")) {
				String[] parts = transaction.getDescription().split(" ");
				model.put("sender", parts[4]);
			}
		}
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMS(smsAccount, user, smsMessage, smsTemplate);
		}
	}
	
	
	@Override
	public void sendUserWoohooSMS(SMSAccount smsAccount, String smsTemplate, WoohooCardDetails dto, User u) {
		// TODO Auto-generated method stub

		    Map model = new HashMap();
		    model.put("userName", dto.getContanctNo());
		    model.put("expiryDate", dto.getExpiry_date());
			model.put("brandName",dto.getCardName());
			model.put("amount", dto.getCard_price());
			model.put("voucherNumber",dto.getCardnumber());
			model.put("voucherPin",dto.getPin_or_url());
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendWoohooSMSNew(smsAccount, u, smsMessage, smsTemplate, dto);
		} else {
			sendWoohooSMS(smsAccount, u, smsMessage, smsTemplate, dto);
		}

	}
	
	@Override
	public void sendWoohooSMS(SMSAccount smsAccount, String smsTemplate, WoohooCardDetails dto, User u) {
		// TODO Auto-generated method stub

		Map model = new HashMap();
		model.put("user", u);
		model.put("transaction", dto);
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendWoohooSMSNew(smsAccount, u, smsMessage, smsTemplate, dto);
		} else {
			sendWoohooSMS(smsAccount, u, smsMessage, smsTemplate, dto);
		}

	}
	
	private void sendWoohooSMS(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate, final WoohooCardDetails giftCard) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", giftCard.getContanctNo());
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendWoohooSMSNew(final SMSAccount smsAccount, final User destination, final String smsMessage,
			final String smsTemplate, final WoohooCardDetails giftCard) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {

						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber", giftCard.getContanctNo());
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveLog(destination, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendFlightTicketSMS(SMSAccount smsAccount, String smsTemplate, User user,
			PQTransaction transaction, AgentTicketDetailsDTOFlight additionalInfo) {

		Map model = new HashMap();
		model.put("user", user.getUserDetail().getFirstName());
		model.put("transaction", transaction);
		model.put("info", additionalInfo);
		model.put("detDate", additionalInfo.getFlightresponse().get(0).getDepartureDate());
		model.put("src", additionalInfo.getFlightresponse().get(0).getOrigin());
		model.put("dest",
				additionalInfo.getFlightresponse().get(additionalInfo.getFlightresponse().size() - 1).getDestination());

		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			sendSMSForBusNew(smsAccount, user, smsMessage, smsTemplate);
		} else {
			sendSMSForBus(smsAccount, user, smsMessage, smsTemplate);
		}
		
	}

	@Override
	public void senderBankTransferSMS(SMSAccount smsAccount, String smsTemplate, AgentSendMoneyBankDTO dto,
			String mobileNo) {
		double amount=BigDecimal.valueOf(Double.parseDouble( dto.getAmount())).setScale(2, RoundingMode.HALF_UP).doubleValue();
		Map model = new HashMap();
		model.put("amount",amount);
		model.put("senderName",dto.getSenderName());
		model.put("receiverName",dto.getAccountName());
		model.put("senderMobileNo",dto.getSenderMobileNo());
		model.put("receiverMobileNo",dto.getReceiverMobileNo());
		model.put("transactionRefNo",dto.getTransactionRef());
		model.put("BankName",dto.getBankName());
		model.put("accountNumber",dto.getAccountNumber());
		model.put("ifscCode",dto.getIfscCode());
		model.put("transactionRefNo",dto.getTransactionRef());
		String smsMessage = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				PayQwikUtil.SMS_TEMPLATE + smsTemplate, model);
		if (DeploymentConstants.PRODUCTION) {
			bankTransferSmsNew(smsAccount, mobileNo, smsMessage, smsTemplate);
		} else {
			bankTransferSms(smsAccount, mobileNo, smsMessage, smsTemplate);
		}
	}
	
	private void bankTransferSmsNew(final SMSAccount smsAccount, final String mobileNo, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_VBANK);
						JSONObject json = new JSONObject();
						json.put("mobileNumber",mobileNo);
						json.put("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
					    	saveBankTransferLog(mobileNo, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void bankTransferSms(final SMSAccount smsAccount,final String mobileNo, final String smsMessage,
			final String smsTemplate) {
		try {
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						Client client = Client.create();
						WebResource webResource = client.resource(SMSURL_ROUTE);
						MultivaluedMapImpl smsData = new MultivaluedMapImpl();
						smsData.add("username", smsAccount.getUsername());
						smsData.add("password", smsAccount.getPassword());
						smsData.add("type", smsAccount.getType());
						smsData.add("dlr", smsAccount.getDlr());
						smsData.add("destination", mobileNo);
						smsData.add("source", smsAccount.getSource());
						smsData.add("message", smsMessage);
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,
								smsData);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							saveBankTransferLog(mobileNo, smsTemplate, smsMessage, strResponse, smsAccount);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void saveBankTransferLog(String mobileNo, String smsTemplate, String smsMessage, String smsResponse, SMSAccount smsAccount) {
		MessageLog mgslog = new MessageLog();
		mgslog.setExecutionTime(new Date());
		mgslog.setDestination(mobileNo);
		mgslog.setMessage(smsMessage);
		mgslog.setTemplate(smsTemplate);
		mgslog.setResponse(smsResponse);
		if(DeploymentConstants.PRODUCTION){
			mgslog.setSender("VIJBNK");
			}else{
				mgslog.setSender(smsAccount.getUsername());	
			}
		messageLogRepository.save(mgslog);
	}
	
	private String[] getUserNames() {
		
		String []unames={"9765325827","8050662639","9980672277","9986202601","8050581012","7529989758","7022620747"};
		return unames;
	}
}
