package com.payqwikapp.api;

import java.util.List;

import com.payqwikapp.entity.PromoCode;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.PromoCodeDTO;
import com.payqwikapp.model.PromoTransactionDTO;
import com.payqwikapp.model.RedeemDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IPromoCodeApi {

	void addPromocode(PromoCodeDTO request);

	List<PromoCodeDTO> getAll();

	void savePromotionCode(PromoCodeDTO promoDTO);

	List<PromoCodeDTO> getpromotionCode();

	boolean isValidRegistrationDate(PromoCode code,User user);

	void updatePromotionCode(PromoCodeDTO promoDTOs);

	void deletePromotionCode(Long id);

	PromoCodeDTO findPromotionCodeById(Long id);
	
	PromoCode checkPromoCodeValid(String code, String promoId);

	double calculateNetAmountOfCoupon(PromoCode code,double amount);

	boolean checkPromoCodeLength(String code);
	
	boolean checkPromoCodeExpireDate(String code);
	
	boolean balanceCheck(double amount);
	
	boolean checkActivePromoCode(String code);
	
	boolean redeemCode(User userId , PromoCode code, double amount);
	
	PromoTransactionDTO findTransactionDateByService(UserDTO user, PromoCode code);
	
	boolean checkTransaction(PromoTransactionDTO transDetail, PromoCode code);

	ResponseDTO process(RedeemDTO dto, User user);

	ResponseDTO processInviteFriend(PromoCode code, User u);

	ResponseDTO processRegistration(PromoCode code,User u);

	ResponseDTO processRegistrationServices(PromoCode code,User u);

	ResponseDTO processServices(PromoCode code,User u);

	ResponseDTO processReferEarn(RedeemDTO dto, User user);

	ResponseDTO processReferEarnServices(PromoCode code, User u);

	boolean checkTransactionRefernEarn(PromoTransactionDTO transDetail, PromoCode code);

	PromoCodeDTO getPromocodeById(String promoCodeId);


}
