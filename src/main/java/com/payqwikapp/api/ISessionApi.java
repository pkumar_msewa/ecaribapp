package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.UserSessionDTO;
import com.payqwikapp.session.UserDetailsWrapper;

public interface ISessionApi {

	void registerNewSession(String sessionId, UserDetailsWrapper principal);

	void removeSession(String tokenKey);

	UserSession getUserSession(String sessionId);
	
	UserSession getActiveUserSession(String sessionId);

	boolean getUserOnlineStatus(User user);

	void refreshSession(String sessionId);

	List<UserSession> getAllUserSession(long userId, boolean includeExpiredSessions);

	void expireSession(String sessionId);

	boolean checkActiveSession(User user);

	long countActiveSessions();

	Page<User> findOnlineUsers(Pageable page);

	List<User> findOnlineUsers();

	List<User> findOnlineUsersBetween(Date start,Date from);

	Page<UserSession> findActiveSessions(Pageable page);

	void registerNewSession(String sessionId, User principal);

	List<UserSessionDTO> getAllActiveUser();

	void clearAllSessionForUser(User user);

}
