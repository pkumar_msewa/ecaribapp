package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import org.json.JSONException;

import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.FlightTravellers;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.FlightDetailsDTO;
import com.payqwikapp.model.FlightPaymentRequest;
import com.payqwikapp.model.FlightPaymentResponse;
import com.payqwikapp.model.travel.flight.CompareCountry;
import com.payqwikapp.model.travel.flight.FlghtTicketResp;
import com.payqwikapp.model.travel.flight.FlightPayment;
import com.payqwikapp.model.travel.flight.TicketsResp;

public interface IFlightApi {

	public FlightDetails saveFlightBooking(FlightPaymentRequest order,PQTransaction transaction,User u);
	public FlightPaymentResponse processPayment(FlightPaymentRequest dto,User user)throws Exception;
	FlightPaymentResponse placeOrder(FlightPaymentRequest order, PQService service, User senderUser);
	List<FlightDetailsDTO> getFlightDetailsForAdmin();
	void processPaymentGateWaySuccess(FlightPaymentRequest dto, User u) throws JSONException;
	void processPaymentGateWayFailure(FlightPaymentRequest dto, User u);
	
	/* Code Done By Rohit End */


	/*##############################################################################################################*/

	/* Fresh Code Started By Subir */
	
	FlightPaymentResponse flightInit(FlightPayment order, PQService service, User senderUser);
	public FlightPaymentResponse flightPayment(FlightPayment dto,User user)throws Exception;
	FlightPaymentResponse paymentGateWaySuccess(FlightPayment dto, User user) throws JSONException;
	void paymentGateWayFailure(FlightPayment dto, User u);
	public List<FlghtTicketResp> getAllTickets(String userName);
	public List<FlightTicket> getFlightDetailForAdmin();
	public void cronforSaveCityList();
	public List<FlightAirLineList> getAllAirLineList();
	public List<FlightTravellers> getFlightTravellersForAdmin(long flightTicketId);
	public List<FlightTicket> getFlightDetailForAdminByDate(Date from, Date to);
	
	public boolean comCountry(CompareCountry dto);
	TicketsResp getFlightTravellersNameForAdmin(long flightTicketId);
	PQTransaction initiateTransaction(String transactionRefNo, double amount, PQService service, User senderUser,
			String convenienceFee, String baseFare);
	FlightTicket saveFlight(FlightPayment order, PQTransaction transaction, User user, String baseFare);
}
