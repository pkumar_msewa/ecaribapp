package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.PartnerDetail;
import com.payqwikapp.entity.PartnerInstall;
import com.payqwikapp.model.PDeviceUpdateDTO;
import com.payqwikapp.model.error.PDeviceUpdateError;

public interface IPartnerApi {

    PDeviceUpdateError saveDetails(PDeviceUpdateDTO dto);

	List<PartnerInstall> listInstallsByPartnerName(PartnerDetail partnerDetail);

	List<PartnerInstall> listInstallsByPartnerNameFilter(PartnerDetail partnerDetail, Date From, Date to);
}
