package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.payqwikapp.entity.ABankTransfer;
import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.EmailLog;
import com.payqwikapp.entity.MBankTransfer;
import com.payqwikapp.entity.MessageLog;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.AnalyticsDTO;
import com.payqwikapp.model.BulkFileDTO;
import com.payqwikapp.model.BulkUploadDTO;
import com.payqwikapp.model.PromoCodeDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.TransactionReport;
import com.payqwikapp.model.UserAnalytics;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.UserResponseDTO;

public interface IAdminApi {

	Page<MessageLog> getMessageLogs(Pageable pageable);
	Page<EmailLog> getEmailLogs(Pageable pageable);
	Page<User> getOnlineUsers(Pageable pagable);
	Page<User> getActiveUsers(Pageable pageable);
	Page<User> getBlockedUsers(Pageable pageable);
	Page<User> getInactiveUsers(Pageable pageable);
	List<BankTransfer> getAllTransferReports();
	List<BankTransfer> getAllTransferReportsLimit();
	List<BankTransfer> getAllTransferReportsByDate(Date startDate, Date endDate);
	PQTransaction getTransactionByRefNo(String transactionRefNo);
	void saveMerchant(RegisterDTO user);
	void savePromoCode(PromoCodeDTO request);
	List<MBankTransfer> getAllMBankTransferReports();
	List<TransactionReport> getTransactionsByDate(Date from,Date to);
	List<UserResponseDTO> getUsersByDate(Date from, Date to);
	List<User> getAllLockedAccountsLists();
	List<UserAnalytics> getUserListForAnalytics(AnalyticsDTO dto);
	List<TransactionReport> getNikkiTransactionsByDate(Date from, Date to);
	ResponseDTO getTransactionsCountByMonthWise();
	ResponseDTO getUserCountByMonthWise();
	void uploadBulkFile(BulkUploadDTO dto);
	ResponseDTO getTransactionsCountByMonthWiseSingleUser(String username);
	void saveBulkFile(BulkUploadDTO bulkUpload);
	List<BulkFileDTO> getBulkUploadFiles();
	ResponseDTO saveBulkRecord(BulkUploadDTO bulkUpload);
	void uploadBulkFiles();
	List<MBankTransfer> getAllMerchantTransferReportsByDate(Date startDate, Date endDate);
	List<ABankTransfer> getAllABankTransferReports();
	List<ABankTransfer> getAllAgentTransferReportsByDate(Date startDate, Date endDate);
	Page<BankTransfer> getAllTransferReportsByDateandPageWise(Pageable pageable, Date startDate, Date endDate);
	void savePredictAndWinFile(BulkUploadDTO bulkUpload);
	void sendMoneyToWinners();
	
}
