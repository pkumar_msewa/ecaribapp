package com.payqwikapp.api;

import com.payqwikapp.entity.ImagicaOrders;
import com.payqwikapp.entity.ImagicaSession;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.model.*;

import java.util.Date;

public interface IImagicaApi {

    void createSession();

    long  getSessionValidityMins();

    boolean isSessionTimeout(Date tokenGenerationTime);

    ImagicaLoginResponse requestLogin(ImagicaLoginRequest request);

    ImagicaAuthDTO getLatestCredentials();

    ImagicaOrderResponse placeOrder(ImagicaOrder order, PQService service,String senderUsername);

    PQTransaction initiateTransaction(String transactionRefNo,double amount,PQService service,String senderUsername);

    double calculateTotalAmount(ImagicaOrder order);

    ImagicaOrders saveImagicaOrders(ImagicaOrder order,PQTransaction transaction);

    ImagicaPaymentResponse processPayment(ImagicaPaymentDTO dto);
}
