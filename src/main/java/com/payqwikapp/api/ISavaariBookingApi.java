package com.payqwikapp.api;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;

public interface ISavaariBookingApi {

	void initiateSavaariBooking(double amount, String description, PQService service, String transactionRefNo, String sender,
			String receiver, String json, String favourite);

	PQTransaction getTransactionByRefNo(String transactionRefNo);
	
	void failedSavaariBooking(String transactionRefNo);

	void successSavaariBooking(String transactionRefNo, String description);

	void CancelledSavaariBooking(String transactionRefNo, double amount, String description);
}
