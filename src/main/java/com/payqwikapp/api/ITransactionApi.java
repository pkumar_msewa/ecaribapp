package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PromoCode;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.Voucher;
import com.payqwikapp.model.WoohooCardDetailsDTO;
import com.payqwikapp.model.AddVoucherDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.LoadMoneyRequest;
import com.payqwikapp.model.MicroPaymentServiceStatus;
import com.payqwikapp.model.OnePayResponse;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionListDTO;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.UpiMobileRedirectReq;
import com.payqwikapp.model.UserDetailDTO;
import com.payqwikapp.model.VNetStatusResponse;
import com.payqwikapp.model.WoohooTransactionRequest;
import com.payqwikapp.model.WoohooTransactionResponse;
import com.payqwikapp.model.admin.EBSStatusResponseDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ITransactionApi {

	PQTransaction findByTransactionRefNoAndStatus(String transactionReferenceNumber, Status status);

	List<TransactionListDTO> getmVisaTransaction();

	List<PQTransaction> getMonthlyTransaction(int month);

	List<PQTransaction> getDailyTransactionBetweeen(Date from, Date to);

	List<PQTransaction> getDailyTransactionBetweenForAccount(Date from, Date to, PQAccountDetail account);

	List<PQTransaction> getDailyTransactionByDate(Date from);

	List<PQTransaction> getDailyTransactionByDateStatus(Date from, Date to, Status status, PQService service);

	List<PQTransaction> getDailyTransactionByDateForAccount(Date from, PQAccountDetail account);

	List<PQTransaction> listTransactionByPaging(PagingDTO dto);

	List<PQTransaction> listTransactionByServiceAndDate(Date from, Date to, PQServiceType serviceType, boolean debit,
			TransactionType transactionType, Status status);

	Long countDailyTransactionByDateForAccount(Date from, PQAccountDetail account);

	Long countDailyTransactionByDate(Date from);

	double countTotalReceivedEBS();

	double countTotalReceivedEBSNow();

	double countTotalReceivedVNET();

	double countTotalReceivedVNETNow();

	double countTotalPay();

	double countTotalTopupNow();

	List<PQTransaction> getLoadMoneyTransactions(Status status);

	PQTransaction getLastReverseTransactionOfUser(PQAccountDetail account);

	PQTransaction getTransactionByRefNo(String transactionRefNo);
	
	PQTransaction getTransactionByRetrivalReferenceNo(String retrivalReferenceNo);

	Date getLastTranasactionTimeStamp(PQAccountDetail account);

	Date getLastTranasactionTimeStampByStatus(PQAccountDetail account, Status status);

	Page<PQTransaction> getTotalTransactions(Pageable page);

	PQTransaction processRefundTransaction(String transactionRefNo);

	Page<PQTransaction> getTotalTransactionsOfUser(Pageable page, String user);

	List<PQTransaction> transactionListByService(PQService service);

	List<PQTransaction> transactionListByServiceAndDebit(PQService service, boolean debit);

	List<PQTransaction> getTotalSuccessfulTransactionsOfUser(String user);

	PQTransaction getLastTransactionsOfUser(User user);

	void revertSendMoneyOperations();

	int updateFavouriteTransaction(String transactionRefNo, boolean favourite);

	void initiateSendMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	void successSendMoney(String transactionRefNo);
	
	void failedSendMoney(String transactionRefNo);

	void initiateSharePoints(long points, String description, String transactionRefNo, String senderUsername,
			String receiverUsername);

	void successSharePoints(String transactionRefNo);

	void initiateMerchantPayment(double amount, String description, String remarks,PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, boolean isPG);

	void successMerchantPayment(String transactionRefNo, boolean isPG);

	void failedMerchantPayment(String transactionRefNo);

	OnePayResponse getTransaction(String transactionRefNo, User u);

	void initiateVisaPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String json);

	void successVisaPayment(String transactionRefNo, String authReferenceNo, String retrivalReferenceNo);

	void failedVisaPayment(String transactionRefNo);

	void initiateBillPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername,PQCommission senderCommission,double netCommissionValue);
	
	TransactionResponse successBillPayment(String transactionRefNo,PQCommission senderCommission,double netCommissionValue,boolean suspicious, TransactionResponse response);

	void failedBillPayment(String transactionRefNo);

	void initiateLoadMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername);
	
	void successLoadMoney(String transactionRefNo, String retrievalRefNo);

	void failedLoadMoney(String transactionRefNo);
	
	void initiateUPILoadMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, boolean isSdk);

	void updateUpiTrasaction(String transactionRefNo, String upiId, String referenceId);
	
	void successUPILoadMoney(String referenceId);
	
	void failedUPILoadMoney(String referenceId);
	
	void successUPISdkLoadMoney(UpiMobileRedirectReq dto);
	
	void failedUPISdkLoadMoney(UpiMobileRedirectReq dto);
	
	void failedUPILoadMoneyWithTxnRef(String txnRefNo);

	void initiateBankTransfer(double amount, String description, PQService service, String transactionRefNo,
			String sender, String receiverUsername);

	void successBankTransfer(String transactionRefNo);

	void failedBankTransfer(String transactionRefNo);

	void initiatePromoCode(double amount, String description, String transactionRefNo, PQService service,
			PromoCode promoCode, String senderUsername, String receiverUsername);

	void successPromoCode(String transactionRefNo);

	void failedPromoCode(String transactionRefNo);

	void initiateMobileBanking(double amount, String description, PQService service, String transactionRefNo,
			String receiverName);

	void successMobileBanking(String transactionRefNo);

	void failedMobileBanking(String transactionRefNo);

	PQTransaction saveOrUpdate(PQTransaction transaction);

	void initiatePromoCodeOld(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, String json);

	void successPromoCodeOld(String transactionRefNo);

	void failedPromoCodeOld(String transactionRefNo);

	List<PQTransaction> findTransactionByAccount(Date from, Date to, PQAccountDetail account, double amount);

	List<PQTransaction> findTransactionByAccount(PQAccountDetail account);

	Page<PQTransaction> findByAccount(Pageable page, PQAccountDetail account);

	Page<PQTransaction> findByService(Pageable page, PQService service);

	List<PQTransaction> findTransactionByAccountAndAmount(Date from, Date to, PQAccountDetail account, double amount);

	void revertFailedBillPayment();

	List<PQTransaction> getTotalTransactions();

	List<PQTransaction> getDailyDebitTransaction(PQAccountDetail account);

	List<PQTransaction> getMonthlyDebitTransaction(PQAccountDetail account);

	List<PQTransaction> getDailyCreditTransation(PQAccountDetail account);

	List<PQTransaction> getMonthlyCreditTransation(PQAccountDetail account);

	List<PQTransaction> getDailyCreditAndDebitTransation(PQAccountDetail account);

	List<PQTransaction> getMonthlyCreditAndDebitTransation(PQAccountDetail account);

	double getDailyDebitTransactionTotalAmount(PQAccountDetail account);

	double getMonthlyDebitTransactionTotalAmount(PQAccountDetail account);

	double getDailyCreditTransationTotalAmount(PQAccountDetail account);

	double getMonthlyCreditTransationTotalAmount(PQAccountDetail account);

	double getDailyCreditAndDebitTransationTotalAmount(PQAccountDetail account);

	double getMonthlyCreditAndDebitTransationTotalAmount(PQAccountDetail account);

	void successBillPaymentNew(String transactionRefNo);

	void failedBillPaymentNew(String transactionRefNo);

	double getLastSuccessTransaction(PQAccountDetail account);

	void reverseTransaction(String transactionRefNo);

	void processRefundLoadMoney(String transactionRefNo);

	void processRefundUPILoadMoney(String transactionRefNo);
	
	void processRefundBillPayment(String transactionRefNo);

	void initiateBusBooking(double amount, String description, PQService service, String transactionRefNo,
			String sender, String receiver, String json);

	void successTravelBus(String transactionRefNo);

	// void failedTravelBus(String transactionRefNo);

	List<PQTransaction> getAllDefaultTransactions();

	void initiateMBankTransfer(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername, String json);

	List<PQTransaction> getTotalTransactionsOfMerchant(String merchant);

	void initiateEventPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	ResponseDTO getBookingStatus(String transactionRefId, String orderID, String sessionId, String accessToken);

	void successEventPayment(String transactionRefNo);

	void failedEventPayment(String transactionRefNo);

	// gci
	void initiateGiftCartPayment(double amount, String description, PQService service, String transactionRefNo,
            User sender, String receiverUsername,PQCommission senderCommission,double netCommissionValue);

	void successGiftCartPayment(String transactionRefNo,String receiptNo,PQCommission commission,double netCommissionValue,GiftCardTransactionResponse response,User u,ProcessGiftCardDTO dto);

	void failedGiftCartPayment(String transactionRefNo);

	PQTransaction transactionIdClickable(String transactionRefNo);

	void successSendMoneyToPredictWinner(String transactionRefNo);

	void initiateSendMoneyToPredictWinner(double amount, String description, String transactionRefNo, PQService service,
			PromoCode promoCode, String senderUsername, String receiverUsername);

	void initiateIpltransaction(double amount, String description, String transactionRefNo, PQService service,
			String senderUsername, String receiverUsername);

	void successPredictAndWin(String transactionRefNo);

	void initiateAgentSendMoney(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	void successAgentSendMoney(String transactionRefNo);

	void failedAgentSendMoney(String transactionRefNo);

	void initiateNikkiChatPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String json, String authReferenceNo);

	void successNikkiChatPayment(String transactionRefNo, String retrivalReferenceNumber);

	void failedNikkiChatPayment(String transactionRefNo);

	List<PQTransaction> getTotalinitateAgentTransactions(Pageable page, String servicecode);

	ResponseDTO processSendMoneyRefund(RefundDTO dto);

	ResponseDTO processLoadMoneyRefund(RefundDTO dto);

	ResponseDTO processBillPaymentRefund(RefundDTO dto);

	Page<PQTransaction> transactionListByServicePage(Pageable page, PQService service);

	List<PQTransaction> filterListByTypeAndStatus(List<PQService> services, Date startDate, Date endDate,Status status);

	double getMonthlyDebitTransactionTotalAmountBank(PQAccountDetail account);

	double getDailyDebitTransactionTotalAmountBank(PQAccountDetail account);

	EBSStatusResponseDTO checkLoadMoneyStatus(String transactionRefNo, String serviceCode);

	double getTotalTransactionsOfAgent(String user);

	void initiatePayloPayment(double amount, String description, PQService service, String transactionRefNo, String senderUsername, String json,String authReferenceNo);

	void successPayloPayment(String transactionRefNo, String retrivalReferenceNumber);

	void failedPayloPayment(String transactionRefNo);


	// Adlabs
	void initiateAdlabsPayment(double amount, String description, PQService service, String transactionRefNo, String senderUsername, String receiverUsername);
	
	void successAdlabsPayment(String transactionRefNo);
	
	void failedAdlabsPayment(String transactionRefNo);

	//Imagica <-----Real One
	PQTransaction initiateImagicaPayment(double amount, String description, PQService service, String transactionRefNo, String senderUsername, String receiverUsername);
	PQTransaction successImagicaPayment(String transactionRefNo);
	void failedImagicaPayment(String transactionRefNo);


	List<Object[]> getTotalTransactionsByServices(Date from, Date to);

	List<Object[]> getTotalTransactionsByServicesCredit(Date from, Date to);

	void initiateQwikrPayPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, String orderId);

	void successQwikrPayPayment(String transactionRefNo);

	void failedQwikrPayPayment(String transactionRefNo);

	PQTransaction initiateYuppTvPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	PQTransaction successYuppTVPayment(String transactionRefNo);

	void failedYuppTVPayment(String transactionRefNo);

	Page<PQTransaction> transactionListByServicePageCommssion(Pageable page, PQService service);

	Page<PQTransaction> getTotalTransactionsOfUserFilter(Pageable page, Date StartDate, Date endDate, String username);

	List<TransactionListDTO> getmVisaTransactionFilter(Date from, Date to);

	List<TPTransaction> transactionListByMerchant(User merchant);

	Page<PQTransaction> transactionListByServicePageFiltered(Pageable page, PQService service, Date from, Date to);

	Page<TPTransaction> transactionListByMerchantPageFiltered(Pageable page, User merchant, Date from, Date to);

	Page<TPTransaction> transactionListByMerchantPage(Pageable page, User merchant);

	Page<PQTransaction> getTotalTransactionsFiltered(Pageable page, Date from, Date to);

	List<PQTransaction> listCommissionTransactionByServiceAndDate(Date from, Date to, PQServiceType serviceType,
			boolean debit, TransactionType transactionType, Status status);

	List<TPTransaction> transactionListMerchantPage(User merchant);

	Page<PQTransaction> transactionListByService(Pageable page, PQService service);

	Page<PQTransaction> transactionListServicePage(Pageable page, PQService service);
	
	Page<PQTransaction> transactionListServiceBescom(Pageable page, PQService service);

	Page<PQTransaction> getTotalTransactionsOfMerchantService(String merchantEmail, PQService service ,Pageable page);

	long getDailyTransactionCountBetweeen(Date from, Date to);

	long getDailyTransactionCountBetweenForAccount(Date from, Date to,PQAccountDetail account);

	List<PQTransaction> filterListByTypeAndStatusAndRefNo(Date startDate, Date endDate,String trasnactionRefNo);

	PQTransaction successFlightPayment(String transactionRefNo);

	void failedFlightPayment(String transactionRefNo);

	PQTransaction initiateFlightPayment(double amount, String description, PQService service, String transactionRefNo,
			User senderUsername, String receiverUsername, String convenienceFee,String baseFare);

	void initiatiateRefernEarn(double amount, String description, String transactionRefNo, PQService service,
			String senderUsername, String receiverUsername);

	void failedRefernEarn(String transactionRefNo);

	void successRefernEarn(String transactionRefNo);
	
	
	UserDetailDTO getUserDataForGenericValidation(String username,int currentDay,int currentMonth,int currentYear);
	
	UserDetailDTO getUserDataForP2PValidation(String username,int currentDay,int currentMonth,int currentYear);
	
	
	void sendMoneyNew(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	PQTransaction initiateFlightPaymentEBS(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	void successFlightPaymentEBS(String transactionRefNo,String paymentId);

	void failedFlightEBS(String transactionRefNo);
	
	PQTransaction initiateBusPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	
	PQTransaction successBusPayment(String transactionRefNo);

	void failedBusPayment(String transactionRefNo);

	String cancelBusTicket(String txnRefNo,double refundedAmt);

	PQTransaction initiateFlightPaymentVnet(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);
	
	void successFlightPaymentVnet(String transactionRefNo, String paymentId);
	
	void failedFlightVnet(String transactionRefNo);

	ResponseDTO processNikiChatRefund(MicroPaymentServiceStatus dto);
	
	void processRefundNikkiTransaction(String transactionRefNo);


//	List<PQTransaction> listTransactionByDate(Date from, Date to, PQServiceType serviceType, boolean debit,
//			TransactionType transactionType, Status status);

	//List<PQTransaction> listTransactionByDate(Date from, Date to, PQService service, boolean debit);

	//List<PQTransaction> listTransactionByDate(Date from, Date to, boolean debit,PQService service,  Status status);

//	List<PQTransaction> listTransactionByDate(Date from, Date to, boolean debit, PQService service,
//			TransactionType type, Status status);

	List<PQTransaction> listTransactionByDate(boolean debit, Date from, Date to, PQService service, Status status);

	List<PQTransaction> listDebitTransaction(boolean debit, Date from, Date to, Status status);

	List<PQTransaction> listCreditTransaction(boolean debit, Date from, Date to,  Status status);

//	List<PQTransaction> listTransactionByDate(Date from, Date to, PQServiceType serviceType, boolean debit,
//			PQService service, Status status);

	//List<PQTransaction> listTransactionByDate(Date startDate, Date endDate, Status success);

	
	//Success RedeemPoints

	List<PQTransaction> getCommissionOfMerchantService(PQService service);

	Page<PQTransaction> getCommissionOfMerchantServicePage(PQService service, Pageable pageable);

	
	//Travelkhana
	void initiateTKPayment(double amount, String description, PQService service, String transactionRefNo, String senderUsername);
	void successTKPayment(String transactionRefNo, long userOrderId,PQService service,double amount,TKOrderDetail orderDetail);
	void failedTKPayment(String transactionRefNo,TKOrderDetail orderDetail);
    
	//Success RedeemPoints
	void successRedeemMoney(double amount, String description, PQService service, String receiverUserName, long point);

	Page<PQTransaction> last20transactionListByServicePage(Pageable page, PQService service);

	Page<PQTransaction> findDebittransactionListByServicePage(Pageable page, PQAccountDetail account);

	void initiateTreatCardPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername, PQCommission senderCommission, double netCommissionValue);

	void successTreatCardPayment(String transactionRefNo, PQCommission senderCommission, double netCommissionValue);

	void failedTreatCardPayment(String transactionRefNo);

	ResponseDTO processMerchantPaymentRefund(RefundDTO dto);

	void processRefundMerchantPayment(String transactionRefNo);
	
	List<TransactionListDTO> getTreatCardTransaction(PQService service);

	
	PQTransaction initiateHouseJoyPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	void successHouseJoyPayment(String transactionRefNo,String jobId);

	void failedHouseJoyPayment(String transactionRefNo);
	
	ResponseDTO processTravelEBSRefund(RefundDTO dto);

	ResponseDTO processTravelWalletRefund(RefundDTO dto);

	void processRefundTravelWallet(String transactionRefNo);

	void successSendMoneyForBulkFile(String transactionRefNo);

	void initiateWoohooPayment(WoohooTransactionRequest dto, String string, PQService service, String transactionRefNo, User user,
			String woohoo, PQCommission commission, double netCommissionValue);

	OrderPlaceResponse successWoohooPaymentNew(String transactionRefNo, PQCommission commission, double netCommissionValue, User user,
			OrderPlaceResponse dto);

	void failedWoohooPayment(String transactionRefNo);
	
	PQTransaction initiateMerchantUPIPayent(double amount, String description, PQService service, String transactionRefNo,
			User sender, String authRefNo);

	void updateMerchantUPIPayentTrasaction(String transactionRefNo, String upiId, String referenceId);
	
	void successMerchantUPIPayent(String referenceId, String newRefNo, String orgRefId);
	
	void failedMerchantUPIPayent(String referenceId, String orgRefId);

	List<WoohooCardDetailsDTO> getallCards();

	VNetStatusResponse vnetVarification(String transactionRefNo, String serviceCode, double amount);

	Page<PQTransaction> transactionListServicePageForTreatCard(Pageable page, PQService service);
	
	List<PQTransaction> iPayRefundedTxnList(PagingDTO dto);

	ResponseDTO processBankTransferRefund(RefundDTO dto);

	void processRefundBankTransferPayment(String transactionRefNo);

	List<PQTransaction> listWooHooTransactionByDate(boolean debit, Date startDate, Date endDate, PQService type,
			Status success);

	OrderPlaceResponse pendingWoohooPaymentNew(String transactionRefNo, PQCommission commission, double netCommissionValue, User user,
			OrderPlaceResponse dto);

	void failedStatusWoohooPayment(String retrivalRefNo);

	void successStatusWoohooPaymentNew(OrderPlaceResponse dto, PQTransaction transaction);

	void successWoohooPayment(String transactionRefNo, PQCommission senderCommission, double netCommissionValue, User u,
			WoohooTransactionResponse response);
	
	void pendingWoohooPayment(String transactionRefNo, PQCommission commission, double netCommissionValue, User user,
			WoohooTransactionResponse dto);

	void successStatusWoohooPayment(WoohooTransactionResponse dto, PQTransaction transaction);
	
	void failedWoohooPaymentNew(String transactionRefNo);

	ResponseDTO processWoohooRefund(RefundDTO dto);

	void processRefundWoohoo(String transactionRefNo);
	Page<PQTransaction> getUPITxn(Pageable pageable, PQService upiService);

	List<PQTransaction> getUPITxnList(PQService upiService, Date fromDate, Date toDate);
	
	User getAccountByAuthority(PQAccountDetail pqAccountDetail);
	
	List<PQTransaction> filterListByTypeAndStatusDebit(List<PQService> services, Date startDate, Date endDate,Status status);

	Page<PQTransaction> getCommissionOfTKService(Pageable pageable, PQService service);

	void initiateABankTransfer(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername, String json);

	List<PQTransaction> getSuspisiousTxn(Status success);
	
	void updateSuspiciousTransaction();

	void successSuspisiouseTxn(String refNo);

	void failedSuspisiousTxn(String transactionRefNo);

	void successCanteenMerchantPayment(double amount, String description, String remarks, PQService service,
			String transactionRefNo, String senderUsername, String receiverUsername , boolean isPG);

	void successMBankTransfer(double amount, String description, PQService service, String transactionRefNo,
			User sender, String receiverUsername, String json);
	
	void findTotalCommisionByMonthWise() throws Exception;

	void findTotalUserMonthWise() throws Exception;

	void SendMoneyPredictUsers(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	void initiateVisaMerchantPayment(double amount, String description, PQService service, String transactionRefNo,
			String senderUserName, String receiverUserName, String merchantUserName);

	void transferMoneyToWallet(double amount, String description, PQService service, String transactionRefNo,
			String senderUsername, String receiverUsername);

	ResponseDTO loadMoneyProcess(LoadMoneyRequest dto, User user, PQService service);

	Voucher getVoucher(String voucherNo);

	Page<Voucher> getAllVouchers(Pageable pageable);

	int addVouchers(AddVoucherDTO dto);
	
}
