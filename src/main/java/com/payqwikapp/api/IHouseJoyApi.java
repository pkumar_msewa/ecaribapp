package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.entity.HouseJoy;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.HouseJoyRequest;

public interface IHouseJoyApi {

	public TransactionResponse successHouseJoyPayment(HouseJoyRequest dto,PQService service,User user);
	public String initiateHouseJoyPayment(HouseJoyRequest dto, User user, PQService service);
	public TransactionResponse cancelHouseJoyPayment(String txnRefNo);
	public List<HouseJoy> getHouseJoyForAdminByDate(Date from, Date to);
	public List<HouseJoy> getHouseJoyForAdmin();
}
