package com.payqwikapp.api;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.EventDetailsDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IEventApi {

	ResponseDTO saveAccessToken(EventDetailsDTO requset, String username);
	
	ResponseDTO saveEventDetails(EventDetailsDTO request,String username);
	
	ResponseDTO eventPayment(EventDetailsDTO request,String username, String status);
	String eventPaymentinitate(EventDetailsDTO request,String username,PQService service, String status);

	
}
