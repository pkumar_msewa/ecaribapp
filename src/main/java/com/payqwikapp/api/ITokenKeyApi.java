package com.payqwikapp.api;

import com.payqwikapp.entity.TokenKey;

public interface ITokenKeyApi {

	TokenKey getTokenByConsumerKeyAndConsumerSecretKey(String consumerKey, String consumerToken);
}
