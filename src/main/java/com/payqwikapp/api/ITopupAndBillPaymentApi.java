package com.payqwikapp.api;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.CallBackRequest;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ITopupAndBillPaymentApi {

	TransactionResponse prepaidTopup(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue,DataConfig config,String description);

	TransactionResponse dthBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue);

	TransactionResponse landlineBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue);

	TransactionResponse electricityBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue);

	TransactionResponse gasBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue);

	TransactionResponse insuranceBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission,double netCommissionValue);
	
	ResponseDTO callbackHandler(CallBackRequest dto);

	TransactionResponse waterBillPayment(CommonRechargeDTO dto, String username, PQService service,PQCommission commission, double netCommissionValue);

}
