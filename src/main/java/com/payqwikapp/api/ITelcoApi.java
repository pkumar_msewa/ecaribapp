package com.payqwikapp.api;

import java.util.List;

import com.payqwikapp.entity.TelcoCircle;
import com.payqwikapp.entity.TelcoMap;
import com.payqwikapp.entity.TelcoOperator;
import com.payqwikapp.entity.TelcoPlans;
import com.payqwikapp.model.TelcoCircleDTO;
import com.payqwikapp.model.TelcoOperatorDTO;
import com.payqwikapp.model.TelcoPlansDTO;

public interface ITelcoApi {
	
	List<TelcoOperator> getAllOperators();

	List<TelcoCircle> getAllCircles();

	TelcoCircle findTelcoCircleByCode(String code);

	TelcoCircleDTO findTelcoCircleDTOByCode(String code);

	void saveTelcoCircle(TelcoCircle dto);

	TelcoOperator findTelcoOperatorByCode(String code);

	TelcoOperatorDTO findTelcoOperatorDTOByCode(String code);

	void saveTelcoOperator(TelcoOperator dto);

	TelcoPlans findTelcoPlans(TelcoPlans plans);

	void saveTelcoPlans(TelcoPlans dto);

	Long countTelcoPlans();

	TelcoMap findTelcoMapByNumber(String number);

	void saveTelcoMap(TelcoMap dto);

	Long countTelcoMap();

	public List<TelcoOperatorDTO> getAllOperatorsDTO();

	public List<TelcoCircleDTO> getAllCirclesDTO();

	public List<TelcoPlans> getTelcoPlansByOperatorCircle(TelcoOperator operator, TelcoCircle circle);

	public List<TelcoPlansDTO> getTelcoPlansDTOByOperatorCircle(TelcoOperatorDTO operatorDTO, TelcoCircleDTO circleDTO);

}
