package com.payqwikapp.api;

import com.payqwikapp.model.TKPlaceOrderRequest;
import com.payqwikapp.model.UserDTO;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TrainList;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.TKOrderDTO;

public interface ITKPlaceOrderApi {
	TKOrderDTO tkpaymentSuccess(TKPlaceOrderRequest dto, String username, PQService service, User u,
			String transactionRefNo);

	String tkpaymentInitiate(TKPlaceOrderRequest tkReq, User user, PQService service);

	List<TKOrderDetail> myorder(User user);

	void getTrainListFromTK();

	List<TrainList> getTrainList();

	List<TKOrderDetail> getOrderDetailsByDateForAdmin(Date from, Date to);

	Page<TKOrderDetail> getOrderDetailsForAdmin(Pageable pageable);

	List<TKOrderDetail> getOrderDetailsForAdmin();
}
