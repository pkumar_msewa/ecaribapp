package com.payqwikapp.api;

import com.instantpay.model.request.AdlabsOrderRequest;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.FlightPaymentRequest;

public interface EaseMyTripApi {

	public String adlabspaymentInitiate(FlightPaymentRequest dto, String senderUser, PQService service, String receiverUser);
}
