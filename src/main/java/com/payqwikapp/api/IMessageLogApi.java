package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.MessageLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IMessageLogApi {

	List<MessageLog> getDailyMessageLogBetweeen(Date from, Date to);

	Page<MessageLog> getAllSMSLogs(Pageable pageable);
}
