package com.payqwikapp.api;

import com.instantpay.model.request.AdlabsOrderRequest;
import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;

public interface IAdlabsApi {
	
    String adlabspaymentInitiate(AdlabsOrderRequest dto, String senderName, PQService service, String receivername);

	
	TransactionResponse adlabspaymentSuccess(AdlabsOrderRequest dto, String username, PQService service,User u);



}
