package com.payqwikapp.api;

import java.text.ParseException;
import java.util.Date;

import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.YTVAccess;
import com.payqwikapp.model.YupTVDTO;

public interface IYuppTVApi {
	 
	 PQTransaction initiateTransaction(String transactionRefNo,double amount,PQService service,String senderUsername);

	String processPayment(String transactionRefNo);

	PQTransaction placeSubscriptionForYuppTV(YupTVDTO order, PQService service, String senderUsername);
	
     YTVAccess isYupTVAccountSaved(YupTVDTO dto, PQAccountDetail account) throws ParseException;
	
	boolean checkRegister(YupTVDTO dto, PQAccountDetail account) throws ParseException;


	YTVAccess findByExpiryDate(Date expiryDate) throws ParseException;

	YTVAccess findByUniqueId(String id);

	boolean checkExistUser(YupTVDTO dto, PQAccountDetail account);

	YTVAccess updateYtvAccess(YupTVDTO dto);

	PQTransaction reverseSubscriptionForYuppTV(YupTVDTO order, PQService service, String senderUsername);

}
