package com.payqwikapp.api;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IQwikrPayApi {

	public TransactionResponse successQwikrPayPayment(QwikrPayRequest dto, String username, PQService service,User user);
	public String initiateQwikrPayPayment(QwikrPayRequest dto, String username, PQService service);
}
