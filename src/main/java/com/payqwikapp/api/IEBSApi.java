package com.payqwikapp.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

import com.ebs.model.EBSRedirectResponse;
import com.ebs.model.EBSRequest;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.RazorPayRequest;
import com.payqwikapp.model.RazorPayResponse;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IEBSApi {

	EBSRequest requestHandler(EBSRequest request, String username, PQService service);
	ModelAndView responseHandlerView(HttpServletRequest request, String username);
	void responseFromWeb();
	EBSRequest requestHandlerFLight(EBSRequest request, String username, PQService service);
	ResponseDTO responseHandlerFlight(EBSRedirectResponse redirectResponse);
	RazorPayRequest requestRazorPay(RazorPayRequest request, String username, PQService service);
	ResponseDTO successRazorPay(RazorPayResponse redirectResponse);
	ResponseDTO responseHandler(EBSRedirectResponse redirectResponse, PQService service, String sessionId, User u);
	
}
