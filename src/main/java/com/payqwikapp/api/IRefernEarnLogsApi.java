package com.payqwikapp.api;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.RefernEarnRequest;

public interface IRefernEarnLogsApi {

	public RefernEarnlogs getlogs(User user);

	boolean sendRefernEarnMoney(double amount, User user);
}
