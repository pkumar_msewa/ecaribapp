package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import org.json.JSONException;

import com.payqwikapp.entity.AgentFlightTicket;
import com.payqwikapp.entity.AgentFlightTravellers;
import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.entity.FlightDetails;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.FlightDetailsDTO;
import com.payqwikapp.model.FlightPaymentRequest;
import com.payqwikapp.model.FlightPaymentResponse;
import com.payqwikapp.model.travel.flight.AgentFlghtTicketResp;
import com.payqwikapp.model.travel.flight.CompareCountry;
import com.payqwikapp.model.travel.flight.FlightPayment;
import com.payqwikapp.model.travel.flight.TicketsResp;

public interface IAgentFlightApi {
	public FlightDetails saveFlightBooking(FlightPaymentRequest order,PQTransaction transaction,User u);
	public FlightPaymentResponse processPayment(FlightPaymentRequest dto,User user)throws Exception;
	public PQTransaction initiateTransaction(String transactionRefNo,double amount,PQService service, User senderUser,String convFee,String baseFare);
	FlightPaymentResponse placeOrder(FlightPaymentRequest order, PQService service, User senderUser);
	List<FlightDetailsDTO> getFlightDetailsForAdmin();
	void processPaymentGateWaySuccess(FlightPaymentRequest dto, User u) throws JSONException;
	void processPaymentGateWayFailure(FlightPaymentRequest dto, User u);
	
	/* Code Done By Rohit End */


	/*##############################################################################################################*/

	/* Fresh Code Started By Subir */
	
	FlightPaymentResponse flightInit(FlightPayment order, PQService service, User senderUser);
	public FlightPaymentResponse flightPayment(FlightPayment dto,User user)throws Exception;
	FlightPaymentResponse paymentGateWaySuccess(FlightPayment dto, User user) throws JSONException;
	void paymentGateWayFailure(FlightPayment dto, User u);
	public List<AgentFlghtTicketResp> getAllTickets(String userName);
	public List<AgentFlightTicket> getFlightDetailForAdmin();
	public void cronforSaveCityList();
	public List<FlightAirLineList> getAllAirLineList();
	public List<AgentFlightTravellers> getFlightTravellersForAdmin(long flightTicketId);
	public List<AgentFlightTicket> getFlightDetailForAdminByDate(Date from, Date to);
	
	public boolean comCountry(CompareCountry dto);
	TicketsResp getFlightTravellersNameForAdmin(long flightTicketId);
	AgentFlightTicket saveFlight(FlightPayment order, PQTransaction transaction, User user, double agentAmount,
			String baseFare);
}
