package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.EmailLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IEmailLogApi {

	List<EmailLog> getDailyEmailLogBetweeen(Date from, Date to);

	Page<EmailLog> getAllEmailLogs(Pageable pageable);
	
}
