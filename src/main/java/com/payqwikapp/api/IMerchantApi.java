package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.VijayaBankReport;
import com.payqwikapp.entity.VisaMerchant;
import com.payqwikapp.model.MRegisterDTO;
import com.payqwikapp.model.MerchantDTO;
import com.payqwikapp.model.MerchantReportDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IMerchantApi {

	ResponseDTO addMerchant(MRegisterDTO user);

	ResponseDTO addVisaMerchant(VisaMerchantRequest dto, User merchant);

	VisaMerchant findByEmail(String emailAddress);

	List<MerchantDTO> getAll(String type, String to, String from);

	List<VisaMerchant> getAllVisaMerchant();

	public ResponseDTO getEntityId(String username);

	PQTransaction getStatusByTran(String transactionRefNo);

	void saveBulkFile(MerchantReportDTO bulkUpload);

	List<VijayaBankReport> getMerchantReport();

	List<VijayaBankReport> getFilteredMerchantReport(Date start, Date end, String paymentMode, String consumerType);

}
