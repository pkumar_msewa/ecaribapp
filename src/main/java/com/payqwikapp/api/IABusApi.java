package com.payqwikapp.api;

import java.util.Date;
import java.util.List;

import com.payqwikapp.entity.AgentBusTicket;
import com.payqwikapp.entity.AgentTravellerDetails;
import com.payqwikapp.entity.BusCityList;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.travel.bus.AgentBusTicketResp;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.model.travel.bus.BusPaymentReq;
import com.payqwikapp.model.travel.bus.BusPaymentResponse;

public interface IABusApi {
	public void saveBusBooking(BusPaymentInit order,PQTransaction transaction,User user);
	public BusPaymentResponse processPayment(BusPaymentReq dto,User user);
	public PQTransaction initiateTransaction(String transactionRefNo,double amount,PQService service, String senderUsername);
	BusPaymentResponse placeOrder(BusPaymentInit order, PQService service, String senderUsername,User user);
	public void failPayment(String transaction);
	public void cronforGetAllCityList();
	public List<BusCityList> getAllCityList();
	public List<AgentBusTicketResp> getAllTickets(String sessionId);
	public List<AgentTravellerDetails> getSingleTicketTravellerDetails(String emtTxnId);
	List<AgentBusTicket> getBusDetailsForAdmin();
	ResponseDTO cancelBookedTicket(String transactionRefNo,double refundedAmt);
	
	List<AgentBusTicket> getBusDetailsForAdminByDate(Date from, Date to);
//	public List<TravellerDetails> getTravellerDetailsForAdmin(String emtTxnId);
	
//  Changes for re price
	
	BusPaymentResponse saveGetTxnId(BusPaymentInit order, PQService service, String senderUsername,User user);
	public BusPaymentResponse bookTicketPayment(BusPaymentReq dto, PQService service, String senderUsername,User user);
	BusPaymentResponse initBusPayment(BusPaymentInit order, PQService service, String senderUsername,User user);

	public List<AgentTravellerDetails> createSingleTicketPdf(String emtTxnId);
	
	BusPaymentResponse saveSeatDetails(BusPaymentInit order, PQService service, String senderUsername,User user);
}
