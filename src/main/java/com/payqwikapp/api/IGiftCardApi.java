package com.payqwikapp.api;

import com.payqwikapp.entity.GiftCardToken;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.GiftCardAuthDTO;
import com.payqwikapp.model.GiftCardTokenResponse;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.PlaceOrderRequest;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.WoohooTransactionRequest;

public interface IGiftCardApi {
	
	GiftCardTokenResponse requestAccessToken(GiftCardAuthDTO dto);

	GiftCardToken createSessionForGiftCard(String sessionId);
	
	GiftCardTransactionResponse processOrder(ProcessGiftCardDTO dto, User user,String transactionRefNo, PQService service,PQCommission commission,double netCommissionValue);
	
	/*GiftCardTransactionResponse getVoucherNumber(String receiptNo, String token,String sessionId);*/
	
	void saveUserReceipt(GiftCardTransactionResponse response,User u,ProcessGiftCardDTO dto);

	OrderPlaceResponse processWoohooOrder(WoohooTransactionRequest dto, User user, String transactionRefNo,
			PQService service, PQCommission commission, double netCommissionValue);

	OrderPlaceResponse placeOrder(WoohooTransactionRequest request);

	void saveWoohooRequest(OrderPlaceResponse dto, String transactionRefNo,String senderMobileNo,String receiverMobileNo);


}
