package com.payqwikapp.api;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.VNetDTO;
import com.payqwikapp.model.VNetResponse;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IVNetApi {
	
	ResponseDTO processRequest(VNetDTO dto, String username, PQService service , User u);

	VNetDTO processRequestFlight(VNetDTO dto, String username, PQService service);

	ResponseDTO handleResponseFlight(VNetResponse dto);

	ResponseDTO handleResponse(VNetResponse dto, PQService service, User u);
}
