package com.payqwikapp.api;

import com.ebs.model.EBSRedirectResponse;
import com.ebs.model.EBSRequest;
import com.payqwikapp.model.*;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ISecurityApi {

    LoginDTO getLoginDTO(ERequestDTO dto);

    SessionDTO getSessionDTO(ERequestDTO dto);

    MpinDTO getMpinDTO(ERequestDTO dto);

    MpinChangeDTO getMpinChangeDTO(ERequestDTO dto);

    EResponseDTO getEResponse(ResponseDTO dto);

    RegisterDTO getRegisterDTO(ERequestDTO dto);

    ChangePasswordDTO getChangePasswordDTO(ERequestDTO dto);

    UserRequestDTO getUserRequestDTO(ERequestDTO dto);

    RefundDTO getRefundDTO(ERequestDTO dto);

    GetTransactionDTO getTransactionDTO(ERequestDTO dto);

    PagingDTO getPagingDTO(ERequestDTO dto);

    MRegisterDTO getMRegisterDTO(ERequestDTO dto);

    PromoCodeDTO getPromoCodeDTO(ERequestDTO dto);

    CallBackDTO getCallBackDTO(ERequestDTO dto);

    DTHBillPaymentDTO getDTHBillPaymentDTO(ERequestDTO dto);

    LandlineBillPaymentDTO getLandlineBillPaymentDTO(ERequestDTO dto);

    ElectricityBillPaymentDTO getElectricityBillPaymentDTO(ERequestDTO dto);

    GasBillPaymentDTO getGasBillPaymentDTO(ERequestDTO dto);

    InsuranceBillPaymentDTO getInsuranceBillPaymentDTO(ERequestDTO dto);

    TelcoDTO getTelcoDTO(ERequestDTO dto);

    PlanDTO getPlanDTO(ERequestDTO dto);

    String encryptText(String rawText);

    CallBackRequest getCallBackRequest(ERequestDTO dto);

    CouponDTO getCouponDTO(ERequestDTO dto);

    VNetDTO getVNetDTO(ERequestDTO dto);

    VNetResponse getVNetResponse(ERequestDTO dto);

    EBSRequest getEBSRequest(ERequestDTO dto);

    EBSRedirectResponse getEBSRedirectResponse(ERequestDTO dto);

    PayAtStoreDTO getPayStoreDTO(ERequestDTO dto);

    RedeemDTO getRedeemDTO(ERequestDTO dto);

}
