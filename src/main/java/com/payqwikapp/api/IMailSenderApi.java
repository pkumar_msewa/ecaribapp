package com.payqwikapp.api;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;

import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.WoohooCardDetails;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.SummaryReportDTO;
import com.payqwikapp.model.UserMonthlyBalanceDTO;
import com.payqwikapp.model.WoohooTransactionResponse;
import com.payqwikapp.model.travel.AgentTicketDetailsDTOFlight;
import com.payqwikapp.model.travel.TicketDetailsDTOFlight;
import com.payqwikapp.model.travel.bus.AgentTicketDetailsDTO;
import com.payqwikapp.model.travel.bus.TicketDetailsDTO;

public interface IMailSenderApi {

	void sendMail(final String subject, final String mailTemplate, final User user,String additionalInfo) throws MailSendException;

	void sendNoReplyMail(final String subject, final String mailTemplate, final User user,String additionalInfo) throws MailSendException;

	void sendChangePasswordMail(String subject, String mailTemplate, User user,String additionalInfo) throws MailSendException;
	
	void sendTransactionMail(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,String additionalInfo,String ccEmail) throws MailSendException;
	
	void saveLog(String subject, String mailTemplate, User user, String receiver, Status status) throws MailSendException;
	void saveLog(String subject, String mailTemplate, User user) throws MailSendException;
	
	void sendAsync(JavaMailSender mailSender, MimeMessagePreparator preparator);;
	
	void sendVijayaBankEmail(String subject, String mailTemplate, User user, PQTransaction transaction,String additionalInfo,String ccEmail) throws MailSendException;

	void sendVijayaBankNoReplyEmail(String subject, String mailTemplate, User user, PQTransaction transaction,String additionalInfo) throws MailSendException;

	void sendAsyncVijayaEmail(MimeMessage message);

	// for gci mail 
	void sendAsyncVijayaNoReplyEmail(MimeMessage message);
	void sendgciMail(final String subject, final String mailTemplate,ProcessGiftCardDTO giftCard, GiftCardTransactionResponse dto,final User user) throws MailSendException;
	void sendgiftcardmail(String subject, String mailTemplate, ProcessGiftCardDTO giftCard,
			GiftCardTransactionResponse dto, User user) throws MailSendException;
	
	void sendQwikrPayMail(final String subject, final String mailTemplate, QwikrPayRequest dto,final User user);

	void sendBusTicketMail(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,TicketDetailsDTO additionalInfo) throws MailSendException;

	void saveLogForMonthlyTrasnaction(String subject, String mailTemplate, UserMonthlyBalanceDTO emailOfUser,
			String sender, Status status);

	void sendFlightTicketMail(final String subject, final String mailTemplate, final User user,
			final PQTransaction transaction,TicketDetailsDTOFlight additionalInfo) throws MailSendException;

	void sendRedeemPointsMail(String subject, String mailTemplate, User user, long point, double balance,PQTransaction receiveTransaction) throws MailSendException;


	void sendVijayaBankEmailTK(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail, long userOrderId,TKOrderDetail orderDetail) throws MailSendException;

	void refundRequestMerchant(String filedetails,String merchantName,String merchantId) throws MailSendException;

	void sendMerchantRefundTransaction(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo) throws MailSendException;

	void sendTransactionMailFoodOrder(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail, long userOrderId, TKOrderDetail orderDetail)
			throws MailSendException;

	void sendTransactionFailTKMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail, TKOrderDetail orderDetail) throws MailSendException;

	void sendFailTKEmail(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail, TKOrderDetail orderDetail) throws MailSendException;
	
	void weeklyReconReport(String filedetails,String merchantName,String merchantId,String upiFileDetails) throws MailSendException;
	
	void weeklyReconReport(String filedetails) throws MailSendException;

	void sendwoohoogiftcardmailNew(String string, String gCI_SENDER, Object object, OrderPlaceResponse response,
			User u);

	void sendAsyncVijayaEmailForBankTransfer(MimeMessage message);

	void dailyBankTrasnferReport(String bankTrasnferFileDeatils, String dateFormat, int fileSize, long amount)
			throws MailSendException;

	void treatCardEmail(String subject, String mailTemplate, User user) throws MailSendException;
	
	void refundRequestBescomMerchant(String filedetails,String merchantName,String merchantId) throws MailSendException;

	void sendVijayaBankEmailNew(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail) throws MailSendException;
	
	void weeklyBesconUPIReconReport(String filedetails) throws MailSendException;

	void sendBulkUploadMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail) throws MailSendException;

	void sendVijayaBankEmailForBulkUpload(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail) throws MailSendException;

	void sendwoohoogiftcardmail(String subject, String mailTemplate, Object object, WoohooTransactionResponse dto,
			User user);

	void sendFlightTicketMail(String subject, String fLIGHTBOOKING_SUCCESS, User user, PQTransaction senderTransaction,
			AgentTicketDetailsDTOFlight additionalInfo);

	void sendBusTicketMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			AgentTicketDetailsDTO additionalInfo) throws MailSendException;

	void sendVijayaBankNoReplyAgentEmail(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo) throws MailSendException;

	void senderReceiverBankTransferMail(String string, String aGENT_BANK_TRANSFER_SENDER, AgentSendMoneyBankDTO dto, String emailId);

	void senderReceiverBankTransfer(String subject, String mailTemplate, AgentSendMoneyBankDTO dto, String emailId)
			throws MailSendException;

	void sendChangePasswordMail(String subject, String mailTemplate, User user, PQTransaction transaction,
			String additionalInfo, String ccEmail) throws MailSendException;

	void sendMonthlyTransactionTesting(String subject, String mailTemplate, UserMonthlyBalanceDTO dto,User u,
			String additionalInfo) throws MailSendException;

	void sendSummaryMail(String mailTemplate, SummaryReportDTO summaryReportDTO);

	void sendFlightTicketMailNew(String subject, String mailTemplate, User user, PQTransaction transaction,
			TicketDetailsDTOFlight additionalInfo) throws MailSendException;
}
