package com.payqwikapp.api;

import com.payqwikapp.entity.User;

public interface IRedeemCodeApi {

	boolean codeUsered(User user);
}
