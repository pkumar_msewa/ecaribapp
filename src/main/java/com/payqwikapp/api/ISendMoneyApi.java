package com.payqwikapp.api;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.MBankTransfer;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.NeftRequestDTO;
import com.payqwikapp.model.PayAtStoreDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyDonateeDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.SendMoneyResponse;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ISendMoneyApi {

	ResponseDTO sendMoneyMobile(SendMoneyMobileDTO dto, String username, PQService service);

	boolean sendMoneyBankInitiate(SendMoneyBankDTO dto, String username, PQService service);

	String sendMoneyBankFailed(String transactionRefNo);

	String sendMoneyBankSuccess(String transactionRefNo);

	String sendMoneyStore(PayAtStoreDTO dto, String username, PQService service);

	String prepareSendMoney(String username);

	ResponseDTO preparePayStore(PayStoreDTO dto, User u);

	ResponseDTO refundMoneyToAccount(RefundDTO dto, User u);

	void refundMoney(RefundDTO dto, User u, PQService service);

	SendMoneyResponse checkInputParameters(SendMoneyBankDTO dto);

	SendMoneyResponse sendMoneyMBankInitiate(SendMoneyBankDTO dto, User user, PQService service);

	List<NeftRequestDTO> getNeftRequestFile() throws IOException;

	List<NeftRequestDTO> convertRequestList(List<BankTransfer> transfers);

	NeftRequestDTO convertTransfer(MBankTransfer transfer);

	String description(String transactionRefNo);

	// Agent Api
	String agentRequestmoneysuccess(String transactionRefNo);

	String sendMoneyAgentMobile(SendMoneyMobileDTO dto, String username, PQService service);

	String requestMoneyAgentMobile(SendMoneyMobileDTO dto, String username, PQService service);

	String prepareSendMoneyToDonatee(String donatee);

	String sendMoneyMobile(SendMoneyDonateeDTO dto, String username, PQService service);
	
	String sendMoneyMobileNew(SendMoneyMobileDTO dto, String username, PQService service);

	NeftRequestDTO convertBankTransfer(BankTransfer transfer);

	String sendMoneyMobileBulkFile(SendMoneyMobileDTO dto, String username, PQService service);

	String getBankCode(SendMoneyBankDTO bank);

	String getAgentBankCode(AgentSendMoneyBankDTO dto);

	SendMoneyResponse checkAInputParameters(AgentSendMoneyBankDTO dto);

	SendMoneyResponse sendMoneyABankInitiate(AgentSendMoneyBankDTO dto, User user, PQService service);

	SendMoneyResponse sendMoneyMBankInitiateNew(SendMoneyBankDTO dto, User user, PQService service);

	List<NeftRequestDTO> convertRequestListFile(List<BankTransfer> transfers);

	NeftRequestDTO convertBankTransferPPI(BankTransfer transfer);

	void uploadPPIFile() throws ParseException;

	List<NeftRequestDTO> generatePPIFile() throws IOException;

	void sendMoneyToWinners(SendMoneyMobileDTO dto, String username, PQService service);



}
