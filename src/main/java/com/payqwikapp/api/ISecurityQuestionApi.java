package com.payqwikapp.api;

import java.util.List;

import com.payqwikapp.model.SecurityQuestionDTO;

public interface ISecurityQuestionApi {

	public List<SecurityQuestionDTO> getAllQuestionDTO();
}
