package com.payqwikapp.api;

import java.util.List;

import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.admin.ServiceListDTO;
import com.payqwikapp.model.admin.UpdateServiceDTO;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface IDataConfigApi {

	DataConfig getDataConfigByType(String type, Status status);
	
	
	List<DataConfig> getAllConfig();


	ResponseDTO updateMdexSwitch(UpdateServiceDTO dto);
}
