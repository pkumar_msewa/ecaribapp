package com.payqwikapp.api;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import com.ebs.model.EBSRequest;
import com.letsManage.model.dto.BlockDto;
import com.letsManage.model.dto.BulkMailRequestDTO;
import com.letsManage.model.dto.BulkSmsRequest;
import com.letsManage.model.dto.GetTransDTO;
import com.letsManage.model.dto.MailRequestDTO;
import com.letsManage.model.dto.PassWordSuperAdminDTO;
import com.letsManage.model.dto.ResponseLetsDTO;
import com.letsManage.model.dto.SmsRequest;
import com.letsManage.model.dto.SuperAdminAddDTO;
import com.letsManage.model.dto.UserTypeDto;
import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.entity.LoginLog;
import com.payqwikapp.entity.Offers;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.POSMerchants;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PQVersion;
import com.payqwikapp.entity.SendNotification;
import com.payqwikapp.entity.ServiceStatus;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.VBankAccountDetail;
import com.payqwikapp.model.AadharDTO;
import com.payqwikapp.model.AadharDetailsDTO;
import com.payqwikapp.model.AgentDTO;
import com.payqwikapp.model.AgentDetailsDTO;
import com.payqwikapp.model.AgentRequest;
import com.payqwikapp.model.AmountDTO;
import com.payqwikapp.model.ApproveKycDTO;
import com.payqwikapp.model.AuthUpdateRequest;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.DRegisterDTO;
import com.payqwikapp.model.DTHBillPaymentDTO;
import com.payqwikapp.model.ElectricityBillPaymentDTO;
import com.payqwikapp.model.GasBillPaymentDTO;
import com.payqwikapp.model.ImageDTO;
import com.payqwikapp.model.InsuranceBillPaymentDTO;
import com.payqwikapp.model.KYCResponse;
import com.payqwikapp.model.KycDTO;
import com.payqwikapp.model.KycLimitDTO;
import com.payqwikapp.model.LandlineBillPaymentDTO;
import com.payqwikapp.model.LoginDTO;
import com.payqwikapp.model.MRegisterDTO;
import com.payqwikapp.model.MTransactionResponseDTO;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.MpinChangeDTO;
import com.payqwikapp.model.MpinDTO;
import com.payqwikapp.model.NearAgentsDetailsDTO;
import com.payqwikapp.model.NearByAgentsDTO;
import com.payqwikapp.model.NearByMerchantsDTO;
import com.payqwikapp.model.NearMerchantDetailsDTO;
import com.payqwikapp.model.NotificationDTO;
import com.payqwikapp.model.OffersDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.PaymentRequestDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyDonateeDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.SendNotificationDTO;
import com.payqwikapp.model.ServiceStatusDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionListDTO;
import com.payqwikapp.model.TransactionReport;
import com.payqwikapp.model.UserBalanceDTO;
import com.payqwikapp.model.UserByLocationDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserMaxLimitDto;
import com.payqwikapp.model.UsernameListDTO;
import com.payqwikapp.model.VPQMerchantDTO;
import com.payqwikapp.model.VersionListDTO;
import com.payqwikapp.model.admin.AccessDTO;
import com.payqwikapp.model.admin.AddServicesDTO;
import com.payqwikapp.model.admin.ServiceListDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.admin.UpdateServiceDTO;
import com.payqwikapp.model.admin.UserListDTO;
import com.payqwikapp.model.admin.report.GetDailyAmountDTO;
import com.payqwikapp.model.admin.report.MISReportDTO;
import com.payqwikapp.model.admin.report.WalletLoadRepot;
import com.payqwikapp.model.admin.report.WalletOutstandingDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.merchant.MerchantRegisterDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.UserResponseDTO;
import com.payqwikapp.util.ClientException;
import com.upi.model.GetValueDTO;
import com.upi.model.MerchantTxnStatusResponse;
import com.upi.model.UpiRequest;
import com.upi.model.UpiResponse;

public interface IUserApi {

	List<UsernameListDTO> getAllAccessUsers();

	ResponseDTO updateServiceDetails(UpdateServiceDTO dto);

	ResponseDTO getAllUserInfo(String username);

	ResponseDTO resendOTPUnregistered(User user);

	ResponseDTO OTPForDirectPayment(User user);

	List<TransactionListDTO> processFiltering(List<PQTransaction> transactionList, List<PQService> services);

	TransactionListDTO getTransactionListDTO(PQTransaction t, User u, PQCommission commission);

	List<TransactionListDTO> getTransactionsBetweenForServiceType(Date startDate, Date endDate, String serviceType);

	List<TransactionReport> getAllTransactions(Date startDate, Date endDate, String serviceType);

	List<TListDTO> getAllList(List<PQTransaction> transactions);

	List<TListDTO> getTodaysTransactions();

	List<TListDTO> getTransactionsBetween(Date startDate, Date endDate);

	List<TListDTO> getAllIpayTxns(Date startDate, Date endDate);

	List<TListDTO> getAllIpayTxnsByStatus(Date startDate, Date endDate, Status status);

	List<TListDTO> getAllLoadMoneyTxns(Date startDate, Date endDate, String serviceCode);

	List<TListDTO> getLoadMoneyTxnsByStatus(Date startDate, Date endDate, Status status, String serviceCode);

	List<?> getSMSOrEmailLogs(String logType);

	List<?> getSMSOrEmailLogsByFilter(Date start, Date end, String logType);

	List<ServiceListDTO> getAllServicesWithCommission();

	List<UserListDTO> getTodayUsers(String type);

	List<UserListDTO> getUsersBetween(Date startDate, Date endDate, String type);

	List<UserDTO> findAllUser();

	double getWalletBalance(User u);

	boolean isVerified(KycDTO dto, User u);

	String getTransactionRequestInJSON(String serviceCode, String transactionRefNo);

	PGDetails findMerchantByMobile(String mobileNumber);

	ArrayList<Long> getDailyTransactionsCountFromDate();

	ArrayList<Double> getDailyTransactionsAmountFromDate();

	ResponseDTO requestOfflinePayment(PaymentRequestDTO dto, User merchant, User user);

	void changePassword(ChangePasswordDTO dto, Long id);

	void saveLMRequest(EBSRequest dto, String transactionRefNo);

	String getLMRequest(String transactionRefNo);

	void saveStoreRequest(PayStoreDTO dto, String transactionRefNo);

	String getStoreRequest(String transactionRefNo);

	void saveBTransferRequest(SendMoneyBankDTO dto, String transactionRefNo);

	String getBTransferRequest(String transactionRefNo);

	double calculateDbCrForAccount(PQAccountDetail accountDetail, boolean debit);

	void saveSendMoneyRequest(SendMoneyMobileDTO dto, String transactionRefNo);

	String getSendMoneyRequest(String transactionRefNo);

	void saveTopupRequest(CommonRechargeDTO dto, String transactionRefNo);

	String getTopupRequest(String transactionRefNo);

	void saveDTHRequest(CommonRechargeDTO dto, String transactionRefNo);

	String getDTHRequest(String transactionRefNo);

	void saveLandlineRequest(CommonRechargeDTO dto, String transactionRefNo);

	String getLandlineRequest(String transactionRefNo);

	void saveECityRequest(CommonRechargeDTO dto, String transactionRefNo);

	String getECityRequest(String transactionRefNo);

	void saveGasRequest(CommonRechargeDTO dto, String transactionRefNo);

	String getGasRequest(String transactionRefNo);

	void saveInsuranceRequest(CommonRechargeDTO dto, String transactionRefNo);

	String getInsuranceRequest(String transactionRefNo);

	boolean updateAccountType(String username, boolean kyc);

	void toggleStatus(String username, Status status);

	KYCResponse verifyByKycApi(KycDTO dto);

	AmountDTO getAdminLoginValues();

	void saveUser(RegisterDTO dto) throws ClientException;

	// merchant signup
	void merchantSignUp(RegisterDTO dto) throws ClientException;

	// merchant visa signup
	void visaMerchantSignUp(com.thirdparty.model.request.VisaMerchantRequest dto) throws ClientException;

	void registerMerchant(RegisterDTO dto) throws ClientException;

	boolean isVBankAccountSaved(KycDTO dto, PQAccountDetail account);

	boolean sendOTP(String mobileNumber, User user);

	boolean containsAccountAndMobile(String vbankAccount, String mobileNumber);

	void updateVBankAccountDetails(KYCResponse dto, VBankAccountDetail accountDetail, User user);

	void saveMerchant(MRegisterDTO dto) throws ClientException;

	void saveUnregisteredUserSendMoney(RegisterDTO dto) throws ClientException;

	User findByUserName(String name);

	User findById(long id);

	User findByAccountDetail(PQAccountDetail accountDetail);

	PQAccountDetail saveOrUpdateAccount(PQAccountDetail accountDetail);

	void changePasswordRequest(User u);

	UserDTO checkPasswordToken(String key);

	void revertAuthority();

	boolean resendMobileToken(String username);

	boolean activateEmail(String key);

	boolean checkMobileToken(String key, String mobileNumber);

	boolean checkMobileTokenForDirectPayment(String key, String mobileNumber);

	void renewPassword(ChangePasswordDTO dto);

	void editUser(RegisterDTO user);

	UserDTO getUserById(Long id);

	List<User> getUserListByPage(int page, int perPage);

	List<User> getUserListByVerifiedUsersPage(int page, int perPage);

	long getUserCount();

	double getTotalWalletBalance();

	String handleLoginFailure(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);

	String handleMpinFailure(HttpServletRequest request, String loginUsername);

	void handleLoginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication,
			String loginUsername, String ipAddress);

	String handleMpinSuccess(HttpServletRequest request, String loginUsername, String ipAddress);

	String authenticateVersion(String version);

	int updatePoints(long points, PQAccountDetail account);

	int getLoginAttempts();

	int getMpinAttempts();

	int updateBalance(double amount, User user);

	double dailyTransactionTotal(User user);

	double dailyDebitTransactionTotal(User user);

	double monthlyTransactionTotal(User user);

	double monthlyLoadMoneyTransactionTotal(User user);

	boolean saveImage(User user, ImageDTO dto);

	int updateVersion(int versionCode, int subVersionCode);

	List<PQTransaction> getUserTransactions(User user);

	void renewPasswordFromAccount(ChangePasswordDTO dto);

	void updateUserAuthority(String authority, long id);

	boolean setNewMpin(MpinDTO dto);

	boolean changeCurrentMpin(MpinChangeDTO dto);

	boolean deleteCurrentMpin(String username);

	Page<User> getTotalUsers(Pageable pageable);

	Page<User> getActiveUsers(Pageable pageable);

	Page<User> getInactiveUsers(Pageable pageable);

	Page<User> getBlockedUsers(Pageable pageable);

	Page<User> getLockedUsers(Pageable pageable);

	List<User> getAllUsers();

	int getTransactionTimeDifference();

	boolean inviteByMobile(String number, String message, User user);

	void inviteByEmail(String subject, String mailTemplate, User user);

	void inviteByEmailAddress(String subject, String mailTemplate, String email);

	void reSendEmailOTP(User user);

	boolean redeemCode(User user, String promoCode);

	boolean changeEmail(User user, String email);

	List<PQVersion> getAllVersions();

	PQVersion getLatestVersion();

	int updateGcmId(String gcmId, String username);

	User saveOrUpdateUser(User u);

	double getBalanceByUserAccount(PQAccountDetail accountDetail);

	List<LoginLog> getLastLoginOfUser(User u, Status status);

	boolean isValidLastLoginDevice(String device, User user);

	void requestNewLoginDevice(User user);

	boolean isValidLoginToken(String otp, User user);

	boolean checkMerchantMobileToken(String key, String mobileNumber);

	boolean resendMerchantMobileToken(String username);

	void unblockAdmins();

	void sendIpayBalanceAlert();

	User createUsingKycResponse(KYCResponse response, POSMerchants posMerchant);

	ResponseDTO verifyKYC(boolean request, MerchantRegisterDTO dto);

	ResponseDTO createMerchantPassword(MerchantRegisterDTO dto);

	ResponseDTO changeAdminPassword(ChangePasswordDTO dto);

	Page<String> getGCMIDs(Pageable page);

	List<PQAccountType> getAllAccountTypes();

	ResponseDTO updateAccountType(KycLimitDTO dto);

	boolean setServiceStatusInactive(String name);

	ServiceStatus setServiceStatusActive(ServiceStatus status, String name);

	ServiceStatus findByName(String name);

	List<ServiceStatusDTO> findAll();

	User getVisaMerchant(String entityId, String mVisaId);

	UserResponseDTO getUserByMobile(String username);

	ResponseDTO processUpdateAuth(AuthUpdateRequest request);

	ResponseDTO getUpdateAuthLogById(String id);

	ResponseDTO getAllAuthUpdateLogs();

	List<UsernameListDTO> getAllPossibilites(String subMobile);

	void sendSingleSMS(String mobile, String content);

	void sendBulkSMS(String content);

	void sendSingleMail(String mailId, String content, String subject);

	void sendBulkMail(String content, String subject);

	AuthenticationError validateUserRequest(String request, User user, PQService service, long timeStamp);

	void updateRequestLog(User u, long timeStamp);

	void merchantSignUp(VisaMerchantRequest merchant) throws ClientException;

	User findByAccountId(PQAccountDetail id);

	ResponseDTO kycUpdate(ApproveKycDTO request);

	ResponseDTO nonKycUpdate(ApproveKycDTO request);

	void updateQRcode(String username, String QRcode);

	boolean checkMerchantMobileTokenNew(String key, String mobileNumber);

	void updateMerchantM2P(String customerId, String mVisaId, String rupayId, String username);

	ResponseDTO updateAccess(AccessDTO dto);

	void saveAgent(AgentRequest dto) throws Exception;

	List<AgentDTO> getAllAgent(String role);

	ResponseDTO saveDonatee(DRegisterDTO dto) throws ClientException;

	List<TListDTO> convertFromRawTransactions(List<PQTransaction> transactionList);

	ResponseDTO setMpinHistory(MpinDTO dto) throws Exception;

	void saveSendMoneyToDonateeRequest(SendMoneyDonateeDTO dto, String transactionRefNo);

	void saveAgentToUser(RegisterDTO dto, User user) throws Exception;

	String getcountofagentregister(User user);

	long monthlyTransactionTotalBank(User user);

	long dailyTransactionTotalBank(User user);

	List<User> getTotalAgents();

	void sendBdayMessageAlert();

	boolean resendMobileTokenDeviceBinding(String username);

	ResponseDTO setPasswordHistory(ChangePasswordDTO dto) throws Exception;

	boolean resendMobileTokenNew(String username);

	UserBalanceDTO getUserTransaction(Date startDate, Date endDate, PQAccountDetail account);

	void saveRegisterUser(RegisterDTO dto) throws ClientException;

	List<MTransactionResponseDTO> getDTOList(List<PQTransaction> transactionList, List<TPTransaction> transactions);

	ResponseDTO getLocationByPin(String pincode);

	UserBalanceDTO getUserFilteredTransaction(PQAccountDetail account);

	// VPQMERCHANT SIGNUP
	void saveVpqMerchant(VPQMerchantDTO dto);

	List<MTransactionResponseDTO> getDTOListMerchant(List<TPTransaction> transactions);

	/*
	 * List<MTransactionResponseDTO> getDTOList(List<PQTransaction> transactions);
	 */

	UserBalanceDTO getUserFilteredTransactionByDate(Date StartDate, Date endDate, PQAccountDetail account);

	void editName(RegisterDTO dto);

	void senSMSforReversedTransactions();

	List<VersionListDTO> getAllVersion();

	ResponseDTO updateVersionDetails(UpdateServiceDTO dto);

	List<UserBalanceDTO> getExpenseOfUser(Date startDate, Date endDate, PQAccountDetail account);

	List<UserBalanceDTO> getBalanceOfUser(Date startDate, Date endDate, PQAccountDetail account);

	List<UserBalanceDTO> getExpenseOfUserForCredit(Date startDate, Date endDate, PQAccountDetail account);

	List<UserBalanceDTO> getExpenseOfTopUpUser(Date startDate, Date endDate, PQAccountDetail account);

	List<MTransactionResponseDTO> getDTOList(List<PQTransaction> transactions);

	List<UserByLocationDTO> getAllUserByLocationCode(Date StartDate, Date endDate, String pinCode);

	List<TListDTO> convertFromRawTransactionsForBillPay(List<PQTransaction> transactionList);

	void sendBillPaymentAlert();

	void saveMerchantDetails(NearByMerchantsDTO dto, long id);

	boolean saveUserDetails(NearByMerchantsDTO dto, long id);

	List<NearMerchantDetailsDTO> calculateDistance(NearByMerchantsDTO dto);

	void blockUser(String username);

	User saveUserForReferNEarn(RegisterDTO dto) throws ClientException;

	void requestNewLoginDeviceForSuperAdmin(User user);

	void createCsvForMonthlyTransaction() throws Exception;

	// User listUser(Date from, Date to, List<PQTransaction> transactions);

	// List<User> listUser(Date startDate, Date endDate);

	List<User> listUser();

	String getGCMIdByUserName(String username);

	User findByAccountDetails(PQAccountDetail accountDetail);

	List<MTransactionResponseDTO> getDebitDTOList(List<PQTransaction> transactions);

	List<MTransactionResponseDTO> getPromoCodeTransactions();

	List<MTransactionResponseDTO> getPromoCodeTransactionsFiltered(Date StartDate, Date endDate);

	ResponseDTO refundMerchantRequest(RefundDTO dto);

	void createCsvForWeeklyReconTransaction() throws Exception;

	ResponseLetsDTO getListCount(String key);

	ResponseLetsDTO getAllUserList(String key);

	ResponseLetsDTO getdebitTransactionsList(String key);

	ResponseLetsDTO getcreditTransactionsList(String key);

	ResponseLetsDTO getServiceList(String key);

	ResponseLetsDTO getTransDTO(String key, GetTransDTO dto);

	ResponseLetsDTO addSuperAdmin(String key, SuperAdminAddDTO dto);

	ResponseLetsDTO changePwdSuperAdmin(String key, PassWordSuperAdminDTO dto);

	ResponseLetsDTO blockSuperAdmin(String key, PassWordSuperAdminDTO dto);

	List<MTransactionResponseDTO> getDTOListForTreatCard(List<PQTransaction> transactionList);

	/* Admin Panel Report */

	Page<WalletOutstandingDTO> getWalletOutsReportByDate(Pageable pageable, Date from, Date to);

	Page<WalletOutstandingDTO> getWalletOutsReport(Pageable pageable, Date from, Date to);

	GetDailyAmountDTO getDailyValuesByDate(Date date);

	MISReportDTO getMISReport();

	WalletLoadRepot getWalletLoadReport(Date date, Date endDate);

	void createCsvForWeeklyPerformaceReport() throws Exception;

	ResponseDTO saveGCMIDs(SendNotificationDTO dto);

	List<TListDTO> getWooHooTransactionsBetween(Date startDate, Date endDate);

	ResponseLetsDTO getSuperAdminList(String key, UserTypeDto dto1);

	ResponseLetsDTO sendsms(String key, SmsRequest dto);

	ResponseLetsDTO sendbulksms(String key, BulkSmsRequest dto);

	ResponseLetsDTO sendmail(String key, MailRequestDTO dto);

	ResponseLetsDTO sendbulkmail(String key, BulkMailRequestDTO dto);

	ResponseLetsDTO getSuperAdminBlockList(String key, UserTypeDto dto1);

	ResponseLetsDTO blockSuperAdmin(String key, BlockDto dto);

	GetValueDTO getBescomDashValue(User user);

	ResponseDTO saveAadhar(AadharDTO dto);

	void otpForAadhar(User user);

	boolean checkMobileTokenAadhar(String key, String mobileNumber);

	void uploadGCMNotification();

	ResponseDTO sendNotification(NotificationDTO dto);

	void createCsvForWeeklyBescomReport() throws Exception;

	List<PQTransaction> getAllVijayaUPITxns(Date startDate, Date endDate);

	MerchantTxnStatusResponse bescomUpiStatus(UpiRequest request);

	List<User> getUserByAccount(List<PQAccountDetail> accounts);

	void updatePasswordHistory();

	void findActiveUserByPQTransaction() throws IOException;

	UpiResponse validateUpiMerchantToken(UpiRequest request);

	UpiResponse validateUpiSdkMerchantToken(UpiRequest request);

	UpiResponse validateUpiMerchantTokenForMerchant(UpiRequest request);

	UpiResponse converUPIstatusRequest(UpiRequest request);

	MerchantTxnStatusResponse upiStatus(UpiRequest request);

	void updateRequestLog();

	boolean agentRenewPassword(ChangePasswordDTO dto);

	ResponseDTO getCityAndState(AgentRequest dto) throws Exception;

	String getCountOfAgentRegister(UserDTO user);

	ResponseDTO getBankList() throws Exception;

	List<RefundDTO> getAllMerchantRefundRequest();

	void password();

	List<OffersDTO> getAllOffers();

	Offers AddOffers(OffersDTO dto);

	boolean checkUserExist(String username);

	List<NearAgentsDetailsDTO> nearByAgentDetails(NearByAgentsDTO dto);

	void createCsvForMonthlyTransactionReport();

	void updateOperatingSystem(User user);

	Page<SendNotification> getGcmList(Pageable pageable);

	void getDailyDetails() throws ParseException;

	ResponseDTO saveServices(AddServicesDTO dto);

	List<AadharDetailsDTO> getAllAadharDetails();

	ResponseDTO saveServiceType(AddServicesDTO dto);

	List<TListDTO> getDailyTransactions(Date startDate, Date endDate, String transactionType);

	AgentDetailsDTO getAgentDetailsById(String agentId);

	User saveUnregisteredSdkUser(RegisterDTO dto, String device) throws ClientException;

	LoginLog getRemoteAddress(User user);

	ResponseDTO updateUserMaxLimit(UserMaxLimitDto dto, User user);

	ResponseDTO getUserMaxLimit(UserMaxLimitDto dto, User user);

	long getDailyTransactionCount(User senderUser);

	long getMonthlyTransactionCount(User senderUser);

	ResponseDTO sendMaxLimitOtp(User user);

	boolean validateUserPassword(LoginDTO dto, String password);

	void createCsvForWeeklyPerformaceReport1() throws Exception;

	void createCsvForWeeklyPerformaceReport2() throws Exception;

	void createCsvForWeeklyPerformaceReport3() throws Exception;

	boolean resendUserMobileToken(String username);

}
