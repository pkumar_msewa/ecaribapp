package com.payqwikapp.cronjob;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.payqwikapp.api.IAdminApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.BankDetails;
import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.BankTransferDTO;

public class AutoReportGenerator {
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	String dirName="C:\\Users\\Abhijit-PC\\Desktop\\demo";   //to be changed while merging....
	private final IAdminApi adminApi;
	private final IUserApi userApi;
	private final JavaMailSender mailSender;
	File fileOut=null;
	FileOutputStream out=null;
	
	
	

	/*public void reportGenerator(){
		System.err.println("cron is working............");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		 Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(System.currentTimeMillis());
	    String date= dateFormat.format(calendar.getTime());

	        // Get a Date object from the date string
	        Date myDate = null;
			try {
				myDate = dateFormat.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        // this calculation may skip a day (Standard-to-Daylight switch)...
	        //oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));

	        // if the Date->time xform always places the time as YYYYMMDD 00:00:00
	        //   this will be safer.
	       Date oneDayBefore = new Date(myDate.getTime() - 2);

	        String result = dateFormat.format(oneDayBefore);
		List<BankTransferDTO> list=bankTransfer.findBankNeftRequests("2016-12-08");
		
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			System.err.println(itr.next());
		}
		
		
	}*/
	
	
	public AutoReportGenerator(IAdminApi adminApi, IUserApi userApi,JavaMailSender mailSender) {
		super();
		this.adminApi = adminApi;
		this.userApi = userApi;
		this.mailSender=mailSender;
	}



	public static String generateSixDigitNumericString() {
		return "" + (int) (Math.random() * 1000000);
	}

	

	
	
	public void reportGenerator() throws InterruptedException{
		 Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(System.currentTimeMillis());
	    String date= dateFormat.format(calendar.getTime());

	        // Get a Date object from the date string
	        Date myDate = null;
			try {
				myDate = dateFormat.parse(date);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        // this calculation may skip a day (Standard-to-Daylight switch)...
	        //oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));

	        // if the Date->time xform always places the time as YYYYMMDD 00:00:00
	        //   this will be safer.
	       Date oneDayBefore = new Date(myDate.getTime()-(24 * 3600000));
	       Thread.sleep(10000);
	       System.err.println("the time is============"+oneDayBefore);
	       Thread.sleep(10000);
	       

	        String result = dateFormat.format(oneDayBefore);
	        System.err.println("this is the string date========="+result);

		 //File file = new File( dirName +"\\"+generateSixDigitNumericString()+"Banktransfer.xlsx");  
		
		 fileOut = new File(dirName +"\\"+"Banktransfer.xls");
		System.err.println("cron is working............");
		List<BankTransfer> bankTransfers = adminApi.getAllTransferReports();
		List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
		User bank = userApi.findByUserName("bank@vpayqwik.com");
		String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
		for(BankTransfer bt: bankTransfers){
			BankTransferDTO bdto = new BankTransferDTO();
			//bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
			
			if((dateFormat.format(bt.getCreated())).equals(result)){
				bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
			bdto.setAmount(""+bt.getAmount());
			bdto.setBeneficiaryAccountNumber(""+bt.getBeneficiaryAccountNumber());
			bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
			bdto.setTransactionID(bt.getTransactionRefNo());
			BankDetails bankDetails = bt.getBankDetails();
			if(bankDetails != null) {
				//bdto.setBankName(bankDetails.getBank().getName());
				bdto.setIfscCode(bankDetails.getIfscCode());
			}
			bdto.setMobileNumber(bt.getSender().getUsername());
			bdto.setName(bt.getSender().getUserDetail().getFirstName());
			//bdto.setEmail(bt.getSender().getUserDetail().getEmail());
			
			
			
			
			
			//bdto.setVirtualAccount(""+bt.getSender().getAccountDetail().getAccountNumber());
			
			//bdto.setBankVirtualAccount(accountNumber);
		//	bdto.setStatus(String.valueOf(Status.Success));
			//PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
			//if(temp != null){
			//	bdto.setStatus(temp.getStatus().getValue());
				//bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
		//	}
			System.out.println(bdto.getTransactionDate());
			System.out.println(bdto.getBankName());
			System.out.println(bdto.getBeneficiaryAccountNumber());
			
			bankTransferDTOs.add(bdto);
			
			
			
		}
		}
		System.out.println(bankTransferDTOs);
		
	      
	        HSSFWorkbook workbook  = new HSSFWorkbook();
	        HSSFSheet excelSheet = workbook.createSheet("banktransfer");
	        setExcelHeader(excelSheet);
	        setExcelRows(excelSheet, bankTransferDTOs);
	        
			try {
	        	 out=new FileOutputStream(fileOut);
				workbook.write(out);
				out.flush();
				sendMail(dirName +"\\"+"Banktransfer.xls");
				
			} catch (IOException | AddressException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        finally{
	        	try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
		}
	

	public static void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("created");
		excelHeader.createCell(1).setCellValue("amount");
		excelHeader.createCell(2).setCellValue("beneficiaryAccountNumber");
		excelHeader.createCell(3).setCellValue("beneficiaryName");
		excelHeader.createCell(4).setCellValue("transactionRefNo");
		excelHeader.createCell(5).setCellValue("ifscCode");
		excelHeader.createCell(6).setCellValue("username");
		excelHeader.createCell(7).setCellValue("firstName");
		excelHeader.createCell(8).setCellValue("AccountType");
		excelHeader.createCell(9).setCellValue("vpayqwik Debit Pool Account");
		

	}
	public void setExcelRows(HSSFSheet excelSheet, List<BankTransferDTO> bankTransferDTOs) {
		int record = 1;
		for (BankTransferDTO dto : bankTransferDTOs) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(dto.getTransactionDate());
			excelRow.createCell(1).setCellValue(dto.getAmount());
			excelRow.createCell(2).setCellValue(dto.getBeneficiaryAccountNumber());
			excelRow.createCell(3).setCellValue(dto.getBeneficiaryAccountName());
			excelRow.createCell(4).setCellValue(dto.getTransactionID());
			excelRow.createCell(5).setCellValue(dto.getIfscCode());
			excelRow.createCell(6).setCellValue(dto.getMobileNumber());
			excelRow.createCell(7).setCellValue(dto.getName());
			excelRow.createCell(8).setCellValue("KYC");
			excelRow.createCell(9).setCellValue("133100037672007");
			
		}
	}
	
	public void sendMail(String fileOut) throws AddressException{
		
		String msg="Hi team,"
				+ "\n"
				+ "Please find the attached sheet of bank transfer request"
				+ "\n"
				+ "Please reply back for any failed transaction so that we can refund to the customer's wallet.";
		
		String [] cc={
				
				"kamal@msewa.com",
				"abhijitp@msewa.com",
				"pkumar@msewa.com",
				"ggupta@msewa.com",
				"manjunath@msewa.com",
				"alladcteam@vijayabank.co.in",
				"sandeepnath.h@vijayabank.co.in"
				
				
		};
		
		 try{  
		        MimeMessage message = mailSender.createMimeMessage();  
		        MimeMessageHelper helper = new MimeMessageHelper(message, true);  
		        //to be changed while pushing to production
		        helper.setFrom("abhi.priyadarshan3019@gmail.com");  
		        helper.setTo("wel.abhijit@gmail.com");  
		        helper.setCc(cc);
		      //  helper.setBcc("wel.abhijit@gmail.com");
		        helper.setSubject("Fund Transfer Requests");  
		        helper.setText(msg);  
		  
		        // attach the file  
		        FileSystemResource file = new FileSystemResource(fileOut);  
		      helper.addAttachment(file.getFilename(), file);
		      
		        mailSender.send(message);  
		        }
		 catch(javax.mail.MessagingException e){e.printStackTrace();
		    }  
	}
	
	}

