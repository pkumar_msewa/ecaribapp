package com.payqwikapp.cronjob;

import com.payqwikapp.api.*;
import com.payqwikapp.util.DeploymentConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.api.IMerchantApi;
import com.upi.util.UPIBescomConstants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.util.SystemPropertyUtils;

public class AutoReverse {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final ISendMoneyApi sendMoneyApi;
	private final IEBSApi ebsApi;
	private final IImagicaApi imagicaApi;
	private final TransactionOperations transactionOperations;
	private final IBusApi busApi;
	private final IFlightApi flightApi;
	private final IAdminApi adminApi;
	private final IMerchantApi merchantApi;
	private final AutoGiftCardStatus autoGiftCardStatus;
	private ITKPlaceOrderApi placeorderApi;

	public AutoReverse(IUserApi userApi, ITransactionApi transactionApi, ISendMoneyApi sendMoneyApi, IEBSApi ebsApi,
			IImagicaApi imagicaApi, TransactionOperations transactionOperations, IBusApi busApi, IFlightApi flightApi,
			IAdminApi adminApi, IMerchantApi merchantApi, AutoGiftCardStatus autoGiftCardStatus,
			ITKPlaceOrderApi placeorderApi) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.sendMoneyApi = sendMoneyApi;
		this.ebsApi = ebsApi;
		this.imagicaApi = imagicaApi;
		this.transactionOperations = transactionOperations;
		this.busApi = busApi;
		this.flightApi = flightApi;
		this.adminApi = adminApi;
		this.merchantApi = merchantApi;
		this.autoGiftCardStatus = autoGiftCardStatus;
		this.placeorderApi = placeorderApi;
	}

	public void checkAccountForReverse() {
		if(DeploymentConstants.PRODUCTION){
		transactionApi.revertSendMoneyOperations();
		}
	}

	public void sendAlertSMS() {
		if(DeploymentConstants.PRODUCTION){
		userApi.sendIpayBalanceAlert();
		}
	}

	public void unblockUser() {
		userApi.revertAuthority();
	}

	public void unblockAdmins() {
		userApi.unblockAdmins();
	}

	public void getNeftRequestFile() throws IOException {
		if(DeploymentConstants.PRODUCTION){
		sendMoneyApi.getNeftRequestFile();
		}
	}

	public void getRefundStatusFromEbs() throws IOException {
		// ebsApi.responseFromWeb();
	}

	public void createImagicaSession() {
		logger.info("i am here to generate session for imagica.");
		if(DeploymentConstants.PRODUCTION){
		imagicaApi.createSession();
		}
	}

	public void sendBdayMessage() {
		logger.info("i am here for my bday");
		if(DeploymentConstants.PRODUCTION){
		userApi.sendBdayMessageAlert();
		}
	}

	public void sendBillPaymentAlert() {
		logger.info("i am here for my monthly bill payment Transaction");
		if(DeploymentConstants.PRODUCTION){
		userApi.sendBillPaymentAlert();
		}
	}

	// EMT Bus City List
	public void getAllCityListForBus() throws Exception {
		logger.info("Cron job for get all city list for bus");
		if(DeploymentConstants.PRODUCTION){
		busApi.cronforGetAllCityList();
		}
	}

/*	public void createCsvForMonthlyTransaction() throws Exception {
		logger.info("Cron job for csv file");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForMonthlyTransaction();
		}
	}*/

	// EMT Bus City List
	public void getAllCityListForflight() throws Exception {
		logger.info("Cron job for get all city list for flight");
		if(DeploymentConstants.PRODUCTION){
		flightApi.cronforSaveCityList();
		}
	}

	public void createCsvForWeeklyReconTransaction() throws Exception {
		logger.info("Cron job for csv1 file");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForWeeklyReconTransaction();
		}
	}

	public void createPayQwikPerformanceWeeklyReport() throws Exception {
		logger.info("Cron job for Performance report file");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForWeeklyPerformaceReport();
		}
	}
	public void createPayQwikPerformanceWeeklyReport1() throws Exception {
		logger.info("Cron job for Performance report file");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForWeeklyPerformaceReport1();
		}
	}
	public void createPayQwikPerformanceWeeklyReport2() throws Exception {
		logger.info("Cron job for Performance report file");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForWeeklyPerformaceReport2();
		}
	}
	public void createPayQwikPerformanceWeeklyReport3() throws Exception {
		logger.info("Cron job for Performance report file");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForWeeklyPerformaceReport3();
		}
	}
	
	public void uploadBulkFile() throws IOException {
		logger.info("Cron job for bulk upload files");
		if(DeploymentConstants.PRODUCTION){
		adminApi.uploadBulkFiles();
		}
	}

	public void bescomS2SCall() throws IOException {
		logger.info("Cron job for BESCOM S2S Call");
//		if(DeploymentConstants.PRODUCTION){
//			merchantApi.bescomS2SCall();
//		}
		merchantApi.bescomS2SCall();
	}
	
	public void sendMoneyToWinners() throws IOException {
		logger.info("Cron job for Predict winner");
		if(DeploymentConstants.PRODUCTION){
		adminApi.sendMoneyToWinners();
		}
	}

	public void bescomReconCall() throws Exception {
		logger.info("Cron job for BESCOM Recon Call");
		if(DeploymentConstants.PRODUCTION){
		userApi.createCsvForWeeklyBescomReport();
		}
	}

	// WOOHOO

	public void GiftCards() throws IOException {
		logger.info("Cron job for WOOHOO Status Check Call");
		if(DeploymentConstants.PRODUCTION){
		autoGiftCardStatus.GiftCards();
		}
	}

	public void uploadGCMNotification() throws IOException {
		logger.info("Cron job for upload notification files");
		if(DeploymentConstants.PRODUCTION){
		userApi.uploadGCMNotification();
		}
	}

	public void updatePasswordHisory() throws IOException {
		logger.info("Cron job for update password history to remove service unavailable");
		if(DeploymentConstants.PRODUCTION){
		userApi.updatePasswordHistory();
		}
	}

	public void findActiveUserByPQTransaction() throws IOException {
		logger.info("Cron job for findActiveUserByPQTransaction");
		if(DeploymentConstants.PRODUCTION){
		userApi.findActiveUserByPQTransaction();
		}
	}

	public void getTrainListFromTK() {
		logger.info("cron job for TrainListFromTK");
		if(DeploymentConstants.PRODUCTION){
		placeorderApi.getTrainListFromTK();
		}
	}

	public void updateRequestLog() {
		logger.info("cron job for update RequestLog");
		if(DeploymentConstants.PRODUCTION){
		userApi.updateRequestLog();
		}
	}

	public void password() {
		logger.info("cron job for password update");
//		if(DeploymentConstants.PRODUCTION){
		userApi.password();
//		}
	}

	public void thinkWallnutProcessing() {
		logger.info("cron job for update think wallnut transactions");
		if(DeploymentConstants.PRODUCTION){
		transactionApi.updateSuspiciousTransaction();
		}
	}

	public void generatePPIFile() throws IOException {
		logger.info("cron job for generate PPI file");
		if(DeploymentConstants.PRODUCTION){
		sendMoneyApi.generatePPIFile();
		}
	}
	

	public void uploadPPIFile() throws ParseException {
		logger.info("cron job for upload PPI file");
		if(DeploymentConstants.PRODUCTION){
		sendMoneyApi.uploadPPIFile();
		}
	}

	public void getTotalCommisionByMonthwise() throws Exception {
		logger.info("cron job for get total commision by monthwise");
		if(DeploymentConstants.PRODUCTION){
		transactionApi.findTotalCommisionByMonthWise();
		}
	}

	public void getTotalUserByMonthwise() throws Exception {
		if(DeploymentConstants.PRODUCTION){
		transactionApi.findTotalUserMonthWise();
		}
	}
	
	public void createCsvForMonthlyTransactionReport() throws Exception {
		System.err.println("Monthly cron running...");
		if (DeploymentConstants.PRODUCTION) {
			userApi.createCsvForMonthlyTransactionReport();
		}
	}
	
	public void getDailyDetails() throws Exception {
		System.err.println("Daily report transaction...");
		if(DeploymentConstants.PRODUCTION){
		userApi.getDailyDetails();
		}
	}
	
	public void testCron() {
		System.out.println("Printing cron test");
		Client colMoney = new Client();
		colMoney.addFilter(new LoggingFilter(System.out));
		WebResource resource = colMoney.resource(UPIBescomConstants.URL_TRANSACTION);
		ClientResponse clientResponse = resource.get(ClientResponse.class);
		String stringResponse = clientResponse.getEntity(String.class);
		System.out.println("String response :: " + stringResponse);
	}
	
	public static void main(String[] args) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		cal.add(Calendar.PM, 23);
		Date startDate = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);
		Date endDate = cal2.getTime();
		
//		dateFormat.parse(startDate);
		
		System.err.println("Start Date : : " + dateFormat.format(startDate));
		System.err.println("End Date   : : " + dateFormat.format(endDate));
	}
}
