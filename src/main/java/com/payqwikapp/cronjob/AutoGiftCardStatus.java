package com.payqwikapp.cronjob;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.WoohooCardDetails;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.WoohooCardDetailsRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.StartupUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class AutoGiftCardStatus {

	private final ITransactionApi transactionApi;
	private final PQTransactionRepository pqTransactionRepository;
	private final PQServiceRepository pqServiceRepository;
	private final WoohooCardDetailsRepository woohooCardDetailsRepository;
	private final ISMSSenderApi smsSenderApi;

	public AutoGiftCardStatus(ITransactionApi transactionApi, PQTransactionRepository pqTransactionRepository,
			PQServiceRepository pqServiceRepository, WoohooCardDetailsRepository woohooCardDetailsRepository,
			ISMSSenderApi smsSenderApi) {
		this.transactionApi = transactionApi;
		this.pqTransactionRepository = pqTransactionRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.woohooCardDetailsRepository = woohooCardDetailsRepository;
		this.smsSenderApi = smsSenderApi;
	}

	public void GiftCards() {
		PQService service = pqServiceRepository.findServiceByCode("WOOHO");
		List<PQTransaction> pending = pqTransactionRepository.findPendingTransacions(service);
		if (pending != null && !pending.isEmpty()) {
			for (PQTransaction transaction : pending) {
				try {
						Client client = Client.create();
						client.addFilter(new LoggingFilter(System.out));
						WebResource webResource = client.resource(StartupUtil.AUTOCHECKSTATUS);
						JSONObject json = new JSONObject();
						json.put("referenceNo", transaction.getRetrivalReferenceNo());
						ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,json);
						String strResponse = response.getEntity(String.class);
						if (response.getStatus() == 200) {
							System.err.println("response ::" + strResponse);
							org.json.JSONObject result = new org.json.JSONObject(strResponse);
							if (result != null) {
								boolean success = result.getBoolean("success");
								String status = result.getString("order_status");
								if (success && status.equalsIgnoreCase("complete")) {
									WoohooCardDetails dto = woohooCardDetailsRepository.findByRetrivalRefNo(transaction.getRetrivalReferenceNo());
									if (dto != null) {
										dto.setCard_price(result.getString("card_price"));
										dto.setCardName(result.getString("cardName"));
										dto.setCardnumber(result.getString("cardnumber"));
										dto.setExpiry_date(result.getString("expiry_date"));	
										dto.setOrderId(result.getString("orderNumber"));
										dto.setPin_or_url(result.getString("pin_or_url"));
										dto.setRetrivalRefNo(result.getString("retrivalRefNo"));
										dto.setStatus("Success");
										woohooCardDetailsRepository.save(dto);
										PQTransaction t= pqTransactionRepository.findByTransactionRetivalReferenceNo(transaction.getRetrivalReferenceNo());
										if(t!=null){
										t.setCheckStatus(false);
										pqTransactionRepository.save(t);
										User u = dto.getUser();
										smsSenderApi.sendWoohooSMS(SMSAccount.PAYQWIK_TRANSACTIONAL, SMSTemplate.WOOHOO_GIFTCARD_SENDER_SUCCESS_SMS,dto, u);
										smsSenderApi.sendWoohooSMS(SMSAccount.PAYQWIK_TRANSACTIONAL,SMSTemplate.WOOHOO_GIFTCARD_RECEIVER_SUCCESS_SMS, dto, u);
										}
									 }
								 }else if(!success && status.equalsIgnoreCase("Failed")){
									 RefundDTO dto = new RefundDTO();
									 dto.setTransactionRefNo(transaction.getTransactionRefNo());
									 transactionApi.processWoohooRefund(dto);
								 }else{
									 System.err.println("transaction is pending.");
								 }
							 }
						}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			System.err.println("their is no pending transaction .");
		}
	}

}
