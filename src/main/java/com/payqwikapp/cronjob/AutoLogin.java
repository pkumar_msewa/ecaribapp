package com.payqwikapp.cronjob;

import com.payqwikapp.api.IEBSApi;
import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import java.io.IOException;

public class AutoLogin {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUserApi userApi;
	private final TransactionOperations transactionOperations;

	public AutoLogin(IUserApi userApi,  TransactionOperations transactionOperations) {
		this.userApi = userApi;
		this.transactionOperations = transactionOperations;
	}

	public void checkAccountForReverse() {
		logger.info("cron");
	    //transactionApi.revertSendMoneyOperations();
	}

	public void sendAlertSMS(){
		userApi.sendIpayBalanceAlert();
	}

	public void unblockUser() {
		userApi.revertAuthority();
	}

	public void unblockAdmins(){
		userApi.unblockAdmins();
	}
	
	public void sendBdayMsg(){
		userApi.sendBdayMessageAlert();
	}
	public void getNeftRequestFile() throws IOException{
//		sendMoneyApi.getAllRequestDTO();
	}
	public void getRefundStatusFromEbs() throws IOException{
//		ebsApi.responseFromWeb();
	}
}
