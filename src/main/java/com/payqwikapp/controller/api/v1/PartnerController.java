package com.payqwikapp.controller.api.v1;

import com.payqwikapp.api.IPartnerApi;
import com.payqwikapp.model.PDeviceUpdateDTO;
import com.payqwikapp.model.error.PDeviceUpdateError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/ws/api/partner")
public class PartnerController {

    private final IPartnerApi partnerApi;

    public PartnerController(IPartnerApi partnerApi) {
        this.partnerApi = partnerApi;
    }

    @RequestMapping(value="/registerDevice",method = RequestMethod.POST,produces= MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO> registerNewPartnerDevice(@RequestBody PDeviceUpdateDTO dto, HttpServletRequest request,HttpServletResponse response){
        ResponseDTO result = new ResponseDTO();
        PDeviceUpdateError error = partnerApi.saveDetails(dto);
        if(error.isSuccess()) {
            result.setStatus(ResponseStatus.SUCCESS);
        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
        }
        result.setMessage(error.getMessage());
        result.setDetails(result.getMessage());
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }
}
