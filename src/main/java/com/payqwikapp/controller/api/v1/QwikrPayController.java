package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.IQwikrPayApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.QwikrPayRequest;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class QwikrPayController {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private IUserApi userApi;
	private PersistingSessionRegistry sessionRegistry;
	private UserSessionRepository userSessionRepository;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository pqServiceRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private IQwikrPayApi qwikrPayApi;
	
	public QwikrPayController(IUserApi userApi, PersistingSessionRegistry sessionRegistry,
			UserSessionRepository userSessionRepository, TransactionValidation transactionValidation,
			PQServiceRepository pqServiceRepository,PGDetailsRepository pgDetailsRepository,IQwikrPayApi qwikrPayApi) {
		super();
		this.userApi = userApi;
		this.sessionRegistry = sessionRegistry;
		this.userSessionRepository = userSessionRepository;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
		this.pgDetailsRepository=pgDetailsRepository;
		this.qwikrPayApi=qwikrPayApi;
	}


	@RequestMapping(value = "/QwikrPayOrderInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> GiftcartorderInitiate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody QwikrPayRequest qwikRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
						User user=userApi.findByUserName(qwikRequest.getMobileNo());
						User merchant = userApi.findByUserName(qwikRequest.getMerchantUserName());
						if(merchant!=null){
						
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.QWIK_PAY_CODE);
						System.err.println("service"+service);
						TransactionError transactionError = transactionValidation.validateQwikrPayPayment(qwikRequest.getAmount(),
								user.getUsername(), service);
						System.err.println("error"+transactionError);
						if (transactionError.isValid()) {
							System.err.println("m here where error is valid");
							String transactionRefNo  = qwikrPayApi.initiateQwikrPayPayment(qwikRequest,user.getUsername(),
									service);

							result.setBalance(user.getAccountDetail().getBalance());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Payment of QwikrPay product Initiated Successful from " + user.getUsername() + " .");
							result.setTxnId(transactionRefNo); 
							result.setValid(true);
							result.setCode("S00");
							System.err.println(result);
							
							// {"status":null,"code":null,"message":null,"balance":2177.6099999999997,"details":null,"txnId":null}
							
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(transactionError.getMessage());
							result.setDetails(transactionError.getMessage());
							result.setTxnId("");
							result.setValid(false);
							result.setCode("F00");
							System.err.println("m herer where error is not valid");
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized Merchant.");
						result.setDetails("Failed, Unauthorized Merchant.");
						result.setTxnId("");
						result.setValid(false);
						result.setCode("F00");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("merchant doesn't exist...");
					result.setDetails("merchant doesn't exist...");
					result.setTxnId("");
					result.setValid(false);
					result.setCode("F00");
				}
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			} 
			
		 
		

	
	
	
	@RequestMapping(value = "/QwikrPayOrderSuccess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> GiftcartorderSuccess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody QwikrPayRequest qwikrPay, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
				System.err.println("m hererererere");
						User user = userApi.findByUserName(qwikrPay.getMobileNo());
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.QWIK_PAY_CODE);
						TransactionResponse resp = qwikrPayApi.successQwikrPayPayment(qwikrPay,
									user.getUsername(), service, user);

						if(resp.isSuccess()){
							result.setBalance(user.getAccountDetail().getBalance());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Payment of QwikrPay product(s) Successful from  " + user.getUsername() + " .");
							result.setDetails(resp);
							result.setTxnId("");
							result.setValid(false);
							result.setCode("S00");
						}else{
							result.setBalance(user.getAccountDetail().getBalance());
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Payment of QwikrPay product(s) failed from  " + user.getUsername() + " .");
							result.setDetails(resp);
							result.setTxnId("");
							result.setValid(false);
							result.setCode("F00");
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						result.setTxnId("");
						result.setValid(false);
						result.setCode("F00");
					}
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}

			
		

	

}
