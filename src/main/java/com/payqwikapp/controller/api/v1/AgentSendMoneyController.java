package com.payqwikapp.controller.api.v1;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.error.SendMoneyMobileError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.BankDetailRepository;
import com.payqwikapp.repositories.BanksRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.SendMoneyValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/SendMoney")
public class AgentSendMoneyController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ISendMoneyApi sendMoneyApi;
	private final IUserApi userApi;
	private final SendMoneyValidation sendMoneyValidation;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final BanksRepository banksRepository;
	private final BankDetailRepository bankDetailRepository;

	public AgentSendMoneyController(ISendMoneyApi sendMoneyApi, IUserApi userApi,
			SendMoneyValidation sendMoneyValidation, TransactionValidation transactionValidation,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			PQServiceRepository pqServiceRepository, BanksRepository banksRepository,
			BankDetailRepository bankDetailRepository) {
		this.sendMoneyApi = sendMoneyApi;
		this.userApi = userApi;
		this.sendMoneyValidation = sendMoneyValidation;
		this.transactionValidation = transactionValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.banksRepository = banksRepository;
		this.bankDetailRepository = bankDetailRepository;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AgentMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processAgentMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileDTO mobile, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(mobile, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				SendMoneyMobileError error = new SendMoneyMobileError();
				String sessionId = mobile.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					String serviceCode = sendMoneyApi.prepareSendMoney(mobile.getMobileNumber());
					error = sendMoneyValidation.checkMobileError(mobile, user.getUsername(), serviceCode);
					if (error.isValid()) {
						if (user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(serviceCode);
							AuthenticationError requestError = userApi.validateUserRequest(mobile.toString(),
									userSession.getUser(), service, timeStamp);
							if (requestError.isSuccess()) {
								TransactionError transactionError = transactionValidation.validateP2PTransaction(
										mobile.getAmount(), user.getUsername(), mobile.getMobileNumber(), service);
								if (transactionError.isValid()) {
									User u = userApi.findByUserName(user.getUsername());
									sendMoneyApi.sendMoneyAgentMobile(mobile, user.getUsername(), service);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Sent money to mobile successful. Money Sent to "
											+ mobile.getMobileNumber());
									result.setDetails("Money Sent to " + mobile.getMobileNumber());
									result.setBalance(u.getAccountDetail().getBalance());
									userApi.updateRequestLog(u, timeStamp);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(transactionError.getMessage());
									userApi.updateRequestLog(userApi.findByUserName(user.getUsername()), timeStamp);
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage(requestError.getMessage());
								result.setDetails(requestError.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage(error.getMobileNumber());
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RequestAgentMoney", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> requestmoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileDTO mobile, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(mobile, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				SendMoneyMobileError error = new SendMoneyMobileError();
				String sessionId = mobile.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());

					String serviceCode = "SALMTA";// sendMoneyApi.prepareSendMoney(mobile.getMobileNumber());
					error = sendMoneyValidation.checkMobileError(mobile, user.getUsername(), serviceCode);
					if (error.isValid()) {
						if (user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(serviceCode);
							AuthenticationError requestError = userApi.validateUserRequest(mobile.toString(),
									userSession.getUser(), service, timeStamp);
							if (requestError.isSuccess()) {
								TransactionError transactionError = transactionValidation.validateP2PTransaction(
										mobile.getAmount(), user.getUsername(), mobile.getMobileNumber(), service);
								if (transactionError.isValid()) {
									System.err.println("" + user.getUsername());
									User u = userApi.findByUserName(user.getUsername());
									sendMoneyApi.requestMoneyAgentMobile(mobile, user.getUsername(), service);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Load money to Request successful.");
									result.setDetails("Load Money to Agent ");
									result.setBalance(u.getAccountDetail().getBalance());
									userApi.updateRequestLog(u, timeStamp);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(transactionError.getMessage());
									userApi.updateRequestLog(userApi.findByUserName(user.getUsername()), timeStamp);
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage(requestError.getMessage());
								result.setDetails(requestError.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage(error.getMobileNumber());
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SucessAgentMoney", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> SucessAgentMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileDTO mobile, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(mobile, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				SendMoneyMobileError error = new SendMoneyMobileError();
				String sessionId = mobile.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());

					String serviceCode = "SALMTA";// sendMoneyApi.prepareSendMoney(mobile.getMobileNumber());
					error = sendMoneyValidation.checkMobileError(mobile, user.getUsername(), serviceCode);
					if (error.isValid()) {
						if (user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(serviceCode);
							AuthenticationError requestError = userApi.validateUserRequest(mobile.toString(),
									userSession.getUser(), service, timeStamp);
							if (requestError.isSuccess()) {
								TransactionError transactionError = transactionValidation.validateP2PTransaction(
										mobile.getAmount(), user.getUsername(), mobile.getMobileNumber(), service);
								if (transactionError.isValid()) {

									User u = userApi.findByUserName(user.getUsername());
									sendMoneyApi.agentRequestmoneysuccess(mobile.getTransactionrefno());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Load money to Request successful.");
									result.setDetails("Load Money to Agent ");
									result.setBalance(u.getAccountDetail().getBalance());
									userApi.updateRequestLog(u, timeStamp);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									 result.setMessage(transactionError.getMessage());
									 result.setDetails(transactionError.getMessage());
									userApi.updateRequestLog(userApi.findByUserName(user.getUsername()), timeStamp);
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage(requestError.getMessage());
								result.setDetails(requestError.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage(error.getMobileNumber());
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

}
