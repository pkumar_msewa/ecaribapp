package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.model.request.ServicesRequest;
import com.instantpay.model.request.UserResponse;
import com.payqwikapp.api.IMerchantApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.BankDetails;
import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.Banks;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.SendNotification;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.BankTransferDTO;
import com.payqwikapp.model.MerchantDTO;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.NikkiChatResponseDTO;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.SendNotificationDTO;
import com.payqwikapp.model.ServiceTypeDTO;
import com.payqwikapp.model.ServicesDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionApiResponse;
import com.payqwikapp.model.TransactionCommissionListDTO;
import com.payqwikapp.model.TransactionDTO;
import com.payqwikapp.model.TransactionListDTO;
import com.payqwikapp.model.TransactionReport;
import com.payqwikapp.model.TransactionType;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserStatus;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.admin.report.WalletOutstandingDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.mobile.UserResponseDTO;
import com.payqwikapp.repositories.BankDetailRepository;
import com.payqwikapp.repositories.BanksRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQCommissionRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.TransactionComparator;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class AjaxController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUserApi userApi;
	private final IMerchantApi merchantApi;
	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final ISessionApi sessionApi;
	private final PQServiceRepository pqServiceRepository;
	private final BanksRepository banksRepository;
	private final BankDetailRepository bankDetailRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQCommissionRepository pqCommissionRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
	private final SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public AjaxController(IUserApi userApi, ITransactionApi transactionApi, UserSessionRepository userSessionRepository,
			ISessionApi sessionApi, IMerchantApi merchantApi, PQServiceRepository pqServiceRepository,
			BanksRepository banksRepository, BankDetailRepository bankDetailRepository,
			PQServiceTypeRepository pqServiceTypeRepository, PQCommissionRepository pqCommissionRepository,PGDetailsRepository pgDetailsRepository) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.userSessionRepository = userSessionRepository;
		this.sessionApi = sessionApi;
		this.merchantApi = merchantApi;
		this.pqServiceRepository = pqServiceRepository;
		this.banksRepository = banksRepository;
		this.bankDetailRepository = bankDetailRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqCommissionRepository = pqCommissionRepository;
		this.pgDetailsRepository=pgDetailsRepository;
	}

	@RequestMapping(value = "/getTotalUsers", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							String strUserStatus = dto.getUserStatus();
							UserStatus userStatus = UserStatus.getEnum(strUserStatus);
							Page<User> pg = null;
							switch (userStatus.getValue()) {
							case "Online":
								pg = sessionApi.findOnlineUsers(pageable);
								break;
							case "Active":
								pg = userApi.getActiveUsers(pageable);
								break;
							case "Blocked":
								pg = userApi.getBlockedUsers(pageable);
								break;
							case "Inactive":
								pg = userApi.getInactiveUsers(pageable);
								break;
							case "Locked":
								pg = userApi.getLockedUsers(pageable);
								break;
							default:
								pg = userApi.getTotalUsers(pageable);
								break;
							}
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | User List");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getReports", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getReports(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = date.format(dto.getStartDate()) + " 00:00:00";
							String endDate = date.format(dto.getEndDate()) + " 23:59:59";
							dto.setStartDate(dateTime.parse(startDate));
							dto.setEndDate(dateTime.parse(endDate));
							String serviceType = "";
							boolean debit = false;
							switch (reportType.toUpperCase()) {
							case "TOPUPBILL":
								serviceType = "Bill Payment";
								debit = false;
								break;
							case "ELECTRICITY":
								serviceType = "Electricity Bill Payment";
								debit = false;
								break;
							case "LOADMONEY":
								serviceType = "Load Money";
								debit = false;
								break;
							case "GCI":
								serviceType = "GiftCart Booking";
								debit = true;
								break;
							default:
								serviceType = "Load Money";
								debit = false;
								break;
							}
							PQServiceType type = pqServiceTypeRepository.findServiceTypeByName(serviceType);
							transactions = transactionApi.listTransactionByServiceAndDate(dto.getStartDate(),
									dto.getEndDate(), type, debit, TransactionType.DEFAULT, Status.Success);
							reports = ConvertUtil.getFromTransactionList(transactions, type);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(reports);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTotalTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactions(pageable);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transaction List");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getTotalTransactionsFiltered", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionPagesFiltered(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = dto.getFromDate() + " 00:00";
							String endDate = dto.getToDate()+ " 23:59";
							dto.setStartDate(dateFormat.parse(startDate));
							dto.setEndDate(dateFormat.parse(endDate));
							String serviceType = "";
							boolean debit = false;
							switch (reportType.toUpperCase()) {
							case "TOPUPBILL":
								serviceType = "Bill Payment";
								debit = false;
								break;
							case "ELECTRICITY":
								serviceType = "Electricity Bill Payment";
								debit = false;
								break;
							case "LOADMONEY":
								serviceType = "Load Money";
								debit = false;
								break;
							case "GCI":
								serviceType = "GiftCart Booking";
								debit = false;
								break;
							default:
								serviceType = "Load Money";
								debit = false;
								break;
							}
							PQServiceType type = pqServiceTypeRepository.findServiceTypeByName(serviceType);
							transactions = transactionApi.listTransactionByServiceAndDate(dto.getStartDate(),
									dto.getEndDate(), type, debit, TransactionType.COMMISSION, Status.Success);
							reports = ConvertUtil.getFromTransactionList(transactions, type);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(reports);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTotalTransactionsByType", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionPagesByType(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							List<PQTransaction> transactionList = transactionApi.listTransactionByPaging(dto);
							List<User> userList = userApi.getAllUsers();
							List<User> filteredList = ConvertUtil.filteredList(userList, dto.getReportType());
							List<PQCommission> commissionList = (List<PQCommission>) pqCommissionRepository.findAll();
							List<TransactionListDTO> resultList = ConvertUtil.convertFromLists(filteredList,transactionList, commissionList);
							Collections.sort(resultList, new TransactionComparator());
							Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
							int start = page.getOffset();
							int end = (start + page.getPageSize()) > resultList.size() ? resultList.size()
									: (start + page.getPageSize());
							List<TransactionListDTO> subList = resultList.subList(start, end);
							Page<TransactionListDTO> resultSet = new PageImpl<TransactionListDTO>(subList, page,
									resultList.size());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transaction List");
							result.setDetails(resultSet);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getUserTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionsOfUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUser(pageable,
									user.getUsername());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("User transaction success.");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/{username}/getTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionsByUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("username") String username, @RequestBody PagingDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUser(pageable, username);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transactions Of User");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transactions Of User");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transactions Of User");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transactions Of User");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionsByUserPost(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUser(pageable,
									dto.getUserStatus());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transactions Of Merchant");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transactions Of Merchant");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transactions Of User");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transactions Of User");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getMerchants", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMerchants(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String type = "STORE";
							List<MerchantDTO> merchantList = merchantApi.getAll(type,null,null);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Merchant List");
							result.setDetails(merchantList);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getServices", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PQService> serviceList = (List<PQService>) pqServiceRepository.findAll();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(serviceList);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetServiceTypes", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getServiceTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		Map<String, Object> detail = new HashMap<String, Object>();
		List<PQServiceType> serviceList = (List<PQServiceType>) pqServiceTypeRepository.findAll();
		List<ServiceTypeDTO> serviceType=ConvertUtil.convertServiceType(serviceList);
		detail.put("serviceType", serviceType);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(serviceType);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetService/{id}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getServiceByTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable(value = "id") long id, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PQService> serviceList = pqServiceRepository.findServiceByServiceTypeID(id);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(ConvertUtil.convertService(serviceList));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAllServices", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PQService> serviceList = (List<PQService>) pqServiceRepository.findAll();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(ConvertUtil.convertService(serviceList));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/GetAllServicesById", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllServiceById(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,@RequestBody ServicesRequest req,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		Map<String, Object> detail = new HashMap<String, Object>();
		List<PQService> serviceList = (List<PQService>) pqServiceRepository.findServiceByServiceTypeID(Long.parseLong(req.getServiceTypeId().trim()));
		List<ServicesDTO> services=ConvertUtil.convertService(serviceList);
		detail.put("services", services);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(services);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/listBanks", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBanks(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<Banks> bankList = (List<Banks>) banksRepository.findAll();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Banks List");
		result.setDetails(bankList);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/listIFSC/{bankCode}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listIfsc(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable(value = "bankCode") String bankCode,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		Banks bank = banksRepository.findByCode(bankCode);
		if(bank!=null){
		List<String> ifscList = bankDetailRepository.getIFSCFromBank(bank);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("IFSC List");
		result.setDetails(ifscList);
		}else{
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("The entered bankCode is wrong.");
			result.setDetails("The entered bankCode is wrong.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
		
	}

	@RequestMapping(value = "/{username}/getMTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getSingleMerchantTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @PathVariable("username") String username,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
						      if(!username.equalsIgnoreCase("enquiries@treatcard.in")){
									User merchantUser = userApi.findByUserName(username);
									PQService service = pgDetailsRepository.findServiceByUser(merchantUser);
									Page<PQTransaction> transList = transactionApi.getTotalTransactionsOfMerchantService(merchantUser.getUsername(),service,pageable);
									List<TransactionListDTO> resultList = new ArrayList<>();
									for(PQTransaction c : transList) {
										TransactionListDTO dt = new TransactionListDTO();	
										User u=userApi.findByAccountDetails(c.getAccount());
										dt=ConvertUtil.convertListFromDetailsMerchantSuccess(merchantUser,c,u);
										resultList.add(dt);
										}
									Page<TransactionListDTO> resultSet = new PageImpl<TransactionListDTO>(resultList, pageable,resultList.size());
									Page<PQTransaction> commissionList =  transactionApi.getCommissionOfMerchantServicePage(service,pageable);
									List<TransactionCommissionListDTO> commissions = new ArrayList<>();
									for(PQTransaction c : commissionList) {
										TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
										commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
										commissions.add(commission);
								      }
									Page<TransactionCommissionListDTO> commisssionSet = new PageImpl<TransactionCommissionListDTO>(commissions, pageable,commissions.size());
									Map<String, Object> detail = new HashMap<String, Object>();
									detail.put("userList", resultSet);
									detail.put("commissionList", commisssionSet);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Ajax | Transaction List");
									result.setDetails(detail);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									}else{
										User merchantUser = userApi.findByUserName(username);
									   PQService service = pqServiceRepository.findServiceByCode("TECD");
									   Page<PQTransaction> transList = transactionApi.getTotalTransactionsOfMerchantService(merchantUser.getUsername(),service,pageable);
									List<TransactionListDTO> resultList = new ArrayList<>();
									for(PQTransaction c : transList) {
										TransactionListDTO dt = new TransactionListDTO();	
										User u=userApi.findByAccountDetails(c.getAccount());
										dt=ConvertUtil.convertListFromDetailsMerchantSuccess(merchantUser,c,u);
										resultList.add(dt);
										}
									Page<TransactionListDTO> resultSet = new PageImpl<TransactionListDTO>(resultList, pageable,resultList.size());
									
									Page<PQTransaction> commissionList =  transactionApi.getCommissionOfMerchantServicePage(service,pageable);
									List<TransactionCommissionListDTO> commissions = new ArrayList<>();
									for(PQTransaction c : commissionList) {
										TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
										commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
										commissions.add(commission);
								      }
									Page<TransactionCommissionListDTO> commisssionSet = new PageImpl<TransactionCommissionListDTO>(commissions, pageable,commissions.size());
									Map<String, Object> detail = new HashMap<String, Object>();
									detail.put("userList", resultSet);
									detail.put("commissionList", commisssionSet);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Ajax | Transaction List");
									result.setDetails(detail);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transactions Of Merchant");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transactions Of Merchant");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transactions Of Merchant");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// transactionId clickable
	@RequestMapping(value = "/{transactionRefNo}/getUserMerchant", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> searchByTranRefNo(@PathVariable(value = "transactionRefNo") String transactionRefNo,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request,
			HttpServletResponse response) {

		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("Admin")) {
			// transactionApi.
		}

		return null;

	}

	// ======================= Test ============================

	@RequestMapping(value = "/test", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> test(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MobileTopupDTO topup, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(topup, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				User u = userApi.findByUserName(topup.getSessionId());
				if (u != null) {
					double amount = transactionApi.getLastSuccessTransaction(u.getAccountDetail());
				} else {
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/findByMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserByMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UserDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		UserResponseDTO userResponse = new UserResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							userResponse = userApi.getUserByMobile(dto.getUsername());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Details of User");
							result.setDetails(userResponse);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(userResponse.getMsg());
						result.setDetails(userResponse);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// clickable=========
	@RequestMapping(value = "/getUserFromTransactionId", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserResponse> getUsersFromTransactionId(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionApiResponse dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		UserResponse resp = new UserResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							// String
							// temp=dto.getTransactionRefNo().substring(0, 13);
							PQTransaction trx = transactionApi.transactionIdClickable(dto.getTransactionRefNo());
							User usr = userApi.findByAccountId(trx.getAccount());
							resp.setDateOfBirth(date.format(usr.getUserDetail().getDateOfBirth()));
							resp.setEmail(usr.getUserDetail().getEmail());
							resp.setContactNo(usr.getUsername());
							resp.setFirstName(usr.getUserDetail().getFirstName());
							resp.setCreated(dateTime.format(usr.getCreated()));
							resp.setAccountType(usr.getAccountDetail().getAccountType().getName());
							resp.setBalance(usr.getAccountDetail().getBalance());
							resp.setAuthority(usr.getAuthority());
							resp.setStatus("Success");
							resp.setCode("S00");
							resp.setMessage("details successfully fetched...");
						} else {
							resp.setStatus("FAILED");
							resp.setMessage("Permission Not Granted");
						}
					}
				} else {
					resp.setStatus("INVALID_SESSION");
					resp.setMessage("Invalid Session");
				}
			} else {
				resp.setStatus("UNAUTHORIZED_USER");
				resp.setMessage("Permission Not Granted");
			}
		} else {
			resp.setStatus("BAD_REQUEST");
			resp.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	
//	for debit transaction
	
	@RequestMapping(value = "/getDebitTransactionsFiltered", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getDebitTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = dto.getFromDate() + " 00:00";
							String endDate = dto.getToDate()+ " 23:59";
							dto.setStartDate(dateFormat.parse(startDate));
							dto.setEndDate(dateFormat.parse(endDate));
	//						String operatorCode = "NIKKI";
						boolean debit = true;

	//						PQService type = pqServiceRepository.findServiceByOperatorCode(operatorCode);
							transactions = transactionApi.listDebitTransaction(debit,dto.getStartDate(),
									dto.getEndDate(),   Status.Success);
							List<NikkiChatResponseDTO> debitList=new ArrayList<>();
							for (PQTransaction pqTransaction : transactions) {
							User u=	userApi.findByAccountDetail(pqTransaction.getAccount());
							NikkiChatResponseDTO res=new NikkiChatResponseDTO();
							res.setContactNo(u.getUsername());
							if(u.getUserDetail()!=null){
								res.setUsername(u.getUserDetail().getFirstName());
							}
							res.setAuthority(u.getAuthority());
							res.setAmount(String.valueOf(pqTransaction.getAmount()));
							res.setDescription(pqTransaction.getDescription());
							res.setTransactionRefNo(pqTransaction.getTransactionRefNo());
							res.setStatus(pqTransaction.getStatus().getValue());
		//					res.setService(type.getServiceName());
							String d1=dateFormat.format(pqTransaction.getCreated());
							res.setDate(d1);
							debitList.add(res);
							}
							//List<User> us=userApi.listUser();
							//reports = ConvertUtil.getTransactionList(transactions, us );
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(debitList);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	
//	for credit transaction 
	
	
	
	
	@RequestMapping(value = "/getCreditTransactionsFiltered", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getCreditTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = dto.getFromDate() + " 00:00";
							String endDate = dto.getToDate()+ " 23:59";
							dto.setStartDate(dateFormat.parse(startDate));
							dto.setEndDate(dateFormat.parse(endDate));
	//						String operatorCode = "NIKKI";
						boolean debit = false;
      //                  int transactionType=0 ;
	//						PQService type = pqServiceRepository.findServiceByOperatorCode(operatorCode);
							transactions = transactionApi.listCreditTransaction(debit,dto.getStartDate(),
									dto.getEndDate(),   Status.Success);
							List<NikkiChatResponseDTO> creditList=new ArrayList<>();
							for (PQTransaction pqTransaction : transactions) {
							User u=	userApi.findByAccountDetail(pqTransaction.getAccount());
							NikkiChatResponseDTO res=new NikkiChatResponseDTO();
							res.setContactNo(u.getUsername());
							if(u.getUserDetail()!=null){
								res.setUsername(u.getUserDetail().getFirstName());
							}
							res.setAuthority(u.getAuthority());
							res.setAmount(String.valueOf(pqTransaction.getAmount()));
							res.setDescription(pqTransaction.getDescription());
							res.setTransactionRefNo(pqTransaction.getTransactionRefNo());
							res.setStatus(pqTransaction.getStatus().getValue());
		//					res.setService(type.getServiceName());
							String d1=dateFormat.format(pqTransaction.getCreated());
							res.setDate(d1);
							creditList.add(res);
							}
							//List<User> us=userApi.listUser();
							//reports = ConvertUtil.getTransactionList(transactions, us );
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(creditList);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
//	for nikki chat
	
	@RequestMapping(value = "/getNikkiTransactionsFiltered", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getNikkiTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = dto.getFromDate() + " 00:00";
							String endDate = dto.getToDate()+ " 23:59";
							dto.setStartDate(dateFormat.parse(startDate));
							dto.setEndDate(dateFormat.parse(endDate));
							String operatorCode = "NIKKI";
						boolean debit = true;

							PQService type = pqServiceRepository.findServiceByOperatorCode(operatorCode);
							transactions = transactionApi.listTransactionByDate(debit,dto.getStartDate(),
									dto.getEndDate(),  type, Status.Success);
							List<NikkiChatResponseDTO> nikkiList=new ArrayList<>();
							for (PQTransaction pqTransaction : transactions) {
							User u=	userApi.findByAccountDetail(pqTransaction.getAccount());
							NikkiChatResponseDTO res=new NikkiChatResponseDTO();
							res.setContactNo(u.getUsername());
							if(u.getUserDetail()!=null){
								res.setUsername(u.getUserDetail().getFirstName());
							}
							res.setAuthority(u.getAuthority());
							res.setAmount(String.valueOf(pqTransaction.getAmount()));
							res.setDescription(pqTransaction.getDescription());
							res.setTransactionRefNo(pqTransaction.getTransactionRefNo());
							res.setStatus(pqTransaction.getStatus().getValue());
							res.setService(type.getServiceName());
							String d1=dateFormat.format(pqTransaction.getCreated());
							res.setDate(d1);
							nikkiList.add(res);
							}
							//List<User> us=userApi.listUser();
							//reports = ConvertUtil.getTransactionList(transactions, us );
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(nikkiList);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	
	
//	for imagica transaction
	
	
	
	@RequestMapping(value = "/getImagicaTransactionsFiltered", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getImagicaTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = dto.getFromDate() + " 00:00";
							String endDate = dto.getToDate()+ " 23:59";
							dto.setStartDate(dateFormat.parse(startDate));
							dto.setEndDate(dateFormat.parse(endDate));
							String operatorCode = "IMGCA";
						boolean debit = true;
//							switch (reportType.toUpperCase()) {
//							case "TOPUPBILL":
//								serviceType = "Bill Payment";
//								debit = false;
//								break;
//							case "LOADMONEY":
//								serviceType = "Load Money";
//								debit = false;
//								break;
//							case "GCI":
//								serviceType = "GiftCart Booking";
//								debit = false;
//								break;
//							default:
//								serviceType = "Load Money";
//								debit = false;
//								break;
//							}
							PQService type = pqServiceRepository.findServiceByOperatorCode(operatorCode);
							transactions = transactionApi.listTransactionByDate(debit,dto.getStartDate(),
									dto.getEndDate(),  type, Status.Success);
							List<NikkiChatResponseDTO> nikkiList=new ArrayList<>();
							for (PQTransaction pqTransaction : transactions) {
							User u=	userApi.findByAccountDetail(pqTransaction.getAccount());
							NikkiChatResponseDTO res=new NikkiChatResponseDTO();
							res.setContactNo(u.getUsername());
							if(u.getUserDetail()!=null){
								res.setUsername(u.getUserDetail().getFirstName());
							}
							res.setAuthority(u.getAuthority());
							res.setAmount(String.valueOf(pqTransaction.getAmount()));
							res.setDescription(pqTransaction.getDescription());
							res.setTransactionRefNo(pqTransaction.getTransactionRefNo());
							res.setStatus(pqTransaction.getStatus().getValue());
							res.setService(type.getServiceName());
							String d1=dateFormat.format(pqTransaction.getCreated());
							res.setDate(d1);
							nikkiList.add(res);
							}
							//List<User> us=userApi.listUser();
							//reports = ConvertUtil.getTransactionList(transactions, us );
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(nikkiList);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/getTreatCardTrasnaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTreatCardTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode("TECD");
						List<TransactionListDTO> transactions = transactionApi.getTreatCardTransaction(service);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("TreatCard List");
							result.setDetails(transactions);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getWalletBalance", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getWalletBalance(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR) && user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<WalletOutstandingDTO> wBal = userApi.getWalletOutsReport(pageable, dto.getStartDate(), dto.getEndDate());
							System.out.println("wBal="+wBal.getSize());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | User List");
							result.setDetails(wBal);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getWalletBalanceByDate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getWalletBalanceByDate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR) && user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<WalletOutstandingDTO> wBal = userApi.getWalletOutsReportByDate(pageable, dto.getStartDate(), dto.getEndDate());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | User List");
							result.setDetails(wBal);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getRefundIpayList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getRefundIpayList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							List<PQTransaction> transactionList = transactionApi.iPayRefundedTxnList(dto);
							List<User> userList = userApi.getAllUsers();
							List<User> filteredList = ConvertUtil.filteredList(userList,dto.getReportType());
							List<PQCommission> commissionList = (List<PQCommission>) pqCommissionRepository.findAll();
							List<TransactionListDTO> resultList = ConvertUtil.convertFromLists(filteredList,transactionList,commissionList);
							Collections.sort(resultList,new TransactionComparator());
							Pageable page = new PageRequest(dto.getPage(),dto.getSize(),sort);
							int start = page.getOffset();
							int end = (start + page.getPageSize()) > resultList.size() ? resultList.size() : (start + page.getPageSize());
							List<TransactionListDTO> subList = resultList.subList(start,end);
							Page<TransactionListDTO> resultSet = new PageImpl<TransactionListDTO>(subList,page,resultList.size());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transaction List");
							result.setDetails(resultSet);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/getUpiTxn", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUpiTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Page<PQTransaction> pg=null;
							PQService upiService=pqServiceRepository.findServiceByCode("LMU");
							try {
								if(dto.isPageable()){
									Sort sort = new Sort(Sort.Direction.DESC, "id");
									Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
									pg = transactionApi.getUPITxn(pageable,upiService);
									result.setCount(pg.getTotalPages());
									result.setDetails(getTransactionList(pg.getContent()));
								}else{
									List<PQTransaction> list=transactionApi.getUPITxnList(upiService,
											dateTime.parse(dto.getFromDate()+CommonUtil.startTime),
											dateTime.parse(dto.getToDate()+CommonUtil.endTime));
									result.setDetails(getTransactionList(list));
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | User List");
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	private List<TListDTO> getTransactionList(List<PQTransaction> content) {
		List<TListDTO> listDto=new ArrayList<>();
		for (PQTransaction pqTxn : content) {
			try {
				User user=transactionApi.getAccountByAuthority(pqTxn.getAccount());
				TListDTO dto = new TListDTO();
				dto.setAmount(String.format("%.2f", pqTxn.getAmount()));
				dto.setContactNo(user.getUsername());
				dto.setCurrentBalance(String.format("%.2f", pqTxn.getCurrentBalance()));
				dto.setEmail((user.getUserDetail().getEmail() != null) ? user.getUserDetail().getEmail() : "");
				dto.setUsername((user.getUserDetail().getFirstName() != null) ? user.getUserDetail().getFirstName() : "");
				dto.setTransactionRefNo(pqTxn.getTransactionRefNo());
				dto.setDateOfTransaction(dateTime.format(pqTxn.getCreated()));
				dto.setDebit(String.valueOf(pqTxn.isDebit()));
				dto.setStatus(pqTxn.getStatus().getValue());
				dto.setDescription(pqTxn.getDescription());
				listDto.add(dto);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return  listDto;
	}

	@RequestMapping(value = "/gcmList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getGcmList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			Page<SendNotification> pg = null;
			try {
				Sort sort = new Sort(Sort.Direction.DESC, "id");
				Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
				pg = userApi.getGcmList(pageable);
				result.setCount(pg.getTotalPages());
				result.setDetails(getGcmDetails(pg.getContent()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Ajax | User List");
		} 
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	private List<SendNotificationDTO> getGcmDetails(List<SendNotification> content) {
		List<SendNotificationDTO> gcmList = new ArrayList<>();
		if (content != null && content.size() > 0) {
			for (SendNotification sendNotification : content) {
				SendNotificationDTO dto = new SendNotificationDTO();
				dto.setDeliveredCount(sendNotification.getDeliveredCount());
				dto.setMessage(sendNotification.getMessage());
				dto.setTitle(sendNotification.getTitle());
				dto.setImage(sendNotification.getImage());
				dto.setStatus(sendNotification.getStatus().toString());
				dto.setCreatedDate(sendNotification.getCreated().toString());
				gcmList.add(dto);
			}
		}
		return gcmList;
	}
}
