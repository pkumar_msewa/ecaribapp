package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.payqwikapp.entity.*;
import com.payqwikapp.model.*;
import com.payqwikapp.model.admin.*;
import com.payqwikapp.model.error.KycLimitError;
import com.payqwikapp.repositories.*;
import com.payqwikapp.validation.AdminAuthValidation;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.model.request.ServicesRequest;
import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.api.IAdminApi;
import com.payqwikapp.api.IDataConfigApi;
import com.payqwikapp.api.IEmailLogApi;
import com.payqwikapp.api.IMerchantApi;
import com.payqwikapp.api.IMessageLogApi;
import com.payqwikapp.api.IPromoCodeApi;
import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.TransactionComparator;
import com.payqwikapp.validation.PromoCodeValidation;
import com.payqwikapp.validation.RegisterValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/SuperAdmin")
public class SuperAdminController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
	private final SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final IUserApi userApi;
	private final IMerchantApi merchantApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ISessionApi sessionApi;
	private final ITransactionApi transactionApi;
	private final IMessageLogApi messageLogApi;
	private final IEmailLogApi emailLogApi;
	private final RegisterValidation registerValidation;
	private final IPromoCodeApi promoCodeApi;
	private final PromoCodeValidation promoCodeValidation;
	private final IAdminApi adminApi;
	private final ISendMoneyApi sendMoneyApi;
	private final PQServiceRepository pqServiceRepository;
	private final BanksRepository banksRepository;
	private final BankDetailRepository bankDetailRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final PQCommissionRepository pqCommissionRepository;
	private final AdminAuthValidation adminAuthValidation;
	private final AccessLogRepository accessLogRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final IDataConfigApi dataConfigApi;
	private final PQOperatorRepository pqOperatorRepository;

	public SuperAdminController(IUserApi userApi, IMerchantApi merchantApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, ISessionApi sessionApi, ITransactionApi transactionApi,
			IMessageLogApi messageLogApi, IEmailLogApi emailLogApi, RegisterValidation registerValidation,
			IPromoCodeApi promoCodeApi, PromoCodeValidation promoCodeValidation, IAdminApi adminApi,
			ISendMoneyApi sendMoneyApi, PQServiceRepository pqServiceRepository, BanksRepository banksRepository,
			BankDetailRepository bankDetailRepository, PQServiceTypeRepository pqServiceTypeRepository,
			PQCommissionRepository pqCommissionRepository, AdminAuthValidation adminAuthValidation,
			AccessLogRepository accessLogRepository, PGDetailsRepository pgDetailsRepository,IDataConfigApi dataConfigApi,PQOperatorRepository pqOperatorRepository) {
		this.userApi = userApi;
		this.merchantApi = merchantApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.sessionApi = sessionApi;
		this.transactionApi = transactionApi;
		this.messageLogApi = messageLogApi;
		this.emailLogApi = emailLogApi;
		this.registerValidation = registerValidation;
		this.promoCodeApi = promoCodeApi;
		this.promoCodeValidation = promoCodeValidation;
		this.adminApi = adminApi;
		this.sendMoneyApi = sendMoneyApi;
		this.pqServiceRepository = pqServiceRepository;
		this.banksRepository = banksRepository;
		this.bankDetailRepository = bankDetailRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
		this.pqCommissionRepository = pqCommissionRepository;
		this.adminAuthValidation = adminAuthValidation;
		this.accessLogRepository = accessLogRepository;
		this.pgDetailsRepository = pgDetailsRepository;
		this.dataConfigApi = dataConfigApi;
		this.pqOperatorRepository=pqOperatorRepository;
	}

	@RequestMapping(value = "/GetReports", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getReports(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String reportType = dto.getReportType();
							List<TransactionReport> reports = null;
							List<PQTransaction> transactions = new ArrayList<>();
							String startDate = date.format(dto.getStartDate()) + " 00:00:00";
							String endDate = date.format(dto.getEndDate()) + " 23:59:59";
							dto.setStartDate(dateTime.parse(startDate));
							dto.setEndDate(dateTime.parse(endDate));
							String serviceType = "";
							boolean debit = false;
							switch (reportType.toUpperCase()) {
							case "TOPUPBILL":
								serviceType = "Bill Payment";
								debit = false;
								break;
							case "ELECTRICITY":
								serviceType = "Electricity Bill Payment";
								debit = false;
								break;
							case "LOADMONEY":
								serviceType = "Load Money";
								debit = false;
								break;
							default:
								serviceType = "Load Money";
								debit = false;
								break;
							}
							PQServiceType type = pqServiceTypeRepository.findServiceTypeByName(serviceType);
							transactions = transactionApi.listTransactionByServiceAndDate(dto.getStartDate(),
									dto.getEndDate(), type, debit, TransactionType.DEFAULT, Status.Success);
							reports = ConvertUtil.getFromTransactionList(transactions, type);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(reportType + " List");
							result.setDetails(reports);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetPossibilities", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> searchByMobileLike(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SearchDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("List of Possibilities");
							result.setDetails(userApi.getAllPossibilites(dto.getMobileSubString()));
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Not a valid user");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAccessUsers", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAccessUsers(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("List of Possibilities");
							result.setDetails(userApi.getAllAccessUsers());
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Not a valid user");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SendSMS", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendSingleSMS(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MessageDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
							if (accessDetails != null) {
								if (accessDetails.isSendSMS()) {
									userApi.sendSingleSMS(dto.getMobile(), dto.getContent());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Message Sent Successfully");
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Access Denied");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access not present");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Not a valid user");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	// API to send Single Mail
	@RequestMapping(value = "/SendMail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendSingleMail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MailDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
							if (accessDetails != null) {
								if (accessDetails.isSendMail()) {
									userApi.sendSingleMail(dto.getDestination(), dto.getContent(), dto.getSubject());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Mail Sent Successfully");
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Access Denied");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Not Available");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Not a valid user");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SendBulkSMS", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendBulkSMS(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MessageDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
							if (accessDetails != null) {
								if (accessDetails.isSendSMS()) {
									userApi.sendBulkSMS(dto.getContent());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Message Sent Successfully");
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Access Denied");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Not Available");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Not a valid user");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	// API to send BULK Mail

	@RequestMapping(value = "/SendBulkMail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendBulkMail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MailDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
							if (accessDetails != null) {
								if (accessDetails.isSendMail()) {
									userApi.sendBulkMail(dto.getContent(), dto.getSubject());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Mail Sent Successfully");
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Access Denied");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Not Available");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Not a valid user");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateAuth/All", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllAuthUpdateLogs(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.getAllAuthUpdateLogs();
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Report/{reportType}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionReport(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable(value = "reportType") String report, @RequestBody GetTransactionDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (role.equalsIgnoreCase("SuperAdmin")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					if (report.equalsIgnoreCase("Transaction")) {
						List<PQTransaction> transactions = null;
						String username = dto.getUsername();
						if (username != null && username.length() != 0) {
							User userTransaction = userApi.findByUserName(dto.getUsername());
							transactions = transactionApi.getDailyTransactionBetweenForAccount(dto.getStart(),
									dto.getEnd(), userTransaction.getAccountDetail());
							result.setMessage("User transaction report.");
						} else {
							transactions = transactionApi.getDailyTransactionBetweeen(dto.getStart(), dto.getEnd());
							result.setMessage("Transaction successful.");
						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(transactions);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else if (report.equalsIgnoreCase("Email")) {
						List<EmailLog> emailLogs = null;
						emailLogs = emailLogApi.getDailyEmailLogBetweeen(dto.getStart(), dto.getEnd());
						result.setMessage("Email Log successful.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(emailLogs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else if (report.equalsIgnoreCase("SMS")) {
						List<MessageLog> messageLogs = null;
						messageLogs = messageLogApi.getDailyMessageLogBetweeen(dto.getStart(), dto.getEnd());
						result.setMessage("Message log successful.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(messageLogs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized user.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Session invalid.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		}
		result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
		result.setMessage("Unauthorized user.");
		result.setDetails(null);
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetValues", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getValues(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("values");
							result.setDetails(userApi.getAdminLoginValues());
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} catch (Exception e) {
							e.printStackTrace();
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed, Please try again later.");
							result.setDetails("Failed, Please try again later.");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ChangePassword", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> changePassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						ChangePasswordError error = new ChangePasswordError();
						dto.setUsername(user.getUsername());
						error = registerValidation.validateAdminPassword(dto);
						if (error.isValid()) {
							try {
								result = userApi.changeAdminPassword(dto);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} catch (Exception e) {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Change Password");
								result.setDetails(e.getMessage());
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("Change Password");
							result.setDetails(error);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Change Password");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Change Password");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Change Password");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get email and sms logs last 1000

	@RequestMapping(method = RequestMethod.POST, value = "/GetLogs/Now/{logType}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getLogsNow(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("logType") String logType, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<?> list = userApi.getSMSOrEmailLogs(logType);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Logs List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Logs List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get SMS and Email Logs between Dates

	@RequestMapping(method = RequestMethod.POST, value = "/GetLogs/Filter/{logType}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getLogsNow(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("logType") String logType, @RequestBody ReportDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date startDate = dateFormat.parse(dto.getStartDate());
						Date endDate = dateFormat.parse(dto.getEndDate());
						List<?> list = userApi.getSMSOrEmailLogsByFilter(startDate, endDate, logType);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Logs List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Logs List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to GET ALL Services
	@RequestMapping(method = RequestMethod.POST, value = "/GetServices", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllServices(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<ServiceListDTO> list = userApi.getAllServicesWithCommission();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to GET ALL Services
	@RequestMapping(method = RequestMethod.POST, value = "/UpdateService", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpdateServiceDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.updateServiceDetails(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Today's Users
	@RequestMapping(method = RequestMethod.POST, value = "/GetUsers/Now/{userType}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserNow(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("userType") String userType, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<UserListDTO> list = userApi.getTodayUsers(userType);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Users in Between Dates
	@RequestMapping(method = RequestMethod.POST, value = "/GetUsers/Filter/{userType}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserBetween(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("userType") String userType, @RequestBody ReportDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date startDate = dateFormat.parse(dto.getStartDate());
						Date endDate = dateFormat.parse(dto.getEndDate());
						List<UserListDTO> list = userApi.getUsersBetween(startDate, endDate, userType);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Change Password");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Change Password");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Today's Transaction
	@RequestMapping(method = RequestMethod.POST, value = "/GetTransaction/Now", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTodaysTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TListDTO> listDTOs = userApi.getTodaysTransactions();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Transaction in Between Dates
	@RequestMapping(method = RequestMethod.POST, value = "/GetTransaction/Filter", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionBetween(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date endDate = dateFormat.parse(dto.getEndDate());
						Date startDate = dateFormat.parse(dto.getStartDate());
						List<TListDTO> listDTOs = userApi.getTransactionsBetween(startDate, endDate);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Transaction in Between Dates with service
	@RequestMapping(method = RequestMethod.POST, value = "/GetTransaction/FilterByService", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionBetweenWithService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date endDate = dateFormat.parse(dto.getEndDate());
						Date startDate = dateFormat.parse(dto.getStartDate());
						String serviceType = "";
						switch (dto.getType().toUpperCase()) {

						case "TOPBILLPAY":
							serviceType = "Bill Payment";
							break;
						case "LOADMONEY":
							serviceType = "Load Money";
							break;
						case "SENDMONEY":
							serviceType = "Fund Transfer";
							break;
						case "SENDMONEYBANK":
							serviceType = "Fund Transfer Bank";
							break;
						case "PROMOCODE":
							serviceType = "Promotional Activity";
							break;
						default:
							serviceType = "Merchant Payment";
							break;
						}
						List<TransactionListDTO> listDTOs = userApi.getTransactionsBetweenForServiceType(startDate,
								endDate, serviceType);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Transaction in Between Dates with service without usernames
	@RequestMapping(method = RequestMethod.POST, value = "/GetTransaction/FFilterByService", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionBetweenWithServiceWithoutNames(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date endDate = dateFormat.parse(dto.getEndDate());
						Date startDate = dateFormat.parse(dto.getStartDate());
						String serviceType = "";
						switch (dto.getType().toUpperCase()) {
						case "TOPBILLPAY":
							serviceType = "Bill Payment";
							break;
						case "LOADMONEY":
							serviceType = "Load Money";
							break;
						case "SENDMONEY":
							serviceType = "Fund Transfer";
							break;
						case "SENDMONEYBANK":
							serviceType = "Fund Transfer Bank";
							break;
						case "PROMOCODE":
							serviceType = "Promotional Activity";
							break;
						default:
							serviceType = "Merchant Payment";
							break;
						}
						List<TransactionListDTO> listDTOs = userApi.getTransactionsBetweenForServiceType(startDate,
								endDate, serviceType);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get GCM id of active users
	@RequestMapping(method = RequestMethod.POST, value = "/GetGCMIDs", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getGCMIDs(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("superAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if (accessDetails != null) {
							if (accessDetails.isSendGCM()) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								Pageable gcmPage = new PageRequest(dto.getPage(), dto.getSize());
								Page<String> gcmList = userApi.getGCMIDs(gcmPage);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("GCM IDs");
								result.setDetails(gcmList);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Change Password");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Merchant/Save", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MRegisterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String email = dto.getEmail();
						User merchant = userApi.findByUserName(email);
						if (merchant == null) {
							PGDetails pgDetails = userApi.findMerchantByMobile(dto.getContactNo());
							if (pgDetails == null) {
								result = merchantApi.addMerchant(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Merchant added successfully");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Mobile already exists");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Merchant Already exists with this EMAIL ID");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/MBankTransferList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> merchantBankTransferRequests(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MBankTransfer> mBankTransfers = adminApi.getAllMBankTransferReports();
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("mbank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (MBankTransfer bt : mBankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
							bdto.setName(bt.getSender().getUserDetail().getFirstName());
							bdto.setEmail(bt.getSender().getUserDetail().getEmail());
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount("" + bt.getAmount());
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
							if (temp != null) {
								bdto.setStatus(temp.getStatus().getValue());
								bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
							}
							bankTransferDTOs.add(bdto);
						}
						result.setMessage("Merchant NEFT Transaction list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetAccessList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAccessList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<AccessDetails> accessEntityList = (List<AccessDetails>) accessLogRepository.findAll();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Access List");
						result.setDetails(ConvertUtil.convertFromList(accessEntityList));
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/BankTransferList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankTransferRequests(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<BankTransfer> bankTransfers = adminApi.getAllTransferReportsLimit();
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("bank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (BankTransfer bt : bankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
							bdto.setName(bt.getSender().getUserDetail().getFirstName());
							bdto.setEmail(bt.getSender().getUserDetail().getEmail());
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount(String.format("%.2f", bt.getAmount()));
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							bankTransferDTOs.add(bdto);
						}
						result.setMessage("Bank Transfer list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/BankTransferListFilter", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankTransferRequestsByDate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<BankTransfer> bankTransfers = adminApi.getAllTransferReportsByDate(
								dateFormat.parse(dto.getStartDate()), dateFormat.parse(dto.getEndDate()));
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("bank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (BankTransfer bt : bankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
							bdto.setName(bt.getSender().getUserDetail().getFirstName());
							bdto.setEmail(bt.getSender().getUserDetail().getEmail());
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount("" + bt.getAmount());
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
							if (temp != null) {
								bdto.setStatus(temp.getStatus().getValue());
								bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
							}
							bankTransferDTOs.add(bdto);
						}
						result.setMessage("Bank Transfer list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/CheckExistingVisaMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> checkExistingVisaMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		System.err.println("Inside Visa Merchant Save");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String email = dto.getEmailAddress();
						User merchant = userApi.findByUserName(email);
						VisaMerchant visaMerchant = merchantApi.findByEmail(email);
						System.err.println("Visa merchant " + visaMerchant);
						if (visaMerchant == null) {
							if (merchant != null) {
								System.err.println("merchant Authority " + merchant.getAuthority());
								if (merchant.getAuthority().contains(Authorities.MERCHANT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Merchant exists");
									result.setDetails("Merchant exists");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Merchant doesn't exists with this EMAIL ID");
									result.setDetails("Merchant doesn't exists with this EMAIL ID");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Merchant doesn't exists with this EMAIL ID");
								result.setDetails("Merchant doesn't exists with this EMAIL ID");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Merchant already exists with this EMAIL ID");
							result.setDetails("Merchant already exists with this EMAIL ID");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/VisaMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveVisaMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String email = dto.getEmailAddress();
						User merchant = userApi.findByUserName(email);
						// if(merchant == null) {
						result = merchantApi.addVisaMerchant(dto, merchant);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						/*
						 * }else { result.setStatus(ResponseStatus.FAILURE);
						 * result. setMessage(
						 * "Merchant Already exists with this EMAIL ID" );
						 * return new ResponseEntity<ResponseDTO>(result,
						 * HttpStatus.OK); }
						 */
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getTotalTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactions(pageable);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transaction List");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTotalTransactionsByType", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionPagesByType(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							List<PQTransaction> transactionList = transactionApi.listTransactionByPaging(dto);
							List<User> userList = userApi.getAllUsers();
							List<User> filteredList = ConvertUtil.filteredList(userList, dto.getReportType());
							List<PQCommission> commissionList = (List<PQCommission>) pqCommissionRepository.findAll();
							List<TransactionListDTO> resultList = ConvertUtil.convertFromLists(filteredList,
									transactionList, commissionList);
							Collections.sort(resultList, new TransactionComparator());
							Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
							int start = page.getOffset();
							int end = (start + page.getPageSize()) > resultList.size() ? resultList.size()
									: (start + page.getPageSize());
							List<TransactionListDTO> subList = resultList.subList(start, end);
							Page<TransactionListDTO> resultSet = new PageImpl<TransactionListDTO>(subList, page,
									resultList.size());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transaction List");
							result.setDetails(resultSet);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transaction List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transaction List");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getUserTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionsOfUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUser(pageable,
									user.getUsername());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("User transaction success.");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionsByUserPost(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUser(pageable,
									dto.getUserStatus());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transactions Of Merchant");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transactions Of Merchant");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transactions Of User");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | Transactions Of User");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAllServices", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PQService> serviceList = (List<PQService>) pqServiceRepository.findAll();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(ConvertUtil.convertService(serviceList));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetServiceTypes", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getServiceTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PQServiceType> serviceList = (List<PQServiceType>) pqServiceTypeRepository.findAllServiceType();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(ConvertUtil.convertServiceType(serviceList));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Merchant/All", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String languagemys,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String type = "ALL";
						List<MerchantDTO> merchants = merchantApi.getAll(type, null, null);
						result.setMessage("Merchant List");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(merchants);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/LMTransactions", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getLoadMoneyTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<PQTransaction> transactionList = transactionApi.getLoadMoneyTransactions(Status.Success);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction List");
						result.setDetails(transactionList);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Un-Authorized User");
						result.setDetails("Un-Authorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/PCTransactions", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getRedeemTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MTransactionResponseDTO> listDTOs = userApi.getPromoCodeTransactions();
						/*
						 * PQService service =
						 * pqServiceRepository.findServiceByCode("PPS");
						 * List<PQTransaction> transactionList =
						 * transactionApi.transactionListByServiceAndDebit(
						 * service,false); List<User> userList =
						 * userApi.getAllUsers(); List<MTransactionResponseDTO>
						 * minimizedList =
						 * ConvertUtil.getMerchantTransactions(transactionList,
						 * userList); Page<MTransactionResponseDTO>
						 * promoCodeTransactions =
						 * ConvertUtil.convertFromList(minimizedList,dto);
						 */
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction List");
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Un-Authorized User");
						result.setDetails("Un-Authorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/PCTransactionsFiltered", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getFilteredTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MTransactionResponseDTO> listDTOs = userApi
								.getPromoCodeTransactionsFiltered(dateFormat.parse(from), dateFormat.parse(to));
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction List");
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Un-Authorized User");
						result.setDetails("Un-Authorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/GetServiceStatus", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> GetFPSwitchStaus(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ServiceStatusDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO resp = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				if (role.equalsIgnoreCase("SuperAdmin")) {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							List<ServiceStatusDTO> serviceStatus = userApi.findAll();
							resp.setStatus(ResponseStatus.SUCCESS);
							resp.setMessage("service Status Detail");
							resp.setDetails(serviceStatus);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						} else {
							resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							resp.setMessage("Unauthorized user.");
							resp.setDetails(null);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					} else {
						resp.setStatus(ResponseStatus.INVALID_SESSION);
						resp.setMessage("Session invalid.");
						resp.setDetails(null);
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				} else {
					resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					resp.setMessage("Unauthorized user.");
					resp.setDetails(null);
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			} else {
				resp.setStatus(ResponseStatus.BAD_REQUEST);
				resp.setMessage("Unable to process request");
				resp.setDetails(null);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setMessage("Please try again later, internal server error");
			resp.setDetails(null);
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ServiceSwitchActive", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> fpSwtichActive(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ServiceStatusDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO resp = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				if (role.equalsIgnoreCase("SuperAdmin")) {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							ServiceStatus serviceStatus = userApi.findByName(dto.getName());
							serviceStatus = userApi.setServiceStatusActive(serviceStatus, dto.getName());
							List<ServiceStatusDTO> status = userApi.findAll();
							resp.setMessage("Service Status Set to Active");
							resp.setStatus(ResponseStatus.SUCCESS);
							resp.setDetails(status);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						} else {
							resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							resp.setMessage("Unauthorized user.");
							resp.setDetails(null);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					} else {
						resp.setStatus(ResponseStatus.INVALID_SESSION);
						resp.setMessage("Session invalid.");
						resp.setDetails(null);
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				} else {
					resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					resp.setMessage("Unauthorized user.");
					resp.setDetails(null);
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			} else {
				resp.setStatus(ResponseStatus.BAD_REQUEST);
				resp.setMessage("Unable to process request");
				resp.setDetails(null);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setMessage("Please try again later, internal server error");
			resp.setDetails(null);
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ServiceSwitchInActive", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> fpSwtichInactive(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ServiceStatusDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO resp = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				if (role.equalsIgnoreCase("SuperAdmin")) {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							if (verifyServiceStatus(dto.getName())) {
								List<ServiceStatusDTO> status = userApi.findAll();
								resp.setMessage("Service Status Set to Inactive");
								resp.setStatus(ResponseStatus.SUCCESS);
								resp.setDetails(status);
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							} else {
								resp.setStatus(ResponseStatus.BAD_REQUEST);
								resp.setMessage("Activate Mobile");
								resp.setDetails("Invalid Activation Key");
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							}
						} else {
							resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							resp.setMessage("Unauthorized user.");
							resp.setDetails(null);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					} else {
						resp.setStatus(ResponseStatus.INVALID_SESSION);
						resp.setMessage("Session invalid.");
						resp.setDetails(null);
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				} else {
					resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					resp.setMessage("Unauthorized user.");
					resp.setDetails(null);
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			} else {
				resp.setStatus(ResponseStatus.BAD_REQUEST);
				resp.setMessage("Unable to process request");
				resp.setDetails(null);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setMessage("Please try again later, internal server error");
			resp.setDetails(null);
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PromoCode/Save", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> savePromoCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PromoCodeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						PromoCode isCodeExist = promoCodeApi.checkPromoCodeValid(dto.getPromoCode(),null);
						if (isCodeExist == null) {
							boolean isValidCode = promoCodeApi.checkPromoCodeLength(dto.getPromoCode());
							if (isValidCode) {
								promoCodeApi.addPromocode(dto);
								result.setMessage("PromoCode added successfully.");
								result.setStatus(ResponseStatus.SUCCESS);
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
							result.setMessage("Please Enter 6 digit Promo Code");
							result.setStatus(ResponseStatus.FAILURE);
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
						result.setMessage("Promo Code Already Exist");
						result.setStatus(ResponseStatus.FAILURE);
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails("Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PromoCode/List", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listPromoCodes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PromoCodeDTO> list = new ArrayList<PromoCodeDTO>();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						list = promoCodeApi.getAll();
						result.setMessage("PromoCode list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/ListAccountTypes", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listAccountTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Account Type List");
						result.setDetails(userApi.getAllAccountTypes());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user. Not a Superadmin authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Unauthorized user. Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Unable to process request");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateAccountType", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listAccountTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody KycLimitDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						KycLimitError error = adminAuthValidation.validateAccountLimits(dto);
						if (error.isValid()) {
							result = userApi.updateAccountType(dto);
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("invalid parameters");
							result.setDetails(error);
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetUserInfo", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserInformation(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UserInfoRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.getAllUserInfo(dto.getUsername());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetAllLogs", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getLogs(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String reportType = dto.getReportType();
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable pageRequest = new PageRequest(dto.getPage(), dto.getSize(), sort);
						switch (reportType.toUpperCase()) {
						case "EMAIL":
							result.setDetails(emailLogApi.getAllEmailLogs(pageRequest));
							break;
						case "SMS":
							result.setDetails(messageLogApi.getAllSMSLogs(pageRequest));
							break;
						default:

						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(reportType + " Logs");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BlockUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> blockUserImmediately(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AuthUpdateRequest dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User blockUser = userApi.findByUserName(dto.getUsername());
						userApi.blockUser(blockUser.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User Locked");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UnblockUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> unblockUserImmediately(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AuthUpdateRequest dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User currentUser = userApi.findByUserName(dto.getUsername());
						if (currentUser != null) {
							if (currentUser.getUserType().equals(UserType.User)) {
								String authority = Authorities.USER + "," + Authorities.AUTHENTICATED;
								userApi.updateUserAuthority(authority, currentUser.getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("User Unlocked");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("You can't unlock " + currentUser.getUserType() + " Type User");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("User not found");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetKycUpdate", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getKycUpdate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ApproveKycDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.kycUpdate(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetNonKycUpdate", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getNonKycUpdate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ApproveKycDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.nonKycUpdate(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateAccess", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateAccess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AccessDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						// TODO update access of user corresponding to username
						result = userApi.updateAccess(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Transactions/ByService", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> filterTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TFilterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (dto.getStartDate() != null || dto.getEndDate() != null) {
							Date startDate = dateFormat.parse(dto.getStartDate());
							Date endDate = dateFormat.parse(dto.getEndDate());
							if (dto.getServiceType() != null) {
								PQServiceType serviceType = pqServiceTypeRepository.findServiceTypeByName(dto.getServiceType());
								if (serviceType != null) {
									 if(serviceType.getName().equalsIgnoreCase("Load Money")){
									List<PQService> serviceList = pqServiceRepository.findByServiceType(serviceType);
									List<PQTransaction> tlist = transactionApi.filterListByTypeAndStatus(serviceList,
											startDate, endDate, dto.getStatus());
									List<TListDTO> resultList = userApi.convertFromRawTransactions(tlist);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage(String.valueOf(resultList.size()));
									result.setDetails(resultList);
								  }else if(serviceType.getName().equalsIgnoreCase("Fund Transfer Bank")){
									  List<PQService> serviceList = pqServiceRepository.findByServiceType(serviceType);
										List<PQTransaction> tlist = transactionApi.filterListByTypeAndStatusDebit(serviceList,
												startDate, endDate, dto.getStatus());
										List<TListDTO> resultList = userApi.convertFromRawTransactions(tlist);
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(String.valueOf(resultList.size()));
										result.setDetails(resultList); 
								  }
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Service Type not found");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Enter Service Type");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Enter start date and end date");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Transactions/BillPayment", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> filterTransactionOfBillPayment(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TFilterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (dto.getStartDate() != null || dto.getEndDate() != null) {
							Date startDate = dateFormat.parse(dto.getStartDate());
							Date endDate = dateFormat.parse(dto.getEndDate());
							if (dto.getServiceType() != null) {
								PQServiceType serviceType = pqServiceTypeRepository
										.findServiceTypeByName(dto.getServiceType());
								if (serviceType != null) {
									List<PQTransaction> tlist = transactionApi.filterListByTypeAndStatusAndRefNo(
											startDate, endDate, dto.getTransactionRefNo());
									List<TListDTO> resultList = userApi.convertFromRawTransactionsForBillPay(tlist);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Refund Transaction");
									result.setDetails(resultList);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Service Type not found");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Enter Service Type");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Enter start date and end date");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Refund/Transaction", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processTransactionsRefund(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RefundDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (dto.getTransactionRefNo() != null) {
							PQTransaction transaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo());
							if (transaction != null) {
								String serviceType = transaction.getService().getServiceType().getName();
								switch (serviceType.toUpperCase()) {
								case "FUND TRANSFER":
									result = transactionApi.processSendMoneyRefund(dto);
									break;
								case "FUND TRANSFER BANK":
									result = transactionApi.processBankTransferRefund(dto);
									break;
								case "BILL PAYMENT":
									result = transactionApi.processBillPaymentRefund(dto);
									break;
								case "LOAD MONEY":
									result = transactionApi.processLoadMoneyRefund(dto);
									break;
								case "TRAVEL EBS":
									result = transactionApi.processLoadMoneyRefund(dto);
									break;
								case "TRAVEL WALLET":
									result = transactionApi.processTravelWalletRefund(dto);
									break;
								default:
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Not a FUND TRANSFER/BILL PAYMENT/LOAD MONEY transaction");
									break;
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Transaction not exists");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Transaction ID is not valid");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Refund/BillPayTransaction", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processTransactionsRefundBillPay(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RefundDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (dto.getTransactionRefNo() != null) {
							PQTransaction transaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo());
							if (transaction != null) {
								String serviceType = transaction.getService().getServiceType().getName();
								switch (serviceType.toUpperCase()) {
								case "BILL PAYMENT":
									result = transactionApi.processBillPaymentRefund(dto);
									break;
								default:
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Not a FUND TRANSFER/BILL PAYMENT/LOAD MONEY transaction");
									break;
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Transaction not exists");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Transaction ID is not valid");
						}

						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	private boolean verifyServiceStatus(String name) {
		if (userApi.setServiceStatusInactive(name)) {
			return true;
		} else {
			return false;
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/ServiceTypes", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getServiceTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Service Type List");
						result.setDetails(pqServiceTypeRepository.findAll());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to GET ALL Services
	@RequestMapping(method = RequestMethod.POST, value = "/All/Versions", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllVersions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<VersionListDTO> list = userApi.getAllVersion();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to GET ALL Services
	@RequestMapping(method = RequestMethod.POST, value = "/Update/Versions", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateVersions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpdateServiceDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.updateVersionDetails(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetBulkUploadRedords", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBulkUpload(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BulkUploadDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("List of UploadFiles");
						result.setDetails(adminApi.getBulkUploadFiles());
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SaveBulkUploadRedords", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveBulkUploadRedords(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BulkUploadDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						result = adminApi.saveBulkRecord(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("approved the List of UploadFiles");
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/{username}/getMTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getSingleMerchantTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @PathVariable("username") String username,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							User merchantUser = userApi.findByUserName(username);
							PQService service = pgDetailsRepository.findServiceByUser(merchantUser);
							Page<PQTransaction> transList = transactionApi.getTotalTransactionsOfMerchantService(
									merchantUser.getUsername(), service, pageable);
							List<TransactionListDTO> resultList = new ArrayList<>();
							for (PQTransaction c : transList) {
								TransactionListDTO dt = new TransactionListDTO();
								User u = userApi.findByAccountDetails(c.getAccount());
								dt = ConvertUtil.convertListFromDetailsMerchantSuccess(merchantUser, c, u);
								resultList.add(dt);
							}
							Page<TransactionListDTO> resultSet = new PageImpl<TransactionListDTO>(resultList, pageable,
									resultList.size());

							Page<PQTransaction> commissionList = transactionApi
									.getCommissionOfMerchantServicePage(service, pageable);
							List<TransactionCommissionListDTO> commissions = new ArrayList<>();
							for (PQTransaction c : commissionList) {
								TransactionCommissionListDTO commission = new TransactionCommissionListDTO();
								commission = ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
								commissions.add(commission);
							}
							Page<TransactionCommissionListDTO> commisssionSet = new PageImpl<TransactionCommissionListDTO>(
									commissions, pageable, commissions.size());
							/*
							 * Pageable page = new PageRequest(dto.getPage(),
							 * dto.getSize(), sort); int start =
							 * page.getOffset(); int end = (start +
							 * page.getPageSize()) > resultList.size() ?
							 * resultList.size() : (start + page.getPageSize());
							 * int commissonend = (start + page.getPageSize()) >
							 * commission.size() ? commission.size() : (start +
							 * page.getPageSize()); List<TransactionListDTO>
							 * subList = resultList.subList(start, end);
							 * Page<TransactionListDTO> resultSet = new
							 * PageImpl<TransactionListDTO>(subList,
							 * page,resultList.size());
							 * 
							 * List<TransactionCommissionListDTO> subList1 =
							 * commission.subList(start, commissonend);
							 * Page<TransactionCommissionListDTO> resultSet1 =
							 * new
							 * PageImpl<TransactionCommissionListDTO>(subList1,
							 * page,commission.size());
							 */

							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("userList", resultSet);
							detail.put("commissionList", commisssionSet);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | Transaction List");
							result.setDetails(detail);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | Transactions Of Merchant");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | Transactions Of Merchant");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Ajax | Transactions Of Merchant");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetGCMIdByUserName", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getGCMIDByUserName(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if (accessDetails != null) {
							if (accessDetails.isSendGCM()) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								String gcmList = userApi.getGCMIdByUserName(dto.getUsername());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("GCM IDs");
								result.setDetails(gcmList);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveGCMIDs", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveGCMIDs(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendNotificationDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			try {
				if (role.equalsIgnoreCase("superAdmin")) {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if ((user.getAuthority().contains(Authorities.SUPER_ADMIN)
								||(user.getAuthority().contains(Authorities.ADMINISTRATOR)))
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							result = userApi.saveGCMIDs(dto);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Change Password");
					result.setDetails("Permission Not Granted");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your request is declined please contact customer care or try again later");
				result.setDetails("your request is declined please contact customer care or try again later");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/getRefundMerchantRequest", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getRefundMerchantRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RefundDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			try {
				if (role.equalsIgnoreCase("superAdmin")) {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							List<RefundDTO> plans = userApi.getAllMerchantRefundRequest();
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Get All Refund Request Of Merchant .");
							result.setDetails(plans);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Change Password");
					result.setDetails("Permission Not Granted");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your request is declined please contact customer care or try again later");
				result.setDetails("your request is declined please contact customer care or try again later");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/RefundMerchantTransaction", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processRefundMerchantTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RefundDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (dto.getTransactionRefNo() != null) {
							PQTransaction transaction = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo());
							if (transaction != null) {
									result = transactionApi.processMerchantPaymentRefund(dto);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Transaction not exists");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Transaction ID is not valid");
						}

						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	
	@RequestMapping(method = RequestMethod.POST, value = "/dataConfigList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllDataConfig(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<DataConfig> list = dataConfigApi.getAllConfig();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateMdexSwitch", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateMdexSwitch(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpdateServiceDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = dataConfigApi.updateMdexSwitch(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/GetAllServicesById", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllServiceById(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,@RequestBody ServicesRequest req,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		Map<String, Object> detail = new HashMap<String, Object>();
		List<PQService> serviceList = (List<PQService>) pqServiceRepository.findServiceByServiceTypeID(Long.parseLong(req.getServiceTypeId().trim()));
		List<ServicesDTO> services=ConvertUtil.convertService(serviceList);
		detail.put("services", services);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(services);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getCommission", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllCommission(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			@RequestBody UpdateServiceDTO req,HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		if(req.getCode()!=null){
		List<CommissionListDTO> comList=getCommissionList(req.getCode());
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Service List");
		result.setDetails(comList);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	private List<CommissionListDTO> getCommissionList(String code) {
		PQService service= pqServiceRepository.findActiveServiceByCode(code);
		List<PQCommission> commission=pqCommissionRepository.findCommissionByService(service);
		List<CommissionListDTO> comList= new ArrayList<>();
		if(commission!=null && commission.size()>0){
			for (PQCommission com : commission) {
				CommissionListDTO dto= new CommissionListDTO();
				dto.setFixed(com.isFixed()?"true":"false");
				dto.setValue(""+com.getValue());
				dto.setType(com.getType());
				dto.setIdentifier(com.getIdentifier());
				dto.setMaxAmount(""+com.getMaxAmount());
				dto.setMinAmount(""+com.getMinAmount());
				dto.setServiceId(com.getService().getId().toString());
				dto.setServiceName(service.getName());
				comList.add(dto);
			}
		}
		return comList;
	}
	@RequestMapping(value = "/updateCommission", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUpdateCommission(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			@RequestBody CommissionListDTO req,HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("SuperAdmin")) {
			String sessionId = req.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					boolean isUpdated= updateServiceCommission(req);
					if(isUpdated){
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setStatus(ResponseStatus.FAILURE);
					}
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

		}
	}

	private boolean updateServiceCommission(CommissionListDTO req) {
		List<PQCommission> commission=pqCommissionRepository.findCommissionByService
				(pqServiceRepository.findServiceById(Long.parseLong(req.getServiceId())));{
			if(commission!=null && !commission.isEmpty()){
				PQCommission com= commission.get(0);
				com.setType(req.getType());
				com.setFixed("true".equalsIgnoreCase(req.getFixed())?true:false);
				com.setValue(Double.parseDouble(req.getValue()));
				com.setIdentifier(getIdentifier(req));
				pqCommissionRepository.save(com);
				return true;
			}
		}
		return false;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/AddServices", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> addService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AddServicesDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.saveServices(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	private String getIdentifier(CommissionListDTO req) {
			String identifier = req.getType() + "|" + req.getValue() + "|" + req.getMinAmount() + "|"
					+ req.getMaxAmount() + "|" + req.getType() + "|" + req.getCode();
			return identifier;
	}
	
	@RequestMapping(value = "/GetOperatorList", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getOperatorList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PQOperator> operatorList = (List<PQOperator>) pqOperatorRepository.findAll();
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("Operator List");
		result.setDetails(ConvertUtil.convertOperator(operatorList));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetAadharDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAadharDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<AadharDetailsDTO> list = userApi.getAllAadharDetails();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(list.size()));
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/AddServiceType", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAddServiceType(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AddServicesDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.saveServiceType(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Users List");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Users List");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

}
