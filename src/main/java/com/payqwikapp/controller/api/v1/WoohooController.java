package com.payqwikapp.controller.api.v1;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IGiftCardApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.GciResponseDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.OrderPlaceResponse;
import com.payqwikapp.model.WoohooTransactionRequest;
import com.payqwikapp.model.WoohooTransactionResponse;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.error.GiftCardError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.GiftCardValidation;
import com.payqwikapp.validation.TransactionValidation;
@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Woohoo")
public class WoohooController {
	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository serviceRepository;
	private final ICommissionApi commissionApi;
	private final IGiftCardApi giftCardApi;
	private final GiftCardValidation giftCardValidation;
	



	public WoohooController(IUserApi userApi, ITransactionApi transactionApi,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			TransactionValidation transactionValidation, PQServiceRepository serviceRepository,
			ICommissionApi commissionApi,IGiftCardApi giftCardApi,GiftCardValidation giftCardValidation) {
		super();
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionValidation = transactionValidation;
		this.serviceRepository = serviceRepository;
		this.commissionApi = commissionApi;
		this.giftCardApi = giftCardApi;
		this.giftCardValidation= giftCardValidation;
	}






	@RequestMapping(value = "/InitiatePayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GciResponseDTO> processOrder(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody WoohooTransactionRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		GciResponseDTO result = new GciResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								PQService service = serviceRepository.findServiceByCode(StartupUtil.WOOHOO_SERVICECODE);
								PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,dto.getPrice());
								double netCommissionValue = commissionApi.getCommissionValue(commission,dto.getPrice());
								TransactionError transactionError = transactionValidation.validateWoohoo(String.valueOf(dto.getPrice()), user.getUsername(), service, commission, netCommissionValue);
								if (transactionError.isValid()) {
									String transactionRefNo=String.valueOf(System.currentTimeMillis());
									transactionApi.initiateWoohooPayment(dto, "Payment of WoohooGiftCards", service, transactionRefNo, user, StartupUtil.WOOHOO, commission, netCommissionValue);
									result.setCode(ResponseStatus.SUCCESS.getValue());
									result.setTransactionRefNo(transactionRefNo);
									return new ResponseEntity<>(result,HttpStatus.OK);
								}else{
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage(transactionError.getMessage());
									return new ResponseEntity<>(result,HttpStatus.OK);
								}
							}else{
								result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
								result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
								return new ResponseEntity<>(result,HttpStatus.OK);

							}
						}else{
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(ResponseStatus.FAILURE.getKey());
							return new ResponseEntity<>(result,HttpStatus.OK);
						}
					}else{
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						return new ResponseEntity<>(result,HttpStatus.OK);
					}
			}else{
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		}else{
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage(ResponseStatus.INVALID_HASH.getKey());
			return new ResponseEntity<>(result,HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/PaymentResponse", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GciResponseDTO> responseOrder(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody WoohooTransactionResponse dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		GciResponseDTO result = new GciResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								PQService service = serviceRepository.findServiceByCode(StartupUtil.WOOHOO_SERVICECODE);
								PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(dto.getCard_price()));
								double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(dto.getCard_price()));
								if (dto.isStatus()) {
									if(dto.getResponseCode().equalsIgnoreCase("00")){
									transactionApi.successWoohooPayment(dto.getTransactionRefNo(), commission, netCommissionValue, user, dto);
									result.setCode(ResponseStatus.SUCCESS.getValue());
									result.setMessage("Payment Successful.");
									result.setTransactionRefNo(dto.getTransactionRefNo());
									return new ResponseEntity<>(result,HttpStatus.OK);
									}else{
										transactionApi.pendingWoohooPayment(dto.getTransactionRefNo(), commission, netCommissionValue, user, dto);
										result.setCode(ResponseStatus.SUCCESS.getValue());
										result.setMessage("Your order is being processed, please wait for the update .");
										result.setTransactionRefNo(dto.getTransactionRefNo());
										return new ResponseEntity<>(result,HttpStatus.OK);
									}
								}else{
									transactionApi.failedWoohooPayment(dto.getTransactionRefNo());
									result.setCode(ResponseStatus.FAILURE.getValue());
									result.setMessage(ResponseStatus.FAILURE.getKey());
									return new ResponseEntity<>(result,HttpStatus.OK);
								}
							}else{
								result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
								result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
								return new ResponseEntity<>(result,HttpStatus.OK);

							}
						}else{
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setMessage(ResponseStatus.FAILURE.getKey());
							return new ResponseEntity<>(result,HttpStatus.OK);
						}
					}else{
						result.setCode(ResponseStatus.INVALID_SESSION.getValue());
						result.setMessage(ResponseStatus.INVALID_SESSION.getKey());
						return new ResponseEntity<>(result,HttpStatus.OK);
					}
			}else{
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		}else{
			result.setCode(ResponseStatus.INVALID_HASH.getValue());
			result.setMessage(ResponseStatus.INVALID_HASH.getKey());
			return new ResponseEntity<>(result,HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/WoohooProcessOrder", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GciResponseDTO> processOrderNew(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody WoohooTransactionRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		GciResponseDTO result = new GciResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				GiftCardError error = giftCardValidation.checkWoohooError(dto);
				if (error.isValid()) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								PQService service = serviceRepository.findServiceByCode(StartupUtil.WOOHOO_SERVICECODE);
								PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,dto.getPrice());
								double netCommissionValue = commissionApi.getCommissionValue(commission,dto.getPrice());
								TransactionError transactionError = transactionValidation.validateWoohoo(String.valueOf(dto.getPrice()), user.getUsername(), service, commission, netCommissionValue);
								if (transactionError.isValid()) {
									String transactionRefNo=String.valueOf(System.currentTimeMillis());
									OrderPlaceResponse giftCardResponse = giftCardApi.processWoohooOrder(dto,user, transactionRefNo, service, commission, netCommissionValue);
									if(giftCardResponse.getResponseCode().equalsIgnoreCase("00") && giftCardResponse.isSuccess()){
										giftCardResponse=transactionApi.successWoohooPaymentNew(transactionRefNo, commission, netCommissionValue, user, giftCardResponse);
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage("Transaction Successful");
										result.setDetails(giftCardResponse.getOrderNumber());
										result.setBalance(giftCardResponse.getRemainingBalance());
										result.setTransactionRefNo(transactionRefNo + "D");
										return new ResponseEntity<GciResponseDTO>(result, HttpStatus.OK);
										}else if(giftCardResponse.getResponseCode().equalsIgnoreCase("11")){
									   giftCardResponse=transactionApi.pendingWoohooPaymentNew(transactionRefNo, commission, netCommissionValue, user, giftCardResponse);
											result.setStatus(ResponseStatus.WOOHOO_PENDING);
											result.setMessage("Your order is being processed and you will receive an SMS notification when completed. "
													+ "Please note that this may take several minutes.");
											result.setDetails("Your order is being processed and you will receive an SMS notification when completed. "
													+ "Please note that this may take several minutes.");
											result.setTransactionRefNo(transactionRefNo);
											result.setBalance(giftCardResponse.getRemainingBalance());
											return new ResponseEntity<GciResponseDTO>(result, HttpStatus.OK);
									}else{
										transactionApi.failedWoohooPayment(transactionRefNo);
										result.setStatus((giftCardResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
										result.setMessage(giftCardResponse.getMessage());
										result.setDetails(giftCardResponse.getMessage());
										result.setBalance(giftCardResponse.getRemainingBalance());
										return new ResponseEntity<GciResponseDTO>(result, HttpStatus.OK);
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(transactionError.getMessage());
								}	
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed to get user authority.");
								result.setDetails("Failed to get user authority.");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please login and try again.");
						result.setDetails("Please login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	}
