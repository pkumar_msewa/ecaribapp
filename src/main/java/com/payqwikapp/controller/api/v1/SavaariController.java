package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ISavaariApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.SavaariBooking;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.BookSavaariDTO;
import com.payqwikapp.model.SavaariTokenDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.LogCat;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class SavaariController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	private final IUserApi userApi;
	private final ISavaariApi savaariApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository pqServiceRepository;

	public SavaariController(IUserApi userApi, ISavaariApi savaariApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, TransactionValidation transactionValidation,
			PQServiceRepository pqServiceRepository) {
		this.userApi = userApi;
		this.savaariApi = savaariApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		// TODO Auto-generated method stub

	}

	@RequestMapping(method = RequestMethod.POST, value = "/AccessToken", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveToken(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SavaariTokenDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		System.out.println(
				"---------------------------- INSIDE SAVE ACCESS TOKEN CONTROLLER IN APP -----------------------");
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user != null) {
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						System.err.println("contact No ::" + user.getContactNo());
						result = savaariApi.saveAccessToken(dto, user.getContactNo());
						if (result.getStatus().equalsIgnoreCase("Success")) {
							System.out.println("--------ACCESS TOKEN SAVED SUCCESSFULLY--------");
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Access token saved");
							result.setDetails(dto.getAccessToken());
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Unable to save access token");
							result.setDetails("Unable to save access token");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("User not found");
					result.setDetails("User not found");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Authenticate/Token", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> aunthticateToken(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SavaariTokenDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		System.out.println(
				"---------------------------- INSIDE SAVE ACCESS TOKEN CONTROLLER IN APP -----------------------");
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user != null) {
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = savaariApi.getAccessToken(dto, user.getContactNo());
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("User not found");
					result.setDetails("User not found");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SaveSavaariDetail", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveBusDetail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BookSavaariDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {

		ResponseDTO result = new ResponseDTO();
		logger.info("Inside | SaveSavaariDetail");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				try {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO userDto = userApi.getUserById(userSession.getUser().getId());
						if (userDto.getAuthority().contains(Authorities.USER)
								&& userDto.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(userDto.getUsername());
							PQService service = pqServiceRepository.findServiceByCode("VSVRI");
							if (savaariApi.checkBalance(user, dto)) {
								// TransactionError transactionError
								// =transactionValidation.validateBillPayment(dto.getPrePayment(),user.getUsername(),
								// service);
								// if (transactionError.isValid()) {
								savaariApi.saveSavaariTicket(dto, user, service);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Savaari Detail Saved");
								result.setDetails("Savaari Detail Saved");
								System.err.println("Savaari Detail Saved");
								result.setBalance(userApi.getBalanceByUserAccount(user.getAccountDetail()));
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Insufficient Funds");
							result.setDetails("Insufficient Funds");
							result.setBalance(userApi.getBalanceByUserAccount(user.getAccountDetail()));
							System.err.println("Insufficient Funds");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
							result.setMessage("Un-Authorised Role");
							result.setDetails("Un-Authorised Role");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					LogCat.print("Exception Caught");
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Internal server error");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BookSavaariResponse", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> BookSavaariResponse(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BookSavaariDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		logger.info("Inside | SavaariBookController | SaveBusDetail");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				try {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO userDto = userApi.getUserById(userSession.getUser().getId());
						if (userDto.getAuthority().contains(Authorities.USER)
								&& userDto.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(userDto.getUsername());
							// Call Savaari Ticket Booked API
							savaariApi.bookSavaariTicket(dto, user);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Savaari Ticket Booked");
							result.setDetails("Savaari Ticket Booked");
							result.setBalance(userApi.getBalanceByUserAccount(user.getAccountDetail()));
							System.err.println("Savaari Ticket Booked");
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
							result.setMessage("Un-Authorised Role");
							result.setDetails("Un-Authorised Role");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} catch (Exception e) {
					LogCat.print("Exception Caught");
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Internal server error");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/failBookSavaariResponse", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> failBookSavaariResponse(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BookSavaariDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		logger.info("Inside | SavaariController | SaveBusDetail");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				try {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO userDto = userApi.getUserById(userSession.getUser().getId());
						if (userDto.getAuthority().contains(Authorities.USER)
								&& userDto.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(userDto.getUsername());
							// call savaari ticket failed API
							savaariApi.failBookSavaariTicket(dto);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Savaari Ticket Failed");
							result.setDetails("Savaari Ticket Failed");
							System.err.println("Savaari Ticket Failed");
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
							result.setMessage("Un-Authorised Role");
							result.setDetails("Un-Authorised Role");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} catch (Exception e) {
					LogCat.print("Exception Caught");
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Internal server error");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	/* to do cancellation api */

	@RequestMapping(method = RequestMethod.POST, value = "/InitiateCancelation", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cancelSavaariTicket(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BookSavaariDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		logger.info("Inside | SavaariController | CancellSavaariDetail");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				try {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO userDto = userApi.getUserById(userSession.getUser().getId());
						if (userDto.getAuthority().contains(Authorities.USER)
								&& userDto.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(userDto.getUsername());
							// call Savaari ticket Cancel API
							boolean bookingId = savaariApi.checkBookingId(user, dto);
							if (bookingId) {
								boolean checkUser = savaariApi.checkUserBookingId(user, dto);
								if (checkUser) {
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Initiate Success Cancellation");
									result.setDetails("Initiate Success Cancellation");
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("User not Available");
									result.setDetails("User not Available");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("BookingId not Available");
								result.setDetails("BookingId not Available");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
							result.setMessage("Un-Authorised Role");
							result.setDetails("Un-Authorised Role");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} catch (Exception e) {
					LogCat.print("Exception Caught");
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Internal server error");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cancelTicket", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cancelSavaari(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BookSavaariDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		logger.info("Inside | SavaariController | CancellSavaariDetail");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				try {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO userDto = userApi.getUserById(userSession.getUser().getId());
						if (userDto.getAuthority().contains(Authorities.USER)
								&& userDto.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(userDto.getUsername());
							PQService service = pqServiceRepository.findServiceByCode("VSVRI");
							// call Savaari ticket Cancel API
							TransactionError transactionError = transactionValidation.validateSavaariCancellation(
									dto.getPrePayment(), user.getUsername(), service, dto.getBookingId());
							if (transactionError.isValid()) {
								savaariApi.CancelBookSavaariTicket(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Savaari Ticket Cancelled");
								result.setDetails("Savaari Ticket Cancelled");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("You didnot get any refund amount");
								result.setDetails(transactionError.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
							result.setMessage("Un-Authorised Role");
							result.setDetails("Un-Authorised Role");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} catch (Exception e) {
					LogCat.print("Exception Caught");
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Internal server error");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetTicketDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User activeUser = userApi.findByUserName(user.getUsername());
						List<SavaariBooking> bookingList=savaariApi.getBookingList(activeUser,Status.Booked);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Booking details received.");
						result.setDetails(bookingList);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

}
