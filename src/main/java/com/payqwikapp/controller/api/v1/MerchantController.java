package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwik.visa.utils.FetchEntityId;
import com.payqwik.visa.utils.SecurityTest;
import com.payqwik.visa.utils.YappayEncryptedResponse;
import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.IMerchantApi;
import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.MerchantRefundRequest;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.entity.VijayaBankReport;
import com.payqwikapp.entity.VisaMerchant;
import com.payqwikapp.model.BulkUploadDTO;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.FilteredReportDTO;
import com.payqwikapp.model.MTransactionResponseDTO;
import com.payqwikapp.model.MerchantReportDTO;
import com.payqwikapp.model.MerchantReportDetails;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.PaymentRequestDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionStatusDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.VPQMerchantDTO;
import com.payqwikapp.model.VerifyMobileDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.error.VerifyMobileOTPError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.MerchantRefundRequestRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.TPTransactionRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ClientException;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.MobileOTPValidation;
import com.payqwikapp.validation.RegisterValidation;
import com.payqwikapp.validation.TransactionValidation;
import com.thirdparty.model.request.VisaMerchantRequest;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Merchant")
public class MerchantController {

	
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final IUserApi userApi;
	private final TPTransactionRepository tpTransactionRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ITransactionApi transactionApi;
	private final RegisterValidation registerValidation;
	private final TransactionValidation transactionValidation;
	private final ISendMoneyApi sendMoneyApi;
	private final IMerchantApi merchantApi;
	private final MobileOTPValidation mobileOTPValidation;
	private final IMailSenderApi mailSenderApi;
	private final MerchantRefundRequestRepository merchantRefundRequestRepository;
	private final PQTransactionRepository transactionRepository;
	private final PQServiceRepository pqServiceRepository;

	public MerchantController(IUserApi userApi, TPTransactionRepository tpTransactionRepository,
			PGDetailsRepository pgDetailsRepository, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, ITransactionApi transactionApi,
			RegisterValidation registerValidation, TransactionValidation transactionValidation,
			ISendMoneyApi sendMoneyApi, IMerchantApi merchantApi, MobileOTPValidation mobileOTPValidation,IMailSenderApi mailSenderApi,MerchantRefundRequestRepository merchantRefundRequestRepository,
			 PQTransactionRepository transactionRepository,PQServiceRepository pqServiceRepository) {
		this.userApi = userApi;
		this.tpTransactionRepository = tpTransactionRepository;
		this.pgDetailsRepository = pgDetailsRepository;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionApi = transactionApi;
		this.registerValidation = registerValidation;
		this.transactionValidation = transactionValidation;
		this.sendMoneyApi = sendMoneyApi;
		this.merchantApi = merchantApi;
		this.mobileOTPValidation = mobileOTPValidation;
		this.mailSenderApi=mailSenderApi;
		this.merchantRefundRequestRepository= merchantRefundRequestRepository;
		this.transactionRepository = transactionRepository;
		this.pqServiceRepository=pqServiceRepository;
	}

	@RequestMapping(value = "/GetTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getTotalTransactions(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TPTransaction> transactionsList = tpTransactionRepository
								.findByMerchant(userSession.getUser());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("PG Transactions");
						result.setDetails(transactionsList);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/GetVPayQwikTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getVPayQwikTransactions(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User merchant = userApi.findByUserName(user.getUsername());
						PQService service = pgDetailsRepository.findServiceByUser(merchant);
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(0, 1000, sort);
						Page<PQTransaction> transactionList = transactionApi.transactionListByServicePage(page,service);
						Page<TPTransaction> transactions = transactionApi.transactionListByMerchantPage(page,merchant);
						List<TPTransaction> tpTransactionList = transactions.getContent();
						List<MTransactionResponseDTO> list = userApi.getDTOList(transactionList.getContent());
						Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
								list.size());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("VPayQwik Merchant Transactions");
						result.setDetails(resultSet);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/GetLast20VPayQwikTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getlast20VPayQwikTransactions(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User merchant = userApi.findByUserName(user.getUsername());
						PQService service = pgDetailsRepository.findServiceByUser(merchant);
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(0, 20, sort);
						Page<PQTransaction> transactionList = transactionApi.last20transactionListByServicePage(page,service);
//						Page<TPTransaction> transactions = transactionApi.transactionListByMerchantPage(page,merchant);
//						List<TPTransaction> tpTransactionList = transactions.getContent();
						List<MTransactionResponseDTO> list = userApi.getDTOList(transactionList.getContent());
						Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
								list.size());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("VPayQwik Merchant Transactions");
						result.setDetails(resultSet);
						System.err.println("the resultset is:::"+result);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/GetDebitVPayQwikTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getDebitVPayQwikTransactions(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User merchant = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = merchant.getAccountDetail();
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
						Page<PQTransaction> transactionList = transactionApi.findDebittransactionListByServicePage(page,account);
						List<MTransactionResponseDTO> list = userApi.getDTOList(transactionList.getContent());
						Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
								list.size());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("VPayQwik Merchant Transactions");
						result.setDetails(resultSet);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(value = "/GetVPayQwikTransactionsFiltered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getVPayQwikTransactionsFiltered(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) throws ParseException {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User merchant = userApi.findByUserName(user.getUsername());
						PQService service = pgDetailsRepository.findServiceByUser(merchant);
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
						Page<PQTransaction> transactionList = transactionApi.transactionListByServicePageFiltered(page,service,dateFormat.parse(from), dateFormat.parse(to));
						System.err.println("trnas ::" + transactionList.getNumberOfElements());
						Page<TPTransaction> transactions = transactionApi.transactionListByMerchantPageFiltered(page,merchant,dateFormat.parse(from), dateFormat.parse(to));
						List<TPTransaction> tpTransactionList = transactions.getContent();
						List<MTransactionResponseDTO> list = userApi.getDTOList(transactionList.getContent());
						Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
								list.size());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("VPayQwik Merchant Transactions");
						result.setDetails(resultSet);
						System.err.println("the resultset is:::"+result);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> changePassword(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody ChangePasswordDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession session = userSessionRepository.findByActiveSessionId(sessionId);
				if (session != null) {
					UserDTO user = userApi.getUserById(session.getUser().getId());
					if (user != null) {
						String authority = user.getAuthority();
						dto.setUsername(user.getUsername());
						if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							ChangePasswordError error = registerValidation.validateChangePasswordDTO(dto);
							if (error.isValid()) {
								userApi.renewPasswordFromAccount(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Password Updated Successfully.");
								result.setDetails("Password Updated Successfully.");
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage(error.getPassword());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Please login and try again");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and Try Again");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed,Unauthorized User");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RequestOffline", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> requestPaymentFromUser(@PathVariable(value = "version") String version,
			@PathVariable("role") String role, @PathVariable("device") String device,
			@PathVariable("language") String language, @RequestHeader(value = "hash") String hash,
			@RequestBody PaymentRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession session = userSessionRepository.findByActiveSessionId(sessionId);
				if (session != null) {
					UserDTO merchant = userApi.getUserById(session.getUser().getId());
					if (merchant != null) {
						String authority = merchant.getAuthority();
						if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(dto.getMobileNumber());
							User currentUser = userApi.findByUserName(merchant.getUsername());
							// TODO checks if user is available
							if (user != null) {
								// TODO checks whether service is available for
								// merchant
								PQService service = pgDetailsRepository.findServiceByUser(currentUser);
								if (service != null) {
									// TODO checks transaction validation
									TransactionError error = transactionValidation
											.validateOfflinePayment(dto.getAmount(), currentUser, user, service);
									if (error.isValid()) {
										// TODO send OTP to user
										result = userApi.requestOfflinePayment(dto, currentUser, user);

									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(error.getMessage());
									}

								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Services not defined for your account");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("User Not Exists");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Please login and try again");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and Try Again");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed,Unauthorized User");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessOffline", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> processPaymentFromUser(@PathVariable(value = "version") String version,
			@PathVariable("role") String role, @PathVariable("device") String device,
			@PathVariable("language") String language, @RequestHeader(value = "hash") String hash,
			@RequestBody PaymentRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession session = userSessionRepository.findByActiveSessionId(sessionId);
				if (session != null) {
					UserDTO merchant = userApi.getUserById(session.getUser().getId());
					if (merchant != null) {
						String authority = merchant.getAuthority();
						if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User user = userApi.findByUserName(dto.getMobileNumber());
							User currentUser = userApi.findByUserName(merchant.getUsername());
							// TODO checks if user is available
							if (user != null) {
								// TODO checks whether service is available for
								// merchant
								PQService service = pgDetailsRepository.findServiceByUser(currentUser);
								if (service != null) {
									// TODO checks transaction validation
									TransactionError error = transactionValidation
											.validateOfflinePayment(dto.getAmount(), currentUser, user, service);
									if (error.isValid()) {
										// TODO checks if OTP is valid
										String otp = user.getMobileToken();
										String receivedToken = dto.getOtp();
										if (otp.equals(receivedToken)) {
											// TODO perform pay at store
											user.setMobileToken(null);
											userApi.saveOrUpdateUser(user);
											PayStoreDTO pay = new PayStoreDTO();
											pay.setId(currentUser.getId());
											pay.setNetAmount(dto.getAmount());
											result = sendMoneyApi.preparePayStore(pay, user);
										} else {
											result.setStatus(ResponseStatus.FAILURE);
											result.setMessage("OTP doesn't match");
										}

									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(error.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Services not defined for your account");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("User Not Exists");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Please login and try again");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and Try Again");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed,Unauthorized User");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SignUp", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody com.thirdparty.model.request.VisaMerchantRequest merchant, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		// boolean isValidHash = SecurityUtil.isHashMatches(merchant, hash);
		System.err.println("----------------- INSIDE SIGN UP CONTROLLER IN APP --------------------");
		// if (isValidHash) {
		if (role.equalsIgnoreCase("Merchant")) {
			// RegisterError error = new RegisterError();
			// try {
			// merchant.setUsername(merchant.getUsername());
			// error = registerValidation.validateMerchant(merchant);
			// if (error.isValid()) {
			merchant.setUserType(UserType.Merchant);
			try {
				userApi.visaMerchantSignUp(merchant);

				System.err.println("saved.....");
			} catch (ClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Merchant SignUp Successful");
			// result.setDetails(EncryptM2PRequest.buildEncryptedJsonForM2P(merchant));
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Register not Merchant");
			System.err.println("merchant not registered....");
			// result.setDetails(error);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		/*
		 * catch (Exception e) {
		 * result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		 * result.setMessage("Please try again later.");
		 * result.setDetails(e.getMessage()); return new
		 * ResponseEntity<ResponseDTO>(result, HttpStatus.OK); } } else {
		 * result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
		 * result.setMessage("Failed, Unauthorized Merchant.");
		 * result.setDetails("Failed, Unauthorized Merchant."); } } else {
		 * result.setStatus(ResponseStatus.INVALID_HASH);
		 * result.setMessage("Failed, Please try again later."); return new
		 * ResponseEntity<ResponseDTO>(result, HttpStatus.OK); } return new
		 * ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		 */

	}

	@RequestMapping(method = RequestMethod.POST, value = "/addBankDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> addMBank(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO merchant, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(merchant, hash);
		System.err.println("----------------- INSIDE SIGN UP CONTROLLER IN APP --------------------");
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				RegisterError error = new RegisterError();
				try {
					merchant.setUsername(merchant.getUsername());
					error = registerValidation.validateMerchant(merchant);
					if (error.isValid()) {
						merchant.setUserType(UserType.Merchant);
						userApi.registerMerchant(merchant);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Merchant Bank Details added Successfully and OTP sent to ::"
								+ merchant.getContactNo());
						result.setDetails("Merchant Bank Details added Successfully and OTP sent to ::"
								+ merchant.getContactNo());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Register Merchant");
						result.setDetails(error);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Please try again later.");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized Merchant.");
				result.setDetails("Failed, Unauthorized Merchant.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/checkExistingMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> checkExistingMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		System.err.println("Inside Visa Merchant Save");
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String email = dto.getEmailAddress();
				User merchant = userApi.findByUserName(email);
				VisaMerchant visaMerchant = merchantApi.findByEmail(email);
				System.err.println("Visa merchant " + visaMerchant);
				if (visaMerchant == null) {
					if (merchant != null) {
						System.err.println("merchant Authority " + merchant.getAuthority());
						if (merchant.getAuthority().contains(Authorities.MERCHANT)) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Merchant exists");
							result.setDetails("Merchant exists");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Merchant doesn't exists with this EMAIL ID");
							result.setDetails("Merchant doesn't exists with this EMAIL ID");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Merchant doesn't exists with this EMAIL ID");
						result.setDetails("Merchant doesn't exists with this EMAIL ID");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Merchant already exists with this EMAIL ID");
					result.setDetails("Merchant already exists with this EMAIL ID");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signUpVisaMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveVisaMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String email = dto.getEmailAddress();
				User merchant = userApi.findByUserName(email);
				// result = merchantApi.addVisaMerchant(dto, merchant);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ActivateMerchantMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				if (verifyMerchantMobileToken(dto.getKey(), dto.getMobileNumber())) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Activate Mobile");
					result.setDetails("Your Mobile is Successfully Verified");
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Resend/MobileOtp", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> generateNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String username = dto.getMobileNumber();
				VerifyMobileOTPError error = new VerifyMobileOTPError();
				error = mobileOTPValidation.validateMerchantResendOTP(username);
				if (error.isValid()) {
					userApi.resendMerchantMobileToken(username);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Resend OTP");
					result.setDetails("New OTP sent on " + username);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Resend OTP");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	private boolean verifyMerchantMobileToken(String key, String mobileNumber) {
		if (userApi.checkMerchantMobileTokenNew(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetEntityId", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getEntityIdM2p(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FetchEntityId dto, HttpServletRequest request, HttpServletResponse response) {
		// verify role here
		ResponseDTO resp = merchantApi.getEntityId(dto.getUsername());
		if (resp != null) {
			String entity = (String) resp.getDetails();
			if (entity != null) {
				resp.setCode("S00");
				resp.setMessage("entity id fetched successfully...");
				resp.setStatus(ResponseStatus.SUCCESS);
				resp.setDetails(entity);

			} else {
				resp.setCode("S00");
				resp.setMessage("entity id not found...");
				resp.setStatus(ResponseStatus.FAILURE);

			}

		}

		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody com.thirdparty.model.request.VisaMerchantRequest merchant, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		System.err.println("----------------- INSIDE UPDATE CONTROLLER IN APP --------------------");
		if (role.equalsIgnoreCase("Merchant")) {
			System.err.println("username" + merchant.getUserName());
			userApi.updateMerchantM2P(merchant.getCustomerId(), merchant.getMvisaId(), merchant.getRupayId(),
					merchant.getUserName());
			System.err.println("updated.....");
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("Merchant updated Successful");
			result.setDetails("Merchant updation Successful");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("merchant not updated");
			System.err.println("merchant not updated....");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveqr", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateQRcode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest merchant, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		System.err.println("----------------- INSIDE UPDATE CONTROLLER IN APP --------------------");
		if (role.equalsIgnoreCase("Merchant")) {
			System.err.println(merchant.getUserName());
			System.err.println(merchant.getQrcode());
			userApi.updateQRcode(merchant.getUserName(), merchant.getQrcode());
			System.err.println("updated.....");
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("QR Updated Successfully");
			result.setDetails("QR Updated Successfully");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("QR not updated");
			System.err.println("QR not updated");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	/*
	 * public static void main(String...a) throws Exception{ VisaMerchantEnc en
	 * = new VisaMerchantEnc(); MerchantBankDetail mDetails = new
	 * MerchantBankDetail(); SettlementBankDetail sDetails = new
	 * SettlementBankDetail(); en.setFirstName("Ajay");
	 * en.setLastName("Sharma"); en.setEmailAddress("asharma@msewa.com");
	 * en.setAddress1("Address 1"); en.setAddress2("Address 2");
	 * en.setCity("Bikaner"); en.setState("Rajasthan"); en.setCountry("India");
	 * en.setPinCode("334001"); en.setMerchantCategory(1);
	 * en.setLattitude("1234.23"); en.setLongitude("1212.23");
	 * en.setPanNo("AAXPG2515M"); en.setMobileNo("919449147913");
	 * en.setAggregator("M2P");
	 * 
	 * mDetails.setAccName("TestBank");
	 * mDetails.setBankAccNo("121216017927854770");
	 * mDetails.setBankIfscCode("CITI00001");
	 * mDetails.setBankLocation("CHENNAI"); mDetails.setBankName("CITI");
	 * 
	 * sDetails.setAccName("TestBank");
	 * sDetails.setBankAccNo("121216017927854770");
	 * sDetails.setBankIfscCode("CITI00001");
	 * sDetails.setBankLocation("CHENNAI"); sDetails.setBankName("CITI");
	 * 
	 * en.setMerchantBankDetail(mDetails); en.setSettlementBankDetail(sDetails);
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); String req1 =
	 * mapper.writeValueAsString(en);
	 * 
	 * SecurityTest sec = new SecurityTest(); String enc =
	 * sec.encodeRequest(req1, "9876543211234562", "VIJAYA");
	 * System.out.println("red  "+enc);
	 * 
	 * Client client = Client.create(); WebResource webResource =
	 * client.resource("https://merc.yappay.in/merchant/v2/signup");
	 * 
	 * ClientResponse response =
	 * webResource.accept("application/json").header("TENANT",
	 * "VIJAYA_MERCHANT").type("application/json").post(ClientResponse.class,
	 * enc);
	 * 
	 * 
	 * String strResponse = response.getEntity(String.class); String decrypted =
	 * sec.decodeResponse( mapper.readValue(strResponse,
	 * YappayEncryptedResponse.class)); System.out.println("de"+decrypted);
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	// }
	@RequestMapping(method = RequestMethod.POST, value = "/decryptresponse", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> decryptResponse(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody String enc, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		System.err.println("----------------- INSIDE dec CONTROLLER IN APP --------------------");
		if (role.equalsIgnoreCase("Merchant")) {
			SecurityTest sec = new SecurityTest();
			ObjectMapper mapper = new ObjectMapper();
			String decrypted = null;
			try {
				decrypted = sec.decodeResponse(mapper.readValue(enc, YappayEncryptedResponse.class));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.err.println("decrypted.....");
			result.setStatus(ResponseStatus.SUCCESS);
			result.setMessage("decrypted");
			result.setDetails(decrypted);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("nt decr");
			System.err.println("merchant not updated....");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/getTransactionStatus", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionStatus(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionStatusDTO trx, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO dto = new ResponseDTO();
		boolean transactionFlag;
		System.err.println("I am here::::::::::");
		PQTransaction Dtransaction = merchantApi.getStatusByTran(trx.getTransactionRefNo() + "D");
		System.err.println(Dtransaction);
		PQTransaction Ctransaction = merchantApi.getStatusByTran(trx.getTransactionRefNo() + "C");
		if (Dtransaction != null && Ctransaction != null) {
			if (Ctransaction.getStatus().getValue().equalsIgnoreCase("Success")
					&& Dtransaction.getStatus().getValue().equalsIgnoreCase("Success")) {

				transactionFlag = true;
				dto.setCode("S00");
				dto.setMessage("Transaction is Success");
				dto.setStatus(ResponseStatus.SUCCESS);
				dto.setTxnId(trx.getTransactionRefNo());
			} else {
				dto.setCode("F00");
				dto.setMessage("Transaction is not Success");
				dto.setStatus(ResponseStatus.FAILURE);
				dto.setTxnId(trx.getTransactionRefNo());

			}
		} else {
			dto.setCode("F00");
			dto.setMessage("Transaction id not found");
			dto.setStatus(ResponseStatus.FAILURE);
			dto.setTxnId(trx.getTransactionRefNo());
		}

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/RegisterVpqMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VPQMerchantDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
				ResponseDTO result = new ResponseDTO();
						String email = dto.getEmail();
						User merchant = userApi.findByUserName(email);
						if (merchant == null) {
							PGDetails pgDetails = userApi.findMerchantByMobile(dto.getContactNo());
							if (pgDetails == null) {
								if(dto.isPaymentGateway()==false){
									dto.setIpAddress("127.0.0.1");
									dto.setSuccessURL("http://localhost:8080/Success");
									dto.setReturnURL("http://localhost:8080/Failure");
								}
								userApi.saveVpqMerchant(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Merchant added successfully");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Mobile already exists");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Merchant Already exists with this EMAIL ID");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} 
	
	@RequestMapping(method = RequestMethod.POST, value = "/getFingerPrint", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getFingerPrint(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UserDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
						ResponseDTO result = new ResponseDTO();
						User user = userApi.findByUserName(dto.getUsername());
						if(user.getAuthority().contains(Authorities.USER)&& user.getAuthority().contains(Authorities.AUTHENTICATED)){
						if (user == null) {
							UserDetail details=user.getUserDetail();
							if(details!=null){
								String fingerPrint=details.getFingerPrint();
								if(fingerPrint!=null){
									
								result.setCode("S00");
								result.setMessage("finger print fetched");
								result.setStatus(ResponseStatus.SUCCESS);
								result.setInfo(fingerPrint);
								}else{
									result.setCode("F00");
									result.setMessage("fingerPrint doesn't exist");
									result.setStatus(ResponseStatus.FAILURE);
									}
							
							} else {
								result.setCode("F00");;
								result.setMessage("User doesn't exist ");
								result.setStatus(ResponseStatus.FAILURE);
							}
						} else {
							result.setCode("F00");
							result.setMessage("User doesn't exist please register in vpayqwik");
							result.setStatus(ResponseStatus.FAILURE);
							
						}
					} else{
						result.setCode("F00");
						result.setMessage("User authentication failed..");
						result.setStatus(ResponseStatus.FAILURE);
					}
			
						return new ResponseEntity<>(result,HttpStatus.OK);
			
	}
	
	@RequestMapping(value = "/GetVPayQwikMerchantCommision", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getVPayQwikMerchantCommision(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User merchant = userApi.findByUserName(user.getUsername());
						PQService service = pgDetailsRepository.findServiceByUser(merchant);
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
						Page<PQTransaction> transactionList = transactionApi.transactionListByServicePageCommssion(page,service);
						Page<TPTransaction> transactions = transactionApi.transactionListByMerchantPage(page,merchant);
						List<TPTransaction> tpTransactionList = transactions.getContent();
						System.err.println(" transactions "+transactionList.getContent());

						System.err.println("TP transaction "+tpTransactionList.size());
						System.err.println(tpTransactionList.isEmpty());
						List<MTransactionResponseDTO> list = userApi.getDTOList(transactionList.getContent());
						Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
								list.size());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("VPayQwik Merchant Transactions");
						result.setDetails(resultSet);
						//System.err.println("the resultset is:::"+result);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/GetVPayQwikMerchantTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getVPayQwikMerchantTransactions(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if(!user.getUsername().equalsIgnoreCase("enquiries@treatcard.in")){
						User merchant = userApi.findByUserName(user.getUsername());
						PQService service = pgDetailsRepository.findServiceByUser(merchant);
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
						Page<PQTransaction> transactionList = transactionApi.transactionListServicePage(page,service);
						List<TPTransaction> transactions = transactionApi.transactionListMerchantPage(merchant);
						List<MTransactionResponseDTO> list = userApi.getDTOList(transactionList.getContent(),transactions);
						Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
								list.size());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("VPayQwik Merchant Transactions");
						result.setDetails(resultSet);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
							PQService service = pqServiceRepository.findServiceByCode("TECD");
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> transactionList = transactionApi.transactionListServicePageForTreatCard(page,service);
							List<MTransactionResponseDTO> list = userApi.getDTOListForTreatCard(transactionList.getContent());
							Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(list, page,
									list.size());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("VPayQwik Merchant Transactions");
							result.setDetails(resultSet);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	@RequestMapping(value = "/RefundMerchant", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> refundRequest(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						mailSenderApi.refundRequestMerchant(dto.getPath(),user.getUsername(),user.getUserId());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Your request has been proccessed successfully.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(value = "/RefundMerchantTransaction", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> refundMTransaction(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody RefundDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						if (dto.getTransactionRefNo() != null) {
							result=userApi.refundMerchantRequest(dto);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Transaction ID is not valid");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	// TODO Besocom Merchant 
	@RequestMapping(value = "/GetBescomMerchantTxns", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> getBescomMercahntTxns(@PathVariable(value = "version") String verison,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User merchant = userApi.findByUserName(user.getUsername());
						PQService service = pgDetailsRepository.findServiceByUser(merchant);
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
						Page<PQTransaction> transactionList = transactionApi.transactionListServiceBescom(page,service);
//						if(transactionList.ge)
						
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Merchant Transactions");
						result.setDetails(transactionList);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/BulkUpload", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBulkUpload(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MerchantReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						merchantApi.saveBulkFile(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("file uploaded successfully");
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMerchantReport", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUpiTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.MERCHANT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							List<VijayaBankReport> report=null;
							try {
									report = merchantApi.getMerchantReport();
									result.setDetails(getTransactionList(report));
							} catch (Exception e) {
								e.printStackTrace();
							}
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | User List");
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getFilteredMerchantReport", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getFilteredMerchantReport(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FilteredReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.MERCHANT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							List<VijayaBankReport> report = merchantApi.getFilteredMerchantReport(
									dateFormat.parse(dto.getStartDate()), dateFormat.parse(dto.getEndDate()), dto.getPaymentMode(),
									dto.getConsumerType());
							result.setDetails(getTransactionList(report));
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Ajax | User List");
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	private List<MerchantReportDetails> getTransactionList(List<VijayaBankReport> content) {
		List<MerchantReportDetails> listDto=new ArrayList<>();
		for (VijayaBankReport report : content) {
			try {
				MerchantReportDetails reportDetails = new MerchantReportDetails();
				reportDetails.setModeOfPayment(report.getModeOfPayment());
				reportDetails.setConsumerType(report.getConsumerType());
				reportDetails.setPaymentChannel(report.getPaymentChannel());
				reportDetails.setTypeOfPayment(report.getTypeOfPayment());
				reportDetails.setTransactionType(report.getTransactionType());
				reportDetails.setSubDivisonCode(report.getSubDivisonCode());
				reportDetails.setAccountId(report.getAccountId());
				reportDetails.setTransactionRefId(report.getTransactionRefId());
				reportDetails.setPgId(report.getPgId());
				reportDetails.setVendorRefId(report.getVendorRefId());
				reportDetails.setBillAmount(report.getBillAmount());
				reportDetails.setCharges(report.getCharges());
				reportDetails.setNetAmount(report.getNetAmount());
				reportDetails.setTransactionDate(report.getTransactionDate());
				reportDetails.setPostedDate(report.getPostedDate());
				reportDetails.setNameOfConsumer(report.getNameOfConsumer());
				reportDetails.setContactNumber(report.getContactNumber());
				reportDetails.setEmailId(report.getEmailId());
				reportDetails.setVendorRrn(report.getVendorRrn());
				listDto.add(reportDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return  listDto;
	}
}
