package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ebs.model.EBSRedirectResponse;
import com.ebs.model.EBSRequest;
import com.ebs.model.error.EBSRequestError;
import com.ebs.validation.EBSValidation;
import com.payqwikapp.api.IEBSApi;
import com.payqwikapp.api.IFlightApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.IVNetApi;
import com.payqwikapp.entity.FlightAirLineList;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.FlightTravellers;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.DateDTO;
import com.payqwikapp.model.FlightPaymentRequest;
import com.payqwikapp.model.FlightPaymentResponse;
import com.payqwikapp.model.FlightResponseDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.VNetDTO;
import com.payqwikapp.model.VNetError;
import com.payqwikapp.model.VNetResponse;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.bus.BusPaymentReq;
import com.payqwikapp.model.travel.flight.CompareCountry;
import com.payqwikapp.model.travel.flight.FlghtTicketResp;
import com.payqwikapp.model.travel.flight.FlightPayment;
import com.payqwikapp.model.travel.flight.TicketsResp;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;
import com.payqwikapp.validation.VNetValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Flight")
public class FlightController {
	
	
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
	private final IUserApi userApi;
	private final PQServiceRepository pqServiceRepository;
	private UserSessionRepository userSessionRepository;
	private final TransactionValidation transactionValidation;
	private final IFlightApi flightApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final IEBSApi ebsApi;
	private final EBSValidation ebsValidation;
	private final IVNetApi vNetApi;
	private final VNetValidation vNetValidation;

	public FlightController(IUserApi userApi, PQServiceRepository pqServiceRepository,UserSessionRepository userSessionRepository,TransactionValidation transactionValidation,IFlightApi flightApi,PersistingSessionRegistry persistingSessionRegistry,
			IEBSApi ebsApi,EBSValidation ebsValidation,IVNetApi vNetApi,VNetValidation vNetValidation) {
		super();
		this.userApi = userApi;
		this.pqServiceRepository = pqServiceRepository;
		this.userSessionRepository=userSessionRepository;
		this.transactionValidation=transactionValidation;
		this.flightApi=flightApi;
		this.persistingSessionRegistry=persistingSessionRegistry;
		this.ebsApi=ebsApi;
		this.ebsValidation=ebsValidation;
		this.vNetApi= vNetApi;
		this.vNetValidation=vNetValidation;
	}
	
	/* Fresh Code Started By Subir */
	
	@RequestMapping(value = "/FlightInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> flightInitiate(@PathVariable(value = "role") String role,
														   @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
														   @RequestBody FlightPayment dto, @RequestHeader(value = "hash", required = true) String hash,
														   HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.FLIGHT_CODE);
							if(service != null && Status.Active.equals(service.getStatus())) {
								TransactionError error = transactionValidation.validateEaseMyTrip(String.valueOf(dto.getPaymentAmount()),user.getUsername(),service);
								if(error.isValid()) {
									dto.setConvenienceFee(String.valueOf(200));
									if ((dto.getBaseFare() != null || dto.getBaseFare().isEmpty())
											&& (dto.getConvenienceFee() != null || dto.getConvenienceFee().isEmpty())) {
										FlightPaymentResponse orderResponse = flightApi.flightInit(dto, service, user);
										if (orderResponse.isValid()) {
											result.setStatus(ResponseStatus.SUCCESS.getKey());
											result.setMessage(orderResponse.getMessage());
											result.setCode(ResponseStatus.SUCCESS.getValue());
											result.setDetails(orderResponse);
										} else {
											result.setStatus(ResponseStatus.FAILURE.getKey());
											result.setMessage(orderResponse.getMessage());
											result.setCode(ResponseStatus.FAILURE.getValue());
											result.setDetails(orderResponse.getMessage());
										}
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage("Base fare and convience fee should not empty.");
										result.setDetails("Base fare and convience fee should not empty.");
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(error.getMessage());
									result.setCode(error.getCode());
									result.setBalance(error.getSplitAmount());
									result.setDetails(error.getMessage());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setMessage("Service down for maintenance");
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setDetails("Service down for maintenance");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setMessage("Failed,Unauthorized User");
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					result.setMessage("Please login and try again.");
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
					result.setDetails("Session Expired");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
				result.setMessage("Unauthorized user.");
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			result.setMessage("Invalid request.");
			result.setCode(ResponseStatus.BAD_REQUEST.getValue());
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/FlightPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> FlightSuccess(@PathVariable(value = "role") String role,
													@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
													@RequestBody FlightPayment dto, @RequestHeader(value = "hash", required = true) String hash,
													HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							User user2=userApi.findByUserName(user.getUsername());
							FlightPaymentResponse paymentResponse = flightApi.flightPayment(dto,user2);
							if(paymentResponse.isValid()) {
								result.setStatus(ResponseStatus.SUCCESS.getKey());
								result.setMessage(paymentResponse.getMessage());
								result.setCode(ResponseStatus.SUCCESS.getValue());
								result.setDetails(paymentResponse.getMessage());
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setCode(ResponseStatus.FAILURE.getValue());
								result.setMessage(paymentResponse.getMessage());
								result.setDetails(paymentResponse.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			result.setCode(ResponseStatus.BAD_REQUEST.getValue());
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/PaymentGateWayDetailsSave", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processPaymentGateway(@PathVariable(value = "role") String role,
													@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
													@RequestBody FlightPayment dto, @RequestHeader(value = "hash", required = true) String hash,
													HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, org.json.JSONException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							User u=userApi.findByUserName(user.getUsername());
							if(dto.isSuccess()){
								System.err.println("TxnRefNo:: "+dto.getMerchantRefNo());
							    flightApi.paymentGateWaySuccess(dto,u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Flight Booked Successfully .");
								result.setDetails("Flight Booked Successfully .");
							}else {
								flightApi.paymentGateWayFailure(dto,u);
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Flight Not Booked .");
								result.setDetails("Flight Not Booked .");
							  }
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMyFlightTickets", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMyFlightTickets(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FlightPayment dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							List<FlghtTicketResp> allTickets=flightApi.getAllTickets(user.getUsername());
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Get All Book Tickets");
							result.setDetails(allTickets);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/*Show Flight details in Backend*/
	
	@RequestMapping(value = "/GetFlightDetailsForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> getFlightDetailsForAdmin(@RequestBody SessionDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		 FlightResponseDTO result=new FlightResponseDTO();
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) ||
							user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<FlightTicket> list=flightApi.getFlightDetailForAdmin();
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Flight Details received");
							result.setDetails(list);
							return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("no flight details found");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
		} 
	
	
	
	@RequestMapping(value = "/cronCheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cronCheck(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							flightApi.cronforSaveCityList();
							result.setDetails("Data Saved");
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getAirLineList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAirLineList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							List<FlightAirLineList> airLineLists=flightApi.getAllAirLineList();
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Get All AriLine List From DB");
							result.setDetails(airLineLists);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getFlightTravellerDetailsForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTravellerDetailsForAdmin(@RequestBody FlightPayment dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) ||
							user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						List<FlightTravellers> list=flightApi.getFlightTravellersForAdmin(dto.getFlightId());
						TicketsResp list2=flightApi.getFlightTravellersNameForAdmin(dto.getFlightId());
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Flight Traveller Details received");
							result.setDetails(list);
							result.setDetails2(list2);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("Flight Traveller not found");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 

	
	@RequestMapping(value = "/GetFlightDetailsByDate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> getFlightDetailsByDate(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		 FlightResponseDTO result=new FlightResponseDTO();
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) ||
							user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						
						String from = dto.getFromDate() + " 00:00";
						String to = dto.getToDate() + " 23:59";
						
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<FlightTicket> list=flightApi.getFlightDetailForAdminByDate(dateFormat.parse(from), dateFormat.parse(to));
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Flight Details received");
							result.setDetails(list);
							return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("no flight details found");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
		} 
	
	@RequestMapping(value = "/compareCountry", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> compareCountry(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CompareCountry dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							boolean val=flightApi.comCountry(dto);
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Compare Country");
							result.setDetails(val);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ProcessFlightEBS", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processFlightEBS(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EBSRequest ebsRequest, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {

		ResponseDTO result = new ResponseDTO();
		FlightPayment order=new FlightPayment();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = ebsRequest.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			EBSRequestError error = new EBSRequestError();
			error = ebsValidation.validateRequest(ebsRequest, "EMTF");
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode("EMTF");
								TransactionError transactionError = transactionValidation
										.validateFlightEBSTransaction(ebsRequest.getAmount(), user.getUsername(), service);
								if (transactionError.isValid()) {
									EBSRequest newRequest = ebsApi.requestHandlerFLight(ebsRequest, user.getUsername(), service);
									if (ebsRequest.getAmount()!=null && !ebsRequest.getAmount().isEmpty()) {
											order.setPaymentAmount(Double.parseDouble(ebsRequest.getAmount()));
									}
									order.setPaymentMethod(ebsRequest.getMode());
									order.setTicketDetails(ebsRequest.getTicketDetails());
									order.setTravellerDetails(ebsRequest.getTravellerDetails());
									flightApi.saveFlight(order, newRequest.getPqTransaction(), userSession.getUser(),ebsRequest.getBaseFare());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Success");
									result.setDetails(newRequest);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Failed to pay at store. Please try again.");
									result.setDetails(transactionError);
								}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		modelMap.addAttribute("error", "FAILED");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedirectFlightEBS", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectLoadMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@ModelAttribute EBSRedirectResponse redirectResponse, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			result = ebsApi.responseHandlerFlight(redirectResponse);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	} 
	
	
	@RequestMapping(value = "/InitiateVNetFlight", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> initiateVNetBanking(@PathVariable(value = "role") String role,
														   @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
														   @RequestBody VNetDTO dto, HttpServletRequest request, HttpServletResponse response,
														   Model modelMap) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			VNetError  error = vNetValidation.validateRequest(dto);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode("EMTF");
								TransactionError transactionError = transactionValidation.validateFlightEBSTransaction(dto.getAmount(), user.getUsername(), service);
								if (transactionError.isValid()) {
									VNetDTO newDTO = vNetApi.processRequestFlight(dto, user.getUsername(), service);
									
									FlightPayment order=new FlightPayment();
									if (dto.getAmount()!=null) {
										if (!dto.getAmount().isEmpty())
										{
											order.setPaymentAmount(Double.parseDouble(dto.getAmount()));
										}
									}
									
									order.setPaymentMethod(dto.getMode());
									order.setTicketDetails(dto.getTicketDetails());
									order.setTravellerDetails(dto.getTravellerDetails());
									
									flightApi.saveFlight(order, newDTO.getPqTransaction(), userSession.getUser(),null);
									
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Success");
									result.setDetails(newDTO);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(transactionError.getMessage());
								}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		modelMap.addAttribute("error", "FAILED");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/RedirectVNetFlight", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectLoadMoneyVNet(@PathVariable(value = "role") String role,
															 @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
															 @RequestBody VNetResponse dto, HttpServletRequest request,
															 HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") && device.equalsIgnoreCase("Website") || role.equalsIgnoreCase("Agent")) {
			result = vNetApi.handleResponseFlight(dto);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/SavePaymentGateWayDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processPaymentGateway(@PathVariable(value = "role") String role,
													@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
													@RequestBody FlightPaymentRequest dto, @RequestHeader(value = "hash", required = true) String hash,
													HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, org.json.JSONException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							User u=userApi.findByUserName(user.getUsername());
							if(dto.isSuccess()){
								System.err.println("TxnRefNo:: "+dto.getTransactionRefno());
							    flightApi.processPaymentGateWaySuccess(dto,u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Flight Booked Successfully .");
								result.setDetails("Flight Booked Successfully .");
							}else {
								flightApi.processPaymentGateWayFailure(dto,u);
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Flight Not Booked .");
								result.setDetails("Flight Not Booked .");
							  }
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getVnetTxnRefNo", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getVnetTxnRefNo(@PathVariable(value = "role") String role,
													@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
													@RequestBody FlightPaymentRequest dto, @RequestHeader(value = "hash", required = true) String hash,
													HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, org.json.JSONException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							User u=userApi.findByUserName(user.getUsername());
							PQAccountDetail account=u.getAccountDetail();
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
}
