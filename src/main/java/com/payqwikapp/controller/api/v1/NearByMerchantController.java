package com.payqwikapp.controller.api.v1;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.NearByMerchantResponseDTO;
import com.payqwikapp.model.NearByMerchantsDTO;
import com.payqwikapp.model.NearMerchantDetailsDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class NearByMerchantController {

	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final UserSessionRepository userSessionRepository;

	public NearByMerchantController(IUserApi userApi, PersistingSessionRegistry persistingSessionRegistry,
			UserSessionRepository userSessionRepository) {
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.userSessionRepository = userSessionRepository;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetNearByMerchants", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NearByMerchantResponseDTO> nearByMerchants(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody NearByMerchantsDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		NearByMerchantResponseDTO result = new NearByMerchantResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)
							|| user.getAuthority().contains(Authorities.MERCHANT)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u=userApi.findByUserName(user.getUsername());
						if (user.getUserType().equals(UserType.User)) {
							boolean isSaved = userApi.saveUserDetails(dto,u.getUserDetail().getId());
							if(isSaved){
							 List<NearMerchantDetailsDTO> merchantList=userApi.calculateDistance(dto);
							    result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Merchant List");
								result.setDetails(merchantList);
							}
						} else if (user.getUserType().equals(UserType.Merchant)) {
							userApi.saveMerchantDetails(dto,u.getUserDetail().getId());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Latitude and longitude of merchant updated.");
							result.setDetails("Latitude and longitude of merchant updated.");
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<NearByMerchantResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/findNearByMerchants", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NearByMerchantResponseDTO> findByMerchants(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody NearByMerchantsDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		NearByMerchantResponseDTO result = new NearByMerchantResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u=userApi.findByUserName(user.getUsername());
						if (user.getUserType().equals(UserType.User)) {
							/*boolean isSaved = userApi.saveUserDetails(dto,u.getUserDetail().getId());
							if(isSaved){
							 List<NearMerchantDetailsDTO> merchantList=userApi.calculateDistance(dto);
							    result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Merchant List");
								result.setDetails(merchantList);
							}*/
							List<NearMerchantDetailsDTO> merchantList=userApi.calculateDistance(dto);
							if (merchantList != null && !merchantList.isEmpty()) {
								result.setStatus(ResponseStatus.SUCCESS.getKey());
								result.setMessage("Merchant List");
								result.setDetails(merchantList);
								result.setCode(ResponseStatus.SUCCESS.getValue());
							} else {
								result.setStatus(ResponseStatus.FAILURE.getKey());
								result.setMessage("No Nearest Merchant Found.");
								result.setDetails("No Nearest Merchant Found.");
								result.setCode(ResponseStatus.FAILURE.getValue());
							}
							
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
							result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER.getKey());
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<NearByMerchantResponseDTO>(result, HttpStatus.OK);
	}
}
