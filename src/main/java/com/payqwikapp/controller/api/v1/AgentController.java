package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.search.StringTerm;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.payqwikapp.api.ISecurityQuestionApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.AgentDetailsDTO;
import com.payqwikapp.model.AgentRequest;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.ForgotPasswordDTO;
import com.payqwikapp.model.LoginDTO;
import com.payqwikapp.model.MerchantDTO;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.PromoCodeDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.VerifyMobileDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.ForgotPasswordError;
import com.payqwikapp.model.error.LoginError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.VerifyMobileOTPError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.EmailOTPValidation;
import com.payqwikapp.validation.MobileOTPValidation;
import com.payqwikapp.validation.RegisterValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class AgentController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUserApi userApi;
	private final ISecurityQuestionApi securityQuestionApi;
	private final RegisterValidation registerValidation;
	private final MobileOTPValidation mobileOTPValidation;
	private final EmailOTPValidation emailOTPValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ISessionApi sessionApi;
	private final ITransactionApi transactionApi;

	public AgentController(IUserApi userApi, ISecurityQuestionApi securityQuestionApi,
			RegisterValidation registerValidation, MobileOTPValidation mobileOTPValidation,
			EmailOTPValidation emailOTPValidation, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, ISessionApi sessionApi,
			ITransactionApi transactionApi) {
		super();
		this.userApi = userApi;
		this.securityQuestionApi = securityQuestionApi;
		this.registerValidation = registerValidation;
		this.mobileOTPValidation = mobileOTPValidation;
		this.emailOTPValidation = emailOTPValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.sessionApi = sessionApi;
		this.transactionApi = transactionApi;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AgentRegister", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerAgent(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AgentRequest user, @RequestHeader(value = "hash") String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(user, hash);
		if (isValidHash) {
			String sessionId = user.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				if (role.equalsIgnoreCase("Agent")) {
					RegisterError error = new RegisterError();
					try {
						user.setUserName("A"+user.getMobileNo());
						error = registerValidation.validateAgent(user);
						if (error.isValid()) {
							userApi.saveAgent(user);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Registration Successful");
							result.setDetails("Registration Successful");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("" + error.getMessage());
							result.setDetails(error);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} catch (Exception e) {
						System.out.println(e);
						result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
						result.setMessage("Please try again later.");
						result.setDetails(e.getMessage());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized Agent.");
					result.setDetails("Failed, Unauthorized Agent.");
				}

			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please login and try again.");
				result.setDetails("Please login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/Activate/AgentMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				if (verifyAgentMobileToken(dto.getKey(), dto.getMobileNumber())) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Activate Mobile");
					result.setDetails("Your Mobile is Successfully Verified");
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/All", produces = 
		{ MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String languagemys,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
					    List<User> userList = userApi.getTotalAgents();
						result.setMessage("Agent List");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(userList);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/GetCityAndState", produces = 
		{ MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getCityState(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String languagemys,
			@RequestBody AgentRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result = userApi.getCityAndState(dto);
						} catch (Exception e) {
							e.printStackTrace();
						}
						/*result.setMessage("Get city and state");
						result.setStatus(ResponseStatus.SUCCESS);
	//					result.setDetails(userList);
*/						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/Agent/GetBankList", produces = 
		{ MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String languagemys,
			@RequestBody AgentRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")||role.equalsIgnoreCase("Agent")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)|| user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) ) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result = userApi.getBankList();
						} catch (Exception e) {
							e.printStackTrace();
						}
						/*result.setMessage("Get city and state");
						result.setStatus(ResponseStatus.SUCCESS);
	//					result.setDetails(userList);
*/						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	private boolean verifyAgentMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, "A" + mobileNumber)) {
			return true;
		} else {
			return false;
		}

	}

	private boolean verifyAgentToUserMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/RegisterUsertoAgent", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerUserToAgent(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader(value = "hash") String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();

		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						RegisterError error = new RegisterError();
						dto.setUsername(dto.getContactNo());
						error = registerValidation.validateNormalUser(dto);
						try {
							User u = userApi.findByUserName(user.getUsername());
							if (error.isValid()) {
								user.setUserType(UserType.User);
								userApi.saveAgentToUser(dto, u);
								result.setStatus(ResponseStatus.SUCCESS);
								/*result.setMessage(
										"User Registration Successful and OTP sent to ::" + user.getContactNo()
												+ " and  verification mail sent to ::" + user.getEmail() + " .");
								result.setDetails(
										"User Registration Successful and OTP sent to ::" + user.getContactNo()
												+ " and  verification mail sent to ::" + user.getEmail() + " .");*/
								result.setMessage("User Registration Successful and OTP sent to ::" + user.getContactNo()+" .");
								result.setDetails("User Registration Successful and OTP sent to ::" + user.getContactNo()+" .");
								
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage("" + error.getMessage());
								result.setDetails(error);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} catch (Exception e) {
							System.out.println(e);
							result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
							result.setMessage("Please try again later.");
							result.setDetails(e.getMessage());
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					}

				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/Activate/AgentUserMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyAgentUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {

			if (role.equalsIgnoreCase("Agent")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					if (verifyAgentToUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Activate Mobile");
						result.setDetails("Your Mobile is Successfully Verified");
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Activate Mobile");
						result.setDetails("Invalid Activation Key");
					}

				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/ResendOTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> generateAgentNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					String username = dto.getMobileNumber();
					VerifyMobileOTPError error = new VerifyMobileOTPError();
					error = mobileOTPValidation.validateResendOTP("A"+username);
					if (error.isValid()) {
						userApi.resendMobileTokenNew("A"+username);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Resend OTP");
						result.setDetails("New OTP sent on " + username);
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Resend OTP");
						result.setDetails(error);
					}

				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "AgentResend/Mobile/OTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> generateUserAgentNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					String username = dto.getMobileNumber();
					VerifyMobileOTPError error = new VerifyMobileOTPError();
					error = mobileOTPValidation.validateResendOTP(username);
					if (error.isValid()) {
						userApi.resendMobileTokenNew(username);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("New OTP sent on " + username);
						result.setDetails("New OTP sent on " + username);
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(error.getOtp());
						result.setDetails(error.getOtp());
					}

				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> AgentForgotPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotPasswordDTO forgot, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(forgot, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {

				String username = forgot.getUsername();
				ForgotPasswordError error = registerValidation.forgotPassword(username);
				if (true) // error.isValid() ==
				{
					User u = userApi.findByUserName("A" + username);
					if (u != null) {
						if (u.getAuthority().contains(Authorities.AUTHENTICATED)) {
							if (true)// (u.getMobileStatus() == Status.Active)
							{
								userApi.changePasswordRequest(u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Forgot Password");
								result.setDetails("Please, Check your SMS for OTP Code to change your password.");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Please try again later.");
								result.setDetails("Please try again later.");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Your account is blocked! Please try after 24 hrs.");
							result.setDetails("Your account is blocked! Please try after 24 hrs.");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Please try again later.");
						result.setDetails("Please try again later.");

					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error);
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AgentRenewPassword", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> AgentRenewPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				dto.setUsername("A" + dto.getUsername());
				ChangePasswordError error = new ChangePasswordError();
				error = registerValidation.renewPasswordValidation(dto);
				if (error.isValid()) {

					userApi.renewPassword(dto);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Renew Password");
					result.setDetails("Password Changed Successfully");
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Renew Password");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/RegisterUsertoAgentgetall", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerUserToAgentgetall(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotPasswordDTO dto, @RequestHeader(value = "hash") String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();

		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (true)// (isValidHash)
		{
			if (role.equalsIgnoreCase("Agent")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String type = "ALL";
						Map<String, Object> detail = new HashMap<String, Object>();

						User u = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User receipts");
						result.setDetails(detail);
						result.setMessage("Merchant List");
						result.setStatus(ResponseStatus.SUCCESS);
						// result.setDetails(userList);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}

			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please login and try again.");
				result.setDetails("Please login and try again.");
			}
		} 
		return new ResponseEntity<>(result, HttpStatus.OK);

	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/editAgentList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAgentDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AgentDetailsDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
				dto = userApi.getAgentDetailsById(dto.getAgentId());
				result.setStatus(ResponseStatus.SUCCESS);
				result.setDetails(dto);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION);
			result.setMessage("Session invalid.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

}
