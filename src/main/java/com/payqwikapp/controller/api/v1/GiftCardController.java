package com.payqwikapp.controller.api.v1;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IGiftCardApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.GiftCardToken;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.GciResponseDTO;
import com.payqwikapp.model.GiftCardTransactionResponse;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.GiftCardError;
import com.payqwikapp.model.error.MobileTopupError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.GiftCardValidation;
import com.payqwikapp.validation.MobileTopupValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Gci")
public class GiftCardController {

	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository serviceRepository;
	private final IGiftCardApi giftCardApi;
	private final ICommissionApi commissionApi;
	private final GiftCardValidation giftCardValidation;

	public GiftCardController(IUserApi userApi, ITransactionApi transactionApi,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			TransactionValidation transactionValidation, PQServiceRepository serviceRepository,
			IGiftCardApi giftCardApi, ICommissionApi commissionApi, GiftCardValidation giftCardValidation) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionValidation = transactionValidation;
		this.serviceRepository = serviceRepository;
		this.giftCardApi = giftCardApi;
		this.commissionApi = commissionApi;
		this.giftCardValidation = giftCardValidation;

	}

	@RequestMapping(value = "/GetAccessToken", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getGciAuthentication(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.ADMINISTRATOR)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							GiftCardToken authDTO = giftCardApi.createSessionForGiftCard(dto.getSessionId());
							if (authDTO != null) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Get Access Token");
								result.setDetails(authDTO);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Gci service is down for maintenance");
								result.setMessage("Gci service is down for maintenance");

							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user authority.");
							result.setDetails("Failed to get user authority.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessGiftCard", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GciResponseDTO> getGiftCardProcess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ProcessGiftCardDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		GciResponseDTO result = new GciResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				GiftCardError error = giftCardValidation.checkError(dto);
				if (error.isValid()) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								PQService service = serviceRepository.findServiceByCode(StartupUtil.GiftcartCode);
								PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(dto.getAmount()));
								double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(dto.getAmount()));
								TransactionError transactionError = transactionValidation.validateGiftcart(dto.getAmount(), user.getUsername(), service, commission, netCommissionValue);
								if (transactionError.isValid()) {
									GiftCardToken authDTO = giftCardApi.createSessionForGiftCard(dto.getSessionId());
									if (authDTO != null) {
										dto.setToken(authDTO.getToken());
										String transactionRefNo = System.currentTimeMillis() + "";
										GiftCardTransactionResponse giftCardResponse = giftCardApi.processOrder(dto,user, transactionRefNo, service, commission, netCommissionValue);
										if (giftCardResponse.isSuccess()) {
											transactionApi.successGiftCartPayment(transactionRefNo,giftCardResponse.getReceiptNo(), commission, netCommissionValue,giftCardResponse, user, dto);
											giftCardApi.saveUserReceipt(giftCardResponse, user, dto);
											result.setStatus((giftCardResponse.isSuccess() ? ResponseStatus.SUCCESS
													: ResponseStatus.FAILURE));
											result.setMessage("Transaction Successful");
											result.setDetails("Transaction Successful");
											return new ResponseEntity<GciResponseDTO>(result, HttpStatus.OK);
										} else {
											transactionApi.failedGiftCartPayment(transactionRefNo);
											result.setStatus((giftCardResponse.isSuccess() ? ResponseStatus.SUCCESS
													: ResponseStatus.FAILURE));
											result.setMessage(giftCardResponse.getMessage());
											result.setDetails(giftCardResponse.getMessage());
											return new ResponseEntity<GciResponseDTO>(result, HttpStatus.OK);
										}
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage("Gci service is down for maintenance");
									}

								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(transactionError.getMessage());
								}

							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed to get user authority.");
								result.setDetails("Failed to get user authority.");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please login and try again.");
						result.setDetails("Please login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
