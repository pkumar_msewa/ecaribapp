package com.payqwikapp.controller.api.v1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ebs.model.EBSRedirectResponse;
import com.ebs.model.EBSRequest;
import com.ebs.model.error.EBSRequestError;
import com.ebs.validation.EBSValidation;
import com.payqwikapp.api.IEBSApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.IVNetApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.entity.Voucher;
import com.payqwikapp.model.LoadMoneyRequest;
import com.payqwikapp.model.RazorPayRequest;
import com.payqwikapp.model.RazorPayResponse;
import com.payqwikapp.model.RefundStatusDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UPIResponse;
import com.payqwikapp.model.UpiMobileRedirectReq;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.VNetDTO;
import com.payqwikapp.model.VNetError;
import com.payqwikapp.model.VNetResponse;
import com.payqwikapp.model.error.LoadMoneyError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.validation.LoadMoneyValidation;
import com.payqwikapp.validation.TransactionValidation;
import com.payqwikapp.validation.VNetValidation;
import com.upi.api.IUpiApi;
import com.upi.model.UpiErrorRequest;
import com.upi.model.UpiRequest;
import com.upi.validation.UpiValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/LoadMoney")
public class LoadMoneyController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	private MessageSource messageSource;

	private final IEBSApi ebsApi;
	private final EBSValidation ebsValidation;
	private final IVNetApi vNetApi;
	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final VNetValidation vNetValidation;
	private final UpiValidation upiValidation;
	private final IUpiApi upiApi;
	private final LoadMoneyValidation loadMoneyValidation;

	public LoadMoneyController(IEBSApi ebsApi, EBSValidation ebsValidation, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, IUserApi userApi, ITransactionApi transactionApi,
			TransactionValidation transactionValidation, PQServiceRepository pqServiceRepository,
			VNetValidation vNetValidation, IVNetApi vNetApi, UpiValidation upiValidation, IUpiApi upiApi,LoadMoneyValidation loadMoneyValidation) {
		this.ebsApi = ebsApi;
		this.ebsValidation = ebsValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
		this.vNetValidation = vNetValidation;
		this.vNetApi = vNetApi;
		this.upiValidation = upiValidation;
		this.upiApi = upiApi;
		this.loadMoneyValidation = loadMoneyValidation;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/ProcessUPI", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processLoadMoneyUPI(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpiRequest upiRequest, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {
		ResponseDTO result = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
				String sessionId = upiRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				UpiErrorRequest error = upiValidation.upiValidation(upiRequest, "LMU");
				if (error.isValid()) {
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)
									|| user.getAuthority().contains(Authorities.AGENT)
											&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								PQService service = pqServiceRepository.findServiceByCode("LMU");
								TransactionError transactionError = transactionValidation.validateLoadMoneyTransaction(
										upiRequest.getAmount(), user.getUsername(), service);
								if (transactionError.isValid()) {
									result = upiApi.upiHandler(upiRequest, user.getUsername(), service);
									result.setMessage("Loadmoney has been initiated successfully");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setTxnId("No Txns Id Found");
									result.setDetails(transactionError);
								}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized user.");
								result.setDetails("Failed, Unauthorized user.");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage(error.getErrMsg());
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}

		modelMap.addAttribute("error", "FAILED");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/FailedUpiTxns", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processLoadMoneyUpiFailed(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpiRequest upiRequest, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {
		ResponseDTO result = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
				String sessionId = upiRequest.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				UpiErrorRequest error = upiValidation.upiValidation(upiRequest, "LMU");
				if (error.isValid()) {
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)
									|| user.getAuthority().contains(Authorities.AGENT)
											&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								transactionApi.failedUPILoadMoneyWithTxnRef(upiRequest.getTransactionRefNo());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Successfully failed");
								result.setDetails("Successfully failed");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized user.");
								result.setDetails("Failed, Unauthorized user.");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage(error.getErrMsg());
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}

		modelMap.addAttribute("error", "FAILED");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedirectUPI", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectLoadMoneyUPI(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UPIResponse dto, HttpServletRequest request, HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") && device.equalsIgnoreCase("Website") || role.equalsIgnoreCase("Agent")) {
			result = upiApi.upiResponseHandler(dto);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/InitiateVNet", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> initiateVNetBanking(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VNetDTO dto, HttpServletRequest request, HttpServletResponse response, Model modelMap) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			VNetError error = vNetValidation.validateRequest(dto);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.AGENT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User u = userApi.findByUserName(user.getUsername());
							PQService service = pqServiceRepository.findServiceByCode("LMB");
							TransactionError transactionError = transactionValidation
									.validateLoadMoneyTransaction(dto.getAmount(), user.getUsername(), service);
							if (transactionError.isValid()) {
								result = vNetApi.processRequest(dto, user.getUsername(), service , u);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(transactionError.getMessage());
								result.setDetails(transactionError);
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		modelMap.addAttribute("error", "FAILED");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedirectVNet", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectLoadMoneyVNet(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VNetResponse dto, HttpServletRequest request, HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") && device.equalsIgnoreCase("Website") || role.equalsIgnoreCase("Agent")) {
			PQService service = pqServiceRepository.findServiceByCode("LMB");
			PQTransaction transaction = transactionApi.getTransactionByRefNo(dto.getPrn() + "C");
			if (transaction != null) {
				PQAccountDetail account = transaction.getAccount();
				if (account != null) {
					User u = userApi.findByAccountDetail(account);
					if (u != null) {
						result = vNetApi.handleResponse(dto, service, u);
					}
				}
			}
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processLoadMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody EBSRequest ebsRequest, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {

		ResponseDTO result = new ResponseDTO();
		PQService service=null;
		if (role.equalsIgnoreCase("User")){
			service = pqServiceRepository.findServiceByCode("LMC");
		}else if(role.equalsIgnoreCase("Agent")){
			service = pqServiceRepository.findServiceByCode("LMA");
		}else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
			String sessionId = ebsRequest.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			EBSRequestError error = new EBSRequestError();
			error = ebsValidation.validateRequest(ebsRequest,service.getCode());
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.AGENT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							User u= userApi.findByUserName(user.getUsername());
							TransactionError transactionError = transactionValidation
									.validateLoadMoneyTransaction(ebsRequest.getAmount(), user.getUsername(), service);
							if (transactionError.isValid()) {
								EBSRequest newRequest= ebsApi.requestHandler(ebsRequest, user.getUsername(), service);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Success");
								result.setDetails(newRequest);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(transactionError.getMessage());
								result.setDetails(transactionError);
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		modelMap.addAttribute("error", "FAILED");
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Redirect", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectLoadMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@ModelAttribute EBSRedirectResponse redirectResponse, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		PQService service=null;
		if (role.equalsIgnoreCase("User")){
			service = pqServiceRepository.findServiceByCode("LMC");
		}else if(role.equalsIgnoreCase("Agent")){
			service = pqServiceRepository.findServiceByCode("LMA");
		}
			PQTransaction transaction = transactionApi.getTransactionByRefNo(redirectResponse.getMerchantRefNo() + "C");
			if (transaction != null) {
				PQAccountDetail account = transaction.getAccount();
				if (account != null) {
					User u = userApi.findByAccountDetail(account);
					if (u != null) {
						UserSession userSession = userSessionRepository.findByActiveSessionIdOfUser(u);
						if (userSession != null) {
							redirectResponse.setAmount(String.valueOf(transaction.getAmount()));
							result = ebsApi.responseHandler(redirectResponse, service,userSession.getSessionId(),u);
						} else {
							result.setStatus(ResponseStatus.INVALID_SESSION);
							result.setMessage("Please, login and try again.");
							result.setDetails("Please, login and try again.");
						}
					}
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("TrnasactionRefNo is Null .");
				result.setDetails("TrnasactionRefNo is Null .");
			}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RefundStatus", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> refundLoadMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RefundStatusDTO dto, HttpServletRequest request, HttpServletResponse response, Model model)
					throws ParseException {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getFrom() + " 00:00";
		String to = dto.getTo() + " 23:59";
		PQService service = pqServiceRepository.findServiceByCode("LMC");
		List<PQTransaction> list = transactionApi.getDailyTransactionByDateStatus(dateFormat1.parse(from),
				dateFormat1.parse(to), Status.Initiated, service);
		List<RefundStatusDTO> refundList = ConvertUtil.getRefundTransactions(list);
		result.setStatus(ResponseStatus.SUCCESS);
		result.setMessage("SUCCESS||RESPONSE");
		result.setDetails(refundList);
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTransactionTimeDiff", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> checkTimeDiff(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, HttpServletRequest request, HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		boolean trxFlag = true;
		try {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchnat")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.MERCHANT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQAccountDetail accountDetail = user.getAccountDetail();
							Date date = transactionApi.getLastTranasactionTimeStamp(user.getAccountDetail());
							Date currentDate = new Date();
							long currentTimeInMillis = System.currentTimeMillis();
							if (date != null) {
								long lastTransactionTimeStamp = date.getTime();
								String lastTransactionDate = dateFormat.format(date);
								String currentTransactionDate = dateFormat.format(currentDate);
								if (accountDetail.getAccountType().getCode().equalsIgnoreCase("NONKYC")) {
									result.setMessage(
											"Dear customer as per RBI guidelines its mandatory to provide your KYC details to load money into your wallet.");
									trxFlag = false;
									result.setValid(trxFlag);
									result.setCode("F02");
								} else if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
									result.setMessage("Please wait for current transaction to complete");
									trxFlag = false;
									result.setValid(trxFlag);
									result.setCode("F00");
								} else if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 0.5) {
									result.setMessage("Please wait for 30 sec");
									trxFlag = false;
									result.setValid(trxFlag);
									result.setCode("F00");
								} else {
									result.setCode("S00");
									result.setValid(trxFlag);
									result.setMessage("redirecting to payment gateway...");
								}
							} else {
								if (accountDetail.getAccountType().getCode().equalsIgnoreCase("NONKYC")) {
									result.setMessage(
											"Dear customer as per RBI guidelines its mandatory to provide your KYC details to load money into your wallet.");
									trxFlag = false;
									result.setValid(trxFlag);
									result.setCode("F02");
								} else {
									result.setCode("S00");
									result.setValid(trxFlag);
									result.setMessage("redirecting to payment gateway....");
								}
							}
						}
					}
				} else {
					result.setCode("F03");
					result.setMessage("Please login again");
					trxFlag = false;
					result.setValid(trxFlag);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("Please Try Again Later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiRedirectStatus", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> checkUpiRedirectStatus(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VNetDTO dto, HttpServletRequest request, HttpServletResponse response, Model modelMap) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			PQTransaction txns = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo() + "C");
			if (txns != null) {
				if (txns.getStatus().equals(Status.Sent)) {
					PQTransaction sucTxn = transactionApi
							.getTransactionByRetrivalReferenceNo(dto.getTransactionRefNo() + "C");
					if (sucTxn != null) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction Success");
						result.setDetails(ConvertUtil.convertUpiTransaction(sucTxn));
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
					long timestamp = txns.getCreated().getTime();
					long currentTime = System.currentTimeMillis();
					if (!((currentTime - timestamp) < 1000 * 60 * 5)) {
						System.err.println("The expiration date of your request has elapsed");
						transactionApi.failedUPILoadMoneyWithTxnRef(dto.getTransactionRefNo());
						result.setStatus(ResponseStatus.UPI_DATE_EXPIRED);
						result.setMessage(
								"The expiration date of your request has elapsed. Therefore your transaction has automatically been rejected.");
						result.setDetails(ConvertUtil.convertUpiTransaction(txns));
					} else {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction Success");
						result.setDetails(ConvertUtil.convertUpiTransaction(txns));
					}
				} else {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Transaction Success");
					result.setDetails(ConvertUtil.convertUpiTransaction(txns));
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Transaction Not Available");
				result.setDetails("Transaction Not Available");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	// TODO SDK Redirect Response
	@RequestMapping(value = "/SDKRedirectUPI", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectSdkLoadMoneyUPI(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpiMobileRedirectReq dto, HttpServletRequest request, HttpServletResponse response,
			Model model) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") && device.equalsIgnoreCase("Website") || role.equalsIgnoreCase("Agent")) {
			result = upiApi.upiSdkResponseHandler(dto);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/RazorPayProcess", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processLoadMoneyRazorPay(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RazorPayRequest razorPayrequest, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {

		ResponseDTO result = new ResponseDTO();
		PQService service=null;
		if (role.equalsIgnoreCase("User")){
			service = pqServiceRepository.findServiceByCode("LMC");
		}else if(role.equalsIgnoreCase("Agent")){
			service = pqServiceRepository.findServiceByCode("LMA");
		}else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
			String sessionId = razorPayrequest.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			EBSRequestError error = new EBSRequestError();
			error = ebsValidation.validateRequest(razorPayrequest,service.getCode());
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.AGENT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							TransactionError transactionError = transactionValidation
									.validateLoadMoneyTransaction(String.valueOf(razorPayrequest.getAmount()), user.getUsername(), service);
							if (transactionError.isValid()) {
								RazorPayRequest requestOfRazorPay = ebsApi.requestRazorPay(razorPayrequest, user.getUsername(), service);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Loadmoney initiated successfully.");
								result.setDetails(requestOfRazorPay);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(transactionError.getMessage());
								result.setDetails(transactionError);
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/RazorPaySuccess", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> successLoadMoneyRazorPay(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RazorPayResponse razorPayResponse, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {
		ResponseDTO result = new ResponseDTO();
		String sessionId = razorPayResponse.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user != null) {
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)
						|| user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					result = ebsApi.successRazorPay(razorPayResponse);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION);
			result.setMessage("Please, login and try again.");
			result.setDetails("Please, login and try again.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/Voucher/Process", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processLoadMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody LoadMoneyRequest dto, HttpServletRequest request, HttpServletResponse response,
			Model modelMap) {

		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			LoadMoneyError error = new LoadMoneyError();
			error = loadMoneyValidation.checkLoadMoneyError(dto);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.AGENT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode("LMC");
							if (service != null) {
								Voucher voucher = transactionApi.getVoucher(dto.getVoucherNumber());
								if (voucher != null) {
									TransactionError transactionError = transactionValidation.validateLoadMoneyTransaction(voucher.getAmount() + "", user.getUsername(),
													service);
									if (transactionError.isValid()) {
										User u = userApi.findByUserName(user.getUsername());
										ResponseDTO responseDTO = transactionApi.loadMoneyProcess(dto, u, service);
										result.setStatus(responseDTO.getStatus());
										result.setMessage(responseDTO.getMessage());
										result.setDetails(responseDTO.getDetails());
										result.setCode(responseDTO.getCode());
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage("Failed. Please try again.");
										result.setDetails(transactionError);
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Invalid voucher No");
									result.setDetails("Invalid voucher No");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Currently Voucher Service is down");
								result.setDetails("Currently Voucher Service is down");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
}
