package com.payqwikapp.controller.api.v1;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.error.MRegistrationError;
import com.payqwikapp.model.merchant.MerchantKycDTO;
import com.payqwikapp.model.merchant.MerchantRegisterDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.MRegistrationValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/MRegister")
public class MRegistrationController {

    private final MRegistrationValidation mRegistrationValidation;
    private final IUserApi userApi;


    public MRegistrationController(MRegistrationValidation mRegistrationValidation,IUserApi userApi) {
        this.mRegistrationValidation = mRegistrationValidation;
        this.userApi = userApi;
    }

    @RequestMapping(value="/VerifyKYCRequestGET",method= RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO> verifyKYC(@PathVariable(value = "role") String role, @PathVariable(value="device") String device, @PathVariable(value="language") String language, @RequestHeader(value="hash",required=false) String hash, HttpServletRequest request, HttpServletResponse response){
        ResponseDTO result = new ResponseDTO();
        MerchantRegisterDTO dto = new MerchantRegisterDTO();
        result.setDetails(dto);
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }


    @RequestMapping(value="/VerifyKYCRequest",consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE,method= RequestMethod.POST)
    ResponseEntity<ResponseDTO> verifyKYC(@PathVariable(value = "role") String role, @PathVariable(value="device") String device, @PathVariable(value="language") String language,@RequestBody MerchantRegisterDTO dto, @RequestHeader(value="hash",required=false) String hash, HttpServletRequest request, HttpServletResponse response){
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
        if (isValidHash) {
            if (role.equalsIgnoreCase("Merchant")) {
                MerchantKycDTO kyc = dto.getKycDTO();
                if(kyc != null){
                    if(kyc.isVbankCustomer()) {
                        User user = userApi.findByUserName(kyc.getMobileNumber());
                        if((user == null) || (user != null && user.getMobileStatus().equals(Status.Inactive))) {
                            MRegistrationError error = mRegistrationValidation.validateKYCRequest(kyc);
                            if (error.isValid()) {
                                 result = userApi.verifyKYC(true,dto);
                            } else {
                                result.setStatus(ResponseStatus.BAD_REQUEST);
                                result.setMessage("Invalid Input");
                                result.setError(error);
                                result.setDetails(dto);
                            }
                        }else {
                                result.setStatus(ResponseStatus.FAILURE);
                                result.setMessage("Not registered as Vijaya Bank Merchant");
                                result.setDetails(dto);
                        }
                    }else {
                        result.setStatus(ResponseStatus.NEW_REGISTRATION);
                        result.setMessage("Enter Other Details");
                        result.setDetails(dto);
                    }
                }else {
                    result.setStatus(ResponseStatus.FAILURE);
                    result.setMessage("Enter Vijaya Bank Details");
                    result.setDetails(dto);
                }

            } else {
                result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
                result.setMessage("Failed, Unauthorized user.");
                result.setDetails(dto);
            }
        } else {
            result.setStatus(ResponseStatus.INVALID_HASH);
            result.setMessage("Failed, Please try again later.");
            result.setDetails(dto);
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }

    @RequestMapping(value="/ValidateOTP",consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE,method= RequestMethod.POST)
    ResponseEntity<ResponseDTO> validateOTP(@PathVariable(value = "role") String role, @PathVariable(value="device") String device, @PathVariable(value="language") String language, @RequestBody MerchantRegisterDTO dto, @RequestHeader(value="hash",required=false) String hash, HttpServletRequest request, HttpServletResponse response){
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
        if (isValidHash) {
            if (role.equalsIgnoreCase("Merchant")) {
                MerchantKycDTO kyc = dto.getKycDTO();
                if(kyc != null){
                    if(kyc.isVbankCustomer()) {
                        User user = userApi.findByUserName(kyc.getMobileNumber());
                        if((user == null) || (user != null && user.getMobileStatus().equals(Status.Inactive))) {
                            MRegistrationError error = mRegistrationValidation.validateOTP(kyc);
                            if (error.isValid()) {
                                result = userApi.verifyKYC(false,dto);
                            } else {
                                result.setStatus(ResponseStatus.BAD_REQUEST);
                                result.setMessage("Invalid Input");
                                result.setError(error);
                                result.setDetails(dto);
                            }
                        }else {
                            result.setStatus(ResponseStatus.FAILURE);
                            result.setMessage("User already exists");
                            result.setDetails(dto);
                        }
                    }else {
                        result.setStatus(ResponseStatus.SUCCESS);
                        result.setMessage("Enter Other Details");
                        result.setDetails(dto);
                    }
                }else {
                    result.setStatus(ResponseStatus.FAILURE);
                    result.setMessage("Enter Vijaya Bank Details");
                    result.setDetails(dto);
                }

            } else {
                result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
                result.setMessage("Failed, Unauthorized user.");
                result.setDetails(dto);
            }
        } else {
            result.setStatus(ResponseStatus.INVALID_HASH);
            result.setMessage("Failed, Please try again later.");
            result.setDetails(dto);
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }


    @RequestMapping(value="/GeneratePassword",consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE,method= RequestMethod.POST)
    ResponseEntity<ResponseDTO> generatePassword(@PathVariable(value = "role") String role, @PathVariable(value="device") String device, @PathVariable(value="language") String language, @RequestBody MerchantRegisterDTO dto, @RequestHeader(value="hash",required=false) String hash, HttpServletRequest request, HttpServletResponse response){
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
        if (isValidHash) {
            if (role.equalsIgnoreCase("Merchant")) {

                        User user = userApi.findByUserName(dto.getMobileNumber());
                        if(user != null) {
                            MRegistrationError error = mRegistrationValidation.validateGeneratePassword(dto);
                            if (error.isValid()) {
                                result = userApi.createMerchantPassword(dto);
                            } else {
                                result.setStatus(ResponseStatus.BAD_REQUEST);
                                result.setMessage("Invalid Input");
                                result.setError(error);
                                result.setDetails(dto);
                            }
                        }else {
                            result.setStatus(ResponseStatus.FAILURE);
                            result.setMessage("User Not Available");
                            result.setDetails(dto);
                        }
            } else {
                result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
                result.setMessage("Failed, Unauthorized user.");
                result.setDetails(dto);
            }
        } else {
            result.setStatus(ResponseStatus.INVALID_HASH);
            result.setMessage("Failed, Please try again later.");
            result.setDetails(dto);
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }


}
