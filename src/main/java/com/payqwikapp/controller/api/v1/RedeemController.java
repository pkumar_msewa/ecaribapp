package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.repositories.PQServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IPromoCodeApi;
import com.payqwikapp.api.IRedeemCodeApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PromoCode;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.PromoTransactionDTO;
import com.payqwikapp.model.RedeemDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class RedeemController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final IPromoCodeApi promoCodeApi;
	private final IRedeemCodeApi redeemCodeApi;
	private final PQServiceRepository pqServiceRepository;

	public RedeemController(UserSessionRepository userSessionRepository, IUserApi userApi,
			PersistingSessionRegistry persistingSessionRegistry, IPromoCodeApi promoCodeApi,
			IRedeemCodeApi redeemCodeApi,PQServiceRepository pqServiceRepository) {
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.promoCodeApi = promoCodeApi;
		this.redeemCodeApi = redeemCodeApi;
		this.pqServiceRepository = pqServiceRepository;

	}

	@RequestMapping(value = "/RedeemProcess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> redeeem(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RedeemDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode("PPC");
						AuthenticationError authError = userApi.validateUserRequest(dto.toString(),userSession.getUser(),service,timeStamp);
								if(authError.isSuccess()) {
									persistingSessionRegistry.refreshLastRequest(sessionId);
									User u = userApi.findByUserName(user.getUsername());
									result = promoCodeApi.process(dto, u);
									double balance = userApi.getWalletBalance(u);
									result.setBalance(balance);
									userApi.updateRequestLog(u,timeStamp);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(authError.getMessage());
									result.setDetails(authError.getMessage());
								}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}
}