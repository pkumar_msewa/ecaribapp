package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.LocationDetails;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQNotifications;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.TreatCardDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.entity.VBankAccountDetail;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.AadharDTO;
import com.payqwikapp.model.AccountDTO;
import com.payqwikapp.model.BasicDTO;
import com.payqwikapp.model.ChangeEmailDTO;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.DateDTO;
import com.payqwikapp.model.FavouriteDTO;
import com.payqwikapp.model.FavouriteRequest;
import com.payqwikapp.model.ImageDTO;
import com.payqwikapp.model.InviteFriendsDTO;
import com.payqwikapp.model.KYCResponse;
import com.payqwikapp.model.KycDTO;
import com.payqwikapp.model.LimitDTO;
import com.payqwikapp.model.LocationDTO;
import com.payqwikapp.model.LoginDTO;
import com.payqwikapp.model.NotificationsDTO;
import com.payqwikapp.model.OnePayRequest;
import com.payqwikapp.model.OnePayResponse;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.SharePointDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionDTO;
import com.payqwikapp.model.UserBalanceDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserMaxLimitDto;
import com.payqwikapp.model.VerifyEmailDTO;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.MobileTopupError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.error.VerifyEmailError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQNotificationRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.TreatCardDetailsRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.CommonValidation;
import com.payqwikapp.validation.RegisterValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class UserController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
	private final SimpleDateFormat dateFormated = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final RegisterValidation registerValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository serviceRepository;
	private final PQNotificationRepository pqNotificationRepository;
	private final IRefernEarnLogsApi refernEarnLogsApi;
	private final UserRepository userRepository;
	private final TreatCardDetailsRepository treatCardDetailsRepository;

	public UserController(IUserApi userApi, ITransactionApi transactionApi, RegisterValidation registerValidation,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			TransactionValidation transactionValidation, PQServiceRepository serviceRepository,PQNotificationRepository pqNotificationRepository,IRefernEarnLogsApi refernEarnLogsApi,UserRepository userRepository,TreatCardDetailsRepository treatCardDetailsRepository) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.registerValidation = registerValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionValidation = transactionValidation;
		this.serviceRepository = serviceRepository;
		this.pqNotificationRepository=pqNotificationRepository;
		this.refernEarnLogsApi=refernEarnLogsApi;
		this.userRepository = userRepository;
		this.treatCardDetailsRepository = treatCardDetailsRepository;
	}

	@RequestMapping(value = "/AccountUpdateRequest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> requestUpdateForKYC(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody KycDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					// TODO check if it is already kyc user
					User u = userApi.findByUserName(user.getUsername());
					PQAccountDetail account = u.getAccountDetail();
					if (account != null) {
						String code = account.getAccountType().getCode();
						if (code.equalsIgnoreCase("NONKYC")) {
							// TODO check if it already linked with bank account
							// of vijaya bank
							VBankAccountDetail vbankAccount = account.getvBankAccount();
							if (vbankAccount == null) {
								// TODO check the entered account is not being
								// used by anyone
								String accountNumber = SecurityUtil.sha1(dto.getAccountNumber());
								boolean exists = userApi.containsAccountAndMobile(accountNumber, dto.getMobileNumber());
								if (!exists) {
									// TODO validate account and mobile number
									// with KYC API
									KYCResponse kycResponse = userApi.verifyByKycApi(dto);
									boolean isValidKYC = kycResponse.isValid();
									if (isValidKYC) {
										// TODO store values in table
										vbankAccount = new VBankAccountDetail();
										userApi.updateVBankAccountDetails(kycResponse, vbankAccount, u);
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage("OTP sent to " + dto.getMobileNumber());
									} else {
										result.setStatus(ResponseStatus.BAD_REQUEST);
										result.setMessage("Account not available in Vijaya Bank");
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Your account number is already in use");
								}
							} else {
								Status status = vbankAccount.getStatus();
								if (status == Status.Inactive) {
									KYCResponse kycResponse = userApi.verifyByKycApi(dto);
									boolean isValidKYC = kycResponse.isValid();
									if (isValidKYC) {
										userApi.updateVBankAccountDetails(kycResponse, vbankAccount, u);
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage("OTP sent to " + dto.getMobileNumber());
									} else {
										result.setStatus(ResponseStatus.BAD_REQUEST);
										result.setMessage("Account not available in Vijaya Bank");
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Account Already linked");
								}
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("You're already a KYC User");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Account Not Exists");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/AccountUpdate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> processUpdateForKYC(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody KycDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					// TODO check whether OTP is same as that of mobileToken of
					// current user
					User u = userApi.findByUserName(user.getUsername());
					boolean isVerified = userApi.isVerified(dto, u);
					if (isVerified) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Your Account is Verified Successfully");
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("OTP doesn't match");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/Validate/Transaction", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> validateTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		System.err.println(" inside Validate/Transaction controller :::::::::: ");
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					dto.setSenderUsername(user.getUsername());
					String transactionRefNo = dto.getTransactionRefNo();
					boolean isCredit = transactionRefNo.equalsIgnoreCase("C");
					boolean isMerchantPay = transactionRefNo.equalsIgnoreCase("M");
					TransactionError error = null;
					if (isCredit) {
						error = transactionValidation.validateGenericTransactionForAPI(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(error.getMessage());
						// result.setBalance(error.getSplitAmount());
						result.setDetails(error);
					} else if (isMerchantPay) {

					} else {
						error = transactionValidation.validateGenericTransaction(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(error.getMessage());
						result.setBalance(error.getSplitAmount());
						result.setCode(error.getCode());
						result.setDetails(error);
					}

				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else if (role.equalsIgnoreCase("Merchant")) {
			System.out.println("its a merchant");
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
 			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.LOCKED)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					System.out.println("m here::::::::::::");

					persistingSessionRegistry.refreshLastRequest(sessionId);
					dto.setSenderUsername(user.getUsername());
					String transactionRefNo = dto.getTransactionRefNo();
					boolean isCredit = transactionRefNo.equalsIgnoreCase("C");
					boolean isMerchantPay = transactionRefNo.equalsIgnoreCase("M");
					TransactionError error = null;
					if (isCredit) {
						System.out.println("credit is true::::" + isCredit);
						error = transactionValidation.validateGenericTransactionForAPI(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(error.getMessage());
						result.setDetails(error);
					} else if (isMerchantPay) {

					} else {
						error = transactionValidation.validateGenericTransaction(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(error.getMessage());
						result.setDetails(error);
					}

				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else if (role.equalsIgnoreCase("Agent")) {
			System.out.println("its a Agent");
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					System.out.println("m here::::::::::::");

					persistingSessionRegistry.refreshLastRequest(sessionId);
					dto.setSenderUsername(user.getUsername());
					String transactionRefNo = dto.getTransactionRefNo();
					boolean isCredit = transactionRefNo.equalsIgnoreCase("C");
					boolean isMerchantPay = transactionRefNo.equalsIgnoreCase("M");
					TransactionError error = null;
					if (isCredit) {
						System.out.println("credit is true::::" + isCredit);
						error = transactionValidation.validateGenericTransactionForAPI(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(error.getMessage());
						result.setDetails(error);
					} else if (isMerchantPay) {

					} else {
						error = transactionValidation.validateGenericTransaction(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(error.getMessage());
						result.setDetails(error);
					}

				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/SharePoints", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> sharePoints(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SharePointDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {

		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					dto.setSenderUsername(user.getUsername());
					PQService service = serviceRepository.findServiceByCode("SPU");
					TransactionError error = transactionValidation.validateSharePoints(dto);
					if (error.isValid()) {
						result.setStatus(ResponseStatus.SUCCESS);
						String transactionRefNo = "SP" + System.currentTimeMillis();
						String description = dto.getPoints() + " shared to " + dto.getReceiverUsername() + " from "
								+ dto.getSenderUsername();
						transactionApi.initiateSharePoints(dto.getPoints(), description, transactionRefNo,
								dto.getSenderUsername(), dto.getReceiverUsername());
						transactionApi.successSharePoints(transactionRefNo);
						result.setMessage("Points Shared Successfully");
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Transaction Error");
						result.setDetails(error);
					}

				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/EditProfile/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processEditedDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)
						|| user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					RegisterError error = new RegisterError();
					error = registerValidation.validateEditUser(dto);
					if (error.isValid()) {
						dto.setUsername(user.getUsername());
						userApi.editUser(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Profile has been edited successfully.");
						result.setDetails("Profile has been edited successfully.");
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Failed, invalid request.");
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else if (role.equalsIgnoreCase("Agent")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.AGENT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					RegisterError error = new RegisterError();
					error = registerValidation.validateEditUser(dto);
					if (error.isValid()) {
						dto.setUsername(user.getUsername());
						userApi.editUser(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Profile has been edited successfully.");
						result.setDetails("Profile has been edited successfully.");
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Failed, invalid request.");
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/EditName/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processEditName(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					RegisterError error = new RegisterError();
					error = registerValidation.checkNameError(dto);
					if (error.isValid()) {
						dto.setUsername(user.getUsername());
						userApi.editName(dto);
						User userDTO=userApi.findByUserName(dto.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Name has been edited successfully.");
						result.setDetails(userDTO.getUserDetail().getFirstName());
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Failed, invalid request.");
						result.setDetails(error.getFirstName());
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdatePassword/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> processNewPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response, @RequestBody ChangePasswordDTO change) {
		ResponseDTO result = new ResponseDTO();
		ChangePasswordError error = registerValidation.validateChangePasswordDTO(change);
		if (error.isValid() == false) {
			result.setMessage(error.getPassword());
			result.setStatus(ResponseStatus.FAILURE);
			result.setDetails(error);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		String sessionId = change.getSessionId();
		if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
			if (error.isValid()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)
							|| user.getAuthority().contains(Authorities.AGENT)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						ResponseDTO resp=null;
						try {
							resp=userApi.setPasswordHistory(change);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(resp.getCode().equalsIgnoreCase("S00")||resp.getCode().equalsIgnoreCase("S01")||resp.getCode().equalsIgnoreCase("S02")){
						userApi.renewPasswordFromAccount(change);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Password Updated Successfully.");
						result.setDetails("Password Updated Successfully.");
					}else{
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(resp.getMessage());
						result.setDetails(resp.getMessage());
					}
						} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UploadPicture/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processProfilePicture(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ImageDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
			UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)
						|| user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
					try {
						userApi.saveImage(userSession.getUser(), dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Profile picture updated successfully.");
					} catch (IllegalStateException e) {
						result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
						result.setMessage("Failed, Please try again later.");
						result.setDetails("Failed, Please try again later.");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Failed, Please try again later.");
				result.setDetails("Failed, Please try again later.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/VerifyEmail/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyEmailDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			VerifyEmailError error = new VerifyEmailError();
			error = registerValidation.checkMailError(dto);
			if (error.isValid()) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
						if (userApi.activateEmail(dto.getKey())) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Email is verified successfully.");
							result.setDetails("Email is verified successfully.");
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed to verify email.");
							result.setDetails("Failed to verify email.");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Failed, Please try again later.");
					result.setDetails("Failed, Please try again later.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetUserDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			try{
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						User activeUser = userApi.findByUserName(user.getUsername());
						RefernEarnlogs lkjs=refernEarnLogsApi.getlogs(activeUser);
						if(lkjs!=null && lkjs.isHasEarned()){
							result.setHasRefer(true);
						}else {
						result.setHasRefer(false);
						}
						TreatCardDetails details=treatCardDetailsRepository.findByUser(activeUser);
						if(details!=null){
							result.setExistTreatCard(true);
							result.setMembershipCode(details.getMembershipCode());
							result.setMemberShipCodeExpire(details.getMembershipCodeExpire());
						}else{
							result.setExistTreatCard(false);
						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User details received.");
						detail.put("userDetail", user);
						PQAccountDetail account = activeUser.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
						detail.put("accountDetail", account);
						detail.put("nikiOffer", "new");
						/*List<OffersDTO> listOffers=userApi.getAllOffers();
                        detail.put("OfferList", listOffers);*/
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else if (role.equalsIgnoreCase("Agent")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						User activeUser = userApi.findByUserName(user.getUsername());

						String count = userApi.getcountofagentregister(activeUser);
						double totaldebitamount = transactionApi.getTotalTransactionsOfAgent(activeUser.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User details received.");
						PQAccountDetail account = activeUser.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
						detail.put("totaldebittransaction", totaldebitamount);
						detail.put("numberofregisteruser", count);
						detail.put("userDetail", user);
						detail.put("accountDetail", account);
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized Agent.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else if (role.equalsIgnoreCase("Admin")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						User activeUser = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Admin details received.");
						detail.put("userDetail", user);
						detail.put("accountDetail", activeUser.getAccountDetail());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						User activeUser = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Merchant details received.");
						PQAccountDetail account = activeUser.getAccountDetail();
						account.setBalance(Double.parseDouble(String.format("%.2f",account.getBalance())));
						JSONObject obj = new JSONObject();
						obj.put("id", activeUser.getId());
						obj.put("name", activeUser.getUserDetail().getFirstName());
						obj.put("email", activeUser.getUsername());
						obj.put("contactNo", activeUser.getUserDetail().getContactNo());
						String qrCode =SecurityUtil.encryptString(obj.toString());
						activeUser.setQrCode(qrCode);
						userRepository.save(activeUser);
						detail.put("qrCode", activeUser.getQrCode());
						detail.put("userDetail", user);
						detail.put("accountDetail", account);
						detail.put("merchantDetail", activeUser.getMerchantDetails());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			} else if (role.equalsIgnoreCase("Donatee")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.DONATEE)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Map<String, Object> detail = new HashMap<String, Object>();
						User activeUser = userApi.findByUserName(user.getUsername());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Merchant details received.");
						detail.put("userDetail", user);
						detail.put("accountDetail", activeUser.getAccountDetail());
						// detail.put("merchantDetail",activeUser.getMerchantDetails());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			}else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				result.setDetails("Not a valid role");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Failed, Please try again later.");
		}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetInfo/{infoName}", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBasicDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable(value = "infoName") String info, @RequestBody SessionDTO session,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request, ModelMap modelMap,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						switch (info.toUpperCase()) {

						case "BASIC":
							BasicDTO basicDetails = ConvertUtil.convertToBasic(u);
							result.setMessage("User details");
							result.setDetails(basicDetails);
							break;
						case "ACCOUNT":
							AccountDTO accountDetails = ConvertUtil.convertToAccount(u.getAccountDetail());
							result.setMessage("Account Details");
							result.setDetails(accountDetails);
							break;
						case "BALANCE":
							result.setMessage("Balance Received");
							double walletBalance = userApi.getWalletBalance(u);
							result.setDetails(Double.parseDouble(String.format("%.2f", walletBalance)));
							break;
						case "LOCATION":
							result.setMessage("Location Details");
							LocationDetails locationDetails = u.getUserDetail().getLocation();
							if (locationDetails != null) {
								LocationDTO location = ConvertUtil.convertToLocation(locationDetails);
								result.setDetails(location);
							}
							break;
						case "IMAGE":
							result.setMessage("Profile Image");
							result.setDetails(u.getUserDetail().getImage());
							break;
						default:
							LimitDTO limits = transactionValidation.getLimitValues(u);
							result.setMessage("Limits Received");
							result.setDetails(limits);
							break;
						}
						result.setStatus(ResponseStatus.SUCCESS);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				result.setDetails("Not a valid role");
			}

		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			result.setDetails("Not a valid hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Invite/Email", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> inviteByEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody InviteFriendsDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				MobileTopupError error = new MobileTopupError();
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						userApi.inviteByEmailAddress("Welcome To VPayQwik", MailTemplate.INVITE_FRIEND, dto.getEmail());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Invitation sent to user");

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("User | Invite Friend");
						result.setDetails("Permission Not Granted");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("User | Invite Friend");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("User | Invite Friend");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Invite/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> inviteByMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody InviteFriendsDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User friend = userApi.findByUserName(dto.getMobileNo());
						if (friend == null) {
							User sender = userApi.findByUserName(user.getUsername());
							String message = String.format(
									"Dear %s \n You're invited by %s Please download VPayQwik app from play store at http://goo.gl/07Kf4u or from iTunes at https://goo.gl/Ou5ZRr and enjoy our services.\n Thank you,\n VPayQwik",
									dto.getMobileNo(), friend.getUsername());
							boolean isSent = userApi.inviteByMobile(dto.getMobileNo(), message, sender);
							if (isSent) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Invitation Sent Successfully");
								result.setDetails("Invitation Sent Successfully");
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage("User Already Invited");
								result.setDetails("User Already Invited");
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("User Already Exists");
							result.setDetails("User Already Exists");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetReceipts", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserReceipts(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());

					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)
								|| user.getAuthority().contains(Authorities.AGENT)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.MERCHANT)&&user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUser(pageable,
									user.getUsername());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("User receipts");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user receipts");
							result.setDetails("Failed to get user receipts");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/GetReceiptsFilter", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserReceiptsFilter(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<PQTransaction> pg = transactionApi.getTotalTransactionsOfUserFilter(pageable,dateFormat.parse(from),dateFormat.parse(to),user.getUsername());
							if(pg.getSize()==0){
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("No Transaction Found Between this"+dto.getFromDate()+"to"+dto.getToDate());
								result.setDetails("No Transaction Found Between this"+dto.getFromDate()+"to"+dto.getToDate());
							
							} else {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("User receipts from"+ dto.getFromDate()+" to " +dto.getToDate());
								result.setDetails(pg);
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user receipts");
							result.setDetails("Failed to get user receipts");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/STransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getSuccessfulTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							List<PQTransaction> transactionList = new ArrayList<>();
							transactionList = transactionApi.getTotalSuccessfulTransactionsOfUser(user.getUsername());
							List<FavouriteDTO> favouriteDTOs = ConvertUtil.convertTransactionList(transactionList);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("User Transactions");
							result.setDetails(favouriteDTOs);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user receipts");
							result.setDetails("Failed to get user receipts");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateFavourite", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> setTransactionsAsFavourite(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FavouriteRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							long rowsUpdated = transactionApi.updateFavouriteTransaction(dto.getTransactionRefNo(),
									dto.isFavourite());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Successfully updated");
							result.setDetails("Rows updated " + rowsUpdated);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user receipts");
							result.setDetails("Failed to get user receipts");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ReSendEmailOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processResendEmailOTP(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				MobileTopupError error = new MobileTopupError();
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)) {
						User userEmail = userApi.findByUserName(user.getUsername());
						userApi.reSendEmailOTP(userEmail);
						Map<String, Object> detail = new HashMap<String, Object>();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User details received.");
						detail.put("userDetail", user);
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangeEmail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processChangeEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangeEmailDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		System.err.println("session ID :: " + session.getSessionId());
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				MobileTopupError error = new MobileTopupError();
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)) {
						User userEmail = userApi.findByUserName(user.getUsername());
						boolean isValidEmail = CommonValidation.isValidMail(session.getEmail());
						if (isValidEmail) {
							userApi.changeEmail(userEmail, session.getEmail());
							Map<String, Object> detail = new HashMap<String, Object>();
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Email ID updated Successfully");
							UserDTO updatedUser = userApi.getUserById(userSession.getUser().getId());
							detail.put("userDetail", updatedUser);
							result.setDetails(detail);
						}
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Email, Invaild Email ID");

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetTransactionDTO", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<OnePayResponse> processOnePayRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody OnePayRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		OnePayResponse result = new OnePayResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)) {
						User u = userApi.findByUserName(user.getUsername());
						result = transactionApi.getTransaction(dto.getTransactionRefNo(), u);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<OnePayResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Validate/MTransaction", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> validateMerchantTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {

		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("Merchant")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.MERCHANT)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					dto.setSenderUsername(user.getUsername());
					String transactionRefNo = dto.getTransactionRefNo();
					boolean isCredit = transactionRefNo.equalsIgnoreCase("C");
					boolean isMerchantPay = transactionRefNo.equalsIgnoreCase("M");
					TransactionError error = null;
					if (isCredit) {
						error = transactionValidation.validateGenericTransactionForAPI(dto);
					} else if (isMerchantPay) {

					} else {
						error = transactionValidation.validateGenericTransaction(dto);
					}
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage(error.getMessage());
					result.setDetails(error);
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else if (role.equalsIgnoreCase("Merchant")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.LOCKED)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					dto.setSenderUsername(user.getUsername());
					String transactionRefNo = dto.getTransactionRefNo();
					boolean isCredit = transactionRefNo.equalsIgnoreCase("C");
					boolean isMerchantPay = transactionRefNo.equalsIgnoreCase("M");
					TransactionError error = null;
					if (isCredit) {
						error = transactionValidation.validateGenericTransactionForAPI(dto);
					} else if (isMerchantPay) {

					} else {
						error = transactionValidation.validateGenericTransaction(dto);
					}
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage(error.getMessage());
					result.setDetails(error);
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/FilteredBalanceOfUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserBalanceDTO> getBankTransferRequestsFiltered(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		UserBalanceDTO result = new UserBalanceDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = u.getAccountDetail();
						if (account != null) {
						List<UserBalanceDTO> userDebitDTOs=userApi.getExpenseOfUser(dateFormat.parse(from),dateFormat.parse(to),account);
						List<UserBalanceDTO> userBalanceDTOs=userApi.getBalanceOfUser(dateFormat.parse(from),dateFormat.parse(to),account);
						List<UserBalanceDTO> userCreditDTOs=userApi.getExpenseOfUserForCredit(dateFormat.parse(from),dateFormat.parse(to),account);
						List<UserBalanceDTO> userDebitTopUpDTOs=userApi.getExpenseOfTopUpUser(dateFormat.parse(from),dateFormat.parse(to),account);
						Map<String, Object> details = new HashMap<>();
						details.put("credit", userCreditDTOs);
						details.put("debit", userDebitDTOs);
						details.put("balance", userBalanceDTOs);
						details.put("DebitTopUp", userDebitTopUpDTOs);
						
						result.setMessage("receipt list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(details);
						return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/TotalDebitAndCreditOfUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserBalanceDTO> getFilteredBalance(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		UserBalanceDTO result = new UserBalanceDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = u.getAccountDetail();
						if (account != null) {
						result.setMessage("receipt list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(userApi.getUserFilteredTransaction(account));
						return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/FilteredBalanceOfUserDateWise", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserBalanceDTO> getTotalDebitAndCreditFiltered(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		UserBalanceDTO result = new UserBalanceDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = u.getAccountDetail();
						if (account != null) {
						result.setMessage("receipt list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(userApi.getUserFilteredTransactionByDate(dateFormat.parse(from),dateFormat.parse(to),account));
						return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<UserBalanceDTO>(result, HttpStatus.OK);
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetNotifications", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getNotifications(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {

		List<NotificationsDTO> list = new ArrayList<>();
		ResponseDTO resp = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					User u = userApi.findByUserName(user.getUsername());
					if (u != null) {
						List<PQNotifications> noti = pqNotificationRepository.getAllNotifications(u);
						if (noti != null && !noti.isEmpty()) {
							for (PQNotifications pqNotifications : noti) {
								NotificationsDTO result = new NotificationsDTO();
								result.setMessage(pqNotifications.getMessage());
								result.setDate(dateFormated.format(pqNotifications.getCreated()));
								list.add(result);

							}
							resp.setCode("S00");
							resp.setStatus(ResponseStatus.SUCCESS);
							resp.setValid(true);
							resp.setDetails(list);
						} else {
							resp.setCode("F00");
							resp.setStatus(ResponseStatus.FAILURE);
							resp.setValid(false);
							resp.setMessage("No notifications received");
						}
					} else {
						resp.setCode("F00");
						resp.setStatus(ResponseStatus.SUCCESS);
						resp.setValid(true);
						resp.setMessage("USER  Doesn't Exist");
					}

				} else {
					resp.setCode("F00");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setValid(false);
					resp.setMessage("UnAunthenticated User");
				}

			} else {
				resp.setCode("F00");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setValid(false);
				resp.setMessage("please login and try again..");
			}
		} else {
			resp.setCode("F00");
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setValid(false);
			resp.setMessage("UnAuthorised User..");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	
	
	@RequestMapping(value = "/SaveAadhar", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveAadhar(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AadharDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws JSONException {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
						dto.setUsername(user.getUsername());
					 result=userApi.saveAadhar(dto);
					 Map<String, Object> details = new HashMap<String, Object>();
					 details.put("name", dto.getCustomerName());
					 details.put("address", dto.getCustomerAddress());
					 result.setDetails(details);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/GetUserOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserOTP(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					User u=userApi.findByUserName(user.getUsername());
						userApi.otpForAadhar(u);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("OTP sent to ::" + u.getUsername());
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/GetUserOTPValidate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserOTPValidate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AadharDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					if (verifyUserMobileToken(dto.getUserOTP(), user.getUsername())) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Your OTP is Successfully Verified ");
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Invalid OTP");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
		
		private boolean verifyUserMobileToken(String key, String mobileNumber) {
			if (userApi.checkMobileTokenAadhar(key, mobileNumber)) {
				return true;
			} else {
				return false;
			}

		}
		@RequestMapping(value = "/updateUserMaxLimit", method = RequestMethod.POST, produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> updateUserMaxLimit(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody UserMaxLimitDto dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response) {
			ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						if(user.getMobileToken()!=null && dto.getMobileToken()!=null && dto.getMobileToken().equals(user.getMobileToken())) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							result= userApi.updateUserMaxLimit(dto,userSession.getUser());
						}else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("Otp mismatch,please retry.");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
			}
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/sendMaxLimitOtp", method = RequestMethod.POST, produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> sendMaxLimitOtp(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody UserMaxLimitDto dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response) {
			ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result= userApi.sendMaxLimitOtp(userSession.getUser());
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
			}
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/getUserMaxLimit", method = RequestMethod.POST, produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> getUserMaxLimit(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody UserMaxLimitDto dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response) {
			ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result= userApi.getUserMaxLimit(dto,userSession.getUser());
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
			}
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}	
		
		@RequestMapping(value = "/validatePassword", method = RequestMethod.POST, produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> validateUserPassword(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody LoginDTO dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response) {
			ResponseDTO result = new ResponseDTO();
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						boolean isMatched= userApi.validateUserPassword(dto,userSession.getUser().getPassword());
						if(isMatched) {
							result.setStatus(ResponseStatus.SUCCESS);
						}else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Your password is invalid.Please try again.");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
			}
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}	
		
}