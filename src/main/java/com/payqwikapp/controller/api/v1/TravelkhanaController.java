
package com.payqwikapp.controller.api.v1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ITKPlaceOrderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TrainList;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.DateDTO;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.TKMyOrderDTO;
import com.payqwikapp.model.TKOrderDTO;
import com.payqwikapp.model.TKOrderDetailDTO;
import com.payqwikapp.model.TKPlaceOrderRequest;
import com.payqwikapp.model.TransactionCommissionListDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Travelkhana")
public class TravelkhanaController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private ITKPlaceOrderApi placeorderApi;
	private IUserApi userApi;
	private PersistingSessionRegistry sessionRegistry;
	private UserSessionRepository userSessionRepository;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository pqServiceRepository;
	private final ITransactionApi transactionApi;

	public TravelkhanaController(ITKPlaceOrderApi placeorderApi, IUserApi userApi, PersistingSessionRegistry sessionRegistry,
			UserSessionRepository userSessionRepository, TransactionValidation transactionValidation,
			PQServiceRepository pqServiceRepository,ITransactionApi transactionApi) {

		this.placeorderApi = placeorderApi;
		this.userApi = userApi;
		this.sessionRegistry = sessionRegistry;
		this.userSessionRepository = userSessionRepository;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
		this.transactionApi = transactionApi;
	}
    
	
	@RequestMapping(value = "/getTrainListFromDB", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TKMyOrderDTO>getTrainList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO req, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		TKMyOrderDTO result = new TKMyOrderDTO();
		
		System.err.println(" Inside getTrainListFromDB ");
		boolean isValidHash = SecurityUtil.isHashMatches(req, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = req.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						
						User u = userApi.findByUserName(user.getUsername());
						List<TrainList> trainList  = placeorderApi.getTrainList();
							if(trainList.size()>0){
								result.setDetails(trainList);
								result.setSuccess(true);
								result.setMessage("Successfully get train list");
							}
							else{
								result.setStatus(ResponseStatus.FAILURE);
								result.setSuccess(false);
								result.setMessage("Train list not found");
							} 
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setSuccess(false);
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			result.setSuccess(false);
		}
		return new ResponseEntity<TKMyOrderDTO>(result, HttpStatus.OK);

	}
  
	
	@RequestMapping(value = "/PlaceOrderInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TKOrderDTO> tkorderInitiate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TKPlaceOrderRequest tkReq, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		TKOrderDTO result = new TKOrderDTO();
		
		System.err.println(" Inside TK Initiate ::::::::::Controller ");
		boolean isValidHash = SecurityUtil.isHashMatches(tkReq, hash);
		if (isValidHash) {
	
			if (role.equalsIgnoreCase("User")) {
				String sessionId = tkReq.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

						User u = userApi.findByUserName(user.getUsername());
						System.err.println("Sender User ::::::::::::::Startup util "+user.getUsername());
					//	System.err.println("Receiver User :: " + receiver.getUsername());
				
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.TRAVELKAHANA_SERVICECODE);
						System.err.println("Service code ;;------------------------------------- " + service.getCode());
						TransactionError transactionError = transactionValidation.validatePlaceOrder(tkReq.getTotalCustomerPayable(),
								user.getUsername(), service);
						System.out.println("transactionError.getMessage()="+transactionError.getMessage());
						if (transactionError.isValid()) {
							String transactionRefNo = placeorderApi.tkpaymentInitiate(tkReq, u,service);
							System.out.println("transactionRefNo="+transactionRefNo);
							User sender = userApi.findByUserName(user.getUsername());
							result.setBalance(sender.getAccountDetail().getBalance());
							 
							TKOrderDTO resp = placeorderApi.tkpaymentSuccess(tkReq, user.getUsername(),service, u,transactionRefNo);
							System.out.println("resp.getMessage()="+resp.getMessage());
							System.out.println("resp.getStatus()="+resp.getStatus());
	                    		if (resp.getCode().equalsIgnoreCase("S00")){
	                    			result.setStatus(ResponseStatus.SUCCESS);
	                    			result.setCode(ResponseStatus.SUCCESS.getValue());
	                    		}
	                    		else{
	                    			result.setCode(ResponseStatus.FAILURE.getValue());
	                    			result.setStatus(ResponseStatus.FAILURE);
	                    		}
							result.setMessage(resp.getMessage());
						//	result.setTxnId(transactionRefNo);
							result.setUserOrderId(resp.getUserOrderId());
						} else {
							result.setCode(ResponseStatus.FAILURE.getValue());
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(transactionError.getMessage());
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<TKOrderDTO>(result, HttpStatus.OK);

	}

	
	@RequestMapping(value = "/MyOrder", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TKMyOrderDTO>myOrder(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO req, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		TKMyOrderDTO result = new TKMyOrderDTO();
		
		System.err.println(" Inside TK Initiate ::::::::::Controller ");
		boolean isValidHash = SecurityUtil.isHashMatches(req, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = req.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						
						User u = userApi.findByUserName(user.getUsername());
						List<TKOrderDetail> orderDetails  = placeorderApi.myorder(u);
							if(orderDetails.size()>0){
								result.setOrderDetail(orderDetails);
								result.setSuccess(true);
								result.setMessage("Successfully myOrders list");
							}
							else{
								result.setStatus(ResponseStatus.FAILURE);
								result.setSuccess(false);
								result.setMessage("Order list not found");
							} 
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setSuccess(false);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setSuccess(false);
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setSuccess(false);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			result.setSuccess(false);
		}
		return new ResponseEntity<TKMyOrderDTO>(result, HttpStatus.OK);

	}
  
	@RequestMapping(value = "/getOrderDetailsForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBusDetailsForAdmin(@RequestBody PagingDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
			//		if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode("TKHANA");
						List<PQTransaction> commissionList =  transactionApi.getCommissionOfMerchantService(service);
						List<TransactionCommissionListDTO> commissions = new ArrayList<>();
						for(PQTransaction c : commissionList) {
							TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
							commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
							commissions.add(commission);
					    }
						List<TKOrderDetail> list  = placeorderApi.getOrderDetailsForAdmin();
						if(list!=null && commissionList !=null){
							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("userList", list);
							detail.put("commissionList", commissions);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setValid(true);
							result.setMessage("Order Details received");
							result.setDetails(detail);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("Bus details not found");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 
	
	/*@RequestMapping(value = "/getOrderDetailsForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBusDetailsForAdmin(@RequestBody PagingDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
			//		if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					 if(dto.isPageable()){
						Sort sort = new Sort(Sort.Direction.DESC, "id");
						Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
						PQService service = pqServiceRepository.findServiceByCode("TKHANA");
						Page<PQTransaction> commissionList =  transactionApi.getCommissionOfTKService(pageable,service);
						List<TransactionCommissionListDTO> commissions = new ArrayList<>();
						for(PQTransaction c : commissionList) {
							TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
							commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
							commissions.add(commission);
					      }
						Page<TKOrderDetail> list  = placeorderApi.getOrderDetailsForAdmin(pageable);
						if(list!=null && commissionList !=null){
							Map<String, Object> detail = new HashMap<String, Object>();
							ObjectMapper mapper = new ObjectMapper();
							
							
							System.out.println("list.getContent()=="+getOrderDetails(list.getContent()));
							System.out.println("commissions==="+commissions);
							List<TKOrderDetailDTO> orderList = getOrderDetails(list.getContent());
				//			orderList=mapper.readValues(orderList,TKOrderDetailDTO.class);
							
							System.out.println("orderList=="+orderList);
							detail.put("userList", list.getContent());
							detail.put("commissionList", commissions);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setValid(true);
							result.setMessage("Order Details received");
							result.setDetails(detail);
							result.setCount(list.getTotalPages());
					//		result.setDetails2(commissions);
							System.out.println("result=="+result);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
							result.setCode("F00");
							result.setMessage("Bus details not found");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					   }
				  }	
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}*/
	
		private List<TKOrderDetailDTO> getOrderDetails(List<TKOrderDetail> list ){
			 List<TKOrderDetailDTO> orderList = new ArrayList<>();
			 for(int i=0;i<list.size();i++){
				 TKOrderDetailDTO orderDetail = new TKOrderDetailDTO();
				 orderDetail.setCoach(list.get(i).getCoach());
				 orderDetail.setCod(list.get(i).getCod());
				 orderDetail.setContact_no(list.get(i).getContact_no());
			//	 orderDetail.setCreated(list.get(i).getCreated());
				 orderDetail.setCustomer_comment(list.get(i).getCustomer_comment());
				 orderDetail.setDate(list.get(i).getDate());
				 orderDetail.setMail_id(list.get(i).getMail_id());
				 orderDetail.setName(list.get(i).getName());
				 orderDetail.setOrder_outlet_id(list.get(i).getOrder_outlet_id());
				 orderDetail.setOrderStatus(list.get(i).getOrderStatus());
				 orderDetail.setPnr(list.get(i).getPnr());
				 orderDetail.setSeat(list.get(i).getSeat());
				 orderDetail.setStation_code(list.get(i).getStation_code());
				 orderDetail.setTotalCustomerPayable(list.get(i).getTotalCustomerPayable());
				 orderDetail.setTrain_number(list.get(i).getTrain_number());
				 orderDetail.setTransactionRefNo(list.get(i).getTransactionRefNo());
				 orderDetail.setTxnStatus(list.get(i).getTxnStatus());
			//	 orderDetail.setUser(list.get(i).getUser());
				 
				 orderList.add(orderDetail);
			 }
			 
			 return orderList;
		}
	
	@RequestMapping(value = "/getOrderDetailsByDateForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBusDetailsByDateForAdmin(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
			//		if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode("TKHANA");
						List<PQTransaction> commissionList =  transactionApi.getCommissionOfMerchantService(service);
						List<TransactionCommissionListDTO> commissions = new ArrayList<>();
						for(PQTransaction c : commissionList) {
							TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
							commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
							commissions.add(commission);
					      }
						String from = dto.getFromDate()+" 00:00";
						String  to = dto.getToDate()+" 23:59";
						List<TKOrderDetail> list  = placeorderApi.getOrderDetailsByDateForAdmin(dateFormat.parse(from),dateFormat.parse(to));
						if(list!=null && commissionList !=null){
							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("userList", list);
							detail.put("commissionList", commissions);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setValid(true);
							result.setMessage("Order Details received");
							result.setDetails(detail);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("Order details not found");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 
	
  /* @RequestMapping(value = "/TKOrderSuccess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TKOrderDTO> tkorderSuccess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TkPlaceOrderRequest tkReq, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		TKOrderDTO result = new TKOrderDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(tkReq, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = tkReq.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

						User u = userApi.findByUserName(user.getUsername());
    //					tkReq.setUser(u);

						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.TRAVELKAHANA_SERVICECODE);
						
						TransactionResponse transactionRefNo = placeorderApi.tkpaymentSuccess(tkReq, user.getUsername(),service, u);

						result.setBalance(u.getAccountDetail().getBalance());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Payment Travelkhana  Successfully " + user.getEmail() + " .");
						result.setDetails(transactionRefNo);
						

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<TKOrderDTO>(result, HttpStatus.OK);

	}*/

}