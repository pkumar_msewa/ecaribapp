package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.ITreatCardApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.DateDTO;
import com.payqwikapp.model.PlansDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.TreatCardPaymentResponse;
import com.payqwikapp.model.TreatCardPlansDTO;
import com.payqwikapp.model.TreatCardRegisterDTO;
import com.payqwikapp.model.TreatCardResponse;
import com.payqwikapp.model.UpdateTreatCardDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.admin.ReportDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.error.TreatCardPlansError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.LogCat;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.TransactionValidation;
import com.payqwikapp.validation.TreatCardValidation;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/TreatCard")
public class TreatCardController {
	
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository serviceRepository;
	private final ITreatCardApi treatCardApi;
	private final TreatCardValidation treatCardValidation;
	private final ICommissionApi commissionApi;

	public TreatCardController(IUserApi userApi, ITransactionApi transactionApi,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			TransactionValidation transactionValidation, PQServiceRepository serviceRepository,
			ITreatCardApi treatCardApi,TreatCardValidation treatCardValidation,ICommissionApi commissionApi) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionValidation = transactionValidation;
		this.serviceRepository = serviceRepository;
		this.treatCardApi = treatCardApi;
		this.treatCardValidation = treatCardValidation;
		this.commissionApi = commissionApi;

	}

	@RequestMapping(value = "/GetCardDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TreatCardResponse> getGciAuthentication(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		TreatCardResponse result = new TreatCardResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							result = treatCardApi.getTreatCardDetails(user);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user authority.");
							result.setDetails("Failed to get user authority.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetTreatCardPlans", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> getPlans(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			@RequestBody SessionDTO session, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		String json = "";
		PlansDTO dto = new PlansDTO();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(session.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED) || 
									user.getAuthority().contains(Authorities.SUPER_ADMIN)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

								List<TreatCardPlansDTO> plans = treatCardApi.getAllPlansDTO();
								if (plans != null) {
									dto = new PlansDTO();
									dto.setStatus(ResponseStatus.SUCCESS);
									dto.setMessage("Get TreatCard Plans .");
									dto.setPlans(plans);
									json = ow.writeValueAsString(dto);
								} else {
									dto.setStatus(ResponseStatus.FAILURE);
									dto.setMessage("Get TreatCard Plans .");
									dto.setPlans(new ArrayList<TreatCardPlansDTO>());
									json = ow.writeValueAsString(dto);
								}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed to get user authority.");
								result.setDetails("Failed to get user authority.");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please login and try again.");
						result.setDetails("Please login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized user.");
					result.setDetails("Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid request.");
				result.setDetails("Invalid request.");
			}

		} catch (Exception e) {
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			result.setMessage("Something went wrong,Please try again letter.");
			result.setDetails(e.getMessage());
			return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
		}

		return new ResponseEntity<String>(json, HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/UpdateTreatCardPlans", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listPlans(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TreatCardPlansDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						TreatCardPlansError error = treatCardValidation.validatePlans(dto);
						if (error.isValid()) {
							result =treatCardApi.updatePlans(dto);
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("invalid parameters");
							result.setDetails(error);
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/updateTreatCard", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TreatCardPaymentResponse> updateTreatCard(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody UpdateTreatCardDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {

		TreatCardPaymentResponse result = new TreatCardPaymentResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				try {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO userDTO = userApi.getUserById(userSession.getUser().getId());
						if (userDTO.getAuthority().contains(Authorities.USER)
								&& userDTO.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = serviceRepository.findServiceByCode("TECD");
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(dto.getAmount()));
							double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(dto.getAmount()));
								 TransactionError transactionError =transactionValidation.validateBillPayment(dto.getAmount(),userDTO.getUsername(),commission,netCommissionValue);
								 if (transactionError.isValid()) {
								result=treatCardApi.updateTreatCard(dto, userDTO.getUsername(), service,commission,netCommissionValue);
								if(result.isSuccess()){
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Payment Successful of TreatCard .");
								result.setDetails("Payment Successful of TreatCard .");
								result.setBalance(userApi.getBalanceByUserAccount(userDTO.getAccount()));
								return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
								}else{
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Payment UnSuccessful of TreatCard .");
									result.setDetails("Payment UnSuccessful of TreatCard .");
									result.setBalance(userApi.getBalanceByUserAccount(userDTO.getAccount()));
									return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
								}
							}else{
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage(transactionError.getMessage());
							result.setDetails(transactionError);
							result.setBalance(userApi.getBalanceByUserAccount(userDTO.getAccount()));
							return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
						} 
						}else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
							result.setMessage("Un-Authorised Role");
							result.setDetails("Un-Authorised Role");
							return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
						return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					LogCat.print("Exception Caught");
					e.printStackTrace();
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Internal server error");
					result.setDetails(e.getMessage());
					return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
				return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<TreatCardPaymentResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetUserTreatCardPlans", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserPlans(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			@RequestBody SessionDTO session, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		String json = "";
		PlansDTO dto = new PlansDTO();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				if (role.equalsIgnoreCase("User")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(session.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								List<TreatCardPlansDTO> plans = treatCardApi.getAllPlansDTO();
								if (plans != null) {
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Get Treat Card Plans");
									result.setDetails(plans);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									dto.setStatus(ResponseStatus.FAILURE);
									dto.setMessage("Get TreatCard Plans .");
									dto.setPlans(new ArrayList<TreatCardPlansDTO>());
									json = ow.writeValueAsString(dto);
									result.setDetails(json);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed to get user authority.");
								result.setDetails("Failed to get user authority.");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please login and try again.");
						result.setDetails("Please login and try again.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized user.");
					result.setDetails("Unauthorized user.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid request.");
				result.setDetails("Invalid request.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}

		} catch (Exception e) {
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
			result.setMessage("Something went wrong,Please try again letter.");
			result.setDetails(e.getMessage());
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/TreatCardRegisterList", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerListPlans(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TreatCardRegisterDTO> listOfUser=treatCardApi.registerList();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("values");
						result.setDetails(listOfUser);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/TreatCardFilteredRegisterList", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> filteredRegisterListPlans(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TreatCardRegisterDTO> listOfUser=treatCardApi.registerListFiltered(dateFormat.parse(from),dateFormat.parse(to));
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("values");
						result.setDetails(listOfUser);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetTreatCardTransaction", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTodaysTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TListDTO> listDTOs = treatCardApi.getTreatCardTransactions();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	// API to get Transaction in Between Dates
	@RequestMapping(method = RequestMethod.POST, value = "/GetTreataCardTransaction/Filter", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionBetween(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date endDate = dateFormat.parse(dto.getEndDate());
						Date startDate = dateFormat.parse(dto.getStartDate());
						List<TListDTO> listDTOs = userApi.getTransactionsBetween(startDate, endDate);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/TreatCardCountList", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> treatCardCounts(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TreatCardRegisterDTO> listOfUser=treatCardApi.countList();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("values");
						result.setDetails(listOfUser);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Invalid authority");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Not a valid role");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	

}
