package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IBusApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.BusCityList;
import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TravellerDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.DateDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionCommissionListDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.model.travel.bus.BusPaymentReq;
import com.payqwikapp.model.travel.bus.BusPaymentResponse;
import com.payqwikapp.model.travel.bus.BusTicketResp;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Bus")
public class TravelBusController {

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final IUserApi userApi;
	private final PQServiceRepository pqServiceRepository;
	private UserSessionRepository userSessionRepository;
	private final TransactionValidation transactionValidation;
	private final IBusApi busApi;
	private final ITransactionApi transactionApi;
	public TravelBusController(IUserApi userApi, PQServiceRepository pqServiceRepository,UserSessionRepository userSessionRepository,
			TransactionValidation transactionValidation, IBusApi busApi,ITransactionApi transactionApi) {
		super();
		this.userApi = userApi;
		this.pqServiceRepository = pqServiceRepository;
		this.userSessionRepository=userSessionRepository;
		this.transactionValidation=transactionValidation;
		this.busApi=busApi;
		this.transactionApi =transactionApi;
	}

	@RequestMapping(value = "/BusBookingInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> validateTransactionFlight(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentInit dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
							if(service != null && Status.Active.equals(service.getStatus())) {
								TransactionError error = transactionValidation.validateEaseMyTrip(String.valueOf(dto.getTotalFare()),user.getUsername(),service);
								if(error.isValid()) {
									System.err.println("User Email is: "+dto.getUserEmail());
									User user2=userApi.findByUserName(user.getUsername());
									BusPaymentResponse busPayResp=busApi.placeOrder(dto,service,user.getUsername(),user2);
									if(busPayResp.isValid()) {
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(busPayResp.getMessage());
										result.setCode("S00");
										result.setDetails(busPayResp);
									}else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(busPayResp.getMessage());
										result.setCode("F00");
										result.setDetails(busPayResp.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(error.getMessage());
									result.setCode(error.getCode());
									result.setDetails(error.getSplitAmount());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Service is under maintenance");
								result.setCode("F00");
								result.setDetails("Service is under maintenance");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setCode("F00");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setCode("F00");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/ProcessPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processPayment(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							User user2=userApi.findByUserName(user.getUsername());
							BusPaymentResponse paymentResponse=busApi.processPayment(dto,user2);
							
							if(paymentResponse.isValid()) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(paymentResponse.getMessage());
								result.setCode("S00");
								result.setDetails(paymentResponse.getMessage());
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setCode("F00");
								result.setMessage(paymentResponse.getMessage());
								result.setDetails(paymentResponse.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/cancelInitPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cancelPaymentforInit(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
								busApi.failPayment(dto.getpQTxnId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Transection cancel successful");
								result.setCode("S00");
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/cronCheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cronCheck(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							busApi.cronforGetAllCityList();
							
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getAllCityList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllCityList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							List<BusCityList>  busCityList=busApi.getAllCityList();
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Get All City List");
							result.setDetails(busCityList);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getAllBookTickets", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMyTickets(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							List<BusTicketResp> allTickets=busApi.getAllTickets(user.getUsername());
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Get All Book Tickets");
							result.setDetails(allTickets);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getMyTicketsforWeb", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMyTicketsforWeb(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							List<BusTicketResp> allTickets=busApi.getAllTickets(user.getUsername());
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Get All Book Tickets");
							result.setDetails(allTickets);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getSingleTicketTravellerDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getSingleTicketTravellerDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
							List<TravellerDetails> travellerDetails=busApi.getSingleTicketTravellerDetails(dto.getBookingTxnId());
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Get All Traveller Details");
							result.setDetails(travellerDetails);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getBusDetailsForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBusDetailsForAdmin(@RequestBody SessionDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode("EMTB");
						List<PQTransaction> commissionList =  transactionApi.getCommissionOfMerchantService(service);
						List<TransactionCommissionListDTO> commissions = new ArrayList<>();
						for(PQTransaction c : commissionList) {
							TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
							if (c.getStatus().equals(Status.Success)) {
								commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
								commissions.add(commission);
							}
							
					      }
						List<BusTicket> list=busApi.getBusDetailsForAdmin();
						if(list!=null && commissionList !=null){
							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("userList", list);
							detail.put("commissionList", commissions);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setValid(true);
							result.setMessage("Bus Details received");
							result.setDetails(detail);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("Bus details not found");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 
	
	
	
	@RequestMapping(value = "/getTravellerDetailsForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTravellerDetailsForAdmin(@RequestBody BusPaymentReq dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin") ||role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						List<TravellerDetails> list=busApi.getSingleTicketTravellerDetails(dto.getBookingTxnId());
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Bus Details received");
							result.setDetails(list);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("Bus details not found");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 
	
	
	@RequestMapping(value = "/cancelBookedTicketForAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cancelTicketForAdmin(@RequestBody BusPaymentReq dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						
						result=busApi.cancelBookedTicket(dto.getpQTxnId(), dto.getRefundedAmount());
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 
	
	
	@RequestMapping(value = "getBusDetailsByDate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBusDetailsForAdminByDate(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		ResponseDTO result=new ResponseDTO();
			if (role.equalsIgnoreCase("Admin") ||role.equalsIgnoreCase("SuperAdmin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED) || user.getAuthority().contains(Authorities.SUPER_ADMIN)
							&&  user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode("EMTB");
						List<PQTransaction> commissionList =  transactionApi.getCommissionOfMerchantService(service);
						List<TransactionCommissionListDTO> commissions = new ArrayList<>();
						for(PQTransaction c : commissionList) {
							TransactionCommissionListDTO commission =new TransactionCommissionListDTO();
							commission =ConvertUtil.convertListFromDetailsMerchantSuccessCommission(c);
							commissions.add(commission);
					      }
						
						String from = dto.getFromDate() + " 00:00";
						String to = dto.getToDate() + " 23:59";
						
						List<BusTicket> list=busApi.getBusDetailsForAdminByDate(dateFormat.parse(from), dateFormat.parse(to));
						if(list!=null && commissionList !=null){
							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("userList", list);
							detail.put("commissionList", commissions);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setValid(true);
							result.setMessage("Bus Details received");
							result.setDetails(detail);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("Bus details not found");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} 
	
	
	
	
//  Changes for re price
	
	
	@RequestMapping(value = "/saveGetTxnId", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveGetTxnId(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentInit dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
							if(service != null && Status.Active.equals(service.getStatus())) {
								TransactionError error = transactionValidation.validateEaseMyTrip(String.valueOf(dto.getTotalFare()),user.getUsername(),service);
								if(error.isValid()) {
									System.err.println("User Email is: "+dto.getUserEmail());
									User user2=userApi.findByUserName(user.getUsername());
									BusPaymentResponse busPayResp=busApi.saveGetTxnId(dto,service,user.getUsername(),user2);
									if(busPayResp.isValid()) {
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(busPayResp.getMessage());
										result.setCode("S00");
										result.setDetails(busPayResp);
									}else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(busPayResp.getMessage());
										result.setCode("F00");
										result.setDetails(busPayResp.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(error.getMessage());
									result.setCode(error.getCode());
									result.setDetails(error.getSplitAmount());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Service is under maintenance");
								result.setCode("F00");
								result.setDetails("Service is under maintenance");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setCode("F00");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setCode("F00");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/bookTicketPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> bookTicketPayment(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							User user2=userApi.findByUserName(user.getUsername());
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
							BusPaymentResponse paymentResponse=busApi.bookTicketPayment(dto,service,user.getUsername(),user2);
							
							if(paymentResponse.isValid()) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(paymentResponse.getMessage());
								result.setCode("S00");
								result.setDetails(paymentResponse.getMessage());
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setCode("F00");
								result.setMessage(paymentResponse.getMessage());
								result.setDetails(paymentResponse.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setCode("F00");
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setCode("F00");
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setCode("F00");
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setCode("F00");
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/BusPaymentInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> busPaymentInit(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentInit dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
							if(service != null && Status.Active.equals(service.getStatus())) {
								TransactionError error = transactionValidation.validateEaseMyTrip(String.valueOf(dto.getPriceRecheckAmt()),user.getUsername(),service);
								if(error.isValid()) {
									User u=userApi.findByUserName(user.getUsername());
									BusPaymentResponse busPayResp=busApi.initBusPayment(dto,service,user.getUsername(),u);
									if(busPayResp.isValid()) {
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(busPayResp.getMessage());
										result.setCode("S00");
										result.setDetails(busPayResp);
									}else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(busPayResp.getMessage());
										result.setCode("F00");
										result.setDetails(busPayResp.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(error.getMessage());
									result.setCode(error.getCode());
									result.setDetails(error.getSplitAmount());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Service is under maintenance");
								result.setCode("F00");
								result.setDetails("Service is under maintenance");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setCode("F00");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setCode("F00");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/createBusTicketPdf", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> createTicketPdf(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentReq dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") ||role.equalsIgnoreCase("Admin") || role.equalsIgnoreCase("SuperAdmin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if ((user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) ||(user.getAuthority().contains(Authorities.ADMINISTRATOR)
										&& user.getAuthority().contains(Authorities.AUTHENTICATED)) ||(user.getAuthority().contains(Authorities.SUPER_ADMIN)
												&& user.getAuthority().contains(Authorities.AUTHENTICATED))) {
							
							List<TravellerDetails> travellerDetails=busApi.createSingleTicketPdf(dto.getBookingTxnId());
							
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setMessage("Get All Traveller Details");
							result.setDetails(travellerDetails);
						
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/saveSeatDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveSeatDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentInit dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
							if(service != null && Status.Active.equals(service.getStatus())) {
								TransactionError error = transactionValidation.validateEaseMyTrip(String.valueOf(dto.getTotalFare()),user.getUsername(),service);
									User user2=userApi.findByUserName(user.getUsername());
									BusPaymentResponse busPayResp=busApi.saveSeatDetails(dto,service,user.getUsername(),user2);
									if(busPayResp.isValid()) {
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(busPayResp.getMessage());
										result.setCode("S00");
										result.setDetails(busPayResp);
									}else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(busPayResp.getMessage());
										result.setCode("F00");
										result.setDetails(busPayResp.getMessage());
									}
								
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Service is under maintenance");
								result.setCode("F00");
								result.setDetails("Service is under maintenance");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setCode("F00");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setCode("F00");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
}
