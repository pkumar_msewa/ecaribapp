package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.ISecurityQuestionApi;
import com.payqwikapp.api.ITreatCardApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;
import com.payqwikapp.mail.util.MailTemplate;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.ForgotPasswordDTO;
import com.payqwikapp.model.QuestionsDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SecurityQuestionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TreatCardDTO;
import com.payqwikapp.model.TreatCardResponse;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.VerifyMobileDTO;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.ForgotPasswordError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.VerifyEmailOTPError;
import com.payqwikapp.model.error.VerifyMobileOTPError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.EmailOTPValidation;
import com.payqwikapp.validation.MobileOTPValidation;
import com.payqwikapp.validation.RegisterValidation;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;
import com.thirdparty.util.FRMConstants;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class RegisterController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUserApi userApi;
	private final ISecurityQuestionApi securityQuestionApi;
	private final RegisterValidation registerValidation;
	private final MobileOTPValidation mobileOTPValidation;
	private final EmailOTPValidation emailOTPValidation;
	private final IRefernEarnLogsApi refernEarnLogsApi;
	private final ITreatCardApi treatCardApi;
	private final IMailSenderApi mailSenderApi;
	private final IFRMApi frmApi;
	public RegisterController(IUserApi userApi, ISecurityQuestionApi securityQuestionApi,
			RegisterValidation registerValidation, MobileOTPValidation mobileOTPValidation,
			EmailOTPValidation emailOTPValidation, IRefernEarnLogsApi refernEarnLogsApi, 
			ITreatCardApi treatCardApi,IMailSenderApi mailSenderApi,IFRMApi frmApi) {
		super();
		this.userApi = userApi;
		this.securityQuestionApi = securityQuestionApi;
		this.registerValidation = registerValidation;
		this.mobileOTPValidation = mobileOTPValidation;
		this.emailOTPValidation = emailOTPValidation;
		this.refernEarnLogsApi = refernEarnLogsApi;
		this.treatCardApi = treatCardApi;
		this.mailSenderApi=mailSenderApi;
		this.frmApi=frmApi;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Register", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO user, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		FRMResponseDTO frmResp = new FRMResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(user, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				RegisterError error = new RegisterError();
				try {
					user.setUsername(user.getContactNo());
					error = registerValidation.validateNormalUser(user);
					if (error.isValid()) {
						user.setUserType(UserType.User);
						frmResp.setSuccess(true);
						if(DeploymentConstants.PRODUCTION){
						 frmResp= frmApi.decideRegistration(user);
						}
						if (frmResp.isSuccess()) {
							user.setDevice(device);
							userApi.saveUser(user);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(
										"User Registration Successful and OTP sent to ::" + user.getContactNo()
												+ " and  verification mail sent to ::" + user.getEmail() + " .");
								result.setDetails(
										"User Registration Successful and OTP sent to ::" + user.getContactNo()
												+ " and  verification mail sent to ::" + user.getEmail() + " .");
						}else{
							result=getFailedResult(result,error);
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("User is not allowed to use eCarib wallet.");
							result.setDetails(error);
						}
						frmApi.saveFrmDetails(user.getUsername(), frmResp.getDecideResp(),frmResp.isEnqueueResp(),
								null,FRMConstants.registration);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Register User");
						result.setDetails(error);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Please try again later.");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	private ResponseDTO getFailedResult(ResponseDTO result, RegisterError error) {
		result.setStatus(ResponseStatus.BAD_REQUEST);
		result.setMessage("User registration failed,user details are not correct.");
		result.setDetails(error);
		return result;
	}

	@RequestMapping(value = "/ForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> forgotPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotPasswordDTO forgot, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(forgot, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = forgot.getUsername();
				ForgotPasswordError error = registerValidation.forgotPassword(username);
				if (error.isValid()) {
					User u = userApi.findByUserName(username);
					if (u != null) {
						if (u.getAuthority().contains(Authorities.AUTHENTICATED)) {
							if (u.getMobileStatus() == Status.Active) {
								userApi.changePasswordRequest(u);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("OTP sent to " + forgot.getUsername());
								result.setDetails("OTP sent to " + forgot.getUsername());
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Please try again later.");
								result.setDetails("Please try again later.");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Your account is blocked! Please try after 24 hrs.");
							result.setDetails("Your account is blocked! Please try after 24 hrs.");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("User doesn't Exist,Please Register.");
						result.setDetails("User doesn't Exist,Please Register.");

					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Activate/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyUserMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
				if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
					/*User user = userApi.findByUserName(dto.getMobileNumber());
					RefernEarnlogs logs = refernEarnLogsApi.getlogs(user);
					if (logs != null) {
						if (logs.isHasEarned()) {
							result.setHasRefer(true);
						} else {
							result.setHasRefer(false);
						}
					}
				    TreatCardDTO treatCard = new TreatCardDTO();
					treatCard.setPhoneNumber(dto.getMobileNumber());
					TreatCardResponse resp = treatCardApi.registerTreatCard(treatCard);
					if(resp.isSuccess()){
						mailSenderApi.treatCardEmail("TreatCard Registration", MailTemplate.TREATCARD_EMAIL,user);
					}*/
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Activate Mobile");
					result.setDetails("Your Mobile is Successfully Verified ");
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Mobile");
					result.setDetails("Invalid Activation Key");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid User");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Resend/Mobile/OTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> generateNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = dto.getMobileNumber();
				VerifyMobileOTPError error = new VerifyMobileOTPError();
				error = mobileOTPValidation.validateUserResendOTP(username);
				if (error.isValid()) {
					userApi.resendUserMobileToken(username);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Resend OTP");
					result.setDetails("New OTP sent on " + username);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Mobile Number Already Verified.");
					result.setDetails(error.getOtp());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Resend/DeviceBinding/OTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> generateNewDeviceBindingOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = dto.getMobileNumber();
				boolean verified = userApi.resendMobileTokenDeviceBinding(username);
				if (verified) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("New OTP sent on " + username);
					result.setDetails("New OTP sent on " + username);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Enter A valid Details");
					result.setDetails("Enter A valid Details");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Change/{key}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyPasswordPost(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("key") String key, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(key, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserDTO dto = userApi.checkPasswordToken(key);
				if (dto != null) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Verify Password");
					result.setDetails("User ::" + dto.getUsername() + " key::" + key);
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Verify Password");
					result.setDetails("Invalid Verification Key");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/RenewPassword", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> renewPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				ChangePasswordError error = new ChangePasswordError();
				error = registerValidation.renewPasswordValidation(dto);
				ResponseDTO resp = null;
				if (error.isValid()) {
					try {
						resp = userApi.setPasswordHistory(dto);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (resp.getCode().equalsIgnoreCase("S00") || resp.getCode().equalsIgnoreCase("S01")
							|| resp.getCode().equalsIgnoreCase("S02")) {
						userApi.renewPassword(dto);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Password Changed Successfully");
						result.setDetails("Password Changed Successfully");
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(resp.getMessage());
						result.setDetails(resp.getMessage());
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage(error.getKey());
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/RenewPassword", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> agentRenewPassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				ChangePasswordError error = new ChangePasswordError();

				error = registerValidation.agentRenewPasswordValidation(dto);
				if (error.isValid()) {
					if(userApi.agentRenewPassword(dto)){
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Password Changed Successfully");
						result.setDetails("Password Changed Successfully");
					}
					else{
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Not a valid user");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Renew Password");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	
	@RequestMapping(method = RequestMethod.POST, value = "/Activate/Email/{key}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyUserEmailPost(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable("key") String key, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(key, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				VerifyEmailOTPError error = new VerifyEmailOTPError();
				error = emailOTPValidation.checkEmailToken(key);
				if (error.isValid()) {
					boolean status = userApi.activateEmail(key);
					if (status) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Activate Email");
						result.setDetails("Your E-Mail is Successfully Verified");
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Activate Email");
						result.setDetails("Unable to verify email. Please contact customer care.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Activate Email");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GetSecurityQuestions", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> getQuestions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		String json = "";
		QuestionsDTO dto = new QuestionsDTO();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		if (role.equalsIgnoreCase("User")) {
			try {
				List<SecurityQuestionDTO> questions = securityQuestionApi.getAllQuestionDTO();
				if (questions != null) {
					dto = new QuestionsDTO();
					dto.setStatus(ResponseStatus.SUCCESS);
					dto.setMessage("Get Questions");
					dto.setQuestions(questions);
					json = ow.writeValueAsString(dto);
				} else {
					dto.setStatus(ResponseStatus.FAILURE);
					dto.setMessage("Get Questions");
					dto.setQuestions(new ArrayList<SecurityQuestionDTO>());
					json = ow.writeValueAsString(dto);
				}
			} catch (Exception e) {
				result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
				result.setMessage("Please try again later.");
				result.setDetails(e.getMessage());
				return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<String>(json, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/URegisterUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> iRegisterUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO user, @RequestHeader(value = "API_KEY", required = true) String apiKey,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isApiKeys = SecurityUtil.isApiKeys(apiKey);
		if (isApiKeys) {
			if (role.equalsIgnoreCase("User")) {
				RegisterError error = new RegisterError();
				try {
					user.setUsername(user.getContactNo());
					error = registerValidation.validateURegisterUser(user);
					if (error.isValid()) {
						user.setUserType(UserType.User);
						userApi.saveRegisterUser(user);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User Registration Successful.");
						result.setDetails("User Registration Successful.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Register User");
						result.setDetails(error);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Please try again later.");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_APIKEY);
			result.setMessage("Failed, Invalid API KEY.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	private boolean verifyUserMobileToken(String key, String mobileNumber) {
		if (userApi.checkMobileToken(key, mobileNumber)) {
			return true;
		} else {
			return false;
		}

	}

}
