package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ISendMoneyApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.BankDetails;
import com.payqwikapp.entity.Banks;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.MRequestDTO;
import com.payqwikapp.model.NeftRequestDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyBankSettleDTO;
import com.payqwikapp.model.SendMoneyDonateeDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.SendMoneyResponse;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.error.SendMoneyBankError;
import com.payqwikapp.model.error.SendMoneyMobileError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.BankDetailRepository;
import com.payqwikapp.repositories.BanksRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.SendMoneyValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/SendMoney")
public class SendMoneyController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ISendMoneyApi sendMoneyApi;
	private final IUserApi userApi;
	private final SendMoneyValidation sendMoneyValidation;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final BanksRepository banksRepository;
	private final BankDetailRepository bankDetailRepository;

	public SendMoneyController(ISendMoneyApi sendMoneyApi, IUserApi userApi, SendMoneyValidation sendMoneyValidation,
			TransactionValidation transactionValidation, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, PQServiceRepository pqServiceRepository,
			BanksRepository banksRepository, BankDetailRepository bankDetailRepository) {
		this.sendMoneyApi = sendMoneyApi;
		this.userApi = userApi;
		this.sendMoneyValidation = sendMoneyValidation;
		this.transactionValidation = transactionValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.banksRepository = banksRepository;
		this.bankDetailRepository = bankDetailRepository;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Merchant/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processMerchantMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileDTO mobile, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(mobile, hash);
		if (isValidHash) {
			try {
				if (role.equalsIgnoreCase("User")) {
					SendMoneyMobileError error = new SendMoneyMobileError();
					String sessionId = mobile.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PGDetails pgDetails = userApi.findMerchantByMobile(mobile.getMobileNumber());
							if (pgDetails != null) {
								PQService service = pgDetails.getService();
								if (service != null) {
									error = sendMoneyValidation.checkMobileError(mobile, user.getUsername(),
											service.getCode());
									if (error.isValid()) {
										AuthenticationError authError = userApi.validateUserRequest(mobile.toString(),
												userSession.getUser(), service, timeStamp);
										if (authError.isSuccess()) {
											TransactionError transactionError = transactionValidation
													.validateP2PTransaction(mobile.getAmount(), user.getUsername(),
															mobile.getMobileNumber(), service);
											User u = userApi.findByUserName(user.getUsername());
											if (transactionError.isValid()) {
												User merchant = pgDetails.getUser();
												PayStoreDTO pay = ConvertUtil.convertToPayStore(merchant.getId(),
														mobile.getAmount());
												result = sendMoneyApi.preparePayStore(pay, u);
												result.setBalance(userApi.getWalletBalance(u));
												userApi.updateRequestLog(u, timeStamp);
											} else {
												result.setStatus(ResponseStatus.FAILURE);
												result.setMessage(transactionError.getMessage());
												result.setDetails(transactionError.getMessage());
												userApi.updateRequestLog(u, timeStamp);
											}
										} else {
											result.setStatus(ResponseStatus.BAD_REQUEST);
											result.setMessage(authError.getMessage());
											result.setDetails(authError.getMessage());
										}
									} else {
										result.setStatus(ResponseStatus.BAD_REQUEST);
										result.setMessage("Failed, invalid request.");
										result.setDetails(error);
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Service Not Found For Merchant");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Merchant Not Found");
								result.setDetails("Merchant Not Found");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session,Please try again later");
						result.setDetails("Invalid Session,Please try again later");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized User");
					result.setDetails("Failed, Unauthorized User");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your sendmoney request is declined . please contact customer care .");
				result.setDetails("your sendmoney request is declined . please contact customer care .");

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Not a valid hash");
			result.setDetails("Not a valid hash");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileDTO mobile, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(mobile, hash);
		if (isValidHash) {
			try {
				if (role.equalsIgnoreCase("User")) {
					SendMoneyMobileError error = new SendMoneyMobileError();
					String sessionId = mobile.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						String serviceCode = sendMoneyApi.prepareSendMoney(mobile.getMobileNumber());
						error = sendMoneyValidation.checkMobileError(mobile, user.getUsername(), serviceCode);
						if (error.isValid()) {
							if (user.getAuthority().contains(Authorities.USER)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								PQService service = pqServiceRepository.findServiceByCode(serviceCode);
									TransactionError transactionError = transactionValidation.validateP2PTransaction(
											mobile.getAmount(), user.getUsername(), mobile.getMobileNumber(), service);
									if (transactionError.isValid()) {
										User u = userApi.findByUserName(user.getUsername());
										result = sendMoneyApi.sendMoneyMobile(mobile, user.getUsername(), service);
										userApi.updateRequestLog(u, timeStamp);
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(transactionError.getMessage());
										result.setDetails(transactionError.getMessage());
										userApi.updateRequestLog(userApi.findByUserName(user.getUsername()), timeStamp);
									}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized user.");
								result.setDetails("Failed, Unauthorized user.");
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage(error.getMobileNumber());
							result.setDetails(error);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}

			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your sendmoney request is declined . please contact customer care .");
				result.setDetails("your sendmoney request is declined . please contact customer care .");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	// send money to donatee
	@RequestMapping(value = "/Donatee", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processDonation(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyDonateeDTO donatee, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(donatee, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				SendMoneyMobileError error = new SendMoneyMobileError();
				String sessionId = donatee.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User donor = userApi.findById(userSession.getUser().getId());
					String serviceCode = sendMoneyApi.prepareSendMoneyToDonatee(donatee.getDonatee());
					error = sendMoneyValidation.checkMobileError(donatee, donor.getUsername(), serviceCode);
					if (error.isValid()) {
						if (donor.getAuthority().contains(Authorities.USER)
								&& donor.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(serviceCode);
							AuthenticationError requestError = userApi.validateUserRequest(donatee.toString(),
									userSession.getUser(), service, timeStamp);
							if (requestError.isSuccess()) {
								TransactionError transactionError = transactionValidation.validateP2PTransaction(
										donatee.getAmount(), donor.getUsername(), donatee.getDonatee(), service);
								if (transactionError.isValid()) {
									User u = userApi.findByUserName(donor.getUsername());
									sendMoneyApi.sendMoneyMobile(donatee, donor.getUsername(), service);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage(
											"Sent money to mobile successful. Money Sent to " + donatee.getDonatee());
									result.setDetails("Money Sent to " + donatee.getDonatee());
									result.setBalance(u.getAccountDetail().getBalance());
									userApi.updateRequestLog(u, timeStamp);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(transactionError.getMessage());
									userApi.updateRequestLog(userApi.findByUserName(donor.getUsername()), timeStamp);
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage(requestError.getMessage());
								result.setDetails(requestError.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage(error.getMobileNumber());
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Bank/Initiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> initiateBank(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyBankDTO bank, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(device, hash);
		if (isValidHash) {
			try {
				if (role.equalsIgnoreCase("User")) {
					SendMoneyBankError error = new SendMoneyBankError();
					error = sendMoneyValidation.checkBankError(bank, "SMB");
					if (error.isValid()) {
						String sessionId = bank.getSessionId();
						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
						if (userSession != null) {
							User u = userApi.findById(userSession.getUser().getId());
							if (u.getAuthority().contains(Authorities.USER)
									&& u.getAuthority().contains(Authorities.AUTHENTICATED)) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								PQService service = pqServiceRepository.findServiceByCode("SMB");
								User user = userApi.findByUserName(u.getUsername());
								String accountType = u.getAccountDetail().getAccountType().getCode();
								if (accountType.equalsIgnoreCase("KYC")) {
										TransactionError transactionError = transactionValidation
												.validateBankTransfer(bank.getAmount(), user.getUsername(), service);
										if (transactionError.isValid()) {
											Banks banks = banksRepository.findByCode(bank.getBankCode());
											if (banks != null) {
												BankDetails bankDetail = bankDetailRepository
														.findByIfscCode(bank.getIfscCode(), banks);
												if (bankDetail != null) {
													boolean fundTr=sendMoneyApi.sendMoneyBankInitiate(bank, u.getUsername(), service);
													if(fundTr){
														result.setStatus(ResponseStatus.SUCCESS);
														result.setMessage("Your request is initiated successfully for "
																+ bank.getAccountNumber() + "  where account holder is "
																+ bank.getAccountName()
																+ ".\n Amount will be credited within 24 hours");
													}else{
														result.setStatus(ResponseStatus.FAILURE);
														result.setMessage("Because of some security reason,your request of fund transfer is failed.");
													}
													result.setDetails("");
													if (u != null) {
														result.setBalance(userApi.getWalletBalance(u));
													}
													userApi.updateRequestLog(u, timeStamp);
												} else {
													result.setStatus(ResponseStatus.FAILURE);
													result.setMessage("IFSC Code is not valid");
													userApi.updateRequestLog(u, timeStamp);
												}
											} else {
												result.setStatus(ResponseStatus.FAILURE);
												result.setMessage("Bank Code is not valid");
												userApi.updateRequestLog(u, timeStamp);
											}
										} else {
											result.setStatus(ResponseStatus.FAILURE);
											result.setMessage(transactionError.getMessage());
											result.setDetails(transactionError.getMessage());
											userApi.updateRequestLog(u, timeStamp);
										}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(
											"You must be KYC user to use this functionality,\n Please contact Customer Care");
									result.setDetails(
											"You must be KYC user to use this functionality,\n Please contact Customer Care");
									userApi.updateRequestLog(u, timeStamp);
								}
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Permission Not Granted");
								result.setDetails("Permission Not Granted");
							}
						} else {
							result.setStatus(ResponseStatus.INVALID_SESSION);
							result.setMessage("Invalid Session");
							result.setDetails("Invalid Session");
						}
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Error Occurred while Processing Request");
						result.setDetails(error);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("User | Send Money Bank");
					result.setDetails("Permission Not Granted");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your sendmoney request is declined . please contact customer care .");
				result.setDetails("your sendmoney request is declined . please contact customer care .");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Bank/Success", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> successBank(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyBankSettleDTO bank, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(device, hash);
		if (isValidHash) {
			try {
				if (role.equalsIgnoreCase("User")) {
					String sessionId = bank.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							sendMoneyApi.sendMoneyBankSuccess(bank.getTransactionRefNo());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("User | Send Money Bank");
							result.setDetails("Bank transfer success. Transaction Reference Number "
									+ bank.getTransactionRefNo());
							User u = userApi.findByUserName(user.getUsername());
							if (u != null) {
								result.setBalance(u.getAccountDetail().getBalance());
							}

						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("User | Send Money Bank");
							result.setDetails("Permission Not Granted");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("User | Send Money Bank");
						result.setDetails("Invalid Session");
					}

				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("User | Send Money Bank");
					result.setDetails("Permission Not Granted");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your sendmoney request is declined . please contact customer care .");
				result.setDetails("your sendmoney request is declined . please contact customer care .");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Bank/Failed", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> failedBank(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyBankSettleDTO bank, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(device, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = bank.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						sendMoneyApi.sendMoneyBankFailed(bank.getTransactionRefNo());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User | Send Money Bank");
						result.setDetails(
								"Bank transfer failed. Transaction Reference Number " + bank.getTransactionRefNo());
						User u = userApi.findByUserName(user.getUsername());
						if (u != null) {
							result.setBalance(u.getAccountDetail().getBalance());
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("User | Send Money Bank");
						result.setDetails("Permission Not Granted");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("User | Send Money Bank");
					result.setDetails("Invalid Session");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("User | Send Money Bank");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/MBankTransfer/Initiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> initiateMBankTransfer(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyBankDTO bank, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(device, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				SendMoneyBankError error = new SendMoneyBankError();
				error = sendMoneyValidation.checkBankError(bank, "SMMB");
				if (error.isValid()) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(bank.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.MERCHANT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(bank.getSessionId());
							PQService service = pqServiceRepository.findServiceByCode("SMMB");
							SendMoneyResponse resp = sendMoneyApi.checkInputParameters(bank);
							if (resp.isValid()) {
								TransactionError transactionError = transactionValidation
										.validateBankTransfer(bank.getAmount(), user.getUsername(), service);
								if (transactionError.isValid()) {
									resp = sendMoneyApi.sendMoneyMBankInitiateNew(bank, user, service);
									if (resp.isValid()) {
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(
												"Your request is initiated successfully for " + bank.getAccountNumber()
														+ "  where account holder is" + bank.getAccountName()
														+ ".\n Amount will be credited within 24 hours");
										result.setBalance(user.getAccountDetail().getBalance());
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage("Bank Transfer Not Avaliable, Please try again later");
									}
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(resp.getMessage());
							}

						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Error Occurred while Processing Request");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("User | Send Money Bank");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ABankTransfer/Initiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> initiateABankTransfer(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AgentSendMoneyBankDTO bank, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		SendMoneyResponse resp = new SendMoneyResponse();
		boolean isValidHash = SecurityUtil.isHashMatches(device, hash);
		if (isValidHash) {
			System.out.println();
			if (role.equalsIgnoreCase("Agent")) {
				SendMoneyBankError error = new SendMoneyBankError();
				error = sendMoneyValidation.checkABankError(bank, "SMMB");
				if (error.isValid()) {
					String sessionId = bank.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode("SMAB");
							User u = userApi.findByUserName(user.getUsername());
							bank.setBankCode(sendMoneyApi
									.getAgentBankCode(bank)); /* Set bankCode */
							resp = sendMoneyApi.checkAInputParameters(bank);
							if (resp.isValid()) {
								AuthenticationError authError = userApi.validateUserRequest(bank.toString(), u, service,
										timeStamp);
								if (authError.isSuccess()) {
									TransactionError transactionError = transactionValidation
											.validateBankTransfer(bank.getAmount(), user.getUsername(), service);
									if (transactionError.isValid()) {
										resp = sendMoneyApi.sendMoneyABankInitiate(bank, user, service);
										System.err.println("response ::" + resp);
										if (resp.isValid()) {
											result.setStatus(ResponseStatus.SUCCESS);
											result.setMessage(
													"Your request is initiated successfully for Account Number "
															+ bank.getAccountNumber() + "  where account holder is "
															+ bank.getAccountName()
															+ ".\n Amount will be credited within 24 hours");
											result.setBalance(user.getAccountDetail().getBalance());
											userApi.updateRequestLog(user, timeStamp);
										} else {
											result.setStatus(ResponseStatus.FAILURE);
											result.setMessage("Bank Transfer Not Avaliable, Please try again later");
										}
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(transactionError.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(authError.getMessage());
									result.setDetails(authError.getMessage());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(resp.getMessage());
							}

						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Error Occurred while Processing Request");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Agent | Send Money Bank");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetNeftRequest", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> getQuestions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody SessionDTO session,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		String json = "";
		MRequestDTO dto = new MRequestDTO();
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		if (role.equalsIgnoreCase("Merchant")) {
			try {
				List<NeftRequestDTO> questions = sendMoneyApi.getNeftRequestFile();
				if (questions != null) {
					dto = new MRequestDTO();
					dto.setRequests(questions);
					json = ow.writeValueAsString(dto);
				}
			} catch (Exception e) {
				result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
				result.setMessage("Please try again later.");
				result.setDetails(e.getMessage());
				return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<String>(json, HttpStatus.OK);

	}
}
