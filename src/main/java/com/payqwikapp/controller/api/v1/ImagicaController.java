package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IImagicaApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.ImagicaAuthDTO;
import com.payqwikapp.model.ImagicaLocationDTO;
import com.payqwikapp.model.ImagicaOrder;
import com.payqwikapp.model.ImagicaOrderResponse;
import com.payqwikapp.model.ImagicaPaymentDTO;
import com.payqwikapp.model.ImagicaPaymentResponse;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.ImagicaError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.ImagicaErrorValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Imagica")
public class ImagicaController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
	private final SimpleDateFormat dateFormated = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository serviceRepository;
	private final IImagicaApi imagicaApi;
	private final ImagicaErrorValidation imagicaErrorValidation;

	public ImagicaController(IUserApi userApi, ITransactionApi transactionApi,
                             UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
                             TransactionValidation transactionValidation, PQServiceRepository serviceRepository,IImagicaApi imagicaApi,ImagicaErrorValidation imagicaErrorValidation) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionValidation = transactionValidation;
		this.serviceRepository = serviceRepository;
		this.imagicaApi = imagicaApi;
		this.imagicaErrorValidation = imagicaErrorValidation;

	}


	@RequestMapping(value = "/LocationInfo", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getLocationDetails(@PathVariable(value = "role") String role,
														 @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
														 @RequestBody ImagicaLocationDTO dto, @RequestHeader(value = "hash", required = true) String hash,
														 HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							result = userApi.getLocationByPin(dto.getPincode());
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed to get user receipts");
							result.setDetails("Failed to get user receipts");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}



	@RequestMapping(value = "/GetAuth", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getImagicaAuthentication(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							ImagicaAuthDTO authDTO = imagicaApi.getLatestCredentials();
							if(authDTO.isActive()) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("AuthDetails");
								result.setDetails(authDTO);
							}else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Imagica service is down for maintenance");
								result.setError("Imagica service is down for maintenance");

							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Un-Authorized User");
							result.setDetails("Un-Authorized User");
							result.setError("Un-Authorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			System.err.println("inside this");
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


//
	@RequestMapping(value = "/PlaceOrder", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> validateTransaction(@PathVariable(value = "role") String role,
														   @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
														   @RequestBody ImagicaOrder dto, @RequestHeader(value = "hash", required = true) String hash,
														   HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				ImagicaError imagicaError=imagicaErrorValidation.checkError(dto);
				if(imagicaError.isValid()){
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = serviceRepository.findServiceByCode(StartupUtil.IMAGICA_SERVICE);
							if(service != null) {
								TransactionError error = transactionValidation.validateImagicaOrder(dto.getTotalAmount(),user.getUsername(),service);
								if(error.isValid()) {
									ImagicaOrderResponse orderResponse = imagicaApi.placeOrder(dto,service,user.getUsername());
									if(orderResponse.isSuccess()) {
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage(orderResponse.getMessage());
										result.setDetails(orderResponse);
									}else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(orderResponse.getMessage());
										result.setCode("F00");
										result.setDetails(orderResponse.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(error.getMessage());
									result.setCode("F00");
									result.setDetails(error.getMessage());
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Invalid Found");
								result.setCode("F00");
								result.setDetails("Invalid Service");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setCode("F00");
					result.setDetails("Please login and try again.");
				}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(imagicaError);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/ProcessPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processPayment(@PathVariable(value = "role") String role,
													@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
													@RequestBody ImagicaPaymentDTO dto, @RequestHeader(value = "hash", required = true) String hash,
													HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							ImagicaPaymentResponse paymentResponse = imagicaApi.processPayment(dto);
							if(paymentResponse.isSuccess()) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(paymentResponse.getMessage());
								result.setDetails(paymentResponse.getMessage());
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(paymentResponse.getMessage());
								result.setDetails(paymentResponse.getMessage());
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setDetails("Please login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}



}