package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.LoginLog;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.SessionLog;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.LoginDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.error.LoginError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.SessionLoggingStrategy;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.LoginValidation;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;
import com.thirdparty.util.FRMConstants;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class LoginController {
	
			 
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final IUserApi userApi;
	private final AuthenticationManager authenticationManager;
	private final UserSessionRepository userSessionRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final ISessionApi sessionApi;
	private final LoginValidation loginValidation;
	private final ITransactionApi transactionApi;
	private final IRefernEarnLogsApi refernEarnLogsApi;
	private final IFRMApi frmApi;

	public LoginController(IUserApi userApi, AuthenticationManager authenticationManager,
			UserSessionRepository userSessionRepository, SessionLoggingStrategy sessionLoggingStrategy,
			ISessionApi sessionApi,LoginValidation loginValidation,ITransactionApi transactionApi,
			IRefernEarnLogsApi refernEarnLogsApi,IFRMApi frmApi) {
		this.userApi = userApi;
		this.authenticationManager = authenticationManager;
		this.userSessionRepository = userSessionRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.sessionApi = sessionApi;
		this.loginValidation = loginValidation;
		this.transactionApi=transactionApi;
		this.refernEarnLogsApi=refernEarnLogsApi;
		this.frmApi= frmApi;
	}

	@RequestMapping(value = "/Login", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> userLogin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody LoginDTO login, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {

		ResponseDTO result = new ResponseDTO();
		FRMResponseDTO frmResp=new FRMResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(login, hash);
		LoginError error = loginValidation.checkLoginValidation(login);
		if (error.isSuccess()) {
				try {
					User user = userApi.findByUserName(login.getUsername());
					System.err.println("");
					if (user != null) {
						if (user.getAuthority().contains(Authorities.BLOCKED)) {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Account is blocked");
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else if (user.getAuthority().contains(Authorities.LOCKED)) {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Account is locked,contact Customer-Care");
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}else if (user.getAuthority().contains(Authorities.SPECIAL_USER)) {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized user.");
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else if (role.equalsIgnoreCase("User")) {
						if (user.getAuthority().contains(Authorities.USER)) {
							frmResp.setSuccess(true);
							SessionLog sessionLog = sessionLoggingStrategy.getLastLoginData(user.getId());
							LoginLog loginLog=userApi.getRemoteAddress(user); 
							if(DeploymentConstants.PRODUCTION){
							  frmResp = frmApi.decideLogin(user, sessionLog,loginLog);
							 frmApi.saveFrmDetails(user.getUserDetail().getContactNo(), frmResp.getDecideResp(),
									 frmResp.isEnqueueResp(),null, FRMConstants.dbLogin);
							}
							if (frmResp.isSuccess()) {
								if (user.getUsername().equalsIgnoreCase("7022620747")
										|| user.getUsername().equalsIgnoreCase("9760210393")
										|| user.getUsername().equalsIgnoreCase("8050581012")) {
									if (!login.isValidate()) {
										AuthenticationError authError = isValidUsernamePassword(login, request);
										if (authError.isSuccess()) {
											boolean validDevice = true;
											if (validDevice) {
												if (sessionApi.checkActiveSession(user)) {
													AuthenticationError auth = authentication(login, request);
													if (auth.isSuccess()) {
														try {
															String gcmId = login.getRegistrationId();
															if (gcmId != null) {
																userApi.updateGcmId(gcmId, user.getUsername());
															}
															if ("Android".equalsIgnoreCase(device)
																	|| "IOS".equalsIgnoreCase(device)) {
																user.setOsName(device);
																userApi.updateOperatingSystem(user);
															}
														} catch (NullPointerException e) {
															e.printStackTrace();
														}
														user.setMobileToken(CommonUtil.generateNDigitNumericString(6));
														Map<String, Object> detail = new HashMap<String, Object>();
														Authentication authentication = SecurityContextHolder
																.getContext().getAuthentication();
														sessionLoggingStrategy.onAuthentication(authentication, request,
																response);
														UserSession userSession = userSessionRepository
																.findByActiveSessionId(RequestContextHolder
																		.currentRequestAttributes().getSessionId());
														System.err.println("session iD ::" + RequestContextHolder
																.currentRequestAttributes().getSessionId());
														UserDTO activeUser = userApi
																.getUserById(userSession.getUser().getId());
														result.setStatus(ResponseStatus.SUCCESS);
														result.setMessage("Login successful.");
														detail.put("sessionId", userSession.getSessionId());
														detail.put("userDetail", activeUser);
														PQAccountDetail account = user.getAccountDetail();
														account.setBalance(Double.parseDouble(String.format("%.2f", account.getBalance())));
														detail.put("accountDetail", account);
														detail.put("nikiOffer", "new");
														result.setDetails(detail);
														RefernEarnlogs logs = refernEarnLogsApi.getlogs(user);
														if (logs != null) {
															if (logs.isHasEarned()) {
																result.setHasRefer(true);
															} else {
																result.setHasRefer(false);
															}
														}
														return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
													} else {
														result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
														result.setMessage(auth.getMessage());
														return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
													}
												}
											} else {
												userApi.requestNewLoginDevice(user);
												RefernEarnlogs logs = refernEarnLogsApi.getlogs(user);
												if (logs != null) {
													if (logs.isHasEarned()) {
														result.setHasRefer(true);
													} else {
														result.setHasRefer(false);
													}
												}
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											}
										} else {
											result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
											result.setMessage(authError.getMessage());
										}
									}
								} else if (!login.isValidate()) {
									AuthenticationError authError = isValidUsernamePassword(login, request);
									if (authError.isSuccess()) {
										boolean validDevice = true;
										if (DeploymentConstants.PRODUCTION) {
											validDevice = userApi.isValidLastLoginDevice(login.getIpAddress(), user);
										}
										if (validDevice) {
											if (sessionApi.checkActiveSession(user)) {
												AuthenticationError auth = authentication(login, request);
												if (auth.isSuccess()) {
													try {
														String gcmId = login.getRegistrationId();
														if (gcmId != null) {
															userApi.updateGcmId(gcmId, user.getUsername());
														}
														if ("Android".equalsIgnoreCase(device)
																|| "IOS".equalsIgnoreCase(device)) {
															user.setOsName(device);
															userApi.updateOperatingSystem(user);
														}
													} catch (NullPointerException e) {
														e.printStackTrace();
													}
													user.setMobileToken(CommonUtil.generateNDigitNumericString(6));
													Map<String, Object> detail = new HashMap<String, Object>();
													Authentication authentication = SecurityContextHolder.getContext()
															.getAuthentication();
													sessionLoggingStrategy.onAuthentication(authentication, request,
															response);
													UserSession userSession = userSessionRepository
															.findByActiveSessionId(RequestContextHolder
																	.currentRequestAttributes().getSessionId());
													System.err.println("session iD ::" + RequestContextHolder
															.currentRequestAttributes().getSessionId());
													UserDTO activeUser = userApi
															.getUserById(userSession.getUser().getId());
													result.setStatus(ResponseStatus.SUCCESS);
													result.setMessage("Login successful.");
													detail.put("sessionId", userSession.getSessionId());
													detail.put("userDetail", activeUser);
													PQAccountDetail account = user.getAccountDetail();
													account.setBalance(Double
															.parseDouble(String.format("%.2f", account.getBalance())));
													detail.put("accountDetail", account);
													detail.put("nikiOffer", "new");
													result.setDetails(detail);
													return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
												} else {
													result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
													result.setMessage(auth.getMessage());
													return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
												}
											}
										} else {
											userApi.requestNewLoginDevice(user);
											result.setStatus(ResponseStatus.NEW_DEVICE);
											result.setMessage("OTP sent to :" + user.getUsername());
											RefernEarnlogs logs = refernEarnLogsApi.getlogs(user);
											if (logs != null) {
												if (logs.isHasEarned()) {
													result.setHasRefer(true);
												} else {
													result.setHasRefer(false);
												}
											}
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}
									} else {
										result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
										result.setMessage(authError.getMessage());
									}
								} else {
									boolean isVerified = true;
									if (DeploymentConstants.PRODUCTION) {
										isVerified = userApi.isValidLoginToken(login.getMobileToken(), user);
									}
									if (isVerified) {
										if (sessionApi.checkActiveSession(user)) {
											AuthenticationError auth = authentication(login, request);
											if (auth.isSuccess()) {
												try {
													String gcmId = login.getRegistrationId();
													if (gcmId != null) {
														userApi.updateGcmId(gcmId, user.getUsername());
													}
													if ("Android".equalsIgnoreCase(device)
															|| "IOS".equalsIgnoreCase(device)) {
														user.setOsName(device);
														userApi.updateOperatingSystem(user);
													}
												} catch (NullPointerException e) {
													e.printStackTrace();
												}
												Map<String, Object> detail = new HashMap<String, Object>();
												Authentication authentication = SecurityContextHolder.getContext()
														.getAuthentication();
												sessionLoggingStrategy.onAuthentication(authentication, request,
														response);
												UserSession userSession = userSessionRepository.findByActiveSessionId(
														RequestContextHolder.currentRequestAttributes().getSessionId());
												UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
												result.setStatus(ResponseStatus.SUCCESS);
												result.setMessage("Login successful.");
												detail.put("sessionId", userSession.getSessionId());
												detail.put("userDetail", activeUser);
												PQAccountDetail account = user.getAccountDetail();
												account.setBalance(Double
														.parseDouble(String.format("%.2f", account.getBalance())));
												detail.put("accountDetail", account);
												detail.put("nikiOffer", "new");
												result.setDetails(detail);
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											} else {
												result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
												result.setMessage(auth.getMessage());
												result.setDetails(null);
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											}
										}
									} else {
										result.setStatus(ResponseStatus.BAD_REQUEST);
										result.setMessage("Not a valid OTP");
									}
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage("Cause of some security reason,Your Login has been blocked.");
							}
						}
					} else if (role.equalsIgnoreCase("Agent")) {
						if (user.getAuthority().contains(Authorities.AGENT)) {
							if (!login.isValidate()) {
								AuthenticationError authError = isValidUsernamePassword(login, request);
								if (authError.isSuccess()) {
									boolean validDevice=true;
                                	if(DeploymentConstants.PRODUCTION){
                                		validDevice = userApi.isValidLastLoginDevice(login.getIpAddress(), user);
                                	}
									if (validDevice) {
										if (sessionApi.checkActiveSession(user)) {
											AuthenticationError auth = authentication(login, request);
											if (auth.isSuccess()) {
												user.setMobileToken(CommonUtil.generateNDigitNumericString(6));
												Map<String, Object> detail = new HashMap<String, Object>();
												Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
												sessionLoggingStrategy.onAuthentication(authentication, request,response);
												UserSession userSession = userSessionRepository.findByActiveSessionId(
														RequestContextHolder.currentRequestAttributes().getSessionId());
												UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
												String count = userApi.getCountOfAgentRegister(activeUser);
												double totaldebitamount = transactionApi.getTotalTransactionsOfAgent(activeUser.getUsername());
												result.setStatus(ResponseStatus.SUCCESS);
												result.setMessage("Login successful.");
												detail.put("sessionId", userSession.getSessionId());
												detail.put("userDetail", activeUser);
												detail.put("accountDetail", user.getAccountDetail());
												detail.put("totalDebitTransaction", totaldebitamount);
												detail.put("numberOfRegisterUser", count);
												result.setDetails(detail);
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											} else {
												result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
												result.setMessage(auth.getMessage());
												result.setDetails(null); 
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											}
										}
									} else {
										userApi.requestNewLoginDevice(user);
										result.setStatus(ResponseStatus.NEW_DEVICE);
										result.setMessage("OTP sent to ::" + user.getUsername());
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									}
								} else {
									result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
									result.setMessage(authError.getMessage());
								}
							} else {
								boolean isVerified = true;
								if(DeploymentConstants.PRODUCTION){
								isVerified = userApi.isValidLoginToken(login.getMobileToken(),user);
								}
								if(isVerified) {
									if (sessionApi.checkActiveSession(user)) {
										AuthenticationError auth = authentication(login, request);
										if (auth.isSuccess()) {
											Map<String, Object> detail = new HashMap<String, Object>();
											Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
											sessionLoggingStrategy.onAuthentication(authentication, request, response);
											UserSession userSession = userSessionRepository.findByActiveSessionId(
													RequestContextHolder.currentRequestAttributes().getSessionId());
											UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
											result.setStatus(ResponseStatus.SUCCESS);
											result.setMessage("Login successful.");
											String count = userApi.getCountOfAgentRegister(activeUser);
											double totaldebitamount = transactionApi.getTotalTransactionsOfAgent(activeUser.getUsername());
											detail.put("sessionId", userSession.getSessionId());
											detail.put("userDetail", activeUser);
											detail.put("accountDetail", user.getAccountDetail());
											detail.put("totalDebitTransaction", totaldebitamount);
											detail.put("numberOfRegisterUser", count);
											result.setDetails(detail);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										} else {
											result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
											result.setMessage(auth.getMessage());
											result.setDetails(null);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage("Not a valid OTP");
								}
							}
						}
					} else if (role.equalsIgnoreCase("SuperAdmin")) {
						if (user.getAuthority().contains(Authorities.SUPER_ADMIN)) {
							if(!DeploymentConstants.PRODUCTION) {
								login.setValidate(true);
							}
							if (!login.isValidate()) {
								LoginError loginError = loginValidation.checkSuperAdminValidation(login, user);
								if (loginError.isSuccess()) {
									AuthenticationError authError = isValidUsernamePassword(login, request);
									if (authError.isSuccess()) {
										userApi.requestNewLoginDeviceForSuperAdmin(user);
										result.setStatus(ResponseStatus.NEW_DEVICE);
										result.setMessage("Please enter the received OTP");
									} else {
										result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
										result.setMessage(authError.getMessage());
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(loginError.getMessage());
								}
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								boolean isVerified = true;
								if(DeploymentConstants.PRODUCTION) {
									isVerified = userApi.isValidLoginToken(login.getMobileToken(), user);
								}
								if (isVerified) {
									if (sessionApi.checkActiveSession(user)) {
										AuthenticationError auth = authentication(login, request);
										if (auth.isSuccess()) {
											Map<String, Object> detail = new HashMap<String, Object>();
											Authentication authentication = SecurityContextHolder.getContext()
													.getAuthentication();
											sessionLoggingStrategy.onAuthentication(authentication, request, response);
											UserSession userSession = userSessionRepository.findByActiveSessionId(
													RequestContextHolder.currentRequestAttributes().getSessionId());
											UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
											result.setStatus(ResponseStatus.SUCCESS);
											result.setMessage("Login successful.");
											detail.put("sessionId", userSession.getSessionId());
											detail.put("userDetail", activeUser);
											detail.put("accountDetail", user.getAccountDetail());
											result.setDetails(detail);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										} else {
											result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
											result.setMessage(auth.getMessage());
											result.setDetails(null);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage("Not a valid OTP");
								}
							}
						}
					} else if (role.equalsIgnoreCase("Admin")) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)) {
							AuthenticationError auth = authentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								result.setDetails(detail);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						}
					} else if (role.equalsIgnoreCase("Merchant")) {
						System.err.println("role merchant");
						if (user.getAuthority().contains(Authorities.MERCHANT)) {
							if(user.getUsername().equalsIgnoreCase("udhaykumar8439@gmail.com")){
							AuthenticationError auth = canteenAuthentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								detail.put("merchantDetail", user.getMerchantDetails());
								result.setDetails(detail);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						  }else{
							  AuthenticationError auth = authentication(login, request);
								if (auth.isSuccess()) {
									Map<String, Object> detail = new HashMap<String, Object>();
									Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
									sessionLoggingStrategy.onAuthentication(authentication, request, response);
									UserSession userSession = userSessionRepository.findByActiveSessionId(
											RequestContextHolder.currentRequestAttributes().getSessionId());
									UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Login successful.");
									detail.put("sessionId", userSession.getSessionId());
									detail.put("userDetail", activeUser);
									detail.put("accountDetail", user.getAccountDetail());
									detail.put("merchantDetail", user.getMerchantDetails());
									result.setDetails(detail);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
									result.setMessage(auth.getMessage());
									result.setDetails(null);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							  
						  }
						}
					} else if (role.equalsIgnoreCase("Donatee")) {
						System.err.println("role Donatee");
						if (user.getAuthority().contains(Authorities.DONATEE)) {
							AuthenticationError auth = authentication(login, request);
							if (auth.isSuccess()) {
								Map<String, Object> detail = new HashMap<String, Object>();
								Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
								sessionLoggingStrategy.onAuthentication(authentication, request, response);
								UserSession userSession = userSessionRepository.findByActiveSessionId(
										RequestContextHolder.currentRequestAttributes().getSessionId());
								UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Login successful.");
								detail.put("sessionId", userSession.getSessionId());
								detail.put("userDetail", activeUser);
								detail.put("accountDetail", user.getAccountDetail());
								// detail.put("merchantDetail",user.getMerchantDetails());
								result.setDetails(detail);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage(auth.getMessage());
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						}
					}

					else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}

				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} catch (Exception e) {
				e.printStackTrace();
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("your request is declined please contact customer care or try again later");
				result.setDetails("your request is declined please contact customer care or try again later");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}

		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage(error.getMessage());
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Logout", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> logoutUserApi(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);

		if (isValidHash) {
			try {
				UserSession userSession = userSessionRepository.findBySessionId(session.getSessionId());
				if (userSession != null) {
					sessionApi.expireSession(session.getSessionId());
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("User logout successful");
					result.setDetails("Session Out");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} catch (Exception e) {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails("Failed, invalid request.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	private AuthenticationError isValidUsernamePassword(LoginDTO dto, HttpServletRequest request) {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			if (auth.isAuthenticated()) {
				error.setSuccess(true);
				error.setMessage("Valid Credentials");
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress()));
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
	}
	
	private AuthenticationError canteenAuthentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		UsernamePasswordAuthenticationToken token = null;
		try {
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			auth = authenticationManager.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (auth.isAuthenticated()) {
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				return error;
			} else {
				error.setSuccess(false);
				error.setMessage("you have entered wrong password .");
				SecurityContextHolder.getContext().setAuthentication(null);
				return error;
			}
		} catch (Exception e) {
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage("you have entered wrong password .");
			return error;
		}
	}

}
