package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.InviteFriendsDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.InviteFriendsError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.InviteFriendValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/InviteFriends")
public class UserAccountController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final InviteFriendValidation inviteFriendValidation;
	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;

	public UserAccountController(InviteFriendValidation inviteFriendValidation, IUserApi userApi,
			UserSessionRepository userSessionRepository) {
		this.inviteFriendValidation = inviteFriendValidation;
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
	}

	@RequestMapping(value = "/ProcessByEmail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> inviteFriendsByEmail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody InviteFriendsDTO friend, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(friend, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				InviteFriendsError error = new InviteFriendsError();
				String sessionId = friend.getSessionId();
				error = inviteFriendValidation.checkErrorByEmail(friend);
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (error.isValid()) {
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						String authority = user.getAuthority();
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Invitation Sent to " + friend.getEmail());
							result.setDetails("Invitation Sent to " + friend.getEmail());
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage(""+error);
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessByMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> inviteFriendsByMobile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody InviteFriendsDTO friend, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(friend, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = friend.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						String authority = user.getAuthority();
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							User dto = userApi.findByUserName(friend.getMobileNo());
							if (dto == null) {
								User sender = userApi.findByUserName(user.getUsername());
								String message = String.format("Dear %s, \n you\'re invited by %s (%s) to be a part of VPayQwik.\n Please download the latest app from iTunes using %s or from Play Store using %s . \n Thank you \n VPayQwik",friend.getMobileNo(),user.getFirstName(),user.getUsername(), CommonUtil.ITUNES_LINK,CommonUtil.PLAY_STORE_LINK);
								boolean isSent=userApi.inviteByMobile(friend.getMobileNo(),message,sender);
								if(isSent){
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Invitation Sent Successfully");
								result.setDetails("Invitation Sent Successfully");
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("User Already Invited");
									result.setDetails("User Already Invited");
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage("User Already Exists");
								result.setDetails("User Already Exists");
							}
								
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
					}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

}
