package com.payqwikapp.controller.api.v1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.IHouseJoyApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.HouseJoy;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.DateDTO;
import com.payqwikapp.model.FlightResponseDTO;
import com.payqwikapp.model.HouseJoyRequest;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class HouseJoyController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private IUserApi userApi;
	private PersistingSessionRegistry sessionRegistry;
	private UserSessionRepository userSessionRepository;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository pqServiceRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private IHouseJoyApi houseJoyApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	
	public HouseJoyController(IUserApi userApi, PersistingSessionRegistry sessionRegistry,
			UserSessionRepository userSessionRepository, TransactionValidation transactionValidation,
			PQServiceRepository pqServiceRepository,PGDetailsRepository pgDetailsRepository,IHouseJoyApi houseJoyApi,
			PersistingSessionRegistry persistingSessionRegistry) {
		super();
		this.userApi = userApi;
		this.sessionRegistry = sessionRegistry;
		this.userSessionRepository = userSessionRepository;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
		this.pgDetailsRepository=pgDetailsRepository;
		this.houseJoyApi=houseJoyApi;
		this.persistingSessionRegistry=persistingSessionRegistry;
	}


	@RequestMapping(value = "/HouseJoyInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> houseJoyInitiate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody HouseJoyRequest qwikRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			User user=userApi.findByUserName(qwikRequest.getMobileNo());
			User merchant = userApi.findByUserName(qwikRequest.getMerchantUserName());

			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(qwikRequest.getSessionId());
				if (userSession != null) {
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

							if(merchant!=null){
								PQService service = pqServiceRepository.findServiceByCode(StartupUtil.HOUSE_JOY_CODE);
								System.err.println("service:: "+service);

								if(service != null && Status.Active.equals(service.getStatus())) {

									TransactionError transactionError = transactionValidation.validateHouseJoyPayment(qwikRequest.getAmount(),
											user.getUsername(), service);
									System.err.println("error"+transactionError);
									if (transactionError.isValid()) {
										System.err.println("error is valid");
										String transactionRefNo  = houseJoyApi.initiateHouseJoyPayment(qwikRequest,user,
												service);
										result.setBalance(user.getAccountDetail().getBalance());
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage("Payment of HouseJoy Initiated Successful from " + user.getUsername() + " .");
										result.setTxnId(transactionRefNo); 
										result.setValid(true);
										result.setCode("S00");
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(transactionError.getMessage());
										result.setDetails(transactionError.getMessage());
										result.setTxnId("");
										result.setValid(false);
										result.setCode("F00");
										System.err.println("m herer where error is not valid");
									}

								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Service is under maintenance");
									result.setCode("F00");
									result.setDetails("Service is under maintenance");
								}
							} 
							else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed, Unauthorized Merchant.");
								result.setDetails("Failed, Unauthorized Merchant.");
								result.setTxnId("");
								result.setValid(false);
								result.setCode("F00");
							}
						}
						else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				}
				else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("session expired.Please login and try again.");
					result.setCode("F03");
					result.setDetails("Please login and try again.");
				}

			}
			else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("merchant doesn't exist...");
			result.setDetails("merchant doesn't exist...");
			result.setTxnId("");
			result.setValid(false);
			result.setCode("F00");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	} 


	@RequestMapping(value = "/HouseJoySuccess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> houseJoySuccess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody HouseJoyRequest qwikrPay, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			System.err.println("Inside HouseJoy Payment Success ");
			User user = userApi.findByUserName(qwikrPay.getMobileNo());

			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(qwikrPay.getSessionId());
				if (userSession != null) {
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.HOUSE_JOY_CODE);

							if (service!=null && Status.Active.equals(service.getStatus())) {

								TransactionResponse resp = houseJoyApi.successHouseJoyPayment(qwikrPay, service, user);

								if(resp.isSuccess()){
									result.setBalance(user.getAccountDetail().getBalance());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Payment of HouseJoy Successful from  " + user.getUsername() + " .");
									result.setDetails(resp);
									result.setTxnId(qwikrPay.getTransactionRefNo());
									result.setValid(false);
									result.setCode("S00");
								}else{
									result.setBalance(user.getAccountDetail().getBalance());
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(resp.getDetails());
									result.setDetails(resp);
									result.setTxnId("");
									result.setValid(false);
									result.setCode("F00");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Service is under maintenance");
								result.setCode("F00");
								result.setDetails("Service is under maintenance");
							}
						} 
						else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized User.");
							result.setDetails("Failed, Unauthorized User.");
							result.setTxnId("");
							result.setValid(false);
							result.setCode("F00");
						}
					}
					else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed,Unauthorized User");
						result.setCode("F00");
						result.setDetails("Failed,Unauthorized User");
					}
				}
				else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session expired.Please login and try again.");
					result.setCode("F03");
					result.setDetails("Please login and try again.");
				}
			}
			else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		}
		else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setCode("F00");
			result.setDetails("Unauthorized user.");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	} 



	@RequestMapping(value = "/HouseJoyCancel", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> houseJoyCancel(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody HouseJoyRequest qwikRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			User user=userApi.findByUserName(qwikRequest.getMobileNo());
			User merchant = userApi.findByUserName(qwikRequest.getMerchantUserName());
			if(merchant!=null){
				if (qwikRequest.getTransactionRefNo()!=null && !qwikRequest.getTransactionRefNo().isEmpty()) {
					System.err.println("m here where error is valid");
					TransactionResponse resp  = houseJoyApi.cancelHouseJoyPayment(qwikRequest.getTransactionRefNo());

					result.setBalance(user.getAccountDetail().getBalance());
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Payment of HouseJoy Canceld Successful from " + user.getUsername() + " .");
					result.setValid(true);
					result.setCode("S00");
					System.err.println(result);

					// {"status":null,"code":null,"message":null,"balance":2177.6099999999997,"details":null,"txnId":null}

				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Transaction not found");
					result.setDetails("Transaction not found");
					result.setTxnId("");
					result.setValid(false);
					result.setCode("F00");
					System.err.println("m herer where error is not valid");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed, Unauthorized Merchant.");
				result.setDetails("Failed, Unauthorized Merchant.");
				result.setTxnId("");
				result.setValid(false);
				result.setCode("F00");
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("merchant doesn't exist...");
			result.setDetails("merchant doesn't exist...");
			result.setTxnId("");
			result.setValid(false);
			result.setCode("F00");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/GetHouseJoyDetailsByDate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> getHouseJoyDetailsByDate(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		 FlightResponseDTO result=new FlightResponseDTO();
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						
						String from = dto.getFromDate() + " 00:00";
						String to = dto.getToDate() + " 23:59";
						
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<HouseJoy> list=houseJoyApi.getHouseJoyForAdminByDate(dateFormat.parse(from), dateFormat.parse(to));
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Flight Details received");
							result.setDetails(list);
							return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("no flight details found");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
		} 
	
	
	@RequestMapping(value = "/GetHouseJoyDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> getHouseJoyDetails(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		 FlightResponseDTO result=new FlightResponseDTO();
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<HouseJoy> list=houseJoyApi.getHouseJoyForAdmin();
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Flight Details received");
							result.setDetails(list);
							return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("no flight details found");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
		} 
	
}
