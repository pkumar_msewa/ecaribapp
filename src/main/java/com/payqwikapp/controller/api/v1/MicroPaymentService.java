package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.impl.TransactionApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.MicroPaymentDTO;
import com.payqwikapp.model.MicroPaymentServiceStatus;
import com.payqwikapp.model.MicroPaymentTransactionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.MicroPaymentError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.MicroPaymentValidation;
import com.payqwikapp.validation.TransactionValidation;
import com.thirdparty.util.MicroPaymentUtils;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class MicroPaymentService {

	private PersistingSessionRegistry persistingSessionRegistry;
	private UserSessionRepository userSessionRepository;
	private IUserApi userApi;
	private final TransactionValidation transactionValidation;
	private final TransactionApi transactionApi;
	private final PQServiceRepository pqServiceRepository;
	private final MicroPaymentValidation microPaymentValidation;
	private final ICommissionApi commissionApi;

	public MicroPaymentService(IUserApi userApi, PersistingSessionRegistry persistingSessionRegistry,
			UserSessionRepository userSessionRepository, TransactionValidation transactionValidation,
			TransactionApi transactionApi, PQServiceRepository pqServiceRepository,
			MicroPaymentValidation microPaymentValidation,ICommissionApi commissionApi) {
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.userSessionRepository = userSessionRepository;
		this.transactionValidation = transactionValidation;
		this.transactionApi = transactionApi;
		this.pqServiceRepository = pqServiceRepository;
		this.microPaymentValidation = microPaymentValidation;
		this.commissionApi=commissionApi;
				
		
	}

	@RequestMapping(value = "/GetNikkiToken", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processTopup(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MicroPaymentDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO resp = new ResponseDTO();
		String sessionId = dto.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)
					|| user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				JSONObject obj = new JSONObject();
				try {
					obj.put("secretKey", MicroPaymentUtils.generateSecret(user.getUsername(), sessionId));
					obj.put("tokenKey", MicroPaymentUtils.generateToken(sessionId));
				} catch (JSONException e) {
					resp.setDetails(obj.toString());
					resp.setCode("F01");
					resp.setMessage("Failure");
					resp.setStatus(ResponseStatus.BAD_REQUEST);
					resp.setError("True");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}

				System.out.println("Json Response :: " + obj.toString());
				resp.setDetails(obj.toString());
				resp.setCode("S00");
				resp.setMessage("Success");
				resp.setStatus(ResponseStatus.SUCCESS);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}
		resp.setMessage("Invalid SessionId");
		resp.setDetails("Failure");
		resp.setStatus(ResponseStatus.BAD_REQUEST);
		resp.setCode("F03");
		resp.setError("True");
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/IntinateNikkiTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> intinatNikkiTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MicroPaymentTransactionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ResponseDTO resp = new ResponseDTO();
		try {
			String sessionId = MicroPaymentUtils.getSessionId(dto.getSecretKey(), dto.getMobileNumber());
			if (sessionId != null) {
				MicroPaymentError paymentError = microPaymentValidation.checkError(dto);
				if (paymentError.isValid()) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode("NIKKI");
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,dto.getAmount());
							double netCommissionValue = commissionApi.getCommissionValue(commission,dto.getAmount());
							User u = userApi.findByUserName(StartupUtil.NIKKI_CHAT);
							if (dto.getAuthCode() != null) {
								if (dto.getConsumerTokenKey() != null
										&& MicroPaymentUtils.checkConsumerTokenKey(dto.getConsumerTokenKey())) {
									if (dto.getConsumerSecretKey() != null
											&& MicroPaymentUtils.checkConsumerSecretKey(dto.getConsumerSecretKey())) {
										if (dto.getTokenKey() != null
												&& MicroPaymentUtils.checkTokenKey(sessionId, dto.getTokenKey())) {
											if (dto.getSecretKey() != null && MicroPaymentUtils.checkSecretKey(
													user.getUsername(), sessionId, dto.getSecretKey())) {
												if (dto.getMobileNumber() != null
														&& user.getUsername().equals(dto.getMobileNumber())) {
													TransactionError transactionError = transactionValidation
															.validateBillPayment(dto.getAmount() + "",
																	user.getUsername(),commission,netCommissionValue);
													if (transactionError.isValid()) {
														String transactionRefNo = System.nanoTime() + "";
														transactionApi.initiateNikkiChatPayment(dto.getAmount(),
																"Payment through Chat of Rs." + dto.getAmount(),service,
																 transactionRefNo,user.getUsername(),u.getUsername(), dto.getAuthCode());
														resp.setStatus(ResponseStatus.SUCCESS);
														resp.setCode("S00");
														resp.setError("False");
														resp.setMessage("Success");
														resp.setTxnId(transactionRefNo);
														resp.setDetails("Transaction Intiated through Nikki Chat");
														return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
													} else {
														resp.setStatus(ResponseStatus.FAILURE);
														resp.setCode("F02");
														resp.setMessage(transactionError.getMessage());
														resp.setDetails(transactionError.getMessage());
														resp.setError("True");
														return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
													}
												} else {
													resp.setStatus(ResponseStatus.FAILURE);
													resp.setCode("F01");
													resp.setError("False");
													resp.setMessage("Invalid Mobile Number");
													resp.setDetails("Invalid Mobile Number");
													return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
												}
											} else {
												resp.setStatus(ResponseStatus.FAILURE);
												resp.setCode("F01");
												resp.setError("False");
												resp.setMessage("Invalid Secret Key");
												resp.setDetails("Invalid Secret Key");
												return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
											}
										} else {
											resp.setStatus(ResponseStatus.FAILURE);
											resp.setCode("F01");
											resp.setError("False");
											resp.setMessage("Invalid Secret Key");
											resp.setDetails("Invalid Secret Key");
											return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
										}
									} else {
										resp.setStatus(ResponseStatus.FAILURE);
										resp.setCode("F01");
										resp.setError("False");
										resp.setMessage("Invalid ConsumerSecret Key");
										resp.setDetails("Invalid Consumer Key");
										return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
									}
								} else {
									resp.setStatus(ResponseStatus.FAILURE);
									resp.setCode("F01");
									resp.setError("False");
									resp.setMessage("Invalid ConsumerToken Key");
									resp.setDetails("Invalid ConsumerToken Key");
									return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
								}
							} else {
								resp.setStatus(ResponseStatus.FAILURE);
								resp.setCode("F01");
								resp.setError("False");
								resp.setMessage("Invalid authcode");
								resp.setDetails("Invalid authcode");
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							}
						} else {
							resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							resp.setCode("F01");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setError("True");
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					} else {
						resp.setStatus(ResponseStatus.INVALID_SESSION);
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
					}
				} else {
					resp.setStatus(ResponseStatus.BAD_REQUEST);
					resp.setMessage("Failed, invalid request.");
					resp.setDetails(paymentError);
				}
			} else {
				resp.setStatus(ResponseStatus.BAD_REQUEST);
				resp.setCode("F03");
				resp.setMessage("Invalid Parameters");
				resp.setDetails("Failure");
				resp.setError("True");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.BAD_REQUEST);
			resp.setMessage("your Nikki Trnasaction request is declined please contact customer care .");
			resp.setDetails("your Nikki Trnasaction request is declined please contact customer care .");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateNikkiTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateeNikkiTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MicroPaymentServiceStatus dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO resp = new ResponseDTO();
		try {
			String sessionId = MicroPaymentUtils.getSessionId(dto.getSecretKey(), dto.getMobileNumber());
			if (sessionId != null) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (dto.getRetrivalReferenceNumber() != null) {
							if (dto.getConsumerTokenKey() != null
									&& MicroPaymentUtils.checkConsumerTokenKey(dto.getConsumerTokenKey())) {
								if (dto.getConsumerSecretKey() != null
										&& MicroPaymentUtils.checkConsumerSecretKey(dto.getConsumerSecretKey())) {
									if (dto.getTokenKey() != null
											&& MicroPaymentUtils.checkTokenKey(sessionId, dto.getTokenKey())) {
										if (dto.getSecretKey() != null && MicroPaymentUtils
												.checkSecretKey(user.getUsername(), sessionId, dto.getSecretKey())) {
											if (dto.getMobileNumber() != null
													&& user.getUsername().equals(dto.getMobileNumber())) {
												if (dto.isStatus()) {
													transactionApi.successNikkiChatPayment(dto.getTransactionRefNo(),dto.getRetrivalReferenceNumber());
													resp.setCode("S00");
													resp.setError("False");
													resp.setMessage("Success");
													resp.setDetails("Transaction Success through Nikki Chat");
													resp.setStatus(ResponseStatus.SUCCESS);
													return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
												} else {
													transactionApi.failedNikkiChatPayment(dto.getTransactionRefNo());
													resp.setCode("S00");
													resp.setError("False");
													resp.setMessage("Success");
													resp.setDetails("Transaction Failed through Nikki Chat");
													resp.setStatus(ResponseStatus.SUCCESS);
													return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
												}
											} else {
												resp.setCode("F01");
												resp.setError("False");
												resp.setMessage("Invalid Mobile Number");
												resp.setDetails("Invalid Mobile Number");
												resp.setStatus(ResponseStatus.FAILURE);
												return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
											}
										} else {
											resp.setCode("F01");
											resp.setError("False");
											resp.setMessage("Invalid Secret Key");
											resp.setDetails("Invalid Secret Key");
											resp.setStatus(ResponseStatus.FAILURE);
											return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
										}
									} else {
										resp.setCode("F01");
										resp.setError("False");
										resp.setMessage("Invalid Secret Key");
										resp.setDetails("Invalid Secret Key");
										resp.setStatus(ResponseStatus.FAILURE);
										return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
									}
								} else {
									resp.setCode("F01");
									resp.setError("False");
									resp.setMessage("Invalid ConsumerSecret Key");
									resp.setDetails("Invalid Consumer Key");
									resp.setStatus(ResponseStatus.FAILURE);
									return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
								}
							} else {
								resp.setCode("F01");
								resp.setError("False");
								resp.setMessage("Invalid ConsumerToken Key");
								resp.setDetails("Invalid ConsumerToken Key");
								resp.setStatus(ResponseStatus.FAILURE);
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							}
						} else {
							resp.setCode("F01");
							resp.setError("False");
							resp.setMessage("Invalid RetrivalReference Number");
							resp.setDetails("Invalid RetrivalReference Number");
							resp.setStatus(ResponseStatus.FAILURE);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					} else {
						resp.setCode("F01");
						resp.setMessage("Failed, Unauthorized user.");
						resp.setDetails("Failed, Unauthorized user.");
						resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						resp.setError("True");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				} else {
					resp.setStatus(ResponseStatus.BAD_REQUEST);
					resp.setCode("F03");
					resp.setMessage("Invalid Token Key");
					resp.setDetails("Failure");
					resp.setError("True");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			} else {
				resp.setStatus(ResponseStatus.BAD_REQUEST);
				resp.setCode("F03");
				resp.setMessage("Invalid Parameters");
				resp.setDetails("Failure");
				resp.setError("True");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.BAD_REQUEST);
			resp.setMessage("your Nikki Trnasaction request is declined please contact customer care .");
			resp.setDetails("your Nikki Trnasaction request is declined please contact customer care .");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

		}
	}

	@RequestMapping(value = "/RefundNikkiTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> refundNikkiTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MicroPaymentServiceStatus dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO resp = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase("User")) {
				if (dto.getRetrivalReferenceNumber() != null) {
					if (dto.getConsumerTokenKey() != null
							&& MicroPaymentUtils.checkConsumerTokenKey(dto.getConsumerTokenKey())) {
						if (dto.getConsumerSecretKey() != null
								&& MicroPaymentUtils.checkConsumerSecretKey(dto.getConsumerSecretKey())) {
							if (!dto.isStatus()) {
								resp = transactionApi.processNikiChatRefund(dto);
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							}
						} else {
							resp.setCode("F01");
							resp.setError("False");
							resp.setMessage("Invalid ConsumerSecret Key");
							resp.setDetails("Invalid Consumer Key");
							resp.setStatus(ResponseStatus.FAILURE);
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					} else {
						resp.setCode("F01");
						resp.setError("False");
						resp.setMessage("Invalid ConsumerToken Key");
						resp.setDetails("Invalid ConsumerToken Key");
						resp.setStatus(ResponseStatus.FAILURE);
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				} else {
					resp.setCode("F01");
					resp.setError("False");
					resp.setMessage("Invalid RetrivalReference Number");
					resp.setDetails("Invalid RetrivalReference Number");
					resp.setStatus(ResponseStatus.FAILURE);
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			} else {
				resp.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				resp.setMessage("Failed, Unauthorized user.");
				resp.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(ResponseStatus.BAD_REQUEST);
			resp.setMessage("your Nikki Trnasaction request is declined please contact customer care .");
			resp.setDetails("your Nikki Trnasaction request is declined please contact customer care .");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

}
