package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.IYuppTVApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.entity.YTVAccess;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.YupTVDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class YuppTVController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	private final IUserApi userApi;
	private final IYuppTVApi yuppTVApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final TransactionValidation transactionValidation;

	public YuppTVController(IUserApi userApi,IYuppTVApi yuppTVApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry,PQServiceRepository pqServiceRepository,TransactionValidation transactionValidation) {
		this.userApi = userApi;
		this.yuppTVApi=yuppTVApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository=pqServiceRepository;
		this.transactionValidation=transactionValidation;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		// TODO Auto-generated method stub

	}

	@RequestMapping(method = RequestMethod.POST, value = "/SaveRegister", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankTransferRequestsFiltered(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody YupTVDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = u.getAccountDetail();
						if (account != null) {
							YTVAccess access = yuppTVApi.isYupTVAccountSaved(dto, account);
							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("Userdetail", access);
							result.setMessage(dto.getMessage());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setDetails(detail);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Unauthorized user.");
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Session invalid.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				}
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Unable to process request");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}

		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CheckRegister", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> CheckRegister(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody YupTVDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = u.getAccountDetail();
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.YuppTv_SERVICE);
						if (account != null) {
							boolean isexist = yuppTVApi.checkRegister(dto, account);
							if (isexist) {
								YTVAccess ytvAccess = yuppTVApi.findByUniqueId(dto.getUnique_id());
								YTVAccess access = yuppTVApi.findByExpiryDate(ytvAccess.getExpiryDate());
								if (access.getExpiryDate().before(new Date())) {
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Already Subscribed." + "your expiry date is "
											+ ytvAccess.getExpiryDate().toString().substring(0, 10));
									result.setDetails(ytvAccess);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.RENEWAL_ACTIVE);
									result.setMessage("Now You can Renew your Subscription.");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} else {
								TransactionError transactionError = transactionValidation.validateYuppTvTransaction(
                                        dto.getAmount(), user.getUsername(), dto.getUnique_id(), service);
								if(transactionError.isValid()){
							    PQTransaction transaction=yuppTVApi.placeSubscriptionForYuppTV(dto,service, u.getUsername());
								result.setStatus(ResponseStatus.NEW_ACTIVATION);
								result.setMessage("You can Register Now");
								result.setTxnId(transaction.getTransactionRefNo());
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage(transactionError.getMessage());
									result.setDetails(result.getMessage());
									result.setTxnId(result.getMessage());
								}
							}
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ExistRegister", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> ExistRegister(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody YupTVDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						PQAccountDetail account = u.getAccountDetail();
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.YuppTv_SERVICE);
						if (account != null) {
							boolean isexist = yuppTVApi.checkExistUser(dto, account);
							if (isexist) {
								YTVAccess access = yuppTVApi.updateYtvAccess(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Updated Successfully.");
								result.setDetails(access);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								YTVAccess access = yuppTVApi.isYupTVAccountSaved(dto, account);
//								TransactionError transactionError = transactionValidation.validateYuppTvTransaction(
//                                        dto.getAmount(), user.getUsername(), dto.getUnique_id(), service);
//								if(transactionError.isValid()){
									PQTransaction transaction=yuppTVApi.reverseSubscriptionForYuppTV(dto,service, u.getUsername());
									Map<String, Object> detail = new HashMap<String, Object>();
									detail.put("userDetail", access);
									detail.put("Transaction", transaction);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Insert Successfully.");
								result.setDetails(detail);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
//								} else {
//									result.setStatus(ResponseStatus.FAILURE);
//									result.setMessage("Insufficent Balance");
//									result.setDetails(transactionError.getMessage());
//								}
							}
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
}
