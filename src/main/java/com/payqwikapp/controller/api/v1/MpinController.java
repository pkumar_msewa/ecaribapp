package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.ForgotMpinDTO;
import com.payqwikapp.model.MpinChangeDTO;
import com.payqwikapp.model.MpinDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.ChangeMpinError;
import com.payqwikapp.model.error.ForgotMpinError;
import com.payqwikapp.model.error.MpinError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.MpinValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class MpinController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final MpinValidation mpinValidation;
	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ISessionApi sessionApi;
	private final IRefernEarnLogsApi refernEarnLogsApi;

	public MpinController(MpinValidation mpinValidation, IUserApi userApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, ISessionApi sessionApi,
			IRefernEarnLogsApi refernEarnLogsApi) {
		this.mpinValidation = mpinValidation;
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.sessionApi = sessionApi;
		this.refernEarnLogsApi = refernEarnLogsApi;
	}

	@RequestMapping(value = "/SetMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> setNewMpin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MpinDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		MpinError error = mpinValidation.validateNewMpin(dto);
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							dto.setUsername(user.getUsername());
							result = userApi.setMpinHistory(dto);
							if (result.getCode().equalsIgnoreCase("S00") || result.getCode().equalsIgnoreCase("S01")
									|| result.getCode().equalsIgnoreCase("S02")) {
								boolean updated = userApi.setNewMpin(dto);
								if (updated) {
									result.setDetails("Mpin Updated");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setDetails("MPIN not updated");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}

				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangeMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> changeOldMpin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MpinChangeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ChangeMpinError error = mpinValidation.validateChangeMpin(dto);
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (error.isValid()) {
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							dto.setUsername(user.getUsername());
							String mpin = dto.getOldMpin();
							try {
								MpinDTO mpinDto = new MpinDTO();
								mpinDto.setUsername(dto.getUsername());
								mpinDto.setNewMpin(dto.getNewMpin());
								String hashedMpin = SecurityUtil.sha512(mpin);
								if (user.getMpin().equals(hashedMpin)) {
									if (!(user.getMpin().equals(SecurityUtil.sha512(mpinDto.getNewMpin())))) {
										result = userApi.setMpinHistory(mpinDto);
										if (result.getCode().equalsIgnoreCase("S00")
												|| result.getCode().equalsIgnoreCase("S01")
												|| result.getCode().equalsIgnoreCase("S02")) {
											boolean updated = userApi.changeCurrentMpin(dto);
											if (updated) {
												result.setStatus(ResponseStatus.SUCCESS);
												result.setMessage("MPIN is updated successfully.");
												result.setDetails("MPIN is updated successfully.");
											} else {
												result.setMessage("Failed, to update MPIN. Please try again later.");
												result.setDetails("Failed, to update MPIN. Please try again later.");
											}
										} else {
											result.setStatus(ResponseStatus.FAILURE);
											result.setDetails("MPIN not updated");
										}
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage("You Can't Enter Same MPIN as oldMPIN.");
										result.setDetails("Failed, invalid request.");
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Failed, Old MPIN is incorrect. Please enter correct MPIN.");
									result.setDetails("Failed, invalid request.");
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Failed, invalid request.");
				result.setDetails(error);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/VerifyMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> verifyMpin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MpinChangeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		try {
			if (isValidHash) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							dto.setUsername(user.getUsername());
							if (dto.getOldMpin() != null) {
								String hasedMpin = SecurityUtil.sha512(dto.getOldMpin());
								if (user.getMpin().equals(hasedMpin)) {
									User u = userApi.findByUserName(user.getUsername());
									RefernEarnlogs logs = refernEarnLogsApi.getlogs(u);
									if (logs != null) {
										if (logs.isHasEarned()) {
											result.setHasRefer(true);
										} else {
											result.setHasRefer(false);
										}
									}
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("MPIN Verified.");
									result.setDetails("MPIN Verified.");
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage(userApi.handleMpinFailure(request, user.getUsername()));
									result.setDetails("MPIN Mismatch !");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("This is not your MPIN. Please enter your correct MPIN.");
								result.setDetails("This is not your MPIN. Please enter your correct MPIN.");
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_HASH);
				result.setMessage("Failed, Please try again later.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ForgotMpin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> forgotMpinRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ForgotMpinDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user != null) {
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User u = userApi.findByUserName(user.getUsername());
						ForgotMpinError error = mpinValidation.checkError(dto, u);
						if (error.isValid()) {
							boolean isDeleted = userApi.deleteCurrentMpin(user.getUsername());
							if (isDeleted) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Please reset your MPIN.");
								result.setDetails("Please reset your MPIN.");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Please try again later.");
								result.setDetails("Please try again later.");
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("MPIN not deleted");
							result.setDetails(error);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Please, login and try again.");
				result.setDetails("Please, login and try again.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
}
