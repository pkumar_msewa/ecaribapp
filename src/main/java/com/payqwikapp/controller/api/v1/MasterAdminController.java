package com.payqwikapp.controller.api.v1;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.letsManage.model.dto.BulkMailRequestDTO;
import com.letsManage.model.dto.BulkSmsRequest;
import com.letsManage.model.dto.GCMDto;
import com.letsManage.model.dto.GCMResponseDTO;
import com.letsManage.model.dto.GetTransDTO;
import com.letsManage.model.dto.GetUsersDto;
import com.letsManage.model.dto.MailRequestDTO;
import com.letsManage.model.dto.PassWordSuperAdminDTO;
import com.letsManage.model.dto.ResponseLetsDTO;
import com.letsManage.model.dto.ResponseStatusLetsMan;
import com.letsManage.model.dto.SmsRequest;
import com.letsManage.model.dto.SuperAdminAddDTO;
import com.letsManage.model.dto.UserTypeDto;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.ApproveKycDTO;
import com.payqwikapp.model.UserInfoRequest;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.admin.UserListDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.Authorities;


@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/VpayQwikProjectInfo")
public class MasterAdminController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final IUserApi userApi;
	
	public MasterAdminController( IUserApi userApi) {
	
		this.userApi= userApi;
		
	}
	@RequestMapping(value = "/ListCount", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> getListCount(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getListCount(key);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/userTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> getuserTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getAllUserList(key);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
  
	@RequestMapping(value = "/DebitTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> gedebitTansactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getdebitTransactionsList(key);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
  
	@RequestMapping(value = "/CreditTransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> gecreditTansactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getcreditTransactionsList(key);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
  

	@RequestMapping(value = "/ServiceList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> getServiceType(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getServiceList(key);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
  
	
	@RequestMapping(value = "/DateWiseTrans", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> getDateWiseTrans(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody GetTransDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getTransDTO(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/SuperAdminList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> getsuperadminListTansactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key,@RequestBody UserTypeDto dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getSuperAdminList(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SuperBlockList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> SuperBlockList(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key,@RequestBody UserTypeDto dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.getSuperAdminBlockList(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addSuperAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> addSuperAdmin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody SuperAdminAddDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.addSuperAdmin(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/ChPwdSuperAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> changePwdSuperAdmin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody PassWordSuperAdminDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  System.err.println("inside change password");
				result = userApi.changePwdSuperAdmin(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/BlockSuperAdmin", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> blockSuperAdmin(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody PassWordSuperAdminDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.blockSuperAdmin(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}


	
	@RequestMapping(method = RequestMethod.POST, value = "/UserProfile", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> UserProfile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,@RequestBody UserInfoRequest dto,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			if(key.equals("5uChIt4TiW4R11510731296976")){
				result = userApi.getAllUserInfo(dto.getUsername());
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("User profile");
				}
			else{
				 result.setCode("F00");
 				result.setStatus(ResponseStatus.BAD_REQUEST);
 				result.setMessage("Key does not mATCH");
			 }
			} 
		else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	
}
	@RequestMapping(method = RequestMethod.POST, value = "/blockUser/{userName}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> blockuser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "userName") String userName, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
				 userApi.blockUser(userName);
						result.setCode("S00");
						result.setStatus(ResponseStatusLetsMan.SUCCESS);
						result.setMessage("User blocked");
					
					}
			 else{
				 result.setCode("F00");
  				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
			
}
	
	@RequestMapping(method = RequestMethod.POST, value = "/unBlockUser/{userName}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> unblockuser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "userName") String userName, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
				 User currentUser = userApi.findByUserName(userName);
					if (currentUser != null) {
						if (currentUser.getUserType().equals(UserType.User)) {
							String authority = Authorities.USER + "," + Authorities.AUTHENTICATED;
							userApi.updateUserAuthority(authority, currentUser.getId());
							result.setStatus(ResponseStatusLetsMan.SUCCESS);
							result.setMessage("User Unlocked");
						} else {
							result.setStatus(ResponseStatusLetsMan.FAILURE);
							result.setMessage("You can't unlock " + currentUser.getUserType() + " Type User");
						}
						result.setCode("S00");
						result.setStatus(ResponseStatusLetsMan.SUCCESS);
						result.setMessage("User blocked");
					
					}
					else {
						result.setStatus(ResponseStatusLetsMan.FAILURE);
						result.setMessage("User not found");
					}
					}
			 else{
				 result.setCode("F00");
  				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
			
}

	
	@RequestMapping(method = RequestMethod.POST, value = "/kycUser/{userName}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> KycUpdate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "userName") String userName, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
				 ApproveKycDTO dto1=new ApproveKycDTO();
				 dto1.setUsername(userName);
				 dto1.setSuccess(false);
				 result = userApi.kycUpdate(dto1);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
			 else{
				 result.setCode("F00");
  				result.setStatus("Bad Request");
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus("Un-Authorized Role");
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			
}
	
	@RequestMapping(method = RequestMethod.POST, value = "/nonKycUser/{userName}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> NonKycUpdate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "language") String language,@PathVariable(value = "userName") String userName, 
			@RequestHeader(value = "SecretKey",required = true) String key,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
				 ApproveKycDTO dto1=new ApproveKycDTO();
				 dto1.setUsername(userName);
				 dto1.setSuccess(true);
				 result = userApi.kycUpdate(dto1);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
			 else{
				 result.setCode("F00");
  				result.setStatus("Bad Request");
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus("Un-Authorized Role");
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			
}
	@RequestMapping(value = "/SendMail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> sendMial(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody MailRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.sendmail(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendBulkMail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> sendbulkMail(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody BulkMailRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.sendbulkmail(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendSms", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> sendSms(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody SmsRequest dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.sendsms(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendBulkSms", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	ResponseEntity<ResponseLetsDTO> sendbulkSms(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key, @RequestBody BulkSmsRequest dto, HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			  
				result = userApi.sendbulksms(key,dto);
			} 
		else {
			result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetUserList/{userType}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> getUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "userType") String userType, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,HttpServletRequest request, HttpServletResponse response) {
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
			GetUsersDto dto1= new GetUsersDto();
		 List<UserListDTO> listDTO = userApi.getTodayUsers(userType);
			dto1.setListdto(listDTO);
						result.setCode("S00");
						result.setStatus(ResponseStatusLetsMan.SUCCESS);
						result.setMessage("Get all user list");
						result.setDetails(dto1);
					}
			 else{
				 result.setCode("F00");
  				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
			
}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetUserListFilter/{userType}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> getUserfilter(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "userType") String userType, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,@RequestBody GetTransDTO dto,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
			GetUsersDto dto1= new GetUsersDto();
			Date startDate = dateFormat.parse(dto.getFrom());
			Date endDate = dateFormat.parse(dto.getTo());
		 List<UserListDTO> listDTO = userApi.getUsersBetween(startDate, endDate, userType);
			dto1.setListdto(listDTO);
						result.setCode("S00");
						result.setStatus(ResponseStatusLetsMan.SUCCESS);
						result.setMessage("Get all user list datewise");
						result.setDetails(dto1);
					}
			 else{
				 result.setCode("F00");
  				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
			
}
	@RequestMapping(method = RequestMethod.POST, value = "/GCMNotification", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseLetsDTO> GCMNotification(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,@PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey",required = true) String key,@RequestBody GCMDto dto,HttpServletRequest request, HttpServletResponse response)
					throws Exception{
		ResponseLetsDTO result = new ResponseLetsDTO();
		if (role.equalsIgnoreCase("ROLE_MASTERADMIN")) {
			 if(key.equals("5uChIt4TiW4R11510731296976")){
				 {    System.out.println("gcm");
						Pageable gcmPage = new PageRequest(dto.getPage(), dto.getSize());
							Page<String> gcmList = userApi.getGCMIDs(gcmPage);
							GCMResponseDTO dto1=new GCMResponseDTO();
							List<String> listDTO=gcmList.getContent();
							dto1.setListdto(listDTO);
							long totalpg=gcmList.getTotalPages();
							dto1.setTotalPages(totalpg);
							result.setStatus(ResponseStatusLetsMan.SUCCESS);
							result.setMessage("GCM IDs");
							result.setDetails(dto1);
						} 
					}
			 else{
				 result.setCode("F00");
  				result.setStatus(ResponseStatusLetsMan.BAD_REQUEST);
  				result.setMessage("Key does not mATCH");
			 }
				 } 
				else {
					result.setStatus(ResponseStatusLetsMan.UNAUTHORIZED_ROLE);
					result.setMessage("Failed, Unauthorized user.");
					result.setDetails("Failed, Unauthorized user.");
				}
				return new ResponseEntity<ResponseLetsDTO>(result, HttpStatus.OK);
			
}
	}

