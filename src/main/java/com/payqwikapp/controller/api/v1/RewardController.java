package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IPromoCodeApi;
import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.FetchMobileList;
import com.payqwikapp.model.FlightDetailsDTO;
import com.payqwikapp.model.FlightResponseDTO;
import com.payqwikapp.model.RedeemDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.RefernEarnRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.RegisterValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Reward")
public class RewardController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final RegisterValidation registerValidation;
	private final IPromoCodeApi promoCodeApi;
	private final IRefernEarnLogsApi refernEarnLogsApi;
	private final RefernEarnRepository refernEarnRepository;

	

	public RewardController(IUserApi userApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry,RegisterValidation registerValidation,IPromoCodeApi promoCodeApi,IRefernEarnLogsApi refernEarnLogsApi,RefernEarnRepository refernEarnRepository) {
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.registerValidation=registerValidation;
		this.promoCodeApi=promoCodeApi; 
		this.refernEarnLogsApi=refernEarnLogsApi;
		this.refernEarnRepository=refernEarnRepository;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Process", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateProfile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("User | Reward");
							result.setDetails("Rewarded");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} catch (Exception e) {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("User | Reward");
							result.setDetails(e.getMessage());
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("User | Reward");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("User | Reward");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("User | Reward");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	} 
	
	@RequestMapping(method = RequestMethod.POST, value = "/ReferNearn", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> referEarn(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody FetchMobileList fetchMobile, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		int count=0;
		RegisterError error = new RegisterError();
		RegisterDTO dto=new RegisterDTO();
			if (role.equalsIgnoreCase("User")) {
				String sessionId = fetchMobile.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					User tobRedeemed=userApi.findById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							Map<String, String> mobilelist=fetchMobile.getMobileList();
							if(!mobilelist.isEmpty()){
								for (Entry<String, String> entry : mobilelist.entrySet()) {
									System.out.println("username = " + entry.getKey() + ", firstname = " + entry.getValue());
									dto.setUsername(entry.getKey());
									dto.setContactNo(entry.getKey());
									dto.setFirstName(entry.getValue());
									dto.setPassword("123456");
									dto.setConfirmPassword("123456");
									dto.setUserType(UserType.User);
									dto.setLocationCode("560037");
									dto.setLastName(" ");
									dto.setReferee(tobRedeemed.getUserDetail().getFirstName());
									error=registerValidation.validateRefernEarnUser(dto);
									if(error.isValid()){
									User u=userApi.saveUserForReferNEarn(dto);
									count++;
									}
									System.err.println("errorrrrrrrrr "+ error.getMessage());
								}
							
							//	if(count>=50){
								System.err.println("hi m here");
								RefernEarnlogs logs=refernEarnLogsApi.getlogs(tobRedeemed);
								if(logs!=null){
									if(logs.isHasEarned()==false){
										if(logs.getReferalCount()+count>=50){
											boolean valid=refernEarnLogsApi.sendRefernEarnMoney(20,tobRedeemed);
											if(valid){
												logs.setReferalCount(count+logs.getReferalCount());
												logs.setHasEarned(true);
												refernEarnRepository.save(logs);
												result.setCode("S00");
												result.setMessage("Earned Rs 20");
												result.setStatus(ResponseStatus.SUCCESS);
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
 
											}
										}else{
												System.err.println("this is the count::"+(count+logs.getReferalCount()));
												logs.setReferalCount(count+logs.getReferalCount());
												refernEarnRepository.save(logs);
												result.setCode("F00");
												result.setStatus(ResponseStatus.FAILURE);
												result.setMessage("you need to refer "+(50-(count+logs.getReferalCount()))+" more");
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}
									}else{
										result.setCode("F00");
										result.setMessage("You have already earned the referal amount");
										result.setStatus(ResponseStatus.FAILURE);
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									}
								}else{
									if(count>=50){
										logs=new RefernEarnlogs();
										logs.setHasEarned(false);
										logs.setReferalCount(count);
										logs.setUser(tobRedeemed);
										refernEarnRepository.save(logs);
										boolean valid=refernEarnLogsApi.sendRefernEarnMoney(20, tobRedeemed);
										if(valid){
										RefernEarnlogs lk=	refernEarnLogsApi.getlogs(tobRedeemed);
										lk.setHasEarned(true);
										refernEarnRepository.save(lk);
										result.setCode("S00");
										result.setMessage("congrats you have earned Rs 20");
										result.setStatus(ResponseStatus.SUCCESS);
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}

									}else{
										logs=new RefernEarnlogs();
										logs.setHasEarned(false);
										logs.setReferalCount(count);
										logs.setUser(tobRedeemed);
										refernEarnRepository.save(logs);
										result.setCode("F00");
										result.setMessage("you need to refer "+(50-count)+" more");
										result.setStatus(ResponseStatus.FAILURE);
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									}
								}
								/*}else{*//*
									RefernEarnlogs lgs=refernEarnLogsApi.getlogs(tobRedeemed);
									if(lgs!=null){
										if(lgs.isHasEarned()){
											result.setCode("F00");
											result.setMessage("You already earned Rs 20");
											result.setStatus(ResponseStatus.FAILURE);
											return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
										}else{
											if((lgs.getReferalCount()+count)>=50){
											boolean valid=refernEarnLogsApi.sendRefernEarnMoney(20,tobRedeemed);
												if(valid){
												lgs.setHasEarned(true);
												lgs.setReferalCount(lgs.getReferalCount()+count);
												refernEarnRepository.save(lgs);
												}											
												result.setCode("S00");
												result.setMessage("Congrats you have earned Rs 20");
												result.setStatus(ResponseStatus.SUCCESS);
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											}else{
												
												result.setCode("F00");
												result.setStatus(ResponseStatus.FAILURE);
												String.format("you need to refer %d more to unlock", (50-count));
												result.setMessage(String.format("you need to refer %d more to unlock", (50-count)));
												return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
											}
									}
									}else{
									lgs=new RefernEarnlogs();
									lgs.setHasEarned(false);
									lgs.setReferalCount(count);
									lgs.setUser(tobRedeemed);
									refernEarnRepository.save(lgs);
									result.setCode("F00");
									result.setStatus(ResponseStatus.FAILURE);
									String.format("you need to refer %d more to unlock", (50-count));
									result.setMessage(String.format("you need to refer %d more to unlock", (50-count)));
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
								*/
							}else{
								result.setCode("F00");
								result.setMessage("You need to refer your friends to earn Rs 20");
								result.setStatus(ResponseStatus.FAILURE);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

							}
						
								
						}catch (Exception e) {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Opps!! Something is not right but it's not you..it's us");
							result.setDetails(e.getMessage());
							e.printStackTrace();
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted | Unauthorized");
				result.setDetails("Permission Not Granted | Unauthorized");
				return new ResponseEntity<ResponseDTO>(result,  HttpStatus.OK);
			}
		
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/GetRefernEarnLogs", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> getRefernEarnLogsForAdmin(@RequestBody SessionDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response) 
			 throws ParseException {
		 FlightResponseDTO result=new FlightResponseDTO();
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<RefernEarnlogs> list=refernEarnRepository.getLogs();
						if(list!=null){
							result.setCode("S00");
							result.setMessage("Refern Earn Details received");
							result.setDetails(list);
							return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
						}else{
						result.setCode("F00");
						result.setMessage("no RefernEarn details found");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
						
					} else {
						result.setMessage("Unauthorized user.");
						result.setCode("F00");
						return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setMessage("Session invalid.");
					result.setCode("F00");
					return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setCode("F00");
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<FlightResponseDTO>(result, HttpStatus.OK);
		} 
	
}

