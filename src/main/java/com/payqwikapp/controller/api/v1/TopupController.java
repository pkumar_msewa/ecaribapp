package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.model.response.TransactionResponse;
import com.instantpay.model.response.ValidationResponse;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IDataConfigApi;
import com.payqwikapp.api.ITopupAndBillPaymentApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.DataConfig;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TopupType;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.MobileTopupValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/MobileTopup")
public class TopupController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IUserApi userApi;
	private final ITopupAndBillPaymentApi topupAndBillPaymentApi;
	private final MobileTopupValidation mobileTopupValidation;
	private final TransactionValidation transactionValidation;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final UserSessionRepository userSessionRepository;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;
	private final IDataConfigApi dataConfigApi;
	private final IValidationApi ipayValidationApi;

	public TopupController(IUserApi userApi, ITopupAndBillPaymentApi topupAndBillPaymentApi,
			MobileTopupValidation mobileTopupValidation, TransactionValidation transactionValidation,
			PersistingSessionRegistry persistingSessionRegistry, UserSessionRepository userSessionRepository,
			PQServiceRepository pqServiceRepository,ICommissionApi commissionApi,IDataConfigApi dataConfigApi,IValidationApi ipayValidationApi) {
		this.userApi = userApi;
		this.topupAndBillPaymentApi = topupAndBillPaymentApi;
		this.mobileTopupValidation = mobileTopupValidation;
		this.transactionValidation = transactionValidation;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.userSessionRepository = userSessionRepository;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi=commissionApi;
		this.dataConfigApi = dataConfigApi;
		this.ipayValidationApi = ipayValidationApi;
		
		
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	//this is for prepaid, postpaid and datacard (updated on 17th feb. 2018 by vinit jain)
	@RequestMapping(value = "/ProcessPrepaid", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processTopup(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CommonRechargeDTO topup, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(topup, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Agent")) {
					String sessionId = topup.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					if (user!=null && user.getAuthority().contains(Authorities.AUTHENTICATED) && (user.getAuthority().contains(Authorities.AGENT)
							|| user.getAuthority().contains(Authorities.USER))) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						PQService service = pqServiceRepository.findServiceByCode(topup.getServiceProvider());
						if(service!=null && Status.Active.equals(service.getStatus())) {
						PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(topup.getAmount()));
						double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(topup.getAmount()));
							TransactionError transactionError = transactionValidation
									.validateBillPayment(topup.getAmount(), user.getUsername(),commission,netCommissionValue);
							if (transactionError.isValid()) {
								String desc = "";
								if(TopupType.Prepaid.equals(topup.getTopupType())) {
									desc = "Prepaid Topup to " + topup.getMobileNo();
								}
								else if(TopupType.Postpaid.equals(topup.getTopupType())) {
									desc = "Postpaid Topup to " + topup.getMobileNo();
								}
								else if(TopupType.DataCard.equals(topup.getTopupType())) {
									desc = "Datacard Topup to " + topup.getMobileNo();

								}	
								DataConfig  config = dataConfigApi.getDataConfigByType("Recharge Biller", Status.Active);
								if(!(config!=null && Status.Active.getValue().equalsIgnoreCase(config.getMdex()))) {
									ValidationRequest vrequest = new ValidationRequest();
									vrequest.setSpKey(topup.getServiceProvider());
									vrequest.setAmount(topup.getAmount());
									vrequest.setAccount(topup.getMobileNo());
									vrequest.setAgentId(System.currentTimeMillis() + "");
									ValidationResponse vresponse = ipayValidationApi.validationRequest(vrequest);
									if(!vresponse.isSuccess()) {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage(vresponse.getIpayErrorDesc());
										result.setDetails(vresponse.getIpayErrorDesc());
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									}
								}
								TransactionResponse ipayResponse = topupAndBillPaymentApi.prepaidTopup(topup,
										user.getUsername(), service ,commission,netCommissionValue,config,desc);
								String msg = (ipayResponse.isSuccess() ? ipayResponse.getTransaction().getResMsg()
										: ipayResponse.getValidation().getIpayErrorDesc());
								result.setStatus(
										(ipayResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
								result.setMessage(msg);
								result.setDetails(msg);
								result.setBalance(userApi.getWalletBalance(user));
								userApi.updateRequestLog(user,timeStamp);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setMessage(transactionError.getMessage());
								result.setCode(transactionError.getCode());
								result.setRemBalance(transactionError.getSplitAmount());
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
					}else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Service down for maintenance,Please try after some time.");
						result.setDetails("Service down for maintenance,Please try after some time.");
					}
				} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
	} else {
		result.setStatus(ResponseStatus.INVALID_HASH);
		result.setMessage("Failed, Please try again later.");
	}
	return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
 }
}
