package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IRefernEarnLogsApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.RefernEarnlogs;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.util.Authorities;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/RefernEarn")
public class RefernEarnLogsController {

	private final IRefernEarnLogsApi refernEarnLogsApi;
	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	public RefernEarnLogsController(IRefernEarnLogsApi refernEarnLogsApi, IUserApi userApi,
			UserSessionRepository userSessionRepository) {
		super();
		this.refernEarnLogsApi = refernEarnLogsApi;
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
	}

	@RequestMapping(value = "/checkReferNEarn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseDTO> checkRefernEarn(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("User")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				User user = userApi.findById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							
						RefernEarnlogs logs=refernEarnLogsApi.getlogs(user);
						if(logs!=null){
							if(logs.isHasEarned()){
								result.setCode("S00");
								result.setMessage("this user has already earned Rs20");
							}
							else{
								result.setCode("F00");
								result.setMessage("this user has not earned Rs 20");
							}
						}else{
							result.setCode("F01");
							result.setMessage("User has not referred any of his friends");
						}
							
					}else{
						result.setCode("F02");
						result.setMessage("Unauthorised User");
					}
			}else{
				result.setCode("F03");
				result.setMessage("Please login and try again");
			}
		}else{
			result.setCode("F04");
			result.setMessage("Unauthorised User");
		}
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}
}
