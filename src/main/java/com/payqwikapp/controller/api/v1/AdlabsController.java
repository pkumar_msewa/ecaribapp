package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.model.request.AdlabsOrderRequest;
import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.IAdlabsApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class AdlabsController {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private IAdlabsApi adlabsApi;
	private IUserApi userApi;
	private PersistingSessionRegistry sessionRegistry;
	private UserSessionRepository userSessionRepository;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository pqServiceRepository;

	public AdlabsController(IAdlabsApi adlabsApi, IUserApi userApi, PersistingSessionRegistry sessionRegistry,
			UserSessionRepository userSessionRepository, TransactionValidation transactionValidation,
			PQServiceRepository pqServiceRepository) {

		this.adlabsApi = adlabsApi;
		this.userApi = userApi;
		this.sessionRegistry = sessionRegistry;
		this.userSessionRepository = userSessionRepository;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
	}

	@RequestMapping(value = "/AdlabsOrderInitiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> AdlabsorderInitiate(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AdlabsOrderRequest adlab, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		
		System.err.println(" Inside Adlab Inintate ::::::::::Controleer ");
		boolean isValidHash = SecurityUtil.isHashMatches(adlab, hash);
		if (isValidHash) {
			System.err.println("AMOUNT:::::::::::::::"+adlab.getAmount());
			if (role.equalsIgnoreCase("User")) {
				String sessionId = adlab.getSessioniId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

						//User u = userApi.findByUserName(user.getUsername());
						User receiver = userApi.findByUserName(StartupUtil.Adlabs);
						//u.setUsername(adlab.getServiceCode());
						System.err.println("Sender User ::::::::::::::Startu pup util "+user.getUsername());
						System.err.println("Receiver User :: " + receiver.getUsername());
						
						
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.AdlabsCode);
						System.err.println("Service code ;;------------------------------------- " + service.getCode());
						TransactionError transactionError = transactionValidation.validateAdlabs(adlab.getAmount(),
								user.getUsername(), service);
						if (transactionError.isValid()) {
							String transactionRefNo = adlabsApi.adlabspaymentInitiate(adlab, user.getUsername(),
									service, receiver.getUsername());
							User sender = userApi.findByUserName(user.getUsername());
							result.setBalance(sender.getAccountDetail().getBalance());
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Payment Adlabs Initiate Successful " + user.getEmail() + " .");
							result.setTxnId(transactionRefNo);

						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Insufficent Balance");
							result.setDetails(transactionError.getMessage());
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

	@RequestMapping(value = "/AdlabsOrderSuccess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> AdlabsorderSuccess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AdlabsOrderRequest adlab, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(adlab, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = adlab.getSessioniId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {

						User u = userApi.findByUserName(user.getUsername());
						adlab.setUser(u);

						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.AdlabsCode);
						
						TransactionResponse transactionRefNo = adlabsApi.adlabspaymentSuccess(adlab, user.getUsername(),
								service, u);

						result.setBalance(u.getAccountDetail().getBalance());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Payment Adalbs  Successful " + user.getEmail() + " .");
						result.setDetails(transactionRefNo);
						

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
				}

			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}

}