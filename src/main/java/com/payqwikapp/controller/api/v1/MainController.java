package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.ISessionLogApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.impl.TransactionApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.TransactionDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class MainController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final ISessionLogApi sessionLogApi;
	private final TransactionApi transactionApi;
	private final PQServiceRepository pqServiceRepository;
	
	public MainController(IUserApi userApi, UserSessionRepository userSessionRepository, ISessionLogApi sessionLogApi,
			TransactionApi transactionApi,PQServiceRepository pqServiceRepository) {
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.sessionLogApi = sessionLogApi;
		this.transactionApi = transactionApi;
		this.pqServiceRepository=pqServiceRepository;
		
	}

	@RequestMapping(value = "/Authenticate/SessionId", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> authenticateUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO session, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(session, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {

				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					boolean requestValid = sessionLogApi.checkRequest(sessionId, request);
					if (requestValid) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Session valid");
						Map<String, Object> detail = new HashMap<String, Object>();
						detail.put("authority", user.getAuthority());
						detail.put("user", user);
						detail.put("merchantDetail", userSession.getUser().getMerchantDetails());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Failed, Please try again later.");
						Map<String, Object> detail = new HashMap<String, Object>();
						// detail.put("authority", null);
						detail.put("user", null);
						result.setDetails(detail);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					Map<String, Object> detail = new HashMap<String, Object>();
					// detail.put("authority", null);
					detail.put("user", null);
					result.setDetails(detail);
				}
			} else if (role.equalsIgnoreCase("Agent")) {
				String sessionId = session.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					boolean requestValid = sessionLogApi.checkRequest(sessionId, request);
					if (requestValid) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Session valid");
						Map<String, Object> detail = new HashMap<String, Object>();
						detail.put("authority", user.getAuthority());
						detail.put("user", user);
						detail.put("merchantDetail", userSession.getUser().getMerchantDetails());
						result.setDetails(detail);
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Failed, Please try again later.");
						Map<String, Object> detail = new HashMap<String, Object>();
						// detail.put("authority", null);
						detail.put("user", null);
						result.setDetails(detail);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					Map<String, Object> detail = new HashMap<String, Object>();
					// detail.put("authority", null);
					detail.put("user", null);
					result.setDetails(detail);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				Map<String, Object> detail = new HashMap<String, Object>();
				// detail.put("authority", null);
				detail.put("user", null);
				result.setDetails(detail);
			}

		}

		else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/TestDTH", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> addCurrency(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		// boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (true) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				User user1 = userApi.findByUserName(dto.getSenderUsername());
				// }
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					Map<String, Object> detail = new HashMap<String, Object>();
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							// transactionApi.initiateBillPaymentNew(dto.getAmount(),
							// dto.getDescription(), service,
							// dto.getTransactionRefNo(),
							// dto.getReceiverUsername(),
							// dto.getSenderUsername());
						}

						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Bill Payment Initiated");
						detail.put("sessionId", userSession.getSessionId());
						result.setDetails(detail);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/cehckService", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> cehckService(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
							
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Service Check");
							result.setCode("S00");
							result.setDetails(service.getStatus());
							
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed,Unauthorized User");
							result.setCode("F00");
							result.setDetails("Failed,Unauthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please login and try again.");
					result.setCode("F00");
					result.setDetails("Please login and try again.");
				}
			}
			else if(role.equalsIgnoreCase("Agent")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.AGENT)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
								
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Service Check");
								result.setCode("S00");
								result.setDetails(service.getStatus());
								
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed,Unauthorized User");
								result.setCode("F00");
								result.setDetails("Failed,Unauthorized User");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please login and try again.");
						result.setCode("F00");
						result.setDetails("Please login and try again.");
					}
				}else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setCode("F00");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
}
