package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.RedeemDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.PayQwikUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class RedeemPointsController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final UserSessionRepository userSessionRepository;
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final TransactionValidation transactionValidation;
	private final ITransactionApi iTransactionApi;
	private final IMailSenderApi mailSenderApi;

	public RedeemPointsController(UserSessionRepository userSessionRepository, IUserApi userApi,
			PersistingSessionRegistry persistingSessionRegistry, PQServiceRepository pqServiceRepository,
			PQAccountDetailRepository pqAccountDetailRepository, TransactionValidation transactionValidation,
			ITransactionApi iTransactionApi, IMailSenderApi mailSenderApi) {
		this.userSessionRepository = userSessionRepository;
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.transactionValidation = transactionValidation;
		this.iTransactionApi = iTransactionApi;
		this.mailSenderApi = mailSenderApi;
	}

	@RequestMapping(value = "/RedeemPointProcess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> redeeem(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RedeemDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String sessionId = dto.getSessionId();
				int getpoints = Integer.parseInt(dto.getPoints()); // Get points
																	// through
																	// User
																	// input
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					User user = userApi.findById(userSession.getUser().getId());
					String receiverUsername = user.getUsername();
					if (user.getAuthority().contains("USER") && user.getAuthority().contains("AUTHENTICATED")) {
						PQAccountDetail detail = user.getAccountDetail();
						double balance = detail.getBalance();
						// String receiverName=user.getUsername();
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.REDEEM_POINT_CASH);
						TransactionError error = transactionValidation.validateRedeemBalance(balance,
								user.getUsername());
						String description = "";
						if (error.isSuccess()) {
							long points = detail.getPoints(); // Get points// through User // database
							if (getpoints < points || getpoints == points) {
								if (getpoints >= 500 || getpoints >= 1000) {
									long div = getpoints / 500;
									long point = (div * 500);
									balance = balance + (div * 50);
									long bal = (div * 50);
									points = points - point;
									description = point + " Redeem points Successfully convert to cash";
									User sender = userApi.findByUserName(PayQwikUtil.PROMOCODE_NUMBER);
									PQAccountDetail senderAccount = sender.getAccountDetail();
									double senderBalance = senderAccount.getBalance();
									if (senderBalance >= bal) {
										iTransactionApi.successRedeemMoney(bal, description, service, receiverUsername,point);
										System.out.println("After mailSenderApi and smsSenderApi");
										result.setMessage(description);
										detail.setBalance(balance);
										detail.setPoints(points);
										pqAccountDetailRepository.save(detail);
										result.setStatus(ResponseStatus.SUCCESS);
										result.setDetails(detail);
									} else {
										result.setStatus(ResponseStatus.BAD_REQUEST);
										result.setMessage("Please try again after some time");
										result.setDetails(null);
										return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
									}
								} else {
									result.setStatus(ResponseStatus.BAD_REQUEST);
									result.setMessage("Enter greater than or equal to 500 redeemPoints");
									result.setDetails(null);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} else {
								result.setStatus(ResponseStatus.BAD_REQUEST);
								result.setMessage("Insufficient Points");
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("Please try again");
							result.setDetails(error);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}
}