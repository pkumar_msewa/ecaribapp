package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.ISessionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.impl.TransactionApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TokenKey;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.DirectPaymentGetBalanceDTO;
import com.payqwikapp.model.LoginDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.AuthenticationError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.TokenRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.session.SessionLoggingStrategy;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.validation.TransactionValidation;
import com.thirdparty.util.MicroPaymentUtils;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class DirectPaymentService {

	private IUserApi userApi;
	private UserSessionRepository userSessionRepository;
	private final AuthenticationManager authenticationManager;
	private final ISessionApi sessionApi;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final TokenRepository tokenRepository;
	private PersistingSessionRegistry persistingSessionRegistry;
	private final TransactionApi transactionApi;
	private final TransactionValidation transactionValidation;
	private final PQServiceRepository pqServiceRepository;
	private final PQTransactionRepository pQTransactionRepository;
	private final ICommissionApi commissionApi;


	public DirectPaymentService(IUserApi userApi, UserSessionRepository userSessionRepository,
			AuthenticationManager authenticationManager, ISessionApi sessionApi,
			SessionLoggingStrategy sessionLoggingStrategy, TokenRepository tokenRepository,
			PersistingSessionRegistry persistingSessionRegistry, TransactionApi transactionApi,
			TransactionValidation transactionValidation, PQServiceRepository pqServiceRepository,
			PQTransactionRepository pQTransactionRepository,ICommissionApi commissionApi) {
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.authenticationManager = authenticationManager;
		this.sessionApi = sessionApi;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.tokenRepository = tokenRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.transactionApi = transactionApi;
		this.transactionValidation = transactionValidation;
		this.pqServiceRepository = pqServiceRepository;
		this.pQTransactionRepository = pQTransactionRepository;
		this.commissionApi=commissionApi;


	}
	

	@RequestMapping(value = "/GetUserBalance", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserBalance(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DirectPaymentGetBalanceDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO resp = new ResponseDTO();
		TransactionError error = transactionValidation.validateGetUserBalance(dto);
		if(error.isValid()) {
			User user = userApi.findByUserName(dto.getUserName());
			if (user != null) {
				if (user.getAuthority().contains(Authorities.USER)) {
					if (user.getAuthority().contains(Authorities.LOCKED)) {
						resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						resp.setMessage("Account is Locked,Contact Customer Care");
						resp.setDetails("Account is Locked,Contact Customer Care");
					} else if (user.getAuthority().contains(Authorities.BLOCKED)) {
						resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						resp.setMessage("Account is Blocked, Please wait for 12 hrs");
						resp.setDetails("Account is Blocked, Please wait for 12 hrs");
					} else if (sessionApi.checkActiveSession(user)) {
//						user.setPassword();
						LoginDTO login = new LoginDTO();
						login.setUsername(user.getUsername());
						login.setPassword("123456");
						authentication(login, request);
						Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
						System.err.println("authentication ::" + authentication);
						sessionLoggingStrategy.onAuthentication(authentication, request, response);
						resp.setStatus(ResponseStatus.SUCCESS);
						UserSession userSession = userSessionRepository
								.findByActiveSessionId(RequestContextHolder.currentRequestAttributes().getSessionId());
						resp.setBalance(user.getAccountDetail().getBalance());
						userApi.OTPForDirectPayment(user);
						resp.setCode("S00");
						resp.setError("False");
						resp.setMessage("Success");
						resp.setDetails(user.getUserDetail().getFirstName() + " " + user.getUserDetail().getLastName());
						resp.setTxnId(MicroPaymentUtils.generatePayloSecret(user.getUsername(), userSession.getSessionId()));
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
			} else {
					resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					resp.setMessage("not a user");
					resp.setDetails("not a user");
				}

			} else {
				resp.setCode("FO1");
				resp.setDetails("User not exist");
				resp.setError("True");
				resp.setMessage("Failure");
				resp.setStatus(ResponseStatus.FAILURE);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
			} else {
			resp.setStatus(ResponseStatus.BAD_REQUEST);
			resp.setMessage(error.getMessage());
			resp.setDetails(error.getMessage());
		}
			return new ResponseEntity<ResponseDTO>(resp,HttpStatus.OK);
	}

	private AuthenticationError authentication(LoginDTO dto, HttpServletRequest request)
			throws ServletException, IOException, Exception {
		AuthenticationError error = new AuthenticationError();
		Authentication auth = null;
		System.err.println("inside authentication");
		UsernamePasswordAuthenticationToken token = null;
		try {
			System.err.println("password ::" + dto.getPassword());
			token = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
			System.err.println("token generated:: "+token);
//			auth = authenticationManager.authenticate(token);
			System.err.println("auth is "+auth);
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if (true) {
				System.err.println("inside true auth");
				securityContext.setAuthentication(auth);
				SecurityContextHolder.getContext().setAuthentication(auth);
				HttpSession session = request.getSession(true);
				session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				error.setSuccess(true);
				error.setMessage("Login successful.");
				userApi.handleLoginSuccess(request, null, auth, String.valueOf(token.getPrincipal()),
						dto.getIpAddress());
				System.err.println("returning error");
				return error;
			}
		} catch (Exception e) {
			System.err.println("inside catch block");
			e.printStackTrace();
			SecurityContextHolder.getContext().setAuthentication(null);
			error.setSuccess(false);
			error.setMessage(userApi.handleLoginFailure(request, null, auth, String.valueOf(token.getPrincipal()),
					dto.getIpAddress()));
			return error;
		}
		return error;
	}

	@RequestMapping(value = "/InitiateTransation", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> initiateTransation(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DirectPaymentGetBalanceDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO resp = new ResponseDTO();
		TransactionError error = transactionValidation.validateGetUserBalance(dto);
		if(error.isValid()) {
			String sessionId = MicroPaymentUtils.getPayloSessionId(dto.tokenKey, dto.getUserName());
			if (sessionId != null) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)
							|| user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						if (userApi.checkMobileTokenForDirectPayment(dto.getOtp(), user.getUsername())) {
							TokenKey token = tokenRepository.getTokenByConsumerKeyAndConsumerToken(
									dto.getConsumerSecretKey(), dto.getConsumerTokenKey());
							if (token != null) {
								PQService service = pqServiceRepository.findServiceByCode("PAYLO");
								PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,dto.getAmount());
								double netCommissionValue = commissionApi.getCommissionValue(commission,dto.getAmount());
								TransactionError transactionError = transactionValidation
										.validateBillPayment(dto.getAmount() + "", user.getUsername(),commission,netCommissionValue);
								if (transactionError.isValid()) {
									String transactionRefNo = System.nanoTime() + "";
									persistingSessionRegistry.refreshLastRequest(sessionId);
									transactionApi.initiateNikkiChatPayment(dto.getAmount(),
											"Payment through Paylo of Rs." + dto.getAmount(), service, transactionRefNo,
											user.getUsername(), user.getUsername(), dto.transactionRefNo);
									resp.setStatus(ResponseStatus.SUCCESS);
									resp.setCode("S00");
									resp.setError("False");
									resp.setMessage("Success");
									resp.setTxnId(transactionRefNo);
									resp.setDetails("Transaction Intiated through Paylo");
									return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
								} else {
									resp.setCode("F01");
									resp.setError("True");
									resp.setMessage("OTP Mismatch");
									resp.setStatus(ResponseStatus.SUCCESS);
									resp.setDetails("Please enter a vaild OTP");
									return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
								}

							} else {
								resp.setCode("F01");
								resp.setError("True");
								resp.setMessage("Invalid Credentials");
								resp.setStatus(ResponseStatus.SUCCESS);
								resp.setDetails("Parameters, ConsumerSecretKey and ConsumerTokenKey are not valid");
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							}
						}
						resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						resp.setCode("F01");
						resp.setMessage("Failed, Invalid OTP.");
						resp.setDetails("Failed, Invalid OTP");
						resp.setError("True");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
					resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					resp.setCode("F01");
					resp.setMessage("Failed, Unauthorized user.");
					resp.setDetails("Failed, Unauthorized user.");
					resp.setError("True");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}

				resp.setCode("FO1");
				resp.setDetails("Invaild Token Key");
				resp.setError("True");
				resp.setMessage("Failure");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setDetails("Invaild Tokenkey Parameter");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

			}
			resp.setCode("FO1");
			resp.setError("True");
			resp.setMessage("Failure");
			resp.setStatus(ResponseStatus.FAILURE);
			resp.setDetails("Invaild Tokenkey Parameter");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		} else {
			resp.setStatus(ResponseStatus.BAD_REQUEST);
			resp.setMessage(error.getMessage());
			resp.setDetails(error.getMessage());
			return new ResponseEntity<ResponseDTO>(resp,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateTransation", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> UpdateTransation(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DirectPaymentGetBalanceDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO resp = new ResponseDTO();
		TransactionError error = transactionValidation.validateGetUserBalance(dto);
		if (error.isValid()) {
			String sessionId = MicroPaymentUtils.getPayloSessionId(dto.tokenKey, dto.getUserName());
			if (sessionId != null) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)
							|| user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						if (true) {
							TokenKey token = tokenRepository.getTokenByConsumerKeyAndConsumerToken(
									dto.getConsumerSecretKey(), dto.getConsumerTokenKey());
							if (token != null) {
								PQService service = pqServiceRepository.findServiceByCode("PAYLO");
								persistingSessionRegistry.refreshLastRequest(sessionId);

								PQTransaction transaction = pQTransactionRepository
										.findByTransactionRefNoAndStatus(dto.getTransactionRefNo() + "D", Status.Initiated);
								if (transaction != null) {
									if (dto.getStatus().equalsIgnoreCase("Success")) {
										transactionApi.successPayloPayment(dto.getTransactionRefNo(),
												dto.getRetrivalReferenceNo());
										resp.setStatus(ResponseStatus.SUCCESS);
										resp.setCode("S00");
										resp.setError("False");
										resp.setMessage("Success");
										resp.setTxnId(dto.getTransactionRefNo());
										resp.setDetails("Transaction Successful through Paylo");
										return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
									} else {
										transactionApi.failedPayloPayment(dto.getTransactionRefNo());
										resp.setStatus(ResponseStatus.SUCCESS);
										resp.setCode("S00");
										resp.setError("False");
										resp.setMessage("Transaction Updated Successfully");
										resp.setDetails("Transaction Updated Successfully");
										return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
									}
								} else {
									resp.setCode("F01");
									resp.setError("True");
									resp.setMessage("Please Enter Valid Transaction Id");
									resp.setStatus(ResponseStatus.FAILURE);
									resp.setDetails("Transaction Updated Successfully");
									return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
								}
							} else {
								resp.setCode("F01");
								resp.setError("True");
								resp.setMessage("Invalid Credentials");
								resp.setStatus(ResponseStatus.SUCCESS);
								resp.setDetails("Parameters, ConsumerSecretKey and ConsumerTokenKey are not valid");
								return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
							}
						}
						resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						resp.setCode("F01");
						resp.setMessage("Failed, Invalid OTP.");
						resp.setDetails("Failed, Invalid OTP");
						resp.setError("True");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
					resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					resp.setCode("F01");
					resp.setMessage("Failed, Unauthorized user.");
					resp.setDetails("Failed, Unauthorized user.");
					resp.setError("True");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}

				resp.setCode("FO1");
				resp.setDetails("Invaild Token Key");
				resp.setError("True");
				resp.setMessage("Failure");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setDetails("Invaild Tokenkey Parameter");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

			}
			resp.setCode("FO1");
			resp.setDetails("Invaild Mobile Number or TokenKey");
			resp.setError("True");
			resp.setMessage("Failure");
			resp.setStatus(ResponseStatus.FAILURE);
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		} else {
			resp.setStatus(ResponseStatus.BAD_REQUEST);
			resp.setMessage(error.getMessage());
			resp.setDetails(error.getMessage());
			return new ResponseEntity<ResponseDTO>(resp,HttpStatus.OK);
		}
	}
}
