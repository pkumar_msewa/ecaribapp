package com.payqwikapp.controller.api.v1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwik.visa.utils.EncDecryptionRequest;
import com.payqwik.visa.utils.SecurityTest;
import com.payqwik.visa.utils.VisaGetAccountNumber;
import com.payqwik.visa.utils.VisaModel;
import com.payqwik.visa.utils.YappayEncryptedResponse;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.api.impl.TransactionApi;
import com.payqwikapp.entity.POSMerchants;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.VisaDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.POSMerchantsRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.repositories.VisaTransactionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.TransactionValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.request.MerchantBankDetailReq;
import com.thirdparty.model.request.MerchantReq;
import com.thirdparty.model.request.SettlementBankDetailReq;
import com.thirdparty.model.request.VisaOnBoadingMerchantReq;
import com.thirdparty.util.CryptLib;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/Mvisa")
public class VisaController {

	private final IUserApi userApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final TransactionApi transctionApi;
	private final VisaTransactionRepository visaTransactionRepository;
	private final TransactionValidation transactionValidation;
	private final ICommissionApi commissionApi;
	private final POSMerchantsRepository pOSMerchantsRepository;

	public VisaController(IUserApi userApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, PQServiceRepository pqServiceRepository,
			TransactionApi transctionApi, VisaTransactionRepository visaTransactionRepository,
			TransactionValidation transactionValidation,ICommissionApi commissionApi,POSMerchantsRepository pOSMerchantsRepository) {
		this.userApi = userApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.transctionApi = transctionApi;
		this.visaTransactionRepository = visaTransactionRepository;
		this.transactionValidation = transactionValidation;
		this.commissionApi=commissionApi;
		this.pOSMerchantsRepository=pOSMerchantsRepository;
	}

	@RequestMapping(value = "/VisaProcess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> VisaProcess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaDTO visaRequest, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) throws Exception {
		System.out.println("i am in apppppp::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		System.out.println("Encrypted text :::::::::::::::::::::::::::: "+visaRequest.getRequest());

		VisaDTO visa = new VisaDTO();
		ResponseDTO result = new ResponseDTO();
		try{
		if(device.equalsIgnoreCase("Ios")){
			String key = "VIJAYSTHJKUOKNGA";
			String iv = "1234567890123456";
			System.out.println("Inside IOS Type request");
			JSONObject jsonObject=new JSONObject(visaRequest.getRequest());
			String encryptedRequest = jsonObject.getString("request").trim();
			System.err.println("encryptedRequest ::" + encryptedRequest);
			System.err.println("key ::" + key);
			visa=CryptLib.getVisaDTOIOS(encryptedRequest,key,iv);
		}else{
			System.out.println("Inside Andoid request ");
			visa=EncDecryptionRequest.getVisaDTO(visaRequest.getRequest());
		}
		//VisaDTO visa = EncDecryptionRequest.getVisaDTO(visaRequest.getRequest());
		System.err.println("session ID "+visaRequest.toString());
		System.out.println("Visa 1 "+visa.getAmount());
		System.out.println("Visa 2 "+visa.getData());
		System.out.println("Visa 3 "+visa.getExternalTransactionId());
		System.out.println("Visa 4 "+visa.getMerchantAddress());
		System.out.println("Visa 5 "+visa.getMerchantId());
		System.out.println("Visa 6 "+visa.getMerchantName());
		System.out.println("Visa 7 "+visa.getRequest());
		System.out.println("Visa 8 "+visa.getSessionId());
		System.out.println("Visa 9 "+visa.getTransactionStatus());
		System.out.println("Visa 1 0 "+visa.getObject());
		System.out.println("Additional Data :: "+visa.getAdditionalData());
		
		boolean isValidHash = SecurityUtil.isHashMatches(visa, hash);
		System.out.println("i am before valid ahash:::::::::::::::::::::::::::::::::::");
		if (isValidHash) {
			System.out.println("hash is valid:::::::::::::::::::::::::::::::::::::::::");
			if (role.equalsIgnoreCase("User")) {
					String sessionId = visa.getSessionId();
					System.err.println("this is sessionId:::::::::"+sessionId);
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {						//++++++++++++++++++++++++++++++++++
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode("MVISA");
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,visa.getAmount());
							double netCommissionValue = commissionApi.getCommissionValue(commission,visa.getAmount());
							TransactionError transactionError = transactionValidation
									.validateBillPayment(visa.getAmount() + "", user.getUsername(),commission,netCommissionValue);
							if (transactionError.isValid()) {
								String transactionRefNo = "VB" + System.currentTimeMillis() + "";
								VisaModel visaModel = new VisaModel();
								visaModel.setFirstName(user.getFirstName());
								visaModel.setLastName(user.getLastName());
								visaModel.setContactNo(user.getUsername());
								visaModel.setSenderCardNo(visa.getAccountNumber()); //userSession.getUser().getAccountDetail().getAccountNumber()
								visaModel.setBillRefNo("INVOICE0001");
								visaModel.setToEntityId("");
								visaModel.setFromEntityId(user.getUsername());
								visaModel.setProductId("GENERAL");
								visaModel.setDescription(visa.getMerchantName() + " " + visa.getMerchantId() + " "
										+ visa.getMerchantAddress());
								visaModel.setAmount(visa.getAmount());
								visaModel.setTransactionType("C2M");
								visaModel.setTransactionOrigin("MOBILE");
								visaModel.setBusinessEntityId("VIJAYA");
								visaModel.setBusinessType("VIJAYA");
								visaModel.setExternalTransactionId(transactionRefNo);
								visaModel.setAdditionalData(visa.getAdditionalData());
								System.err.println("QR code is :::"+visa.getRequest());
								visaModel.setMerchantData(visa.getRequest());
								
								ObjectMapper mapper = new ObjectMapper();
								String req = mapper.writeValueAsString(visaModel);
								
								User u = userApi.findByUserName(StartupUtil.MVISA);
								POSMerchants merchant =pOSMerchantsRepository.findByMerchantId(visa.getMerchantId());
								if(merchant!=null){
									User merchantUser=userApi.findByUserName("M"+merchant.getMobileNumber());
									if(merchantUser!=null){
								transctionApi.initiateVisaMerchantPayment(visa.getAmount(),
										visa.getMerchantName() + " " + visa.getMerchantId() + " "
												+ visa.getMerchantAddress(),
										service, transactionRefNo, user.getUsername(), u.getUsername(),merchantUser.getUsername());
									}
								}else{
									transctionApi.initiateVisaPayment(visa.getAmount(),
											visa.getMerchantName() + " " + visa.getMerchantId() + " "
													+ visa.getMerchantAddress(),
													service, transactionRefNo, user.getUsername(), u.getUsername());	
								}
								SecurityTest sec = new SecurityTest();
								String enc = sec.encodeRequest(req, "9876543211234562", "VIJAYA");
								result.setCode(transactionRefNo);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(visa.getSessionId());
								result.setDetails(enc);
								result.setTxnId(transactionRefNo);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						  }else{
							result.setCode("F05");
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(transactionError.getMessage());
							result.setTxnId(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						  }
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("User Not Authenticated");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Session Expired,Please Login Again.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
			  }
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ResponseProcess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> VisaResponseProcess(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaDTO visa, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(visa, hash);
		if (isValidHash) {
			/*System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@1 "+visa.getAmount());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@2"+visa.getData());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@3"+visa.getRequest());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@4"+visa.getObject());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@5"+visa.getTransactionStatus());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@6"+visa.getTxRef());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@7"+visa.getObject());
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@8"+(String)visa.getRequest());*/
			if (role.equalsIgnoreCase("User")) {
				String sessionId = visa.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						PQService service = pqServiceRepository.findServiceByCode("MVISA");
						if (service != null) {
							SecurityTest sec = new SecurityTest();
							ObjectMapper mapper = new ObjectMapper();
							String decrypted = sec.decodeResponse(mapper.readValue(visa.getObject().toString(), YappayEncryptedResponse.class));
							System.out.println("Decrypted :: "+decrypted);
							org.json.JSONObject obj = new org.json.JSONObject(decrypted);
							System.out.println("Object ..................................."+obj);
							org.json.JSONObject fetchStatus = new org.json.JSONObject((String)visa.getRequest());
							System.out.println("FETCHSTATUS 1.......................... :: "+fetchStatus);
							try {
								System.out.println("INSIDE TRY BLOCK     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+obj.toString());
								int txId = (int) obj.getJSONObject("result").get("txId");
								System.out.println("Transaction  2.......................... :: "+txId);
								String txref = fetchStatus.getJSONObject("result").getJSONObject("transaction").getString("txRef");
								System.out.println("txref 3 .............................................."+txId);
								String transactionStatus = fetchStatus.getJSONObject("result").getJSONObject("transaction").getString("transactionStatus");
								System.out.println("transactionStatus 4 .................................."+transactionStatus);
								String transactionAmount = fetchStatus.getJSONObject("result").getJSONObject("transaction").getString("amount");
								System.out.println("transactionAmount 5 .................................."+transactionAmount);
								if (txId != 0 && transactionStatus.equals("PAYMENT_SUCCESS") && (txId+"").equals(txref)) {
									JSONObject r = obj.getJSONObject("result");
									String authCode = null;
									String retreivalRef = null;
									try{
										authCode = r.getString("authCode");
										retreivalRef = r.getString("retrivalReferenceNo"); 
									}catch(Exception ex) {
										ex.printStackTrace();
										authCode  = (authCode==null)? null:authCode;
										retreivalRef = (retreivalRef == null)? null:retreivalRef;
									}
									transctionApi.successVisaPayment(visa.getData(),authCode, retreivalRef);
									visaTransactionRepository.updateVisaTransactionRefNo(txId + "", visa.getData());
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Transaction Successful.");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									transctionApi.failedVisaPayment(visa.getData());
									visaTransactionRepository.updateVisaTransactionRefNo(visa.getData(), null);
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Failure");
									result.setCode("F01");
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
							} catch (JSONException e) {
								e.printStackTrace();
								transctionApi.failedVisaPayment(visa.getData());
								visaTransactionRepository.updateVisaTransactionRefNo(visa.getData(), null);
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Payment Failure from Merchant End");
								result.setCode("F02");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								 
							} catch (Exception e) {
							    e.printStackTrace();
							 	transctionApi.failedVisaPayment(visa.getData());
								visaTransactionRepository.updateVisaTransactionRefNo(visa.getData(), null);
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Transaction Failed, Please try again later");
								result.setCode("F02");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("User Not Authenticated");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Invalid Session Id");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/AccountNumberRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> accountNumberRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaDTO visa, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) throws Exception {
		System.err.println("inside account number request controller");
		ResponseDTO resp = new ResponseDTO();
		String sessionId = visa.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			System.err.println("user session is not null");
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				System.err.println("authority is user");
				VisaGetAccountNumber req = new VisaGetAccountNumber();
				req.setFirstName(user.getFirstName());
				req.setLastName(user.getLastName());
				req.setContactNo(user.getUsername());
				req.setEntityId(user.getUsername());
				req.setProductId("GENERAL");
//				req.setEntityId("CUST001");
				req.setBusiness("VIJAYA");
				ArrayList arr = new ArrayList();
//				arr.add("VISA");
//				arr.add("MASTER");
				arr.add("RUPAY");
				req.setNetwork(arr);
				
				System.err.println("VisaGetAccountNumber request is prepared");
				ObjectMapper mapper = new ObjectMapper();
				String req1 = mapper.writeValueAsString(req);
				System.err.println("mapper is used for json conversion");
				SecurityTest sec = new SecurityTest();
				String enc = sec.encodeRequest(req1, "9876543211234562", "VIJAYA");
				System.out.println("Message comming from encrypted is  :: "+enc);
				resp.setCode("S00");
				resp.setMessage(enc);
				resp.setStatus(ResponseStatus.SUCCESS);
				System.err.println("yes aa gya");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}else {
				resp.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				resp.setMessage("Failed, Invalid Authority");
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION);
			resp.setMessage("Failed,Invalid Session");
		}
		return new ResponseEntity<ResponseDTO>(resp,HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/AccountNumberDecrypt", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> accountNumberDecrypt(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaDTO visa, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) throws Exception {
		ResponseDTO dto = new ResponseDTO();
		SecurityTest sec = new SecurityTest();
		ObjectMapper mapper = new ObjectMapper();
		String decrypted = sec.decodeResponse(mapper.readValue(visa.getObject().toString(), YappayEncryptedResponse.class));
		dto.setDetails(decrypted);
		dto.setCode("S00");
		return new ResponseEntity<ResponseDTO>(dto, HttpStatus.OK);
	}	
	
	
	public static void main(String...a) throws Exception{
		
		VisaOnBoadingMerchantReq req = new VisaOnBoadingMerchantReq();
		MerchantBankDetailReq mbr = new MerchantBankDetailReq();
		mbr.setAccName("VPAYQWIK");
		mbr.setBankAccNo("Vijaya BANK");
		mbr.setBankIfscCode("133100037672008");
		mbr.setBankLocation("VIJB0001331");
		mbr.setBankName("BANGALORE");
		
		SettlementBankDetailReq sbr = new SettlementBankDetailReq();
		sbr.setAccName("VPAYQWIK");
		sbr.setBankAccNo("Vijaya BANK");
		sbr.setBankIfscCode("133100037672008");
		sbr.setBankLocation("VIJB0001331");
		sbr.setBankName("BANGALORE");
		
		MerchantReq mer = new MerchantReq();
		mer.setMerchantName("VPAYQWIK");
		mer.setFirstName("Prashant");
		mer.setLastName("Pauranik");
		mer.setEmailAddress("prashant@msewa.com");
		mer.setAddress1("APITEST");
		mer.setAddress2("APITEST");
		mer.setCity("BANGALORE");
		mer.setState("KN");
		mer.setCountry("IN");
		mer.setPinCode("560060");
		mer.setMobileNo("+917795221595");
		mer.setAggregator("Business");
		mer.setIdProofPath("test");
		mer.setPanNo("CBIPP7167C");
		mer.setMerchantCategory("5812");
		mer.setVpa("vpayqwik02@vijb");
		mer.setAadharNo("5879456123654");
		mer.setAddressProofPath("test");
		mer.setGstNumber("test");
		req.setMerchat(mer);
		req.setmBankDetails(mbr);
		req.setsBankdetails(sbr);
		
//		VisaGetAccountNumber req = new VisaGetAccountNumber();
//		req.setFirstName("Ajay");
//		req.setLastName("Sharma");
//		req.setContactNo("9449147913");
//		req.setProductId("GENERAL");
//		req.setEntityId("CUST001");
//		req.setBusiness("VIJAYA");
//		ArrayList arr = new ArrayList();
////		arr.add("VISA");
////		arr.add("MASTER");
//		arr.add("RUPAY");
//		req.setNetwork(arr);
		
		ObjectMapper mapper = new ObjectMapper();
		System.err.println(req.getJsonRequest());
		String req1 = mapper.writeValueAsString(req.getJsonRequest());
		
//		System.err.println(req);
//		String req1 = mapper.writeValueAsString(req);
		
		SecurityTest sec = new SecurityTest();
		String enc = sec.encodeRequest(req1, "9876543211234562", "VIJAYA_M2P");
		System.out.println("red  "+enc);
		
		Client client = StartupUtil.createClient();
		client.addFilter(new LoggingFilter(System.out));
//		WebResource webResource = client.resource("http://vijaya.sit.yappay.in:9000/Yappay/registration-manager/v2/register");
		WebResource webResource = client.resource("https://ssltest.yappay.in/merchant/v2/signup");
	
		ClientResponse response = webResource.accept("application/json").header("TENANT", "VIJAYA_MERCHANT").type("application/json").post(ClientResponse.class, enc);
		
				
		String strResponse = response.getEntity(String.class);
		String decrypted = sec.decodeResponse(
				mapper.readValue(strResponse, YappayEncryptedResponse.class));
		System.out.println("de"+decrypted);
	}
	
	@RequestMapping(value = "/MvisaTest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> MvisaTest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) throws Exception {
		PQService service = pqServiceRepository.findServiceByCode("MVISA");
		String transationId = System.currentTimeMillis()+"";
//			transctionApi.initiateVisaPayment(10, "Test", service, transationId, "9449147913", "9449147913");
//			transctionApi.failedVisaPayment(transationId);
		
		
		String ss = "02004000400100C01002000000000000000000070110000810014CA Branch code000003168";
		 Socket clientSocket = new Socket("103.31.225.204", 11105);
		 System.out.println("11");
		 DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		 System.out.println("111");
		 BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		 System.out.println("11111");
		 outToServer.writeBytes(ss + '\n');
		 System.out.println("1111111");
		 System.out.println("OUT PUT  :: "+inFromServer.readLine());
		 System.out.println("1111111111111");
		return null;
	}
}
