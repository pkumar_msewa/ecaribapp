package com.payqwikapp.controller.api.v1;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ebs.util.EBSConstants;
import com.payqwik.visa.utils.VisaMerchantRequest;
import com.payqwikapp.api.*;
import com.payqwikapp.api.IMerchantApi;
import com.payqwikapp.entity.*;
import com.payqwikapp.model.*;
import com.payqwikapp.model.admin.ReportDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.admin.report.WalletLoadReportDTO;
import com.payqwikapp.model.admin.report.WalletLoadRepot;
import com.payqwikapp.model.error.KycLimitError;
import com.payqwikapp.repositories.AccessLogRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PartnerDetailRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.validation.AdminAuthValidation;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.mobile.UserResponseDTO;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ClientException;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.VNetUtils;
import com.payqwikapp.validation.PromoCodeValidation;
import com.payqwikapp.validation.RegisterValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class AdminController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
	private final IUserApi userApi;
	private final IMerchantApi merchantApi;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final ISessionApi sessionApi;
	private final ITransactionApi transactionApi;
	private final IMessageLogApi messageLogApi;
	private final IEmailLogApi emailLogApi;
	private final RegisterValidation registerValidation;
	private final IPromoCodeApi promoCodeApi;
	private final PromoCodeValidation promoCodeValidation;
	private final IAdminApi adminApi;
	private final ISendMoneyApi sendMoneyApi;
	private final PQServiceRepository pqServiceRepository;
	private final AdminAuthValidation adminAuthValidation;
	private final AccessLogRepository accessLogRepository;
	private final PartnerDetailRepository partnerDetailRepository;
	private final IPartnerApi partnerApi;
	private final UserDetailRepository userDetailRepository;

	public AdminController(IUserApi userApi, IMerchantApi merchantApi, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, ISessionApi sessionApi, ITransactionApi transactionApi,
			IMessageLogApi messageLogApi, IEmailLogApi emailLogApi, RegisterValidation registerValidation,
			IPromoCodeApi promoCodeApi, PromoCodeValidation promoCodeValidation, IAdminApi adminApi,
			ISendMoneyApi sendMoneyApi, PQServiceRepository pqServiceRepository,
			AdminAuthValidation adminAuthValidation, AccessLogRepository accessLogRepository,PartnerDetailRepository partnerDetailRepository,IPartnerApi partnerApi,
			UserDetailRepository userDetailRepository) {
		this.userApi = userApi;
		this.merchantApi = merchantApi;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.sessionApi = sessionApi;
		this.transactionApi = transactionApi;
		this.messageLogApi = messageLogApi;
		this.emailLogApi = emailLogApi;
		this.registerValidation = registerValidation;
		this.promoCodeApi = promoCodeApi;
		this.promoCodeValidation = promoCodeValidation;
		this.adminApi = adminApi;
		this.sendMoneyApi = sendMoneyApi;
		this.pqServiceRepository = pqServiceRepository;
		this.adminAuthValidation = adminAuthValidation;
		this.accessLogRepository = accessLogRepository;
		this.partnerDetailRepository=partnerDetailRepository;
	    this.partnerApi=partnerApi;	
	    this.userDetailRepository = userDetailRepository;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/payAmount", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO>payAmount(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Agent")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.AGENT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
					/*	RegisterError error = new RegisterError();
						error = registerValidation.validateEditUser(dto);
						if (error.isValid()) {*/
							try {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Get pay amount successfully.");
								result.setDetails(user.getAccount().getBalance());
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} catch (Exception e) {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Failed, Please try again later.");
								result.setDetails("Failed, Please try again later.");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						/*} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("Failed, Please try again later.");
							result.setDetails(error);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
*/
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	
	@RequestMapping(method = RequestMethod.POST, value = "/Update/Profile", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateProfile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RegisterDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						RegisterError error = new RegisterError();
						error = registerValidation.validateEditUser(dto);
						if (error.isValid()) {
							try {
								userApi.editUser(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Profile updated successfully.");
								result.setDetails("Profile updated successfully.");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} catch (Exception e) {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Failed, Please try again later.");
								result.setDetails("Failed, Please try again later.");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("Failed, Please try again later.");
							result.setDetails(error);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BlockUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> blockUserImmediately(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AuthUpdateRequest dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if (accessDetails != null) {
							if (accessDetails.isUpdateAuthAdmin()) {
								User blockUser = userApi.findByUserName(dto.getUsername());
								userApi.blockUser(blockUser.getUsername());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("User Locked");
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/listVisaMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> mVisaTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TransactionListDTO> mVisaTransactions = transactionApi.getmVisaTransaction();
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User Locked");
						result.setDetails(mVisaTransactions);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/listVisaMerchantFilter", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> mVisaTransactionFilter(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody TransactionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<TransactionListDTO> mVisaTransactions = transactionApi.getmVisaTransactionFilter(dateFormat.parse(from),dateFormat.parse(to));
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("User Locked");
						result.setDetails(mVisaTransactions);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UnblockUser", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> unblockUserImmediately(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AuthUpdateRequest dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User currentUser = userApi.findByUserName(dto.getUsername());
						if (currentUser != null) {
							if (currentUser.getUserType().equals(UserType.User)) {
								AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
								if (accessDetails != null) {
									if (accessDetails.isUpdateAuthUser()) {
										String authority = Authorities.USER + "," + Authorities.AUTHENTICATED;
										userApi.updateUserAuthority(authority, currentUser.getId());
										result.setStatus(ResponseStatus.SUCCESS);
										result.setMessage("User Unlocked");
									} else {
										result.setStatus(ResponseStatus.FAILURE);
										result.setMessage("Access Denied");
									}
								} else {
									result.setStatus(ResponseStatus.FAILURE);
									result.setMessage("Access Not Available");
								}
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("You can't unlock " + currentUser.getUserType() + " Type User");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("User not found");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Search/UpdateAuth", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> searchUpdateAuthorityRequest(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AuthUpdateSearch dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.getUpdateAuthLogById(dto.getRequestId());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Request/UpdateAuth", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateAuthority(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AuthUpdateRequest dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.processUpdateAuth(dto);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateAuth/All", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAllAuthUpdateLogs(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result = userApi.getAllAuthUpdateLogs();
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetValues", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateProfile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("values");
							result.setDetails(userApi.getAdminLoginValues());
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} catch (Exception e) {
							e.printStackTrace();
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed, Please try again later.");
							result.setDetails("Failed, Please try again later.");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ChangePassword", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> changePassword(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ChangePasswordDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						ChangePasswordError error = new ChangePasswordError();
						error = registerValidation.validateChangePassword(dto);
						if (error.isValid()) {
							try {
								dto.setPassword(dto.getNewPassword());
								userApi.changePassword(dto, userSession.getUser().getId());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Change Password");
								result.setDetails("Password Has Been Changed Successfully");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} catch (Exception e) {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Change Password");
								result.setDetails(e.getMessage());
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("Change Password");
							result.setDetails(error);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Change Password");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Change Password");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Change Password");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/RefundAmount", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> refundMoneyFromUser(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody RefundDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						User blockUser = userApi.findByUserName(dto.getUsername());
						userApi.blockUser(blockUser.getUsername());
						if (blockUser != null) {
							result = sendMoneyApi.refundMoneyToAccount(dto, blockUser);
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("User Not Found");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized User");
						result.setDetails("Unauthorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/LMTransactions", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getLoadMoneyTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<PQTransaction> transactionList = transactionApi.getLoadMoneyTransactions(Status.Success);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction List");
						result.setDetails(transactionList);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Un-Authorized User");
						result.setDetails("Un-Authorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/PCTransactions", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getRedeemTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MTransactionResponseDTO> listDTOs = userApi.getPromoCodeTransactions();
						/*PQService service = pqServiceRepository.findServiceByCode("PPS");
						List<PQTransaction> transactionList = transactionApi.transactionListByServiceAndDebit(service,false);
						List<User> userList = userApi.getAllUsers();
						List<MTransactionResponseDTO> minimizedList = ConvertUtil.getMerchantTransactions(transactionList, userList);
						Page<MTransactionResponseDTO> promoCodeTransactions = ConvertUtil.convertFromList(minimizedList,dto);*/
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction List");
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Un-Authorized User");
						result.setDetails("Un-Authorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/PCTransactionsFiltered", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getFilteredTransactions(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MTransactionResponseDTO> listDTOs = userApi.getPromoCodeTransactionsFiltered(dateFormat.parse(from),dateFormat.parse(to));
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction List");
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Un-Authorized User");
						result.setDetails("Un-Authorized User");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user");
			result.setDetails("Permission Not Granted");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Report/{reportType}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionReport(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@PathVariable(value = "reportType") String report, @RequestBody GetTransactionDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

		if (role.equalsIgnoreCase("Admin")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					if (report.equalsIgnoreCase("Transaction")) {
						long transactions = 0;
						String username = dto.getUsername();
						if (username != null && username.length() != 0) {
							User userTransaction = userApi.findByUserName(dto.getUsername());
							transactions = transactionApi.getDailyTransactionCountBetweenForAccount(dto.getStart(),
									dto.getEnd(), userTransaction.getAccountDetail());
							result.setMessage("User transaction report.");
						} else {
							transactions = transactionApi.getDailyTransactionCountBetweeen(dto.getStart(), dto.getEnd());
							result.setMessage("Transaction successful.");
						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(transactions);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else if (report.equalsIgnoreCase("Email")) {
						List<EmailLog> emailLogs = null;
						emailLogs = emailLogApi.getDailyEmailLogBetweeen(dto.getStart(), dto.getEnd());
						result.setMessage("Email Log successful.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(emailLogs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else if (report.equalsIgnoreCase("SMS")) {
						List<MessageLog> messageLogs = null;
						messageLogs = messageLogApi.getDailyMessageLogBetweeen(dto.getStart(), dto.getEnd());
						result.setMessage("Message log successful.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(messageLogs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized user.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Session invalid.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		}
		result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
		result.setMessage("Unauthorized user.");
		result.setDetails(null);
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/adminDashBoard", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAdminDashBoardValues(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			 @RequestBody GetTransactionDTO dto,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request,
			HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("Admin")) {
			String sessionId = dto.getSessionId();
			UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
			if (userSession != null) {
				UserDTO user = userApi.getUserById(userSession.getUser().getId());
				if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
						&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
					persistingSessionRegistry.refreshLastRequest(sessionId);
					long transactions = 0;
					Map<String, Object> details = new HashMap<>();
					
					Calendar cal1 = Calendar.getInstance();
					cal1.add(Calendar.DATE, -1);
					Date adate = new Date(cal1.getTimeInMillis());
					transactions = transactionApi.getDailyTransactionCountBetweeen(adate, adate);
					details.put("aRes", transactions);
					
					Calendar cal2 = Calendar.getInstance();
					cal2.add(Calendar.DATE, -2);
					adate = new Date(cal2.getTimeInMillis());
					transactions = transactionApi.getDailyTransactionCountBetweeen(adate, adate);
					details.put("bRes", transactions);
					
					Calendar cal3 = Calendar.getInstance();
					cal3.add(Calendar.DATE, -3);
					adate = new Date(cal3.getTimeInMillis());
					transactions = transactionApi.getDailyTransactionCountBetweeen(adate, adate);
					details.put("cRes", transactions);
					
					Calendar cal4 = Calendar.getInstance();
					cal4.add(Calendar.DATE, -4);
					adate = new Date(cal3.getTimeInMillis());
					transactions = transactionApi.getDailyTransactionCountBetweeen(adate, adate);
					details.put("dRes", transactions);
					
					Calendar cal5 = Calendar.getInstance();
					cal5.add(Calendar.DATE, -5);
					adate = new Date(cal5.getTimeInMillis());
					transactions = transactionApi.getDailyTransactionCountBetweeen(adate, adate);
					details.put("eRes", transactions);
					
					result.setMessage("Transaction successful.");
					result.setStatus(ResponseStatus.SUCCESS);
					result.setDetails(details);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized user.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.INVALID_SESSION);
				result.setMessage("Session invalid.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		}
		result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
		result.setMessage("Unauthorized user.");
		result.setDetails(null);
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	}
	

	@RequestMapping(method = RequestMethod.POST, value = "/Merchant/Save", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MRegisterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String email = dto.getEmail();
						User merchant = userApi.findByUserName(email);
						if (merchant == null) {
							PGDetails pgDetails = userApi.findMerchantByMobile(dto.getContactNo());
							if (pgDetails == null) {
								result = merchantApi.addMerchant(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Merchant added successfully");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Mobile already exists");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Merchant Already exists with this EMAIL ID");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	//donatee
	
	@RequestMapping(method = RequestMethod.POST, value = "/Donatee/Save", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveDonatee(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DRegisterDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ClientException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String mobile = dto.getMobileNumber();
						User donatee = userApi.findByUserName("D"+mobile);
						if (donatee == null) {
								result = userApi.saveDonatee(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Donatee added successfully");
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						 else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Donatee Already exists with this EMAIL ID");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/Merchant/All", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String languagemys,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String type = "ALL";
						List<MerchantDTO> merchants = merchantApi.getAll(type,dto.getToDate(),dto.getFromDate());
						result.setMessage("Merchant List");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(merchants);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PromoCode/Save", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> savePromoCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PromoCodeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						PromoCode isCodeExist = promoCodeApi.checkPromoCodeValid(dto.getPromoCode(),dto.getPromoCodeId());
						if (isCodeExist == null ) {
							boolean isValidCode = promoCodeApi.checkPromoCodeLength(dto.getPromoCode());
							if (isValidCode) {
								promoCodeApi.addPromocode(dto);
								result.setMessage("PromoCode added successfully.");
								result.setStatus(ResponseStatus.SUCCESS);
								result.setDetails(null);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
							result.setMessage("Please Enter 6 digit Promo Code");
							result.setStatus(ResponseStatus.FAILURE);
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
						result.setMessage("Promo Code Already Exist");
						result.setStatus(ResponseStatus.FAILURE);
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails("Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails("Session invalid.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PromoCode/List", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listPromoCodes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		List<PromoCodeDTO> list = new ArrayList<PromoCodeDTO>();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						list = promoCodeApi.getAll();
						result.setMessage("PromoCode list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(list);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}
	@RequestMapping(method = RequestMethod.POST, value = "/PromoCode/Edit", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> editPromoCode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PromoCodeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						promoCodeApi.updatePromotionCode(dto);
						result.setMessage("Promo Code Update Successfully");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setCode(ResponseStatus.SUCCESS.getValue());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setCode(ResponseStatus.INVALID_SESSION.getValue());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setCode(ResponseStatus.BAD_REQUEST.getValue());
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BankTransferList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankTransferRequests(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<BankTransfer> bankTransfers = adminApi.getAllTransferReports();
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("bank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (BankTransfer bt : bankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
					//		System.out.println("bt.getSender().getUserDetail().getId()="+bt.getSender().getUserDetail().getId());
					//		UserDetail us = userDetailRepository.findUserById(bt.getSender().getUserDetail().getId());
							if(bt.getSender().getUserDetail()!=null){
								System.out.println("bt.getSender().getUserDetail().getFirstName()="+bt.getSender().getUserDetail().getFirstName());
								System.out.println("email"+bt.getSender().getUserDetail().getEmail());
								bdto.setName(bt.getSender().getUserDetail().getFirstName()!=null?bt.getSender().getUserDetail().getFirstName():"NA");
								bdto.setEmail(bt.getSender().getUserDetail().getEmail()!=null?bt.getSender().getUserDetail().getEmail():"NA");
							}
							else{
								System.out.println("In NA");
								bdto.setName("NA");
								bdto.setEmail("NA");
							}
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount("" + bt.getAmount());
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
							if (temp != null) {
								bdto.setStatus(temp.getStatus().getValue());
								bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
							}
							bankTransferDTOs.add(bdto);

						}
						result.setMessage("BankTransfer list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateBankTransferStatus", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankTransferRequests(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CallBackDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String transactionRefNo = dto.getTransactionRefNo();
						String message = "";
						if (dto.isSuccess()) {
							sendMoneyApi.sendMoneyBankSuccess(transactionRefNo);
							message = "Bank Transfer Successful";
						} else {
							sendMoneyApi.sendMoneyBankFailed(transactionRefNo);
							message = "Bank Transfer Failed";
						}
						result.setMessage(message);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails("");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/MBankTransferList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> merchantBankTransferRequests(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<MBankTransfer> mBankTransfers = adminApi.getAllMBankTransferReports();
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("mbank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (MBankTransfer bt : mBankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
							bdto.setName(bt.getSender().getUserDetail().getFirstName());
							bdto.setEmail(bt.getSender().getUserDetail().getEmail());
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount("" + bt.getAmount());
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
							if (temp != null) {
								bdto.setStatus(temp.getStatus().getValue());
								bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
							}
							bankTransferDTOs.add(bdto);
						}
						result.setMessage("Merchant NEFT Transaction list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/ABankTransferList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> agentBankTransferRequests(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<ABankTransfer> aBankTransfers = adminApi.getAllABankTransferReports();
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("mbank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (ABankTransfer bt : aBankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setSenderName(bt.getSenderInfo().getSenderName());
							bdto.setSenderEmailId(bt.getSenderInfo().getSenderEmailId());
							bdto.setSenderMobileNo(bt.getSenderInfo().getSenderMobileNo());
							bdto.setIdProofNo(bt.getSenderInfo().getIdProofNo());
							bdto.setImage(bt.getImage());
							bdto.setImageContent(bt.getImageContent());
							bdto.setAddImageType(bt.getAddImageType());
							bdto.setAddressImage(bt.getAddressImage());
							bdto.setReceiverEmailId(bt.getReceiverEmailId());
							bdto.setReceiverMobileNo(bt.getReceiverMobileNo());
							bdto.setDescription(bt.getSenderInfo().getDescription());
							
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
							bdto.setName(bt.getSender().getUserDetail().getFirstName());
							bdto.setEmail(bt.getSender().getUserDetail().getEmail());
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount("" + bt.getAmount());
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
							if (temp != null) {
								bdto.setStatus(temp.getStatus().getValue());
								bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
							}
							bankTransferDTOs.add(bdto);
						}
						result.setMessage("Agent NEFT Transaction list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "/CheckExistingVisaMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> checkExistingVisaMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		System.err.println("Inside Visa Merchant Save");
		// boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		// if (isValidHash) {
		if (role.equalsIgnoreCase("Merchant")) {
			String email = dto.getEmailAddress();
			User merchant = userApi.findByUserName(email);
			if (merchant != null) {
				System.err.println("merchant Authority " + merchant.getAuthority());
				if (merchant.getUserType().equals(UserType.Merchant)
						&& merchant.getMobileStatus().equals(Status.Inactive)) {
					result = userApi.resendOTPUnregistered(merchant);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Merchant exists");
					result.setDetails("Merchant exists");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Merchant doesn't exists with this EMAIL ID");
				result.setDetails("Merchant doesn't exists with this EMAIL ID");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Merchant doesn't exists with this EMAIL ID");
			result.setDetails("Merchant doesn't exists with this EMAIL ID");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/VisaMerchant", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveVisaMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						String email = dto.getEmailAddress();
						User merchant = userApi.findByUserName(email);
						// if(merchant == null) {
						result = merchantApi.addVisaMerchant(dto, merchant);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						/*
						 * }else { result.setStatus(ResponseStatus.FAILURE);
						 * result. setMessage(
						 * "Merchant Already exists with this EMAIL ID" );
						 * return new ResponseEntity<ResponseDTO>(result,
						 * HttpStatus.OK); }
						 */
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ListAccountTypes", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listAccountTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Account Type List");
						result.setDetails(userApi.getAllAccountTypes());
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateAccountType", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> listAccountTypes(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody KycLimitDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						KycLimitError error = adminAuthValidation.validateAccountLimits(dto);
						if (error.isValid()) {
							result = userApi.updateAccountType(dto);
						} else {
							result.setStatus(ResponseStatus.BAD_REQUEST);
							result.setMessage("invalid parameters");
							result.setDetails(error);
						}

						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

/*	@RequestMapping(value = "/findByDate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> findTransactionsFiltered(@RequestBody DateDTO dto,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {

		List<TransactionReport> trans = null;
		ResponseDTO dto1 = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		System.err.println(from);
		System.err.println(to);
		trans = adminApi.getTransactionsByDate(dateFormat.parse(from), dateFormat.parse(to));
		if (trans != null) {
			System.err.println(trans);
			dto1.setCode("S00");
			dto1.setMessage("SUCCESS||RESPONSE");
			dto1.setDetails(trans);
			return new ResponseEntity<ResponseDTO>(dto1, HttpStatus.OK);
		}
		dto1.setCode("FOO");
		dto1.setMessage("FAILED||RESPONSE");
		return new ResponseEntity<ResponseDTO>(dto1, HttpStatus.OK);
	}*/
	
	@RequestMapping(method = RequestMethod.POST, value = "/findByDate", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTransactionBetween(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date endDate = dateFormat.parse(dto.getEndDate());
						Date startDate = dateFormat.parse(dto.getStartDate());
						List<TListDTO> listDTOs = userApi.getDailyTransactions(startDate, endDate,dto.getTransactionType());
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	
	
	//for nikki
	
	@RequestMapping(value = "/findByDate1", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> findNikkiTransactionsFiltered(@RequestBody DateDTO dto,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {

		List<TransactionReport> trans = null;
		ResponseDTO dto1 = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		System.err.println(from);
		System.err.println(to);

		/*
		 * long milliSecondsFrom= Long.parseLong(dto.getFromDate()); long
		 * milliSecondsTo=Long.parseLong(dto.getToDate());
		 */

		/*
		 * Calendar calendar = Calendar.getInstance();
		 * calendar.setTimeInMillis(milliSecondsFrom); String
		 * from=dateFormater.format(calendar.getTime());
		 * System.out.println("DATE:"+dateFormater.parse(from));
		 * System.out.println(dateFormater.format(calendar.getTimeInMillis()));
		 * Calendar calendar2 = Calendar.getInstance();
		 * calendar2.setTimeInMillis(milliSecondsTo); String
		 * to=dateFormater.format(calendar2.getTime());
		 * System.out.println("DATE:"+dateFormater.parse(to));
		 * System.out.println(dateFormater.format(calendar2.getTimeInMillis()));
		 */
		// System.err.println("");

		System.err.println("DATE::" + from);
		System.err.println("DATE::" + to);

		trans = adminApi.getTransactionsByDate(dateFormat.parse(from), dateFormat.parse(to));

		if (trans != null) {
			System.err.println(trans);
			dto1.setCode("S00");
			dto1.setMessage("SUCCESS||RESPONSE");

			dto1.setDetails(trans);

			return new ResponseEntity<ResponseDTO>(dto1, HttpStatus.OK);
		}
		dto1.setCode("FOO");
		dto1.setMessage("FAILED||RESPONSE");
		return new ResponseEntity<ResponseDTO>(dto1, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findUserByDate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> findUserFiltered(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {
		List<UserResponseDTO> trans = null;
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						trans = adminApi.getUsersByDate(dateFormat.parse(from), dateFormat.parse(to));
						if (trans != null) {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("SUCCESS||RESPONSE");
							result.setDetails(trans);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("FAILED||RESPONSE");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BankTransferFilteredList", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBankTransferRequestsFiltered(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<BankTransfer> bankTransfers = adminApi.getAllTransferReportsByDate(dateFormat.parse(from),dateFormat.parse(to));
						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
						User bank = userApi.findByUserName("bank@vpayqwik.com");
						String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
						for (BankTransfer bt : bankTransfers) {
							BankTransferDTO bdto = new BankTransferDTO();
							bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
							if(bt.getSender().getUserDetail()!=null){
								bdto.setName(bt.getSender().getUserDetail().getFirstName()!=null?bt.getSender().getUserDetail().getFirstName():"NA");
								bdto.setEmail(bt.getSender().getUserDetail().getEmail()!=null?bt.getSender().getUserDetail().getEmail():"NA");
							}
							else{
								bdto.setName("NA");
								bdto.setEmail("NA");
							}
							bdto.setMobileNumber(bt.getSender().getUsername());
							bdto.setAmount("" + bt.getAmount());
							BankDetails bankDetails = bt.getBankDetails();
							if (bankDetails != null) {
								bdto.setBankName(bankDetails.getBank().getName());
								bdto.setIfscCode(bankDetails.getIfscCode());
							}
							bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
							bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
							bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
							bdto.setTransactionID(bt.getTransactionRefNo());
							bdto.setBankVirtualAccount(accountNumber);
							bdto.setStatus(String.valueOf(Status.Success));
							PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
							if (temp != null) {
								bdto.setStatus(temp.getStatus().getValue());
								bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
							}
							bankTransferDTOs.add(bdto);
						}
						result.setMessage("PromoCode list.");
						result.setStatus(ResponseStatus.SUCCESS);
						result.setDetails(bankTransferDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			result.setDetails(null);
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	// API to get GCM id of active users
	@RequestMapping(method = RequestMethod.POST, value = "/GetGCMIDs", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getGCMIDs(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if (accessDetails != null) {
							if (accessDetails.isSendGCM()) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								Pageable gcmPage = new PageRequest(dto.getPage(), dto.getSize());
								Page<String> gcmList = userApi.getGCMIDs(gcmPage);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("GCM IDs");
								result.setDetails(gcmList);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	
	
	// API to get All Agent List For Credit
	@RequestMapping(value = "/GetAllAgentCreditList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserReceipts(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {

		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if(true)// (accessDetails != null) 
							{
							if (true)//(accessDetails.isSendGCM())
							{

								Sort sort = new Sort(Sort.Direction.DESC, "id");
								Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
								List<PQTransaction> pg = transactionApi.getTotalinitateAgentTransactions(pageable,"SALMTA");
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Agent Credit List");
								result.setDetails(pg);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	

	@RequestMapping(value = "/SucessAgentMoney", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> SucessAgentMoney(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {

		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if(true)// (accessDetails != null) 
							{
							if (true)//(accessDetails.isSendGCM())
							{

//								transactionApi.successRequestAgentSendMoney(dto.getTransactionrefno());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Transction Successful");
								
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetListLockedAccounts", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<List<String>> getListLockedAccounts(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		List<String> lockedAccountList=new ArrayList<String>();
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						List<User> lockedAccounts=adminApi.getAllLockedAccountsLists();
						for (User lockedUsers : lockedAccounts) {
							lockedAccountList.add(lockedUsers.getUsername());
						}
						
						
					}
				}
			}
		}
		return new ResponseEntity<>(lockedAccountList,HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/GetAnalytics", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAnalytics(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		ResponseDTO responseAnalytics=new ResponseDTO();
		List<AnalyticsDTO> analyticsDTOs=new ArrayList<>();
		Date to=null;
		Date from=null;
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(
								sessionId);
						try {
							 from=dateFormater.parse(dto.getFromDate());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							to=dateFormater.parse(dto.getToDate());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						List<Object[]> analytics=transactionApi.getTotalTransactionsByServices(from,to);
						System.err.println(analyticsDTOs);
						for (Object[] objects : analytics) {
							AnalyticsDTO dtos=new AnalyticsDTO();
							 long count=(long) objects[0];
							 double amt=(double)objects[1];
							 PQService ser=(PQService)objects[2];
							 dtos.setAmount(amt);
							 dtos.setCount(count);
							 dtos.setServiceName(ser.getName());
							 analyticsDTOs.add(dtos);
							 
						}
						responseAnalytics.setStatus("S00");
						responseAnalytics.setMessage("Analytics data fetched successfully");
						responseAnalytics.setDetails(analyticsDTOs);
					}else{
						responseAnalytics.setCode("F00");
						responseAnalytics.setMessage("UnAuthorised User");
					}
				}else{
					responseAnalytics.setCode("F00");
					responseAnalytics.setMessage("please login again");
					
				}
			}else{
				responseAnalytics.setCode("F00");
				responseAnalytics.setMessage("User Should have admin previlages");
			}
		}
		
		return new ResponseEntity<>(responseAnalytics,HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetAnalyticsCredit", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getAnalyticsCredit(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		ResponseDTO responseAnalytics=new ResponseDTO();
		List<AnalyticsDTO> analyticsDTOs=new ArrayList<>();
		Date to=null;
		Date from=null;
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(
								sessionId);
						try {
							 from=dateFormater.parse(dto.getFromDate());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							to=dateFormater.parse(dto.getToDate());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.err.println("to date::"+to);
						List<Object[]> analytics=transactionApi.getTotalTransactionsByServicesCredit(from,to);
						System.err.println(analyticsDTOs);
						for (Object[] objects : analytics) {
							
							AnalyticsDTO dtos=new AnalyticsDTO();
							 long count=(long) objects[0];
							 double amt=(double)objects[1];
							 PQService ser=(PQService)objects[2];
							 dtos.setAmount(amt);
							 dtos.setCount(count);
							 dtos.setServiceName(ser.getCode());
							 analyticsDTOs.add(dtos);
							 
						}
						responseAnalytics.setStatus("S00");
						responseAnalytics.setMessage("Analytics data fetched successfully");
						responseAnalytics.setDetails(analyticsDTOs);
					}else{
						responseAnalytics.setCode("F00");
						responseAnalytics.setMessage("UnAuthorised User");
					}
				}else{
					responseAnalytics.setCode("F00");
					responseAnalytics.setMessage("please login again");
					
				}
			}else{
				responseAnalytics.setCode("F00");
				responseAnalytics.setMessage("User Should have admin previlages");
			}
		}
		
		return new ResponseEntity<>(responseAnalytics,HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.POST, value = "/GetUserAnalytics", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserAnalytics(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AnalyticsDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		ResponseDTO responseAnalytics=new ResponseDTO();
		List<UserAnalytics> list=null;
		List<AnalyticsDTO> analyticsDTOs=new ArrayList<>();
		System.err.println("serviceName:::"+dto.getServiceName());
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						System.err.println("i am hewrrrrrrrree:::");
						System.err.println(dto);
						 list=adminApi.getUserListForAnalytics(dto);
						
						System.err.println(responseAnalytics);
						responseAnalytics.setDetails(list);
						responseAnalytics.setCode("S00");
						return new ResponseEntity<>(responseAnalytics,HttpStatus.OK);
						
	}
}
}
		}
		System.err.println(responseAnalytics);
		responseAnalytics.setCode("F00");
		return new ResponseEntity<>(responseAnalytics,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPartnerInstallReports", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getReports(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PartnerDetailDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String partnerName = dto.getPartnerName();
							List<PartnerDetailDTO> reports = null;
							List<PartnerInstall> installs = new ArrayList<>();
							String partnerType = "";
							switch (partnerName.toUpperCase()) {
							case "LAVA":
								partnerType = "LAVA";
								break;
							case "GIONEE":
								partnerType = "GIONEE";
								break;
							default:
								partnerType = "LAVA";
							}
							PartnerDetail detail = partnerDetailRepository.findByPartnerName(partnerName);
							installs = partnerApi.listInstallsByPartnerName(detail);
							reports = ConvertUtil.getFromPartnerInstall(installs, detail);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(partnerName + " List");
							result.setDetails(reports);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPartnerInstallReportsFilter", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getReportsFilter(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PartnerDetailDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		ResponseDTO result = new ResponseDTO();
		String from = dto.getStartDate() + " 00:00";
		String to = dto.getEndDate() + " 23:59";
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							String partnerName = dto.getPartnerName();
							List<PartnerDetailDTO> reports = null;
							List<PartnerInstall> installs = new ArrayList<>();
							String partnerType = "";
							switch (partnerName.toUpperCase()) {
							case "LAVA":
								partnerType = "LAVA";
								break;
							case "GIONEE":
								partnerType = "GIONEE";
								break;
							default:
								partnerType = "LAVA";
							}
							PartnerDetail detail = partnerDetailRepository.findByPartnerName(partnerName);
							installs = partnerApi.listInstallsByPartnerNameFilter(detail,dateFormat.parse(from), dateFormat.parse(to));
							reports = ConvertUtil.getFromPartnerInstall(installs, detail);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage(partnerName + " List");
							result.setDetails(reports);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Ajax | User List");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Ajax | User List");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Ajax | User List");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{locationCode}/UserByLocationCode", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> findUserByLocationCode(@RequestBody DateDTO dto, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			HttpServletRequest request, HttpServletResponse response,@PathVariable("locationCode") String locationCode,
			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {
		List<UserByLocationDTO> trans = null;
		String from = dto.getFromDate() + " 00:00";
		String to = dto.getToDate() + " 23:59";
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						trans = userApi.getAllUserByLocationCode(dateFormat.parse(from), dateFormat.parse(to),locationCode);
						if (trans != null) {
							Map<String, Object> detail = new HashMap<String, Object>();
							detail.put("UserByLocationCode", trans);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Get List of Users By PinCode.");
							result.setDetails(detail);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("FAILED||RESPONSE");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized user.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session invalid.");
					result.setDetails(null);
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			}
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized user.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Unable to process request");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	
	
	
	
	
	
//	for single user notification
	
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetGCMIdByUserName", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getGCMIDByUserName(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						AccessDetails accessDetails = accessLogRepository.getByUser(userSession.getUser());
						if (accessDetails != null) {
							if (accessDetails.isSendGCM()) {
								persistingSessionRegistry.refreshLastRequest(sessionId);
								String gcmList = userApi.getGCMIdByUserName(dto.getUsername());
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("GCM IDs");
								result.setDetails(gcmList);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Access Denied");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Access Not Available");
						}
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}

	
	
	
	@RequestMapping(value = "/CountByMonthWise", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> countByMonthWise(@RequestBody DateDTO dto,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						result = adminApi.getTransactionsCountByMonthWise();
		                       if (result != null) {
		                    	   result.setStatus(ResponseStatus.SUCCESS);
		                    	   result.setMessage("get all the transaction count of user");
			                  return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		                     }
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/CountUserByMonthWise", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> countUserByMonthWise(@RequestBody DateDTO dto,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {

		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						result = adminApi.getUserCountByMonthWise();
		                       if (result != null) {
		                    	   result.setStatus(ResponseStatus.SUCCESS);
		                    	   result.setMessage("get all the count of user");
			                  return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		                     }
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/BulkUpload", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getBulkUpload(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BulkUploadDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						        adminApi.saveBulkFile(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("file uploaded successfully");
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed,Un-Authorized User");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getReconcilingTxn", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getReconcilingTransaction(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ReportDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						Date endDate = dateFormat.parse(dto.getEndDate());
						Date startDate = dateFormat.parse(dto.getStartDate());
						List<TListDTO> listDTOs = null;
						if(dto.getType().equals("IPay")){
							switch (dto.getStatus()) {
							case "Initiated":
								listDTOs = userApi.getAllIpayTxnsByStatus(startDate, endDate, Status.Initiated);
								break;
							case "Failed":
								listDTOs = userApi.getAllIpayTxnsByStatus(startDate, endDate, Status.Failed);
								break;
							case "Success":
								listDTOs = userApi.getAllIpayTxnsByStatus(startDate, endDate, Status.Success);
								break;
							default:
								listDTOs = userApi.getAllIpayTxns(startDate, endDate);
								break;
							}
						}else if(dto.getType().equals("EBS")){
							switch (dto.getStatus()) {
							case "Initiated":
								listDTOs = userApi.getLoadMoneyTxnsByStatus(startDate, endDate, Status.Initiated, EBSConstants.EBS_SERVICE_CODE);
								break;
							case "Failed":
								listDTOs = userApi.getLoadMoneyTxnsByStatus(startDate, endDate, Status.Failed, EBSConstants.EBS_SERVICE_CODE);
								break;
							case "Success":
								listDTOs = userApi.getLoadMoneyTxnsByStatus(startDate, endDate, Status.Success, EBSConstants.EBS_SERVICE_CODE);
								break;
							default:
								listDTOs = userApi.getAllLoadMoneyTxns(startDate, endDate, EBSConstants.EBS_SERVICE_CODE);
								break;
							}
						}else if(dto.getType().equals("VET")){
							switch (dto.getStatus()) {
							case "Initiated":
								listDTOs = userApi.getLoadMoneyTxnsByStatus(startDate, endDate, Status.Initiated, VNetUtils.VNET_SERVICE_CODE);
								break;
							case "Failed":
								listDTOs = userApi.getLoadMoneyTxnsByStatus(startDate, endDate, Status.Failed, VNetUtils.VNET_SERVICE_CODE);
								break;
							case "Success":
								listDTOs = userApi.getLoadMoneyTxnsByStatus(startDate, endDate, Status.Success, VNetUtils.VNET_SERVICE_CODE);
								break;
							default:
								listDTOs = userApi.getAllLoadMoneyTxns(startDate, endDate, VNetUtils.VNET_SERVICE_CODE);
								break;
							}
						}
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(String.valueOf(listDTOs.size()));
						result.setDetails(listDTOs);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Permission Not Granted");
						result.setDetails("Permission Not Granted");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Permission Not Granted");
				result.setDetails("Permission Not Granted");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	     
	     
	     @RequestMapping(value = "/CountByMonthWiseForSingleUser", method = RequestMethod.POST, produces = {
	 			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	 	ResponseEntity<ResponseDTO> countByMonthWiseSingleUser(@RequestBody DateDTO dto,
	 			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
	 			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
	 			@RequestHeader(value = "hash", required = false) String hash) throws ParseException {
	 		ResponseDTO result = new ResponseDTO();
	 		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
	 		if (isValidHash) {
	 			if (role.equalsIgnoreCase("Admin")) {
	 				String sessionId = dto.getSessionId();
	 				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	 				if (userSession != null) {
	 					UserDTO user = userApi.getUserById(userSession.getUser().getId());
	 					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
	 							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	 						result = adminApi.getTransactionsCountByMonthWiseSingleUser(dto.getUsername());
	 		                       if (result != null) {
	 		                    	   result.setStatus(ResponseStatus.SUCCESS);
	 		                    	   result.setMessage("get all the transaction count of user");
	 			                  return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		                     }
	 					} else {
	 						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 						result.setMessage("Permission Not Granted");
	 						result.setDetails("Permission Not Granted");
	 						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 					}
	 				} else {
	 					result.setStatus(ResponseStatus.INVALID_SESSION);
	 					result.setMessage("Invalid Session");
	 					result.setDetails("Invalid Session");
	 					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 				}
	 			} else {
	 				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 				result.setMessage("Failed,Un-Authorized User");
	 				result.setDetails("Permission Not Granted");
	 				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 			}
	 		} else {
	 			result.setStatus(ResponseStatus.BAD_REQUEST);
	 			result.setMessage("Invalid Secure Hash");
	 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		}
	 		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 	}
	     
	     @RequestMapping(method = RequestMethod.POST, value = "/MerchantBankTransferFilteredList", produces = {
	 			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	 	ResponseEntity<ResponseDTO> getMerchnatBankTransferRequestsFiltered(@PathVariable(value = "role") String role,
	 			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
	 			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
	 			HttpServletRequest request, HttpServletResponse response)
	 			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
	 		ResponseDTO result = new ResponseDTO();
	 		String from = dto.getFromDate() + " 00:00";
	 		String to = dto.getToDate() + " 23:59";
	 		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
	 		if (isValidHash) {
	 			if (role.equalsIgnoreCase("Admin")) {
	 				String sessionId = dto.getSessionId();
	 				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	 				if (userSession != null) {
	 					UserDTO user = userApi.getUserById(userSession.getUser().getId());
	 					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
	 							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	 						persistingSessionRegistry.refreshLastRequest(sessionId);
	 						List<MBankTransfer> mBankTransfers = adminApi.getAllMerchantTransferReportsByDate(dateFormat.parse(from),dateFormat.parse(to));
	 						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
							User bank = userApi.findByUserName("mbank@vpayqwik.com");
							String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
							for (MBankTransfer bt : mBankTransfers) {
								BankTransferDTO bdto = new BankTransferDTO();
								bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
								bdto.setName(bt.getSender().getUserDetail().getFirstName());
								bdto.setEmail(bt.getSender().getUserDetail().getEmail());
								bdto.setMobileNumber(bt.getSender().getUsername());
								bdto.setAmount("" + bt.getAmount());
								BankDetails bankDetails = bt.getBankDetails();
								if (bankDetails != null) {
									bdto.setBankName(bankDetails.getBank().getName());
									bdto.setIfscCode(bankDetails.getIfscCode());
								}
								bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
								bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
								bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
								bdto.setTransactionID(bt.getTransactionRefNo());
								bdto.setBankVirtualAccount(accountNumber);
								bdto.setStatus(String.valueOf(Status.Success));
								PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
								if (temp != null) {
									bdto.setStatus(temp.getStatus().getValue());
									bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
								}
								bankTransferDTOs.add(bdto);
							}
							result.setMessage("Merchant NEFT Transaction list.");
							result.setStatus(ResponseStatus.SUCCESS);
							result.setDetails(bankTransferDTOs);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 					} else {
	 						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 						result.setMessage("Unauthorized user.");
	 						result.setDetails(null);
	 						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 					}
	 				} else {
	 					result.setStatus(ResponseStatus.INVALID_SESSION);
	 					result.setMessage("Session invalid.");
	 					result.setDetails(null);
	 					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 				}
	 			}
	 			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 			result.setMessage("Unauthorized user.");
	 			result.setDetails(null);
	 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		} else {
	 			result.setStatus(ResponseStatus.BAD_REQUEST);
	 			result.setMessage("Unable to process request");
	 			result.setDetails(null);
	 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		}

	 	}
	     
	     @RequestMapping(method = RequestMethod.POST, value = "/AgentBankTransferFilteredList", produces = {
		 			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		 	ResponseEntity<ResponseDTO> getAgentBankTransferRequestsFiltered(@PathVariable(value = "role") String role,
		 			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
		 			@RequestBody DateDTO dto, @RequestHeader(value = "hash", required = true) String hash,
		 			HttpServletRequest request, HttpServletResponse response)
		 			throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
		 		ResponseDTO result = new ResponseDTO();
		 		String from = dto.getFromDate() + " 00:00";
		 		String to = dto.getToDate() + " 23:59";
		 		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		 		if (isValidHash) {
		 			if (role.equalsIgnoreCase("Admin")) {
		 				String sessionId = dto.getSessionId();
		 				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		 				if (userSession != null) {
		 					UserDTO user = userApi.getUserById(userSession.getUser().getId());
		 					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
		 							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
		 						persistingSessionRegistry.refreshLastRequest(sessionId);
		 						List<ABankTransfer> aBankTransfers = adminApi.getAllAgentTransferReportsByDate(dateFormat.parse(from),dateFormat.parse(to));
		 						List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
								User bank = userApi.findByUserName("abank@vpayqwik.com");
								String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
								for (ABankTransfer bt : aBankTransfers) {
									BankTransferDTO bdto = new BankTransferDTO();
									
									bdto.setSenderName(bt.getSenderInfo().getSenderName());
									bdto.setSenderEmailId(bt.getSenderInfo().getSenderEmailId());
									bdto.setSenderMobileNo(bt.getSenderInfo().getSenderMobileNo());
									bdto.setIdProofNo(bt.getSenderInfo().getIdProofNo());
									bdto.setImage(bt.getImage());
									bdto.setImageContent(bt.getImageContent());
									bdto.setReceiverEmailId(bt.getReceiverEmailId());
									bdto.setReceiverMobileNo(bt.getReceiverMobileNo());
									bdto.setDescription(bt.getSenderInfo().getDescription());
									
									bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
									bdto.setName(bt.getSender().getUserDetail().getFirstName());
									bdto.setEmail(bt.getSender().getUserDetail().getEmail());
									bdto.setMobileNumber(bt.getSender().getUsername());
									bdto.setAmount("" + bt.getAmount());
									BankDetails bankDetails = bt.getBankDetails();
									if (bankDetails != null) {
										bdto.setBankName(bankDetails.getBank().getName());
										bdto.setIfscCode(bankDetails.getIfscCode());
									}
									bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
									bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
									bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
									bdto.setTransactionID(bt.getTransactionRefNo());
									bdto.setBankVirtualAccount(accountNumber);
									bdto.setStatus(String.valueOf(Status.Success));
									PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
									if (temp != null) {
										bdto.setStatus(temp.getStatus().getValue());
										bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
									}
									bankTransferDTOs.add(bdto);
								}
								result.setMessage("Agent NEFT Transaction list.");
								result.setStatus(ResponseStatus.SUCCESS);
								result.setDetails(bankTransferDTOs);
								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		 					} else {
		 						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
		 						result.setMessage("Unauthorized user.");
		 						result.setDetails(null);
		 						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		 					}
		 				} else {
		 					result.setStatus(ResponseStatus.INVALID_SESSION);
		 					result.setMessage("Session invalid.");
		 					result.setDetails(null);
		 					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		 				}
		 			}
		 			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
		 			result.setMessage("Unauthorized user.");
		 			result.setDetails(null);
		 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		 		} else {
		 			result.setStatus(ResponseStatus.BAD_REQUEST);
		 			result.setMessage("Unable to process request");
		 			result.setDetails(null);
		 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		 		}

		 	}
		     
	    		 @RequestMapping(method = RequestMethod.POST, value = "/FetchsAllCardDetails", produces = {
	    					MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	    			ResponseEntity<ResponseDTO> woohooCardList(@PathVariable(value = "role") String role,
	    					@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
	    					@RequestBody TransactionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
	    					HttpServletResponse response) throws Exception {
	    				ResponseDTO result = new ResponseDTO();
	    				boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
	    				if (isValidHash) {
	    					if (role.equalsIgnoreCase("Admin")) {
	    						String sessionId = dto.getSessionId();
	    						UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	    						if (userSession != null) {
	    							UserDTO user = userApi.getUserById(userSession.getUser().getId());
	    							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
	    									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	    								persistingSessionRegistry.refreshLastRequest(sessionId);
	    								List<WoohooCardDetailsDTO> cards= transactionApi.getallCards();
	    								result.setStatus(ResponseStatus.SUCCESS);
	    								result.setMessage("WooHoo Card List");
	    								result.setDetails(cards);
	    								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	    							} else {
	    								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	    								result.setMessage("Failed, Unauthorized user.");
	    								result.setDetails("Failed, Unauthorized user.");
	    								return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	    							}
	    						} else {
	    							result.setStatus(ResponseStatus.INVALID_SESSION);
	    							result.setMessage("Please, login and try again.");
	    							result.setDetails("Please, login and try again.");
	    							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	    						}
	    					} else {
	    						result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
	    						result.setMessage("Failed, Unauthorized user.");
	    						result.setDetails("Failed, Unauthorized user.");
	    						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	    					}
	    				} else {
	    					result.setStatus(ResponseStatus.INVALID_HASH);
	    					result.setMessage("Failed, Please try again later.");
	    					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	    				}
	    			}
	
	     
	     /*Admin Panel report*/
	 	
	 	@RequestMapping(method = RequestMethod.POST, value = "/GetTransactionReport", produces = {
	 			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	 	ResponseEntity<ResponseDTO> getDailyTrasactionReport(@PathVariable(value = "role") String role,
	 			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
	 			@RequestBody PagingDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
	 			HttpServletResponse response) throws Exception {
	 		System.err.println("DAte : :: :  " + dto.getStartDate());

	 		ResponseDTO result = new ResponseDTO();
	 		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
	 		if (isValidHash) {
	 			if (role.equalsIgnoreCase("Admin")) {
	 				String sessionId = dto.getSessionId();
	 				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	 				if (userSession != null) {
	 					UserDTO user = userApi.getUserById(userSession.getUser().getId());
	 					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
	 							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	 						persistingSessionRegistry.refreshLastRequest(sessionId);
	 						try {
	 							result.setStatus(ResponseStatus.SUCCESS);
	 							result.setMessage("values");
	 							result.setDetails(userApi.getDailyValuesByDate(dto.getStartDate()));
	 							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 						} catch (Exception e) {
	 							e.printStackTrace();
	 							result.setStatus(ResponseStatus.FAILURE);
	 							result.setMessage("Failed, Please try again later");
	 							result.setDetails("Failed, Please try again later");
	 							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 						}
	 					} else {
	 						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 						result.setMessage("Failed, Unauthorized user");
	 						result.setDetails("Failed, Unauthorized user");
	 						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 					}
	 				} else {
	 					result.setStatus(ResponseStatus.INVALID_SESSION);
	 					result.setMessage("Please, login and try again");
	 					result.setDetails("Please, login and try again");
	 					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 				}
	 			} else {
	 				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
	 				result.setMessage("Failed, Unauthorized user");
	 				result.setDetails("Failed, Unauthorized user");
	 				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	 			}
	 		} else {
	 			result.setStatus(ResponseStatus.INVALID_HASH);
	 			result.setMessage("Failed, Please try again later.");
	 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		}
	 	}
	 	
	 	
	 	
	 	@RequestMapping(method = RequestMethod.POST, value = "/MISReport", produces = {
	 			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	 	ResponseEntity<ResponseDTO> getMISReport(@PathVariable(value = "role") String role,
	 			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
	 			@RequestBody SessionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
	 			HttpServletResponse response) throws Exception {

	 		ResponseDTO result = new ResponseDTO();
	 		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
	 		if (isValidHash) {
	 			if (role.equalsIgnoreCase("Admin")) {
	 				String sessionId = dto.getSessionId();
	 				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	 				if (userSession != null) {
	 					UserDTO user = userApi.getUserById(userSession.getUser().getId());
	 					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
	 							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	 						persistingSessionRegistry.refreshLastRequest(sessionId);
	 						try {
	 							result.setStatus(ResponseStatus.SUCCESS);
	 							result.setMessage("values");
	 							result.setDetails(userApi.getMISReport());
	 							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 						} catch (Exception e) {
	 							e.printStackTrace();
	 							result.setStatus(ResponseStatus.FAILURE);
	 							result.setMessage("Failed, Please try again later");
	 							result.setDetails("Failed, Please try again later");
	 							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 						}
	 					} else {
	 						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 						result.setMessage("Failed, Unauthorized user");
	 						result.setDetails("Failed, Unauthorized user");
	 						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 					}
	 				} else {
	 					result.setStatus(ResponseStatus.INVALID_SESSION);
	 					result.setMessage("Please, login and try again");
	 					result.setDetails("Please, login and try again");
	 					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 				}
	 			} else {
	 				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
	 				result.setMessage("Failed, Unauthorized user");
	 				result.setDetails("Failed, Unauthorized user");
	 				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	 			}
	 		} else {
	 			result.setStatus(ResponseStatus.INVALID_HASH);
	 			result.setMessage("Failed, Please try again later.");
	 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		}
	 	}
	 	
	 	
	 	@RequestMapping(method = RequestMethod.POST, value = "/WalletLoadReport", produces = {
	 			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	 	ResponseEntity<ResponseDTO> getWalletLoadReport(@PathVariable(value = "role") String role,
	 			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
	 			@RequestBody WalletLoadReportDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
	 			HttpServletResponse response) throws Exception {

	 		ResponseDTO result = new ResponseDTO();
	 		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
	 		if (isValidHash) {
	 			if (role.equalsIgnoreCase("Admin")) {
	 				String sessionId = dto.getSessionId();
	 				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
	 				if (userSession != null) {
	 					UserDTO user = userApi.getUserById(userSession.getUser().getId());
	 					if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
	 							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
	 						persistingSessionRegistry.refreshLastRequest(sessionId);
	 						try {
	 							Date date=dateFormat.parse(dto.getDate());
	 							Date endDate=dateFormat.parse(dto.getEndDate());
	 							WalletLoadRepot  repot=userApi.getWalletLoadReport(date,endDate);
	 							result.setStatus(ResponseStatus.SUCCESS);
	 							result.setMessage("values");
	 							result.setDetails(repot.getCcAvenueLoadMoney());
	 							result.setUserList(repot.getUserList());
	 							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 						} catch (Exception e) {
	 							e.printStackTrace();
	 							result.setStatus(ResponseStatus.FAILURE);
	 							result.setMessage("Failed, Please try again later");
	 							result.setDetails("Failed, Please try again later");
	 							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 						}
	 					} else {
	 						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
	 						result.setMessage("Failed, Unauthorized user");
	 						result.setDetails("Failed, Unauthorized user");
	 						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 					}
	 				} else {
	 					result.setStatus(ResponseStatus.INVALID_SESSION);
	 					result.setMessage("Please, login and try again");
	 					result.setDetails("Please, login and try again");
	 					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 				}
	 			} else {
	 				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
	 				result.setMessage("Failed, Unauthorized user");
	 				result.setDetails("Failed, Unauthorized user");
	 				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

	 			}
	 		} else {
	 			result.setStatus(ResponseStatus.INVALID_HASH);
	 			result.setMessage("Failed, Please try again later.");
	 			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	 		}
	 	}
	 	
	 	@RequestMapping(method = RequestMethod.POST, value = "/findWooHooTransactions", produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> getWooHooTransaction(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response) throws Exception {
	 		ResponseDTO result = new ResponseDTO();
			boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
			if (isValidHash) {
				if (role.equalsIgnoreCase("Admin")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								String reportType = dto.getReportType();
								List<TransactionReport> reports = null;
								String startDate = dto.getFromDate() + " 00:00";
								String endDate = dto.getToDate()+ " 23:59";
								dto.setStartDate(dateFormat.parse(startDate));
								dto.setEndDate(dateFormat.parse(endDate));
								String operatorCode = "WOOHO";
							    boolean debit = true;
								PQService type = pqServiceRepository.findServiceByOperatorCode(operatorCode);
								List<PQTransaction>	transactions = transactionApi.listWooHooTransactionByDate(debit,dto.getStartDate(),
										dto.getEndDate(),type, Status.Success);
								List<NikkiChatResponseDTO> nikkiList=new ArrayList<>();
								for (PQTransaction pqTransaction : transactions) {
								User u=	userApi.findByAccountDetail(pqTransaction.getAccount());
								NikkiChatResponseDTO res=new NikkiChatResponseDTO();
								res.setContactNo(u.getUsername());
								if(u.getUserDetail()!=null){
									res.setUsername(u.getUserDetail().getFirstName());
								}
								res.setAuthority(u.getAuthority());
								res.setAmount(String.valueOf(pqTransaction.getAmount()));
								res.setDescription(pqTransaction.getDescription());
								res.setTransactionRefNo(pqTransaction.getTransactionRefNo());
								res.setStatus(pqTransaction.getStatus().getValue());
								res.setService(type.getServiceName());
								String d1=dateFormat.format(pqTransaction.getCreated());
								res.setDate(d1);
								nikkiList.add(res);
								}
								//List<User> us=userApi.listUser();
								//reports = ConvertUtil.getTransactionList(transactions, us );
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(reportType + " List");
								result.setDetails(nikkiList);
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Ajax | Transaction List");
								result.setDetails("Permission Not Granted");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Ajax | Transaction List");
						result.setDetails("Invalid Session");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Ajax | Transaction List");
					result.setDetails("Permission Not Granted");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid Secure Hash");
			}
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	 	
	 	@RequestMapping(value = "/Offers/Save", method = RequestMethod.POST, produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> saveOffers(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody OffersDTO dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response)
				throws JSONException, JsonGenerationException, JsonMappingException, IOException {
			ResponseDTO result = new ResponseDTO();
			boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
			if (isValidHash) {
				if (role.equalsIgnoreCase("Admin")) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user != null) {
							if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
									&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
								userApi.AddOffers(dto);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Offers saved Successfully.");
								result.setDetails("Offers saved Successfully.");
							} else {
								result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
								result.setMessage("Failed to get user receipts");
								result.setDetails("Failed to get user receipts");
							}
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please login and try again.");
						result.setDetails("Please login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Unauthorized user.");
					result.setDetails("Unauthorized user.");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid request.");
				result.setDetails("Invalid request.");
			}
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	 	
	 	@RequestMapping(value = "/FilteredNeftRequest", method = RequestMethod.POST, produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> getFilteredNeftRequest(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response)
				throws JSONException, JsonGenerationException, JsonMappingException, IOException, ParseException {
			ResponseDTO result = new ResponseDTO();
			String from = dto.getFromDate() + " 00:00";
			String to = dto.getToDate() + " 23:59";
			boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
			if (isValidHash) {
				if (role.equalsIgnoreCase("Admin")) {
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							Page<BankTransfer> pg=null;
							if(dto.isPageable()){
								Sort sort = new Sort(Sort.Direction.DESC, "id");
								Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
								pg = adminApi.getAllTransferReportsByDateandPageWise(pageable,dateFormat.parse(from),dateFormat.parse(to));
								List<BankTransfer> bankTransfer = pg.getContent();
							List<BankTransferDTO> bankTransferDTOs = new ArrayList<>();
							User bank = userApi.findByUserName("bank@vpayqwik.com");
							String accountNumber = String.valueOf(bank.getAccountDetail().getAccountNumber());
							for (BankTransfer bt : bankTransfer) {
								BankTransferDTO bdto = new BankTransferDTO();
								bdto.setTransactionDate(dateFormat.format(bt.getCreated()));
								if(bt.getSender().getUserDetail()!=null){
									bdto.setName(bt.getSender().getUserDetail().getFirstName()!=null?bt.getSender().getUserDetail().getFirstName():"NA");
									bdto.setEmail(bt.getSender().getUserDetail().getEmail()!=null?bt.getSender().getUserDetail().getEmail():"NA");
								}
								else{
									bdto.setName("NA");
									bdto.setEmail("NA");
								}
								bdto.setMobileNumber(bt.getSender().getUsername());
								bdto.setAmount("" + bt.getAmount());
								BankDetails bankDetails = bt.getBankDetails();
								if (bankDetails != null) {
									bdto.setBankName(bankDetails.getBank().getName());
									bdto.setIfscCode(bankDetails.getIfscCode());
								}
								bdto.setBeneficiaryAccountName(bt.getBeneficiaryName());
								bdto.setBeneficiaryAccountNumber("" + bt.getBeneficiaryAccountNumber());
								bdto.setVirtualAccount("" + bt.getSender().getAccountDetail().getAccountNumber());
								bdto.setTransactionID(bt.getTransactionRefNo());
								bdto.setBankVirtualAccount(accountNumber);
								bdto.setStatus(String.valueOf(Status.Success));
								PQTransaction temp = adminApi.getTransactionByRefNo(bt.getTransactionRefNo());
								if (temp != null) {
									bdto.setStatus(temp.getStatus().getValue());
									bdto.setTransactionDate(dateFormat.format(temp.getCreated()));
								}
								bankTransferDTOs.add(bdto);
							}
							result.setMessage("PromoCode list.");
							result.setStatus(ResponseStatus.SUCCESS);
							result.setDetails(bankTransferDTOs);
							result.setCount(pg.getNumberOfElements());
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
							}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Unauthorized user.");
							result.setDetails(null);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Session invalid.");
						result.setDetails(null);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				}
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Unable to process request");
				result.setDetails(null);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}

		}
	 	
		@RequestMapping(method = RequestMethod.POST, value = "/PredictAndWinUpload", produces = {
				MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
		ResponseEntity<ResponseDTO> getPredictAndWinUpload(@PathVariable(value = "role") String role,
				@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
				@RequestBody BulkUploadDTO dto, @RequestHeader(value = "hash", required = true) String hash,
				HttpServletRequest request, HttpServletResponse response) throws Exception {
			ResponseDTO result = new ResponseDTO();
			boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
			if (isValidHash) {
				if (role.equalsIgnoreCase("Admin")) {
					System.err.println("inside predict win and payment.");
					String sessionId = dto.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							        adminApi.savePredictAndWinFile(dto);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("file uploaded successfully");
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Permission Not Granted");
							result.setDetails("Permission Not Granted");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Invalid Session");
						result.setDetails("Invalid Session");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
					result.setMessage("Failed,Un-Authorized User");
					result.setDetails("Permission Not Granted");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Invalid Secure Hash");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	     
	@RequestMapping(method = RequestMethod.POST, value = "/editPromoCode", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getPromocode(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PromoCodeDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
					throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				persistingSessionRegistry.refreshLastRequest(dto.getSessionId());
				dto = promoCodeApi.getPromocodeById(dto.getPromoCodeId());
				result.setStatus(ResponseStatus.SUCCESS);
				result.setDetails(dto);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION);
			result.setMessage("Session invalid.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/addVouchers", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> addVouchers(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AddVoucherDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO Admin = userApi.getUserById(userSession.getUser().getId());
					if (Admin != null) {
						if (Admin.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& Admin.getAuthority().contains(Authorities.AUTHENTICATED)) {
							int totalGenreted=transactionApi.addVouchers(dto);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Vouchers Added Successfully");
							result.setDetails(totalGenreted);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Unable to add vouchers.Unauthoruzed user");
							result.setDetails("Permission Not Granted");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Session Invalid.Please Login");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unable to add vouchers.Unauthoruzed user");
				result.setDetails("Permission Not Granted");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/getTotalVouchers", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getVoucherPages(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PagingDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Admin")) {
				UserSession userSession = userSessionRepository.findByActiveSessionId(dto.getSessionId());
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user != null) {
						if (user.getAuthority().contains(Authorities.ADMINISTRATOR)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							Sort sort = new Sort(Sort.Direction.DESC, "id");
							Pageable pageable = new PageRequest(dto.getPage(), dto.getSize(), sort);
							Page<Voucher> pg = null;
							pg = transactionApi.getAllVouchers(pageable);
							result.setValid(true);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Voucher List");
							result.setDetails(pg);
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("UnAuthorized User");
							result.setDetails("UnAuthorized User");
						}
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Invalid Session");
					result.setDetails("Invalid Session");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("UnAuthorized User");
				result.setDetails("UnAuthorized User");

			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
}