package com.payqwikapp.controller.api.v1;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.NearAgentsDetailsDTO;
import com.payqwikapp.model.NearByAgentResponseDTO;
import com.payqwikapp.model.NearByAgentsDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}")
public class NearByAgentController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final IUserApi userApi;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final UserSessionRepository userSessionRepository;
	private final static String USER = "User";

	public NearByAgentController(IUserApi userApi, PersistingSessionRegistry persistingSessionRegistry,
			UserSessionRepository userSessionRepository) {
		this.userApi = userApi;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.userSessionRepository = userSessionRepository;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/findNearByAgents", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NearByAgentResponseDTO> findByAgents(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody NearByAgentsDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response) {

		NearByAgentResponseDTO nearByAgentResponse = new NearByAgentResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase(USER)) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if ((user.getAuthority().contains(Authorities.USER))
							&& (user.getAuthority().contains(Authorities.AUTHENTICATED))) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						if (user.getUserType().equals(UserType.User)) {
							List<NearAgentsDetailsDTO> agentList = userApi.nearByAgentDetails(dto);
							if (agentList != null && !agentList.isEmpty()) {
								nearByAgentResponse.setStatus(ResponseStatus.SUCCESS);
								nearByAgentResponse.setMessage("Agent List");
								nearByAgentResponse.setDetails(agentList);
								nearByAgentResponse.setCode(ResponseStatus.SUCCESS.getValue());
							} else {
								logger.info("agent is not available");
								nearByAgentResponse.setStatus(ResponseStatus.FAILURE);
								nearByAgentResponse.setMessage("No Nearest Agent Found.");
								nearByAgentResponse.setDetails("No Nearest Agent Found.");
								nearByAgentResponse.setCode(ResponseStatus.FAILURE.getValue());
							}
						} else {
							nearByAgentResponse.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							nearByAgentResponse.setMessage("Failed, Unauthorized user.");
							nearByAgentResponse.setDetails("Failed, Unauthorized user.");
							nearByAgentResponse.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						}

					} else {
						nearByAgentResponse.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
						nearByAgentResponse.setMessage("Failed, Unauthorized Role.");
						nearByAgentResponse.setDetails("Failed, Unauthorized Role.");
						nearByAgentResponse.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getValue());
					}
				}
			} else {
				nearByAgentResponse.setStatus(ResponseStatus.INVALID_SESSION);
				nearByAgentResponse.setMessage("Failed, Invalid Session");
				nearByAgentResponse.setDetails("Failed, Invalid Session");
				nearByAgentResponse.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}

		} else {
			nearByAgentResponse.setStatus(ResponseStatus.INVALID_HASH);
			nearByAgentResponse.setMessage("Failed, Unauthorized user.");
			nearByAgentResponse.setDetails("Failed, Unauthorized user.");
			nearByAgentResponse.setCode(ResponseStatus.INVALID_HASH.getValue());
		}

		return new ResponseEntity<NearByAgentResponseDTO>(nearByAgentResponse, HttpStatus.OK);
	}

}
