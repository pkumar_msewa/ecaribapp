package com.payqwikapp.controller.api.v1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.payqwikapp.model.error.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.ITopupAndBillPaymentApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.DTHBillPaymentDTO;
import com.payqwikapp.model.ElectricityBillPaymentDTO;
import com.payqwikapp.model.GasBillPaymentDTO;
import com.payqwikapp.model.InsuranceBillPaymentDTO;
import com.payqwikapp.model.LandlineBillPaymentDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.DTHBillPaymentValidation;
import com.payqwikapp.validation.ElectricityBillPaymentValidation;
import com.payqwikapp.validation.GasBillPaymentValidation;
import com.payqwikapp.validation.InsuranceBillPaymentValidation;
import com.payqwikapp.validation.LandlineBillPaymentValidation;
import com.payqwikapp.validation.TransactionValidation;

@Controller
@RequestMapping("/Api/v1/{role}/{device}/{language}/BillPay")
public class BillPaymentController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IUserApi userApi;
	private final ITopupAndBillPaymentApi topupAndBillPaymentApi;
	private final DTHBillPaymentValidation dthBillPaymentValidation;
	private final GasBillPaymentValidation gasBillPaymentValidation;
	private final InsuranceBillPaymentValidation insuranceBillPaymentValidation;
	private final ElectricityBillPaymentValidation electricityBillPaymentValidation;
	private final LandlineBillPaymentValidation landlineBillPaymentValidation;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final ICommissionApi commissionApi;

	public BillPaymentController(IUserApi userApi, ITopupAndBillPaymentApi topupAndBillPaymentApi,
			DTHBillPaymentValidation dthBillPaymentValidation, GasBillPaymentValidation gasBillPaymentValidation,
			InsuranceBillPaymentValidation insuranceBillPaymentValidation,
			ElectricityBillPaymentValidation electricityBillPaymentValidation,
			LandlineBillPaymentValidation landlineBillPaymentValidation, TransactionValidation transactionValidation,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			PQServiceRepository pqServiceRepository,ICommissionApi commissionApi) {
		this.userApi = userApi;
		this.topupAndBillPaymentApi = topupAndBillPaymentApi;
		this.dthBillPaymentValidation = dthBillPaymentValidation;
		this.gasBillPaymentValidation = gasBillPaymentValidation;
		this.insuranceBillPaymentValidation = insuranceBillPaymentValidation;
		this.electricityBillPaymentValidation = electricityBillPaymentValidation;
		this.landlineBillPaymentValidation = landlineBillPaymentValidation;
		this.transactionValidation = transactionValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.commissionApi=commissionApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/ProcessDTH", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processDTH(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CommonRechargeDTO dth, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dth, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Agent")) {
				DTHBillPaymentError error = new DTHBillPaymentError();
				error = dthBillPaymentValidation.checkError(dth);
				if (error.isValid()) {
					String sessionId = dth.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(dth.getServiceProvider());
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(dth.getAmount()));
							double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(dth.getAmount()));
								TransactionError transactionError = transactionValidation
										.validateBillPayment(dth.getAmount(), user.getUsername(),commission,netCommissionValue);
								if (transactionError.isValid()) {
									TransactionResponse ipayResponse = topupAndBillPaymentApi.dthBillPayment(dth,
											user.getUsername(), service,commission,netCommissionValue);
									String msg = (ipayResponse.isSuccess() ? ipayResponse.getTransaction().getResMsg()
											: ipayResponse.getValidation().getIpayErrorDesc());
									result.setStatus(
											(ipayResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
									result.setMessage(msg);
									result.setDetails(msg);
									User u = userApi.findByUserName(user.getUsername());
									result.setBalance(userApi.getWalletBalance(user));
									userApi.updateRequestLog(u,timeStamp);
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								} else {
									result.setStatus("FAILURE");
									result.setMessage(transactionError.getMessage());
									result.setCode(transactionError.getCode());
									result.setRemBalance(transactionError.getSplitAmount());
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage(error.getMessage());
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessLandline", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processLandline(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CommonRechargeDTO landline, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(landline, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Agent")) {
				LandlineBillPaymentError error = new LandlineBillPaymentError();
				error = landlineBillPaymentValidation.checkError(landline);
				if (error.isValid()) {
					String sessionId = landline.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(landline.getServiceProvider());
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(landline.getAmount()));
							double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(landline.getAmount()));
								TransactionError transactionError = transactionValidation.validateBillPayment(landline.getAmount(), user.getUsername(),commission,netCommissionValue);
								if (transactionError.isValid()) {
									TransactionResponse ipayResponse = topupAndBillPaymentApi.landlineBillPayment(landline,
											user.getUsername(), service,commission,netCommissionValue);
									String msg = (ipayResponse.isSuccess() ? ipayResponse.getTransaction().getResMsg()
											: ipayResponse.getValidation().getIpayErrorDesc());
									result.setStatus(
											(ipayResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
									result.setMessage(msg);
									result.setDetails(msg);
									User u = userApi.findByUserName(user.getUsername());
									result.setBalance(ipayResponse.getRemainingBalance());
									userApi.updateRequestLog(u,timeStamp);
								} else {
									result.setStatus("FAILURE");
									result.setMessage(transactionError.getMessage());
									result.setCode(transactionError.getCode());
									result.setRemBalance(transactionError.getSplitAmount());
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
								}
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessElectricity", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processElectricity(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CommonRechargeDTO electricity,
			@RequestHeader(value = "hash", required = true) String hash, HttpServletRequest request, ModelMap modelMap,
			HttpServletResponse response) {
		long timeStamp = System.nanoTime();
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(electricity, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Agent")) {
				ElectricityBillPaymentError error = new ElectricityBillPaymentError();
				error = electricityBillPaymentValidation.checkError(electricity);
				if (error.isValid()) {
					UserSession userSession = userSessionRepository.findByActiveSessionId(electricity.getSessionId());
					if (userSession != null) {
						User user = userApi.findById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(electricity.getSessionId());
							PQService service = pqServiceRepository.findServiceByCode(electricity.getServiceProvider());
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(electricity.getAmount()));
							double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(electricity.getAmount()));
                                TransactionError transactionError = transactionValidation.validateBillPayment(electricity.getAmount(), user.getUsername(),commission,netCommissionValue);
                                if (transactionError.isValid()) {
                                    TransactionResponse ipayResponse = topupAndBillPaymentApi
                                            .electricityBillPayment(electricity, user.getUsername(), service,commission,netCommissionValue);
                                    String msg = (ipayResponse.isSuccess() ? ipayResponse.getTransaction().getResMsg()
                                                  : ipayResponse.getValidation().getIpayErrorDesc());
                                    result.setStatus((ipayResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
                                    result.setMessage(msg);
                                    result.setDetails(msg);
                                    result.setBalance(ipayResponse.getRemainingBalance());
                                    userApi.updateRequestLog(user,timeStamp);
                                } else {
                                	result.setStatus("FAILURE");
									result.setMessage(transactionError.getMessage());
									result.setCode(transactionError.getCode());
									result.setRemBalance(transactionError.getSplitAmount());
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                }
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessGas", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processGas(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CommonRechargeDTO gas, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
        ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(gas, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Agent")) {
				GasBillPaymentError error = new GasBillPaymentError();
				error = gasBillPaymentValidation.checkError(gas);
				if (error.isValid()) {
					String sessionId = gas.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(gas.getServiceProvider());
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(gas.getAmount()));
							double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(gas.getAmount()));
                                TransactionError transactionError = transactionValidation
                                        .validateBillPayment(gas.getAmount(), user.getUsername(),commission,netCommissionValue);
                                if (transactionError.isValid()) {
                                    TransactionResponse ipayResponse = topupAndBillPaymentApi.gasBillPayment(gas,
                                            user.getUsername(), service,commission,netCommissionValue);
                                    String msg = (ipayResponse.isSuccess() ? ipayResponse.getTransaction().getResMsg()
                                            : ipayResponse.getValidation().getIpayErrorDesc());
                                    result.setStatus(
                                            (ipayResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
                                    result.setMessage(msg);
                                    result.setDetails(msg);
                                    User u = userApi.findByUserName(user.getUsername());
                                    result.setBalance(ipayResponse.getRemainingBalance());
                                    userApi.updateRequestLog(u,timeStamp);
                                } else {
                                	result.setStatus("FAILURE");
									result.setMessage(transactionError.getMessage());
									result.setCode(transactionError.getCode());
									result.setRemBalance(transactionError.getSplitAmount());
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                }
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessInsurance", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> processInsurance(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody CommonRechargeDTO insurance, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		long timeStamp = System.nanoTime();
        ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(insurance, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")||role.equalsIgnoreCase("Agent")) {
				InsuranceBillPaymentError error = new InsuranceBillPaymentError();
				error = insuranceBillPaymentValidation.checkError(insurance);
				if (error.isValid()) {
					String sessionId = insurance.getSessionId();
					UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
					if (userSession != null) {
						UserDTO user = userApi.getUserById(userSession.getUser().getId());
						if (user.getAuthority().contains(Authorities.USER)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)||user.getAuthority().contains(Authorities.AGENT)
								&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
							persistingSessionRegistry.refreshLastRequest(sessionId);
							PQService service = pqServiceRepository.findServiceByCode(insurance.getServiceProvider());
							PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service,Double.parseDouble(insurance.getAmount()));
							double netCommissionValue = commissionApi.getCommissionValue(commission,Double.parseDouble(insurance.getAmount()));
                                TransactionError transactionError = transactionValidation.validateBillPayment(insurance.getAmount(), user.getUsername(),commission,netCommissionValue);
                                if (transactionError.isValid()) {
                                    TransactionResponse ipayResponse = topupAndBillPaymentApi
                                            .insuranceBillPayment(insurance, user.getUsername(), service,commission,netCommissionValue);
                                    String msg = (ipayResponse.isSuccess() ? ipayResponse.getTransaction().getResMsg()
                                            : ipayResponse.getValidation().getIpayErrorDesc());
                                    result.setStatus(
                                            (ipayResponse.isSuccess() ? ResponseStatus.SUCCESS : ResponseStatus.FAILURE));
                                    result.setMessage(msg);
                                    result.setDetails(msg);
                                    User u = userApi.findByUserName(user.getUsername());
                                    result.setBalance(ipayResponse.getRemainingBalance());
                                    userApi.updateRequestLog(u,timeStamp);
                                } else {
                                	result.setStatus("FAILURE");
									result.setMessage(transactionError.getMessage());
									result.setCode(transactionError.getCode());
									result.setRemBalance(transactionError.getSplitAmount());
									return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                }
						} else {
							result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
							result.setMessage("Failed, Unauthorized user.");
							result.setDetails("Failed, Unauthorized user.");
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_SESSION);
						result.setMessage("Please, login and try again.");
						result.setDetails("Please, login and try again.");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Failed, invalid request.");
					result.setDetails(error);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

}
