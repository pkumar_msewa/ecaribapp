package com.payqwikapp.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CommonRechargeDTO {

	private String sessionId;
	private TopupType topupType;
	private String serviceProvider;
	private String mobileNo;
	private String area;
	private String amount;
	private boolean fromMdex;
	private String accountNumber;
	private String policyNumber;
	private String policyDate;
	private String cycleNumber;
	private String cycleName;
	private String billingUnit;
	private String cityName;
	private String stdCode;
	private String dthNo;
	private String landlineNumber;
	private String processingCycle;
	private String transactionRefNo;
	private String serviceCode;
	
	
	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getProcessingCycle() {
		return processingCycle;
	}

	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getDthNo() {
		return dthNo;
	}

	public void setDthNo(String dthNo) {
		this.dthNo = dthNo;
	}

	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyDate() {
		return policyDate;
	}

	public void setPolicyDate(String policyDate) {
		this.policyDate = policyDate;
	}

	public String getCycleNumber() {
		return cycleNumber;
	}

	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	public String getBillingUnit() {
		return billingUnit;
	}

	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public TopupType getTopupType() {
		return topupType;
	}

	public void setTopupType(TopupType topupType) {
		this.topupType = topupType;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public boolean isFromMdex() {
		return fromMdex;
	}

	public void setFromMdex(boolean fromMdex) {
		this.fromMdex = fromMdex;
	}

	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("serviceProvider", serviceProvider);
			json.put("mobileNo", mobileNo);
			json.put("area", area);
			json.put("amount", amount);
			json.put("fromMdex", fromMdex);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

}
