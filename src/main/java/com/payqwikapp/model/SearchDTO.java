package com.payqwikapp.model;

public class SearchDTO extends SessionDTO{
    private String mobileSubString;

    public String getMobileSubString() {
        return mobileSubString;
    }

    public void setMobileSubString(String mobileSubString) {
        this.mobileSubString = mobileSubString;
    }
}
