package com.payqwikapp.model;

import java.util.Date;

public class UserDetailDTO {

	private String username;
	private double monthlyLimit;
	private double dailyLimit;
	private double balanceLimit;
	private String mobileStatus; 
	private double balance;
	private String contactNo;
	private double currentMonthCredit;
	private double currentMonthDebit;
	private double dailyDebitAmount;
	private double dailyCrebitAmount;
	private Date lastTransactionTime;
	private Date lastReverseTransactionTime;
	private double lastSuccessTransactionBalance;
	
	private double totalMonthlyTransaction;
	
	public double getTotalMonthlyTransaction() {
		return totalMonthlyTransaction;
	}
	public void setTotalMonthlyTransaction(double totalMonthlyTransaction) {
		this.totalMonthlyTransaction = totalMonthlyTransaction;
	}
	public double getLastSuccessTransactionBalance() {
		return lastSuccessTransactionBalance;
	}
	public void setLastSuccessTransactionBalance(double lastSuccessTransactionBalance) {
		this.lastSuccessTransactionBalance = lastSuccessTransactionBalance;
	}
	public double getDailyDebitAmount() {
		return dailyDebitAmount;
	}
	public void setDailyDebitAmount(double dailyDebitAmount) {
		this.dailyDebitAmount = dailyDebitAmount;
	}
	public double getDailyCrebitAmount() {
		return dailyCrebitAmount;
	}
	public void setDailyCrebitAmount(double dailyCrebitAmount) {
		this.dailyCrebitAmount = dailyCrebitAmount;
	}
	public double getCurrentMonthDebit() {
		return currentMonthDebit;
	}
	public void setCurrentMonthDebit(double currentMonthDebit) {
		this.currentMonthDebit = currentMonthDebit;
	}
	public double getCurrentMonthCredit() {
		return currentMonthCredit;
	}
	public void setCurrentMonthCredit(double currentMonthCredit) {
		this.currentMonthCredit = currentMonthCredit;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public double getMonthlyLimit() {
		return monthlyLimit;
	}
	public void setMonthlyLimit(double monthlyLimit) {
		this.monthlyLimit = monthlyLimit;
	}
	public double getDailyLimit() {
		return dailyLimit;
	}
	public void setDailyLimit(double dailyLimit) {
		this.dailyLimit = dailyLimit;
	}
	public double getBalanceLimit() {
		return balanceLimit;
	}
	public void setBalanceLimit(double balanceLimit) {
		this.balanceLimit = balanceLimit;
	}
	public String getMobileStatus() {
		return mobileStatus;
	}
	public void setMobileStatus(String mobileStatus) {
		this.mobileStatus = mobileStatus;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public Date getLastTransactionTime() {
		return lastTransactionTime;
	}
	public void setLastTransactionTime(Date lastTransactionTime) {
		this.lastTransactionTime = lastTransactionTime;
	}
	public Date getLastReverseTransactionTime() {
		return lastReverseTransactionTime;
	}
	public void setLastReverseTransactionTime(Date lastReverseTransactionTime) {
		this.lastReverseTransactionTime = lastReverseTransactionTime;
	}
	
}
