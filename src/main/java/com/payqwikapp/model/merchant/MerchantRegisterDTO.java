package com.payqwikapp.model.merchant;

public class MerchantRegisterDTO {

    private String firstName;

    private String lastName;

    private String email;

    private String mobileNumber;

    private String password;

    private String confirmPassword;

    private MerchantAddressDTO addressDTO;

    private MerchantBankDTO merchantBankDTO;

    private SettlementBankDTO settlementBankDTO;

    private MerchantUID uidDTO;

    private MerchantKycDTO kycDTO;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public MerchantAddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setAddressDTO(MerchantAddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }

    public MerchantBankDTO getMerchantBankDTO() {
        return merchantBankDTO;
    }

    public void setMerchantBankDTO(MerchantBankDTO merchantBankDTO) {
        this.merchantBankDTO = merchantBankDTO;
    }

    public SettlementBankDTO getSettlementBankDTO() {
        return settlementBankDTO;
    }

    public void setSettlementBankDTO(SettlementBankDTO settlementBankDTO) {
        this.settlementBankDTO = settlementBankDTO;
    }

    public MerchantUID getUidDTO() {
        return uidDTO;
    }

    public void setUidDTO(MerchantUID uidDTO) {
        this.uidDTO = uidDTO;
    }

    public MerchantKycDTO getKycDTO() {
        return kycDTO;
    }

    public void setKycDTO(MerchantKycDTO kycDTO) {
        this.kycDTO = kycDTO;
    }
}
