package com.payqwikapp.model;

import java.util.List;

import com.payqwikapp.model.mobile.ResponseStatus;

public class PlansDTO {
	
	private String status;
	private String code;
	private String message;
	private List<TreatCardPlansDTO> plans;

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		code = status.getValue();
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<TreatCardPlansDTO> getPlans() {
		return plans;
	}

	public void setPlans(List<TreatCardPlansDTO> plans) {
		this.plans = plans;
	}

	public void setCode(String code) {
		this.code = code;
	}




}
