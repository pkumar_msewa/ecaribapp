package com.payqwikapp.model;

import java.util.Date;

import com.payqwikapp.model.mobile.ResponseStatus;

public class TreatCardPaymentResponse {

	private String status;
	private String code;
	private boolean success;
	private String message;
	private String sessionId;
	private double balance;
	private Object details;
	private long membershipCode;
	private Date memberShipCodeExpire;

	public long getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(long membershipCode) {
		this.membershipCode = membershipCode;
	}

	public Date getMemberShipCodeExpire() {
		return memberShipCodeExpire;
	}

	public void setMemberShipCodeExpire(Date memberShipCodeExpire) {
		this.memberShipCodeExpire = memberShipCodeExpire;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
