package com.payqwikapp.model;

public class TreatCardRegisterDTO {

	private long membershipCode;
	private String memberShipCodeExpire;
	private String username;
	private String emailId;
	private String firstName;
	private long count;
	private String created;
	
	
	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(long membershipCode) {
		this.membershipCode = membershipCode;
	}

	public String getMemberShipCodeExpire() {
		return memberShipCodeExpire;
	}

	public void setMemberShipCodeExpire(String memberShipCodeExpire) {
		this.memberShipCodeExpire = memberShipCodeExpire;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
