package com.payqwikapp.model;

public class RazorPayRequest extends SessionDTO {

	private double amount;

	private String transactionId;

	private String razorPayPaymetId;

	public String getRazorPayPaymetId() {
		return razorPayPaymetId;
	}

	public void setRazorPayPaymetId(String razorPayPaymetId) {
		this.razorPayPaymetId = razorPayPaymetId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
}
