package com.payqwikapp.model;

public class SummaryReportDTO {

	private long totalUser;
	private long totalNoOfTransaction;
	private double totalDebitTransaction;
	private double totalCreditTransaction;
	private String date;
	private long totalNoOfInitiatedTransaction;
	private double totalEBSCreditTrasnaction;
	private double totalVnetCreditTransaction;
	private double totalUpiCreditTransaction;
	private long totalNoOfFailedTransaction;

	private long userGrowth;
	private long noOfTransactionGrowth;
	private long totalDebitGrowth;
	private long totalCreditGrowth;
	private long totalEBSCreditGrowth;
	private long totalVentCreditGrowth;
	private long totalUPICreditGrowth;
	private long noOfInitiatedTransactionGrowth;
	private long totalNoOfFailedTransactionGrowth;

	private String userGrowthString;
	private String totalDebitGrowthString;
	private String totalCreditGrowthString;
	private String totalEBSCreditGrowthString;
	private String totalVnetCreditGrowthString;
	private String totalUPICreditGrowthString;
	private String noOfTransactionGrowthString;
	private String noOfInitiatedTransactionGrowthString;
	private String noOfFailedTransactionGrowthString;
	
	
	
	public long getTotalNoOfFailedTransactionGrowth() {
		return totalNoOfFailedTransactionGrowth;
	}

	public void setTotalNoOfFailedTransactionGrowth(long totalNoOfFailedTransactionGrowth) {
		this.totalNoOfFailedTransactionGrowth = totalNoOfFailedTransactionGrowth;
	}

	public long getTotalEBSCreditGrowth() {
		return totalEBSCreditGrowth;
	}

	public void setTotalEBSCreditGrowth(long totalEBSCreditGrowth) {
		this.totalEBSCreditGrowth = totalEBSCreditGrowth;
	}

	public long getTotalVentCreditGrowth() {
		return totalVentCreditGrowth;
	}

	public void setTotalVentCreditGrowth(long totalVentCreditGrowth) {
		this.totalVentCreditGrowth = totalVentCreditGrowth;
	}

	public long getTotalUPICreditGrowth() {
		return totalUPICreditGrowth;
	}

	public void setTotalUPICreditGrowth(long totalUPICreditGrowth) {
		this.totalUPICreditGrowth = totalUPICreditGrowth;
	}

	public String getTotalEBSCreditGrowthString() {
		return totalEBSCreditGrowthString;
	}

	public void setTotalEBSCreditGrowthString(String totalEBSCreditGrowthString) {
		this.totalEBSCreditGrowthString = totalEBSCreditGrowthString;
	}

	public String getTotalVnetCreditGrowthString() {
		return totalVnetCreditGrowthString;
	}

	public void setTotalVnetCreditGrowthString(String totalVnetCreditGrowthString) {
		this.totalVnetCreditGrowthString = totalVnetCreditGrowthString;
	}

	public String getTotalUPICreditGrowthString() {
		return totalUPICreditGrowthString;
	}

	public void setTotalUPICreditGrowthString(String totalUPICreditGrowthString) {
		this.totalUPICreditGrowthString = totalUPICreditGrowthString;
	}

	public String getNoOfFailedTransactionGrowthString() {
		return noOfFailedTransactionGrowthString;
	}

	public void setNoOfFailedTransactionGrowthString(String noOfFailedTransactionGrowthString) {
		this.noOfFailedTransactionGrowthString = noOfFailedTransactionGrowthString;
	}

	public double getTotalEBSCreditTrasnaction() {
		return totalEBSCreditTrasnaction;
	}

	public void setTotalEBSCreditTrasnaction(double totalEBSCreditTrasnaction) {
		this.totalEBSCreditTrasnaction = totalEBSCreditTrasnaction;
	}

	public double getTotalVnetCreditTransaction() {
		return totalVnetCreditTransaction;
	}

	public void setTotalVnetCreditTransaction(double totalVnetCreditTransaction) {
		this.totalVnetCreditTransaction = totalVnetCreditTransaction;
	}

	public double getTotalUpiCreditTransaction() {
		return totalUpiCreditTransaction;
	}

	public void setTotalUpiCreditTransaction(double totalUpiCreditTransaction) {
		this.totalUpiCreditTransaction = totalUpiCreditTransaction;
	}

	public long getTotalNoOfInitiatedTransaction() {
		return totalNoOfInitiatedTransaction;
	}

	public void setTotalNoOfInitiatedTransaction(long totalNoOfInitiatedTransaction) {
		this.totalNoOfInitiatedTransaction = totalNoOfInitiatedTransaction;
	}

	public long getTotalNoOfFailedTransaction() {
		return totalNoOfFailedTransaction;
	}

	public void setTotalNoOfFailedTransaction(long totalNoOfFailedTransaction) {
		this.totalNoOfFailedTransaction = totalNoOfFailedTransaction;
	}

	public long getNoOfInitiatedTransactionGrowth() {
		return noOfInitiatedTransactionGrowth;
	}

	public void setNoOfInitiatedTransactionGrowth(long noOfInitiatedTransactionGrowth) {
		this.noOfInitiatedTransactionGrowth = noOfInitiatedTransactionGrowth;
	}

	public String getNoOfInitiatedTransactionGrowthString() {
		return noOfInitiatedTransactionGrowthString;
	}

	public void setNoOfInitiatedTransactionGrowthString(String noOfInitiatedTransactionGrowthString) {
		this.noOfInitiatedTransactionGrowthString = noOfInitiatedTransactionGrowthString;
	}

	public String getUserGrowthString() {
		return userGrowthString;
	}

	public void setUserGrowthString(String userGrowthString) {
		this.userGrowthString = userGrowthString;
	}

	public String getNoOfTransactionGrowthString() {
		return noOfTransactionGrowthString;
	}

	public void setNoOfTransactionGrowthString(String noOfTransactionGrowthString) {
		this.noOfTransactionGrowthString = noOfTransactionGrowthString;
	}

	public String getTotalDebitGrowthString() {
		return totalDebitGrowthString;
	}

	public void setTotalDebitGrowthString(String totalDebitGrowthString) {
		this.totalDebitGrowthString = totalDebitGrowthString;
	}

	public String getTotalCreditGrowthString() {
		return totalCreditGrowthString;
	}

	public void setTotalCreditGrowthString(String totalCreditGrowthString) {
		this.totalCreditGrowthString = totalCreditGrowthString;
	}

	public long getTotalDebitGrowth() {
		return totalDebitGrowth;
	}

	public void setTotalDebitGrowth(long totalDebitGrowth) {
		this.totalDebitGrowth = totalDebitGrowth;
	}

	public long getTotalCreditGrowth() {
		return totalCreditGrowth;
	}

	public void setTotalCreditGrowth(long totalCreditGrowth) {
		this.totalCreditGrowth = totalCreditGrowth;
	}

	public long getUserGrowth() {
		return userGrowth;
	}

	public void setUserGrowth(long userGrowth) {
		this.userGrowth = userGrowth;
	}

	public long getNoOfTransactionGrowth() {
		return noOfTransactionGrowth;
	}

	public void setNoOfTransactionGrowth(long noOfTransactionGrowth) {
		this.noOfTransactionGrowth = noOfTransactionGrowth;
	}

	public long getTotalNoOfTransaction() {
		return totalNoOfTransaction;
	}

	public void setTotalNoOfTransaction(long totalNoOfTransaction) {
		this.totalNoOfTransaction = totalNoOfTransaction;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getTotalUser() {
		return totalUser;
	}

	public void setTotalUser(long totalUser) {
		this.totalUser = totalUser;
	}

	public double getTotalDebitTransaction() {
		return totalDebitTransaction;
	}

	public void setTotalDebitTransaction(double totalDebitTransaction) {
		this.totalDebitTransaction = totalDebitTransaction;
	}

	public double getTotalCreditTransaction() {
		return totalCreditTransaction;
	}

	public void setTotalCreditTransaction(double totalCreditTransaction) {
		this.totalCreditTransaction = totalCreditTransaction;
	}
}
