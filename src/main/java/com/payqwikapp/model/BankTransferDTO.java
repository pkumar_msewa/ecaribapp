package com.payqwikapp.model;

import javax.persistence.Column;

public class BankTransferDTO {

    private String name;
    private String mobileNumber;
    private String email;
    private String transactionDate;
    private String transactionID;
    private String bankName;
    private String ifscCode;
    private String beneficiaryAccountNumber;
    private String beneficiaryAccountName;
    private String virtualAccount;
    private String bankVirtualAccount;
    private String amount;
    private String status;
    
    private String receiverEmailId;
    private String receiverMobileNo;
    private String image;
    private byte[] imageContent;
    private String addImageType;
    private byte[] addressImage;
    private String senderName;
	private String senderEmailId;
	private String senderMobileNo;
	private String idProofNo;
	private String description;
	
    public String getAddImageType() {
		return addImageType;
	}
	public void setAddImageType(String addImageType) {
		this.addImageType = addImageType;
	}

	public byte[] getAddressImage() {
		return addressImage;
	}

	public void setAddressImage(byte[] addressImage) {
		this.addressImage = addressImage;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public byte[] getImageContent() {
		return imageContent;
	}

	public void setImageContent(byte[] imageContent) {
		this.imageContent = imageContent;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderEmailId() {
		return senderEmailId;
	}

	public void setSenderEmailId(String senderEmailId) {
		this.senderEmailId = senderEmailId;
	}

	public String getSenderMobileNo() {
		return senderMobileNo;
	}

	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}

	public String getIdProofNo() {
		return idProofNo;
	}

	public void setIdProofNo(String idProofNo) {
		this.idProofNo = idProofNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReceiverEmailId() {
		return receiverEmailId;
	}

	public void setReceiverEmailId(String receiverEmailId) {
		this.receiverEmailId = receiverEmailId;
	}

	public String getReceiverMobileNo() {
		return receiverMobileNo;
	}

	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBeneficiaryAccountNumber() {
        return beneficiaryAccountNumber;
    }

    public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
        this.beneficiaryAccountNumber = beneficiaryAccountNumber;
    }

    public String getBeneficiaryAccountName() {
        return beneficiaryAccountName;
    }

    public void setBeneficiaryAccountName(String beneficiaryAccountName) {
        this.beneficiaryAccountName = beneficiaryAccountName;
    }

    public String getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(String virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public String getBankVirtualAccount() {
        return bankVirtualAccount;
    }

    public void setBankVirtualAccount(String bankVirtualAccount) {
        this.bankVirtualAccount = bankVirtualAccount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	@Override
	public String toString() {
		return "BankTransferDTO [name=" + name + ", mobileNumber=" + mobileNumber + ", email=" + email
				+ ", transactionDate=" + transactionDate + ", transactionID=" + transactionID + ", bankName=" + bankName
				+ ", ifscCode=" + ifscCode + ", beneficiaryAccountNumber=" + beneficiaryAccountNumber
				+ ", beneficiaryAccountName=" + beneficiaryAccountName + ", virtualAccount=" + virtualAccount
				+ ", bankVirtualAccount=" + bankVirtualAccount + ", amount=" + amount + ", status=" + status + "]";
	}
}
