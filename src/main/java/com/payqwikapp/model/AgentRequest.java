package com.payqwikapp.model;

public class AgentRequest {

	private String sessionId;
	private String firstName;
	private String lastName;
	private String agencyName;
	private String mobileNo;
	private String emailAddress;
	private String address1;
	private String address2;//
	private String city;
	private String state;
	private String country;
	private String pinCode;
	private String agentBusinessType;//
	private String lattitude;//
	private String longitude;//
	private String isEnabled;//
	private String panNo;
	private String userName;
	private String aadharNo;
	private String bankAccountNo;
	private String agentName;
	
	private String password;
	// private String aadharNo;
	private String userType;
	// agentBankDetail:
	private String agentAccountName;
	private String agentBankName;
	private String agentBranchName;
	private String agentAccountNumber;
	private String agentBankIfscCode;
	private String agentBankLocation;

	private String encodedBytes;
	private String contentType;
	
	public String getAgencyName() {
		return agencyName;
	}
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	public String getAgentBranchName() {
		return agentBranchName;
	}
	public void setAgentBranchName(String agentBranchName) {
		this.agentBranchName = agentBranchName;
	}
	public String getEncodedBytes() {
		return encodedBytes;
	}
	public void setEncodedBytes(String encodeBytes) {
		this.encodedBytes = encodeBytes;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getAgentBusinessType() {
		return agentBusinessType;
	}

	public void setAgentBusinessType(String agentBusinessType) {
		this.agentBusinessType = agentBusinessType;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getAgentAccountName() {
		return agentAccountName;
	}

	public void setAgentAccountName(String agentAccountName) {
		this.agentAccountName = agentAccountName;
	}

	public String getAgentBankName() {
		return agentBankName;
	}

	public void setAgentBankName(String agentBankName) {
		this.agentBankName = agentBankName;
	}

	public String getAgentAccountNumber() {
		return agentAccountNumber;
	}

	public void setAgentAccountNumber(String agentAccountNumber) {
		this.agentAccountNumber = agentAccountNumber;
	}

	public String getAgentBankIfscCode() {
		return agentBankIfscCode;
	}

	public void setAgentBankIfscCode(String agentBankIfscCode) {
		this.agentBankIfscCode = agentBankIfscCode;
	}

	public String getAgentBankLocation() {
		return agentBankLocation;
	}

	public void setAgentBankLocation(String agentBankLocation) {
		this.agentBankLocation = agentBankLocation;
	}

}
