package com.payqwikapp.model;

public class NearMerchantDetailsDTO {

	private String merchantName;
	private double latitude;
	private double longitude;
	private double betweenDistance;
	private String address;
	
	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getBetweenDistance() {
		return betweenDistance;
	}

	public void setBetweenDistance(double betweenDistance) {
		this.betweenDistance = betweenDistance;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
