package com.payqwikapp.model.admin;

import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;

public class TFilterDTO extends SessionDTO{

    private String startDate;

    private String endDate;

    private Status status;

    private String serviceType;
    
    private String transactionRefNo;
    
    

    public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getStartDate() {
        return startDate+" 00:00";
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate+ " 23:59";
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
