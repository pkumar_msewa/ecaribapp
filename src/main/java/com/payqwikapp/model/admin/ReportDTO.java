package com.payqwikapp.model.admin;

import com.payqwikapp.model.SessionDTO;

public class ReportDTO extends SessionDTO{

    private String startDate;

    private String endDate;

    private String type;
    
    private String status;
    
	private int page;
	
	private int size;
	
	private boolean pageable;
	
	private String transactionType;
	

    public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public boolean isPageable() {
		return pageable;
	}

	public void setPageable(boolean pageable) {
		this.pageable = pageable;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartDate() {
        return startDate+" 00:00";
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate+" 23:59";
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
