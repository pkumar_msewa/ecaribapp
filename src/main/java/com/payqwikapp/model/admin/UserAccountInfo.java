package com.payqwikapp.model.admin;

public class UserAccountInfo {

    private String balance;
    private String accountNumber;
    private String points;
    private boolean isVBankCustomer;
    private String branchCode;
    private String vijayaAccountNo;
    private String transactionLimit;
    private String balanceLimit;
    private String name;
    private String code;
    private String description;
    private String dailyLimit;
    private String monthlyLimit;
    private String linkedAccountMobile;
    private String linkedAccountName;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public boolean isVBankCustomer() {
        return isVBankCustomer;
    }

    public void setVBankCustomer(boolean VBankCustomer) {
        isVBankCustomer = VBankCustomer;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getVijayaAccountNo() {
        return vijayaAccountNo;
    }

    public void setVijayaAccountNo(String vijayaAccountNo) {
        this.vijayaAccountNo = vijayaAccountNo;
    }

    public String getTransactionLimit() {
        return transactionLimit;
    }

    public void setTransactionLimit(String transactionLimit) {
        this.transactionLimit = transactionLimit;
    }

    public String getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(String balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public String getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(String monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public String getLinkedAccountMobile() {
        return linkedAccountMobile;
    }

    public void setLinkedAccountMobile(String linkedAccountMobile) {
        this.linkedAccountMobile = linkedAccountMobile;
    }

    public String getLinkedAccountName() {
        return linkedAccountName;
    }

    public void setLinkedAccountName(String linkedAccountName) {
        this.linkedAccountName = linkedAccountName;
    }
}
