package com.payqwikapp.model.admin;

import com.payqwikapp.model.SessionDTO;

public class MailDTO extends SessionDTO{

    private String subject;
    private String destination;
    private String content;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
