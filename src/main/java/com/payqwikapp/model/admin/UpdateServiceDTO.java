package com.payqwikapp.model.admin;

import com.payqwikapp.model.SessionDTO;

public class UpdateServiceDTO extends SessionDTO {

    private String code;
    private String status;
    private boolean updateCommission;
    private String commissionIdentifier;
    private double value;
    private boolean fixed;
    private String version;
    
    public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isUpdateCommission() {
        return updateCommission;
    }

    public void setUpdateCommission(boolean updateCommission) {
        this.updateCommission = updateCommission;
    }

    public String getCommissionIdentifier() {
        return commissionIdentifier;
    }

    public void setCommissionIdentifier(String commissionIdentifier) {
        this.commissionIdentifier = commissionIdentifier;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }
}
