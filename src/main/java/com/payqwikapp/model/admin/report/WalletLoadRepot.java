package com.payqwikapp.model.admin.report;

import java.util.List;

import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;



public class WalletLoadRepot {

	private List<PQTransaction> ccAvenueLoadMoney;
	private List<PQTransaction> billDeskLoadMoney;
	private List<User> userList;
	
	public List<PQTransaction> getCcAvenueLoadMoney() {
		return ccAvenueLoadMoney;
	}
	public void setCcAvenueLoadMoney(List<PQTransaction> ccAvenueLoadMoney) {
		this.ccAvenueLoadMoney = ccAvenueLoadMoney;
	}
	public List<PQTransaction> getBillDeskLoadMoney() {
		return billDeskLoadMoney;
	}
	public void setBillDeskLoadMoney(List<PQTransaction> billDeskLoadMoney) {
		this.billDeskLoadMoney = billDeskLoadMoney;
	}
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
}
