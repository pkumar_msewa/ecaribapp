package com.payqwikapp.model.admin.report;

import java.util.List;

public class MISReportDTO {

	private List<Object> loadMoney;
	private List<Object> closing;
	private List<Object> created;
	private List<Object> spent;
	private List<Object> wallet;

	public List<Object> getLoadMoney() {
		return loadMoney;
	}
	public void setLoadMoney(List<Object> loadMoney) {
		this.loadMoney = loadMoney;
	}
	public List<Object> getClosing() {
		return closing;
	}
	public void setClosing(List<Object> closing) {
		this.closing = closing;
	}
	public List<Object> getCreated() {
		return created;
	}
	public void setCreated(List<Object> created) {
		this.created = created;
	}
	public List<Object> getSpent() {
		return spent;
	}
	public void setSpent(List<Object> spent) {
		this.spent = spent;
	}
	public List<Object> getWallet() {
		return wallet;
	}
	public void setWallet(List<Object> wallet) {
		this.wallet = wallet;
	}
}
