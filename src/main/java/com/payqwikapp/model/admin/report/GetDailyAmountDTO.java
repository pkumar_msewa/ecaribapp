package com.payqwikapp.model.admin.report;

public class GetDailyAmountDTO {

	private double walletBalance;
	private double totalDailyLoadMoneyAmount;
	private double totalDailyLoadMoneyCount;
	private double totalDailyPayoutAmount;
	private double totalDailyPayoutCount;
	private double poolBalance;
	private double totalBankTranferNow;
	private double totalPromoCode;
	private double totalMerchantRefund;
	private long totalCountBankTranferNow;
	private long totalCountPromoCode;
	private long totalCountMerchantRefund;
	private double totalSendMoneyCredit = 0;
	private double totalSendMoneyDebit = 0;
	private long countSendMoneyCredit = 0;
	private long countSendMoneyDebit = 0;
	
	public double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}

	public double getTotalDailyLoadMoneyAmount() {
		return totalDailyLoadMoneyAmount;
	}

	public void setTotalDailyLoadMoneyAmount(double totalDailyLoadMoneyAmount) {
		this.totalDailyLoadMoneyAmount = totalDailyLoadMoneyAmount;
	}

	public double getTotalDailyLoadMoneyCount() {
		return totalDailyLoadMoneyCount;
	}

	public void setTotalDailyLoadMoneyCount(double totalDailyLoadMoneyCount) {
		this.totalDailyLoadMoneyCount = totalDailyLoadMoneyCount;
	}

	public double getTotalDailyPayoutAmount() {
		return totalDailyPayoutAmount;
	}

	public void setTotalDailyPayoutAmount(double totalDailyPayoutAmount) {
		this.totalDailyPayoutAmount = totalDailyPayoutAmount;
	}

	public double getTotalDailyPayoutCount() {
		return totalDailyPayoutCount;
	}

	public void setTotalDailyPayoutCount(double totalDailyPayoutCount) {
		this.totalDailyPayoutCount = totalDailyPayoutCount;
	}

	public double getPoolBalance() {
		return poolBalance;
	}

	public void setPoolBalance(double poolBalance) {
		this.poolBalance = poolBalance;
	}

	public double getTotalBankTranferNow() {
		return totalBankTranferNow;
	}

	public void setTotalBankTranferNow(double totalBankTranferNow) {
		this.totalBankTranferNow = totalBankTranferNow;
	}

	public double getTotalPromoCode() {
		return totalPromoCode;
	}

	public void setTotalPromoCode(double totalPromoCode) {
		this.totalPromoCode = totalPromoCode;
	}

	public double getTotalMerchantRefund() {
		return totalMerchantRefund;
	}

	public void setTotalMerchantRefund(double totalMerchantRefund) {
		this.totalMerchantRefund = totalMerchantRefund;
	}

	public long getTotalCountBankTranferNow() {
		return totalCountBankTranferNow;
	}

	public void setTotalCountBankTranferNow(long totalCountBankTranferNow) {
		this.totalCountBankTranferNow = totalCountBankTranferNow;
	}

	public long getTotalCountPromoCode() {
		return totalCountPromoCode;
	}

	public void setTotalCountPromoCode(long totalCountPromoCode) {
		this.totalCountPromoCode = totalCountPromoCode;
	}

	public long getTotalCountMerchantRefund() {
		return totalCountMerchantRefund;
	}

	public void setTotalCountMerchantRefund(long totalCountMerchantRefund) {
		this.totalCountMerchantRefund = totalCountMerchantRefund;
	}

	public double getTotalSendMoneyCredit() {
		return totalSendMoneyCredit;
	}

	public void setTotalSendMoneyCredit(double totalSendMoneyCredit) {
		this.totalSendMoneyCredit = totalSendMoneyCredit;
	}

	public double getTotalSendMoneyDebit() {
		return totalSendMoneyDebit;
	}

	public void setTotalSendMoneyDebit(double totalSendMoneyDebit) {
		this.totalSendMoneyDebit = totalSendMoneyDebit;
	}

	public long getCountSendMoneyCredit() {
		return countSendMoneyCredit;
	}

	public void setCountSendMoneyCredit(long countSendMoneyCredit) {
		this.countSendMoneyCredit = countSendMoneyCredit;
	}

	public long getCountSendMoneyDebit() {
		return countSendMoneyDebit;
	}

	public void setCountSendMoneyDebit(long countSendMoneyDebit) {
		this.countSendMoneyDebit = countSendMoneyDebit;
	}
}
