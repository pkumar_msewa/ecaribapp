package com.payqwikapp.model.admin;

import com.payqwikapp.model.Status;

import java.util.List;

public class ServiceListDTO {

    private String name;
    private String code;
    private String description;
    private Status status;
    private String minAmount;
    private String maxAmount;
    private String typeName;
    private String typeDescription;
    private List<CommissionListDTO> list;
    private String configType;
    
    private String mdex;
     
    private String toEmail;

	private String ccEmail;

	private String bccEmail;
	


    public String getConfigType() {
		return configType;
	}

	public void setConfigType(String configType) {
		this.configType = configType;
	}

	public String getMdex() {
		return mdex;
	}

	public void setMdex(String mdex) {
		this.mdex = mdex;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public String getBccEmail() {
		return bccEmail;
	}

	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public String getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(String maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public List<CommissionListDTO> getList() {
        return list;
    }

    public void setList(List<CommissionListDTO> list) {
        this.list = list;
    }
}
