package com.payqwikapp.model;

public class KycLimitDTO {

    private String sessionId;

    private String accountType;

    private double monthlyLimit;

    private double balanceLimit;

    private double dailyLimit;

    private int transactionLimit;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMontlyLimit(double montlyLimit) {
        this.monthlyLimit = montlyLimit;
    }

    public double getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(double balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public double getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(double dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public int getTransactionLimit() {
        return transactionLimit;
    }

    public void setTransactionLimit(int transactionLimit) {
        this.transactionLimit = transactionLimit;
    }
}
