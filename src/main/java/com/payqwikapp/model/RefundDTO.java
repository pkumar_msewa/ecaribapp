package com.payqwikapp.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.request.JSONWrapper;

public class RefundDTO extends SessionDTO implements JSONWrapper{

    private String username;
    private double netAmount;
    private String transactionRefNo;
    private String orderId;
    private String refundAmount;
    private String created;
    private String status;
    
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }


    @Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try{
            json.put("netAmount",getNetAmount());
            json.put("transactionRefNo",getTransactionRefNo());
            json.put("username",getUsername());
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        return json;
    }

}
