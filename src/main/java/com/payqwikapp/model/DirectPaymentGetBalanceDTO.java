package com.payqwikapp.model;

public class DirectPaymentGetBalanceDTO {

	public String userName;
	public String consumerTokenKey;
	public String consumerSecretKey;
	public double amount;
	public String transactionRefNo;
	public String tokenKey;
	public String otp;
	public String status;
	public String retrivalReferenceNo;

	public String getConsumerTokenKey() {
		return consumerTokenKey;
	}

	public void setConsumerTokenKey(String consumerTokenKey) {
		this.consumerTokenKey = consumerTokenKey;
	}

	public String getConsumerSecretKey() {
		return consumerSecretKey;
	}

	public void setConsumerSecretKey(String consumerSecretKey) {
		this.consumerSecretKey = consumerSecretKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRetrivalReferenceNo() {
		return retrivalReferenceNo;
	}

	public void setRetrivalReferenceNo(String retrivalReferenceNo) {
		this.retrivalReferenceNo = retrivalReferenceNo;
	}

}
