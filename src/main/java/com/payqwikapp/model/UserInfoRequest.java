package com.payqwikapp.model;

public class UserInfoRequest extends SessionDTO{

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
