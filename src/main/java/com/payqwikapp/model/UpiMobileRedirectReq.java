package com.payqwikapp.model;

public class UpiMobileRedirectReq {

	private String txnRefNo;
	private String refId;
	private String upiId;
	private String status;
	private String desc;
	private String txnsRespCode;

	public String getTxnsRespCode() {
		return txnsRespCode;
	}

	public void setTxnsRespCode(String txnsRespCode) {
		this.txnsRespCode = txnsRespCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTxnRefNo() {
		return txnRefNo;
	}

	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getUpiId() {
		return upiId;
	}

	public void setUpiId(String upiId) {
		this.upiId = upiId;
	}
}
