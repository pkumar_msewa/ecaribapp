package com.payqwikapp.model;

public class CallBackDTO extends SessionDTO{
    private String transactionRefNo;
    private boolean success;

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "CallBackDTO{" +
                "transactionRefNo='" + transactionRefNo + '\'' +
                ", success=" + success +
                '}';
    }
}
