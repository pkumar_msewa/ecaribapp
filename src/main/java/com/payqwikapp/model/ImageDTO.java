package com.payqwikapp.model;

public class ImageDTO extends SessionDTO{

    private String encodedBytes;
    private String contentType;

    public String getEncodedBytes() {
        return encodedBytes;
    }

    public void setEncodedBytes(String encodedBytes) {
        this.encodedBytes = encodedBytes;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
