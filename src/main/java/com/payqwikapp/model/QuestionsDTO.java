package com.payqwikapp.model;

import java.util.List;

import com.payqwikapp.model.mobile.ResponseStatus;

public class QuestionsDTO {

	private String status;
	private String code;
	private String message;
	private List<SecurityQuestionDTO> questions;

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		code = status.getValue();
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SecurityQuestionDTO> getQuestions() {
		return questions;
	}

	public void setQuestions(List<SecurityQuestionDTO> questions) {
		this.questions = questions;
	}


}
