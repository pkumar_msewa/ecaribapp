package com.payqwikapp.model;

public class RefernEarnRequest {

	private String sessionId;
	private int count;
	private boolean hasEarned;
	private double amount;
	private String code;
	private String message;
	private String mobileNumber;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public boolean isHasEarned() {
		return hasEarned;
	}
	public void setHasEarned(boolean hasEarned) {
		this.hasEarned = hasEarned;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
