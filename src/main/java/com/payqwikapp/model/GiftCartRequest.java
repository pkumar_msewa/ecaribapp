package com.payqwikapp.model;

import com.payqwikapp.entity.User;

public class GiftCartRequest {

	private String productType;
	private String quantity;
	private String amount;
	private String brandName;
	private String imagepath;
	private String brandHash;

	private String skuId;
	private String productId;
	private String sessioniId;

	private User userid;

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getBrandHash() {
		return brandHash;
	}

	public void setBrandHash(String brandHash) {
		this.brandHash = brandHash;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getSessioniId() {
		return sessioniId;
	}

	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}

	public User getUserid() {
		return userid;
	}

	public void setUserid(User userid) {
		this.userid = userid;
	}


	@Override
	public String toString() {
		return "GiftCartRequest{" +
				"productType='" + productType + '\'' +
				", quantity='" + quantity + '\'' +
				", amount='" + amount + '\'' +
				", brandName='" + brandName + '\'' +
				", imagepath='" + imagepath + '\'' +
				", brandHash='" + brandHash + '\'' +
				", skuId='" + skuId + '\'' +
				", productId='" + productId + '\'' +
				", sessioniId='" + sessioniId + '\'' +
				", userid=" + userid +
				'}';
	}
}
