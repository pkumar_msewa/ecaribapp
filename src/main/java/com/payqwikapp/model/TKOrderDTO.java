package com.payqwikapp.model;

import com.payqwikapp.model.mobile.ResponseStatus;

import ch.qos.logback.core.status.Status;

public class TKOrderDTO {
	private boolean success;
	private String code;
	private String message;
	private ResponseStatus status;
	private String Response;
	private long userOrderId;
	private double balance;
	private String txnId;
	private String trainNumber;
	private String trainName;
	
	public String getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getTrainName() {
		return trainName;
	}
	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus failure) {
		this.status = failure;
	}
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	public long getUserOrderId() {
		return userOrderId;
	}
	public void setUserOrderId(long userOrderId) {
		this.userOrderId = userOrderId;
	}
	
	
}
