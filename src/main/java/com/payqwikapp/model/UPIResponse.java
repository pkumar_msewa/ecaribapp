package com.payqwikapp.model;

public class UPIResponse {
	
	private String referenceId;
	private String respCode;
	private String consumerId;
	private String orgRefId;

	public String getOrgRefId() {
		return orgRefId;
	}

	public void setOrgRefId(String orgRefId) {
		this.orgRefId = orgRefId;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
}
