package com.payqwikapp.model;

public class UserAnalytics {

	@Override
	public String toString() {
		return "UserAnalytics [username=" + username + ", created=" + created + ", transactionRefNo=" + transactionRefNo
				+ ", amount=" + amount + ", status=" + status + ", description=" + description + "]";
	}
	private String username;
	private String created;
	private String transactionRefNo;
	private String amount;
	private String status;
	private String description;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
