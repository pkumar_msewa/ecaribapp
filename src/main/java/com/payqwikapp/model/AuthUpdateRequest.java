package com.payqwikapp.model;

public class AuthUpdateRequest {

    private String sessionId;
    private String username;
    private String requestType;
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Override
    public String toString() {
        return "AuthUpdateRequest{" +
                "username='" + username + '\'' +
                ", requestType='" + requestType + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
