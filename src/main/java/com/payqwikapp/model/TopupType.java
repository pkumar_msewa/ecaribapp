package com.payqwikapp.model;

public enum TopupType {
	Prepaid, Postpaid, DataCard,Dth,Landline,Electricity,Gas,Insurance,Water
}
