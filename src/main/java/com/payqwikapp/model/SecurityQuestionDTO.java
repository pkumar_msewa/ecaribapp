package com.payqwikapp.model;

public class SecurityQuestionDTO {

	private String question;
	private String code;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "SecurityQuestionDTO{" +
				"question='" + question + '\'' +
				", code='" + code + '\'' +
				'}';
	}
}
