package com.payqwikapp.model;

import java.util.List;

public class MerchantReportDTO {
	
	private String sessionId;
    private String contentType;
    private String fileName;
    private List<MerchantReportDetails> mobileDTO;
    
	public List<MerchantReportDetails> getMobileDTO() {
		return mobileDTO;
	}

	public void setMobileDTO(List<MerchantReportDetails> mobileDTO) {
		this.mobileDTO = mobileDTO;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
