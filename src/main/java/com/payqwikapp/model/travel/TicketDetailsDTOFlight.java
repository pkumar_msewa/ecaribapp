package com.payqwikapp.model.travel;

import java.util.List;

import com.payqwikapp.entity.FlightTravellers;

public class TicketDetailsDTOFlight 
{
	private List<FlightResponseEmail> flightresponse;
	private List<FlightResponseEmail> flightresponsearrreturn;
	private List<FlightTicketdtoForname> nameandticket;
	private String bookingRefId;
	private double paymentAmount;
	private String paymentmethod;
	List<FlightTravellers> travellerDetails;
	private String pnrNo;
	private String originName;
	private String destinationName;
	
	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public List<FlightResponseEmail> getFlightresponse() {
		return flightresponse;
	}

	public void setFlightresponse(List<FlightResponseEmail> flightresponse) {
		this.flightresponse = flightresponse;
	}

	public List<FlightResponseEmail> getFlightresponsearrreturn() {
		return flightresponsearrreturn;
	}

	public void setFlightresponsearrreturn(List<FlightResponseEmail> flightresponsearrreturn) {
		this.flightresponsearrreturn = flightresponsearrreturn;
	}

	public String getBookingRefId() {
		return bookingRefId;
	}

	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentmethod() {
		return paymentmethod;
	}

	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}
	public List<FlightTicketdtoForname> getNameandticket() {
		return nameandticket;
	}

	public void setNameandticket(List<FlightTicketdtoForname> nameandticket) {
		this.nameandticket = nameandticket;
	}
	public List<FlightTravellers> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<FlightTravellers> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public String getPnrNo() {
		return pnrNo;
	}
	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}
 

}
