package com.payqwikapp.model.travel.flight;

import com.payqwikapp.model.SessionDTO;

public class CompareCountry extends SessionDTO {

	private String org;
	private String dest;
	
	public String getOrg() {
		return org;
	}
	public void setOrg(String org) {
		this.org = org;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	
}
