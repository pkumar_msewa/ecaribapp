package com.payqwikapp.model.travel.flight;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
	public class TicketsResp {

		@JsonProperty("Tickets")
		private Tickets tickets;

		public Tickets getTickets() {
			return tickets;
		}
		public void setTickets(Tickets tickets) {
			this.tickets = tickets;
		}

}