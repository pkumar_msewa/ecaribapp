package com.payqwikapp.model.travel.flight;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown=true)
public class FlightTicketDeatilsDTO {

	@JsonProperty("departureTime")
	private String departureTime;
	@JsonProperty("journeyTime")
	private String journeyTime;
	@JsonProperty("baggageWeight")
	private String baggageWeight;
	@JsonProperty("origin")
	private String origin;
	@JsonProperty("destination")
	private String destination;
	@JsonProperty("cabin")
	private String cabin;
	@JsonProperty("arrivalDate")
	private String arrivalDate;
	@JsonProperty("flightNumber")
	private String flightNumber;
	@JsonProperty("duration")
	private String duration;
	@JsonProperty("arrivalTime")
	private String arrivalTime;
	@JsonProperty("departureDate")
	private String departureDate;
	@JsonProperty("baggageUnit")
	private String baggageUnit;
	@JsonProperty("airlineName")
	private String airlineName;
	
	@JsonProperty("destinationName")
	private String destinationName;
	
	@JsonProperty("sourceName")
	private String sourceName;

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getJourneyTime() {
		return journeyTime;
	}

	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}

	public String getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getBaggageUnit() {
		return baggageUnit;
	}

	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	
}
