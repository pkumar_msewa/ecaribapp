package com.payqwikapp.model.travel.flight;

import java.util.List;

import com.payqwikapp.entity.AgentFlightTicket;
import com.payqwikapp.entity.AgentFlightTravellers;

public class AgentFlghtTicketResp {
private List<AgentFlightTravellers> travellerDetails;
	
	private AgentFlightTicket flightTicket;

	public List<AgentFlightTravellers> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<AgentFlightTravellers> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public AgentFlightTicket getFlightTicket() {
		return flightTicket;
	}

	public void setFlightTicket(AgentFlightTicket flightTicket) {
		this.flightTicket = flightTicket;
	}
	
}
