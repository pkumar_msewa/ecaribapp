package com.payqwikapp.model.travel.flight;

import java.util.List;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.FlightTicket;
import com.payqwikapp.entity.FlightTravellers;
import com.payqwikapp.entity.TravellerDetails;

public class FlghtTicketResp {

	private List<FlightTravellers> travellerDetails;
	
	private FlightTicket flightTicket;

	public List<FlightTravellers> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<FlightTravellers> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public FlightTicket getFlightTicket() {
		return flightTicket;
	}

	public void setFlightTicket(FlightTicket flightTicket) {
		this.flightTicket = flightTicket;
	}
	
}
