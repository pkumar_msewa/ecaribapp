package com.payqwikapp.model.travel.bus;

import java.util.List;

import com.payqwikapp.entity.BusCityList;

public class BusCityListResponse {

	private String status;
	private String code;
	private String message;
	private boolean isValid;
	private Object details;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	
}
