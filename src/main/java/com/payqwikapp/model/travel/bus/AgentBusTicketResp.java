package com.payqwikapp.model.travel.bus;

import java.util.List;

import com.payqwikapp.entity.AgentBusTicket;
import com.payqwikapp.entity.AgentTravellerDetails;

public class AgentBusTicketResp {
private List<AgentTravellerDetails> travellerDetails;
	
	private AgentBusTicket busTicket;

	public List<AgentTravellerDetails> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<AgentTravellerDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public AgentBusTicket getBusTicket() {
		return busTicket;
	}

	public void setBusTicket(AgentBusTicket busTicket) {
		this.busTicket = busTicket;
	}
	
}
