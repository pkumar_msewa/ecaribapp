package com.payqwikapp.model.travel.bus;

import com.payqwikapp.model.SessionDTO;

public class BusPaymentReq extends SessionDTO  {

	private String userMobile;
	private String ticketPnr;
	private String operatorPnr;
	private String bookingTxnId;
	private String busId;
	private String transactionId;
	private String pQTxnId;
	private double totalFare;
	private boolean success;
	private double refundedAmount;
	private String mdexBookingResp;
	private double priceRechecktotalFare;
	private String seatHoldId; 
	
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	public String getTicketPnr() {
		return ticketPnr;
	}
	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}
	public String getOperatorPnr() {
		return operatorPnr;
	}
	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}
	public String getBookingTxnId() {
		return bookingTxnId;
	}
	public void setBookingTxnId(String bookingTxnId) {
		this.bookingTxnId = bookingTxnId;
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getpQTxnId() {
		return pQTxnId;
	}
	public void setpQTxnId(String pQTxnId) {
		this.pQTxnId = pQTxnId;
	}
	public double getRefundedAmount() {
		return refundedAmount;
	}
	public void setRefundedAmount(double refundedAmount) {
		this.refundedAmount = refundedAmount;
	}
	public String getMdexBookingResp() {
		return mdexBookingResp;
	}
	public void setMdexBookingResp(String mdexBookingResp) {
		this.mdexBookingResp = mdexBookingResp;
	}
	public double getPriceRechecktotalFare() {
		return priceRechecktotalFare;
	}
	public void setPriceRechecktotalFare(double priceRechecktotalFare) {
		this.priceRechecktotalFare = priceRechecktotalFare;
	}
	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
	
}
