package com.payqwikapp.model.travel.bus;

import java.util.List;

import com.payqwikapp.entity.BusTicket;
import com.payqwikapp.entity.TravellerDetails;

public class BusTicketResp {

	private List<TravellerDetails> travellerDetails;
	
	private BusTicket busTicket;

	public List<TravellerDetails> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<TravellerDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public BusTicket getBusTicket() {
		return busTicket;
	}

	public void setBusTicket(BusTicket busTicket) {
		this.busTicket = busTicket;
	}
	
	
}
