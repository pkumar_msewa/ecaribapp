package com.payqwikapp.model.travel.bus;

import java.util.List;

import com.payqwikapp.model.SessionDTO;

public class BusPaymentInit extends SessionDTO  {

	private String userMobile;
	private String emtTxnId;
	private String busId;
	private double totalFare;
	private boolean success;
	private String userEmail;
	private String busType;
	private String boardId;
	private String boardName;
	private String journeyDate;
	private String source;
	private String destination;
	private String boardingId;
	private String boardingAddress;
	private String travelName;
	private String arrTime;
	private String deptTime;
	private String emtTransactionScreenId;
	private String getMdexTxnIdResp;
	private String boardTime;
	private double priceRecheckAmt;
	private String seatHoldId; 
	private String error;
	private String tripId;
	private String seatDetailsId;
	
	private List<TravellerDetailsDTO> travellers;
	
	
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public List<TravellerDetailsDTO> getTravellers() {
		return travellers;
	}
	public void setTravellers(List<TravellerDetailsDTO> travellers) {
		this.travellers = travellers;
	}
	public String getEmtTxnId() {
		return emtTxnId;
	}
	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getBoardName() {
		return boardName;
	}
	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getBoardingId() {
		return boardingId;
	}
	public void setBoardingId(String boardingId) {
		this.boardingId = boardingId;
	}
	public String getBoardingAddress() {
		return boardingAddress;
	}
	public void setBoardingAddress(String boardingAddress) {
		this.boardingAddress = boardingAddress;
	}
	public String getTravelName() {
		return travelName;
	}
	public void setTravelName(String travelName) {
		this.travelName = travelName;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public String getDeptTime() {
		return deptTime;
	}
	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}
	public String getEmtTransactionScreenId() {
		return emtTransactionScreenId;
	}
	public void setEmtTransactionScreenId(String emtTransactionScreenId) {
		this.emtTransactionScreenId = emtTransactionScreenId;
	}
	public String getGetMdexTxnIdResp() {
		return getMdexTxnIdResp;
	}
	public void setGetMdexTxnIdResp(String getMdexTxnIdResp) {
		this.getMdexTxnIdResp = getMdexTxnIdResp;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	public double getPriceRecheckAmt() {
		return priceRecheckAmt;
	}
	public void setPriceRecheckAmt(double priceRecheckAmt) {
		this.priceRecheckAmt = priceRecheckAmt;
	}
	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getSeatDetailsId() {
		return seatDetailsId;
	}
	public void setSeatDetailsId(String seatDetailsId) {
		this.seatDetailsId = seatDetailsId;
	}
	
}
