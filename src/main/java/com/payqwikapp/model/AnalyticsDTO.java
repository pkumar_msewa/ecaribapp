package com.payqwikapp.model;

import com.payqwikapp.entity.PQService;

public class AnalyticsDTO extends SessionDTO{
	
	
	@Override
	public String toString() {
		return "AnalyticsDTO [count=" + count + ", service=" + service + ", amount=" + amount + ", serviceName="
				+ serviceName + ", to=" + to + ", from=" + from + "]";
	}
	private long count;
	private PQService service;
	private double amount;
	private String serviceName;
	private String to;
	private String from;
	
	
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	
	public PQService getService() {
		return service;
	}
	public void setService(PQService service) {
		this.service = service;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	

}
