package com.payqwikapp.model;

public class SendMoneyDonateeDTO {

	private String sessionId;
	private String donatee;
	private String amount;
	private String message;
	private String transactionrefno;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTransactionrefno() {
		return transactionrefno;
	}
	public void setTransactionrefno(String transactionrefno) {
		this.transactionrefno = transactionrefno;
	}
	public String getDonatee() {
		return donatee;
	}
	public void setDonatee(String donatee) {
		this.donatee = donatee;
	}
}
