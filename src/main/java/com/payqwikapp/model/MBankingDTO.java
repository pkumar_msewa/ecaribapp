package com.payqwikapp.model;

public class MBankingDTO {

    private String sessionId;
    private String transactionID;
    private double amount;
    private long accountNumber;
    private long username;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getUsername() {
        return username;
    }

    public void setUsername(long username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "MBankingDTO{" +
                "transactionID='" + transactionID + '\'' +
                ", amount=" + amount +
                ", accountNumber=" + accountNumber +
                ", username=" + username +
                '}';
    }
}
