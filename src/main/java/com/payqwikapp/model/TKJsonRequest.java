package com.payqwikapp.model;

import org.codehaus.jettison.json.JSONException;

public interface TKJsonRequest {
	public String getJsonRequest() throws JSONException, org.json.JSONException;
}
