package com.payqwikapp.model.error;

public class KycLimitError {

    private boolean valid;

    private String accountType;

    private String montlyLimit;

    private String balanceLimit;

    private String dailyLimit;

    private String transactionLimit;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getMontlyLimit() {
        return montlyLimit;
    }

    public void setMontlyLimit(String montlyLimit) {
        this.montlyLimit = montlyLimit;
    }

    public String getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(String balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public String getTransactionLimit() {
        return transactionLimit;
    }

    public void setTransactionLimit(String transactionLimit) {
        this.transactionLimit = transactionLimit;
    }
}
