package com.payqwikapp.model.error;

public class TransactionError {

	private boolean valid;
	private String message;
	private String code;
	private double splitAmount;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public double getSplitAmount() {
		return splitAmount;
	}

	public void setSplitAmount(double splitAmount) {
		this.splitAmount = splitAmount;
	}

	@Override
	public String toString() {
		return "TransactionError [valid=" + valid + ", message=" + message + ", code=" + code + ", splitAmount="
				+ splitAmount + "]";
	}

	public boolean isSuccess() {
		return true;
	}

}
