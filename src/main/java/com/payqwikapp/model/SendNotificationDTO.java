package com.payqwikapp.model;

public class SendNotificationDTO {
	
	    private String title;
	    private String message;
	    private String image;
	    private boolean imageGCM;
	    private String sessionId;
	    private String status;
	    private Long deliveredCount;
	    private String createdDate;

	    public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}

		public Long getDeliveredCount() {
			return deliveredCount;
		}

		public void setDeliveredCount(Long deliveredCount) {
			this.deliveredCount = deliveredCount;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public boolean isImageGCM() {
	        return imageGCM;
	    }

	    public void setImageGCM(boolean imageGCM) {
	        this.imageGCM = imageGCM;
	    }

	    public String getTitle() {
	        return title;
	    }

	    public void setTitle(String title) {
	        this.title = title;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

	    public String getImage() {
	        return image;
	    }

	    public void setImage(String image) {
	        this.image = image;
	    }


}
