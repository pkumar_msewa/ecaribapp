package com.payqwikapp.model;

public class LocationDTO {

    private String pinCode;
    private String districtName;
    private String circleName;
    private String locality;
    private String stateName;
    private String regionName;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }


    @Override
    public String toString() {
        return "LocationDTO{" +
                "pinCode='" + pinCode + '\'' +
                ", districtName='" + districtName + '\'' +
                ", circleName='" + circleName + '\'' +
                ", locality='" + locality + '\'' +
                ", stateName='" + stateName + '\'' +
                ", regionName='" + regionName + '\'' +
                '}';
    }
}
