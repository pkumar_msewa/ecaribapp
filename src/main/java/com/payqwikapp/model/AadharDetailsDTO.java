package com.payqwikapp.model;

public class AadharDetailsDTO {
	
	private String aadharName;

	private String aadharAddress;

	private String aadharNumber;

	private String mobileNumber;
	
	private String dateOfUpdate;

	public String getDateOfUpdate() {
		return dateOfUpdate;
	}

	public void setDateOfUpdate(String dateOfUpdate) {
		this.dateOfUpdate = dateOfUpdate;
	}

	public String getAadharName() {
		return aadharName;
	}

	public void setAadharName(String aadharName) {
		this.aadharName = aadharName;
	}

	public String getAadharAddress() {
		return aadharAddress;
	}

	public void setAadharAddress(String aadharAddress) {
		this.aadharAddress = aadharAddress;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


}
