package com.payqwikapp.model;

import com.thirdparty.model.request.JSONWrapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class ImagicaLoginRequest implements JSONWrapper{

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("username",getUsername());
            payload.put("password",getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }
}
