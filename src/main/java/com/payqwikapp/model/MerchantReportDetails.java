package com.payqwikapp.model;

import java.util.Date;

public class MerchantReportDetails {

	private String modeOfPayment;
	private String consumerType;
	private String paymentChannel;
	private String typeOfPayment;
	private String transactionType;
	private String subDivisonCode;
	private String accountId;
	private String transactionRefId;
	private String pgId;
	private String vendorRefId;
	private double billAmount;
	private double charges;
	private double netAmount;
	private String transactionDate;
	private String postedDate;
	private String nameOfConsumer;
	private String contactNumber;
	private String emailId;
	private String vendorRrn;
	private Date   created;
	private String status;
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getConsumerType() {
		return consumerType;
	}

	public void setConsumerType(String consumerType) {
		this.consumerType = consumerType;
	}

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	public String getTypeOfPayment() {
		return typeOfPayment;
	}

	public void setTypeOfPayment(String typeOfPayment) {
		this.typeOfPayment = typeOfPayment;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getSubDivisonCode() {
		return subDivisonCode;
	}

	public void setSubDivisonCode(String subDivisonCode) {
		this.subDivisonCode = subDivisonCode;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getTransactionRefId() {
		return transactionRefId;
	}

	public void setTransactionRefId(String transactionRefId) {
		this.transactionRefId = transactionRefId;
	}

	public String getPgId() {
		return pgId;
	}

	public void setPgId(String pgId) {
		this.pgId = pgId;
	}

	public String getVendorRefId() {
		return vendorRefId;
	}

	public void setVendorRefId(String vendorRefId) {
		this.vendorRefId = vendorRefId;
	}

	public double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public double getCharges() {
		return charges;
	}

	public void setCharges(double charges) {
		this.charges = charges;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public String getNameOfConsumer() {
		return nameOfConsumer;
	}

	public void setNameOfConsumer(String nameOfConsumer) {
		this.nameOfConsumer = nameOfConsumer;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getVendorRrn() {
		return vendorRrn;
	}

	public void setVendorRrn(String vendorRrn) {
		this.vendorRrn = vendorRrn;
	}

}
