package com.payqwikapp.model;

import com.payqwikapp.model.mobile.ResponseStatus;

public class OrderPlaceResponse {

	private String message;
	private String code;
	private String order_status;
	private String orderNumber;
	private boolean success;
	private String transactionRefNo;
	private String cardName;
	private String expiry_date;
	private String card_price;
	private String cardnumber;
	private String pin_or_url;
	private String status;
	private String retrivalRefNo;
	private String sessionId;
	private String termsAndConditions;
	private String responseCode;
	private String email;
	private String name;
	private double remainingBalance;
	
	public double getRemainingBalance() {
		return remainingBalance;
	}

	public void setRemainingBalance(double remainingBalance) {
		this.remainingBalance = remainingBalance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}

	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}

	public String getCard_price() {
		return card_price;
	}

	public void setCard_price(String card_price) {
		this.card_price = card_price;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public String getPin_or_url() {
		return pin_or_url;
	}

	public void setPin_or_url(String pin_or_url) {
		this.pin_or_url = pin_or_url;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		return "OrderPlaceResponse [message=" + message + ", code=" + code + ", order_status=" + order_status
				+ ", orderNumber=" + orderNumber + ", success=" + success + ", transactionRefNo=" + transactionRefNo
				+ ", cardName=" + cardName + ", expiry_date=" + expiry_date + ", card_price=" + card_price
				+ ", cardnumber=" + cardnumber + ", pin_or_url=" + pin_or_url + ", status=" + status
				+ ", retrivalRefNo=" + retrivalRefNo + ", sessionId=" + sessionId + ", termsAndConditions="
				+ termsAndConditions + ", responseCode=" + responseCode + ", email=" + email + ", name=" + name + "]";
	}
	
	
}
