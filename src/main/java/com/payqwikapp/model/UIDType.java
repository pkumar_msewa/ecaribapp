package com.payqwikapp.model;

/**
 * Created by vibhanshu on 25/2/17.
 */
public enum UIDType {
    AADHAAR("Aadhaar");
    private String name;

    private UIDType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static UIDType getEnum(String name) {
        if (name == null)
            throw new IllegalArgumentException();
        for (UIDType v : values())
            if (name.equalsIgnoreCase(v.getName()))
                return v;
        throw new IllegalArgumentException();
    }
}
