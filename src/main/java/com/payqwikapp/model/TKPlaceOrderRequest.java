package com.payqwikapp.model;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.codehaus.jettison.json.JSONException;

public class TKPlaceOrderRequest implements TKJsonRequest{
//public class TKPlaceOrderRequest{
	private String sessionId;
	
//	private  String items;
	private ArrayList<TKItemsRequest> items;
	//userOrderInfo
	private String cod;
	private String order_outlet_id;
	private String date;
	private String pnr;
	private String station_code;
	private String coach;
	private String seat;
	private String customer_comment;
	private double totalCustomerPayable;
	
	//userBasicInfo
	private String name;
	private String contact_no;
	private String mail_id;
	
	//trainUpdateInfo
	private String train_number;
	private String eta;
	
	
	public ArrayList<TKItemsRequest> getItems() {
		return items;
	}
	public void setItems(ArrayList<TKItemsRequest> items) {
		this.items = items;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/*public String getItems() {
		return items;
	}
	public void setItems(String items) {
		this.items = items;
	}*/
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getOrder_outlet_id() {
		return order_outlet_id;
	}
	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getStation_code() {
		return station_code;
	}
	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getCustomer_comment() {
		return customer_comment;
	}
	public void setCustomer_comment(String customer_comment) {
		this.customer_comment = customer_comment;
	}
	public double getTotalCustomerPayable() {
		return totalCustomerPayable;
	}
	public void setTotalCustomerPayable(double totalCustomerPayable) {
		this.totalCustomerPayable = totalCustomerPayable;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getMail_id() {
		return mail_id;
	}
	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}
	public String getTrain_number() {
		return train_number;
	}
	public void setTrain_number(String train_number) {
		this.train_number = train_number;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	/*
	 @Override
		public String getJsonRequest() throws JSONException {
		   	JSONObject jobj = new JSONObject();
		   	jobj.put("sessionId",getSessionId());
		   	JSONObject jobj2 = new JSONObject();
		   	if(items!=null){
		   	for(int i=0;i<getItems().size();i++)
		   	{
		   		jobj2.put("itemId",getItems().get(i).getItemId());
		   		jobj2.put("quantity",getItems().get(i).getQuantity());
		   	}
		   	jobj.put("items",jobj2);
		   	jobj.put("cod",getCod());jobj.put("order_outlet_id",getOrder_outlet_id());
		   	jobj.put("pnr",getPnr());jobj.put("coach",getCoach());
		   	jobj.put("seat",getSeat());jobj.put("customer_comment",getCustomer_comment());
		   	jobj.put("totalCustomerPayable",getTotalCustomerPayable());jobj.put("name",getName());
		   	jobj.put("contact_no",getContact_no());jobj.put("mail_id",getMail_id());
		   	jobj.put("train_number",getTrain_number());jobj.put("station_code",getStation_code());
		   	jobj.put("date",getDate());jobj.put("eta",getEta());
		   	}
		   	return jobj.toString();
		   	
	   }
	*/
	
	@Override
	public String getJsonRequest() throws JSONException, org.json.JSONException {
	 JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
		JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
		jsonBuilder.add("sessionId",getSessionId());
		for(int i=0;i<getItems().size();i++)
		{
			orderMenuObj.add("itemId",getItems().get(i).getItemId());
			orderMenuObj.add("quantity",getItems().get(i).getQuantity());
			orderMenu.add(orderMenuObj.build());
		}
		jsonBuilder.add("items",orderMenu);
		
		jsonBuilder.add("station_code",getStation_code());jsonBuilder.add("cod",getCod());
		jsonBuilder.add("order_outlet_id",getOrder_outlet_id());
		jsonBuilder.add("date",getDate());jsonBuilder.add("pnr",getPnr());
		jsonBuilder.add("coach",getCoach());
		jsonBuilder.add("seat",getSeat());jsonBuilder.add("customer_comment",getCustomer_comment());
		jsonBuilder.add("totalCustomerPayable",getTotalCustomerPayable());
		
		jsonBuilder.add("name",getName());
		jsonBuilder.add("contact_no",getContact_no());
		jsonBuilder.add("mail_id",getMail_id());

		jsonBuilder.add("train_number",getTrain_number());
		jsonBuilder.add("station_code",getStation_code());
		jsonBuilder.add("eta",getEta());
		
		 JsonObject empObj = jsonBuilder.build();
	     StringWriter jsnReqStr = new StringWriter();
	     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
	     jsonWtr.writeObject(empObj);
	     jsonWtr.close();
	     System.out.println(jsnReqStr.toString());
			
	   return jsnReqStr.toString();	
	   	
   }
}
