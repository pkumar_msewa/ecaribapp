package com.payqwikapp.model;

import com.thirdparty.model.request.*;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class URegisterDTO implements com.thirdparty.model.request.JSONWrapper{
    private String contactNo;
    private String firstName;
    private String lastName;
    private String password;
    private String locationCode;
    private String dateOfBirth;
    private String email;
    private String mpin;
    private Gender gender;


    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMpin() {
        return mpin;
    }

    public void setMpin(String mpin) {
        this.mpin = mpin;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("contactNo",getContactNo());
            json.put("gender",getGender());
            json.put("email",getEmail());
            json.put("firstName",getFirstName());
            json.put("lastName",getLastName());
            json.put("password",getPassword());
            json.put("locationCode",getLocationCode());
            json.put("dateOfBirth",getDateOfBirth());
            json.put("mpin",getMpin());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }
}
