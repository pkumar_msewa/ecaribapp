package com.payqwikapp.model;

public class EResponseDTO {

    private String encryptedText;

    public String getEncryptedText() {
        return encryptedText;
    }

    public void setEncryptedText(String encryptedText) {
        this.encryptedText = encryptedText;
    }

    @Override
    public String toString() {
        return "EResponseDTO{" +
                "encryptedText='" + encryptedText + '\'' +
                '}';
    }
}
