package com.payqwikapp.model;

import org.json.JSONException;
import org.json.JSONObject;

public class EventDetailsDTO extends SessionDTO {

	private String accessToken;
	private String orderId;
	private int eventSignUpId;
	private String city;
	private String state;
	private String country;
	private String category;
	private String eventName;
	private String eventType;
	private int eventId;
	private String eventVanue;
	private String bookingDate;
	private String startDate;
	private String endDate;
	private int noOfAttendees;
	private double netAmount;
	private int ticketId;
	private String ticketName;
	private String ticketType;
	private String price;
	private int quantity;
	private String status;
	private String description;
	private String serviceCode;
	private String sessionId;
	private String transactionRefNo;

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getEventSignUpId() {
		return eventSignUpId;
	}

	public void setEventSignUpId(int eventSignUpId) {
		this.eventSignUpId = eventSignUpId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventVanue() {
		return eventVanue;
	}

	public void setEventVanue(String eventVanue) {
		this.eventVanue = eventVanue;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getNoOfAttendees() {
		return noOfAttendees;
	}

	public void setNoOfAttendees(int noOfAttendees) {
		this.noOfAttendees = noOfAttendees;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("serviceProvider", serviceCode);
			json.put("amount", netAmount);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	@Override
	public String toString() {
		return "EventDetailsDTO{" + "accessToken='" + accessToken + '\'' + ", orderId='" + orderId + '\''
				+ ", eventSignUpId=" + eventSignUpId + ", city='" + city + '\'' + ", state='" + state + '\''
				+ ", country='" + country + '\'' + ", category='" + category + '\'' + ", eventName='" + eventName + '\''
				+ ", eventType='" + eventType + '\'' + ", eventId=" + eventId + ", eventVanue='" + eventVanue + '\''
				+ ", bookingDate='" + bookingDate + '\'' + ", startDate='" + startDate + '\'' + ", endDate='" + endDate
				+ '\'' + ", noOfAttendees=" + noOfAttendees + ", netAmount=" + netAmount + ", ticketId=" + ticketId
				+ ", ticketName='" + ticketName + '\'' + ", ticketType='" + ticketType + '\'' + ", price='" + price
				+ '\'' + ", quantity=" + quantity + ", status='" + status + '\'' + ", description='" + description
				+ '\'' + ", serviceCode='" + serviceCode + '\'' + '}';
	}
}
