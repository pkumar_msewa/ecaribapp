package com.payqwikapp.model;

public class PasswordHistoryDTO {
	
	private String message;
	private String result;
	private String status;
	private String newMpin;
	private String username;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNewMpin() {
		return newMpin;
	}
	public void setNewMpin(String newMpin) {
		this.newMpin = newMpin;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
