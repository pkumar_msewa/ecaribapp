package com.payqwikapp.model;

public class RedeemDTO {

	private String sessionId;
	private String promoCode;
	private String points;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	@Override
	public String toString() {
		return "RedeemDTO{" +
				"promoCode='" + promoCode + '\'' +
				'}';
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points){
		this.points=points;
	}
}