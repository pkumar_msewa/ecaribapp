package com.payqwikapp.model;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public class AgentSendMoneyBankDTO {
	private String sessionId;
	private String bankCode;
	private String ifscCode;
	private String username;
	private long accountNumber;
	private String amount;
	private String bankName;
	
    private String senderName;
	private String senderEmailId;
	private String senderMobileNo;
	private String description;
	private String idProofNo;
	/*private MultipartFile idProofImage;*/
	private String accountName;
	private String receiverEmailId;
	private String receiverMobileNo;	
	private String transactionRef;
	
	private String encodedBytes;
    private String contentType;
    private String encodedBytes2;
    private String contentType2;
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("bankCode", getBankCode());
			json.put("ifscCode", getIfscCode());
			json.put("username", getUsername());
			json.put("amount", getAmount());
			json.put("accountNumber", getAccountNumber());
			
            json.put("senderName",getSenderName());
            json.put("senderEmailId",getSenderEmailId());
            json.put("senderMobileNo",getSenderMobileNo());
            json.put("description",getDescription());
            json.put("idProofNo",getIdProofNo());
        //    json.put("idProofImage",getIdProofImage());
            
            json.put("accountName", getAccountName());
            json.put("receiverEmailId",getReceiverEmailId());
            json.put("receiverMobileNo",getReceiverMobileNo());
           
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	
	public String getEncodedBytes2() {
		return encodedBytes2;
	}
	public void setEncodedBytes2(String encodedBytes2) {
		this.encodedBytes2 = encodedBytes2;
	}

	public String getContentType2() {
		return contentType2;
	}
	public void setContentType2(String contentType2) {
		this.contentType2 = contentType2;
	}

	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getEncodedBytes() {
		return encodedBytes;
	}

	public void setEncodedBytes(String encodedBytes) {
		this.encodedBytes = encodedBytes;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getTransactionRef() {
		return transactionRef;
	}

	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderEmailId() {
		return senderEmailId;
	}

	public void setSenderEmailId(String senderEmailId) {
		this.senderEmailId = senderEmailId;
	}

	public String getSenderMobileNo() {
		return senderMobileNo;
	}

	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}

	/*public MultipartFile getIdProofImage() {
		return idProofImage;
	}

	public void setIdProofImage(MultipartFile idProofImage) {
		this.idProofImage = idProofImage;
	}*/

	public String getIdProofNo() {
		return idProofNo;
	}

	public void setIdProofNo(String idProofNo) {
		this.idProofNo = idProofNo;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getReceiverEmailId() {
		return receiverEmailId;
	}

	public void setReceiverEmailId(String receiverEmailId) {
		this.receiverEmailId = receiverEmailId;
	}

	public String getReceiverMobileNo() {
		return receiverMobileNo;
	}

	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}
	
}
