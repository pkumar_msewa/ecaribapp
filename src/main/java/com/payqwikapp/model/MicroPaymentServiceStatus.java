package com.payqwikapp.model;

public class MicroPaymentServiceStatus {

	private String tokenKey;
	private String secretKey;
	private String mobileNumber;
	private double amount;
	private String consumerSecretKey;
	private String consumerTokenKey;
	private boolean status;
	private String retrivalReferenceNumber;
	private String transactionRefNo;

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getConsumerSecretKey() {
		return consumerSecretKey;
	}

	public void setConsumerSecretKey(String consumerSecretKey) {
		this.consumerSecretKey = consumerSecretKey;
	}

	public String getConsumerTokenKey() {
		return consumerTokenKey;
	}

	public void setConsumerTokenKey(String consumerTokenKey) {
		this.consumerTokenKey = consumerTokenKey;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getRetrivalReferenceNumber() {
		return retrivalReferenceNumber;
	}

	public void setRetrivalReferenceNumber(String retrivalReferenceNumber) {
		this.retrivalReferenceNumber = retrivalReferenceNumber;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

}
