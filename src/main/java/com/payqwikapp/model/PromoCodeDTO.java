package com.payqwikapp.model;

import java.util.ArrayList;
import java.util.List;

import com.payqwikapp.model.admin.CommissionListDTO;
import com.payqwikapp.model.admin.ServiceListDTO;

public class PromoCodeDTO {

	private String sessionId;

	private String promoCode;

	private String terms;

	private String startDate;

	private String endDate;

	private double value;
	
	private double cashBackValue;
	
	private ArrayList<String> services;
	private Status status;

	private String description;

	private String promoCodeId;

	private boolean fixed;
	private List<CommissionListDTO> serviceList;
	private long serviceTypeId;

	public long getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(long serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public List<CommissionListDTO> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<CommissionListDTO> serviceList) {
		this.serviceList = serviceList;
	}

	public double getCashBackValue() {
		return cashBackValue;
	}

	public void setCashBackValue(double cashBackValue) {
		this.cashBackValue = cashBackValue;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public ArrayList<String> getServices() {
		return services;
	}

	public void setServices(ArrayList<String> services) {
		this.services = services;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPromoCodeId() {
		return promoCodeId;
	}

	public void setPromoCodeId(String promoCodeId) {
		this.promoCodeId = promoCodeId;
	}

	@Override
	public String toString() {
		return "PromoCodeDTO{" +
				"promoCode='" + promoCode + '\'' +
				", terms='" + terms + '\'' +
				", startDate='" + startDate + '\'' +
				", endDate='" + endDate + '\'' +
				", value=" + value +
				", services=" + services +
				", status=" + status +
				", description='" + description + '\'' +
				", promoCodeId=" + promoCodeId +
				", fixed=" + fixed +
				'}';
	}
}
