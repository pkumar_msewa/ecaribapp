package com.payqwikapp.model;

import java.util.List;

import com.payqwikapp.entity.TKOrderDetail;
import com.payqwikapp.entity.TravelkhanaItems;
import com.payqwikapp.model.mobile.ResponseStatus;

public class TKMyOrderDTO {
	private boolean success;
	private String code;
	private String message;
	private ResponseStatus status;
	private List<TKOrderDetail>orderDetail;
    private List<TravelkhanaItems>items;
    private List<TxnStatusDTO>txnStatus;
    private Object details;
    
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public List<TxnStatusDTO> getTxnStatus() {
		return txnStatus;
	}
	public void setTxnStatus(List<TxnStatusDTO> txnStatus) {
		this.txnStatus = txnStatus;
	}
	public List<TravelkhanaItems> getItems() {
		return items;
	}
	public void setItems(List<TravelkhanaItems> items) {
		this.items = items;
	}
	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus status) {
		this.status = status;
	}
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TKOrderDetail> getOrderDetail() {
		return orderDetail;
	}
	public void setOrderDetail(List<TKOrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}
	
	
}
