package com.payqwikapp.model;

public class ImagicaLocationDTO extends SessionDTO{

    private String pincode;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
}
