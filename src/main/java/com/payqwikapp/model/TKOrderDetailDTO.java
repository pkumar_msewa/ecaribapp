package com.payqwikapp.model;


public class TKOrderDetailDTO {
	
	private Status orderStatus;
	private long userOrderId;
	private String name;	
	private String contact_no;
	private String mail_id;
	private String customer_comment;
	private String order_outlet_id;
	private String train_number;
	private double totalCustomerPayable;
	private String station_code;
	private String date;
	private String pnr;
	private String coach;
	private String seat;
	private String eta;
	private String cod;
	private String transactionRefNo;
    private Status txnStatus;
    
	public Status getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Status orderStatus) {
		this.orderStatus = orderStatus;
	}
	public long getUserOrderId() {
		return userOrderId;
	}
	public void setUserOrderId(long userOrderId) {
		this.userOrderId = userOrderId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getMail_id() {
		return mail_id;
	}
	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}
	public String getCustomer_comment() {
		return customer_comment;
	}
	public void setCustomer_comment(String customer_comment) {
		this.customer_comment = customer_comment;
	}
	public String getOrder_outlet_id() {
		return order_outlet_id;
	}
	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}
	public String getTrain_number() {
		return train_number;
	}
	public void setTrain_number(String train_number) {
		this.train_number = train_number;
	}
	public double getTotalCustomerPayable() {
		return totalCustomerPayable;
	}
	public void setTotalCustomerPayable(double totalCustomerPayable) {
		this.totalCustomerPayable = totalCustomerPayable;
	}
	public String getStation_code() {
		return station_code;
	}
	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public Status getTxnStatus() {
		return txnStatus;
	}
	public void setTxnStatus(Status txnStatus) {
		this.txnStatus = txnStatus;
	}
    
}