package com.payqwikapp.model;

public class PPIRequestDTO {
	
	private String remitterIFSCCode;
	private String recipientIFSCCode;
	private String txnValue;
	private double txnVolume;
	private String txnSubCategoryCode;
	private String others;
	public String getRemitterIFSCCode() {
		return remitterIFSCCode;
	}
	public void setRemitterIFSCCode(String remitterIFSCCode) {
		this.remitterIFSCCode = remitterIFSCCode;
	}
	public String getRecipientIFSCCode() {
		return recipientIFSCCode;
	}
	public void setRecipientIFSCCode(String recipientIFSCCode) {
		this.recipientIFSCCode = recipientIFSCCode;
	}
	public String getTxnValue() {
		return txnValue;
	}
	public void setTxnValue(String txnValue) {
		this.txnValue = txnValue;
	}
	public double getTxnVolume() {
		return txnVolume;
	}
	public void setTxnVolume(double txnVolume) {
		this.txnVolume = txnVolume;
	}
	public String getTxnSubCategoryCode() {
		return txnSubCategoryCode;
	}
	public void setTxnSubCategoryCode(String txnSubCategoryCode) {
		this.txnSubCategoryCode = txnSubCategoryCode;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}

}
