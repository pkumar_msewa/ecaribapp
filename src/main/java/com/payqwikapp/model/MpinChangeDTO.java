package com.payqwikapp.model;

public class MpinChangeDTO {

	private String sessionId;

	private String username;
	private String mpin;
	private String newMpin;
	private String confirmMpin;
	private String ipAddress;
	private String oldMpin;

	public String getOldMpin() {
		return oldMpin;
	}

	public void setOldMpin(String oldMpin) {
		this.oldMpin = oldMpin;
	}


	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMpin() {
		return mpin;
	}

	public void setMpin(String mpin) {
		this.mpin = mpin;
	}

	public String getNewMpin() {
		return newMpin;
	}

	public void setNewMpin(String newMpin) {
		this.newMpin = newMpin;
	}

	public String getConfirmMpin() {
		return confirmMpin;
	}

	public void setConfirmMpin(String confirmMpin) {
		this.confirmMpin = confirmMpin;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "MpinChangeDTO{" +
				"username='" + username + '\'' +
				", mpin='" + mpin + '\'' +
				", newMpin='" + newMpin + '\'' +
				", confirmMpin='" + confirmMpin + '\'' +
				", ipAddress='" + ipAddress + '\'' +
				", oldMpin='" + oldMpin + '\'' +
				'}';
	}
}
