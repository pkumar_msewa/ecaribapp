package com.payqwikapp.model;

public class LimitDTO {

    private double balanceLimit;
    private double dailyLimit;
    private double monthlyLimit;
    private double creditConsumed;
    private double debitConsumed;

    public double getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(double balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public double getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(double dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public double getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(double monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public double getCreditConsumed() {
        return creditConsumed;
    }

    public void setCreditConsumed(double creditConsumed) {
        this.creditConsumed = creditConsumed;
    }

    public double getDebitConsumed() {
        return debitConsumed;
    }

    public void setDebitConsumed(double debitConsumed) {
        this.debitConsumed = debitConsumed;
    }

}
