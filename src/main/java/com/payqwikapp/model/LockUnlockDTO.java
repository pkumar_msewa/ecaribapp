package com.payqwikapp.model;

public class LockUnlockDTO {

    private String id;
    private String date;
    private String requestType;
    private String status;
    private UserDTO userDTO;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    @Override
    public String toString() {
        return "LockUnlockDTO{" +
                "id='" + id + '\'' +
                ", date='" + date + '\'' +
                ", requestType='" + requestType + '\'' +
                ", status='" + status + '\'' +
                ", userDTO=" + userDTO +
                '}';
    }
}
