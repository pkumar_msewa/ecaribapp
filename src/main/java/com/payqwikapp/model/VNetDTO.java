package com.payqwikapp.model;

import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.model.travel.flight.FlightTravellerDetailsDTO;
import com.thirdparty.model.request.JSONWrapper;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class VNetDTO extends SessionDTO implements JSONWrapper{
    private String pid;
    private String amount;
    private String merchantName;
    private String mid;
    private String itc;
    private String crnNo;
    private String prnNo;
    private String returnURL;
    private String serviceCode;
    private String transactionRefNo;
    @JsonIgnore
    private PQTransaction pqTransaction;
    
    private List<FlightTravellerDetailsDTO> travellerDetails;
    private String mode;
	private String ticketDetails;
	
    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getItc() {
        return itc;
    }

    public void setItc(String itc) {
        this.itc = itc;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getPrnNo() {
        return prnNo;
    }

    public void setPrnNo(String prnNo) {
        this.prnNo = prnNo;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }
    public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public PQTransaction getPqTransaction() {
		return pqTransaction;
	}

	public void setPqTransaction(PQTransaction pqTransaction) {
		this.pqTransaction = pqTransaction;
	}
	
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getTicketDetails() {
		return ticketDetails;
	}
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	public List<FlightTravellerDetailsDTO> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<FlightTravellerDetailsDTO> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	/**
     *     private String pid;
     private String amount;
     private String merchantName;
     private String mid;
     private String itc;
     private String crnNo;
     private String prnNo;
     private String returnURL;
     private String serviceCode;

     * @return
     */
    @Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("pid", getPid());
            json.put("amount",getAmount());
            json.put("merchantName",getMerchantName());
            json.put("mid",getMid());
            json.put("itc",getItc());
            json.put("crnNo",getCrnNo());
            json.put("prnNo",getPrnNo());
            json.put("returnURL",getReturnURL());
            json.put("serviceCode",getServiceCode());
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        return json;
    }

    @Override
    public String toString() {
        return "VNetDTO{" +
                "pid='" + pid + '\'' +
                ", amount='" + amount + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", mid='" + mid + '\'' +
                ", itc='" + itc + '\'' +
                ", crnNo='" + crnNo + '\'' +
                ", prnNo='" + prnNo + '\'' +
                ", returnURL='" + returnURL + '\'' +
                ", serviceCode='" + serviceCode + '\'' +
                '}';
    }
}
