package com.payqwikapp.model;

import java.util.List;

import org.json.JSONArray;

public class BulkUploadDTO {

	private String sessionId;
    private String contentType;
    private String fileName;
    private List<SendMoneyMobileDTO> mobileDTO;
    
	public List<SendMoneyMobileDTO> getMobileDTO() {
		return mobileDTO;
	}

	public void setMobileDTO(List<SendMoneyMobileDTO> mobileDTO) {
		this.mobileDTO = mobileDTO;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }


}
