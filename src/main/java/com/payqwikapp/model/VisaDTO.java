package com.payqwikapp.model;

import org.json.JSONException;
import org.json.JSONObject;

public class VisaDTO {

	private String request;
	private String data;
	private String sessionId;
	private Object object;
	private String txRef;
	private String transactionStatus;
	private String externalTransactionId;
	private double amount;
	private String merchantName;
	private String merchantId;
	private String merchantAddress;
	private String additionalData;
	private String accountNumber;
	private boolean success;
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getMerchantName() {
		return merchantName;
	}


	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}


	public String getMerchantId() {
		return merchantId;
	}


	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}


	public String getMerchantAddress() {
		return merchantAddress;
	}


	public void setMerchantAddress(String merchantAddress) {
		this.merchantAddress = merchantAddress;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getTxRef() {
		return txRef;
	}


	public void setTxRef(String txRef) {
		this.txRef = txRef;
	}


	public String getTransactionStatus() {
		return transactionStatus;
	}


	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}


	public String getExternalTransactionId() {
		return externalTransactionId;
	}


	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}


	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("request", request);
			json.put("data", data);
			json.put("object", object);
			json.put("sessionId", sessionId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	
	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


	public String getSessionId() {
		return sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public Object getObject() {
		return object;
	}


	public void setObject(Object object) {
		this.object = object;
	}


	public String getAdditionalData() {
		return additionalData;
	}


	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	@Override
	public String toString() {
		return "VisaDTO{" +
				"request='" + request + '\'' +
				", data='" + data + '\'' +
				", object=" + object +
				", txRef='" + txRef + '\'' +
				", transactionStatus='" + transactionStatus + '\'' +
				", externalTransactionId='" + externalTransactionId + '\'' +
				", amount=" + amount +
				", merchantName='" + merchantName + '\'' +
				", merchantId='" + merchantId + '\'' +
				", merchantAddress='" + merchantAddress + '\'' +
				", additionalData='" + additionalData + '\'' +
				", accountNumber='" + accountNumber + '\'' +
				'}';
	}
}
