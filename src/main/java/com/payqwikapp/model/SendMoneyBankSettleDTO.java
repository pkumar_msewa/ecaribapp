package com.payqwikapp.model;

public class SendMoneyBankSettleDTO {

	private String sessionId;
	private String transactionRefNo;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	@Override
	public String toString() {
		return "SendMoneyBankSettleDTO{" +
				"transactionRefNo='" + transactionRefNo + '\'' +
				'}';
	}
}
