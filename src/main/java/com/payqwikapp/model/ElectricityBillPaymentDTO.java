package com.payqwikapp.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ElectricityBillPaymentDTO {

	private String sessionId;

	private String serviceProvider;

	private String accountNumber;

	private String cycleNumber;

	private String amount;

	private String billingUnit;

	private String cityName;

	private String processingCycle;
	
	private boolean fromMdex;
	
	private String additionalInfo;


	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("serviceProvider", serviceProvider);
			json.put("accountNumber", accountNumber);
			json.put("cycleNumber", cycleNumber);
			json.put("amount", amount);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public boolean isFromMdex() {
		return fromMdex;
	}
	public void setFromMdex(boolean fromMdex) {
		this.fromMdex = fromMdex;
	}
	public String getProcessingCycle() {
		return processingCycle;
	}

	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getBillingUnit() {
		return billingUnit;
	}

	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCycleNumber() {
		return cycleNumber;
	}

	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Override
	public String toString() {
		return "ElectricityBillPaymentDTO{" +
				"serviceProvider='" + serviceProvider + '\'' +
				", accountNumber='" + accountNumber + '\'' +
				", cycleNumber='" + cycleNumber + '\'' +
				", amount='" + amount + '\'' +
				", billingUnit='" + billingUnit + '\'' +
				", cityName='" + cityName + '\'' +
				", processingCycle='" + processingCycle + '\'' +
				'}';
	}
}
