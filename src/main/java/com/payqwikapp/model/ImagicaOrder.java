package com.payqwikapp.model;

public class ImagicaOrder extends SessionDTO{
    private String uid;
    private String orderId;
    private String profileId;
    private double totalAmount;
    private double baseAmount;
    private double serviceTax;
    private double sbCess;
    private double kkCess;


    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(double serviceTax) {
        this.serviceTax = serviceTax;
    }

    public double getSbCess() {
        return sbCess;
    }

    public void setSbCess(double sbCess) {
        this.sbCess = sbCess;
    }

    public double getKkCess() {
        return kkCess;
    }

    public void setKkCess(double kkCess) {
        this.kkCess = kkCess;
    }
}
