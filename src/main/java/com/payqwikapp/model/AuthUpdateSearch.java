package com.payqwikapp.model;

public class AuthUpdateSearch extends SessionDTO{

    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }


}
