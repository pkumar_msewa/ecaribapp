package com.payqwikapp.model;

import java.util.List;

public class MRequestDTO {

	private List<NeftRequestDTO> requests;

	public List<NeftRequestDTO> getRequests() {
		return requests;
	}

	public void setRequests(List<NeftRequestDTO> requests) {
		this.requests = requests;
	}

}
