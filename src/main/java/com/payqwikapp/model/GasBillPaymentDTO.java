package com.payqwikapp.model;

import org.json.JSONException;
import org.json.JSONObject;

public class GasBillPaymentDTO {

	private String sessionId;

	private String serviceProvider;

	private String accountNumber;

	private String amount;
	
	private boolean fromMdex;
	
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("serviceProvider", serviceProvider);
			json.put("accountNumber", accountNumber);
			json.put("amount", amount);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public boolean isFromMdex() {
		return fromMdex;
	}



	public void setFromMdex(boolean fromMdex) {
		this.fromMdex = fromMdex;
	}



	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	@Override
	public String toString() {
		return "GasBillPaymentDTO{" +
				"serviceProvider='" + serviceProvider + '\'' +
				", accountNumber='" + accountNumber + '\'' +
				", amount='" + amount + '\'' +
				'}';
	}
}
