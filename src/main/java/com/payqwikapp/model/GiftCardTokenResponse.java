package com.payqwikapp.model;

import java.util.Date;

public class GiftCardTokenResponse {
	
	 private String sessionName;
	    private String token;
	    private boolean success;
	    private String message;
	    private Date expiryDate;
	    private String sessionId;

	    public String getSessionId() {
	        return sessionId;
	    }

	    public void setSessionId(String sessionId) {
	        this.sessionId = sessionId;
	    }

	    public String getSessionName() {
	        return sessionName;
	    }

	    public void setSessionName(String sessionName) {
	        this.sessionName = sessionName;
	    }

	    public String getToken() {
	        return token;
	    }

	    public void setToken(String token) {
	        this.token = token;
	    }

	    public boolean isSuccess() {
	        return success;
	    }

	    public void setSuccess(boolean success) {
	        this.success = success;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

		public Date getExpiryDate() {
			return expiryDate;
		}

		public void setExpiryDate(Date expiryDate) {
			this.expiryDate = expiryDate;
		}

	

}
