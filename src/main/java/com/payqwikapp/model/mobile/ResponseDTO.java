package com.payqwikapp.model.mobile;

import java.util.Date;

/**
 * @author Kamal
 *
 */
public class ResponseDTO {

	private String status;
	private String code;
	private String message;
	private double balance;
	private Object details;
	private String txnId;
	private Object error;
	private boolean valid;
	private String info;
	private boolean hasRefer;
	private boolean isExistTreatCard;
	private String transactionRefNO;
	private long membershipCode;
	private Date memberShipCodeExpire;
	private Object userList;
	private Object details2;
	private long count;
	private String sessionId;
	private String transactionDate;
	private boolean userExist;
	private Object details3;
	private Object details4;
	private double remBalance;

	public double getRemBalance() {
		return remBalance;
	}

	public void setRemBalance(double remBalance) {
		this.remBalance = remBalance;
	}

	public Object getDetails4() {
		return details4;
	}

	public void setDetails4(Object details4) {
		this.details4 = details4;
	}

	public Object getDetails3() {
		return details3;
	}

	public void setDetails3(Object details3) {
		this.details3 = details3;
	}

	public boolean isUserExist() {
		return userExist;
	}

	public void setUserExist(boolean userExit) {
		this.userExist = userExit;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Object getDetails2() {
		return details2;
	}

	public void setDetails2(Object details2) {
		this.details2 = details2;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public long getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(long membershipCode) {
		this.membershipCode = membershipCode;
	}

	public Date getMemberShipCodeExpire() {
		return memberShipCodeExpire;
	}

	public void setMemberShipCodeExpire(Date memberShipCodeExpire) {
		this.memberShipCodeExpire = memberShipCodeExpire;
	}

	public boolean isExistTreatCard() {
		return isExistTreatCard;
	}

	public void setExistTreatCard(boolean isExistTreatCard) {
		this.isExistTreatCard = isExistTreatCard;
	}

	public String getTransactionRefNO() {
		return transactionRefNO;
	}

	public void setTransactionRefNO(String transactionRefNO) {
		this.transactionRefNO = transactionRefNO;
	}

	public boolean isHasRefer() {
		return hasRefer;
	}

	public void setHasRefer(boolean hasRefer) {
		this.hasRefer = hasRefer;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getUserList() {
		return userList;
	}

	public void setUserList(Object userList) {
		this.userList = userList;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
