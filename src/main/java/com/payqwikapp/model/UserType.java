package com.payqwikapp.model;

public enum UserType {
	Admin,User,Merchant,Locked,SuperAdmin,Agent,SuperAgent,Donatee,SpecialUser;
}
