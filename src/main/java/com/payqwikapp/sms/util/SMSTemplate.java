package com.payqwikapp.sms.util;

public class SMSTemplate {

	public static String ADMIN_TEMPLATE = "admintemplate.vm";

	public static String MERCHANT_REGISTRATION = "merchant_registration.vm";

	public static String DEVICE_BINDING = "device_binding.vm";

	public static String IPAY_ALERT = "ipay_alert.vm";

	public static String VERIFICATION_MOBILE = "verification_mobile.vm";
	
	public static String UPDATE_MAX_LIMIT = "update_max_limit.vm";
	public static String SUCCESS_PAYMENT_LIMIT = "success_update_payment_limit.vm";

	public static String BHARATQR_SUCCESS = "bqr_success.vm";

	public static String OFFLINE_VERIFICATION_MOBILE = "offline_verification_mobile.vm";
//
	public static String VERIFICATION_MOBILE_KYC = "verification_mobile_kyc.vm";

	public static String VERIFICATION_KYC_SUCCESS = "verification_kyc_success.vm";

	public static String REFUND_SUCCESS = "refund_transaction_success.vm";

	public static String REFUND_SUCCESS_SENDER = "refund_transaction_success_sender.vm";

	//
	public static String VERIFICATION_SUCCESS = "verification_success.vm";
	
	public static String AGENT_VERIFICATION_SUCCESS = "agent_verification_success.vm";
//
	public static String REGENERATE_OTP = "regenerate_otp.vm";
	
	public static String TRANSACTION_OTP ="transaction_otp.vm";
//
//	public static String TRANSACTION_CREDIT = "transaction_credit.vm";
//
//	public static String TRANSACTION_DEBIT = "transaction_debit.vm";
//	
	public static String PROMOTIONAL_SMS = "promotional_sms.vm";
//
	public static String CHANGE_PASSWORD_REQUEST = "changepassword_request.vm";
//	
	public static String CHANGE_PASSWORD_SUCCESS = "changepassword_success.vm";
//	
//	public static String TRANSACTION_CREDIT_SEND_MONEY = "transaction_credit_sm.vm";

	public static String FUNDTRANSFER_SUCCESS_SENDER = "fundtransfer_success_sender.vm";
	
	public static String FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED = "fundtransfer_success_receiver_unregistered.vm";
	
	public static String FUNDTRANSFER_SUCCESS_RECEIVER = "fundtransfer_success_receiver.vm";
	
	public static String TRANSACTION_FAILED = "transaction_failed.vm";
	
	public static String BILLPAYMENT_SUCCESS = "billpayment_success.vm";
	
	public static String AGENT_SENDER_BANK_TRANSFER = "agentSenderBankTransfer.vm";
	
	public static String AGENT_RECEIVER_BANK_TRANSFER = "agentReceiverBankTransfer.vm";
	
	public static String BILLPAYMENT_SUCCESS_MERAEVENT = "meraevent.vm";
	
	public static String LOADMONEY_SUCCESS = "loadmoney_success.vm";

	public static String PROMO_SUCCESS = "success_promo.vm";
	
	public static String PREDICT_SUCCESS = "predict_success.vm";

	public static String MERCHANT_SENDER = "merchant_success_sender.vm";

	public static String MERCHANT_RECEIVER = "merchant_success_receiver.vm";
	
	public static String NEFT_TRANSFER_SUCCESS = "NEFT_Transfer_Success.vm";
	
	public static String NIKKI_CHAT_SUCCESS = "nikki_chat.vm";
	
// gci sms
	public static String GCI_SMS = "gci_success.vm";
	
	public static String BDAY_ALERT = "bday_alert.vm";
	
	//Qwikrpay
	
	public static String QWIK_PAY="qwikrpay_success";
	
	//for topup reversed transactions
	
	public static String TRANSACTIONAL_SMS_PROMOTION="reversed_trx.vm";
	//for refer n earn
	public static String REFER_EARN_VM="RefernEarn.vm";
	
	public static String REFER_EARN_SMS = "success_refer.vm";
	
	public static String REMAINDER_SMS = "reminder_message.vm";
	
	public static String BUSTICKET_SUCCESS = "busTicketpayment_success.vm";
	
	public static String FLIGHTTICKET_SUCCESS = "flightTicketpayment_success.vm";
	
	public static String SUPERADMIN_VERIFICATION_MOBILE = "superadmin_verification.vm";
	
	public static String FLIGHT_LOADMONEY_SUCCESS = "FlightLoadMoney_Success.vm";
	
	public static String REDEEMPOINTS_SUCCESS = "success_redeempoints.vm";
	
	//Travelkhana
	public static String TK_BILLPAYMENT_SUCCESS = "TKbillpayment_success.vm";
	
    public static String HOUSEJOY_PAYMENT_SUCCESS = "houseJoypayment_success.vm";
    
    public static String BULKUPLOAD_PAYMENT_SUCCESS = "bulkupload_success.vm";
    
    public static String BESCOM_UPI_FAILED = "bescom_upi_failure.vm";
    
    public static String BESCOM_UPI_SUCCESS = "bescom_upi_success.vm";

    public static String WOOHOO_GIFTCARD_SMS = "woohoosms.vm";
    
    public static String WOOHOO_GIFTCARD_PENDING_SMS = "woohoo_pending.vm";
    
    public static String WOOHOO_GIFTCARD_SENDER_SUCCESS_SMS = "woohoo_sender_success.vm";
    
    public static String WOOHOO_GIFTCARD_RECEIVER_SUCCESS_SMS = "woohoo_receiver_success.vm";
    
    public static String PREDICT_PAYMENT_SUCCESS = "predict_winner.vm";
    
    public static String LESS_BALANCE_ALERT = "lessamount_message.vm";
}
