package com.payqwikapp.mail.util;

import java.util.Properties;

import com.payqwikapp.util.DeploymentConstants;

public class MailConstants {

	public static final String SMTP_HOST_BULK_EMAIL = getSmtpHostBulkEmail();
	public static final String CC_MAIL = getCCEmail();
	public static final String DEBUG = getDebug();
	public static final String SENDER_EMAIL = getSenderEmail();
	public static final String PASSWORD = getPassword();
	public static final String USER_ID = getUserId();
	public static final String SMTP_AUTH = getSmtpAuth();
	public static final String SMTP_STARTTLS_ENABLE = getSmtpStartTlsEnable();
	public static final boolean SMTP_AUTH_PLAIN_DISABLE = getSmtpAuthPlainDisable();
	public static final String SMTP_HOST = getSmtpHost();
	public static final String SMTP_SSL_TRUST = getSmtpSslTrust();
	public static final int SMTP_PORT = getSmtPort();
	public static final String SENDER_EMAIL_NO_REPLY = getSenderEmailNoReply();
	public static final String MAIL_SERVER = getMailServer();

	// TEST EMAIL
	public static final String SMTP_HOST_BULK_EMAIL_TEST = "172.16.7.58";
	public static final String CC_MAIL_TEST = "info@msewa.com";
	public static final String DEBUG_TEST = "false";
	public static final String SENDER_EMAIL_TEST = "vpayqwiktest@gmail.com";
	public static final String PASSWORD_TEST = "Msewa@vpayqwik";
	public static final String USER_ID_TEST = "vpayqwiktest@gmail.com";
	public static final String SMTP_AUTH_TEST = "true";
	public static final String SMTP_STARTTLS_ENABLE_TEST = "true";
	public static final boolean SMTP_AUTH_PLAIN_DISABLE_TEST = true;
	public static final String SMTP_HOST_TEST = "smtp.gmail.com";
	public static final String SMTP_SSL_TRUST_TEST = "smtp.gmail.com";
	public static final int SMTP_PORT_TEST = 587;
	private static final String MAIL_SERVER_TEST = "smtp";
	public static final String SENDER_EMAIL_NO_REPLY_TEST = "vpayqwiktest@gmail.com";

	// LIVE EMAIL
	public static final String SMTP_HOST_BULK_EMAIL_LIVE = "172.16.7.58";
	public static final String CC_MAIL_LIVE = "info@msewa.com";
	public static final String DEBUG_LIVE = "false";
	public static final String SENDER_EMAIL_LIVE = "vpayqwikcare@vijayabank.co.in";
	public static final String PASSWORD_LIVE = "msewa@123";
	public static final String USER_ID_LIVE = "vpayqwikcare";
	public static final String SENDER_EMAIL_NO_REPLY_LIVE = "vpayqwikcare@vijayabank.co.in";
	public static final String USER_ID_NO_REPLY_LIVE = "noreplyvpayqwikcare";
	public static final String SMTP_AUTH_LIVE = "true";
	public static final String SMTP_STARTTLS_ENABLE_LIVE = "true";
	public static final boolean SMTP_AUTH_PLAIN_DISABLE_LIVE = true;
	public static final String SMTP_HOST_LIVE = "172.16.7.25";
	public static final String SMTP_SSL_TRUST_LIVE = "*";
	public static final int SMTP_PORT_LIVE = 25;
	private static final String MAIL_SERVER_LIVE = "172.16.7.25";

	public static Properties getEmailProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", MailConstants.SMTP_AUTH);
		props.put("mail.smtp.starttls.enable", MailConstants.SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth.plain.disable", MailConstants.SMTP_AUTH_PLAIN_DISABLE);
		props.put("mail.smtp.host", MailConstants.SMTP_HOST);
		props.put("mail.smtp.ssl.trust", MailConstants.SMTP_SSL_TRUST);
		props.put("mail.smtp.port", MailConstants.SMTP_PORT);
		props.put("mail.debug", MailConstants.DEBUG);
		// props.put("mail.smtp.ssl.enable", "true");
		return props;
	}

	public static Properties getBulkEmailProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", MailConstants.SMTP_AUTH);
		props.put("mail.smtp.starttls.enable", MailConstants.SMTP_STARTTLS_ENABLE);
		props.put("mail.smtp.auth.plain.disable", MailConstants.SMTP_AUTH_PLAIN_DISABLE);
		props.put("mail.smtp.host", MailConstants.SMTP_HOST_BULK_EMAIL);
		props.put("mail.smtp.ssl.trust", MailConstants.SMTP_SSL_TRUST);
		props.put("mail.smtp.port", MailConstants.SMTP_PORT);
		props.put("mail.debug", MailConstants.DEBUG);
		// props.put("mail.smtp.ssl.enable", "true");
		return props;
	}

	public static String getSenderEmail() {
		if (DeploymentConstants.PRODUCTION) {
			return SENDER_EMAIL_LIVE;
		} else {
			return SENDER_EMAIL_TEST;
		}
	}

	public static String getCCEmail() {
		if (DeploymentConstants.PRODUCTION) {
			return CC_MAIL_LIVE;
		} else {
			return CC_MAIL_TEST;
		}
	}

	public static String getPassword() {
		if (DeploymentConstants.PRODUCTION) {
			return PASSWORD_LIVE;
		} else {
			return PASSWORD_TEST;
		}
	}

	public static String getUserId() {
		if (DeploymentConstants.PRODUCTION) {
			return USER_ID_LIVE;
		} else {
			return USER_ID_TEST;
		}
	}

	public static String getSmtpHost() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_HOST_LIVE;
		} else {
			return SMTP_HOST_TEST;
		}
	}

	public static String getSmtpSslTrust() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_SSL_TRUST_LIVE;
		} else {
			return SMTP_SSL_TRUST_TEST;
		}
	}

	public static int getSmtPort() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_PORT_LIVE;
		} else {
			return SMTP_PORT_TEST;
		}
	}

	public static String getSenderEmailNoReply() {
		if (DeploymentConstants.PRODUCTION) {
			return SENDER_EMAIL_NO_REPLY_LIVE;
		} else {
			return SENDER_EMAIL_NO_REPLY_TEST;
		}
	}

	public static String getSmtpHostBulkEmail() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_HOST_BULK_EMAIL_LIVE;
		} else {
			return SMTP_HOST_BULK_EMAIL_TEST;
		}
	}

	public static String getDebug() {
		if (DeploymentConstants.PRODUCTION) {
			return DEBUG_LIVE;
		} else {
			return DEBUG_TEST;
		}
	}

	public static String getSmtpAuth() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_AUTH_LIVE;
		} else {
			return SMTP_AUTH_TEST;
		}
	}

	public static String getSmtpStartTlsEnable() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_STARTTLS_ENABLE_LIVE;
		} else {
			return SMTP_STARTTLS_ENABLE_TEST;
		}
	}

	public static String getMailServer() {
		if (DeploymentConstants.PRODUCTION) {
			return MAIL_SERVER_LIVE;
		} else {
			return MAIL_SERVER_TEST;
		}
	}

	public static boolean getSmtpAuthPlainDisable() {
		if (DeploymentConstants.PRODUCTION) {
			return SMTP_AUTH_PLAIN_DISABLE_LIVE;
		} else {
			return SMTP_AUTH_PLAIN_DISABLE_TEST;
		}
	}

}
