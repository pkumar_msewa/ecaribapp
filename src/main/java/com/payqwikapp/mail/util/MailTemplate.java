package com.payqwikapp.mail.util;

public class MailTemplate {

	public static String MERCHANT_REGISTRATION = "merchant_registration.vm";

	public static String AGENT_REGISTRATION = "agent_registration.vm";
	
	public static String ADMIN_TEMPLATE = "admintemplate.vm";

	public static String BHARATQR_SUCCESS = "bqr_success.vm";

	public static String VERIFICATION_EMAIL = "verification_email.vm";
	//
	public static String VERIFICATION_SUCCESS = "verification_success.vm";
	//
	//	public static String CHANGE_PASSWORD_REQUEST = "changepassword_request.vm";
	//
	public static String CHANGE_PASSWORD_SUCCESS = "changepassword_success.vm";
	// gci meltemplate
	public static String GCI_SENDER ="gci_success_customer.vm";
	//
	//	public static String TRANSACTION_DEBIT = "transaction_debit.vm";
	//	
	//	public static String TRANSACTION_CREDIT = "transaction_credit.vm";
	//	
	public static String INVITE_FRIEND = "invite_friend.vm";
	//	
	//	public static String TRANSACTION_CREDIT_SEND_MONEY = "transaction_credit_sm.vm";

	public static String FUNDTRANSFER_SUCCESS_SENDER = "fundtransfer_success_sender.vm";

	public static String FUNDTRANSFER_SUCCESS_RECEIVER_UNREGISTERED = "fundtransfer_success_receiver_unregistered.vm";

	public static String FUNDTRANSFER_SUCCESS_RECEIVER = "fundtransfer_success_receiver.vm";

	public static String MERCHANT_SENDER = "merchant_success_customer.vm";

	public static String MERCHANT_RECEIVER = "merchant_success_receiver.vm";

	public static String TRANSACTION_FAILED = "transaction_failed.vm";

	public static String BILLPAYMENT_SUCCESS = "billpayment_success.vm";

	public static String AGENT_BANK_TRANSFER_SENDER = "agentBankTransferSender.vm";
	
	public static String AGENT_BANK_TRANSFER_RECEIVER = "agentBankTransferReceiver.vm";
	
	public static String LOADMONEY_SUCCESS = "loadmoney_success.vm";

	public static String PROMO_SUCCESS = "success_promo.vm";

	public static String NIKKI_CHAT_SUCCESS = "nikki_chat_success.vm";

	public static String QWIK_PAY="qwikrpay_success.vm";

	public static String VERIFICATION_EMAIL_FOR_MONTHLY_TRANSACTION = "Monthly_Transaction.vm";

	public static String BUSBOOKING_SUCCESS = "busPayment_success.vm";

	public static String FLIGHTBOOKING_SUCCESS = "flightPayment_success.vm";

	public static String REDEEMPOINTS_CASH_SUCCESS = "redeempoints_cash.vm";

	//Travelkhana
	public static String TK_BILLPAYMENT_SUCCESS = "TKbillpayment_success.vm";

	public static String REFUND_TRANSACTION = "refund_request.vm";

	public static String HOUSEJOY_SUCCESS="houseJoy_success.vm";
	
	//WOOHOO
	public static String WOOHOO_SENDER ="woohoo_gift_card.vm";
	
	public static String WOOHOO_SENDER_WITHOUT_TC ="woohoo_gift_card_without_t&c.vm";
	
	public static String TREATCARD_EMAIL ="treatcard.vm";
	
	
	public static String BULKUPLOAD_PAYMENT_SUCCESS ="bulkpaymentsuccess.vm";
	
	public static String MERCHANT_SENDER_CANTEEN = "merchant_success_canteen.vm";
	
	public static String SUMMARY_EMAIL = "summary_email.vm";
}