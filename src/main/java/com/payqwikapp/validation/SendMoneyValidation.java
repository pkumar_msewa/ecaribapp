package com.payqwikapp.validation;

import com.payqwikapp.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyDonateeDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.error.SendMoneyBankError;
import com.payqwikapp.model.error.SendMoneyMobileError;
import com.payqwikapp.repositories.PQServiceRepository;

public class SendMoneyValidation {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final PQServiceRepository pqServiceRepository;

	public SendMoneyValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public SendMoneyMobileError checkMobileError(SendMoneyMobileDTO mobile, String username, String serviceCode) {
		SendMoneyMobileError error = new SendMoneyMobileError();
		boolean valid = true;
		if (mobile.getMobileNumber().equals(username)) {
			error.setMessage("You cannot send money to your own mobile .");
			valid = false;
		}
		PQService service = pqServiceRepository.findServiceByCode(serviceCode);
		if (service.getStatus().equals(Status.Inactive)) {
			error.setMessage("Service is down for maintenance .");
			valid = false;
		}
		if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				mobile.getAmount())) {
			error.setMessage(
					"Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

	//send money to donatee
	
	public SendMoneyMobileError checkMobileError(SendMoneyDonateeDTO mobile, String username, String serviceCode) {
		SendMoneyMobileError error = new SendMoneyMobileError();
		boolean valid = true;
		if (mobile.getDonatee().equals(username)) {
			error.setAmount("You cannot send money to your own mobile .");
			valid = false;
		}
		PQService service = pqServiceRepository.findServiceByCode(serviceCode);
		if(service.getStatus().equals(Status.Inactive)) {
			error.setAmount("Service is down for maintenance");
			valid = false;
		}
		if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(), mobile.getAmount())) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

	public SendMoneyBankError checkBankError(SendMoneyBankDTO bank, String serviceCode) {
		SendMoneyBankError error = new SendMoneyBankError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode(serviceCode);
		if (service.getStatus().equals(Status.Inactive)) {
			error.setAmount("Service is down for maintenance");
			valid = false;
		}
		if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(), bank.getAmount())) {
			error.setAmount(
					"Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public SendMoneyBankError checkABankError(AgentSendMoneyBankDTO bank, String serviceCode) {
		SendMoneyBankError error = new SendMoneyBankError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode(serviceCode);
		if (service.getStatus().equals(Status.Inactive)) {
			error.setAmount("Service is down for maintenance");
			valid = false;
		}
		if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(), bank.getAmount())) {
			error.setAmount(
					"Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
}
