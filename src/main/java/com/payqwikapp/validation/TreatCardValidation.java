package com.payqwikapp.validation;

import com.payqwikapp.model.TreatCardPlansDTO;
import com.payqwikapp.model.error.TreatCardPlansError;

public class TreatCardValidation {
	
    public TreatCardPlansError validatePlans(TreatCardPlansDTO dto){
    	
    	TreatCardPlansError error = new TreatCardPlansError();
        boolean valid = true;
        
        if(dto.getAmount() <= 0) {
            error.setAmount("Balance Limit must be greater than 0");
            valid = false;
        }
        error.setValid(valid);
        return error;
    }

}
