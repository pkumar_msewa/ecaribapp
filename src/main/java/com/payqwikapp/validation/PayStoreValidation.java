package com.payqwikapp.validation;

import com.payqwikapp.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.PayAtStoreDTO;
import com.payqwikapp.model.error.PayAtStoreError;
import com.payqwikapp.repositories.PQServiceRepository;

public class PayStoreValidation {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final PQServiceRepository pqServiceRepository;

	public PayStoreValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public PayAtStoreError checkError(PayAtStoreDTO store, String serviceCode) {
		boolean valid = true;
		PayAtStoreError error = new PayAtStoreError();
//		if (CommonValidation.isNull(store.getServiceProvider())) {
//			error.setServiceProvider("Please select service provider");
//			valid = false;
//		}
//		if (CommonValidation.isNull(store.getOrderID())) {
//			error.setOrderID("Please enter Order ID");
//			valid = false;
//		}
//		if (CommonValidation.isNull(store.getMessage())) {
//			error.setAmount("Enter Message in field");
//			valid = false;
//		}
//
//		if (CommonValidation.isNull(store.getAmount())) {
//			error.setAmount("Enter amount in field");
//			valid = false;
//		}
//		if (!CommonValidation.isNumeric(store.getAmount())) {
//			error.setAmount("Amount must be in digits");
//			valid = false;
//		}
//
//		if (!CommonValidation.isNumeric(store.getOrderID())) {
//			error.setOrderID("Enter Valid Order ID");
//			valid = false;
//		}

		PQService service = pqServiceRepository.findServiceByCode(serviceCode);
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				store.getAmount())) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}
}
