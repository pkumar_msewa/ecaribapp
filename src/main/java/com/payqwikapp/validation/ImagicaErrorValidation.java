package com.payqwikapp.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.ImagicaOrder;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.error.ImagicaError;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.util.StartupUtil;

public class ImagicaErrorValidation {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public ImagicaErrorValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public ImagicaError checkError(ImagicaOrder imagica) {
		ImagicaError error = new ImagicaError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode(StartupUtil.GiftcartCode);
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				String.valueOf(imagica.getTotalAmount()))) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

}
