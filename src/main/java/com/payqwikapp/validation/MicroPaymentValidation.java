package com.payqwikapp.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.MicroPaymentTransactionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.error.MicroPaymentError;
import com.payqwikapp.repositories.PQServiceRepository;

public class MicroPaymentValidation {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public MicroPaymentValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public MicroPaymentError checkError(MicroPaymentTransactionDTO mobile) {
		MicroPaymentError error = new MicroPaymentError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode("NIKKI");
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				String.valueOf(mobile.getAmount()))) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

}
