package com.payqwikapp.validation;

import com.payqwikapp.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.GasBillPaymentDTO;
import com.payqwikapp.model.error.GasBillPaymentError;
import com.payqwikapp.repositories.PQServiceRepository;

public class GasBillPaymentValidation {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public GasBillPaymentValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public GasBillPaymentError checkError(CommonRechargeDTO gas) {
		GasBillPaymentError error = new GasBillPaymentError();
		boolean valid = true;
//		if (CommonValidation.isNull(gas.getAccountNumber())) {
//			error.setAccountNumber("Please enter Account Number");
//			valid = false;
//		}
//		if (CommonValidation.isNull(gas.getAmount())) {
//			error.setAmount("Please enter Amount");
//			valid = false;
//		}
//		if (CommonValidation.isNull(gas.getServiceProvider())) {
//			error.setServiceProvider("Please select Service Provider");
//			valid = false;
//		}
//		if (!CommonValidation.isNumeric(gas.getAccountNumber())) {
//			error.setAccountNumber("Enter valid account number");
//			valid = false;
//		}
//		if (!CommonValidation.isNumeric(gas.getAmount())) {
//			error.setAmount("Enter valid amount in the field");
//			valid = false;
//		}
		
		PQService service = pqServiceRepository.findServiceByCode(gas.getServiceProvider());
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(), gas.getAmount())) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
}
