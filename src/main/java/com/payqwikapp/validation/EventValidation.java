package com.payqwikapp.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.EventDetailsDTO;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.error.EventError;
import com.payqwikapp.repositories.PQServiceRepository;

public class EventValidation {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pQServiceRepository;

	public EventValidation(PQServiceRepository pQServiceRepository) {
		this.pQServiceRepository = pQServiceRepository;
	}

	public EventError checkError(EventDetailsDTO event) {
		EventError error = new EventError();
		boolean valid = true;
		PQService service = pQServiceRepository.findServiceByCode(event.getServiceCode());
		if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				""+event.getNetAmount())) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}
}

