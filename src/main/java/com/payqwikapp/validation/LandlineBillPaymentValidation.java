package com.payqwikapp.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.error.LandlineBillPaymentError;
import com.payqwikapp.repositories.PQServiceRepository;

public class LandlineBillPaymentValidation {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public LandlineBillPaymentValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public LandlineBillPaymentError checkError(CommonRechargeDTO landline) {

		LandlineBillPaymentError error = new LandlineBillPaymentError();
		boolean valid = true;
		
		PQService service = pqServiceRepository.findServiceByCode(landline.getServiceProvider());
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(), landline.getAmount())) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. " + service.getMaxAmount());
			valid = false;
		}
		error.setValid(valid);
		return error;

	}
}
