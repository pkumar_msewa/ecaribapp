package com.payqwikapp.validation;

import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.VBankAccountDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.error.MRegistrationError;
import com.payqwikapp.model.merchant.MerchantKycDTO;
import com.payqwikapp.model.merchant.MerchantRegisterDTO;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.VBankAccountDetailRepository;
import com.payqwikapp.util.SecurityUtil;

public class MRegistrationValidation {

    public MRegistrationValidation(VBankAccountDetailRepository vBankAccountDetailRepository,PQAccountDetailRepository pqAccountDetailRepository) {
        this.vBankAccountDetailRepository = vBankAccountDetailRepository;
        this.pqAccountDetailRepository = pqAccountDetailRepository;
    }
    private VBankAccountDetailRepository vBankAccountDetailRepository;
    private PQAccountDetailRepository pqAccountDetailRepository;

    public MRegistrationError validateKYCRequest(MerchantKycDTO dto){
        MRegistrationError error = new MRegistrationError();
        boolean valid = true;
        if(!CommonValidation.validateMerchantVBankAccountNumber(dto.getAccountNumber())) {
            error.setValid(false);
            error.setMessage("Please enter valid account number");
            return error;
        } else if(!CommonValidation.validateMerchantMobileNumber(dto.getMobileNumber())){
            error.setValid(false);
            error.setMessage("Please enter valid mobile number");
            return error;
        }else {
            try {
                String hashedAccountNumber = SecurityUtil.sha1(dto.getAccountNumber());
                VBankAccountDetail vBankAccountDetail = vBankAccountDetailRepository.findByAccountNumber(hashedAccountNumber);
                if(vBankAccountDetail != null){
                    if(vBankAccountDetail.getStatus().equals(Status.Active)) {
                        error.setMessage("Account No. is already in use");
                        valid = false;
                    }else {
                        PQAccountDetail account = pqAccountDetailRepository.findUsingVBankAccount(vBankAccountDetail);
                        account.setvBankAccount(null);
                        pqAccountDetailRepository.save(account);
                        vBankAccountDetailRepository.delete(vBankAccountDetail);
                        valid = true;
                    }
                }
            } catch (Exception e) {
                error.setMessage("Exception Occurred");
                valid = false;
            }
        }
        error.setValid(valid);
        return error;
    }

    public MRegistrationError validateOTP(MerchantKycDTO dto){
        MRegistrationError error = new MRegistrationError();
        boolean valid = true;
        if(!CommonValidation.isValidOTP(dto.getOtp())){
            error.setMessage("Please enter valid otp");
            error.setValid(false);
            return error;
        }else {
            try {
                VBankAccountDetail vbank = vBankAccountDetailRepository.findByAccountNumber(SecurityUtil.sha1(dto.getAccountNumber()));
                if(vbank != null) {
                    if(vbank.getOtp().equals(dto.getOtp())) {
                        vbank.setOtp(null);
                        vbank.setStatus(Status.Active);
                        vBankAccountDetailRepository.save(vbank);
                        valid = true;
                        error.setMessage("Valid Input");
                    }else {
                        valid = false;
                        error.setMessage("OTP mismatch");
                    }
                }else {
                    valid = false;
                    error.setMessage("Account Not Linked");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        error.setValid(valid);
        return error;
    }

    public MRegistrationError validateGeneratePassword(MerchantRegisterDTO dto){
        MRegistrationError error =  new MRegistrationError();
        boolean valid = true;
        if(!CommonValidation.checkMerchantPassword(dto.getPassword())){
            error.setValid(false);
            error.setMessage("Password must be 6 digits long");
            return error;
        }else if(!dto.getPassword().equals(dto.getConfirmPassword())){
            error.setValid(false);
            error.setMessage("Please re enter password correctly");
            return error;
        }
        error.setValid(valid);
        return error;
    }
}
