package com.payqwikapp.validation;

import java.text.SimpleDateFormat;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.entity.User;
import com.payqwikapp.model.ForgotMpinDTO;
import com.payqwikapp.model.MpinChangeDTO;
import com.payqwikapp.model.MpinDTO;
import com.payqwikapp.model.error.ChangeMpinError;
import com.payqwikapp.model.error.ForgotMpinError;
import com.payqwikapp.model.error.MpinError;

public class MpinValidation {

	private final PasswordEncoder passwordEncoder;
	public MpinValidation(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public MpinError validateNewMpin(MpinDTO dto) {
		MpinError error = new MpinError();
		boolean valid = true;
		if (CommonValidation.isNull(dto.getNewMpin())) {
			error.setNewMpin("MPIN must not be Empty");
			valid = false;
		} else if (CommonValidation.isNull(dto.getConfirmMpin())) {
			error.setConfirmMpin("Please Re-Enter your MPIN");
			valid = false;
		} else if (!(dto.getNewMpin().equals(dto.getConfirmMpin()))) {
			error.setConfirmMpin("Both MPIN must be equal");
			valid = false;
		} else if (!(CommonValidation.isNumeric(dto.getNewMpin()))) {
			error.setNewMpin("MPIN must be numeric");
			valid = false;
		} else if (!(CommonValidation.checkValidMpin(dto.getNewMpin()))) {
			error.setNewMpin("MPIN must be 4 characters long");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	public ChangeMpinError validateChangeMpin(MpinChangeDTO dto) {
		ChangeMpinError error = new ChangeMpinError();
		boolean valid = true;
        if (CommonValidation.isNull(dto.getNewMpin())) {
			error.setNewMpin("enter your new MPIN");
			valid = false;
		} else if (CommonValidation.isNull(dto.getConfirmMpin())) {
			error.setConfirmMpin("Re Enter your new MPIN");
			valid = false;
		} else if (!CommonValidation.isNumeric(dto.getNewMpin())) {
			error.setNewMpin("MPIN must be in numeric form");
			valid = false;
		} else if (!CommonValidation.checkValidMpin(dto.getNewMpin())) {
			error.setNewMpin("MPIN but be 6 digits long");
			valid = false;
		} else if (!(dto.getNewMpin().equals(dto.getConfirmMpin()))) {
			error.setConfirmMpin("Both MPIN must be equal");
			valid = false;
		}
		error.setValid(valid);
		return error;

	}

	public ForgotMpinError checkError(ForgotMpinDTO dto,User u){
		ForgotMpinError error = new ForgotMpinError();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		boolean valid = true;
		String currentPassword = u.getPassword();

		String currentDateOfBirth = dateFormat.format(u.getUserDetail().getDateOfBirth());
		if(!(passwordEncoder.matches(dto.getPassword(),currentPassword))){
			valid = false;
			error.setPassword("Password doesn't match");
		}

		if(!(dto.getDateOfBirth().equals(currentDateOfBirth))){
			valid = false;
			error.setDateOfBirth("Date Of Birth doesn't match");
		}
		error.setValid(valid);
		return error;
	}
}
