package com.payqwikapp.validation;

import java.util.List;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.AgentDetail;
import com.payqwikapp.entity.LocationDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.AgentRequest;
import com.payqwikapp.model.ChangePasswordDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.VerifyEmailDTO;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.ForgotPasswordError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.VerifyEmailError;
import com.payqwikapp.repositories.AgentDetailRepository;
import com.payqwikapp.repositories.LocationDetailsRepository;
import com.payqwikapp.repositories.MerchantDetailsRepository;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.thirdparty.model.request.MerchantRegisterDTO;

public class RegisterValidation {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserDetailRepository userDetailRepository;
	private final LocationDetailsRepository locationDetailsRepository;
	private final MerchantDetailsRepository merchantDetailsRepository;
	private final IUserApi userApi;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final AgentDetailRepository agentDetailRepository;

	public RegisterValidation(UserRepository UserRepository, PasswordEncoder passwordEncoder,
			UserDetailRepository userDetailRepository, LocationDetailsRepository locationDetailsRepository,
			MerchantDetailsRepository merchantDetailsRepository,IUserApi userApi,
			PQAccountDetailRepository pqAccountDetailRepository,AgentDetailRepository agentDetailRepository) {
		this.userRepository = UserRepository;
		this.passwordEncoder = passwordEncoder;
		this.userDetailRepository = userDetailRepository;
		this.locationDetailsRepository = locationDetailsRepository;
		this.merchantDetailsRepository = merchantDetailsRepository;
		this.userApi = userApi;
		this.pqAccountDetailRepository = pqAccountDetailRepository;
		this.agentDetailRepository =agentDetailRepository;
	}

	public RegisterError validateNormalUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		/*if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}*/

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }
		 
	/*	 if(dto.getAccountNumber()==null || dto.getAccountNumber().isEmpty()){
			 error.setMessage("Account number required");
			 valid = false;
		 }
		 else if(CommonValidation.isValidNumber(dto.getAccountNumber())){
			 error.setMessage("Please enter valid account number");
			 valid = false;
		 }*/
//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			 error.setEmail("Please enter valid email address.");
			 error.setMessage("Please enter valid email address.");
			 valid = false; 
		 }else {
		if (!CommonValidation.isNull(dto.getEmail())) {
			List<UserDetail> userDetail = userDetailRepository.checkMail(dto.getEmail());
			if (userDetail != null) {
				for (UserDetail ud : userDetail) {
					User user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
							Status.Active, Status.Active);
					if (user != null) {
						valid = false;
						error.setEmail("Email already exists");
						error.setMessage("Email already exists");
					 }
				   }
				 }
			}
		}

		error.setValid(valid);
		return error;
	}

	public RegisterError validateEditUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;

		if (CommonValidation.isNull(dto.getFirstName())) {
			error.setFirstName("First Name Required");
			valid = false;
		} else if (!CommonValidation.containsAlphabets(dto.getFirstName())) {
			error.setFirstName("First Name must be alphabets only");
			valid = false;
		}
		if (dto.getAddress() == null || dto.getAddress().isEmpty()) {
			 error.setAddress("Address Required");
			 valid = false;
			}
		error.setValid(valid);
		return error;

	}
	
	public RegisterError checkNameError(RegisterDTO request) {
		RegisterError error = new RegisterError();
		boolean valid = true;

		if (CommonValidation.isNull(request.getFirstName())) {
			error.setFirstName("First Name Required");
			valid = false;
		} else if (!CommonValidation.containsAlphabets(request.getFirstName())) {
			error.setFirstName("First Name must be alphabets only");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	public ChangePasswordError validateChangePassword(ChangePasswordDTO dto) {
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;

		if (CommonValidation.isNull(dto.getNewPassword())) {
			error.setPassword("Insert Your New Password");
			valid = false;
		} else if (CommonValidation.isNull(dto.getConfirmPassword())) {
			error.setConfirmPassword("Please Re-Type Your Password");
			valid = false;
		} else if (dto.getPassword() != null && dto.getConfirmPassword() != null) {
			if (!dto.getPassword().equals(dto.getConfirmPassword())) {
				error.setConfirmPassword("Password Mis-match");
				valid = false;
			}
			if (CommonValidation.checkLength6(dto.getNewPassword())) {
				error.setNewPassword("Password must be atleast 6 characters long");
				valid = false;
			}
		}

		error.setValid(valid);
		return error;
	}

	public ChangePasswordError validateAdminPassword(ChangePasswordDTO dto) {
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;
		User user = userRepository.findByUsername(dto.getUsername());
		if (CommonValidation.isNull(dto.getPassword())) {
			error.setPassword("Please Enter Current Password");
			valid = false;
		}
		if (CommonValidation.isNull(dto.getNewPassword())) {
			error.setNewPassword("Please Enter New Password");
			valid = false;
		} else if (!CommonValidation.validateAdminPassword(dto.getNewPassword())) {
			error.setNewPassword("Password must be 10 digits long");
			valid = false;
		}
		if (CommonValidation.isNull(dto.getConfirmPassword())) {
			error.setConfirmPassword("Please Re enter New Password");
			valid = false;
		} else if (!dto.getNewPassword().equals(dto.getConfirmPassword())) {
			valid = false;
			error.setConfirmPassword("Both Passwords must match");
		}
		if (user != null) {
			if (!passwordEncoder.matches(dto.getPassword(), user.getPassword())) {
				valid = false;
				error.setPassword("Current Password you entered doesn\'t match");
			}
		}
		if (!dto.isRequest()) {
			if (CommonValidation.isNull(dto.getKey())) {
				error.setKey("Please Enter OTP");
				valid = false;
			} else if (!dto.getKey().equals(user.getMobileToken())) {
				error.setKey("Enter valid OTP");
				valid = false;
			}
		}
		error.setValid(valid);
		return error;
	}

	public ChangePasswordError validateChangePasswordDTO(ChangePasswordDTO dto) {
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;
		User u = userRepository.findByUsername(dto.getUsername());
		if (u != null) {
			System.err.println("password ::" + dto.getPassword());
			System.err.println(" backend password ::" + u.getPassword());
			if (!(passwordEncoder.matches(dto.getPassword(), u.getPassword()))) {
				error.setPassword("Enter your current password correctly");
				valid = false;
			}
		}
		error.setValid(valid);
		return error;
	}

	public ChangePasswordError renewPasswordValidation(ChangePasswordDTO dto) {
		System.err.println("change passowrd error");
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;
		User user = userRepository.findByMobileTokenAndUsername(dto.getKey(), dto.getUsername());
		if (user == null) {
			valid = false;
			error.setKey("The OTP you have entered is INCORRECT.");
			error.setUsername("Invalid user");
		}

		if (!(dto.getConfirmPassword().equals(dto.getNewPassword()))) {
			valid = false;
			error.setKey("Password Mis-Match");
		}
		error.setValid(valid);
		return error;
	}
	
	public ChangePasswordError agentRenewPasswordValidation(ChangePasswordDTO dto) {
		System.err.println("change passowrd error");
		ChangePasswordError error = new ChangePasswordError();
		boolean valid = true;

		if(dto.getConfirmPassword()==null || dto.getNewPassword()==null){
			valid = false;
			error.setKey("Please enter Password");
		}
		else if (!(dto.getConfirmPassword().equals(dto.getNewPassword()))) {
			valid = false;
			error.setKey("Password Mis-Match");
		}
		if(dto.getUsername()==null){
			valid = false;
			error.setKey("Username is empty");
		}
		error.setValid(valid);
		return error;
	}

	public ForgotPasswordError forgotPassword(String username) {
		ForgotPasswordError error = new ForgotPasswordError();
		if (username == null) {
			error.setErrorLength("Please enter Mobile Number");
			error.setValid(false);
			return error;
		} else if (!CommonValidation.checkLength10(username)) {
			error.setErrorLength("Mobile number must be 10 digits long");
			error.setValid(false);
			return error;
		}
		error.setValid(true);
		return error;

	}

	public VerifyEmailError checkMailError(VerifyEmailDTO email) {
		VerifyEmailError error = new VerifyEmailError();
		boolean valid = true;
		if (CommonValidation.isNull(email.getKey())) {
			error.setEmail("Please enter email in the field");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	public RegisterError validateMerchant(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				valid = false;
			}
		}

		if (dto.getContactNo() != null) {
			UserDetail userDetail = userDetailRepository.findByContactNo(dto.getContactNo());
			if (userDetail != null) {
				error.setContactNo("Contact No. already exists");
				valid = false;
			}

		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
			}
		}

		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setPassword("Password Required");
			valid = false;
		}

		// if (dto.getConfirmPassword() == null ||
		// dto.getConfirmPassword().isEmpty()) {
		// error.setConfirmPassword("Password Required");
		// valid = false;
		// } else {
		// if (!dto.getPassword().equals(dto.getConfirmPassword())) {
		// error.setConfirmPassword("Password Mis-Match");
		// valid = false;
		// }
		// }

		if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
			error.setContactNo("Contact No Required");
			valid = false;
		} else if (CommonValidation.isNumeric(dto.getContactNo())) {
			if (!CommonValidation.checkLength10(dto.getContactNo())) {
				error.setContactNo("Mobile number must be 10 digits long");
				valid = false;
			}
		} else {
			error.setContactNo("Please enter valid mobile number");
			valid = false;
		}

		if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			error.setFirstName("First Name Required");
			valid = false;
		}

		// if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
		// error.setLastName("Last Name Required");
		// valid = false;
		// }
		//
		if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
			error.setEmail("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setEmail("Please enter valid mail address");
			valid = false;
		} else {
			if (!CommonValidation.isNull(dto.getEmail())) {
				List<UserDetail> userDetail = userDetailRepository.checkMail(dto.getEmail());
				if (userDetail != null) {
					for (UserDetail ud : userDetail) {
						User user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(ud.getEmail(),
								Status.Active, Status.Active);
						if (user != null) {
							valid = false;
							error.setEmail("Email already exists");
						}
					}
					// }
				}
			}
		}
		error.setValid(valid);
		return error;

	}

	/* Agent Validation */
	public RegisterError validateAgent(AgentRequest dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
	 try{
		if (dto.getUserName() == null || dto.getUserName().isEmpty()) {
			error.setUsername("Username Required");

			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUserName().toLowerCase()), Status.Active);
			if (user != null) {
				error.setMessage("Mobile Number Already Exist");
				error.setUsername("Mobile Number Already Exist");
				error.setContactNo("User Already Exist");
				valid = false;
			}
		}
/*
		if (dto.getMobileNo() != null) {
			UserDetail userDetail = userDetailRepository.findByContactNo(dto.getMobileNo());
			if (userDetail != null) {
				error.setContactNo("Contact No. already exists");
				error.setMessage("Contact No. already exists");

				valid = false;
			}

		}*/
		if (dto.getPanNo() != null) {
			AgentDetail agentDetail = userRepository.findByAgentPannumber(dto.getPanNo());
			User us  = userRepository.findUserByAgentIdAndStatus(agentDetail,Status.Active);
			if (us != null) {
				error.setMessage("PanCard No. already exists");
				error.setPanCardNo("PanCard No. already exists");
				valid = false;
			}
		}
		
		if(dto.getAgentAccountNumber()!=null){
			AgentDetail agentId = agentDetailRepository.findUserbyAccountNumber(dto.getAgentAccountNumber());
			User user = userRepository.findUserByAgentIdAndStatus(agentId, Status.Active);
			if(user!=null){
				error.setMessage("Account number already exist");
				valid=false;
			}
			
		}

		if (CommonValidation.isNull(dto.getPinCode())) {
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
			valid = false;

		} else {
			/*
			 * LocationDetails locationDetails =
			 * locationDetailsRepository.findLocationByPin(dto.getPinCode());
			 * if(locationDetails == null) {
			 * 
			 * valid = false; error.setMessage("Not a valid Pincode");
			 * error.setLocationCode("Not a valid Pincode"); }
			 */
		}

		if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
			error.setMessage("Password Required");
			error.setPassword("Password Required");

			valid = false;
		}
		
		
		// if (dto.getConfirmPassword() == null ||
		// dto.getConfirmPassword().isEmpty()) {
		// error.setConfirmPassword("Password Required");
		// valid = false;
		// } else {
		// if (!dto.getPassword().equals(dto.getConfirmPassword())) {
		// error.setConfirmPassword("Password Mis-Match");
		// valid = false;
		// }
		// }
		
		/*if(dto.getAgentAccountName()==null || dto.getAgentAccountName().isEmpty()){
			error.setMessage("Account number required");
			valid=false;
		}
		else if(!CommonValidation.containsAlphabets(dto.getAgentAccountName())){
			valid=false;
			error.setMessage("Please enter valid account number");
		}*/
		
		if(dto.getAgentBankIfscCode()==null || dto.getAgentBankIfscCode().isEmpty()){
			error.setMessage("Bank IFSC code required");
			valid=false;
		}
		
		
		if(dto.getAgentAccountNumber()==null || dto.getAgentAccountNumber().isEmpty()){
			error.setMessage("Account number required");
			valid=false;
		}
		else if(!CommonValidation.isNumeric(dto.getAgentAccountNumber())){
			valid=false;
			error.setMessage("Please enter valid account number");
		}
		
		if(dto.getCity()==null || dto.getCity().isEmpty()){
			error.setMessage("City name required");
			valid=false;
		}
		else if(!CommonValidation.containsAlphabets(dto.getCity())){
			valid=false;
			error.setMessage("Please enter valid city name ");
		}
		
		if(dto.getState()==null || dto.getState().isEmpty()){
			error.setMessage("State name required");
			valid=false;
		}
		else if(!CommonValidation.containsAlphabets(dto.getState())){
			valid=false;
			error.setMessage("Please enter valid state name");
		}
		
		if(dto.getAddress1()==null || dto.getAddress1().isEmpty()){
			error.setMessage("Address required");
			valid=false;
		}
		else if(!CommonValidation.containsAlphabets(dto.getAddress1())){
			valid=false;
			error.setMessage("Please enter valid address");
		}
		
		if (dto.getMobileNo() == null || dto.getMobileNo().isEmpty()) {
			error.setMessage("Contact No Required");
			error.setContactNo("Contact No Required");
			valid = false;
		} else if (CommonValidation.isNumeric(dto.getMobileNo())) {
			if (!CommonValidation.checkLength10(dto.getMobileNo())) {
				error.setMessage("Mobile number must be 10 digits long");
				error.setContactNo("Mobile number must be 10 digits long");

				valid = false;
			}
		} else {
			error.setMessage("Please enter valid mobile number");
			error.setContactNo("Please enter valid mobile number");

			valid = false;
		}

		if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
			error.setMessage("First Name Required");
			error.setFirstName("First Name Required");
			valid = false;
		}

		// if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
		// error.setLastName("Last Name Required");
		// valid = false;
		// }
		
		if (dto.getEmailAddress() == null || dto.getEmailAddress().isEmpty()) {
			error.setEmail("Email Required");
			error.setMessage("Email Required");
			valid = false;
		} else if (!(CommonValidation.isValidMail(dto.getEmailAddress()))) {
			error.setEmail("Please enter valid mail address");
			error.setMessage("Please enter valid mail address");
			valid = false;
		} else {
			/*if (!CommonValidation.isNull(dto.getEmailAddress())) {

				List<UserDetail> userDetail1 = userDetailRepository.checkMail(dto.getEmailAddress());
				if (userDetail1 != null) {
					error.setMessage("Email already exists");
					valid = false;
				}
				// }
			}*/
		}
	 }
	 catch(Exception e){
		 System.out.println(e);
	 }
		error.setValid(valid);
		return error;

	}
	
	public RegisterError validateURegisterUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsername((dto.getUsername().toLowerCase()));
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }

//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 } else {

		if (!CommonValidation.isNull(dto.getEmail())) {
			List<UserDetail> userDetail = userDetailRepository.checkMail(dto.getEmail());
			if (userDetail != null) {
				for (UserDetail ud : userDetail) {
					User user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
							Status.Active, Status.Active);
					if (user != null) {
						valid = false;
						error.setEmail("Email already exists");
						error.setMessage("Email already exists");
					}
				}
				 }
			}
		}

		error.setValid(valid);
		return error;
	}
	
	public RegisterError validateRefernEarnUser(RegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			error.setMessage("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		
//		 if (dto.getConfirmPassword() == null ||
//		 dto.getConfirmPassword().isEmpty()) {
//		 error.setConfirmPassword("Password Required");
//		 error.setMessage
//		 valid = false;
//		 } else {
//		 if (!dto.getPassword().equals(dto.getConfirmPassword())) {
//		 error.setConfirmPassword("Password Mis-Match");
//		 error.setMessage
//		 valid = false;
//		 }
//		 }

		 if (dto.getContactNo() == null || dto.getContactNo().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getContactNo())) {
		 if (!CommonValidation.checkLength10(dto.getContactNo())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }

//		 if (dto.getFirstName() == null || dto.getFirstName().isEmpty()) {
//		 error.setFirstName("First Name Required");
//		 error.setMessage("First Name Required");
//		 valid = false;
//		 }
//		
//		 if (dto.getLastName() == null || dto.getLastName().isEmpty()) {
//		 error.setLastName("Last Name Required");
//		 error.setMessage("Last Name Required");
//		 valid = false;
//		 }
		
					User user = userRepository.findByUsername(dto.getUsername());
					if (user != null) {
						valid = false;
						error.setMessage("username already exists");
					}

		error.setValid(valid);
		return error;
	}
	
	
	public RegisterError validateMerchantUser(MerchantRegisterDTO dto) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (dto.getUsername() == null || dto.getUsername().isEmpty()) {
			error.setUsername("Username Required");
			valid = false;
		} else {
			User user = userRepository.findByUsernameAndStatus((dto.getUsername().toLowerCase()), Status.Active);
			if (user != null) {
				error.setUsername("Username Already Exist");
				error.setContactNo("User Already Exist");
				error.setMessage("User Already Exist");
				valid = false;
			}
		}

		if (CommonValidation.isNull(dto.getLocationCode())) {
			valid = false;
			error.setLocationCode("Pincode Required");
			error.setMessage("Pincode Required");
		} else {
			LocationDetails locationDetails = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
			if (locationDetails == null) {
				valid = false;
				error.setLocationCode("Not a valid Pincode");
				error.setMessage("Not a valid Pincode");
			}
		}

		 if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
		 error.setPassword("Password Required");
		 error.setMessage("Password Required");
		 valid = false;
		 }
		
		 if (dto.getMobileNumber() == null || dto.getMobileNumber().isEmpty()) {
		 error.setContactNo("Contact No Required");
		 error.setMessage("Contact No Required");
		 valid = false;
		 } else if (CommonValidation.isValidNumber(dto.getMobileNumber())) {
		 if (!CommonValidation.checkLength10(dto.getMobileNumber())) {
		 error.setContactNo("Mobile number must be 10 digits long");
		 error.setMessage("Mobile number must be 10 digits long");
		 valid = false;
		 }
		 } else {
		 error.setContactNo("Please enter valid mobile number");
		 error.setMessage("Please enter valid mobile number");
		 valid = false;
		 }
		 
		 if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
		 error.setEmail("Email Required");
		 error.setMessage("Email Required");
		 valid = false;
		 } else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
		 error.setEmail("Please enter valid mail address");
		 error.setMessage("Please enter valid mail address");
		 valid = false;
		 }else if(dto.getEmail().contains("@yopmail.com") || dto.getEmail().contains("@mailinator.com")){
			 error.setEmail("Please enter valid email address.");
			 error.setMessage("Please enter valid email address.");
			 valid = false; 
		 }else {
		if (!CommonValidation.isNull(dto.getEmail())) {
			List<UserDetail> userDetail = userDetailRepository.checkMail(dto.getEmail());
			if (userDetail != null) {
				for (UserDetail ud : userDetail) {
					User user = userRepository.findByUsernameAndMobileStatusAndEmailStatus(ud.getContactNo(),
							Status.Active, Status.Active);
					if (user != null) {
						valid = false;
						error.setEmail("Email already exists");
						error.setMessage("Email already exists");
					 }
				   }
				 }
			}
		}

		error.setValid(valid);
		return error;
	}
}