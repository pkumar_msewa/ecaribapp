package com.payqwikapp.validation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.payqwikapp.api.ICommissionApi;
import com.payqwikapp.api.ISavaariApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.DirectPaymentGetBalanceDTO;
import com.payqwikapp.model.LimitDTO;
import com.payqwikapp.model.SharePointDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TransactionDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PQServiceTypeRepository;
import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.RequestLogRepository;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.ConvertUtil;

public class TransactionValidation {
 
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final ICommissionApi commissionApi;
	private final RequestLogRepository requestLogRepository;
	private final ISavaariApi savaariApi;
	private final PQServiceRepository pqServiceRepository;
	private final PQTransactionRepository pqTransactionRepository;
	private final PQServiceTypeRepository pqServiceTypeRepository;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public TransactionValidation(IUserApi userApi, ITransactionApi transactionApi, ICommissionApi commissionApi,
			RequestLogRepository requestLogRepository,ISavaariApi savaariApi,PQServiceRepository pqServiceRepository,
			PQTransactionRepository pqTransactionRepository,PQServiceTypeRepository pqServiceTypeRepository) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.commissionApi = commissionApi;
		this.requestLogRepository = requestLogRepository;
		this.savaariApi=savaariApi;
		this.pqServiceRepository = pqServiceRepository;
		this.pqTransactionRepository = pqTransactionRepository;
		this.pqServiceTypeRepository = pqServiceTypeRepository;
	}


	public TransactionError validateGetUserBalance(DirectPaymentGetBalanceDTO dto){
		TransactionError error = new TransactionError();
		boolean valid = true;
		if(CommonValidation.isNull(dto.getUserName())) {
			valid = false;
			error.setMessage("enter mobile number");
		} else if (!CommonValidation.validateMobileNumber(dto.getUserName())) {
			valid = false;
			error.setMessage("enter valid mobile number");
		}
		error.setValid(valid);
		return error;

	}
	public TransactionError validateMerchantTransaction(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		double transactionAmount = Double.parseDouble(amount);
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			error.setCode("F01");
			return error;
		}
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(),UserType.Merchant.toString());
		double totalDebitMonthly = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
	
		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(),UserType.Merchant.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(),UserType.Merchant.toString());
	
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		
		
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			error.setCode("F02");
			return error;
		}
		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			error.setMessage(
					"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance());
			valid = false;
			error.setCode("F04");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F06");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalDebitMonthly, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F05");
			error.setValid(valid);
			return error;
		}
		
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}
		
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}
		
		error.setValid(valid);
		return error;
	}

	public TransactionError validateP2PTransaction(String amount, String senderUsername, String receiverUsername,
			PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		double totalCreditMonthlyReceiver = 0;
		double monthlyCreditLimitReceiver = 0;
		User senderUser = userApi.findByUserName(senderUsername);
		User receiverUser = userApi.findByUserName(receiverUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		if(senderUser.getAccountDetail().getAccountType().getCode().equalsIgnoreCase("NONKYC")){
			error.setMessage("Sender must be kyc to send money.");
			error.setValid(false);
			return error;
		}
		if(receiverUser.getAccountDetail().getAccountType().getCode().equalsIgnoreCase("NONKYC")){
			error.setMessage("Receiver must be kyc to receive money.");
			error.setValid(false);
			return error;
		}

		double balanceLimit = 0;
		if (receiverUser != null) {
			balanceLimit = receiverUser.getAccountDetail().getAccountType().getBalanceLimit();
			totalCreditMonthlyReceiver = transactionApi.getMonthlyCreditTransationTotalAmount(receiverUser.getAccountDetail());
			monthlyCreditLimitReceiver = receiverUser.getAccountDetail().getAccountType().getMonthlyLimit();
		}
		double transactionAmount = Double.parseDouble(amount);
		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.User.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.User.toString());
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDailyDebit = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(),UserType.User.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(),UserType.User.toString());
	
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}

		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setCode("T01");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDailyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your daily transaction limit is Rs. " + dailyTransactionLimit
					+ " and you already made total transaction of Rs." + totalDailyDebit);
			valid = false;
			error.setValid(valid);
			error.setCode("F00");
			return error;
		} else if ((receiverUser != null) && !(CommonValidation.receiverBalanceLimit(balanceLimit,
				receiverUser.getAccountDetail().getBalance(), transactionAmount))) {
			error.setMessage(
					"Balance Limit Exceeded. " + receiverUser.getUsername() + " balance limit is Rs." + balanceLimit);
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyCreditLimitReceiver, totalCreditMonthlyReceiver,
				transactionAmount)) {
			error.setMessage("Receiver Transaction Limit Exceeded.");
			valid = false;
			error.setValid(valid);
			return error;
		}

		/* TODO calculate time diff by transaction ref no */
		PQTransaction lastTransaction = transactionApi.getLastTransactionsOfUser(senderUser);
		if(lastTransaction != null) {
			  String transactionRefNo = lastTransaction.getTransactionRefNo();
				if(transactionRefNo.contains("FL")){
					transactionRefNo=transactionRefNo.substring(2);
				}else if(transactionRefNo.contains("VB")){
					transactionRefNo = transactionRefNo.substring(2);
				}
				transactionRefNo = transactionRefNo.substring(0, transactionRefNo.length()-1);
				
				long transactionTime = Long.parseLong(transactionRefNo);
				long currentTime = System.currentTimeMillis();
				/*TODO difference is computed with 60 seconds */
			    if(((currentTime-transactionTime)/1000) <= 60) {
			    	logger.info("inside current time below 60 seconds");
			    	error.setMessage("Please wait for 1 min ");
			    	error.setValid(false);
			    	return error;
			    } 
		}
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}
//		if (date != null) {
//			long lastTransactionTimeStamp = date.getTime();
//			long currentTimeInMillis = System.currentTimeMillis();
//			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
//				error.setMessage("Please wait for 1 min");
//				error.setValid(false);
//				return error;
//			}
//		}

		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}


		error.setValid(valid);
		return error;
	}

	public TransactionError validateBankTransfer(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		PQServiceType serviceType=pqServiceTypeRepository.findServiceTypeByName("Load Money");
		List<PQService> loadMoneyService = pqServiceRepository.findByServiceType(serviceType);
		List<PQTransaction> transaction=pqTransactionRepository.findLoadMoneyTransactions(senderUser.getAccountDetail(),loadMoneyService);
		if(transaction ==null){
			error.setMessage("You have to do loadmoney before send money to bank.");
			error.setValid(false);
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double	dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.User.toString());
		double dailyTransactionLimitBank=CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(),"Bank");
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(),UserType.User.toString());
		double monthlyTransactionLimitBank=CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(),"Bank");
		long dailyCountLimitBank = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(),"Bank");
		long monthlyCountLimitBank = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(),"Bank");
		
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		long totalMonthlyTransactionBank=userApi.monthlyTransactionTotalBank(senderUser);
		long totalDailyTransactionBank=userApi.dailyTransactionTotalBank(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalMonthlyDebitBank=transactionApi.getMonthlyDebitTransactionTotalAmountBank(senderUser.getAccountDetail());
		double totalDailyDebit = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDailyDebitBank=transactionApi.getDailyDebitTransactionTotalAmountBank(senderUser.getAccountDetail());
		
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		if(totalMonthlyTransactionBank > monthlyCountLimitBank){
			error.setMessage("Opps!! You can do only "+monthlyCountLimitBank+" transactions in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		
		if(totalDailyTransactionBank > dailyCountLimitBank){
			error.setMessage("Opps!! You can do only "+dailyCountLimitBank+" transactions in a day");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setCode("T01");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDailyDebit, transactionAmount)) {
			
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}else if(!CommonValidation.dailyLimitCheckBank(dailyTransactionLimitBank, totalDailyDebitBank, transactionAmount)){
			error.setMessage("Transaction Limit Exceeded. Your daily Bank transaction limit is Rs. " + dailyTransactionLimitBank
					+ " Your Requested Amount after service charges:Rs "+transactionAmount);
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}
		
		else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}
		
		else if (!CommonValidation.monthlyLimitCheckBank(monthlyTransactionLimitBank, totalMonthlyDebitBank, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your monthly transaction limit is Rs."
					+ monthlyTransactionLimitBank + " and you already made total transaction of Rs." + totalMonthlyDebitBank);
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}
		
		error.setValid(valid);
		return error;
	}

	public TransactionError validateOfflinePayment(double amount, User merchant, User user, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		if (merchant.getEmailStatus().equals(Status.Inactive)) {
			error.setMessage(
					"Please verify your email " + merchant.getUserDetail().getEmail() + " before any transaction");
			error.setValid(false);
			return error;
		}

		if (user.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage(
					"User " + user.getUsername() + " mobile number is not verified, hence not able to do transaction");
			error.setValid(false);
			return error;
		}

		double totalCreditMonthlyReceiver = transactionApi
				.getMonthlyCreditTransationTotalAmount(merchant.getAccountDetail());
		double monthlyCreditLimitReceiver = merchant.getAccountDetail().getAccountType().getMonthlyLimit();
		double transactionAmount = amount;
		double dailyTransactionLimit = user.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = user.getAccountDetail().getAccountType().getMonthlyLimit();
		double currentWalletBalance = user.getAccountDetail().getBalance();
		double totalDailyTransaction = userApi.dailyTransactionTotal(user);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(user);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(user.getAccountDetail());
		double totalDailyDebit = transactionApi.getDailyDebitTransactionTotalAmount(user.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(user.getUsername());
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}

		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		if (!CommonValidation.balanceCheck(user.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage("Insufficient Balance of Wallet . " + user.getUsername() + " current balance is Rs."
						+ user.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. of Wallet. " + user.getUsername() + " current balance is Rs."
						+ user.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDailyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded of Wallet User " + user.getUsername()
					+ ". Daily transaction limit is Rs. " + dailyTransactionLimit);
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded of Wallet User " + user.getUsername()
					+ ". Monthly transaction limit is Rs." + monthlyTransactionLimit);
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyCreditLimitReceiver, totalCreditMonthlyReceiver,
				transactionAmount)) {
			error.setMessage(
					"The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		}
		double currentBalance = user.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(user.getAccountDetail());

		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}

		Date dateOfUser = transactionApi.getLastTranasactionTimeStamp(user.getAccountDetail());
		if (dateOfUser != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(dateOfUser);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				// userApi.blockUser(user.getUsername());
				error.setValid(false);
				return error;
			}
		}
		if (dateOfUser != null) {
			long lastTransactionTimeStamp = dateOfUser.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}

		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(user.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Wallet User " + user.getUsername()
						+ " last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		error.setValid(valid);
		return error;

	}

	public TransactionError validateSharePoints(SharePointDTO dto) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		User senderUser = userApi.findByUserName(dto.getSenderUsername());
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your contactNo " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		if (senderUser.getAccountDetail().getPoints() < dto.getPoints()) {
			error.setMessage("You don't have sufficient points to do that transaction");
			error.setValid(false);
			return error;
		}

		User receiverUser = userApi.findByUserName(dto.getReceiverUsername());
		// if (receiverUser.getEmailStatus().equals(Status.Inactive)
		// && receiverUser.getMobileStatus().equals(Status.Active)) {
		// error.setMessage("Receiver email is not verified. Please verify " +
		// senderUser.getUserDetail().getEmail()
		// + " before any transaction");
		// error.setValid(false);
		// return error;
		// }

		error.setValid(valid);
		return error;
	}

	public TransactionError validateEventPayment(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double transactionAmount = Double.parseDouble(amount);
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}
		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);

			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		
		error.setValid(valid);
		return error;
	}

	public TransactionError validateLoadMoneyTransaction(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		User senderUser = userApi.findByUserName(senderUsername);
		PQAccountDetail accountDetail = senderUser.getAccountDetail();
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		
		/*if (accountDetail.getAccountType().getCode().equalsIgnoreCase("NONKYC")) {
			error.setMessage("Dear customer as per RBI guidelines its mandatory to provide your KYC details to load money into your wallet.");
			error.setValid(false);
			return error;
		}*/
		
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.User.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.User.toString());
		
//		if (totalDailyTransaction >= dailyCountLimit) {
//			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
//			error.setValid(false);
//			error.setCode("F00");
//			return error;
//		}
//
//		if (totalMonthlyTransaction >= monthlyCountLimit) {
//			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
//			error.setValid(false);
//			error.setCode("F00");
//			return error;
//		}
		
		double transactionAmount = Double.parseDouble(amount);
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double balanceLimit = senderUser.getAccountDetail().getAccountType().getBalanceLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is $." + monthlyTransactionLimit
					+ " and you've already credited total of Rs." + totalCreditMonthly);
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.receiverBalanceLimit(balanceLimit, currentWalletBalance, transactionAmount)) {
			error.setMessage(
					"Balance Limit Exceeded. " + senderUser.getUsername() + " balance limit is $." + balanceLimit);
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}

		/*if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}*/
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}

	public TransactionError validateGenericTransaction(TransactionDTO dto) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		User senderUser = userApi.findByUserName(dto.getSenderUsername());
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			error.setCode("F01");
			return error;
		}
		double transactionAmount = dto.getAmount();
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(),UserType.Merchant.toString()); 
		
		
		double totalDebitMonthly = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());

		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			return error;
		}
		
		
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(dto.getSenderUsername());
			error.setCode("F02");
			return error;
		}

		if (totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			error.setCode("F02");
			return error;
		}
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			valid = false;
			error.setCode("F04");
			error.setValid(valid);
			return error;
		}
		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			error.setMessage("Insufficient Balance. Your current balance is Rs."
					+ senderUser.getAccountDetail().getBalance());
			valid = false;
			error.setValid(valid);
			error.setCode("T01");
			error.setSplitAmount(transactionAmount - senderUser.getAccountDetail().getBalance());
			return error;
		} else if (!CommonValidation.monthlyDebitLimitCheck(monthlyTransactionLimit, totalDebitMonthly,
				transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F05");
			error.setValid(valid);
			return error;
		}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		}
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				userApi.blockUser(dto.getSenderUsername());
				error.setValid(false);
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}
		
		error.setValid(valid);
		error.setCode("S00");
		return error;
	}

	public TransactionError validateGenericTransactionForAPI(TransactionDTO dto) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		User senderUser = userApi.findByUserName(dto.getSenderUsername());
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
			error.setValid(false);
			error.setCode("F01");
			return error;
		}
		double transactionAmount = dto.getAmount();
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double balanceLimit = senderUser.getAccountDetail().getAccountType().getBalanceLimit();
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDebitMonthly = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalCreditDaily = transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		
		
		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		
//		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
//		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
//		
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
//		
//		if (totalDailyTransaction >= dailyCountLimit) {
//			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
//			error.setValid(false);
//			error.setCode("F00");
//			return error;
//		}
//
//		if (totalMonthlyTransaction >= monthlyCountLimit) {
//			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
//			error.setValid(false);
//			error.setCode("F00");
//			return error;
//		}
		
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(dto.getSenderUsername());
			error.setCode("F02");
			return error;
		}
		if (totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			error.setCode("F02");
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
				transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F05");
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.receiverBalanceLimit(balanceLimit, currentWalletBalance, transactionAmount)) {
			error.setMessage(
					"Balance Limit Exceeded. " + senderUser.getUsername() + " balance limit is Rs." + balanceLimit);
			valid = false;
			error.setCode("F06");
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}
		
		error.setMessage("Transaction processed.. ");
		error.setCode("S00");
		error.setValid(valid);
		return error;
	}

	public LimitDTO getLimitValues(User user) {
		LimitDTO limits = new LimitDTO();
		double balanceLimit = user.getAccountDetail().getAccountType().getBalanceLimit();
		double dailyTransactionLimit = user.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = user.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(user.getAccountDetail());
		double totalDebitMonthly = transactionApi.getMonthlyDebitTransactionTotalAmount(user.getAccountDetail());
		limits.setBalanceLimit(ConvertUtil.formatDouble(balanceLimit));
		limits.setDailyLimit(ConvertUtil.formatDouble(dailyTransactionLimit));
		limits.setMonthlyLimit(ConvertUtil.formatDouble(monthlyTransactionLimit));
		limits.setCreditConsumed(ConvertUtil.formatDouble(totalCreditMonthly));
		limits.setDebitConsumed(ConvertUtil.formatDouble(totalDebitMonthly));
		return limits;
	}

	public TransactionError validateBillPayment(String amount, String senderUsername, PQCommission commission,double netCommissionValue) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());

		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}
		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);

			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			error.setCode("T01");
			error.setSplitAmount(transactionAmount-senderUser.getAccountDetail().getBalance());
			return error;
			
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			error.setCode("F00");
			return error;
		}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			error.setCode("F00");
			return error;
		}
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}else{
			error.setCode("F00");
			error.setMessage("Please contact customer care.Your transaction is blocked by Admin");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			return error;
		}
		error.setValid(valid);
		return error;
	}
	// -------------------------------GIFT CART-------------------------------\\

	public TransactionError validateGiftcart(String amount, String senderUsername, PQService service,PQCommission commission,double netCommissionValue) {
		TransactionError error = new TransactionError();
		boolean valid = true;

		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		
		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double transactionAmount = Double.parseDouble(amount);
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());

		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		
		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			System.out.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your monthly transaction limit is Rs."
					+ monthlyTransactionLimit + " and you already made total transaction of Rs." + totalMonthlyDebit);
			valid = false;
			error.setValid(valid);
			return error;
		}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your daily transaction limit is Rs. " + dailyTransactionLimit
					+ " and you already made total transaction of Rs." + totalDebitDaily);
			valid = false;
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		Date currentDate = new Date();
		if (date != null) {
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}

		// Date date =
		// transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}
	
	// -------------------------------SAVAARI-------------------------------\\
	
	public TransactionError validateSavaariCancellation(String amount, String senderUsername, PQService service,String bookingId) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		Date date = savaariApi.getLastTranasactionTimeByStatus(bookingId, Status.Booked);
		System.err.println("date ::" + date);
		Date currentDate = new Date();
        long diff = date.getTime() - currentDate.getTime();
		long diffHours = diff / (60 * 60 * 1000);
		if (date != null) {
			if (diffHours<=24) {
				error.setMessage("the time limit is crossed 24 hours");
				error.setValid(false);
				return error;
			}
		}
		
		error.setValid(valid);
		return error;
	}

	//-------------------------------Adlabs-------------------------------\\


	public TransactionError validateAdlabs(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;


		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage(
					"Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
			error.setValid(false);
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());

		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		
		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		System.err.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount))
		{
			System.out.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);

			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setCode("T01");
			error.setValid(valid);
			return error;
		}
		else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit,
				transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setCode("F00");
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		Date currentDate = new Date();
		if(date!=null){
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if(lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				userApi.blockUser(senderUsername);
				error.setValid(false);
				return error;
			}
		}



//			Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),Status.Reversed);
		if(reverseTransactionDate != null){
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if((currentTime - timestamp) < 1000 * 60 * 1){
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		error.setValid(valid);
		return error;
	}
	
	public TransactionError validateYuppTvTransaction(String amount, String senderUsername, String receiverUsername,
			PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		double totalCreditMonthlyReceiver = 0;
		double monthlyCreditLimitReceiver = 0;
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		User receiverUser = userApi.findByUserName(receiverUsername);
		// if (receiverUser.getEmailStatus().equals(Status.Inactive)
		// && receiverUser.getMobileStatus().equals(Status.Active)) {
		// error.setMessage("Receiver email is not verified. Please verify " +
		// receiverUser.getUserDetail().getEmail() + " before any transaction");
		// error.setValid(false);
		// return error;
		// }

		double balanceLimit = 0;
		if (receiverUser != null) {
			balanceLimit = receiverUser.getAccountDetail().getAccountType().getBalanceLimit();
			totalCreditMonthlyReceiver = transactionApi
					.getMonthlyCreditTransationTotalAmount(receiverUser.getAccountDetail());
			monthlyCreditLimitReceiver = receiverUser.getAccountDetail().getAccountType().getMonthlyLimit();
		}
		double transactionAmount = Double.parseDouble(amount);
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDailyDebit = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}

		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDailyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		} else if ((receiverUser != null) && !(CommonValidation.receiverBalanceLimit(balanceLimit,
				receiverUser.getAccountDetail().getBalance(), transactionAmount))) {
			error.setMessage(
					"Balance Limit Exceeded. " + receiverUser.getUsername() + " balance limit is Rs." + balanceLimit);
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyCreditLimitReceiver, totalCreditMonthlyReceiver,
				transactionAmount)) {
			error.setMessage("Receiver Transaction Limit Exceeded.");
			valid = false;
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				userApi.blockUser(senderUsername);
				error.setValid(false);
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}

		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}

		error.setValid(valid);
		return error;
	}


	public TransactionError validateImagicaOrder(double amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage(
					"Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
			error.setValid(false);
			return error;
		}
		double transactionAmount = amount;
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}
		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit,
				transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		}
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		System.err.println("DATE::::::"+date);
		Date currentDate = new Date();
		if(date!=null){
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if(lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),Status.Reversed);
		if(reverseTransactionDate != null){
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if((currentTime - timestamp) < 1000 * 60 * 1){
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if(currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}


	public TransactionError validateQwikrPayPayment(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		System.err.println("inside qwikrpay validation");
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}

		double transactionAmount = Double.parseDouble(amount);
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());

		System.out.println(transactionAmount + "\n*" + dailyTransactionLimit + "\n&" + monthlyTransactionLimit + "\n#"
				+ totalDailyTransaction + "\n!" + totalMonthlyTransaction + "\n@" + totalMonthlyDebit);
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			System.out.println(transactionAmount + "\n" + dailyTransactionLimit + "\n&" + monthlyTransactionLimit
					+ "\n" + totalDailyTransaction + "\n!" + totalMonthlyTransaction + "\n@" + totalMonthlyDebit);
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}
		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		System.err.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
		System.err.println("user's balance--"+senderUser.getAccountDetail().getBalance()+"/n");
		System.err.println("transactionAmount--"+transactionAmount+"/n");
		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			System.out.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
	
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		System.err.println("DATE::::::" + date);
		Date currentDate = new Date();
		if (date != null) {
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}

		// Date date =
		// transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		double currentBalance = senderUser.getAccountDetail().getBalance();

		System.err.println("currentBalance=====================================" + currentBalance);

		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		System.err.println("lastTransactionBalance ==================================== " + lastTransactionBalance);
		System.out.println(currentBalance + "\n" + lastTransactionBalance);
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated please contact customer care");
				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}
	
	
	
	public TransactionError validateEaseMyTrip(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		System.out.println("");
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage(
					"Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());


		double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
		
		long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
		long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
		
		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}
		
		if (totalDailyTransaction >= dailyCountLimit) {
			error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}

		if (totalMonthlyTransaction >= monthlyCountLimit) {
			error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}


		System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			error.setCode("F00");
			return error;
		}
		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		System.err.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount))
		{
			System.out.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);

			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			error.setCode("T01");
			error.setSplitAmount(transactionAmount-senderUser.getAccountDetail().getBalance());
			return error;
		}
		else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit,
				transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			error.setCode("F00");
			
			return error;
		}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
			error.setMessage("The amount you are requesting is more than your maximum payment limit");
			valid = false;
			error.setValid(valid);
			error.setCode("F00");
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		System.err.println("DATE::::::"+date);
		Date currentDate = new Date();
		if(date!=null){
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if(lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				userApi.blockUser(senderUsername);
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}



//			Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),Status.Reversed);
		if(reverseTransactionDate != null){
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if((currentTime - timestamp) < 1000 * 60 * 1){
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
		}
		error.setCode("S00");
		error.setValid(valid);
		return error;
	}
	
	public TransactionError refernEarnValidation(String amount, String receiverUsername, String senderUsername,PQService service) {
		
		TransactionError error = new TransactionError();
		boolean valid = true;
		double totalCreditMonthlyReceiver = 0;
		double monthlyCreditLimitReceiver = 0;
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		User receiverUser = userApi.findByUserName(receiverUsername);
		// if (receiverUser.getEmailStatus().equals(Status.Inactive)
		// && receiverUser.getMobileStatus().equals(Status.Active)) {
		// error.setMessage("Receiver email is not verified. Please verify " +
		// receiverUser.getUserDetail().getEmail() + " before any transaction");
		// error.setValid(false);
		// return error;
		// }

		double balanceLimit = 0;
		if (receiverUser != null) {
			balanceLimit = receiverUser.getAccountDetail().getAccountType().getBalanceLimit();
			totalCreditMonthlyReceiver = transactionApi
					.getMonthlyCreditTransationTotalAmount(receiverUser.getAccountDetail());
			monthlyCreditLimitReceiver = receiverUser.getAccountDetail().getAccountType().getMonthlyLimit();
		}
		double transactionAmount = Double.parseDouble(amount);
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
		double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
		double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		double totalDailyDebit = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}

		PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
		double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
		if (commission.getType().equalsIgnoreCase("POST")) {
			transactionAmount = transactionAmount + netCommissionValue;
		}

		if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
			if (commission.getType().equalsIgnoreCase("POST")) {
				error.setMessage(
						"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
								+ " and service charges is Rs." + netCommissionValue);
			} else {
				error.setMessage("Insufficient Balance. Your current balance is Rs."
						+ senderUser.getAccountDetail().getBalance());
			}
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDailyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your daily transaction limit is Rs. " + dailyTransactionLimit
					+ " and you already made total transaction of Rs." + totalDailyDebit);
			valid = false;
			error.setValid(valid);
			return error;
		} else if ((receiverUser != null) && !(CommonValidation.receiverBalanceLimit(balanceLimit,
				receiverUser.getAccountDetail().getBalance(), transactionAmount))) {
			error.setMessage(
					"Balance Limit Exceeded. " + receiverUser.getUsername() + " balance limit is Rs." + balanceLimit);
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
			error.setMessage("Transaction Limit Exceeded. Your monthly transaction limit is Rs."
					+ monthlyTransactionLimit + " and you already made total transaction of Rs." + totalMonthlyDebit);
			valid = false;
			error.setValid(valid);
			return error;
		} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyCreditLimitReceiver, totalCreditMonthlyReceiver,
				transactionAmount)) {
			error.setMessage("Receiver Transaction Limit Exceeded.");
			valid = false;
			error.setValid(valid);
			return error;
		}

		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		if (date != null) {
			Date currentDate = new Date();
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				userApi.blockUser(senderUsername);
				error.setValid(false);
				return error;
			}
		}
		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}

		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}

		double currentBalance = senderUser.getAccountDetail().getBalance();
		double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
		if (currentBalance >= 0 && lastTransactionBalance >= 0) {
			if (currentBalance != lastTransactionBalance) {
				error.setMessage("Transaction is not validated");
				error.setValid(false);
				return error;
			}
		}

		error.setValid(valid);
		return error;

			}

	
	//validation for flight EBS
	
	
	public TransactionError validateFlightEBSTransaction(String amount, String senderUsername, PQService service) {
		TransactionError error = new TransactionError();
		boolean valid = true;
		User senderUser = userApi.findByUserName(senderUsername);
		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			error.setValid(false);
			return error;
		}
		PQAccountDetail accountDetail=senderUser.getAccountDetail();
		if (accountDetail.getAccountType().getCode().equalsIgnoreCase("NONKYC")){
			error.setMessage("Dear customer as per RBI guidelines its mandatory to provide your KYC details to load money into your wallet.");
			error.setValid(false);
			return error;
		}
		double transactionAmount = Double.parseDouble(amount);
		double currentWalletBalance = senderUser.getAccountDetail().getBalance();
		double balanceLimit = senderUser.getAccountDetail().getAccountType().getBalanceLimit();
		double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
		double totalMonthlyTransaction = userApi.monthlyLoadMoneyTransactionTotal(senderUser);
		double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
		double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
		if (currentWalletBalance < 0) {
			error.setMessage("Your account is locked, contact customer care");
			error.setValid(false);
			userApi.blockUser(senderUsername);
			error.setCode("F02");
			return error;
		}

		if (totalMonthlyTransaction < 0) {
			error.setMessage("Opps!! Unable to process transaction.");
			error.setValid(false);
			return error;
		}

		if (senderUser.getMobileStatus().equals(Status.Inactive)) {
			error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
					+ " before any transaction");
			valid = false;
			error.setValid(valid);
			return error;
		} 
		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
		System.err.println(" DATE:::::::::::" + date);
		if (date != null) {
			Date currentDate = new Date();
			System.err.println("CURRENT DATE:::::::::::::::::::" + currentDate);
			String lastTransactionDate = sdf.format(date);
			String currentTransactionDate = sdf.format(currentDate);
			if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
				error.setMessage("Please wait for current transaction to complete");
				error.setValid(false);
				return error;
			}
		}

		if (date != null) {
			long lastTransactionTimeStamp = date.getTime();
			long currentTimeInMillis = System.currentTimeMillis();
			if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
				error.setMessage("Please wait for 1 min");
				error.setValid(false);
				return error;
			}
		}
		Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
				Status.Reversed);
		if (reverseTransactionDate != null) {
			long timestamp = reverseTransactionDate.getTime();
			long currentTime = System.currentTimeMillis();
			if ((currentTime - timestamp) < 1000 * 60 * 1) {
				error.setMessage("Your last transaction was reversed. Please try again after 1 min");
				error.setValid(false);
				return error;
			}
		}
		error.setValid(valid);
		return error;
	}
	
	/** VALIDATE REDEEMBALANCE **/
	   public TransactionError validateRedeemBalance(Double transactionAmount, String senderUsername){
		   
		   TransactionError error = new TransactionError();
		   boolean valid=true;
		 //  double transactionAmount = Double.parseDouble(amount);
			User senderUser = userApi.findByUserName(senderUsername);
			if (senderUser.getMobileStatus().equals(Status.Inactive)) {
				error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
						+ " before any transaction");
				error.setValid(false);
				error.setCode("F01");
				return error;
			}
			double currentWalletBalance = senderUser.getAccountDetail().getBalance();
			double balanceLimit = senderUser.getAccountDetail().getAccountType().getBalanceLimit();
			double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
			double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
			double totalMonthlyTransaction = userApi.monthlyLoadMoneyTransactionTotal(senderUser);
			double totalCreditMonthly = transactionApi.getMonthlyCreditTransationTotalAmount(senderUser.getAccountDetail());
			double totalDebitMonthly = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
			double totalCreditDaily = transactionApi.getDailyCreditTransationTotalAmount(senderUser.getAccountDetail());
			double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
			System.err.print("Total Credit Monthly ::" + totalCreditMonthly);
			System.err.println("Total Debit Monthly ::" + totalDebitMonthly);
			System.err.println("Total Credit Daily ::" + totalCreditDaily);
			System.err.println("Total Debit Daily ::" + totalDebitDaily);
			if (currentWalletBalance < 0) {
				error.setMessage("Your account is locked, contact customer care");
				error.setValid(false);
				userApi.blockUser(senderUsername);
				error.setCode("F02");
				return error;
			}
			if (totalMonthlyTransaction < 0) {
				error.setMessage("Opps!! Unable to process transaction.");
				error.setValid(false);
				error.setCode("F02");
				return error;
			} else if (!CommonValidation.monthlyCreditLimitCheck(monthlyTransactionLimit, totalCreditMonthly,
					transactionAmount)) {
				error.setMessage("Monthly Credit Limit Exceeded. Your monthly limit is Rs." + monthlyTransactionLimit
						+ " and you've already by credited Rs." + totalCreditMonthly);
				valid = false;
				error.setCode("F05");
				error.setValid(valid);
				return error;
			} else if (!CommonValidation.receiverBalanceLimit(balanceLimit, currentWalletBalance, transactionAmount)) {
				error.setMessage(
						"Balance Limit Exceeded. " + senderUser.getUsername() + " balance limit is Rs." + balanceLimit);
				valid = false;
				error.setCode("F06");
				error.setValid(valid);
				return error;
			}
			
	        Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			if (date != null) {
				Date currentDate = new Date();
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					userApi.blockUser(senderUsername);
					error.setValid(false);
					return error;
				}
			}
			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait for 1 min");
					error.setValid(false);
					return error;
				}
			}
			
			Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
					Status.Reversed);
			if (reverseTransactionDate != null) {
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if ((currentTime - timestamp) < 1000 * 60 * 1) {
					error.setMessage("Your last transaction was reversed. Please try again after 1 min");
					error.setValid(false);
					return error;
				}
			}

			double currentBalance = senderUser.getAccountDetail().getBalance();
			double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
			if (currentBalance >= 0 && lastTransactionBalance >= 0) {
				if (currentBalance != lastTransactionBalance) {
					error.setMessage("Transaction is not validated");
					error.setValid(false);
					return error;
				}
			}
			
			if(CommonValidation.isNull(senderUsername)) {
				valid = false;
				error.setMessage("enter mobile number");
			} else if (!CommonValidation.validateMobileNumber(senderUsername)) {
				valid = false;
				error.setMessage("enter valid mobile number");
			}
			return error;
		    
	 }
	   
	 //Travelkhana 
	   public TransactionError validatePlaceOrder(double TotalCustomerPayable , String senderUsername, PQService service) {
			TransactionError error = new TransactionError();
			boolean valid = true;


			User senderUser = userApi.findByUserName(senderUsername);
			if (senderUser.getMobileStatus().equals(Status.Inactive)) {
				error.setMessage(
						"Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
				error.setValid(false);
				return error;
			}
			System.err.println("Amount:::::::::::::::::"+TotalCustomerPayable);
			double transactionAmount =TotalCustomerPayable;
			double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());


			double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
			long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			
			long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
			long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
			
			double currentBalance = senderUser.getAccountDetail().getBalance();
			double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
			if(currentBalance >= 0 && lastTransactionBalance >= 0) {
				if (currentBalance != lastTransactionBalance) {
					error.setMessage("Transaction is not validated");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}
			
			if (totalDailyTransaction >= dailyCountLimit) {
				error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}

			if (totalMonthlyTransaction >= monthlyCountLimit) {
				error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}

			
			System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
			if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
				System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
				error.setMessage("Opps!! Unable to process transaction.");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
			PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
			double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
			if (commission.getType().equalsIgnoreCase("POST")) {
				transactionAmount = transactionAmount + netCommissionValue;
			}

			System.err.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);

			if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount))
			{
				System.out.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);
				if (commission.getType().equalsIgnoreCase("POST")) {
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ senderUser.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);

				} else {
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ senderUser.getAccountDetail().getBalance());
				}
				valid = false;
				error.setCode("T01");
				error.setValid(valid);
				return error;
			}
			else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit,
					transactionAmount)) {
				error.setMessage("The amount you are requesting is more than your maximum payment limit");
				valid = false;
				error.setCode("F00");
				error.setValid(valid);
				return error;
			}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
				error.setMessage("The amount you are requesting is more than your maximum payment limit");
				valid = false;
				error.setCode("F00");
				error.setValid(valid);
				return error;
			}

			Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			System.err.println("DATE::::::"+date);
			Date currentDate = new Date();
			if(date!=null){
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if(lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete.Try again after some time");
			//		userApi.blockUser(senderUsername);
					error.setValid(false);
					return error;
				}
			}

//				Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait for 1 min");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}
			Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),Status.Reversed);
			if(reverseTransactionDate != null){
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if((currentTime - timestamp) < 1000 * 60 * 1){
					error.setMessage("Your last transaction was reversed. Please try again after 1 min");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}

			error.setValid(valid);
			return error;
		}

	   
	   
//	   HouseJoy
	   
	   public TransactionError validateHouseJoyPayment(String amount, String senderUsername, PQService service) {
			TransactionError error = new TransactionError();
			boolean valid = true;
			System.err.println("inside HouseJoy validation");
			User senderUser = userApi.findByUserName(senderUsername);
			if (senderUser.getMobileStatus().equals(Status.Inactive)) {
				error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
						+ " before any transaction");
				error.setValid(false);
				return error;
			}

			double currentBalance = senderUser.getAccountDetail().getBalance();
			double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
			if (currentBalance >= 0 && lastTransactionBalance >= 0) {
				if (currentBalance != lastTransactionBalance) {
					error.setMessage("Transaction is not validated,please contact customer care");
					error.setValid(false);
					return error;
				}
			}
			double transactionAmount = Double.parseDouble(amount);
			double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
			double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
			double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
			double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
			double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());

			System.out.println(transactionAmount + "\n*" + dailyTransactionLimit + "\n&" + monthlyTransactionLimit + "\n#"
					+ totalDailyTransaction + "\n!" + totalMonthlyTransaction + "\n@" + totalMonthlyDebit);
			if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
				System.out.println(transactionAmount + "\n" + dailyTransactionLimit + "\n&" + monthlyTransactionLimit
						+ "\n" + totalDailyTransaction + "\n!" + totalMonthlyTransaction + "\n@" + totalMonthlyDebit);
				error.setMessage("Opps!! Unable to process transaction.");
				error.setValid(false);
				return error;
			}
			PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
			double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
			if (commission.getType().equalsIgnoreCase("POST")) {
				transactionAmount = transactionAmount + netCommissionValue;
			}

			System.err.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
			System.err.println("user's balance--"+senderUser.getAccountDetail().getBalance()+"/n");
			System.err.println("transactionAmount--"+transactionAmount+"/n");
			if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
				System.out.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
				if (commission.getType().equalsIgnoreCase("POST")) {
					error.setMessage(
		
							"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
									+ " and service charges is Rs." + netCommissionValue);
				} else {
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ senderUser.getAccountDetail().getBalance());
				}
				valid = false;
				error.setCode("T01");
				error.setValid(valid);
				return error;
			} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
				error.setMessage("The amount you are requesting is more than your maximum payment limit");
				valid = false;
				error.setValid(valid);
				return error;
			}

			Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			System.err.println("DATE::::::" + date);
			Date currentDate = new Date();
			if (date != null) {
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					error.setValid(false);
					return error;
				}
			}

			// Date date =
			// transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait for 1 min");
					error.setValid(false);
					return error;
				}
			}
			Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
					Status.Reversed);
			if (reverseTransactionDate != null) {
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if ((currentTime - timestamp) < 1000 * 60 * 1) {
					error.setMessage("Your last transaction was reversed. Please try again after 1 min");
					error.setValid(false);
					return error;
				}
			}

			
			error.setValid(valid);
			return error;
		}


	 /* WOOHOO VALIDATION*/
	   
	   public TransactionError validateWoohoo(String amount, String senderUsername, PQService service,PQCommission commission,double netCommissionValue) {
			TransactionError error = new TransactionError();
			boolean valid = true;

			User senderUser = userApi.findByUserName(senderUsername);
			if (senderUser.getMobileStatus().equals(Status.Inactive)) {
				error.setMessage("Please verify your mobile " + senderUser.getUserDetail().getContactNo()
						+ " before any transaction");
				error.setValid(false);
				return error;
			}
			double currentBalance = senderUser.getAccountDetail().getBalance();
			double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
			if (currentBalance >= 0 && lastTransactionBalance >= 0) {
				if (currentBalance != lastTransactionBalance) {
					error.setMessage("Transaction is not validated");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}
			
			double transactionAmount = Double.parseDouble(amount);
			double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());
			double dailyTransactionLimit = CommonUtil.getDailyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			double monthlyTransactionLimit = CommonUtil.getMonthlyTransactionLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			double totalDebitDaily = transactionApi.getDailyDebitTransactionTotalAmount(senderUser.getAccountDetail());
			long dailyCountLimit = CommonUtil.getDailyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			long monthlyCountLimit = CommonUtil.getMonthlyCountLimit(senderUser.getAccountDetail(), UserType.Merchant.toString());
			
			long totalMonthlyTransaction = userApi.getMonthlyTransactionCount(senderUser);
			long totalDailyTransaction = userApi.getDailyTransactionCount(senderUser);
			
			if (totalDailyTransaction >= dailyCountLimit) {
				error.setMessage("You can not do more than "+dailyCountLimit+ " transaction in a day");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}

			if (totalMonthlyTransaction >= monthlyCountLimit) {
				error.setMessage("You can not do more than "+monthlyCountLimit+ " transaction in a month");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
			
			if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
				error.setMessage("Opps!! Unable to process transaction.");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
			if (commission.getType().equalsIgnoreCase("POST")) {
				transactionAmount = transactionAmount + netCommissionValue;
			}

			if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount)) {
				System.out.println(senderUser.getAccountDetail().getBalance() + "&&&&&&/n" + senderUsername);
				if (commission.getType().equalsIgnoreCase("POST")) {
					error.setMessage(
							"Insufficient Balance. Your current balance is Rs." + senderUser.getAccountDetail().getBalance()
									+ " and service charges is Rs." + netCommissionValue);
				} else {
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ senderUser.getAccountDetail().getBalance());
				}
				valid = false;
				error.setCode("T01");
				error.setValid(valid);
				return error;
			} else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit, transactionAmount)) {
				error.setMessage("The amount you are requesting is more than your maximum payment limit");
				valid = false;
				error.setCode("F00");
				error.setValid(valid);
				return error;
			}else if (!CommonValidation.dailyLimitCheck(dailyTransactionLimit, totalDebitDaily, transactionAmount)) {
				error.setMessage("The amount you are requesting is more than your maximum payment limit");
				valid = false;
				error.setCode("F00");
				error.setValid(valid);
				return error;
			}

			Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			Date currentDate = new Date();
			if (date != null) {
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if (lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					error.setValid(false);
					return error;
				}
			}
			if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait for 1 min");
					error.setCode("F00");
					error.setValid(false);
					return error;
				}
			}
			Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),
					Status.Reversed);
			if (reverseTransactionDate != null) {
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if ((currentTime - timestamp) < 1000 * 60 * 1) {
					error.setMessage("Your last transaction was reversed. Please try again after 1 min");
					error.setValid(false);
					return error;
				}
			}
			error.setValid(valid);
			return error;
		}
	   
	   
	   
	   public TransactionError validateEaseMyTripSplit(String amount, String senderUsername, PQService service) {
			TransactionError error = new TransactionError();
			boolean valid = true;

			User senderUser = userApi.findByUserName(senderUsername);
			if (senderUser.getMobileStatus().equals(Status.Inactive)) {
				error.setMessage(
						"Please verify your mobile " + senderUser.getUserDetail().getContactNo()+ " before any transaction");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
			double currentBalance = senderUser.getAccountDetail().getBalance();
			double lastTransactionBalance = transactionApi.getLastSuccessTransaction(senderUser.getAccountDetail());
			if (currentBalance >= 0 && lastTransactionBalance >= 0) {
				if (currentBalance != lastTransactionBalance) {
					error.setMessage("Transaction is not validated");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}
			System.err.println("Amount:::::::::::::::::"+amount);
			double transactionAmount = Double.parseDouble(amount);
			double dailyTransactionLimit = senderUser.getAccountDetail().getAccountType().getDailyLimit();
			double monthlyTransactionLimit = senderUser.getAccountDetail().getAccountType().getMonthlyLimit();
			double totalDailyTransaction = userApi.dailyTransactionTotal(senderUser);
			double totalMonthlyTransaction = userApi.monthlyTransactionTotal(senderUser);
			double totalMonthlyDebit = transactionApi.getMonthlyDebitTransactionTotalAmount(senderUser.getAccountDetail());


			System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
			if (totalDailyTransaction < 0 || totalMonthlyTransaction < 0) {
				System.out.println(transactionAmount+"\n*"+dailyTransactionLimit+"\n&"+monthlyTransactionLimit+"\n#"+totalDailyTransaction+"\n!"+totalMonthlyTransaction+"\n@"+totalMonthlyDebit);
				error.setMessage("Opps!! Unable to process transaction.");
				error.setValid(false);
				error.setCode("F00");
				return error;
			}
			PQCommission commission = commissionApi.findCommissionByServiceAndAmount(service, transactionAmount);
			double netCommissionValue = commissionApi.getCommissionValue(commission, transactionAmount);
			if (commission.getType().equalsIgnoreCase("POST")) {
				transactionAmount = transactionAmount + netCommissionValue;
			}

			System.err.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);

			if (!CommonValidation.balanceCheck(senderUser.getAccountDetail().getBalance(), transactionAmount))
			{
				System.out.println(senderUser.getAccountDetail().getBalance()+"&&&&&&/n"+senderUsername);
				if (commission.getType().equalsIgnoreCase("POST")) {
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ senderUser.getAccountDetail().getBalance() + " and service charges is Rs." + netCommissionValue);

				} else {
					error.setMessage("Insufficient Balance. Your current balance is Rs."
							+ senderUser.getAccountDetail().getBalance());
				}
				valid = false;
				error.setValid(valid);
				error.setCode("T01");
				error.setSplitAmount(transactionAmount-senderUser.getAccountDetail().getBalance());
				return error;
			}
			else if (!CommonValidation.monthlyLimitCheck(monthlyTransactionLimit, totalMonthlyDebit,
					transactionAmount)) {
				error.setMessage("The amount you are requesting is more than your maximum payment limit");
				valid = false;
				error.setValid(valid);
				error.setCode("F00");
				
				return error;
			}

	/*		Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
			System.err.println("DATE::::::"+date);
			Date currentDate = new Date();
			if(date!=null){
				String lastTransactionDate = sdf.format(date);
				String currentTransactionDate = sdf.format(currentDate);
				if(lastTransactionDate.equalsIgnoreCase(currentTransactionDate)) {
					error.setMessage("Please wait for current transaction to complete");
					userApi.blockUser(senderUsername);
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}*/



//				Date date = transactionApi.getLastTranasactionTimeStamp(senderUser.getAccountDetail());
	/*		if (date != null) {
				long lastTransactionTimeStamp = date.getTime();
				long currentTimeInMillis = System.currentTimeMillis();
				if ((currentTimeInMillis - lastTransactionTimeStamp) < 1000 * 60 * 1) {
					error.setMessage("Please wait for 1 min");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}*/
			Date reverseTransactionDate = transactionApi.getLastTranasactionTimeStampByStatus(senderUser.getAccountDetail(),Status.Reversed);
			if(reverseTransactionDate != null){
				long timestamp = reverseTransactionDate.getTime();
				long currentTime = System.currentTimeMillis();
				if((currentTime - timestamp) < 1000 * 60 * 1){
					error.setMessage("Your last transaction was reversed. Please try again after 1 min");
					error.setValid(false);
					error.setCode("F00");
					return error;
				}
			}

			error.setCode("S00");
			error.setValid(valid);
			return error;
		}
}