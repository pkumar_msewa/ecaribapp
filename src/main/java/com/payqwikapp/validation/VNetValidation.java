package com.payqwikapp.validation;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.VNetDTO;
import com.payqwikapp.model.VNetError;
import com.payqwikapp.repositories.PQServiceRepository;

public class VNetValidation {
    private final PQServiceRepository pqServiceRepository;

    public VNetValidation(PQServiceRepository pqServiceRepository) {
        this.pqServiceRepository = pqServiceRepository;
    }
    public VNetError validateRequest(VNetDTO dto){
        VNetError error = new VNetError();
        boolean valid = true;
        PQService service = pqServiceRepository.findServiceByCode(dto.getServiceCode());
        if(service.getStatus().equals(Status.Inactive)) {
            valid = false;
            error.setAmount("Service is down for maintenance");
        }else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
                dto.getAmount())) {
            error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
                    + service.getMaxAmount());
            valid = false;
        }
        error.setValid(valid);
        return error;
    }
}
