package com.payqwikapp.validation;

import com.payqwikapp.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.MobileTopupDTO;
import com.payqwikapp.model.error.MobileTopupError;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.util.PayQwikUtil;

public class MobileTopupValidation {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public MobileTopupValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public MobileTopupError checkError(MobileTopupDTO mobile) {
		MobileTopupError error = new MobileTopupError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode(mobile.getServiceProvider());
		if(service.getStatus().equals(Status.Inactive)) {
			
			error.setMessage("Service is down for maintenance");
			valid = false;
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				mobile.getAmount())) {
			error.setAmount("Amount should be between "+PayQwikUtil.CURRENCY +" "+ service.getMinAmount() + " to "+PayQwikUtil.CURRENCY +" "+
					+ service.getMaxAmount());
			error.setMessage("Amount should be between "+PayQwikUtil.CURRENCY +" "+ service.getMinAmount() + " to "+PayQwikUtil.CURRENCY +" "+
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}
}
