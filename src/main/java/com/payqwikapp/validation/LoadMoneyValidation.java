package com.payqwikapp.validation;

import com.payqwikapp.model.error.LoadMoneyError;
import com.payqwikapp.model.LoadMoneyRequest;
public class LoadMoneyValidation {

	public LoadMoneyError checkError(double amount) {
		LoadMoneyError error = new LoadMoneyError();
		boolean valid = true;
//		if (!CommonValidation.isGreaterthan10(amount)) {
//			error.setAmount("Amount must be greater than 10");
//			valid = false;
//		}
		error.setValid(valid);
		return error;
	}
	
	public LoadMoneyError checkLoadMoneyError(LoadMoneyRequest request){
		boolean valid = true;
		LoadMoneyError error = new LoadMoneyError();
		if(CommonValidation.isNull(request.getVoucherNumber())){
			error.setAmount("Please Enter Voucher Number");
			valid = false;
		}else if(!CommonValidation.is16DigitVoucher(request.getVoucherNumber())){
			error.setAmount("Please Enter 16 Digit Voucher Number");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
}
