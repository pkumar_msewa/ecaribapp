package com.payqwikapp.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.ProcessGiftCardDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.WoohooTransactionRequest;
import com.payqwikapp.model.error.GiftCardError;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.util.StartupUtil;

public class GiftCardValidation {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PQServiceRepository pqServiceRepository;

	public GiftCardValidation(PQServiceRepository pqServiceRepository) {
		this.pqServiceRepository = pqServiceRepository;
	}

	public GiftCardError checkError(ProcessGiftCardDTO giftCard) {
		GiftCardError error = new GiftCardError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode(StartupUtil.GiftcartCode);
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				giftCard.getAmount())) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}
	
	public GiftCardError checkWoohooError(WoohooTransactionRequest giftCard) {
		GiftCardError error = new GiftCardError();
		boolean valid = true;
		PQService service = pqServiceRepository.findServiceByCode(StartupUtil.WOOHOO_SERVICECODE);
		if(service.getStatus().equals(Status.Inactive)) {
			valid = false;
			error.setAmount("Service is down for maintenance");
		}else if (!CommonValidation.isAmountInMinMaxRange(service.getMinAmount(), service.getMaxAmount(),
				String.valueOf(giftCard.getPrice()))) {
			error.setAmount("Amount should be between Rs. " + service.getMinAmount() + " to Rs. "
					+ service.getMaxAmount());
			valid = false;
		}

		error.setValid(valid);
		return error;
	}

}
