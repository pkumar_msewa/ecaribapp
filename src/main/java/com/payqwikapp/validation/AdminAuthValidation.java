package com.payqwikapp.validation;

import com.payqwikapp.model.KycLimitDTO;
import com.payqwikapp.model.error.KycLimitError;

public class AdminAuthValidation {

    public KycLimitError validateAccountLimits(KycLimitDTO dto){
        KycLimitError error = new KycLimitError();
        boolean valid = true;
        if(!CommonValidation.validateUppercase(dto.getAccountType())){
            error.setAccountType("Enter valid Account Type");
            valid = false;
        }
        if(dto.getBalanceLimit() <= 0) {
            error.setBalanceLimit("Balance Limit must be greater than 0");
            valid = false;
        }
        if(dto.getDailyLimit() <= 0){
            error.setDailyLimit("Daily Limit must be greater than 0");
            valid = false;
        }
        if(dto.getMonthlyLimit() <= 0) {
            valid = false;
            error.setMontlyLimit("Monthly Limit must be greater than 0");
        }
        if(dto.getTransactionLimit() <= 0) {
            valid = false;
            error.setTransactionLimit("Transaction Limit must be greater than 0");
        }
        error.setValid(valid);
        return error;
    }


}
