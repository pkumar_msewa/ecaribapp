package com.payqwikapp.validation;

import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.error.VerifyMobileOTPError;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;

public class MobileOTPValidation {

	private final UserRepository userRepository;
	private final UserDetailRepository userDetailRepository;

	public MobileOTPValidation(UserRepository userRepository,UserDetailRepository userDetailRepository) {
		this.userRepository = userRepository;
		this.userDetailRepository = userDetailRepository;
	}

	/**
	 * For OTP Validation
	 * 
	 * @param OTP
	 * @param phoneNO
	 * @return
	 */
	public VerifyMobileOTPError validateOTP(String OTP, String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;

		if (OTP.length() == 0) {
			error.setOtp("OTP Required");
			valid = false;
		}

		if (OTP.length() != 6) {
			error.setOtp("OTP is 6 digit");
			valid = false;
		}

		User u = userRepository.findByUsername(username);
		if (u != null) {
			if (u.getMobileStatus().equals(Status.Active)) {
				error.setOtp(username + " Mobile Number is already Verified");
				valid = false;
			}
		}
		error.setValid(valid);
		return error;
	}

	public VerifyMobileOTPError validateUserResendOTP(String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;

		User u = userRepository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			valid = true;
		} else {
			error.setOtp(username + " Mobile Number is already verified");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public VerifyMobileOTPError validateResendOTP(String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;

		User u = userRepository.findByUsernameAndStatus(username, Status.Inactive);
		if (u == null) {
			valid = true;
		} else {
			error.setOtp(username + " Mobile Number is already verified");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public VerifyMobileOTPError validateMerchantResendOTP(String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;
		UserDetail userDetail = userDetailRepository.findByContactNo(username);
		System.out.println(userDetail.getEmail());
		username=userDetail.getEmail();
		User u = userRepository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			valid = true;
		} else {
			error.setOtp(username + " Mobile Number is already verified");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public VerifyMobileOTPError validateResendOTPSDKUSer(String username) {
		VerifyMobileOTPError error = new VerifyMobileOTPError();
		boolean valid = true;

		User u = userRepository.findByUsernameAndStatus(username, Status.Active);
		if (u == null) {
			valid = true;
		} else {
			error.setOtp(username + " Mobile Number is already verified");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
}
