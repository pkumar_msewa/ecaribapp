
package com.payqwikapp.util;

public class DeploymentConstants {

//	public static final boolean PRODUCTION = true;
	public static final boolean PRODUCTION = false;
	public static final String WEB_URL = getWebUrl();
	
	private static final String LIVE_URL = "http://172.16.7.29";
//	private static final String TEST_URL = "http://23.101.178.130";
	private static final String TEST_URL = "http://localhost:8082";
	
	private static final String LIVE_URL_IMAGES = "https://www.vpayqwik.com";
	private static final String TEST_URL_IMAGES = "http://23.101.178.130";
	
	public static String getWebUrl(){
    	return(PRODUCTION ? LIVE_URL : TEST_URL);
	}
	
	public static String getWebUrlForImages(){
    	return(PRODUCTION ? LIVE_URL_IMAGES : TEST_URL_IMAGES);
	}
}
