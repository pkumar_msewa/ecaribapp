package com.payqwikapp.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.payqwikapp.entity.PPIFiles;
import com.payqwikapp.model.UploadResponse;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.*;
import java.net.SocketException;

public class FTPUtil {

    private final static String ALLIANZA_SERVER = "173.240.8.43";
    private final static int ALLIAZA_PORT = 21;
    private final static String ALLIANZA_USERNAME = "faspay";
    private final static String ALLIANZA_PASSWORD = "FasPay";
    
    private final static String PPI_SERVER = "172.16.241.173";
    private final static int PPI_PORT = 22;
    private final static String PPI_USERNAME = "ONUSMEITY";
    private final static String PPI_PASSWORD = "MEITY@321";


    public static UploadResponse uploadAllianzaFile(PPIFiles file) {
        UploadResponse result = new UploadResponse();
        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(ALLIANZA_SERVER, ALLIAZA_PORT);
            ftpClient.login(ALLIANZA_USERNAME, ALLIANZA_PASSWORD);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // APPROACH #1: uploads first file using an InputStream
            File firstLocalFile = new File(file.getPath());

            InputStream inputStream = new FileInputStream(firstLocalFile);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(file.getName(), inputStream);
            inputStream.close();
            if (done) {
                result.setSuccess(true);
                System.out.println("The first file is uploaded successfully.");
                result.setMessage(file.getName() +" is uploaded successfully.");
            } else {
                result.setMessage("File Failed to upload");
            }

        } catch (SocketException e) {
            e.printStackTrace();
            result.setMessage(e.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            result.setMessage(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            result.setMessage(e.getMessage());
        }
        return result;

    }
    
    public static UploadResponse uploadPPIFile(PPIFiles file,String date) {
        UploadResponse result = new UploadResponse();
        String SFTPHOST = PPI_SERVER;
        int SFTPPORT = PPI_PORT;
        String SFTPUSER = PPI_USERNAME;
        String SFTPPASS = PPI_PASSWORD;
        String SFTPWORKINGDIR = "OnUsMEITY/";

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            String absolutePath = ((ChannelSftp) channel).getHome() + "/" + SFTPWORKINGDIR;
            channelSftp.cd(absolutePath);
            File firstLocalFile = new File(file.getPath());
            InputStream inputStream = new FileInputStream(firstLocalFile);
            channelSftp.put(inputStream,file.getName());
            result.setSuccess(true);
            result.setMessage(file.getName() +" is uploaded successfully.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            result.setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage(e.getMessage());
        }
        finally{
            channelSftp.exit();
            System.out.println("sftp Channel exited.");
            channel.disconnect();
            System.out.println("Channel disconnected.");
            session.disconnect();
            System.out.println("Host Session disconnected.");
        }
		return result;
    }   
}
