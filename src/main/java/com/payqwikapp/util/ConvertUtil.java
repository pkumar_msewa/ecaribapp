package com.payqwikapp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.m2p.model.QRRequestDTO;
import com.m2p.model.VisaMerchantRequest;
import com.payqwikapp.entity.ABankTransfer;
import com.payqwikapp.entity.AadharDetails;
import com.payqwikapp.entity.AccessDetails;
import com.payqwikapp.entity.AuthUpdateLog;
import com.payqwikapp.entity.BankDetails;
import com.payqwikapp.entity.BankTransfer;
import com.payqwikapp.entity.BulkFile;
import com.payqwikapp.entity.LocationDetails;
import com.payqwikapp.entity.LoginLog;
import com.payqwikapp.entity.MBankTransfer;
import com.payqwikapp.entity.MerchantDetails;
import com.payqwikapp.entity.MerchantRefundRequest;
import com.payqwikapp.entity.Offers;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.POSMerchants;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.PQCommission;
import com.payqwikapp.entity.PQOperator;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQServiceType;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.PartnerDetail;
import com.payqwikapp.entity.PartnerInstall;
import com.payqwikapp.entity.PromoCode;
import com.payqwikapp.entity.PromoServices;
import com.payqwikapp.entity.SecurityQuestion;
import com.payqwikapp.entity.SenderBankTransferInfo;
import com.payqwikapp.entity.ServiceStatus;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.TelcoCircle;
import com.payqwikapp.entity.TelcoOperator;
import com.payqwikapp.entity.TelcoPlans;
import com.payqwikapp.entity.TreatCardCount;
import com.payqwikapp.entity.TreatCardDetails;
import com.payqwikapp.entity.TreatCardPlans;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.entity.VBankAccountDetail;
import com.payqwikapp.entity.VersionLogs;
import com.payqwikapp.model.AadharDetailsDTO;
import com.payqwikapp.model.AccountDTO;
import com.payqwikapp.model.AgentSendMoneyBankDTO;
import com.payqwikapp.model.BankTransferDTO;
import com.payqwikapp.model.BasicDTO;
import com.payqwikapp.model.BulkFileDTO;
import com.payqwikapp.model.FavouriteDTO;
import com.payqwikapp.model.Gender;
import com.payqwikapp.model.KycDTO;
import com.payqwikapp.model.LocationDTO;
import com.payqwikapp.model.LockUnlockDTO;
import com.payqwikapp.model.MTransactionResponseDTO;
import com.payqwikapp.model.MerchantDTO;
import com.payqwikapp.model.NearByMerchantsDTO;
import com.payqwikapp.model.NearMerchantDetailsDTO;
import com.payqwikapp.model.OffersDTO;
import com.payqwikapp.model.OnePayResponse;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.PartnerDetailDTO;
import com.payqwikapp.model.PayStoreDTO;
import com.payqwikapp.model.PromoCodeDTO;
import com.payqwikapp.model.PromoTransactionDTO;
import com.payqwikapp.model.RefundDTO;
import com.payqwikapp.model.RefundStatusDTO;
import com.payqwikapp.model.SecurityQuestionDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.ServiceStatusDTO;
import com.payqwikapp.model.ServiceType;
import com.payqwikapp.model.ServiceTypeDTO;
import com.payqwikapp.model.ServicesDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.TelcoCircleDTO;
import com.payqwikapp.model.TelcoOperatorDTO;
import com.payqwikapp.model.TelcoPlansDTO;
import com.payqwikapp.model.TransactionCommissionListDTO;
import com.payqwikapp.model.TransactionListDTO;
import com.payqwikapp.model.TransactionReport;
import com.payqwikapp.model.TreatCardPlansDTO;
import com.payqwikapp.model.TreatCardRegisterDTO;
import com.payqwikapp.model.UserByLocationDTO;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserSessionDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.UsernameListDTO;
import com.payqwikapp.model.VNetDTO;
import com.payqwikapp.model.VersionListDTO;
import com.payqwikapp.model.admin.AccessDTO;
import com.payqwikapp.model.admin.CommissionListDTO;
import com.payqwikapp.model.admin.ServiceListDTO;
import com.payqwikapp.model.admin.TListDTO;
import com.payqwikapp.model.admin.UserAccountInfo;
import com.payqwikapp.model.admin.UserBasicInfo;
import com.payqwikapp.model.admin.UserListDTO;
import com.payqwikapp.model.admin.UserLoginInfo;
import com.payqwikapp.model.admin.UserTransactionsInfo;
import com.payqwikapp.model.admin.report.WalletOutstandingDTO;
import com.payqwikapp.model.merchant.Merchant;
import com.payqwikapp.model.merchant.MerchantAddressDTO;
import com.payqwikapp.model.merchant.MerchantBankDTO;
import com.payqwikapp.model.merchant.MerchantBankDetail;
import com.payqwikapp.model.merchant.MerchantKycDTO;
import com.payqwikapp.model.merchant.MerchantRegisterDTO;
import com.payqwikapp.model.merchant.MerchantSignupDTO;
import com.payqwikapp.model.merchant.MerchantUID;
import com.payqwikapp.model.merchant.SettlementBankDTO;
import com.payqwikapp.model.merchant.SettlementBankDetail;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQCommissionRepository;
import com.payqwikapp.repositories.UserRepository;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.request.S2SRequest;
import com.thirdparty.model.request.StatusResponse;
import com.thirdparty.model.request.UpiAuthenticationDTO;
import com.upi.util.UPIBescomConstants;

public class ConvertUtil {

	private static long fileNum = 00;
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private final static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private final static SimpleDateFormat dobFormat = new SimpleDateFormat("dd MMM yyyy");
	private final static SimpleDateFormat transactionDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 public static final String APP_KEY = "AIzaSyDvi8WYlYf_IS1MExNMgF-9SasbY6JhdU8";
	public static final String RUPEE = "\u20B9";

	private ConvertUtil() {

	}

	public static MerchantRegisterDTO convertToMerchantDTO(User user){
		MerchantRegisterDTO registerDTO = new MerchantRegisterDTO();
		UserDetail detail  = user.getUserDetail();
		if(detail != null) {
			registerDTO.setMobileNumber(detail.getContactNo());
			registerDTO.setFirstName(detail.getFirstName());
			registerDTO.setEmail(detail.getEmail());
		}
		MerchantDetails merchantDetails = user.getMerchantDetails();
		if(merchantDetails != null) {
			MerchantAddressDTO addressDTO = new MerchantAddressDTO();
			addressDTO.setAddress(merchantDetails.getAddress());
			addressDTO.setPinCode(merchantDetails.getLocationCode());
			LocationDetails location = user.getUserDetail().getLocation();
			if(location != null) {
				addressDTO.setCity(location.getRegionName());
				addressDTO.setState(location.getStateName());
			}
			registerDTO.setAddressDTO(addressDTO);
			MerchantBankDTO bankDTO = new MerchantBankDTO();
			bankDTO.setAccountNumber(merchantDetails.getBankAccountNo());
			bankDTO.setAccountName(merchantDetails.getBankAccountName());
			bankDTO.setBankName(merchantDetails.getBankName());
			bankDTO.setIfscCode(merchantDetails.getIfscCode());
			bankDTO.setLocation(merchantDetails.getBankLocation());
			registerDTO.setMerchantBankDTO(bankDTO);
			SettlementBankDTO settlementBankDTO = new SettlementBankDTO();
			settlementBankDTO.setAccountNumber(merchantDetails.getBankAccountNo());
			settlementBankDTO.setAccountName(merchantDetails.getBankAccountName());
			settlementBankDTO.setBankName(merchantDetails.getBankName());
			settlementBankDTO.setIfscCode(merchantDetails.getIfscCode());
			settlementBankDTO.setLocation(merchantDetails.getBankLocation());
			registerDTO.setSettlementBankDTO(settlementBankDTO);
			MerchantUID uid = new MerchantUID();
			uid.setPanNumber(merchantDetails.getPanCardNo());
			registerDTO.setUidDTO(uid);
		}
		PQAccountDetail accountDetail = user.getAccountDetail();
		if(accountDetail != null) {
			VBankAccountDetail vbank = accountDetail.getvBankAccount();
			if(vbank != null) {
				MerchantKycDTO kyc = new MerchantKycDTO();
				kyc.setVbankCustomer(true);
				kyc.setAccountNumber(accountDetail.getVijayaAccountNumber());
				kyc.setMobileNumber(vbank.getMobileNumber());
				registerDTO.setKycDTO(kyc);
			}
		}
		return registerDTO;

	}

	public static AccessDetails convertFromAccessDTO(AccessDetails details,AccessDTO dto) {
		details.setUpdateLimits(dto.isUpdateLimits());
		details.setUpdateAuthUser(dto.isUpdateAuthUser());
		details.setUpdateAuthAgents(dto.isUpdateAuthAgents());
		details.setDeductMoney(dto.isDeductMoney());
		details.setLoadMoney(dto.isLoadMoney());
		details.setRequestMoney(dto.isRequestMoney());
		details.setSendGCM(dto.isSendGCM());
		details.setSendMail(dto.isSendGCM());
		details.setUpdateAuthAdmin(dto.isUpdateAuthAdmin());
		details.setUpdateAuthMerchants(dto.isUpdateAuthMerchants());
		details.setSendSMS(dto.isSendSMS());
		details.setSendMoney(dto.isSendMoney());
		return details;
	}


	public static AccessDTO convertFromAccessDetail(AccessDetails details) {
		AccessDTO dto = new AccessDTO();
		dto.setAuthority(details.getUser().getAuthority());
		dto.setUsername(details.getUser().getUsername());
		dto.setUpdateLimits(details.isUpdateLimits());
		dto.setUpdateAuthUser(details.isUpdateAuthUser());
		dto.setUpdateAuthAgents(details.isUpdateAuthAgents());
		dto.setDeductMoney(details.isDeductMoney());
		dto.setLoadMoney(details.isLoadMoney());
		dto.setRequestMoney(details.isRequestMoney());
		dto.setSendGCM(details.isSendGCM());
		dto.setSendMail(details.isSendGCM());
		dto.setUpdateAuthAdmin(details.isUpdateAuthAdmin());
		dto.setUpdateAuthMerchants(details.isUpdateAuthMerchants());
		dto.setSendSMS(details.isSendSMS());
		dto.setSendMoney(details.isSendMoney());
		return dto;
	}

	public static List<AccessDTO> convertFromList(List<AccessDetails> accessDetailsList){
		List<AccessDTO> list = new ArrayList<>();
		for(AccessDetails a: accessDetailsList){
			if(a != null) {
				list.add(convertFromAccessDetail(a));
			}
		}
		return list;
	}



	public static QRRequestDTO getFromCustomerId(String customerId){
		QRRequestDTO dto = new QRRequestDTO();
		dto.setUsername(customerId);
		return dto;
	}

	public static TListDTO getDTO(User u, PQTransaction t) {
		TListDTO dto = new TListDTO();
		dto.setAmount(String.format("%.2f", t.getAmount()));
		dto.setContactNo(u.getUsername());
		dto.setCurrentBalance(String.format("%.2f", t.getCurrentBalance()));
		dto.setEmail((u.getUserDetail().getEmail() != null) ? u.getUserDetail().getEmail() : "");
		dto.setUsername((u.getUserDetail().getFirstName() != null) ? u.getUserDetail().getFirstName() : "");
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setDateOfTransaction(transactionDate.format(t.getCreated()));
		dto.setDebit(String.valueOf(t.isDebit()));
		dto.setStatus(t.getStatus().getValue());
		dto.setDescription(t.getDescription());
		dto.setUpiId(t.getUpiId());
		dto.setSkdStatus(t.isSdkRequest());
		return dto;
	}

	public static List<PQAccountDetail> getAccounts(List<PQTransaction> transactions) {
		List<PQAccountDetail> accounts = new ArrayList<>();
		if (transactions != null && !transactions.isEmpty()) {
			for (PQTransaction t : transactions) {
				accounts.add(t.getAccount());
			}
		}
		return accounts;
	}


	public static MerchantSignupDTO convertFromPOSToDTO(POSMerchants posMerchants){
		MerchantSignupDTO dto = new MerchantSignupDTO();
		Merchant merchant = new Merchant();
		merchant.setMerchantName(posMerchants.getMerchantName());
		merchant.setFirstName(posMerchants.getMerchantName());
		merchant.setEmailAddress(posMerchants.getEmail());
		merchant.setPinCode(posMerchants.getPincode());
		merchant.setCity(posMerchants.getCity());
		merchant.setState(posMerchants.getState());
		merchant.setCountry(posMerchants.getCountry());
		merchant.setPanNo(posMerchants.getPanNo());
		merchant.setMobileNo("+91"+posMerchants.getMobileNumber());
		merchant.setAddress1(posMerchants.getAddress());
		merchant.setAddress2(" ");
//		merchant.setMerchantCategory(Integer.parseInt(posMerchants.getMcc()));
		dto.setMerchant(merchant);
		MerchantBankDetail bankDetail = new MerchantBankDetail();
		bankDetail.setAccName(posMerchants.getMerchantName());
		bankDetail.setBankAccNo(posMerchants.getAccountNumber());
		bankDetail.setBankIfscCode(posMerchants.getIfscCode());
		bankDetail.setBankName(posMerchants.getBankName());
		bankDetail.setBankLocation(posMerchants.getBankLocation());
		dto.setMerchantBankDetail(bankDetail);
		SettlementBankDetail settlementBankDetail = new SettlementBankDetail();
		settlementBankDetail.setAccName(posMerchants.getMerchantName());
		settlementBankDetail.setBankAccNo(posMerchants.getAccountNumber());
		settlementBankDetail.setBankIfscCode(posMerchants.getIfscCode());
		settlementBankDetail.setBankName(posMerchants.getBankName());
		settlementBankDetail.setBankLocation(posMerchants.getBankLocation());
		dto.setSettlementBankDetail(settlementBankDetail);
		return dto;
	}
	public static VisaMerchantRequest convertFromPOS(POSMerchants posMerchants){
		System.err.println("merchant::"+posMerchants);
		VisaMerchantRequest request = new VisaMerchantRequest();
		request.setCity(posMerchants.getCity());
		request.setAddress1(posMerchants.getAddress());
		request.setState(posMerchants.getState());
		request.setCountry(posMerchants.getCountry());
		request.setEmailAddress(posMerchants.getEmail());
		request.setBankAccountNo(posMerchants.getAccountNumber());
		request.setMerchantBankLocation(posMerchants.getBankLocation());
		request.setMerchantBankName(posMerchants.getBankName());
		request.setMerchantBankIfscCode(posMerchants.getIfscCode());
		request.setMerchantAccountName(posMerchants.getMerchantName());
		request.setMerchantName(posMerchants.getMerchantName());
		request.setFirstName(posMerchants.getMerchantName());
		request.setSettlementAccountNumber(posMerchants.getAccountNumber());
		request.setSettlementBankLocation(posMerchants.getBankLocation());
		request.setSettlementBankName(posMerchants.getBankName());
		request.setSettlementIfscCode(posMerchants.getIfscCode());
		request.setSettlementAccountName(posMerchants.getAccountName());
		request.setPinCode(posMerchants.getPincode());
		request.setPanNo(posMerchants.getPanNo());
		request.setLattitude("1234.23");
		request.setLongitude("1212.23");
		request.setMobileNo(posMerchants.getMobileNumber());
		return request;
	}

	public static UserBasicInfo convertFromUser(User user) {
		UserBasicInfo dto = new UserBasicInfo();
		if (user != null) {
			dto.setRegistrationDate(dateTimeFormat.format(user.getCreated()));
			dto.setUsername(user.getUsername());
			dto.setEmailStatus(user.getEmailStatus().getValue());
			dto.setMobileStatus(user.getMobileStatus().getValue());
			dto.setMobileToken(user.getMobileToken());
			dto.setAuthority(user.getAuthority());
			UserDetail detail = user.getUserDetail();
			if (detail != null) {
				dto.setName(detail.getFirstName());
				dto.setMobile(detail.getContactNo());
				dto.setEmail(detail.getEmail());
				dto.setGender(String.valueOf(detail.getGender()));
				dto.setImage(detail.getImage());
				dto.setDateOfBirth((detail.getDateOfBirth() != null) ? dobFormat.format(detail.getDateOfBirth()) : "");
				LocationDetails location = detail.getLocation();
				if (location != null) {
					dto.setPinCode(location.getPinCode());
					dto.setCircleName(location.getCircleName());
					dto.setStateName(location.getStateName());
			  }
			}
		}
		return dto;
	}

	public static UserAccountInfo getAccountFromEntity(PQAccountDetail account) {
		UserAccountInfo dto = new UserAccountInfo();
		if (account != null) {
			dto.setAccountNumber(String.valueOf(account.getAccountNumber()));
			dto.setBalance(String.format("%.2f", account.getBalance()));
			dto.setPoints(String.valueOf(account.getPoints()));
			dto.setVBankCustomer(account.isVBankCustomer());
			dto.setVijayaAccountNo(String.valueOf(account.getVijayaAccountNumber()));
			dto.setBranchCode(account.getBranchCode());
			PQAccountType type = account.getAccountType();
			if (type != null) {
				dto.setName(type.getName());
				dto.setDailyLimit(String.format("%.2f", type.getDailyLimit()));
				dto.setMonthlyLimit(String.format("%.2f", type.getMonthlyLimit()));
				dto.setBalanceLimit(String.format("%.2f", type.getBalanceLimit()));
				dto.setTransactionLimit(String.valueOf(type.getTransactionLimit()));
				dto.setDescription(type.getDescription());
				dto.setCode(type.getCode());
			}
			VBankAccountDetail vbank = account.getvBankAccount();
			if (vbank != null) {
				dto.setLinkedAccountMobile(vbank.getMobileNumber());
				dto.setLinkedAccountName(vbank.getAccountName());
			}
		}
		return dto;
	}

	public static UserTransactionsInfo convertFromEntity(PQTransaction transaction) {
		UserTransactionsInfo dto = new UserTransactionsInfo();
		if (transaction != null) {
			dto.setDescription(transaction.getDescription());
			dto.setAmount(String.format("%.2f", transaction.getAmount()));
			dto.setCommissionIdentifier(transaction.getCommissionIdentifier());
			dto.setCurrentBalance(String.format("%.2f", transaction.getCurrentBalance()));
			dto.setDate(transactionDate.format(transaction.getCreated()));
			dto.setDebit(transaction.isDebit());
			dto.setStatus(transaction.getStatus().getValue());
			dto.setFavourite(transaction.isFavourite());
			dto.setTransactionRefNo(transaction.getTransactionRefNo());
		}
		return dto;
	}

	public static List<String> getAlertMobileList() {
		List<String> destinations = new ArrayList<>();
		destinations.add("7022620747");
		destinations.add("9980672277");
		destinations.add("9760210393");
		return destinations;
	}

	public static UserLoginInfo convertFromLoginEntity(LoginLog login) {
		UserLoginInfo loginInfo = new UserLoginInfo();
		if (login != null) {
			loginInfo.setLoginTime(transactionDate.format(login.getCreated()));
			loginInfo.setDeviceId(login.getRemoteAddress());
			loginInfo.setStatus(login.getStatus().getValue());
		}
		return loginInfo;
	}

	public static List<UserLoginInfo> convertEntityFromLoginList(List<LoginLog> loginList) {
		List<UserLoginInfo> logs = new ArrayList<>();
		if (loginList != null && loginList.size() > 0) {
			for (LoginLog l : loginList) {
				logs.add(convertFromLoginEntity(l));
			}
		}
		return logs;
	}

	public static List<UserTransactionsInfo> convertFromEntityList(List<PQTransaction> transactions) {
		List<UserTransactionsInfo> list = new ArrayList<>();
		if (transactions != null && transactions.size() > 0) {
			for (PQTransaction transaction : transactions) {
				list.add(convertFromEntity(transaction));
			}
		}
		return list;
	}

	public static List<TListDTO> getListDTOs(List<User> users, List<PQTransaction> transactions) {
		List<TListDTO> lists = new ArrayList<>();
		if (transactions != null && !transactions.isEmpty()) {
			if (users != null && !users.isEmpty()) {
				for (PQTransaction t : transactions) {
					long accountNo = t.getAccount().getAccountNumber();
					for (User u : users) {
						long userAccountNo = u.getAccountDetail().getAccountNumber();
						if (accountNo == userAccountNo) {
							lists.add(getDTO(u, t));
						}
					}
				}
			}
		}
		return lists;
	}

	public static PayStoreDTO convertToPayStore(long id, String amount) {
		PayStoreDTO dto = new PayStoreDTO();
		dto.setId(id);
		dto.setNetAmount(Double.parseDouble(amount));
		return dto;
	}

	public static List<LockUnlockDTO> convertToList(List<AuthUpdateLog> logs) {
		List<LockUnlockDTO> nLogs = new ArrayList<>();
		for (AuthUpdateLog log : logs) {
			nLogs.add(convertToDTO(log));
		}
		return nLogs;
	}

	public static String createCommissionIdentifier(PQCommission commission) {
		String identifier = commission.getType() + "|" + commission.getValue() + "|" + commission.getMinAmount() + "|"
				+ commission.getMaxAmount() + "|" + commission.isFixed() + "|" + commission.getService().getCode();
		return identifier;
	}

	public static CommissionListDTO getCommissionDTO(PQCommission commission) {
		CommissionListDTO commissionDTO = new CommissionListDTO();
		commissionDTO.setIdentifier(commission.getIdentifier());
		commissionDTO.setFixed(String.valueOf(commission.isFixed()));
		commissionDTO.setValue(String.format("%.2f", commission.getValue()));
		commissionDTO.setMaxAmount(String.format("%.2f", commission.getMaxAmount()));
		commissionDTO.setMinAmount(String.format("%.2f", commission.getMinAmount()));
		return commissionDTO;
	}

	public static ServiceListDTO getDTOFrom(PQService s, List<PQCommission> commissionList) {
		ServiceListDTO services = new ServiceListDTO();
		try {
			List<CommissionListDTO> commList = new ArrayList<>();
			services.setDescription(s.getDescription());
			services.setCode(s.getCode());
			services.setStatus(s.getStatus());
			services.setMaxAmount(String.format("%.2f", s.getMaxAmount()));
			services.setMinAmount(String.format("%.2f", s.getMinAmount()));
			services.setName(s.getName());
			PQServiceType serviceType = s.getServiceType();
			if (serviceType != null) {
				services.setTypeDescription(serviceType.getDescription());
				services.setTypeName(serviceType.getName());
			}
			if (commissionList != null && !commissionList.isEmpty()) {
				for (PQCommission c : commissionList) {
					CommissionListDTO dto = new CommissionListDTO();
					dto.setMaxAmount(String.format("%.2f", c.getMaxAmount()));
					dto.setMinAmount(String.format("%.2f", c.getMinAmount()));
					dto.setFixed(String.valueOf(c.isFixed()));
					dto.setValue(String.format("%.2f", c.getValue()));
					dto.setIdentifier(c.getIdentifier());
					commList.add(dto);
				}
			}
			services.setList(commList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return services;
	}

	public static List<ServiceListDTO> getListForServices(List<PQService> services, PQCommissionRepository pqCommissionRepository) {
		List<ServiceListDTO> serviceListDTOs = new ArrayList<>();
		for (PQService s : services) {
			try {
				List<PQCommission> filteredList = pqCommissionRepository.findCommissionByService(s);
				serviceListDTOs.add(getDTOFrom(s, filteredList));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return serviceListDTOs;
	}

	public static List<UsernameListDTO> convertList(List<User> userList) {
		List<UsernameListDTO> list = new ArrayList<>();
		for (User user : userList) {
			list.add(convertToDTO(user));
		}
		return list;
	}

	public static UserListDTO convertToUserDTO(User u) {
		UserListDTO userList = new UserListDTO();
		UserDetail detail = u.getUserDetail();
		PQAccountDetail account = u.getAccountDetail();
		userList.setAuthority(u.getAuthority());
		userList.setRegistrationDate(dateTimeFormat.format(u.getCreated()));
		if (account != null) {
			userList.setVbankAccountNumber(
					(account.getVijayaAccountNumber() != null) ? account.getVijayaAccountNumber() : "");
			userList.setAccountType(account.getAccountType().getName());
			userList.setBalance(String.format("%.2f", account.getBalance()));
			userList.setPoints(String.format("%d", account.getPoints()));
		}
		if (detail != null) {
			userList.setGender((detail.getGender() != null) ? detail.getGender().getValue() : "");
			userList.setDateOfBirth(
					dobFormat.format((detail.getDateOfBirth() != null) ? detail.getDateOfBirth() : new Date()));
			userList.setName(detail.getFirstName());
			userList.setMobile(detail.getContactNo());
			userList.setEmail(detail.getEmail());
			LocationDetails location = detail.getLocation();
			if (location != null) {
				userList.setPinCode(location.getPinCode());
				userList.setCircleName(location.getCircleName());
			} else {
				userList.setPinCode("");
				userList.setCircleName("");
			}
		}
		return userList;
	}

	public static List<UserListDTO> getUserAuthority(List<User> users) {
		List<UserListDTO> list = new ArrayList<>();
		if (users != null && !users.isEmpty()) {
			for (User user : users) {
				list.add(convertToUserDTO(user));
			}
		}
		return list;
	}

	public static UsernameListDTO convertToDTO(User user) {
		UsernameListDTO dto = new UsernameListDTO();
		dto.setFirstName(user.getUserDetail().getFirstName());
		dto.setMobileNumber(user.getUsername());
		return dto;
	}

	public static LockUnlockDTO convertToDTO(AuthUpdateLog log) {

		LockUnlockDTO lockUnlockDTO = new LockUnlockDTO();
		lockUnlockDTO.setDate(dateTimeFormat.format(log.getCreated()));
		lockUnlockDTO.setId(String.valueOf(log.getId()));
		lockUnlockDTO.setStatus(log.getStatus().getValue());
		lockUnlockDTO.setUserDTO(convertUser(log.getUser()));
		lockUnlockDTO.setRequestType(String.valueOf(log.getRequestType()));
		return lockUnlockDTO;

	}

	public static TransactionListDTO convertListFromDetails(User u, PQTransaction t, PQCommission c) {
		TransactionListDTO dto = new TransactionListDTO();
		dto.setId(t.getId());
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setAuthority(u.getAuthority());
		dto.setCommission(c.getValue());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(t.getCurrentBalance());
		dto.setImage(u.getUserDetail().getImage());
		dto.setDebit(t.isDebit());
		dto.setDateOfTransaction(t.getCreated());
		dto.setDescription(t.getDescription());
		dto.setServiceType(t.getService().getName());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setUsername(u.getUserDetail().getFirstName());
		return dto;
	}
	
	
	public static TransactionListDTO convertListFromDetailsMerchant(User u, PQTransaction t, PQCommission c) {
		TransactionListDTO dto = new TransactionListDTO();
		dto.setId(t.getId());
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setAuthority(u.getAuthority());
		dto.setCommission(c.getValue());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(t.getCurrentBalance());
		dto.setImage(u.getUserDetail().getImage());
		dto.setDebit(t.isDebit());
		dto.setDateOfTransaction(t.getCreated());
		dto.setDescription(t.getDescription());
		dto.setServiceType(t.getService().getName());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setUsername(u.getUserDetail().getFirstName());
		return dto;
	}
	
	public static TransactionListDTO convertListFromDetailsMerchantSuccessPatanjali(User u, PQTransaction t,User merchant,double amount) {
		TransactionListDTO dto = new TransactionListDTO();
		dto.setId(t.getId());
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setAuthority(u.getAuthority());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(t.getCurrentBalance());
		dto.setImage(u.getUserDetail().getImage());
		dto.setDebit(t.isDebit());
		dto.setDateOfTransaction(t.getCreated());
		dto.setDescription(t.getDescription());
		dto.setServiceType(t.getService().getName());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setUsername(u.getUserDetail().getFirstName());
		dto.setUserMobileNo(merchant.getUsername());
		dto.setUserFirstName(merchant.getUserDetail().getFirstName());
		dto.setCommission(amount);
		return dto;
	}
	
	public static TransactionListDTO convertListFromDetailsMerchantSuccess(User u, PQTransaction t,User merchant) {
		TransactionListDTO dto = new TransactionListDTO();
		dto.setId(t.getId());
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setAuthority(u.getAuthority());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(t.getCurrentBalance());
		dto.setImage(u.getUserDetail().getImage());
		dto.setDebit(t.isDebit());
		dto.setDateOfTransaction(t.getCreated());
		dto.setDescription(t.getDescription());
		dto.setServiceType(t.getService().getName());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setUsername(u.getUserDetail().getFirstName());
		dto.setUserMobileNo(merchant.getUsername());
		dto.setUserFirstName(merchant.getUserDetail().getFirstName());
		return dto;
	}
	
	public static TransactionCommissionListDTO convertListFromDetailsMerchantSuccessCommission(PQTransaction t ) {
		TransactionCommissionListDTO dto = new TransactionCommissionListDTO();
		dto.setId(t.getId());
		dto.setStatus(t.getStatus());
		dto.setDateOfTransaction(t.getCreated());
		dto.setServiceType(t.getService().getName());
		dto.setCommission(t.getAmount());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		return dto;
	}
	
	public static TransactionListDTO convertListFromTransaction(User u, PQTransaction t) {
		TransactionListDTO dto = new TransactionListDTO();
		dto.setId(t.getId());
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setAuthority(u.getAuthority());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(t.getCurrentBalance());
		dto.setImage(u.getUserDetail().getImage());
		dto.setDebit(t.isDebit());
		dto.setDateOfTransaction(t.getCreated());
		dto.setDescription(t.getDescription());
		dto.setServiceType(t.getService().getName());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setUsername(u.getUserDetail().getFirstName());
		dto.setAuthReferenceNo(t.getAuthReferenceNo());
		dto.setRetrivalReferenceNo(t.getRetrivalReferenceNo());
		
		return dto;
	}

	public static List<TransactionReport> getFromTransactionList(List<PQTransaction> transactions, PQServiceType serviceType) {
		List<TransactionReport> reports = new ArrayList<>();
		for (PQTransaction p : transactions) {
			if (p.getService().getServiceType().getName().equals(serviceType.getName())) {
				reports.add(convertListFromEntity( p));
			}
		}
		return reports;

	}

	
	public static List<TransactionReport> getFromTransactionList(List<PQTransaction> trans) {
		List<TransactionReport> reports = new ArrayList<>();
		for (PQTransaction p : trans) {
			
			
			// if(p.getService().getServiceType().getName().equals(serviceType.getName()))
			// {
			reports.add(convertListFromEntity(p));
			// }
		}
		return reports;

	}

	
	
	
	
	
	
	
	public static List<TransactionReport> getTransactionList(List<PQTransaction> trans,List<User> us) {
		List<TransactionReport> reports = new ArrayList<>();
		for (PQTransaction p : trans) {
			
			for(User u:us)
			{
				
			long i=	u.getAccountDetail().getId();
			long j= p.getAccount().getId();
				if(i==j)
				{
				reports.add(convertListEntity(u,p));
				}
			}
			// if(p.getService().getServiceType().getName().equals(serviceType.getName()))
			// {
			//reports.add(convertListFromEntity(p));
			// }
		}
		return reports;

	}

	
	public static TransactionReport convertListEntity(User u, PQTransaction t) {
		TransactionReport dto = new TransactionReport();
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setDebit(t.isDebit());
		dto.setDescription(t.getDescription());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setDate(dateTimeFormat.format(t.getCreated()));
		dto.setService(t.getService().getDescription());
		dto.setAccount_id(t.getAccount().getId());
		dto.setAccount_id(u.getAccountDetail().getId());
		if(t.getRetrivalReferenceNo()!=null){
		dto.setReceiptNo(t.getRetrivalReferenceNo());
		}else{
			dto.setReceiptNo("NA");
		}
		return dto;
	}
	
	
	
	
	public static TransactionReport convertListFromEntity( PQTransaction t) {
		TransactionReport dto = new TransactionReport();
		dto.setStatus(t.getStatus());
		dto.setAmount(t.getAmount());
		dto.setDebit(t.isDebit());
		dto.setDescription(t.getDescription());
		dto.setTransactionRefNo(t.getTransactionRefNo());
		dto.setDate(dateTimeFormat.format(t.getCreated()));
		dto.setService(t.getService().getDescription());
		dto.setAccount_id(t.getAccount().getId());
		//dto.setAccount_id(u.getAccountDetail().getId());
		if(t.getRetrivalReferenceNo()!=null){
		dto.setReceiptNo(t.getRetrivalReferenceNo());
		}else{
			dto.setReceiptNo("NA");
		}
		return dto;
	}

	public static final List<User> filteredList(List<User> userList, String reportType) {
		List<User> filteredList = new ArrayList<>();
		System.err.println("reportType ::" + reportType);
		switch (reportType.toUpperCase()) {

		case "ALL":
			for (User user : userList) {
				String authority = user.getAuthority();
				if (authority.contains(Authorities.USER) || authority.contains(Authorities.MERCHANT)) {
					filteredList.add(user);
				}
			}
			break;
		case "SETTLEMENT":
			for (User user : userList) {
				String username = StartupUtil.SETTLEMENT;
				if (user.getUserDetail().equals(username)) {
					userList.add(user);
				}
			}
			break;
		case "COMMISSION":
			for (User user : userList) {
				String username = StartupUtil.COMMISSION;
				if (user.getUsername().equals(username)) {
					userList.add(user);
				}
			}
			break;

		default:
			break;
		}

		return filteredList;
	}

	public static List<TransactionListDTO> convertFromLists(List<User> userList, List<PQTransaction> transactionList,
			List<PQCommission> commissionList) {
		List<TransactionListDTO> resultList = new ArrayList<>();
		for (PQTransaction p : transactionList) {
			long transactionAccount = p.getAccount().getAccountNumber();
			PQService service = p.getService();
			for (User u : userList) {
				long userAccount = u.getAccountDetail().getAccountNumber();
				if (userAccount == transactionAccount) {
					if (service != null) {
						PQCommission commission = getCommissionFromList(commissionList, service);
						if (commission != null) {
							resultList.add(convertListFromDetails(u, p, commission));
						}
					}
				}
			}
		}
		return resultList;
	}
	
	
	public static List<TransactionListDTO> convertFromListsMerchant(List<User> userList, List<PQTransaction> transactionList,List<PQCommission> commissionList) {
		List<TransactionListDTO> resultList = new ArrayList<>();
		for (PQTransaction p : transactionList) {
			long transactionAccount = p.getAccount().getAccountNumber();
			PQService service = p.getService();
			for (User u : userList) {
				long userAccount = u.getAccountDetail().getAccountNumber();
				if (userAccount == transactionAccount) {
					if (service != null) {
						PQCommission commission = getCommissionFromList(commissionList, service);
						if (commission != null) {
							resultList.add(convertListFromDetailsMerchant(u, p, commission));
						}
					}
				}
			}
		  }
		return resultList;
	}
	
	public static List<TransactionListDTO> convertFromListsMerchantSuccess(List<PQTransaction> transactionList ,List<User> userList) {
		List<TransactionListDTO> resultList = new ArrayList<>();
			for(PQTransaction c : transactionList) {
			for (User u : userList) {
//				resultList.add(convertListFromDetailsMerchantSuccess(u,c));
			}
		  }
		return resultList;
	}
	
	public static List<TransactionCommissionListDTO> convertFromListsMerchantSuccessCommission(List<PQTransaction> commissionList) {
		List<TransactionCommissionListDTO> resultList = new ArrayList<>();
			for(PQTransaction c : commissionList) {
				resultList.add(convertListFromDetailsMerchantSuccessCommission(c));
		  }
		return resultList;
	}
	
	
	public static List<TransactionListDTO> convertTransactionUserLists(List<User> user,List<PQTransaction> transaction){
		List<TransactionListDTO> resultList = new ArrayList<>();
		for(PQTransaction p: transaction){
			for(User u:user){
				resultList.add(convertListFromTransaction(u, p));
			}
		}
		return resultList;
	}


	public static List<TransactionListDTO> filterUsingServiceType(List<User> userList,
			List<PQTransaction> transactionList, List<PQCommission> commissionList, List<PQService> services) {
		List<TransactionListDTO> resultList = new ArrayList<>();
		for (PQTransaction p : transactionList) {
			long transactionAccount = p.getAccount().getAccountNumber();
			PQService service = p.getService();
			for (User u : userList) {
				long userAccount = u.getAccountDetail().getAccountNumber();
				if (userAccount == transactionAccount) {
					if (service != null) {
						if (services.contains(service)) {
							PQCommission commission = getCommissionFromList(commissionList, service);
							if (commission != null) {
								resultList.add(convertListFromDetails(u, p, commission));
							}
						}
					}
				}
			}
		}
		return resultList;
	}

	public static PQCommission getCommissionFromList(List<PQCommission> commissionList, PQService service) {
		PQCommission commission = null;
		long serviceId = service.getId();
		for (PQCommission c : commissionList) {
			PQService serviceOfCommission = c.getService();
			if (serviceOfCommission != null) {
				long serviceIdInList = c.getService().getId();
				if (serviceIdInList == serviceId) {
					commission = c;
				}
			}
		}
		return commission;
	}

	public static List<BankTransferDTO> convertToBankTransfer(List<BankTransfer> bankTransfers) {
		List<BankTransferDTO> bankTransferList = new ArrayList<>();
		for (BankTransfer bt : bankTransfers) {
			bankTransferList.add(convertToDTO(bt));
		}
		return bankTransferList;
	}

	public static List<User> convertFromPGDetails(List<PGDetails> pgDetails) {
		List<User> userList = new ArrayList<>();
		if (pgDetails != null && !pgDetails.isEmpty()) {
			for (PGDetails p : pgDetails) {
				if (p != null) {
					userList.add(p.getUser());
				}
			}
		}
		return userList;
	}

	public static Page<MTransactionResponseDTO> convertFromList(List<MTransactionResponseDTO> transactionList,
			PagingDTO dto) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
		Collections.sort(transactionList, new ComparatorUtil());
		int start = page.getOffset();
		int end = (start + page.getPageSize()) > transactionList.size() ? transactionList.size()
				: (start + page.getPageSize());
		List<MTransactionResponseDTO> subList = transactionList.subList(start, end);
		Page<MTransactionResponseDTO> resultSet = new PageImpl<MTransactionResponseDTO>(subList, page,
				transactionList.size());
		return resultSet;
	}

	public static List<MTransactionResponseDTO> getMerchantTransactions(List<PQTransaction> transactionList,List<User> userList) {
		List<MTransactionResponseDTO> mTransactionLists = new ArrayList<>();
		for (PQTransaction transaction : transactionList) {
			for (User user : userList) {
				if (transaction.getAccount().getAccountNumber() == user.getAccountDetail().getAccountNumber()) {
					mTransactionLists.add(ConvertUtil.convertFromUserAndTransaction(user, transaction));
				}
			}
		}
		return mTransactionLists;
	}
	
	public static List<RefundStatusDTO> getRefundTransactions(List<PQTransaction> transactionList){
		List<RefundStatusDTO>loadmoneyLists = new ArrayList<>();
		for(PQTransaction transaction : transactionList){
			loadmoneyLists.add(ConvertUtil.convertTransaction(transaction));
				}
		return loadmoneyLists;
	}
	
	public static RefundStatusDTO convertTransaction(PQTransaction p){
		RefundStatusDTO dto = new RefundStatusDTO();
		if( p != null) {
			dto.setId(p.getId());
			dto.setAmount(p.getAmount());
			dto.setTransactionRefNo(p.getTransactionRefNo());
			dto.setDescription(p.getDescription());
			dto.setStatus(p.getStatus());
			return dto;
		}
		return dto;
	}

	public static MTransactionResponseDTO convertFromUserAndTransaction(User u, PQTransaction p) {
		MTransactionResponseDTO dto = new MTransactionResponseDTO();
		if (u != null && p != null) {
			dto.setAmount(p.getAmount());
			dto.setContactNo(u.getUsername());
			dto.setEmail(u.getUserDetail().getEmail());
			dto.setCurrentBalance(u.getAccountDetail().getBalance());
			dto.setDate(p.getCreated());
			dto.setDebit(p.isDebit());
			dto.setStatus(p.getStatus());
			dto.setTransactionRefNo(p.getTransactionRefNo());
			dto.setDescription(p.getDescription());
			dto.setId(p.getId());
			return dto;
		}
		return dto;
	}

	public static BankTransferDTO convertToDTO(BankTransfer bankTransfer) {

		BankTransferDTO dto = new BankTransferDTO();

		dto.setName(bankTransfer.getSender().getUserDetail().getFirstName());

		dto.setEmail(bankTransfer.getSender().getUserDetail().getEmail());

		dto.setMobileNumber(bankTransfer.getSender().getUsername());

		dto.setAmount("" + bankTransfer.getAmount());

		dto.setBankName(bankTransfer.getBankDetails().getBank().getName());

		dto.setIfscCode(bankTransfer.getBankDetails().getIfscCode());

		dto.setBeneficiaryAccountName(bankTransfer.getBeneficiaryName());

		dto.setBeneficiaryAccountNumber("" + bankTransfer.getBeneficiaryAccountNumber());

		dto.setVirtualAccount("" + bankTransfer.getSender().getAccountDetail().getAccountNumber());

		dto.setTransactionID(bankTransfer.getTransactionRefNo());

		return dto;
	}

	public static List<ServiceTypeDTO> convertServiceType(List<PQServiceType> servicesList) {
		List<ServiceTypeDTO> serviceTypeList = new ArrayList<>();
		for (PQServiceType s : servicesList) {
			serviceTypeList.add(convertPQServiceTypeToServiceTypeDTO(s));
		}
		return serviceTypeList;
	}

	public static AuthenticationDTO getByPayment(PaymentDTO dto) {
		AuthenticationDTO authenticationDTO = new AuthenticationDTO();
		authenticationDTO.setTransactionID(dto.getTransactionID());
		authenticationDTO.setAmount(String.valueOf(dto.getNetAmount()));
		return authenticationDTO;

	}

	public static List<ServicesDTO> convertService(List<PQService> services) {
		List<ServicesDTO> servicesList = new ArrayList<>();
		for (PQService s : services) {
			servicesList.add(convertPQServiceToDTO(s));
		}
		return servicesList;
	}

	public static ServicesDTO convertPQServiceToDTO(PQService service) {
		ServicesDTO servicesDTO = new ServicesDTO();
		servicesDTO.setDescription(service.getDescription());
		servicesDTO.setCode(service.getCode());
		servicesDTO.setOperatorCode(service.getOperatorCode());
		return servicesDTO;
	}

	public static ServiceTypeDTO convertPQServiceTypeToServiceTypeDTO(PQServiceType serviceType) {
		ServiceTypeDTO serviceTypeDTO = new ServiceTypeDTO();
		serviceTypeDTO.setId(serviceType.getId());
		serviceTypeDTO.setName(serviceType.getName());
		serviceTypeDTO.setDescription(serviceType.getDescription());
		return serviceTypeDTO;
	}

	public static List<UserSessionDTO> convertSessionList(List<UserSession> userSession) {
		List<UserSessionDTO> dtoList = new ArrayList<>();
		for (UserSession session : userSession) {
			dtoList.add(convertSession(session));
		}
		return dtoList;
	}

	public static List<FavouriteDTO> convertTransactionList(List<PQTransaction> transaction) {
		List<FavouriteDTO> dtoList = new ArrayList<>();
		for (PQTransaction t : transaction) {
			dtoList.add(getByTransaction(t));
		}
		return dtoList;
	}

	public static FavouriteDTO getByTransaction(PQTransaction transaction) {
		FavouriteDTO dto = new FavouriteDTO();
		try {
			if (transaction != null) {
				dto.setId(transaction.getId());
				dto.setAmount(transaction.getAmount());
				dto.setDate(transaction.getCreated());
				dto.setDebit(transaction.isDebit());
				dto.setFavourite(transaction.isFavourite());
				dto.setDescription(transaction.getDescription());
				dto.setStatus(transaction.getStatus());
				dto.setService(transaction.getService().getServiceType().getName());
				dto.setServiceCode(transaction.getService().getCode());
				dto.setTransactionRefNo(transaction.getTransactionRefNo());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dto;
	}

	public static OnePayResponse get(PQTransaction transaction) {
		OnePayResponse dto = new OnePayResponse();
		try {
			dto.setStatus(ResponseStatus.SUCCESS);
			dto.setServiceCode(transaction.getService().getCode());
			dto.setServiceName(transaction.getService().getName());
			dto.settStatus(transaction.getStatus());
			dto.setJson(transaction.getRequest());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return dto;
	}
	
	public static double convertAdditionalAmount(double addAmount){
		if(addAmount >=2000){
			return 0;
		}
		return 0;
	}

	public static String convertAuthenticationDTO(AuthenticationDTO dto) {
		return dto.getToken() + "|" + dto.getAmount() + "|" + dto.getId() + "|" + dto.getTransactionID();
	}
	
	public static String convertUpiAuthenticationDTO(UpiAuthenticationDTO dto) {
		return dto.getToken() + "|" + String.format("%.2f", dto.getAmount()) + "|" + String.format("%.2f", convertAdditionalAmount(dto.getAdditionalcharges())) + "|" + dto.getId() + "|" + dto.getTransactionID();
	}

	public static double formatDouble(double value) {
		return Double.parseDouble(String.format("%.2f", value));
	}

	public static VNetDTO convertVNet(VNetDTO dto, String transactionRefNo) {
		VNetDTO newDTO = new VNetDTO();
		Double preciseAmount = Double.parseDouble(dto.getAmount());
		newDTO.setAmount(String.format("%.2f", preciseAmount));
		newDTO.setCrnNo("INR");
		newDTO.setMid("vPayQwik");
		newDTO.setMerchantName("vPayQwik");
		newDTO.setPid("10420249");
		newDTO.setItc(transactionRefNo);
		newDTO.setReturnURL(dto.getReturnURL());
		newDTO.setPrnNo(transactionRefNo);
		newDTO.setTransactionRefNo(transactionRefNo);
		return newDTO;
	}

	public static BasicDTO convertToBasic(User u) {
		BasicDTO user = new BasicDTO();
		if (u != null) {
			user.setEmailStatus(u.getEmailStatus());
			user.setMobileStatus(u.getMobileStatus());
			UserDetail userDetail = u.getUserDetail();
			if (userDetail != null) {
				user.setName(userDetail.getFirstName());
				user.setAddress(userDetail.getAddress());
				user.setContactNo(userDetail.getContactNo());
				user.setEmail(userDetail.getEmail());
				if (userDetail.getDateOfBirth() != null) {
					user.setDateOfBirth(dobFormat.format(userDetail.getDateOfBirth()));
				}
				if (userDetail.getGender() != null) {
					user.setGender(userDetail.getGender());
				}
			}
		}
		return user;
	}

	// public static void main(String... args){
	// Date now = new Date();
	// System.err.println(dobFormat.format(now));
	// }

	public static AccountDTO convertToAccount(PQAccountDetail account) {
		AccountDTO accountDetails = new AccountDTO();
		if (account != null) {
			accountDetails.setAccountNumber(account.getAccountNumber());
			accountDetails.setRewardPoints(account.getPoints());
			PQAccountType accountType = account.getAccountType();
			if (accountType != null) {
				accountDetails.setAccountType(accountType.getName());
			}
		}
		return accountDetails;
	}

	public static LocationDTO convertToLocation(LocationDetails location) {
		LocationDTO locationDetails = new LocationDTO();
		if (locationDetails != null) {
			locationDetails.setPinCode(location.getPinCode());
			locationDetails.setCircleName(location.getCircleName());
			locationDetails.setDistrictName(location.getDistrictName());
			locationDetails.setRegionName(location.getRegionName());
			locationDetails.setStateName(location.getStateName());
			locationDetails.setLocality(location.getLocality());
		}
		return locationDetails;
	}

	public static UserSessionDTO convertSession(UserSession session) {
		SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
		UserSessionDTO dto = new UserSessionDTO();
		dto.setUsername(session.getUser().getUsername());
		dto.setId(session.getId());
		dto.setSessionId(session.getSessionId());
		dto.setUserId(session.getUser().getId());
		dto.setUserType(session.getUser().getUserType());
		dto.setLastRequest(time.format(session.getLastRequest()));
		return dto;
	}

	public static List<TelcoOperatorDTO> convertTelcoOperatorList(List<TelcoOperator> telcoOperators) {
		List<TelcoOperatorDTO> dtoList = new ArrayList<TelcoOperatorDTO>();
		for (TelcoOperator telcoOperator : telcoOperators) {
			dtoList.add(convertTelcoOperator(telcoOperator));
		}
		return dtoList;
	}

	public static TelcoOperatorDTO convertTelcoOperator(TelcoOperator telcoOperator) {
		TelcoOperatorDTO dto = new TelcoOperatorDTO();
		dto.setCode(telcoOperator.getCode());
		dto.setServiceCode(telcoOperator.getServiceCode());
		dto.setName(telcoOperator.getName());
		return dto;
	}

	public static List<TelcoCircleDTO> convertTelcoCircleList(List<TelcoCircle> telcoCircles) {
		List<TelcoCircleDTO> dtoList = new ArrayList<>();
		for (TelcoCircle telcoCircle : telcoCircles) {
			dtoList.add(convertTelcoCircle(telcoCircle));
		}
		return dtoList;

	}

	public static TelcoCircleDTO convertTelcoCircle(TelcoCircle telcoCircle) {
		TelcoCircleDTO dto = new TelcoCircleDTO();
		dto.setCode(telcoCircle.getCode());
		dto.setName(telcoCircle.getName());
		return dto;
	}

	public static List<TelcoPlansDTO> convertTelcoPlansList(List<TelcoPlans> telcoPlans) {
		List<TelcoPlansDTO> dtoList = new ArrayList<>();
		for (TelcoPlans telcoPlan : telcoPlans) {
			dtoList.add(convertTelcoPlans(telcoPlan));
		}
		return dtoList;
	}

	public static TelcoPlansDTO convertTelcoPlans(TelcoPlans telcoPlans) {
		TelcoPlansDTO dto = new TelcoPlansDTO();
		dto.setAmount(telcoPlans.getAmount());
		dto.setDescription(telcoPlans.getDescription());
		dto.setOperatorCode(telcoPlans.getOperatorCode());
		dto.setPlanName(telcoPlans.getPlanName());
		dto.setPlanType(telcoPlans.getPlanType());
		dto.setSmsDaakCode(telcoPlans.getSmsDaakCode());
		dto.setState(telcoPlans.getState());
		dto.setValidity(telcoPlans.getValidity());
		return dto;
	}

	public static List<UserDTO> convertUserList(List<User> user) {
		List<UserDTO> dtoList = new ArrayList<UserDTO>();
		for (User u : user) {
			dtoList.add(convertUser(u));
		}
		return dtoList;
	}

	public static List<MerchantDTO> convertMerchantList(List<User> merchant) {
		List<MerchantDTO> dtoList = new ArrayList<>();
		for (User u : merchant) {
			dtoList.add(convertMerchant(u));
		}
		return dtoList;
	}

	public static MerchantDTO convertMerchant(User merchant) {
		MerchantDTO merchantDTO = new MerchantDTO();
		try {
			merchantDTO.setId(merchant.getId());
			UserDetail merchantDetail = merchant.getUserDetail();
			merchantDTO.setContactNo((merchantDetail.getContactNo() == null) ? "" : merchantDetail.getContactNo());
			merchantDTO.setEmail((merchantDetail.getEmail() == null) ? "" : merchantDetail.getEmail());
			merchantDTO.setImage((merchantDetail.getImage() == null) ? "" : merchantDetail.getImage());
			merchantDTO.setName((merchantDetail.getFirstName() == null) ? "" : merchantDetail.getFirstName());
			merchantDTO.setDateOfRegistration(dateTimeFormat.format(merchant.getCreated()));
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		return merchantDTO;
	}

	public static UserDTO convertUser(User u) {
		UserDTO dto = new UserDTO();
		dto.setUserId(String.valueOf(u.getId()));
		dto.setAddress((u.getUserDetail().getAddress() == null) ? "" : u.getUserDetail().getAddress());
		dto.setContactNo(u.getUserDetail().getContactNo());
		dto.setFirstName(u.getUserDetail().getFirstName());
		dto.setMiddleName((u.getUserDetail().getMiddleName() == null) ? "" : u.getUserDetail().getMiddleName());
		dto.setLastName(u.getUserDetail().getLastName());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setMobileStatus(u.getMobileStatus());
		dto.setEmailStatus(u.getEmailStatus());
		dto.setUserType(u.getUserType());
		dto.setUsername(u.getUsername());
		dto.setMobileToken(u.getMobileToken());
		dto.setAuthority(u.getAuthority());
		dto.setDateOfBirth("" + u.getUserDetail().getDateOfBirth());
		dto.setGender(u.getUserDetail().getGender());
		dto.setMpin((u.getUserDetail().getMpin() == null) ? "" : u.getUserDetail().getMpin());
		dto.setImage((u.getUserDetail().getImage() == null) ? "" : u.getUserDetail().getImage());
		dto.setEncodedImage((u.getUserDetail().getImageContent() != null)? Base64.getEncoder().encodeToString(u.getUserDetail().getImageContent()):"");
		dto.setMpinPresent((u.getUserDetail().getMpin() == null) ? false : true);
		dto.setGcmId((u.getGcmId() == null) ? "" : u.getGcmId());
		dto.setAccount(u.getAccountDetail());
		LocationDetails location = u.getUserDetail().getLocation();
		if (location != null) {
			dto.setPinCode(location.getPinCode());
			dto.setCircleName(location.getCircleName());
			dto.setLocality(location.getLocality());
			dto.setDistrictName(location.getDistrictName());
		}
		return dto;
	}

	public static PromoCode convertPromotionCodeDTO(PromoCodeDTO promoDTO) {
		PromoCode promoCode = new PromoCode();
		try {
			promoCode.setPromoCode(promoDTO.getPromoCode());
			promoCode.setTerms(promoDTO.getTerms());
			promoCode.setStartDate(sdf.parse(promoDTO.getStartDate()));
			promoCode.setEndDate(sdf.parse(promoDTO.getEndDate()));
			promoCode.setValue(promoDTO.getValue());
			System.err.println(promoDTO.isFixed());
			promoCode.setFixed(promoDTO.isFixed());
			promoCode.setStatus(Status.Active);
			promoCode.setDescription(promoDTO.getDescription());
			promoCode.setUptoCash(String.valueOf(promoDTO.getCashBackValue()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return promoCode;
	}

	public static String convertServiceType(ServiceType serviceType) {
		if (serviceType.getValue().equals("TopUp"))
			return "PTPS";
		else if (serviceType.getValue().equals("BillPay"))
			return "PBPS";
		else if (serviceType.getValue().equals("SendMoney"))
			return "PSMS";
		else if (serviceType.getValue().equals("LoadMoney"))
			return "PLMS";
		else if (serviceType.getValue().equals("GiftCart"))
			return "PGCS";
		else
			return "";

	}

	public static List<String> getServiceCodeFromPromoServices(List<PromoServices> serviceList) {
		List<String> serviceCodes = new ArrayList<>();
		for (PromoServices services : serviceList) {
			serviceCodes.add(services.getService().getCode());
		}
		return serviceCodes;
	}

	public static PromoCodeDTO convertPromotionCode(PromoCode promoCode) {
		PromoCodeDTO promoDTO = new PromoCodeDTO();
		try {
			promoDTO.setPromoCodeId(promoCode.getId().toString());
			promoDTO.setPromoCode(promoCode.getPromoCode());
			promoDTO.setTerms(promoCode.getTerms());
			promoDTO.setStartDate("" + promoCode.getStartDate());
			promoDTO.setEndDate("" + promoCode.getEndDate());
			promoDTO.setFixed(promoCode.isFixed());
			promoDTO.setValue(promoCode.getValue());
			promoDTO.setStatus(promoCode.getStatus());
			promoDTO.setDescription(promoCode.getDescription());
			if(promoCode.getUptoCash()!=null){
				promoDTO.setCashBackValue(Double.parseDouble(promoCode.getUptoCash()));
			}
			else{
				promoDTO.setCashBackValue(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return promoDTO;

	}

	public static List<PromoCodeDTO> convertPromotionCodeList(List<PromoCode> promoCodes) {
		List<PromoCodeDTO> promotionCodeDTOs = new ArrayList<PromoCodeDTO>();
		for (PromoCode promoCode : promoCodes) {
			promotionCodeDTOs.add(convertPromotionCode(promoCode));
		}
		return promotionCodeDTOs;
	}

	public static BankTransfer bankTransfer(SendMoneyBankDTO dto, String transactionRefNo, BankDetails details,
			User u) {
		BankTransfer transfer = new BankTransfer();
		transfer.setAmount(Double.parseDouble(dto.getAmount()));
		transfer.setTransactionRefNo(transactionRefNo + "D");
		transfer.setBankDetails(details);
		transfer.setBeneficiaryAccountNumber(dto.getAccountNumber());
		transfer.setSender(u);
		transfer.setBeneficiaryName(dto.getAccountName());
		return transfer;

	}

	public static MBankTransfer mBankTransfer(SendMoneyBankDTO dto, String transactionRefNo, BankDetails details,
			User u) {
		MBankTransfer transfer = new MBankTransfer();
		transfer.setAmount(Double.parseDouble(dto.getAmount()));
		transfer.setTransactionRefNo(transactionRefNo + "D");
		transfer.setBankDetails(details);
		transfer.setBeneficiaryAccountNumber(dto.getAccountNumber());
		transfer.setSender(u);
		transfer.setBeneficiaryName(dto.getAccountName());
		return transfer;

	}
	
	public static ABankTransfer aBankTransfer(AgentSendMoneyBankDTO dto,String transactionRefNo, BankDetails details,User u){
		ABankTransfer transfer = new ABankTransfer();
		transfer.setAmount(Double.parseDouble(dto.getAmount()));
		transfer.setTransactionRefNo(transactionRefNo + "D");
		transfer.setBeneficiaryName(dto.getAccountName());
		transfer.setBeneficiaryAccountNumber(dto.getAccountNumber());
		transfer.setReceiverEmailId(dto.getReceiverEmailId());
		transfer.setReceiverMobileNo(dto.getReceiverMobileNo());
		transfer.setBankDetails(details);
		transfer.setSender(u);
		byte[] byteArr = Base64.getDecoder().decode(dto.getEncodedBytes());
		System.err.println("byteArr" + byteArr);
		transfer.setImageContent(byteArr);
		transfer.setImage(dto.getContentType());
		
		byte[] byteArr2 = Base64.getDecoder().decode(dto.getEncodedBytes2());
		transfer.setAddImageType(dto.getContentType2());
		transfer.setAddressImage(byteArr2);
		return transfer;
	}
	
	public static SenderBankTransferInfo senderBankTransferInfo(AgentSendMoneyBankDTO dto){
		
		SenderBankTransferInfo transfer = new SenderBankTransferInfo();
		
		transfer.setSenderName(dto.getSenderName());
		transfer.setSenderEmailId(dto.getSenderEmailId());
		transfer.setSenderMobileNo(dto.getSenderMobileNo());
		transfer.setIdProofNo(dto.getIdProofNo());
		transfer.setDescription(dto.getDescription());
		return transfer;
	}
	
	public static PromoTransactionDTO convertPromoTransaction(List<PQTransaction> trasList, PromoCode code,
			List<PromoServices> servicesList) {
		PromoTransactionDTO dto = null;
		double amount = Double.parseDouble(code.getTerms());
		if (trasList != null && trasList.size() > 0) {
			for (PromoServices services : servicesList) {
				PQService service = services.getService();
				for (PQTransaction pqTransaction : trasList) {
					try {
						if (service.getCode().equalsIgnoreCase(pqTransaction.getService().getCode())) {
							if (amount <= pqTransaction.getAmount() && pqTransaction.getStatus().equals(Status.Success)) {
								dto = new PromoTransactionDTO();
								dto.setAmount(String.valueOf(pqTransaction.getAmount()));
								dto.setServiceCode(service.getCode());
								dto.setTransactionDate(sdf.format(pqTransaction.getCreated()));
							} else {
								System.err.println("Inside check amount false");
							}
						}
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
				}
			}
			return dto;
		} else {
			return null;
		}
	}

	public static List<SecurityQuestionDTO> convertSecurityQuestionList(List<SecurityQuestion> securityQuestions) {
		List<SecurityQuestionDTO> dtoList = new ArrayList<>();
		for (SecurityQuestion securityQuestion : securityQuestions) {
			dtoList.add(convertSecurityQuestion(securityQuestion));
		}
		return dtoList;
	}

	public static SecurityQuestionDTO convertSecurityQuestion(SecurityQuestion securityQuestion) {
		SecurityQuestionDTO dto = new SecurityQuestionDTO();
		dto.setCode(securityQuestion.getCode());
		dto.setQuestion(securityQuestion.getQuestion());
		return dto;
	}

	public static List<ServiceStatusDTO> convertServiceList(List<ServiceStatus> status) {
		List<ServiceStatusDTO> dtoList = new ArrayList<ServiceStatusDTO>();
		for (ServiceStatus s : status) {
			dtoList.add(convertService(s));
		}
		return dtoList;
	}

	public static MerchantDetails getFromPOS(POSMerchants merchant) {
		MerchantDetails merchantDetails = new MerchantDetails();
		if(merchant != null) {
			merchantDetails.setBankAccountNo(merchant.getAccountNumber());
			merchantDetails.setBankLocation(merchant.getBankLocation());
			merchantDetails.setIfscCode(merchant.getIfscCode());
			merchantDetails.setAddress(merchant.getAddress());
			merchantDetails.setLocationCode(merchant.getPincode());
			merchantDetails.setPanCardNo(merchant.getPanNo());
			merchantDetails.setMerchantName(merchant.getMerchantName());
			merchantDetails.setBankName(merchant.getBankName());
			merchantDetails.setBranchName(merchant.getBankLocation());
			merchantDetails.setmVisaId(merchant.getMvisaId());
			merchantDetails.setCustomerId(merchant.getCustomerId());
			merchantDetails.setRupayId(merchant.getRupayId());
			merchantDetails.setEntityId(merchant.getCustomerId());
			merchantDetails.setBankAccountName(merchant.getBankName());
			merchantDetails.setQrCode(merchant.getQrCode());
		}else {
			merchantDetails.setBankAccountNo(" ");
			merchantDetails.setBankLocation(" ");
			merchantDetails.setIfscCode(" ");
			merchantDetails.setAddress(" ");
			merchantDetails.setLocationCode(" ");
			merchantDetails.setPanCardNo(" ");
			merchantDetails.setMerchantName(" ");
			merchantDetails.setBankName(" ");
			merchantDetails.setBranchName(" ");
			merchantDetails.setmVisaId(" ");
			merchantDetails.setCustomerId(" ");

		}
		return merchantDetails;
	}
	public static KycDTO convertFromMerchant(MerchantKycDTO dto){
		KycDTO kyc = new KycDTO();
		kyc.setAccountNumber(convertFromMerchant(dto.getAccountNumber()));
		kyc.setMobileNumber(convertFromMerchant(dto.getMobileNumber()));
		kyc.setOtp(dto.getOtp());
		return kyc;
	}
	public static String convertFromMerchant(String string){
		return string.substring(1);
	}
	public static String convertToMerchant(String string){
		return "M"+string;
	}

	public static ServiceStatusDTO convertService(ServiceStatus s) {
		ServiceStatusDTO dto = new ServiceStatusDTO();
		dto.setName((s.getName() == null) ? "" : s.getName());
		dto.setStatus((s.getStatus() == null) ? "" : s.getStatus().toString());
		return dto;
	}

	public static String generateFileName() {
		String Num = String.format("%02d", ++fileNum);
		return Num;
	}

	

	public static MTransactionResponseDTO convert(User u, PQTransaction p) {
		MTransactionResponseDTO dto = new MTransactionResponseDTO();
		dto.setAmount(p.getAmount());
		dto.setContactNo(u.getUsername());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(p.getCurrentBalance());
		dto.setDate(p.getCreated());
		dto.setDebit(p.isDebit());
		dto.setStatus(p.getStatus());
		dto.setTransactionRefNo(p.getTransactionRefNo());
		dto.setDescription(p.getDescription());
		dto.setId(p.getId());
		dto.setOrderId(" ");
		return dto;
	}

	public static MTransactionResponseDTO convert(User u, TPTransaction tpTransaction,PQTransaction p) {
		MTransactionResponseDTO dto = new MTransactionResponseDTO();
		dto.setAmount(p.getAmount());
		dto.setContactNo(u.getUsername());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(p.getCurrentBalance());
		dto.setDate(p.getCreated());
		dto.setDebit(p.isDebit());
		dto.setStatus(p.getStatus());
		dto.setTransactionRefNo(p.getTransactionRefNo());
		dto.setDescription(p.getDescription());
		dto.setId(p.getId());
		dto.setOrderId(tpTransaction.getOrderId());
		dto.setFirstName(u.getUserDetail().getFirstName());
		return dto;
	}
	
	public static MTransactionResponseDTO convertTreatCardUser(User u,PQTransaction p) {
		MTransactionResponseDTO dto = new MTransactionResponseDTO();
		dto.setAmount(p.getAmount());
		dto.setContactNo(u.getUsername());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(p.getCurrentBalance());
		dto.setDate(p.getCreated());
		dto.setDebit(p.isDebit());
		dto.setStatus(p.getStatus());
		dto.setTransactionRefNo(p.getTransactionRefNo());
		dto.setDescription(p.getDescription());
		dto.setId(p.getId());
		dto.setOrderId("NA");
		dto.setFirstName(u.getUserDetail().getFirstName());
		return dto;
	}

	public static MTransactionResponseDTO convert(User u, TPTransaction tpTransaction) {
		MTransactionResponseDTO dto = new MTransactionResponseDTO();
		dto.setAmount(tpTransaction.getAmount());
		dto.setContactNo(u.getUsername());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setCurrentBalance(u.getAccountDetail().getBalance());
		dto.setDate(tpTransaction.getCreated());
		dto.setStatus(tpTransaction.getStatus());
		dto.setTransactionRefNo(tpTransaction.getTransactionRefNo());
		dto.setOrderId(tpTransaction.getOrderId());
			return dto;
	}
	
	public static List<PartnerDetailDTO> getFromPartnerInstall(List<PartnerInstall> installs,
			PartnerDetail partnerName) {
		List<PartnerDetailDTO> reports = new ArrayList<>();
		for (PartnerInstall p : installs) {
			if (p.getPartnerDetail().getName().equals(partnerName.getName())) {
				reports.add(convertListFromPartnerInstall(p));
			}
		}
		return reports;

	}
	
	public static PartnerDetailDTO convertListFromPartnerInstall(PartnerInstall pi) {
		PartnerDetailDTO dto = new PartnerDetailDTO();
		dto.setImei(pi.getImei());
		dto.setDevice(pi.getDevice());
		dto.setModel(pi.getModel());
		dto.setDate(pi.getCreated());
		return dto;
	}
	
	public static List<VersionListDTO> getListForVersions(List<VersionLogs> versions) {
		List<VersionListDTO> versionListDTOs = new ArrayList<>();
		for (VersionLogs s : versions) {
			versionListDTOs.add(convertListFromVersionList(s));
		}
		return versionListDTOs;
	}
	
	public static VersionListDTO convertListFromVersionList(VersionLogs vl) {
		VersionListDTO dto = new VersionListDTO();
		dto.setStatus(String.valueOf(vl.getStatus()));
		dto.setVersion(vl.getVersion());
		dto.setCreated(vl.getCreated());
		return dto;
	}
	
	
	public static List<UserByLocationDTO> getUserListByLocationCode(List<User> userByLocationcode) {
		List<UserByLocationDTO> userByLocationCode = new ArrayList<>();
		for (User u : userByLocationcode) {
			userByLocationCode.add(convertListByLocationCode(u));
		}
		return userByLocationCode;
	}
	
	public static UserByLocationDTO convertListByLocationCode(User u) {
		UserByLocationDTO dto = new UserByLocationDTO();
		dto.setCreated(transactionDate.format(u.getCreated()));
		dto.setAuthority(u.getAuthority());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setMobileStatus(u.getMobileStatus());
		dto.setEmailStatus(u.getEmailStatus());
		dto.setMobileToken(u.getMobileToken());
		dto.setImage(u.getUserDetail().getImage());
		dto.setUserType(UserType.User);
		if(u.getUserDetail().getLocation()!=null){
		dto.setPinCode(u.getUserDetail().getLocation().getPinCode());
		dto.setCircleName(u.getUserDetail().getLocation().getCircleName());
		}else{
			dto.setPinCode("");
			dto.setCircleName("");
		}
		String accNo=u.getAccountDetail().getVijayaAccountNumber();
		if(accNo !=null){
		dto.setVijayaBankAccount(accNo);
		}else{
			dto.setVijayaBankAccount("");
		}
		dto.setUserType(u.getUserType());
		dto.setAccountType(u.getAccountDetail().getAccountType().getName());
		if(u.getUserDetail().getDateOfBirth()!=null){
		dto.setDOB(transactionDate.format(u.getUserDetail().getDateOfBirth()).substring(0, 10));
		}else{
			dto.setDOB("");
		}
		if(u.getUserDetail().getGender() !=null){
		dto.setGender(u.getUserDetail().getGender());
		}else{
			dto.setGender(Gender.NA);
		}
		dto.setBalance(u.getAccountDetail().getBalance());
		dto.setPoints(u.getAccountDetail().getPoints());
		dto.setUsername(u.getUserDetail().getFirstName());
		dto.setMobile(u.getUsername());
		return dto;
	}
	
	
	public static NearMerchantDetailsDTO getMerchantDetails(NearByMerchantsDTO dto) {
		NearMerchantDetailsDTO merchantDTO = new NearMerchantDetailsDTO();
		merchantDTO.setMerchantName(dto.getMerchantName());
		merchantDTO.setLongitude(dto.getLongitude());
		merchantDTO.setLatitude(dto.getLatitude());
		merchantDTO.setBetweenDistance(dto.getBetweenDistance());
		merchantDTO.setAddress(dto.getAddress());
		return merchantDTO;
	}
	
	public static List<TreatCardPlansDTO> convertTreatCardPlansList(List<TreatCardPlans> plans) {
		List<TreatCardPlansDTO> dtoList = new ArrayList<>();
		for (TreatCardPlans plan : plans) {
			dtoList.add(convertPlans(plan));
		}
		return dtoList;
	}

	public static TreatCardPlansDTO convertPlans(TreatCardPlans plan) {
		TreatCardPlansDTO dto = new TreatCardPlansDTO();
		dto.setDays(plan.getDays());
		dto.setAmount(plan.getAmount());
		dto.setStatus(plan.getStatus());
		dto.setId(plan.getId());
		return dto;
	}
	
	public static String removeLastString(String str){
		str = str.substring(0, str.length() - 1);
		System.out.println(str);
		return str;
	}
	public static List<BulkFileDTO> convertFileList(List<BulkFile> fileList) {
		List<BulkFileDTO> list = new ArrayList<>();
		for (BulkFile file : fileList) {
			list.add(convertToDTOFile(file));
		}
		return list;
	}
	
	public static BulkFileDTO convertToDTOFile(BulkFile file) {
		BulkFileDTO dto = new BulkFileDTO();
		dto.setMobileNumber(file.getMobileNumber());
		dto.setAmount(file.getAmount());
		dto.setStatus(file.getStatus());
		return dto;
	}
	
	public static double getPercent(long exUser, long curUser){
		System.err.println("exUser 1 ---------- " + exUser);
		System.err.println("curUser 1 ---------- " + curUser);
		double percent = (curUser - exUser);
		System.err.println("Percent 1 ---------- " + percent);
		percent = percent/exUser;
		System.err.println("Percent 2 ---------- " + percent);
		percent = percent * 100;
		System.err.println("Percent 3 ---------- " + percent);
		return percent;
	}
	
	public static List<TreatCardRegisterDTO> convertRegisterList(List<TreatCardDetails> registerList) {
		List<TreatCardRegisterDTO> details = new ArrayList<>();
		for (TreatCardDetails list : registerList) {
			details.add(convert(list));
		}
		return details;
	}
	
	public static TreatCardRegisterDTO convert(TreatCardDetails list) {
		TreatCardRegisterDTO dto = new TreatCardRegisterDTO();
		User u=list.getUser();
		dto.setCreated(dateTimeFormat.format(list.getCreated()));
		dto.setMembershipCode(list.getMembershipCode());
		dto.setMemberShipCodeExpire(String.valueOf(list.getMembershipCodeExpire()));
		dto.setUsername(u.getUsername());
		dto.setEmailId(u.getUserDetail().getEmail());
		dto.setFirstName(u.getUserDetail().getFirstName());
		return dto;
	}
	
	
	public static List<TreatCardRegisterDTO> convertTreatCardList(List<TreatCardCount> registerList) {
		List<TreatCardRegisterDTO> details = new ArrayList<>();
		for (TreatCardCount list : registerList) {
			details.add(convertCountList(list));
		}
		return details;
	}
	
	public static TreatCardRegisterDTO convertCountList(TreatCardCount list) {
		TreatCardRegisterDTO dto = new TreatCardRegisterDTO();
		dto.setMembershipCode(list.getMembershipCode());
		dto.setUsername(list.getUsername());
		dto.setEmailId(list.getEmailId());
		dto.setCount(list.getCount());
		dto.setCreated(dateTimeFormat.format(list.getCreated()));
		return dto;
	}

	
/*Admin Panel Report*/
	
	public static String converCurToPastDate(Date d) throws ParseException{
		String curDate = "";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat formatter2 = new SimpleDateFormat("yyyy-MM");
		String s = formatter.format(d);
		String ss[] = s.split("-");
		long l = Long.parseLong(ss[2]);
		l = l-1;
		String t = formatter2.format(d);
		if(l<10){
			curDate = t+"-0"+l;
		}else{
			curDate = t+"-"+l;
		}
		return curDate;
	} 
	
	
	public static WalletOutstandingDTO convertWalletOustReportWithTxn(User u, PQTransaction t){
		WalletOutstandingDTO dto = new WalletOutstandingDTO();
		dto.setDate(u.getCreated());
		dto.setEmail(u.getUserDetail().getEmail());
		dto.setMobile(u.getUsername());
		dto.setName(u.getUserDetail().getFirstName() + " " + u.getUserDetail().getLastName());
		dto.setStatus(u.getMobileStatus().name());
		dto.setBalance(t.getCurrentBalance());
		dto.setId(t.getAccount().getId());
		return dto;
	}
	
	public static WalletOutstandingDTO convertWalletOustReport(User u){
		WalletOutstandingDTO dto = new WalletOutstandingDTO();
		dto.setDate(u.getCreated());
		dto.setEmail(u.getUserDetail().getEmail()!=null?u.getUserDetail().getEmail():"NA");
		dto.setMobile(u.getUsername()!=null?u.getUsername():"NA");
		dto.setName(u.getUserDetail().getFirstName()!=null?u.getUserDetail().getFirstName():"NA" + " " + u.getUserDetail().getLastName());
		dto.setStatus(u.getMobileStatus().name());
		dto.setBalance(u.getAccountDetail().getBalance());
		dto.setId(u.getAccountDetail().getId());
		return dto;
	}
	
	public static StatusResponse convertUpiTransaction(PQTransaction transaction) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setAmount(String.valueOf(transaction.getAmount()));
		statusResponse.setTransactionDate(sdf.format(transaction.getCreated()));
		statusResponse.setPaymentId(transaction.getTransactionRefNo());
		statusResponse.setStatus(transaction.getStatus());
		statusResponse.setMerchantRefNo(transaction.getRetrivalReferenceNo());
		return statusResponse;
	}
	
	public static String converCurToNextDate(Date d) throws ParseException{
		String curDate = "";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat formatter2 = new SimpleDateFormat("yyyy-MM");
		String s = formatter.format(d);
		String ss[] = s.split("-");
		long l = Long.parseLong(ss[2]);
		l = l+1;
		String t = formatter2.format(d);
		if(l<10){
			curDate = t+"-0"+l;
		}else{
			curDate = t+"-"+l;
		}
		return curDate;
	} 
	
	public static String convertDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String conDate = sdf.format(date);
		return conDate;
	}
	
	public static S2SRequest convertS2SRequest(TPTransaction t, PGDetails merDetails) throws Exception{
		S2SRequest request = new S2SRequest();
		request.setKey(t.getMerchant().getId().toString());
		request.setPgtransactionid(t.getTransactionRefNo());
		request.setMerchanttxnid(t.getOrderId());
		request.setStatus(ResponseStatus.SUCCESS.getKey());
		request.setCode(ResponseStatus.SUCCESS.getValue());
		request.setBillamount(String.format("%.2f", t.getCustDetails().getOriginalAmount()));
		request.setAdditionalcharges(String.format("%.2f", t.getCustDetails().getAdditionalAmount()));
		request.setPhone(t.getCustDetails().getPhone());
		request.setEmail(t.getCustDetails().getEmail());
		request.setTransactiondatetime(convertDate(new Date()));
		request.setHash(SecurityUtil.md5(merDetails.getToken() + "|" + String.format("%.2f", t.getCustDetails().getOriginalAmount()) + "|" 
				+ String.format("%.2f", t.getCustDetails().getAdditionalAmount()) + "|" + t.getMerchant().getId() + "|" + t.getOrderId()));
		request.setS2sCallUrl(merDetails.getSuccessURL());
		
		if (merDetails.getUser().getUsername().equals(UPIBescomConstants.BECOM_RURAL_MERCHANT)) {
			request.setRural(true);
			request.setMsg(com.thirdparty.util.ConvertUtil.convertBescomRuralRequest(request, t));
		}
		return request;
	}
	
	public static List<RefundDTO> convertRefundRequest(List<MerchantRefundRequest> request) {
		List<RefundDTO> dtoList = new ArrayList<>();
		for (MerchantRefundRequest req : request) {
			dtoList.add(convertRequest(req));
		}
		return dtoList;
	}

	public static RefundDTO convertRequest(MerchantRefundRequest req) {
		RefundDTO dto = new RefundDTO();
		dto.setTransactionRefNo(req.getTrnasactionRefNo());
		dto.setNetAmount(req.getAmount());
		dto.setRefundAmount(String.valueOf(req.getRefundAmount()));
		dto.setOrderId(String.valueOf(req.getOrderId()));
		dto.setCreated(String.valueOf(req.getCreated()));
		dto.setStatus(String.valueOf(req.getStatus()));
		return dto;
	}
	
	public static List<OffersDTO> convertOfferRequest(List<Offers> request) {
		List<OffersDTO> dtoList = new ArrayList<>();
		for (Offers req : request) {
			dtoList.add(convertOfferRequest(req));
		}
		return dtoList;
	}

	public static OffersDTO convertOfferRequest(Offers req) {
		OffersDTO dto = new OffersDTO();
		
		dto.setServices((req.getServices() == null)? "NA" : req.getServices());
		dto.setOffers((req.getOffers() == null) ? "NA" : req.getOffers());
		dto.setActiveOffer(req.getActiveOffer());
		dto.setSessionId("NA");
		return dto;
	}

	
	
	public static void main(String[] args) throws Exception {
		
		
		String refNo="VB1485645140304593C";
		System.err.println("refno ::" + refNo.substring(2));
		
//		System.err.println(getPercent(98358, 99281));
//		 final SimpleDateFormat dateFormat = new SimpleDateFormat("yyy998744y-MM-dd hh:mm");
//		System.err.println(SecurityUtil.md5("BA5E1FA6D0BD9D18A43C2AE531B6F9BC" + "|" + "10" + "|" +271481+"|" + "124056759"));
//		System.err.println(SecurityUtil.md5("918106932532|phani@niki.ai"));
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		Date startDate = cal.getTime();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DATE, -1);
		Date endDate = cal2.getTime();

		System.err.println("Start Date : : " + startDate);
		System.err.println("End Date   : : " + endDate);
		
		/* Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			String date = dateFormat.format(calendar.getTime());
			Date myDate = null;
			myDate = dateFormat.parse(date);

			Date oneDayBefore = new Date(myDate.getTime() - (24 * 3600000));
			String result = dateFormat.format(oneDayBefore);
			result = result+ " 00:00:00";
			System.err.println("result ::" + dateFormat.format(new Date()));
			
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			Date startDate = cal.getTime();
			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.DATE, -1);
			Date endDate =  cal2.getTime();
			System.err.println("Start Date : : " + dateFormat.format(startDate));
			System.err.println("End Date   : : " + dateFormat.format(endDate));
			
			System.err.println("result ::" + result);
			
			calendar.add(Calendar.DATE, -1);
			Date previousDay = calendar.getTime();
			System.err.println("Previous Day Date : :  " + previousDay);*/
		 
		/* Calendar cal  = Calendar.getInstance();
		    cal.add(Calendar.DATE, -1);

		    SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
		    String result = s.format(new Date(cal.getTimeInMillis()));
		    System.err.println("result ::" + result);*/
		 
	/**/
		 
		 /*String response= "{\"success\":true,\"message\":null,\"code\":200,\"links\":{\"self\":{\"href\":\"/rest/status/3181286300\"}},\"error_code\":null,\"data\":{\"process\":true,\"status\":\"processing\",\"order_id\":\"3181286300\"}}";
		 JSONObject jsonObject = new JSONObject(response);
		    JSONObject myResponse = jsonObject.getJSONObject("data");
		    String data=myResponse.getString("status");*/
		
		String password = "vpayqwik@mss";
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		System.out.println(hashedPassword);
		
		/*Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, 15);
		
		long timestamp=System.nanoTime();
		System.err.println("timestamp ::" + timestamp);
		long timeDiff = timestamp - 11413428630077L;
		long validTimeForTransaction = (long) (Math.pow(10, 9)) * 60 * 1;
		System.err.println("timediff ::" + timeDiff);
		System.err.println("valid time ::" + validTimeForTransaction);
		if (timeDiff > validTimeForTransaction) {
			System.err.println(true);
		}else{
			System.err.println(false);
		}
		
		System.err.println(System.currentTimeMillis());*/
		
		  String [] someArray = new String[]{"A", "B", "C", "A", "B", "C"};
		    Map<String,Integer> repeatationMap= new HashMap<String,Integer>();
		    for(String str : someArray){

		        if(repeatationMap.containsKey(str)) {
		            repeatationMap.put(str,repeatationMap.get(str) + 1);
		        }
		        else {
		            repeatationMap.put(str, 1);
		        }
		    }

		    int count = 0;
		    for(int repatCount : repeatationMap.values()){
		        if(repatCount > 1) {
		            count++;
		        }
		    }
		    System.out.println("Number of Strings repeated : " + count);
		    double netCommissionValue = (9 * 500) / 100;
		    String v = String.format("%.2f", netCommissionValue);
		    System.err.println("v ::" + Double.parseDouble(v));
		    
		    String value="";
		    if(StringUtils.isEmpty(value)){
		    	String val="NA";
		    	System.err.println(val);
		    }else{
		    	System.err.println(false);
		    }
		    
	}
	
	public static List<ServiceTypeDTO> convertOperator(List<PQOperator> operatorList) {
		List<ServiceTypeDTO> serviceTypeList = new ArrayList<>();
		for (PQOperator s : operatorList) {
			serviceTypeList.add(convertPQServiceTypeToServiceTypeDTO(s));
		}
		return serviceTypeList;
	}
	
	public static ServiceTypeDTO convertPQServiceTypeToServiceTypeDTO(PQOperator operatorList) {
		ServiceTypeDTO serviceTypeDTO = new ServiceTypeDTO();
		serviceTypeDTO.setName(operatorList.getName());
		return serviceTypeDTO;
	}
	
	public static List<AadharDetailsDTO> getListForAadharDetails(List<AadharDetails> aadharServices,UserRepository userRepository) {
		List<AadharDetailsDTO> AadharServiceListDTOs = new ArrayList<>();
		for (AadharDetails s : aadharServices) {
			try {
				User user = userRepository.findByUsername(s.getUser().getUsername());
				AadharServiceListDTOs.add(getDTOFromAadhar(s, user));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return AadharServiceListDTOs;
	}
	
	public static AadharDetailsDTO getDTOFromAadhar(AadharDetails s, User user) {
		AadharDetailsDTO details = new AadharDetailsDTO();
		try {
			details.setAadharAddress(s.getAadharAddress());
			details.setAadharName(s.getAadharName());
			details.setAadharNumber(s.getAadharNumber());
            details.setMobileNumber(user.getUsername());
            details.setDateOfUpdate(dateTimeFormat.format(s.getCreated()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return details;
	}
	
}
