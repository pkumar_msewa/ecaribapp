package com.payqwikapp.util;

public class VNetUtils {
	
	private static final String URL = DeploymentConstants.WEB_URL+"/ws/api/LoadMoney/"; // VPAYQWIK
	public static final String VNET_STATUS = URL + "VnetStatus";

	public static final String VNET_VERIFICATION_URL = "https://www.vijayabankonline.in/NASApp/BANA623WAR/BANKAWAY?Action.ShoppingMall.Login.Init=Y&BankId=029%20&MD=V&CRN=INR&CG=Y&USER_LANG_ID=001&UserType=1&AppType=corporate";
	public static final String VNET_PID = "10420249";
	public static final String VNET_SERVICE_CODE = "LMB";
	
}
