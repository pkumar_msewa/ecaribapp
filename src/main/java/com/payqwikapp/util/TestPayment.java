package com.payqwikapp.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class TestPayment {

	public static void main(String[] args) {
		String secret="BEE17FB76511833978F7E0A94231A5F2";
		String amount= "500";
		long id=271383;
		String transactionId = "12471897267";
		String str = secret.trim()+"|"+amount.toString().trim()+"|"+id+"|"+transactionId.trim();
		System.out.println(str);
		try {
			String hash = md5(str);
			System.err.println(hash);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String md5(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}

}
