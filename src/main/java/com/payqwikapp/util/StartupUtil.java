package com.payqwikapp.util;

import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

public class StartupUtil {

	// For fgmtest server
	// public static final String CSV_FILE =
	// "/home/vibhanshu/Documents/PayQwikCentralSystem/PayQwikApp/WebContent/WEB-INF/startup/";
	//

//	 public static final String CSV_FILE = "D:\\Piyush\\Software\\VPayQwik-app\\payqwikapp\\WebContent\\WEB-INF\\startup\\";
	
//	 public static final String CSV_FILE = "D:\\Projects\\Work Spaces\\TriPayAppWorkSpace\\VpayQwik-App\\WebContent\\WEB-INF\\startup\\";
	 
	 

	 //local tomcat deployment
//	public static final String CSV_FILE = "C:\\tomcat7\\webapps\\ROOT\\WEB-INF\\startup\\";

	
	//this was open before  i use
//	public static final String CSV_FILE = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/startup/";
	

//	public static final String CSV_FILE = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/startup/";
	
//	public static final String CSV_FILE_TEST= "D:\\Projects\\Work Spaces\\TriPayAppWorkSpace\\VpayQwik-App\\WebContent\\WEB-INF\\startup\\";
	
	public static final String CSV_FILE_TEST = "D:\\Piyush\\ECarin_App\\ecaribapp\\WebContent\\WEB-INF\\startup\\";
	
//	public static final String CSV_FILE_TEST = "/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/startup/";
	
	public static final String CSV_FILE_LIVE = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/startup/";
	
	// public static final String CSV_FILE = "/usr/local/tomcat7/webapps/PayQwikApp/WebContent/WEB-INF/startup/";

// public static final String CSV_FILE = "/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/startup/";

	// public static final String CSV_FILE =
	// "C:\\PayQwikCentralSystem\\PayQwikApp\\WebContent\\WEB-INF\\startup\\";
	// //AJ172.16.7.206

	// public static final String CSV_FILE =
	// "D:\\VPayQwikAppWorkspace\\vpayqwik-app\\WebContent\\WEB-INF\\startup\\";

//	 public static final String CSV_FILE_SENDMONEY_TEST = "D:/OFFICE PROJECTS/office project/APP/APP/WebContent/WEB-INF/banktransfer/" ;
	
	public static final String CSV_FILE_LOADMONEY = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/startup/ebsRefundMoney/";

	public static final String CSV_FILE_SENDMONEY_TEST = "/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/banktransfer/";
	
	public static final String CSV_FILE_SENDMONEY_LIVE = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/banktransfer/";
	
	public static final String FILE_PATH = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/reconreport/";
	
//	public static final String FILE_PATH = "D:\\OFFICE PROJECTS\\office project\\APP\\APP\\WebContent\\WEB-INF\\reconreport\\";
	
	public static final String PPI_FILE_PATH = "/usr/local/tomcat1/webapps/ROOT/WEB-INF/ppiFiles/";
	
//	public static final String PPI_FILE_PATH = "D:\\OFFICE PROJECTS\\office project\\APP\\APP\\WebContent\\WEB-INF\\ppiFiles\\";
//	public static final String FILE_PATH = "D:\\Projects\\Work Spaces\\TriPayAppWorkSpace\\VpayQwik-App\\src\\main\\resources\\";
	
	
	public static final String CSV_FILE = getCSVFile();
	
	public static final String SEND_MONEY_FILE = getSendMoneyFile();
	
//	public static final String FILE_PATH = getPerformaceReprotFilePath();
	
	public static String getCSVFile() {
		if (DeploymentConstants.PRODUCTION) {
			return CSV_FILE_LIVE;
		} else {
			return CSV_FILE_TEST;
		}
	}
	
	/*public static String getPerformaceReprotFilePath() {
		if (DeploymentConstants.PRODUCTION) {
			return FILE_PATH_LIVE;
		} else {
			return FILE_PATH_TEST;
		}
	}*/
	
	public static String getSendMoneyFile() {
		if (DeploymentConstants.PRODUCTION) {
			return CSV_FILE_SENDMONEY_LIVE;
		} else {
			return CSV_FILE_SENDMONEY_TEST;
		}
	}

	public static final String TRAVEL = "travel@msewa.com";
	public static final String KYC = "KYC";
	public static final String SUPER_KYC = "SUPERKYC";
	public static final String NON_KYC = "NONKYC";
	public static final String ADMIN = "admin@ecarib.com";
	public static final String SETTLEMENT = "settlement@ecarib.com";
	public static final String COMMISSION = "commission@ecarib.com";
	public static final String BANK = "bank@ecarib.com";
	public static final String PROMO_CODE = "promoCode@gmail.com";
	public static final String PRDEICT_AND_WIN = "predictAndWin@msewa.com";
	public static final String REFUND = "refund@msewa.com";
	public static final String RISK = "risk@ecarib.com";
	public static final String VAT = "vat@msewa.com";
	public static final String CGST="cgst@msewa.com";
	public static final String SGST="sgst@msewa.com";
	public static final String KRISHI_KALYAN = "kkcess@msewa.com";
	public static final String SWACCH_BHARAT = "sbcess@msewa.com";
	public static final String MBANK = "mbank@ecarib.com";
	public static final String MERCHANT = "merchant@ecarib.com";
	public static final String MVISA = "mvisa@msewa.com";
	public static final String ABANK = "abank@ecarib.com";
	
	// mera Event
	public static final String MERA_EVENTS = "meraevents@msewa.com";
	public static final String MeraEventCode = "EVNT";
	public static final String MOBILE_BANK = "mbank@msewa.com";
	public static final String SUPER_ADMIN = "superadmin@ecarib.com";
	public static final String Giftcart = "giftcart@msewa.com";
	public static final String SAVAARI = "savaari@msewa.com";
	public static final String GiftcartCode = "GCART";
	public static final String NIKKI_CHAT = "nikkichat@gmail.com";
	public static final String SUPER_AGENT = "superagent@ecarib.com";
	public static final String PAYLO_MERCHANT = "paylomerchant_1@gmail.com";
	// for adlabs
	public static final String Adlabs = "adlabs@msewa.com";
	public static final String AdlabsCode = "ADLABS";
	public static final String IMAGICA = "imagica@ecarib.com";
	public static final String IMAGICA_SERVICE = "IMGCA";
	public static final String QWIKR_PAY="qwikrpay@msewa.com";
	public static final String QWIK_PAY_CODE="QWKP";
	
	//for YuppTv
	
	public static final String yuppTv = "yuppTv@msewa.com";
	public static final String yuppTvCode = "YUPTV";
	public static final String YUPPTV = "yuppTV@ecarib.com";
	public static final String YuppTv_SERVICE = "YUPPTV";
	
	public static final String STATE_GST = "stateservicetax@msewa.com";
	public static final String CENTRAL_GST = "centralservicetax@msewa.com";
	
	//for refer and earn
	
	public static final String REFER_EARN="refernearn@msewa.com";
	//for flight booking
	
	public static final String FLIGHT_BOOKING="easeMyTrip_flight@msewa.com";
	public static final String FLIGHT_CODE="EMTF";
	
	
	// For  Bus Booking
	public static final String BUS_BOOKING="easeMyTripBus@msewa.com";
	public static final String BUS_CODE="EMTB";
	
	//RedeemPoints 
	public static final String REDEEM_POINT_CASH="REDEEM";
	
	//Travelkhana 
	public static final String TRAVELKHANA = "tavelkhana@msewa.com";
	public static final String TRAVELKAHANA_SERVICECODE = "TKHANA";
	
	public static final String TREAT_CARD="treatcard@msewa.com";
	
//	HouseJoy
	
	public static final String HOUSE_JOY="houseJoy@msewa.com";
	public static final String HOUSE_JOY_CODE="HJP";
	
	//woohoo
	public static final String WOOHOO = "woohoocards@msewa.com";
	public static final String WOOHOO_SERVICECODE = "WOOHO";
	public static final String AUTOCHECKSTATUS = DeploymentConstants.WEB_URL+"/GciOrder/Woohoo/PendingCards";
	
	
	public static Client createClient() throws Exception{
		Client restClient = null;
        try {
//	        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

	        // Create a trust manager that does not validate certificate chains 
	        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
	            @Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
					// TODO Auto-generated method stub
					
				}

				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					// TODO Auto-generated method stub
					return null;
				}
	        }};

	        // Ignore differences between given hostname and certificate hostname 
	        HostnameVerifier hv = new HostnameVerifier() {
	          public boolean verify(String hostname, SSLSession session) { 
	        	  return true; 
	        	   }
	        }; 
	        
	        ClientConfig config = new DefaultClientConfig();
	        SSLContext sc = SSLContext.getInstance("TLSv1.2"); //$NON-NLS-1$
	        sc.init(null, trustAllCerts, new SecureRandom());
	        sc.init(null, trustAllCerts, new SecureRandom());
	        HttpsURLConnection.setDefaultHostnameVerifier(hv);
	        config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, sc));
	        restClient = Client.create(config);
	        System.setProperty("javax.net.debug", "ssl");
	        System.setProperty("mail.smtp.socketFactory.fallback", "true"); 
        } catch (KeyManagementException e) {
	        String errorMsg = "client initialization error: " //$NON-NLS-1$
	                + e.getMessage();
	       System.err.println(errorMsg);
	        throw new Exception(errorMsg, e);
	    }
		return restClient; 
 }
}
