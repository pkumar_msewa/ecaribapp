package com.payqwikapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.payqwikapp.entity.PQAccountDetail;

public class CommonUtil {

	/**
	 * Random Number Generator for MObile OTP return 6 digit random number
	 * 
	 * @return
	 */
	private static final double RADIUS_OF_EARTH = 6371.0D;
	public static String ITUNES_LINK = "https://goo.gl/Ou5ZRr";
	public static String PLAY_STORE_LINK = "http://goo.gl/07Kf4u";
	public static String startTime = " 00:00:00";
	public static String endTime = " 23:59:59";

	public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat dateFormate2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	public static String generateSixDigitNumericString() {
		int number = (int) (Math.random() * 1000000);
		String formattedNumber = String.format("%06d", number);
		return formattedNumber;
	}

	/**
	 * random n digit number generator
	 * 
	 * @param n
	 * @return
	 */

	public static String generateNDigitNumericString(long n) {
		double mul = Math.pow(10, n);
		long result = (long) (Math.random() * mul);
		// long result = 12;
		String number = String.format("%0" + n + "d", result);
		// System.err.println(number);
		return number;
	}

	//
	// public static void main(String... args){
	// generateSixDigitNumericString();
	// generateNDigitNumericString(12);
	// }

	public static String generateNineDigitNumericString() {
		return "" + (int) (Math.random() * 1000000000);
	}

	public static void sleep(long timeInMS) {
		try {
			Thread.sleep(timeInMS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Date getStartTimeOfYesterDayDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, -1);
		Date yesterDayDate = calendar.getTime();
		try {
			yesterDayDate = dateFormate.parse(formatter.format(yesterDayDate) + startTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return yesterDayDate;
	}

	public static Date getEndTimeOfYesterDayDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, -1);
		Date yesterDayDate = calendar.getTime();
		try {
			yesterDayDate = dateFormate.parse(formatter.format(yesterDayDate) + endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return yesterDayDate;
	}

	public static Date getDateBeforeYesterday() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(calendar.DATE, -2);
		Date yesterDayDate = calendar.getTime();
		return yesterDayDate;
	}

	public static Date getStartTimeOfDateBeforeYesterday(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(calendar.DATE, -2);
		Date yesterDayDate = calendar.getTime();
		try {
			yesterDayDate = dateFormate.parse(formatter.format(yesterDayDate) + startTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return yesterDayDate;
	}

	public static Date getEndTimeOfDateBeforeYesterday(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(calendar.DATE, -2);
		Date yesterDayDate = calendar.getTime();
		try {
			yesterDayDate = dateFormate.parse(formatter.format(yesterDayDate) + endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return yesterDayDate;
	}

	public static double calculateDistance(double lat1, double long1, double lat2, double long2) {
		double distance = 0.0D;
		double lat1InRad = Math.toRadians(lat1);
		double lat2InRad = Math.toRadians(lat2);
		double diffInLat = Math.toRadians(lat2 - lat1);
		double diffInLong = Math.toRadians(long2 - long1);
		double a = Math.pow(Math.sin(diffInLat / 2.0D), 2.0D)
				+ Math.cos(lat1InRad) * Math.cos(lat2InRad) * Math.pow(Math.sin(diffInLong / 2.0D), 2.0D);
		distance = RADIUS_OF_EARTH * Math.atan2(Math.sqrt(a), Math.sqrt(1.0D - a));
		return distance;
	}

	public static String getCurrentDateTime() {
		return dateFormate2.format(new Date());
	}

	public static int calculateAge(Date birthDate) {
		int years = 0;
		int months = 0;
		int days = 0;
		// create calendar object for birth day
		Calendar birthDay = Calendar.getInstance();
		birthDay.setTimeInMillis(birthDate.getTime());
		// create calendar object for current day
		long currentTime = System.currentTimeMillis();
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(currentTime);
		// Get difference between years
		years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		int currMonth = now.get(Calendar.MONTH) + 1;
		int birthMonth = birthDay.get(Calendar.MONTH) + 1;
		// Get difference between months
		months = currMonth - birthMonth;
		// if month difference is in negative then reduce years by one and
		// calculate the number of months.
		if (months < 0) {
			years--;
			months = 12 - birthMonth + currMonth;
			if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
				months--;
		} else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
			years--;
			months = 11;
		}
		// Calculate the days
		if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
			days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
		else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
			int today = now.get(Calendar.DAY_OF_MONTH);
			now.add(Calendar.MONTH, -1);
			days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
		} else {
			days = 0;
			if (months == 12) {
				years++;
				months = 0;
			}
		}
		// Create new Age object
		return years;
	}

	public static void main(String[] args) throws ParseException {
		/* SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	      Date birthDate;
		try {
			birthDate = sdf.parse("29/11/1981");
			int age = calculateAge(birthDate);
			System.out.println(age);
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d1 = sdf.parse("2018-07-02 15:43:22");
		Date d2 = new Date();

		long diff = d2.getTime() - d1.getTime();
		long diffSeconds = diff / (1000 * 60);
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000);
		int diffInDays = (int) diff / (1000 * 60 * 60 * 24);
		System.err.println("diff in seconds ::" + diffSeconds);
		System.err.println("diff in min ::" + diffMinutes);
		System.err.println("diff in hour ::" + diffHours);
		System.err.println("diff in days ::" + diffInDays);
		
		
	}
	
	public static double getMonthlyTransactionLimit(PQAccountDetail accountDetail, String type) {
		try {
			if (accountDetail.getUserMaxLimit() != null) {
				switch (type.toUpperCase()) {
				case "BANK":
					return accountDetail.getUserMaxLimit().getUserMonthlyAmountOfBT();
				case "MERCHANT":
					return accountDetail.getUserMaxLimit().getMerchantMonthlyAmountOfTxn();
				default:
					return accountDetail.getUserMaxLimit().getUserMonthlyAmountOfTxn();
				}
			} else {
				switch (type.toUpperCase()) {
				case "BANK":
					return 15000;
				case "MERCHANT":
					return accountDetail.getAccountType().getMonthlyLimit();
				default:
					return accountDetail.getAccountType().getMonthlyLimit();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static double getDailyTransactionLimit(PQAccountDetail accountDetail, String type) {
		try {
			if (accountDetail.getUserMaxLimit() != null) {
				switch (type.toUpperCase()) {
				case "BANK":
					return accountDetail.getUserMaxLimit().getUserDailyAmountOfBT();
				case "MERCHANT":
					return accountDetail.getUserMaxLimit().getMerchantDailyAmountOfTxn();
				default:
					return accountDetail.getUserMaxLimit().getUserDailyAmountOfTxn();
				}
			} else {
				switch (type.toUpperCase()) {
				case "BANK":
					return 5000;
				case "MERCHANT":
					return accountDetail.getAccountType().getDailyLimit();
				default:
					return accountDetail.getAccountType().getDailyLimit();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static long getDailyCountLimit(PQAccountDetail accountDetail, String type) {
		try {
			if (accountDetail.getUserMaxLimit() != null) {
				switch (type.toUpperCase()) {
				case "BANK":
					return accountDetail.getUserMaxLimit().getUserDailyNoOfBT();
				case "MERCHANT":
					return accountDetail.getUserMaxLimit().getMerchantDailyNoOfTxn();
				default:
					return accountDetail.getUserMaxLimit().getUserDailyNoOfTxn();
				}
			} else {
				switch (type.toUpperCase()) {
				case "BANK":
					return 1;
				case "MERCHANT":
					return 5000;
				default:
					return 5000;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static long getMonthlyCountLimit(PQAccountDetail accountDetail, String type) {
		try {
			if (accountDetail.getUserMaxLimit() != null) {
				switch (type.toUpperCase()) {
				case "BANK":
					return accountDetail.getUserMaxLimit().getUserMonthlyNoOfBT();
				case "MERCHANT":
					return accountDetail.getUserMaxLimit().getMerchantMonthlyNoOfTxn();
				default:
					return accountDetail.getUserMaxLimit().getUserMonthlyNoOfTxn();
				}
			} else {
				switch (type.toUpperCase()) {
				case "BANK":
					return 3;
				case "MERCHANT":
					return 5000;
				default:
					return 5000;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	

}
