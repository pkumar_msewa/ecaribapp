package com.letsManage.model.dto;

public class GetMainList {

private long userCount;
	
	private long transactionCount;
	
	private long loadMoneyCount;
	
	private long rechargeCount;
	
	private long billPayCount;
	
	private long totalUsersprev1;
	
	private long totalTransprev1;
	
    private long totalUsersprev2;
	
	private long totalTransprev2;


	public long getTotalUsersprev1() {
		return totalUsersprev1;
	}

	public void setTotalUsersprev1(long totalUsersprev1) {
		this.totalUsersprev1 = totalUsersprev1;
	}

	public long getTotalTransprev1() {
		return totalTransprev1;
	}

	public void setTotalTransprev1(long totalTransprev1) {
		this.totalTransprev1 = totalTransprev1;
	}

	public long getTotalUsersprev2() {
		return totalUsersprev2;
	}

	public void setTotalUsersprev2(long totalUsersprev2) {
		this.totalUsersprev2 = totalUsersprev2;
	}

	public long getTotalTransprev2() {
		return totalTransprev2;
	}

	public void setTotalTransprev2(long totalTransprev2) {
		this.totalTransprev2 = totalTransprev2;
	}

	

	public long getUserCount() {
		return userCount;
	}

	public void setUserCount(long userCount) {
		this.userCount = userCount;
	}

	public long getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(long transactionCount) {
		this.transactionCount = transactionCount;
	}

	public long getLoadMoneyCount() {
		return loadMoneyCount;
	}

	public void setLoadMoneyCount(long loadMoneyCount) {
		this.loadMoneyCount = loadMoneyCount;
	}

	public long getRechargeCount() {
		return rechargeCount;
	}

	public void setRechargeCount(long rechargeCount) {
		this.rechargeCount = rechargeCount;
	}

	public long getBillPayCount() {
		return billPayCount;
	}

	public void setBillPayCount(long billPayCount) {
		this.billPayCount = billPayCount;
	}

	
	
}
