package com.letsManage.model.dto;

public class TransChartCount {
	private long present;

	private long current;

	private long prevmonth1;
	
	private long prevmonth2;

	public long getCurrent() {
		return current;
	}

	public void setCurrent(long current) {
		this.current = current;
	}

	public long getPrevmonth1() {
		return prevmonth1;
	}

	public void setPrevmonth1(long prevmonth1) {
		this.prevmonth1 = prevmonth1;
	}

	public long getPrevmonth2() {
		return prevmonth2;
	}

	public void setPrevmonth2(long prevmonth2) {
		this.prevmonth2 = prevmonth2;
	}

	public long getPresent() {
		return present;
	}

	public void setPresent(long present) {
		this.present = present;
	}
    
	
}
