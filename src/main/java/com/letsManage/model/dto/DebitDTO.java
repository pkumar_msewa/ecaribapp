/**
 * 
 */
package com.letsManage.model.dto;

import java.util.List;

/**
 * @author Admin
 *
 */
public class DebitDTO {

	private List<DebitCreditListDTO> listDTO;

	public List<DebitCreditListDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<DebitCreditListDTO> listDTO) {
		this.listDTO = listDTO;
	}

	
	
}
