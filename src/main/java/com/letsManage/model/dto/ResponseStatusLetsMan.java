package com.letsManage.model.dto;



public enum ResponseStatusLetsMan {

	SUCCESS("Success","S00"),
	
	FAILURE("Failure","F00"),

	INTERNAL_SERVER_ERROR("Internal Server Error","F02"),

    BAD_REQUEST("Bad Request","F04"),

	UNAUTHORIZED_ROLE("Un-Authorized Role","F06"),
	
	INVALID_VERSION("Invalid Version","F08");


	private ResponseStatusLetsMan() {

	}
	
	private String key;

	private String value;

	private ResponseStatusLetsMan(String key,String value) {
		this.key=key;
		this.value = value;
	}
	
	public String getKey(){
		return key;
	}
	public String getValue() {
		return value;
	}

	public static ResponseStatusLetsMan getEnumByValue(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (ResponseStatusLetsMan v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
	
	public static ResponseStatusLetsMan getEnumByKey(String key) {
		if (key == null)
			throw new IllegalArgumentException();
		for (ResponseStatusLetsMan v : values())
			if (key.equalsIgnoreCase(v.getKey()))
				return v;
		throw new IllegalArgumentException();
	}

}
