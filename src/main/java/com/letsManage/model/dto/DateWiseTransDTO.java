package com.letsManage.model.dto;

import java.util.ArrayList;
import java.util.List;

public class DateWiseTransDTO {
private List<UserTransactionListDTO> listDTO;
	
	public List<UserTransactionListDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<UserTransactionListDTO> listDTO) {
		this.listDTO = listDTO;
	}

	public DateWiseTransDTO() {
		super();
		this.listDTO =  new ArrayList<>();
	}
   
}
