package com.letsManage.model.dto;

public class UserTypeDto {

	public String userType;

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
}
