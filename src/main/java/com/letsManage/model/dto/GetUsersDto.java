package com.letsManage.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.payqwikapp.model.admin.UserListDTO;

public class GetUsersDto {
private List<UserListDTO> listDTO;

public List<UserListDTO> getListdto() {
	return listDTO;
}

public void setListdto(List<UserListDTO> listdto) {
	this.listDTO = listdto;
}
public GetUsersDto() {
	super();
	this.listDTO = new ArrayList<>();
}
}
