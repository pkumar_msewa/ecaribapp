package com.letsManage.model.dto;

public class BulkSmsRequest {
	 private String userType;
	   
	    private String content;

	    public String getContent() {
	        return content;
	    }

	    public void setContent(String content) {
	        this.content = content;
	    }

		public String getUserType() {
			return userType;
		}

		public void setUserType(String userType) {
			this.userType = userType;
		}
	   
}
