package com.letsManage.model.dto;

import java.util.ArrayList;
import java.util.List;

public class GetAllUserList {
	
	
	
	private long debitCount;
	
	private long creditCount;
	
	private List<UserTransactionListDTO> listDTO;
	
	public List<UserTransactionListDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<UserTransactionListDTO> listDTO) {
		this.listDTO = listDTO;
	}

	
	public long getDebitCount() {
		return debitCount;
	}

	public void setDebitCount(long debitCount) {
		this.debitCount = debitCount;
	}

	public long getCreditCount() {
		return creditCount;
	}

	public void setCreditCount(long creditCount) {
		this.creditCount = creditCount;
	}
	
	private TransChartCount transCount;

	public TransChartCount getTransCount() {
		return transCount;
	}

	public void setTransCount(TransChartCount transCount) {
		this.transCount = transCount;
	}
	public GetAllUserList() {
		super();
		this.listDTO = new ArrayList<>();
	}
}
