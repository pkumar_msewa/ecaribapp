package com.letsManage.model.dto;

import java.util.List;

public class SuperAdminListDTO {

	private List<SuperAdminDTO> listDTO;

	public List<SuperAdminDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<SuperAdminDTO> listDTO) {
		this.listDTO = listDTO;
	}

}
