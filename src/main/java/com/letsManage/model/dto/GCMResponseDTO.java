package com.letsManage.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.payqwikapp.model.admin.UserListDTO;

public class GCMResponseDTO {
	private long totalPages;
    public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

	public List<String> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<String> listDTO) {
		this.listDTO = listDTO;
	}

	private List<String> listDTO;

	public List<String> getListdto() {
		return listDTO;
	}

	public void setListdto(List<String> listdto) {
		this.listDTO = listdto;
	}
	public GCMResponseDTO() {
		super();
		this.listDTO = new ArrayList<>();
	}
}
