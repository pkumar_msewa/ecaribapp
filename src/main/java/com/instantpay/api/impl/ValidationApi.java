package com.instantpay.api.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.Validation;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.model.response.ValidationResponse;
import com.instantpay.util.InstantPayConstants;
import com.payqwikapp.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class ValidationApi implements IValidationApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public ValidationResponse request(ValidationRequest request) {
		ValidationResponse response = new ValidationResponse();
		Validation validationResponse = new Validation();
		try {
			String stringResponse = "";
			WebResource resource = Client.create().resource(InstantPayConstants.URL_VALIDATION)
					.queryParam(InstantPayConstants.API_KEY_SPKEY, request.getSpKey())
					.queryParam(InstantPayConstants.API_KEY_AGENTID, request.getAgentId())
					.queryParam(InstantPayConstants.API_KEY_ACCOUNT, request.getAccount())
					.queryParam(InstantPayConstants.API_KEY_AMOUNT, request.getAmount())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL1, request.getOptional1())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL2, request.getOptional2())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL3, request.getOptional3())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL4, request.getOptional4())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL5, request.getOptional5());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			if (clientResponse.getStatus() == 200) {
				stringResponse = clientResponse.getEntity(String.class);
				JSONObject o = new JSONObject(stringResponse);
				String ipay_errorcode = JSONParserUtil.getString(o, "ipay_errorcode");
				response.setSuccess(false);
				if(ipay_errorcode != null) {
					String ipay_errordesc = JSONParserUtil.getString(o, "​ipay_errordesc");
					validationResponse.setIpayErrorCode(ipay_errorcode);
					validationResponse.setIpayErrorDesc(ipay_errordesc);
					if(ipay_errorcode.equalsIgnoreCase("TXN")) {
						response.setSuccess(true);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ValidationResponse validationRequest(ValidationRequest request) {
		ValidationResponse response = new ValidationResponse();
		try {
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_VALIDATEREQUEST)
					.queryParam(InstantPayConstants.API_KEY_SPKEY,
							(request.getSpKey() == null) ? "" : request.getSpKey())
					.queryParam(InstantPayConstants.API_KEY_ACCOUNT,
							(request.getAccount() == null) ? "" : request.getAccount())
					.queryParam(InstantPayConstants.API_KEY_AMOUNT,
							(request.getAmount() == null) ? "" : request.getAmount());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			if (clientResponse.getStatus() == 200) {
				stringResponse = clientResponse.getEntity(String.class);
				JSONObject o = new JSONObject(stringResponse);
				logger.info("o ::" + stringResponse);
				String ipay_errorcode = JSONParserUtil.getString(o, "code");
				response.setSuccess(false);
				if(ipay_errorcode != null) {
					String ipay_errordesc = JSONParserUtil.getString(o, "message");
					response.setIpayErrorCode(ipay_errorcode);
					response.setIpayErrorDesc(ipay_errordesc);
					if(ipay_errorcode.equalsIgnoreCase("TXN")) {
						response.setSuccess(JSONParserUtil.getBoolean(o, "success"));
					}
				}else{
					response.setIpayErrorDesc(JSONParserUtil.getString(o, "message"));
					response.setIpayErrorCode(JSONParserUtil.getString(o, "code"));
				}
			}else {
				response.setSuccess(false);
				Validation validation = new Validation();
				validation.setIpayErrorCode("ERROR");
				validation.setIpayErrorDesc("Service down");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			Validation validation = new Validation();
			validation.setIpayErrorCode("ERROR");
			validation.setIpayErrorDesc("Service down");
		}
		return response;
	}
}
