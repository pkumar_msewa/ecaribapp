package com.instantpay.api.impl;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.instantpay.api.ITransactionApi;
import com.instantpay.model.Transaction;
import com.instantpay.model.Validation;
import com.instantpay.model.request.TransactionRequest;
import com.instantpay.model.response.TransactionResponse;
import com.instantpay.util.InstantPayConstants;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.TopupType;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class TransactionApi implements ITransactionApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public TransactionResponse request(TransactionRequest request, PQService service) {
		TransactionResponse response = new TransactionResponse();
		try {
			
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_TRANSACTION)
					.queryParam(InstantPayConstants.API_KEY_SPKEY,
							(request.getSpKey() == null) ? "" : service.getOperatorCode())
					.queryParam(InstantPayConstants.API_KEY_AGENTID,
							(request.getAgentId() == null) ? "" : request.getAgentId())
					.queryParam(InstantPayConstants.API_KEY_ACCOUNT,
							(request.getAccount() == null) ? "" : request.getAccount())
					.queryParam(InstantPayConstants.API_KEY_AMOUNT,
							(request.getAmount() == null) ? "" : request.getAmount())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL1,
							(request.getOptional1() == null) ? "" : request.getOptional1())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL2,
							(request.getOptional2() == null) ? "" : request.getOptional2())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL3,
							(request.getOptional3() == null) ? "" : request.getOptional3())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL4,
							(request.getOptional4() == null) ? "" : request.getOptional4())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL5,
							(request.getOptional5() == null) ? "" : request.getOptional5())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL6,
					        (request.getOptional6() == null) ? "" : request.getOptional6())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL7,
					        (request.getOptional7() == null) ? "" : request.getOptional7())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL8,
					        (request.getOptional8() == null) ? "" : request.getOptional8())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL9,
					        (request.getOptional9() == null) ? "" : request.getOptional9())
			        .queryParam(InstantPayConstants.API_KEY_BBPS_OUTLET_ID,
					        (request.getOutletId() == null) ? "" : request.getOutletId())
			        .queryParam(InstantPayConstants.API_KEY_PAYMENT_CHANNEL,
					        (request.getPaymentChannel() == null) ? "" : request.getPaymentChannel())
			        .queryParam(InstantPayConstants.API_KEY_PAYMENT_MODE,
					        (request.getPaymentMode() == null) ? "" : request.getPaymentMode())
			        .queryParam(InstantPayConstants.API_KEY_CUNSUMER_MOBILE,
					        (request.getCustomerMobile() == null) ? "" : request.getCustomerMobile());
			
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			if (clientResponse.getStatus() == 200) {
				stringResponse = clientResponse.getEntity(String.class);
				org.json.JSONObject o = new org.json.JSONObject(stringResponse);
				if (JSONParserUtil.checkKey(o, "res_code")) {
					Transaction transactionResponse = new Transaction();
					String res_code = JSONParserUtil.getString(o, "res_code");
					transactionResponse.setResCode(res_code);
					String res_msg = JSONParserUtil.getString(o, "res_msg");
					transactionResponse.setResMsg(res_msg);
					String ipay_id = JSONParserUtil.getString(o, "ipay_id");
					transactionResponse.setIpayId(ipay_id);
					String agent_id = JSONParserUtil.getString(o, "agent_id");
					transactionResponse.setAgentId(agent_id);
					String opr_id = JSONParserUtil.getString(o, "opr_id");
					transactionResponse.setOprId(opr_id);
					String account_no = JSONParserUtil.getString(o, "account_no");
					transactionResponse.setAccountNo(account_no);
					String sp_key = JSONParserUtil.getString(o, "sp_key");
					transactionResponse.setSpKey(sp_key);
					String trans_amt = JSONParserUtil.getString(o, "trans_amt");
					transactionResponse.setTransAmt(trans_amt);
					String charged_amt = JSONParserUtil.getString(o, "charged_amt");
					transactionResponse.setChargedAmt(charged_amt);
					String opening_bal = JSONParserUtil.getString(o, "opening_bal");
					transactionResponse.setOpeningBal(opening_bal);
					String datetime = JSONParserUtil.getString(o, "datetime");
					transactionResponse.setDateTime(datetime);
					String status = JSONParserUtil.getString(o, "status");
					transactionResponse.setStatus(status);
					response.setTransaction(transactionResponse);
					Validation validationResponse = new Validation();
				     validationResponse.setIpayErrorCode(res_code);
				     validationResponse.setIpayErrorDesc(res_msg);
					response.setValidation(validationResponse);
					response.setSuccess(true);
				}else if (JSONParserUtil.checkKey(o, "ipay_errorcode")) {
					Validation validationResponse = new Validation();
					String ipay_errorcode = JSONParserUtil.getString(o, "ipay_errorcode");
					validationResponse.setIpayErrorCode(ipay_errorcode);
					String ipay_errordesc = (ipay_errorcode.equalsIgnoreCase("IAB"))?"Recharge services are temporarily down":JSONParserUtil.getString(o, "ipay_errordesc");
					validationResponse.setIpayErrorDesc(ipay_errordesc);
					response.setValidation(validationResponse);
					response.setTransaction(null);
					response.setSuccess(false);
				}
			}else {
				response.setSuccess(false);
				Validation validation = new Validation();
				validation.setIpayErrorCode("ERROR");
				validation.setIpayErrorDesc("Service down");
				response.setValidation(validation);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			Validation validation = new Validation();
			validation.setIpayErrorCode("ERROR");
			validation.setIpayErrorDesc("Service down");
			response.setValidation(validation);
		}
		return response;
	}

	@Override
	public TransactionResponse rechargeRequestToMdex(TransactionRequest request) {
		TransactionResponse resp = new TransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("amount", request.getAmount());
			payload.put("transactionId", request.getTransactionId());
			payload.put("topupType",request.getTopupType());
			payload.put("area", request.getArea());
			payload.put("additionalInfo", request.getAdditionalInfo());
			if(TopupType.Dth.equals(request.getTopupType())) {
			payload.put("dthNo", request.getDthNo());
			}
			if(TopupType.Landline.equals(request.getTopupType())) {
			payload.put("stdCode", request.getStdCode());
			payload.put("landlineNumber", request.getLandlineNumber());
			}
			if(TopupType.Electricity.equals(request.getTopupType())) {
				payload.put("cycleNumber", (request.getCycleNumber() == null) ? "" : request.getCycleNumber());
				payload.put("cityName", (request.getCityName() == null) ? "" : request.getCityName());
				payload.put("billingUnit", (request.getBillingUnit() == null)? "" :request.getBillingUnit());
				payload.put("processingCycle", (request.getProcessingCycle() == null) ? "" : request.getProcessingCycle());
			}
			if(TopupType.Insurance.equals(request.getTopupType())) {
				payload.put("policyDate", (request.getPolicyDate() == null) ? "" : request.getPolicyDate());
			}
			payload.put("accountNumber", request.getAccountNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(InstantPayConstants.PREPAID_RECHARGE_FROM_MDEX_URL);
			ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null && strResponse!="") {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
					String code = JSONParserUtil.getString(jobj, "code");
					String message = JSONParserUtil.getString(jobj, "message");
					//transactionId,referenceNo,operatorId
					if("S00".equalsIgnoreCase(code) || "P00".equalsIgnoreCase(code)) {
						Transaction transactionResponse = new Transaction();
						transactionResponse.setResCode(code);
						transactionResponse.setResMsg(message);
						transactionResponse.setOprId(JSONParserUtil.getString(jobj, "operatorId"));
						transactionResponse.setIpayId(JSONParserUtil.getString(jobj, "referenceNo"));
						transactionResponse.setStatus("Success");
						resp.setTransaction(transactionResponse);
						resp.setSuccess(true);
						if("P00".equalsIgnoreCase(code)) {
						resp.setSuspicious(true);	
						}
					}else {
						Validation validationResponse = new Validation();
						validationResponse.setIpayErrorCode(code);
						validationResponse.setIpayErrorDesc(message);
						resp.setValidation(validationResponse);
						resp.setSuccess(false);
					}
					} 
				} else {
					Validation validationResponse = new Validation();
					validationResponse.setIpayErrorCode("F00");
					validationResponse.setIpayErrorDesc("Error connecting to operator");
					resp.setValidation(validationResponse);
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}
	
	@Override
	public TransactionResponse checkMdexStatus(String refNo) {
		TransactionResponse resp = new TransactionResponse();
		org.codehaus.jettison.json.JSONObject jobject = new org.codehaus.jettison.json.JSONObject();
		try {
			jobject.put("transactionId", refNo);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.MDEX_TRANSACTION_STATUS);
			ClientResponse clientResponse = resource.accept("application/json").type("application/json")
					.post(ClientResponse.class, jobject);
			if (clientResponse.getStatus() != 200) {
				resp.setSuspicious(true);
			} else {
				String strResponse = clientResponse.getEntity(String.class);
				if (strResponse != null && strResponse!="") {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
					String code = JSONParserUtil.getString(jobj, "code");
					String message = JSONParserUtil.getString(jobj, "message");
					String status = JSONParserUtil.getString(jobj, "status");
					if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
						resp.setSuccess(true);
						resp.setSuspicious(false);
						resp.setStatus(status);
					}else if(ResponseStatus.FAILURE.getValue().equalsIgnoreCase(code)) {
						resp.setSuccess(false);
						resp.setSuspicious(false);
						resp.setStatus(status);
					}else {
						resp.setSuspicious(true);
					}
					}
			}else {
				resp.setSuspicious(true);
			}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			resp.setSuspicious(true);
		}
		return resp;
		}
	
}
