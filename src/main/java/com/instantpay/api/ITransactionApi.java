package com.instantpay.api;

import com.instantpay.model.request.TransactionRequest;
import com.instantpay.model.response.TransactionResponse;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.model.mobile.ResponseDTO;

public interface ITransactionApi {
	
	TransactionResponse request(TransactionRequest request, PQService service);
	
	TransactionResponse rechargeRequestToMdex(TransactionRequest request);
	
TransactionResponse checkMdexStatus(String refNo);


}
