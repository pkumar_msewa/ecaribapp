package com.instantpay.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.instantpay.model.request.TransactionRequest;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.DTHBillPaymentDTO;
import com.payqwikapp.model.ElectricityBillPaymentDTO;
import com.payqwikapp.model.GasBillPaymentDTO;
import com.payqwikapp.model.InsuranceBillPaymentDTO;
import com.payqwikapp.model.LandlineBillPaymentDTO;
import com.payqwikapp.model.MobileTopupDTO;

public class IPayConvertUtil {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static TransactionRequest prepaidRequest(CommonRechargeDTO dto) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getMobileNo());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		if (dto.getServiceProvider().equalsIgnoreCase("ATP")) {
			request.setOptional5(InstantPayConstants.OUTLET_ID);
		}
		String transactionReferenceNumber = System.currentTimeMillis() + "";
		request.setAgentId(transactionReferenceNumber);
		return request;
	}
	public static TransactionRequest postpaidRequest(CommonRechargeDTO dto) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getMobileNo());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		request.setAgentId(InstantPayConstants.AGENT_ID);
		return request;
	}
	public static TransactionRequest datacardRequest(MobileTopupDTO dto) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getMobileNo());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		if (dto.getServiceProvider().equalsIgnoreCase("ATP")) {
			request.setOptional5(InstantPayConstants.AGENT_ID);
		}
		request.setAgentId(InstantPayConstants.AGENT_ID);
		return request;
	}
	public static TransactionRequest dthRequest(CommonRechargeDTO dto) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getDthNo());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		request.setAgentId(InstantPayConstants.AGENT_ID);
		return request;
	}
	public static TransactionRequest landlineRequest(CommonRechargeDTO dto) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getLandlineNumber());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		request.setOptional1(dto.getStdCode());
		if (dto.getServiceProvider().equalsIgnoreCase("BGL")) {
			request.setOptional2(dto.getAccountNumber());
			request.setOptional3("LLI");
		}
		request.setAgentId(InstantPayConstants.AGENT_ID);
		return request;
	}
	public static TransactionRequest electricityRequest(CommonRechargeDTO dto,boolean bbpsEnabled,String username) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getAccountNumber());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		if (dto.getServiceProvider().equalsIgnoreCase("VREE")) {
			request.setOptional1(dto.getCycleNumber());
		}else if(dto.getServiceProvider().equalsIgnoreCase("VTPE")){
			request.setOptional1(dto.getCityName());
		}else if(dto.getServiceProvider().equalsIgnoreCase("VMDE")){
			request.setOptional1(dto.getBillingUnit());
		}
		request.setAgentId(InstantPayConstants.AGENT_ID);
		
		if(bbpsEnabled) {
			request.setOutletId(InstantPayConstants.BBPS_OUTLET_ID);
			request.setOptional8(InstantPayConstants.optional8);
			request.setOptional9(InstantPayConstants.optional9);
			request.setCustomerMobile(username);
			request.setPaymentMode(InstantPayConstants.PAYMENT_MODE);
			request.setPaymentChannel(InstantPayConstants.PAYMENT_CHANNEL);
			}
		
		return request;
	}
	public static TransactionRequest gasRequest(CommonRechargeDTO dto,boolean bbpsEnabled,String username) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getAccountNumber());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		request.setAgentId(InstantPayConstants.AGENT_ID);
		if(bbpsEnabled) {
			request.setOutletId(InstantPayConstants.BBPS_OUTLET_ID);
			request.setOptional8(InstantPayConstants.optional8);
			request.setOptional9(InstantPayConstants.optional9);
			request.setCustomerMobile(username);
			request.setPaymentMode(InstantPayConstants.PAYMENT_MODE);
			request.setPaymentChannel(InstantPayConstants.PAYMENT_CHANNEL);
			}
		return request;
	}
	public static TransactionRequest insuranceRequest(CommonRechargeDTO dto) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getPolicyNumber());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		request.setOptional1(dto.getPolicyDate());
		request.setAgentId(InstantPayConstants.AGENT_ID);
		return request;
	}
	
	public static TransactionRequest waterRequest(CommonRechargeDTO dto,boolean bbpsEnabled,String username) {
		TransactionRequest request = new TransactionRequest();
		request.setAccount(dto.getAccountNumber());
		request.setAmount(dto.getAmount());
		request.setSpKey(dto.getServiceProvider());
		request.setAgentId(InstantPayConstants.AGENT_ID);
		if(bbpsEnabled) {
			request.setOutletId(InstantPayConstants.BBPS_OUTLET_ID);
			request.setOptional8(InstantPayConstants.optional8);
			request.setOptional9(InstantPayConstants.optional9);
			request.setCustomerMobile(username);
			request.setPaymentMode(InstantPayConstants.PAYMENT_MODE);
			request.setPaymentChannel(InstantPayConstants.PAYMENT_CHANNEL);
			}
		return request;
	}
}
