package com.instantpay.util;

import com.payqwikapp.util.DeploymentConstants;

public class InstantPayConstants {

	private static final String URL = DeploymentConstants.WEB_URL+"/InstantPay/"; // VPAYQWIK

	public static final double WALLET_THRESHOLD = 10000;
	public static final String URL_TRANSACTION = URL + "Transaction/Process";
	public static final String URL_VALIDATEREQUEST = URL+"ValidateRequest/Process";
	public static final String URL_BALANCE = URL + "Balance/Process";
	public static final String URL_STATUS = URL + "StatusCheck/Process";
	public static final String URL_SERVICE = URL + "Services/Process";
	public static final String URL_VALIDATION = URL + "Validation/Process";
	

	// 210.212.204.39 - Vijaya Bank (USE THIS PUBLIC IP TOKEN)
	public static final String AGENT_ID = "565Y10837"; // MSEWA Dealer code
	public static final String OUTLET_ID = "10019339"; // MSEWA Outlet ID for 66.207.206.54
	public static final String API_KEY_SPKEY = "spKey";
	public static final String API_KEY_AGENTID = "agentId";
	public static final String API_KEY_ACCOUNT = "account";
	public static final String API_KEY_AMOUNT = "amount";
	public static final String API_KEY_OPTIONAL1 = "optional1";
	public static final String API_KEY_OPTIONAL2 = "optional2";
	public static final String API_KEY_OPTIONAL3 = "optional3";
	public static final String API_KEY_OPTIONAL4 = "optional4";
	public static final String API_KEY_OPTIONAL5 = "optional5";
	public static final String API_KEY_OPTIONAL6 = "optional6";
	public static final String API_KEY_OPTIONAL7 = "optional7";
	public static final String API_KEY_OPTIONAL8 = "optional8";
	public static final String API_KEY_OPTIONAL9 = "optional9";
	public static final String API_KEY_BBPS_OUTLET_ID = "outletid";
	public static final String API_KEY_PAYMENT_CHANNEL= "paymentchannel";
	public static final String API_KEY_PAYMENT_MODE = "paymentmode";
	public static final String API_KEY_CUNSUMER_MOBILE = "customermobile";
	
	public static final String API_KEY_TYPE = "type";
	public static final String USERNAME = "instantpay@ecarib.in";
	public static final String BBPS_OUTLET_ID = "751";
	public static final String PAYMENT_CHANNEL = "AGT";
	public static final String PAYMENT_MODE = "CASH";
	public static final String optional8 = "Remarks";
	public static final String optional9 = "12.9369,77.6407|560095";
	public static final String CUNSUMER_MOBILE = "7022620747";
	
	
	
	public static final String PREPAID_RECHARGE_FROM_MDEX_URL = DeploymentConstants.WEB_URL + "/Recharge/Mdex/Prepaid";
	public static final String MDEX_TRANSACTION_STATUS =DeploymentConstants.WEB_URL + "/Recharge/Mdex/Status";

}
