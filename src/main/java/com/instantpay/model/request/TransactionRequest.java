package com.instantpay.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionRequest extends MdexTransactionRequest {

	private String spKey;
	private String agentId;
	private String account;
	private String amount;
	private String optional1;
	private String optional2;
	private String optional3;
	private String optional4;
	private String optional5;
	private String optional6;
	private String optional7;
	private String optional8;
	private String optional9;
	private String outletId;
	private String customerMobile;
	private String paymentMode;
	private String paymentChannel;
	private String endpointIp;
	private String stdCode;
	private String landlineNumber;
	private String accountNumber;
	private String cycleNumber;
	private String billingUnit;
	private String cityName;
	private String processingCycle;
	private String additionalInfo;
	private String policyNumber;
	private String policyDate;
	
	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyDate() {
		return policyDate;
	}

	public void setPolicyDate(String policyDate) {
		this.policyDate = policyDate;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getSpKey() {
		return spKey;
	}

	public void setSpKey(String spKey) {
		this.spKey = spKey;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOptional1() {
		return optional1;
	}

	public void setOptional1(String optional1) {
		this.optional1 = optional1;
	}

	public String getOptional2() {
		return optional2;
	}

	public void setOptional2(String optional2) {
		this.optional2 = optional2;
	}

	public String getOptional3() {
		return optional3;
	}

	public void setOptional3(String optional3) {
		this.optional3 = optional3;
	}

	public String getOptional4() {
		return optional4;
	}

	public void setOptional4(String optional4) {
		this.optional4 = optional4;
	}

	public String getOptional5() {
		return optional5;
	}

	public void setOptional5(String optional5) {
		this.optional5 = optional5;
	}

	public String getOptional6() {
		return optional6;
	}

	public void setOptional6(String optional6) {
		this.optional6 = optional6;
	}

	public String getOptional7() {
		return optional7;
	}

	public void setOptional7(String optional7) {
		this.optional7 = optional7;
	}

	public String getOptional8() {
		return optional8;
	}

	public void setOptional8(String optional8) {
		this.optional8 = optional8;
	}

	public String getOptional9() {
		return optional9;
	}

	public void setOptional9(String optional9) {
		this.optional9 = optional9;
	}

	public String getOutletId() {
		return outletId;
	}

	public void setOutletId(String outletId) {
		this.outletId = outletId;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	public String getEndpointIp() {
		return endpointIp;
	}

	public void setEndpointIp(String endpointIp) {
		this.endpointIp = endpointIp;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCycleNumber() {
		return cycleNumber;
	}

	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}

	public String getBillingUnit() {
		return billingUnit;
	}

	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProcessingCycle() {
		return processingCycle;
	}

	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}
	
	

}
