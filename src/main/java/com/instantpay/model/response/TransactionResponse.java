package com.instantpay.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.instantpay.model.Transaction;
import com.instantpay.model.Validation;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionResponse {

	private boolean success;
	private Transaction transaction;
	private Validation validation;
	private String details;
	private boolean suspicious;
	private String status;
	private double remainingBalance;
	
	public double getRemainingBalance() {
		return remainingBalance;
	}

	public void setRemainingBalance(double remainingBalance) {
		this.remainingBalance = remainingBalance;
	}

	public boolean isSuspicious() {
		return suspicious;
	}

	public void setSuspicious(boolean suspicious) {
		this.suspicious = suspicious;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Validation getValidation() {
		return validation;
	}

	public void setValidation(Validation validation) {
		this.validation = validation;
	}
	
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TransactionResponse [success = " + success + ", transaction = " + ((transaction==null)?null:(transaction.toString())) + ", validation = "
				+ ((validation==null)?null:(validation.toString())) + "]";
	}

}
