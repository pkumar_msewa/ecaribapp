package com.instantpay.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.instantpay.model.Validation;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidationResponse extends  Validation{

	private boolean success;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	}
