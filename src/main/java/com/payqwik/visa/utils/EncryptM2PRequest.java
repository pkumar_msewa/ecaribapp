package com.payqwik.visa.utils;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.thirdparty.model.request.Merchant;
import com.thirdparty.model.request.MerchantSignUp;
import com.thirdparty.model.request.VisaMerchantRequest;

public class EncryptM2PRequest {

	public static String buildEncryptedJsonForM2P(VisaMerchantRequest request)
	{
		String enc=null;
	
		MerchantSignUp sign=new MerchantSignUp();
		Merchant en = new Merchant();
		MerchantBankDetail mDetails = new MerchantBankDetail();
		SettlementBankDetail sDetails = new SettlementBankDetail();
		en.setFirstName(request.getFirstName());
		en.setLastName(request.getLastName());
		en.setEmailAddress(request.getEmailAddress());
		en.setAddress1(request.getAddress1());
		en.setAddress2(request.getAddress1());
		en.setCity(request.getCity());
		en.setState(request.getState());
		en.setCountry(request.getCountry());
		en.setPinCode(request.getPinCode());
		en.setMerchantCategory(1);		
		en.setLattitude(Double.parseDouble("1234.23"));
		en.setLongitude(Double.parseDouble("1212.23"));
		en.setPanNo(request.getPanNo());
		en.setMobileNo(request.getMobileNo());
		en.setAggregator("M2P");
		
		mDetails.setAccName(request.getMerchantAccountName());
		mDetails.setBankAccNo(request.getBankAccountNo());
		mDetails.setBankIfscCode(request.getMerchantBankIfscCode());
		mDetails.setBankLocation(request.getSettlementBankLocation());
		mDetails.setBankName(request.getSettlementBankName());
		
		sDetails.setAccName(request.getMerchantAccountName());
		sDetails.setBankAccNo(request.getMerchantAccountNumber());
		sDetails.setBankIfscCode(request.getMerchantBankIfscCode());
		sDetails.setBankLocation(request.getSettlementBankLocation());
		sDetails.setBankName(request.getMerchantBankName());
		
		sign.setMerchant(en);
		sign.setMerchantBankDetail(mDetails);
		sign.setSettlementBankDetail(sDetails);
		
		
		ObjectMapper mapper = new ObjectMapper();
		String req1 = null;
		try {
			req1 = mapper.writeValueAsString(sign);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		SecurityTest sec = new SecurityTest();
		try {
			 enc=sec.encodeRequest(req1, "9876543211234562", "VIJAYA");
			 System.err.println("the encrypted response::"+enc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return enc;
	}
	
}
