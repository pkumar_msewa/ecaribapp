package com.payqwik.visa.utils;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikapp.model.VisaDTO;

import sun.misc.BASE64Decoder;


public class EncDecryptionRequest {

	private static final String ALGO = "AES";
	private static final String WORKING_KEY = "VIJAYABANKM2PBNK";
//
//	public static void main(String... a) {
//		JSONObject obj = new JSONObject();
//		try {
//			if(false)
//			System.out.println("A");
//			System.out.println("B");
//			obj.put("request", "0B4444444483214Ajay24531139BANGALORE42In53356M2PYN76039201");
//			obj.put("data", "10");
//			obj.put("sessionId", "eis9fc58ll371dkygrzepm38q");
//			obj.put("merchantName", "Merchant Ajay Test");
//			obj.put("merchantId", "562345346123");
//			obj.put("merchantAddress", "Bangalore");
//			try {
//				//System.out.println("en :: " + encrypt(obj.toString()));
//				//System.err.println("out :: " + decrypt(encrypt(obj.toString())));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

	public static String encrypt(String Data) throws Exception {
		Key key = generateKey(WORKING_KEY);
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		String encryptedValue = "";
		String ss = URLEncoder.encode(encryptedValue, "UTF-8");
		return ss;
	}

	public static String decrypt(String encryptedData) throws Exception {
		String d = URLDecoder.decode(encryptedData, "UTF-8");
		Key key = generateKey(WORKING_KEY);
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(d);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	private static Key generateKey(String workingKey) throws Exception {
		byte[] keyValue = workingKey.getBytes();
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}

	public static void main(String ...args) throws Exception {
//			getVisaDTO("U%2B5BNxz8mmTI5ESW%2B7V48nTIZbH5mf66RqSn%2F99iEnDGqGLQS6Kp3F%2BIAQjlbB6ZlrD9l6Qo%2BgYC%0AvyIHxdP8%2BLQzVB472JzPATWY3AKloJTscop%2FJq8WoCDFQd1jXtbX8cUbKg%2BcQ6UljrJlR0AGOAKM%0ALOBYAXJTnHoE2xHyRiDoa%2BMNhTN7GRF9SjNj4GyloNEXbuKJT6y8wLXxNL9gWSj0QIsVg7Z3pBp8%0AmbDy5ThC1jB3L6wHvD22INCH5V7DLcijJnfDnLoNV2ozDS5J9Rm%2B3Xbi%2B46rbwfYJwTDGM6BTlwn%0AEhE2l7UjRO7eMbaEqnKBcm%2FHx2o32W6wcmYJzyrTTItwt34DwRPVJsotqCA151fw9dtZ15TOvWdc%0AQR0bDWtvXpMCXwkDn%2BvHY2XI1SbS3Vo0LGzgp5OdAiq0si2zMe3rPD3Hwm3LTqFSW7h%2BU7osuFHj%0AoxqgBJSLLdsZ3sM7JSZTectCTgiTzZtv6kVYpFZv7RLPdx8rjXoBhkm8%0A");
//		getVisaDTO("U%2B5BNxz8mmTI5ESW%2B7V48nTIZbH5mf66RqSn%2F99iEnCCPmnjmT3fv5xKnnBnJ4Nn462C0RbGPjwx%0A%2FLyItiL2MGFpr2n8f%2FFQOk1yx%2Fq6aYhdO0HOQpPgEj%2FQT5tDuaxBuncelp9iuk%2BtNKKKCP6Uur0E%0AWvI109%2FDwXv1sY09jQoR2P%2FBnZzVupX3Yk29ZHGJ1bTXn72xotFOLqSHsXvmqLOYYzOeU%2Bh23zzL%0A8GAhUwJWOZ34Uy0jXQb7zfOzRCsH3EPElYv0Su%2BLGwbCH%2BAVqX9Xh234DYHWLSynA32opk2rlkpo%0A84OqQh8pZN1Vi7IGiSUCZpEtsEgaWgZG%2Bgp8DAw%2BOEGPi4vgxn51V0mTqL%2B7s%2B3M3xW5l3f%2F27Va%0AJOLHU0sc4P3mP%2FPy7UjVExaJzU2CIIOjvjSAbGEn8p0JSo01vT7kooQi%2BYYpiXWBLzrsSPpjRF7s%0AtPl3zAiuS%2FTOveYfdrWFg1luWqZrUdsLfoo%3D%0A");
		JSONObject obj = new JSONObject();
			obj.put("request", "00020101021102164054560000168096061661003000001179600811VIJB00013315204541153033565802IN5917SREE SAI CATERERS6009Bangalore61065600726304CB5A");
			obj.put("data", "10");
			obj.put("sessionId", "1efmy8282bzb4ggp7jsr6f96s");
			obj.put("merchantName", "SREE SAI CATERERS");
			obj.put("merchantId", "6100300000117960");
			obj.put("merchantAddress", "BANGALORE");
			obj.put("accountNumber", "5087070200009460");
			obj.put("additionalData", "");
			System.err.println("obj ::" + obj.toString());
		   System.out.println("encrypt :: " + encrypt(obj.toString()));	
	}

	public static VisaDTO getVisaDTO(String request) {

		VisaDTO visaDTO = new VisaDTO();
		try {
			String req = EncDecryptionRequest.decrypt(request);
			System.out.println("String after decrypt :: " + req);
			org.json.JSONObject obj = new org.json.JSONObject(req);
			visaDTO.setData(obj.getString("data"));
			visaDTO.setMerchantAddress(obj.getString("merchantAddress"));
			visaDTO.setMerchantId(obj.getString("merchantId"));
			visaDTO.setRequest(obj.getString("request"));
			visaDTO.setSessionId(obj.getString("sessionId"));
			visaDTO.setMerchantName(obj.getString("merchantName"));
			visaDTO.setAmount(Double.parseDouble(obj.getString("data")));
			visaDTO.setAdditionalData(obj.getString("additionalData"));
			visaDTO.setAccountNumber(obj.getString("accountNumber"));
			

//			 visaDTO.setData(1+"");
//			 visaDTO.setMerchantAddress("BANGALORE");
//			 visaDTO.setMerchantId("6100300000117960");
//			 visaDTO.setRequest("00020101021102164054560000168096061661003000001179600811VIJB00013315204541153033565802IN5917SREE SAI CATERERS6009Bangalore61065600726304CB5A");
//			 visaDTO.setSessionId(request);
//			 visaDTO.setMerchantName("SREE SAI CATERERS");
//			 visaDTO.setAdditionalData("");
//			 visaDTO.setAccountNumber("5087070200009460");
//			 System.err.println("encrypt ::" + encrypt(Data));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return visaDTO;
	}
}
