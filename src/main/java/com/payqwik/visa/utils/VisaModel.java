package com.payqwik.visa.utils;

public class VisaModel {

	public String firstName;
	public String lastName;
	public String contactNo;
	public String senderCardNo;
	public String billRefNo;
	public String merchantData;
	public String toEntityId;
	public String fromEntityId;
	public String productId;
	public String description;
	public double amount;
	public String transactionType;
	public String transactionOrigin;
	public String businessType;
	public String businessEntityId;
	public String externalTransactionId;
	public String additionalData;

	public String getFromEntityId() {
		return fromEntityId;
	}

	public void setFromEntityId(String fromEntityId) {
		this.fromEntityId = fromEntityId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getSenderCardNo() {
		return senderCardNo;
	}

	public void setSenderCardNo(String senderCardNo) {
		this.senderCardNo = senderCardNo;
	}

	public String getBillRefNo() {
		return billRefNo;
	}

	public void setBillRefNo(String billRefNo) {
		this.billRefNo = billRefNo;
	}

	public String getMerchantData() {
		return merchantData;
	}

	public void setMerchantData(String merchantData) {
		this.merchantData = merchantData;
	}

	public String getToEntityId() {
		return toEntityId;
	}

	public void setToEntityId(String toEntityId) {
		this.toEntityId = toEntityId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionOrigin() {
		return transactionOrigin;
	}

	public void setTransactionOrigin(String transactionOrigin) {
		this.transactionOrigin = transactionOrigin;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getBusinessEntityId() {
		return businessEntityId;
	}

	public void setBusinessEntityId(String businessEntityId) {
		this.businessEntityId = businessEntityId;
	}

	public String getExternalTransactionId() {
		return externalTransactionId;
	}

	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}
	
}
