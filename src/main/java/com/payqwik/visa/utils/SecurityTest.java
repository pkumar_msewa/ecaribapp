package com.payqwik.visa.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

//import org.apache.commons.codec.binary.Base64;
import com.payqwikapp.util.StartupUtil;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
public class SecurityTest {

		public static void main(String[] a) {
//
		SecurityTest test = new SecurityTest();

		try {
		String json = "{\"headers\":{\"refNo\":\"9876543211234562\",\"key\":\"egOv9pxA+yD27jm8sMqpDpoGS/F0Hdb4zjN9OrEfHEdAcvP6yJgs7Hi+6QqjS5rMUla/l7oEh87GVSi/2OJM6n8uMVtDh2Pjen2t0zXP3JYHyVH2ex3+a+/yrTekw7yLGoh6LC1bt6PjRmQ49xhtxJrnvi1ZSbtNK0se4We3Qp4=\","
				+ "\"entity\":\"SHG5YfTdEDEpt9Z1+5JAL5OTRlsbwOkdsAvymczT/NDE85xWtI/GAFBUZKHb4ysozLMF6BSwgasB+T1ysCKj4rBZOwHGeDb65MOZaB3PC9IFhxR4A5Gkv1DqQagfsDVu/sssjz3zfw2JOU/BWlrrOyQ/MKrPjqk0FpawsiF/+ds=\","
				+ "\"hash\":\"KRFuTarExc0HPNVLfT60gUfNAa+UzY0ODB8wpkelrfqGFZ+00mmsXIa7jCYmKHrJczmVK2A9QRZxwgDUyiWJ6/h+C6qREotFFZqkPDudhWnidPK1R0MID8XlrKAo+1kv6bRhNxjDIxPTcwGHjUkEqDUSK8F4fYz23X4LhScPz0p3MLljE/f+7VE14bMYYkrCs9ysh7pfakR/d1EiYtTiMEtKWsNpFnSBlDtP6wBZYaxc8GwGJB5xXFKXIb7O+JgilS/jPlH/U58IXpdR+p4lhBFLwtnpldsSiZKln5PJSlZg/6kQWoiTo3h2RK0u9b1ZJzwTRKMT+WOFXTaBZVla+Q==\"},"
				+ "\"body\":\"JYtmXtR5D8gT3Jz5e0kLve+7A3cYq5R9v9QT89DhXLKW6Deb/9Z/naMhMKzCrZehYWdW+/LpXWfl/ZOAVa/Vu/y6C4ocTbhF6Qhuv4sx9XeOY8Fha4aVdcJLMd7FlBK7dmz0UdshM22y72eYXyxThM75GQvEgzmG/0kk/dxm+brbAfMABq0+gv1C7HTa4rkQ8aZ/8LGoIUWIG9uuRpN+OrQ9c9zut1/Lo/1kV3Lgvlw=\"}";

			ObjectMapper mapper = new ObjectMapper();
			String decodeData=test.decodeResponse(mapper.readValue(json, YappayEncryptedResponse.class));
			System.out.println("Result "+ decodeData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//
//	/*
//	 * { "entityId" : "PIGGY", "entityType" : "BUSINESS", "name" : "PIGGY",
//	 * "address" : "West Mambalam", "city" : "Chennai", "state" : "Tamil Nadu",
//	 * "country" : "India", "pincode" : "600033", "contactNo" : "+919000000001"
//	 * 
//	 * }
//	 */
//
	public static String getmVisaRequest() throws IOException {
		
		VisaModel visaModel = new VisaModel();
		visaModel.setFirstName("Ajay");
		visaModel.setLastName("Kumar");
		visaModel.setContactNo("9449147913");
		visaModel.setSenderCardNo("5087070200008249");
		visaModel.setBillRefNo("INVOICE0001");
		visaModel.setMerchantData("00020101021102164054560000049940061661003000000499405204549953033565802IN5925DEMO MERCHANT – 11111111M6009KARNATAKA61065600686304d8ed");
		visaModel.setToEntityId("");
		visaModel.setFromEntityId("9449147913");
		visaModel.setProductId("GENERAL");
		visaModel.setDescription("Change");
		visaModel.setAmount(1.00);
		visaModel.setTransactionType("C2M");
		visaModel.setTransactionOrigin("MOBILE");
		visaModel.setBusinessEntityId("VIJAYA");
		visaModel.setBusinessType("VIJAYA");
		visaModel.setExternalTransactionId(System.currentTimeMillis()+"");
//		// 0A45673487751OJoe’s
//		// Sandwich2458123BBangalore42IN53INR6C0000000002507QBill57453289Q
//		// 0F1234501110016011CMERCHANT000121237CHENNAI42IN53356M2PYN75005601
		ObjectMapper mapper = new ObjectMapper();
//
		System.err.println("visa model"+ mapper.writeValueAsString(visaModel));
		return mapper.writeValueAsString(visaModel);
//
	}
//
////	public String getData() throws JsonProcessingException {
////		RegistrationDto dto = new RegistrationDto();
////		dto.setEntityId("PIGGY");
////		dto.setFirstName("PIGGY");
////		dto.setAddress("WM");
////		dto.setCity("Chennai");
////		dto.setState("TN");
////		dto.setCountry("INDIA");
////		dto.setPinCode(600033);
////		dto.setEntityType(EntityType.BUSINESS.getValue());
////		dto.setContactNo("+919500125352");
////		ObjectMapper mapper = new ObjectMapper();
////		return mapper.writeValueAsString(dto);
////	}
//
	public String sendRequest(String body, String url) throws IOException {
		String out=null;
		try {

			WebResource resource = Client.create().resource("https://vijaya.yappay.in/Yappay/payment-manager/payment");

			ClientResponse clientResponse = resource.accept("application/json").header("TENANT", "VIJAYA")
					.header("Authorization", "Basic YWRtaW46YWRtaW4=").type("application/json")
					.post(ClientResponse.class, body);
			System.out.println("Request ::::: "+body);
			if (clientResponse.getStatus() == 200) {
				ObjectMapper mapper = new ObjectMapper();
				String output = clientResponse.getEntity(String.class);
				out=output;
				System.out.println("OUT :: " + output);
//				EncDecryptionRequest.decrypt(output);
				String dec = decodeResponse(mapper.readValue(output,YappayEncryptedResponse.class));
				System.err.println("DEC::"+dec);
			} else {
				System.out.println("OUT Failure:: " + clientResponse.getStatus());
			}
		}catch(Exception e){
			
		}
		return out;
	}
//			// System.out.println("3");
//			// httpclient.getCredentialsProvider().setCredentials(new
//			// AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
//			// new UsernamePasswordCredentials("admin", "admin"));
//			// System.out.println("4.1");
//			// HttpPost httppost = new HttpPost(url.toString());
//			// System.out.println("4.2");
//			// httppost.setHeader("Content-Type", "application/json");
//			// System.out.println("4.3");
//			// StringEntity se = new StringEntity(body.toString());
//			// System.out.println("4.4");
//			// se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//			// "application/json"));
//			// System.out.println("4.5");
//			// httppost.setEntity(se);
//			// System.out.println("4.6");
//			// HttpResponse response = null ;
//			// System.out.println("4.7");
//			// httpclient.execute(httppost);
//			// System.out.println("4.8");
//			// String temp = EntityUtils.toString(response.getEntity());
//			// System.out.println("4.9");
//			// return temp;
//			//
//			// } catch (ClientProtocolException e) {
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		return out;
//	}
//
	public String encodeRequest(String requestData, String messageRefNo, String entity) throws Exception {
		System.err.println("m2p public key file::"+m2pPublicFile);
		System.err.println("bus file ::"+busPrivateFile);
		System.err.println("inside encode request");
		YappayEncryptedRequest request = new YappayEncryptedRequest();
		byte[] sessionKeyByte = this.generateToken();
		System.out.print("token generated :" + this.generateToken());
		request.setToken(this.generateDigitalSignedToken(requestData));
		System.err.println("digital sign generated");
		request.setBody(this.encryptData(requestData, sessionKeyByte, messageRefNo));
		System.out.println("encrypt data completed");
		request.setKey(this.encryptKey(sessionKeyByte));
		System.err.println("encrypt key completed");
		request.setEntity(this.encryptKey(entity.getBytes()));
		System.out.println("encrypt key in bytes completed with the entity :  " + entity);
		request.setRefNo(messageRefNo);
		System.err.println("converting to mapper :::");
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(request);
	}
//
	public String decodeResponse(YappayEncryptedResponse response) throws Exception {
		String sessionKey = response.getHeaders().get("key");
		String token = response.getHeaders().get("hash");
		String messageRefNo = response.getHeaders().get("refNo");
		return this.decryptMessage(response.getBody(), sessionKey, token, messageRefNo);
	}
//
	public String decryptMessage(String xmlResponse, String encSessionKey, String token, String messageRefNo)
			throws Exception {

		byte[] sessionKey = this.decryptSessionKey(encSessionKey);
		String data = decryptWithAESKey(xmlResponse, sessionKey, messageRefNo.getBytes());
		return data;

	}


//
	private String decryptWithAESKey(String inputData, byte[] key, byte[] iv) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		Cipher cipher = Cipher.getInstance(symmetricKeyAlgorithm, pkiProvider);
		SecretKeySpec secKey = new SecretKeySpec(key, symmetricKeyAlgorithm);

		IvParameterSpec spec = new IvParameterSpec(iv);

		cipher.init(Cipher.DECRYPT_MODE, secKey, spec);
		byte[] newData = cipher.doFinal(Base64.getDecoder().decode(inputData));
		return new String(newData);

	}
//
	private byte[] decryptSessionKey(String sessionKey) throws Exception {
		PrivateKey privateKey = readPrivateKeyFromFile(busPrivateFile);
		Cipher cipher = Cipher.getInstance(asymmetricKeyAlgorithm);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		cipher.update(Base64.getDecoder().decode(sessionKey));
		byte[] sessionKeyBytes = cipher.doFinal();
		return sessionKeyBytes;
	}
//
	public String encryptData(String requestData, byte[] sessionKey, String messageRefNo) throws Exception {
		System.err.println("inside encrypt data");
		SecretKey secKey = new SecretKeySpec(sessionKey,"AES");
		Cipher cipher = Cipher.getInstance(symmetricKeyAlgorithm);
		IvParameterSpec ivSpec = new IvParameterSpec(messageRefNo.getBytes());
		cipher.init(Cipher.ENCRYPT_MODE, secKey, ivSpec);
		byte[] newData = cipher.doFinal(requestData.getBytes());
		return Base64.getEncoder().encodeToString(newData);
	}
//
	public String encryptKey(byte[] sessionKey) throws Exception {
		System.err.println("inside encrypt key");
		PublicKey pubKey = readPublicKeyFromFile(m2pPublicFile);
		Cipher cipher = Cipher.getInstance(asymmetricKeyAlgorithm);
		cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		byte[] cipherData = cipher.doFinal(sessionKey);
		return Base64.getEncoder().encodeToString(cipherData);

	}
//
	public byte[] generateToken() throws Exception {
		System.err.println("inside token generator");
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(128);
		SecretKey key = generator.generateKey();
		byte[] symmetricKey = key.getEncoded();
		return symmetricKey;

	}
//
	public String generateDigitalSignedToken(String requestData) throws Exception {
		System.err.println("inside generate digital signed token");
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		Signature signature = Signature.getInstance(digitalSignatureAlgorithm);
		PrivateKey privateKey = this.readPrivateKeyFromFile(busPrivateFile);
		System.err.println("private key from file completed");
		signature.initSign(privateKey, new SecureRandom());
		byte[] message = requestData.getBytes();
		signature.update(message);
		byte[] sigBytes = signature.sign();
		return Base64.getEncoder().encodeToString(sigBytes);

	}
//
	private static PrivateKey readPrivateKeyFromFile(String keyFileName) throws Exception {
		System.err.println("inside read private key from file");
		File filePrivateKey = new File(keyFileName);
		FileInputStream fis = new FileInputStream(filePrivateKey);
		byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
		fis.read(encodedPrivateKey);
		fis.close();
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
		return privateKey;
	}
//
	private static PublicKey readPublicKeyFromFile(String keyFileName) throws Exception {

		File filePublicKey = new File(keyFileName);
		FileInputStream fis = new FileInputStream(filePublicKey);
		byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
		fis.read(encodedPublicKey);
		fis.close();

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
		return publicKey;
	}
	
	/*
	 * 
	 * Local Server 
	 */
//	private String m2pPublicFile = "/home/vibhanshu-pc/Desktop/keys/a.der";
//	private String busPrivateFile = "/home/vibhanshu-pc/Desktop/keys/private.pkcs8";
	private String m2pPublicFile= StartupUtil.CSV_FILE+"a.der";
	private String busPrivateFile= StartupUtil.CSV_FILE+"private.pkcs8";

	/*
	 * 
	 * Production Server
	 * 
	 */
	
	private String symmetricKeyAlgorithm = "AES/CBC/PKCS5Padding";
	private String asymmetricKeyAlgorithm = "RSA/ECB/PKCS1Padding";
	private String digitalSignatureAlgorithm = "SHA1withRSA";
	private String pkiProvider = "BC";
	public static void call() {
	}
}