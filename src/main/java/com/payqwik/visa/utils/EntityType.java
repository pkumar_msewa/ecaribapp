package com.payqwik.visa.utils;

public enum EntityType {

	BUSINESS("BUSINESS");
		
	private final String value;
	

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	private EntityType(String value) {
		this.value = value;
	}
	
}
