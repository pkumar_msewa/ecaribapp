package com.payqwik.visa.utils;

import com.payqwikapp.model.UserType;
import com.thirdparty.model.request.Merchant;

public class VisaMerchantEnc {
		 
	private String firstName;
	private String lastName;
	private String mobileNo;
	private String emailAddress;
	private String address1;
	private String address2;//
	private String city;
	private String state;
	private String country;
	private String pinCode;
	private String lattitude;//
	private String longitude;//
	private String panNo;
	private String aggregator;
	private int merchantCategory;
	
	
	private MerchantBankDetail merchantBankDetail;
	private SettlementBankDetail settlementBankDetail;
	private Merchant merchant;
	
	
	public int getMerchantCategory() {
		return merchantCategory;
	}
	public void setMerchantCategory(int merchantCategory) {
		this.merchantCategory = merchantCategory;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getAggregator() {
		return aggregator;
	}
	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}
	public MerchantBankDetail getMerchantBankDetail() {
		return merchantBankDetail;
	}
	public void setMerchantBankDetail(MerchantBankDetail merchantBankDetail) {
		this.merchantBankDetail = merchantBankDetail;
	}
	public SettlementBankDetail getSettlementBankDetail() {
		return settlementBankDetail;
	}
	public void setSettlementBankDetail(SettlementBankDetail settlementBankDetail) {
		this.settlementBankDetail = settlementBankDetail;
	}
	public Merchant getMerchant() {
		return merchant;
	}
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
}