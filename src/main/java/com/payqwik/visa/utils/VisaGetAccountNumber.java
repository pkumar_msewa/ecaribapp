package com.payqwik.visa.utils;

import java.util.List;

import org.json.JSONArray;

public class VisaGetAccountNumber {

	
	private String firstName;
	private String lastName;
	private String contactNo;
	private String entityId;
	private String business;
	private String productId;
	private List network;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public List getNetwork() {
		return network;
	}
	public void setNetwork(List network) {
		this.network = network;
	}
	
	
}
