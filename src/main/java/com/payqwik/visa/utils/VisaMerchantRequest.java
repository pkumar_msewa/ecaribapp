package com.payqwik.visa.utils;

public class VisaMerchantRequest {

	private String sessionId;
	private String firstName;
	private String lastName;
	private String mobileNo;
	private String emailAddress;
	private String address1;
	private String address2;//
	private String city;
	private String state;
	private String country;
	private String pinCode;
	private String merchantBusinessType;//
	private String lattitude;//
	private String longitude;//
	private String isEnabled;//
	private String panNo;
	private String userName;
	private String aadharNo;
	private String bankAccountNo;	
	private String merchantName;
	public String getBankAccountNo() {
		return bankAccountNo;
	}
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	private String aggregator;//
	private String mvisaId;//
	private String entityId;//
	private String password;
//	private String aadharNo;
	private String userType;
//	merchantBankDetail:
	private String merchantAccountName;
	private String merchantBankName;
	private String merchantAccountNumber;
	private String merchantBankIfscCode;
	private String merchantBankLocation;
//	settlementBankDetail
	private String settlementAccountName;
	private String settlementBankName;
	private String settlementAccountNumber;
	private String settlementIfscCode;
	private String settlementBankLocation;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getMerchantBusinessType() {
		return merchantBusinessType;
	}
	public void setMerchantBusinessType(String merchantBusinessType) {
		this.merchantBusinessType = merchantBusinessType;
	}
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getIsEnabled() {
		return isEnabled;
	}
	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getAadharNo() {
		return aadharNo;
	}
	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}
	public String getAggregator() {
		return aggregator;
	}
	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}
	public String getMvisaId() {
		return mvisaId;
	}
	public void setMvisaId(String mvisaId) {
		this.mvisaId = mvisaId;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSettlementAccountName() {
		return settlementAccountName;
	}
	public void setSettlementAccountName(String settlementAccountName) {
		this.settlementAccountName = settlementAccountName;
	}
	public String getSettlementBankName() {
		return settlementBankName;
	}
	public void setSettlementBankName(String settlementBankName) {
		this.settlementBankName = settlementBankName;
	}
	public String getSettlementAccountNumber() {
		return settlementAccountNumber;
	}
	public void setSettlementAccountNumber(String settlementAccountNumber) {
		this.settlementAccountNumber = settlementAccountNumber;
	}
	public String getSettlementIfscCode() {
		return settlementIfscCode;
	}
	public void setSettlementIfscCode(String settlementIfscCode) {
		this.settlementIfscCode = settlementIfscCode;
	}
	
	public String getMerchantAccountName() {
		return merchantAccountName;
	}
	public void setMerchantAccountName(String merchantAccountName) {
		this.merchantAccountName = merchantAccountName;
	}
	public String getMerchantBankName() {
		return merchantBankName;
	}
	public void setMerchantBankName(String merchantBankName) {
		this.merchantBankName = merchantBankName;
	}
	public String getMerchantAccountNumber() {
		return merchantAccountNumber;
	}
	public void setMerchantAccountNumber(String merchantAccountNumber) {
		this.merchantAccountNumber = merchantAccountNumber;
	}
	public String getMerchantBankIfscCode() {
		return merchantBankIfscCode;
	}
	public void setMerchantBankIfscCode(String merchantBankIfscCode) {
		this.merchantBankIfscCode = merchantBankIfscCode;
	}
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getMerchantBankLocation() {
		return merchantBankLocation;
	}
	public void setMerchantBankLocation(String merchantBankLocation) {
		this.merchantBankLocation = merchantBankLocation;
	}
	public String getSettlementBankLocation() {
		return settlementBankLocation;
	}
	public void setSettlementBankLocation(String settlementBankLocation) {
		this.settlementBankLocation = settlementBankLocation;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	

	
	
}