package com.thirdparty.model.request;

public interface JSONBuilder {

	public String getJsonRequest();
}
