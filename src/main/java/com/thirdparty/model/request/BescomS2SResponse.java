package com.thirdparty.model.request;

public class BescomS2SResponse {
	
	private String key;
	private String merchanttxnid;
	private String pgtransactionid;
	private String status;
	private double billamount;
	private double additionalcharges;
	private String acknowledgementid;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMerchanttxnid() {
		return merchanttxnid;
	}

	public void setMerchanttxnid(String merchanttxnid) {
		this.merchanttxnid = merchanttxnid;
	}

	public String getPgtransactionid() {
		return pgtransactionid;
	}

	public void setPgtransactionid(String pgtransactionid) {
		this.pgtransactionid = pgtransactionid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getBillamount() {
		return billamount;
	}

	public void setBillamount(double billamount) {
		this.billamount = billamount;
	}

	public double getAdditionalcharges() {
		return additionalcharges;
	}

	public void setAdditionalcharges(double additionalcharges) {
		this.additionalcharges = additionalcharges;
	}

	public String getAcknowledgementid() {
		return acknowledgementid;
	}

	public void setAcknowledgementid(String acknowledgementid) {
		this.acknowledgementid = acknowledgementid;
	}
}
