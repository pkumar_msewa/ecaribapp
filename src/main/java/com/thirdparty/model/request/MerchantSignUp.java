package com.thirdparty.model.request;

import com.payqwik.visa.utils.MerchantBankDetail;
import com.payqwik.visa.utils.SettlementBankDetail;

public class MerchantSignUp {

	private Merchant merchant;

	private MerchantBankDetail merchantBankDetail;

	private SettlementBankDetail settlementBankDetail;

	public Merchant getMerchant() {
		return merchant;
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}

	public MerchantBankDetail getMerchantBankDetail() {
		return merchantBankDetail;
	}

	public void setMerchantBankDetail(MerchantBankDetail merchantBankDetail) {
		this.merchantBankDetail = merchantBankDetail;
	}

	public SettlementBankDetail getSettlementBankDetail() {
		return settlementBankDetail;
	}

	public void setSettlementBankDetail(SettlementBankDetail settlementBankDetail) {
		this.settlementBankDetail = settlementBankDetail;
	}

}
