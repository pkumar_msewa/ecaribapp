package com.thirdparty.model.request;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class UpiAuthenticationDTO extends AbstractDTO implements JSONWrapper{
    private String ipAddress;
    private double amount;
    private String hash;
    private String transactionID;
    private double additionalcharges;
    private String productinfo;
    private String firstname;
    private String email;
    private String phone;
    private String lastname;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private String surl;
    private String furl;
    private String curl;
    private String custom_note;  
    private String additionalInfo;
    private String vpa;
    private String accountid;
    private String paymenttype;
    private String transactiontype;
    private String subdivisioncode;
    private String consumerid;
    private String locationCode;
    private String billNo;

    public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    public double getAdditionalcharges() {
		return additionalcharges;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setAdditionalcharges(double additionalcharges) {
		this.additionalcharges = additionalcharges;
	}

	public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getProductinfo() {
		return productinfo;
	}

	public void setProductinfo(String productinfo) {
		this.productinfo = productinfo;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSurl() {
		return surl;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public String getFurl() {
		return furl;
	}

	public void setFurl(String furl) {
		this.furl = furl;
	}

	public String getCurl() {
		return curl;
	}

	public void setCurl(String curl) {
		this.curl = curl;
	}

	public String getCustom_note() {
		return custom_note;
	}

	public void setCustom_note(String custom_note) {
		this.custom_note = custom_note;
	}

	public String getVpa() {
		return vpa;
	}

	public void setVpa(String vpa) {
		this.vpa = vpa;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getPaymenttype() {
		return paymenttype;
	}

	public void setPaymenttype(String paymenttype) {
		this.paymenttype = paymenttype;
	}

	public String getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getSubdivisioncode() {
		return subdivisioncode;
	}

	public void setSubdivisioncode(String subdivisioncode) {
		this.subdivisioncode = subdivisioncode;
	}

	public String getConsumerid() {
		return consumerid;
	}

	public void setConsumerid(String consumerid) {
		this.consumerid = consumerid;
	}

	@Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("transactionID",getTransactionID());
            jsonObject.put("ipAddress",getIpAddress());
            jsonObject.put("id",getId());
            jsonObject.put("hash",getHash());
            jsonObject.put("amount",getAmount());
            jsonObject.put("additionalcharges",getAdditionalcharges());
            jsonObject.put("productinfo",getProductinfo());
            jsonObject.put("firstname",getFirstname());
            jsonObject.put("email",getEmail());
            jsonObject.put("phone",getPhone());
            jsonObject.put("lastname",getLastname());
            jsonObject.put("address1",getAddress1());
            jsonObject.put("address2",getAddress2());
            jsonObject.put("city",getCity());
            jsonObject.put("state",getState());
            jsonObject.put("country",getCountry());
            jsonObject.put("zipcode",getZipcode());
            jsonObject.put("surl",getSurl());
            jsonObject.put("furl",getFurl());
            jsonObject.put("curl",getCurl());
            jsonObject.put("custom_note",getCustom_note());
            jsonObject.put("accountid",getAccountid());
            jsonObject.put("paymenttype",getPaymenttype());
            jsonObject.put("transactiontype",getTransactiontype());
            jsonObject.put("subdivisioncode",getSubdivisioncode());
            jsonObject.put("consumerid",getConsumerid());
            jsonObject.put("locationCode",getLocationCode());
            jsonObject.put("billNo",getBillNo());
            return jsonObject;
        } catch (JSONException e) {
            return null;
        }
	}
}
