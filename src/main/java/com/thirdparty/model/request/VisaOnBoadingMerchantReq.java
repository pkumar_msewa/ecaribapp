package com.thirdparty.model.request;

import java.io.StringWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.codehaus.jettison.json.JSONObject;

public class VisaOnBoadingMerchantReq implements JSONBuilder {

	private MerchantReq merchat;
	private MerchantBankDetailReq mBankDetails;
	private SettlementBankDetailReq sBankdetails;

	public MerchantReq getMerchat() {
		return merchat;
	}

	public void setMerchat(MerchantReq merchat) {
		this.merchat = merchat;
	}

	public MerchantBankDetailReq getmBankDetails() {
		return mBankDetails;
	}

	public void setmBankDetails(MerchantBankDetailReq mBankDetails) {
		this.mBankDetails = mBankDetails;
	}

	public SettlementBankDetailReq getsBankdetails() {
		return sBankdetails;
	}

	public void setsBankdetails(SettlementBankDetailReq sBankdetails) {
		this.sBankdetails = sBankdetails;
	}

	@Override
	public String getJsonRequest() {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonObjectBuilder merchant = Json.createObjectBuilder();
		merchant.add("merchantName", getMerchat().getMerchantName());
		merchant.add("firstName", getMerchat().getFirstName());
		merchant.add("lastName", getMerchat().getLastName());
		merchant.add("emailAddress", getMerchat().getEmailAddress());
		merchant.add("address1", getMerchat().getAddress1());
		merchant.add("address2", getMerchat().getAddress2());
		merchant.add("city", getMerchat().getCity());
		merchant.add("state", getMerchat().getState());
		merchant.add("country", getMerchat().getCountry());
		merchant.add("pinCode", getMerchat().getPinCode());
		merchant.add("lattitude", 12121212402.123459);
		merchant.add("longitude", 121212134.50550403);
		merchant.add("isEnabled", true);
		merchant.add("mobileNo", getMerchat().getMobileNo());
		merchant.add("aggregator", getMerchat().getAggregator());
		merchant.add("idProofPath", getMerchat().getIdProofPath());
		merchant.add("addressProofPath", getMerchat().getAddressProofPath());
		merchant.add("panNo", getMerchat().getPanNo());
		merchant.add("merchantCategory", getMerchat().getMerchantCategory());
		merchant.add("vpa", getMerchat().getVpa());
		merchant.add("aadharNo", getMerchat().getAadharNo());
		merchant.add("gstNumber", getMerchat().getGstNumber());
		merchant.add("mdrValue", true);
		
		jsonBuilder.add("merchant", merchant);
		
		JsonObjectBuilder mBankDetails = Json.createObjectBuilder();
		mBankDetails.add("accName", getmBankDetails().getAccName());
		mBankDetails.add("bankName", getmBankDetails().getBankName());
		mBankDetails.add("bankAccNo", getmBankDetails().getBankAccNo());
		mBankDetails.add("bankIfscCode", getmBankDetails().getBankIfscCode());
		mBankDetails.add("bankLocation", getmBankDetails().getBankLocation());
		jsonBuilder.add("merchantBankDetail", mBankDetails);
		
		JsonObjectBuilder sBankDetails = Json.createObjectBuilder();
		sBankDetails.add("accName", getsBankdetails().getAccName());
		sBankDetails.add("bankName", getsBankdetails().getBankName());
		sBankDetails.add("bankAccNo", getsBankdetails().getBankAccNo());
		sBankDetails.add("bankIfscCode", getsBankdetails().getBankIfscCode());
		sBankDetails.add("bankLocation", getsBankdetails().getBankLocation());
		jsonBuilder.add("settlementBankDetail", sBankDetails);

		JsonObject empObj = jsonBuilder.build();

		StringWriter jsnReqStr = new StringWriter();
		JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		jsonWtr.writeObject(empObj);
		jsonWtr.close();
		return jsnReqStr.toString();
	}

}
