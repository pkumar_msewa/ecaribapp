package com.thirdparty.controller;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.*;
import com.payqwikapp.model.error.ChangePasswordError;
import com.payqwikapp.model.error.ForgotPasswordError;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.VerifyMobileOTPError;
import com.payqwikapp.model.mobile.*;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.EmailOTPValidation;
import com.payqwikapp.validation.MobileOTPValidation;
import com.payqwikapp.validation.RegisterValidation;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.SessionLoggingStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Controller
public class RegistrationController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final IUserApi userApi;
    private final RegisterValidation registerValidation;
    private final MobileOTPValidation mobileOTPValidation;
    private final EmailOTPValidation emailOTPValidation;
    private final UserSessionRepository userSessionRepository;
    private final SessionLoggingStrategy sessionLoggingStrategy;

    public RegistrationController(IUserApi userApi, RegisterValidation registerValidation, MobileOTPValidation mobileOTPValidation, EmailOTPValidation emailOTPValidation,
    		UserSessionRepository userSessionRepository, SessionLoggingStrategy sessionLoggingStrategy) {
        this.userApi = userApi;
        this.registerValidation = registerValidation;
        this.mobileOTPValidation = mobileOTPValidation;
        this.emailOTPValidation = emailOTPValidation;
        this.userSessionRepository = userSessionRepository;
        this.sessionLoggingStrategy = sessionLoggingStrategy;
    }



    @RequestMapping(method = RequestMethod.POST, value = "/Register", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> registerUser(@RequestBody RegisterDTO user, @RequestHeader(value = "hash", required = true) String hash,
                                             HttpServletRequest request, HttpServletResponse response)
            throws JSONException, JsonGenerationException, JsonMappingException, IOException {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(user, hash);
        if (isValidHash) {
                RegisterError error = new RegisterError();
                try {
                    user.setUsername(user.getContactNo());
                    error = registerValidation.validateNormalUser(user);
                    if (error.isValid()) {
                        user.setUserType(UserType.User);
                        userApi.saveUser(user);
                        result.setStatus(ResponseStatus.SUCCESS);
                        result.setMessage("User Registration Successful and OTP sent to ::" + user.getContactNo()
                                + " and  verification mail sent to ::" + user.getEmail() + " .");
                        result.setDetails("User Registration Successful and OTP sent to ::" + user.getContactNo()
                                + " and  verification mail sent to ::" + user.getEmail() + " .");
                        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                    } else {
                        result.setStatus(ResponseStatus.BAD_REQUEST);
                        result.setMessage(error.getMessage());
                        result.setDetails(error);
                        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                    }
                } catch (Exception e) {
                    result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
                    result.setMessage("Please try again later.");
                    result.setDetails(e.getMessage());
                    return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                }

        } else {
            result.setStatus(ResponseStatus.INVALID_HASH);
            result.setMessage("Failed, Please try again later.");
            return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
        }
    }



    @RequestMapping(value = "/ForgotPassword", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> forgotPassword(@RequestBody ForgotPasswordDTO forgot, @RequestHeader(value = "hash", required = true) String hash,
                                               HttpServletRequest request, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(forgot, hash);
        if (isValidHash) {
                String username = forgot.getUsername();
                ForgotPasswordError error = registerValidation.forgotPassword(username);
                if (error.isValid() == true) {
                    User u = userApi.findByUserName(username);
                    if (u != null) {
                        if (u.getAuthority().contains(Authorities.AUTHENTICATED)) {
                            if (u.getMobileStatus() == Status.Active) {
                                userApi.changePasswordRequest(u);
                                result.setStatus(ResponseStatus.SUCCESS);
                                result.setMessage("Forgot Password");
                                result.setDetails("Please, Check your SMS for OTP Code to change your password.");
                            } else {
                                result.setStatus(ResponseStatus.FAILURE);
                                result.setMessage("Please try again later.");
                                result.setDetails("Please try again later.");
                            }
                        } else {
                            result.setStatus(ResponseStatus.FAILURE);
                            result.setMessage("Your account is blocked! Please try after 24 hrs.");
                            result.setDetails("Your account is blocked! Please try after 24 hrs.");
                        }
                    } else {
                        result.setStatus(ResponseStatus.FAILURE);
                        result.setMessage("Not a valid User");
                        result.setDetails("Not a Valid User");
                    }
                } else {
                    result.setStatus(ResponseStatus.BAD_REQUEST);
                    result.setMessage("Failed, invalid request.");
                    result.setDetails(error);
                }
        } else {
            result.setStatus(ResponseStatus.INVALID_HASH);
            result.setMessage("Failed, Please try again later.");
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }


    @RequestMapping(value = "/Activate/Mobile", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> verifyUserMobile(@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
                                                 HttpServletRequest request, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);

        if (isValidHash) {
                if (verifyUserMobileToken(dto.getKey(), dto.getMobileNumber())) {
                    result.setStatus(ResponseStatus.SUCCESS);
                    result.setMessage("Activate Mobile");
                    result.setDetails("Your Mobile is Successfully Verified");
                } else {
                    result.setStatus(ResponseStatus.BAD_REQUEST);
                    result.setMessage("Activate Mobile");
                    result.setDetails("Invalid Activation Key");
                }

        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
            result.setMessage("Invalid Secure Hash");
        }

        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/Resend/Mobile/OTP", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> generateNewOtp(@PathVariable(value = "role") String role,
                                               @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
                                               @RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
                                               HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
        if (isValidHash) {
            if (role.equalsIgnoreCase("User")) {
                String username = dto.getMobileNumber();
                VerifyMobileOTPError error = new VerifyMobileOTPError();
                error = mobileOTPValidation.validateResendOTP(username);
                if (error.isValid()) {
                    userApi.resendMobileToken(username);
                    result.setStatus(ResponseStatus.SUCCESS);
                    result.setMessage("Resend OTP");
                    result.setDetails("New OTP sent on " + username);
                } else {
                    result.setStatus(ResponseStatus.FAILURE);
                    result.setMessage("Resend OTP");
                    result.setDetails(error);
                }
            } else {
                result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
                result.setMessage("Not a valid user");
            }
        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
            result.setMessage("Invalid Secure Hash");
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/Change/{key}", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> verifyPasswordPost(@PathVariable("key") String key, @RequestHeader(value = "hash", required = true) String hash,
                                                   HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(key, hash);
        if (isValidHash) {
                UserDTO dto = userApi.checkPasswordToken(key);
                if (dto != null) {
                    result.setStatus(ResponseStatus.SUCCESS);
                    result.setMessage("Verify Password");
                    result.setDetails("User ::" + dto.getUsername() + " key::" + key);
                    result.setCode("S00");
                } else {
                    result.setStatus(ResponseStatus.BAD_REQUEST);
                    result.setMessage("Verify Password");
                    result.setDetails("Invalid Verification Key");
                }

        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
            result.setMessage("Invalid Secure Hash");
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/VerifyOtp/{key}", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> verifyUserOtp(@PathVariable("key") String key, @RequestHeader(value = "hash", required = true) String hash,
                                                   HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(key, hash);
        if (isValidHash) {
                UserDTO dto = userApi.checkPasswordToken(key);
                if (dto != null) {
                    result.setStatus(ResponseStatus.SUCCESS);
                    result.setMessage("Verify Password");
                    result.setDetails("User ::" + dto.getUsername() + " key::" + key);
                    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            //        sessionLoggingStrategy.onAuthenticationUser(authentication, request, response,Long.parseLong(dto.getUserId()), RequestContextHolder.currentRequestAttributes().getSessionId(),"127.0.0.1",null,null);
                    sessionLoggingStrategy.onAuthenticationUser(Long.parseLong(dto.getUserId()), RequestContextHolder.currentRequestAttributes().getSessionId(),"127.0.0.1",null,null);
                    User user = userApi.findByUserName(dto.getUsername());
                    UserSession userSession = new UserSession();
                    userSession.setExpired(false);
                    userSession.setLastRequest(new Date());
                    userSession.setSessionId(RequestContextHolder.currentRequestAttributes().getSessionId());
                    userSession.setUser(user);
                    userSessionRepository.save(userSession);
                    UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
                   
                    result.setSessionId(userSession.getSessionId());
                    result.setDetails2(activeUser.getFirstName()+" "+activeUser.getLastName());
                    result.setDetails3(user.getAccountDetail().getBalance());
                    result.setDetails4(activeUser.getUsername());
                    result.setCode("S00");
                } else {
                    result.setStatus(ResponseStatus.BAD_REQUEST);
                    result.setMessage("Verify Password");
                    result.setDetails("Invalid Verification Key");
                }

        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
            result.setMessage("Invalid Secure Hash");
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }
    @RequestMapping(method = RequestMethod.POST, value = "/RenewPassword", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> renewPassword(@RequestBody ChangePasswordDTO dto, @RequestHeader(value = "hash", required = true) String hash,
                                              HttpServletRequest request, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
        if (isValidHash) {

                ChangePasswordError error = new ChangePasswordError();
                error = registerValidation.renewPasswordValidation(dto);
                if (error.isValid()) {
                    userApi.renewPassword(dto);
                    result.setStatus(ResponseStatus.SUCCESS);
                    result.setMessage("Renew Password");
                    result.setDetails("Password Changed Successfully");
                }

        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
            result.setMessage("Invalid Secure Hash");
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }

    private boolean verifyUserMobileToken(String key, String mobileNumber) {
        if (userApi.checkMobileToken(key, mobileNumber)) {
            return true;
        } else {
            return false;
        }
    }

}
