package com.thirdparty.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;

import com.payqwikapp.api.IBusApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.session.SessionLoggingStrategy;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.TransactionValidation;
import com.thirdparty.api.ISDKPaymentApi;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.response.SDKResponseDTO;

@Controller
@RequestMapping("/SDK/Authenticate")
public class SDKRegistrationController {
	
	private final ISDKPaymentApi sdkPaymentApi;
	private final IUserApi userApi;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final IBusApi busApi;

	public SDKRegistrationController(ISDKPaymentApi sdkPaymentApi, IUserApi userApi, TransactionValidation transactionValidation,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			PQServiceRepository pqServiceRepository,SessionLoggingStrategy sessionLoggingStrategy,IBusApi busApi) {
		this.sdkPaymentApi = sdkPaymentApi;
		this.userApi = userApi;
		this.transactionValidation = transactionValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.sessionLoggingStrategy=sessionLoggingStrategy;
		this.busApi=busApi;
	}

	@RequestMapping(value = "/MerchantAuth", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SDKResponseDTO> authenticateMerchant(@RequestBody AuthenticationDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		SDKResponseDTO result = new SDKResponseDTO();
		User user = userApi.findById(dto.getId());
		if (user != null) {
			String authority = user.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				String token = sdkPaymentApi.getTokenByMerchant(user);
				dto.setToken(token);
				String receivedHash = dto.getHash();
				try {
					String calculatedHash = SecurityUtil.md5(ConvertUtil.convertAuthenticationDTO(dto));
					if (receivedHash.equalsIgnoreCase(calculatedHash)) {
						result = sdkPaymentApi.authenticateMerchant(dto, user);
						boolean isExist=userApi.checkUserExist(dto.getUsername());
						if(isExist){
						result.setUserExist(isExist);
						result.setStatus(ResponseStatus.SUCCESS);
						}else{
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("You are not registered at VPayQwik,Please register and try again");
							result.setUserExist(isExist);
						}
					} else {
						result.setStatus(ResponseStatus.INVALID_HASH);
						result.setMessage("Invalid Hash");
						result.setDetails("Invalid Hash");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized User");
				result.setDetails("Permission not granted");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized User");
			result.setDetails("User Not Avaiable");
		}
		return new ResponseEntity<SDKResponseDTO>(result, HttpStatus.OK);
	}
	
    @RequestMapping(method = RequestMethod.POST, value = "/VerifyOtp/{key}", produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> verifyUserOtp(@PathVariable("key") String key, @RequestHeader(value = "hash", required = true) String hash,
                                                   HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
        ResponseDTO result = new ResponseDTO();
        boolean isValidHash = SecurityUtil.isHashMatches(key, hash);
        if (isValidHash) {
                UserDTO dto = sdkPaymentApi.checkPasswordTokenForSDKUser(key);
                if (dto != null) {
                    result.setStatus(ResponseStatus.SUCCESS);
                    result.setMessage("Verify OTP");
                    result.setDetails("User ::" + dto.getUsername() + " key::" + key);
                    User user = userApi.findByUserName(dto.getUsername());
                    UserSession userSession = new UserSession();
                    userSession.setExpired(false);
                    userSession.setLastRequest(new Date());
                    userSession.setSessionId(RequestContextHolder.currentRequestAttributes().getSessionId());
                    userSession.setUser(user);
                    userSessionRepository.save(userSession);
                    UserDTO activeUser = userApi.getUserById(userSession.getUser().getId());
                    result.setSessionId(userSession.getSessionId());
                    result.setDetails2(activeUser.getFirstName()+" "+activeUser.getLastName());
                    result.setDetails3(user.getAccountDetail().getBalance());
                    result.setDetails4(activeUser.getUsername());
                    result.setCode("S00");
                } else {
                    result.setStatus(ResponseStatus.BAD_REQUEST);
                    result.setMessage("Verify Password");
                    result.setDetails("Invalid Verification Key");
                }

        } else {
            result.setStatus(ResponseStatus.BAD_REQUEST);
            result.setMessage("Invalid Secure Hash");
        }
        return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
    }
    
}
