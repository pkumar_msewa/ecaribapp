package com.thirdparty.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IBusApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.VerifyMobileDTO;
import com.payqwikapp.model.error.RegisterError;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.error.VerifyMobileOTPError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.model.travel.bus.BusPaymentResponse;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.session.SessionLoggingStrategy;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.MobileOTPValidation;
import com.payqwikapp.validation.RegisterValidation;
import com.payqwikapp.validation.TransactionValidation;
import com.thirdparty.api.ISDKPaymentApi;
import com.thirdparty.model.request.MerchantRegisterDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/SDK/Payment")
public class SDKPaymentController {

	private final ISDKPaymentApi sdkPaymentApi;
	private final IUserApi userApi;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;
	private final SessionLoggingStrategy sessionLoggingStrategy;
	private final IBusApi busApi;
	private final RegisterValidation registerValidation;
	private final MobileOTPValidation mobileOTPValidation;

	public SDKPaymentController(ISDKPaymentApi sdkPaymentApi, IUserApi userApi,
			TransactionValidation transactionValidation, UserSessionRepository userSessionRepository,
			PersistingSessionRegistry persistingSessionRegistry, PQServiceRepository pqServiceRepository,
			SessionLoggingStrategy sessionLoggingStrategy, IBusApi busApi, RegisterValidation registerValidation,MobileOTPValidation mobileOTPValidation) {
		this.sdkPaymentApi = sdkPaymentApi;
		this.userApi = userApi;
		this.transactionValidation = transactionValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
		this.sessionLoggingStrategy = sessionLoggingStrategy;
		this.busApi = busApi;
		this.registerValidation = registerValidation;
		this.mobileOTPValidation = mobileOTPValidation;
	}

	@RequestMapping(value = "/saveSDKSeatDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> saveSeatDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BusPaymentInit dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response)
			throws JSONException, JsonGenerationException, JsonMappingException, IOException {

		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				User user = userApi.findByUserName(dto.getUserMobile());
				if (user != null) {
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
						if (service != null && Status.Active.equals(service.getStatus())) {
							TransactionError error = transactionValidation.validateEaseMyTrip(
									String.valueOf(dto.getTotalFare()), user.getUsername(), service);
							User user2 = userApi.findByUserName(user.getUsername());
							BusPaymentResponse busPayResp = busApi.saveSeatDetails(dto, service, user.getUsername(),
									user2);
							if (busPayResp.isValid()) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(busPayResp.getMessage());
								result.setCode("S00");
								result.setDetails(busPayResp);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(busPayResp.getMessage());
								result.setCode("F00");
								result.setDetails(busPayResp.getMessage());
							}

						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Service is under maintenance");
							result.setCode("F00");
							result.setDetails("Service is under maintenance");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed,Unauthorized User");
						result.setCode("F00");
						result.setDetails("Failed,Unauthorized User");
					}
				} else {
					user = sdkPaymentApi.setUnregisteredUser(dto, device);
					if (user.getAuthority().contains(Authorities.USER)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						PQService service = pqServiceRepository.findServiceByCode(StartupUtil.BUS_CODE);
						if (service != null && Status.Active.equals(service.getStatus())) {
							TransactionError error = transactionValidation.validateEaseMyTrip(
									String.valueOf(dto.getTotalFare()), user.getUsername(), service);
							User user2 = userApi.findByUserName(user.getUsername());
							BusPaymentResponse busPayResp = busApi.saveSeatDetails(dto, service, user.getUsername(),
									user2);
							if (busPayResp.isValid()) {
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(busPayResp.getMessage());
								result.setCode("S00");
								result.setDetails(busPayResp);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage(busPayResp.getMessage());
								result.setCode("F00");
								result.setDetails(busPayResp.getMessage());
							}

						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Service is under maintenance");
							result.setCode("F00");
							result.setDetails("Service is under maintenance");
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed,Unauthorized User");
						result.setCode("F00");
						result.setDetails("Failed,Unauthorized User");
					}
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized user.");
				result.setCode("F00");
				result.setDetails("Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid request.");
			result.setCode("F00");
			result.setDetails("Invalid request.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/MerchantRegistration", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> registerMerchant(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MerchantRegisterDTO user, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, HttpServletResponse response){
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(user, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				RegisterError error = new RegisterError();
				try {
					user.setUsername(user.getMobileNumber());
					error = registerValidation.validateMerchantUser(user);
					if (error.isValid()) {
						user.setUserType(UserType.User);
						sdkPaymentApi.saveUser(user,device);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage(
										"User Registration Successful and OTP sent to ::" + user.getMobileNumber()
												+ " and  verification mail sent to ::" + user.getEmail() + " .");
								result.setDetails(
										"User Registration Successful and OTP sent to ::" + user.getMobileNumber()
												+ " and  verification mail sent to ::" + user.getEmail() + " .");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Register User");
						result.setDetails(error);
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} catch (Exception e) {
					result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
					result.setMessage("Please try again later.");
					result.setDetails(e.getMessage());
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "MerchantResendOTP", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> generateNewOtp(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VerifyMobileDTO dto, @RequestHeader(value = "hash", required = true) String hash,
			HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("User")) {
				String username = dto.getMobileNumber();
				VerifyMobileOTPError error = new VerifyMobileOTPError();
				error = mobileOTPValidation.validateResendOTPSDKUSer(username);
				if (error.isValid()) {
					sdkPaymentApi.resendMobileTokenSDKUser(username);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("New OTP sent on " + username);
					result.setDetails("New OTP sent on " + username);
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Mobile Number Already Verified.");
					result.setDetails(error.getOtp());
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid user");
			}
		} else {
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Invalid Secure Hash");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
}
