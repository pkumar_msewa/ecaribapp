package com.thirdparty.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserSession;
import com.payqwikapp.model.BescomRefundRequestDTO;
import com.payqwikapp.model.SessionDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UPIResponse;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.error.TransactionError;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.UserSessionRepository;
import com.payqwikapp.session.PersistingSessionRegistry;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.SecurityUtil;
import com.payqwikapp.validation.TransactionValidation;
import com.thirdparty.api.IMerchantApi;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.BescomStatusResponse;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.request.S2SRequest;
import com.thirdparty.model.request.StatusCheckDTO;
import com.thirdparty.model.request.StatusResponse;
import com.thirdparty.model.request.UpiAuthenticationDTO;
import com.upi.model.UpiRequest;

@Controller
@RequestMapping("/Authenticate")
public class MerchantController {

	private final IMerchantApi merchantApi;
	private final IUserApi userApi;
	private final TransactionValidation transactionValidation;
	private final UserSessionRepository userSessionRepository;
	private final PersistingSessionRegistry persistingSessionRegistry;
	private final PQServiceRepository pqServiceRepository;

	public MerchantController(IMerchantApi merchantApi, IUserApi userApi, TransactionValidation transactionValidation,
			UserSessionRepository userSessionRepository, PersistingSessionRegistry persistingSessionRegistry,
			PQServiceRepository pqServiceRepository) {
		this.merchantApi = merchantApi;
		this.userApi = userApi;
		this.transactionValidation = transactionValidation;
		this.userSessionRepository = userSessionRepository;
		this.persistingSessionRegistry = persistingSessionRegistry;
		this.pqServiceRepository = pqServiceRepository;
	}

	@RequestMapping(value = "/Merchant", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> authenticateMerchant(@RequestBody AuthenticationDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		User user = userApi.findById(dto.getId());
		if (user != null) {
			String authority = user.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				String token = merchantApi.getTokenByMerchant(user);
				dto.setToken(token);
				String receivedHash = dto.getHash();
				try {
					String calculatedHash = SecurityUtil.md5(ConvertUtil.convertAuthenticationDTO(dto));
					if (receivedHash.equalsIgnoreCase(calculatedHash)) {
						result = merchantApi.authenticateMerchant(dto, user);
						result.setUserExist(userApi.checkUserExist(dto.getUsername()));
					} else {
						result.setStatus(ResponseStatus.INVALID_HASH);
						result.setMessage("Invalid Hash");
						result.setDetails("Invalid Hash");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized User");
				result.setDetails("Permission not granted");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized User");
			result.setDetails("User Not Avaiable");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Payment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> authenticatePayment(@RequestBody PaymentDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		String sessionId = dto.getSessionId();
		UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
		if (userSession != null) {
			UserDTO user = userApi.getUserById(userSession.getUser().getId());
			if (user.getAuthority().contains(Authorities.USER)
					&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
				persistingSessionRegistry.refreshLastRequest(sessionId);
				User merchant = userApi.findById(dto.getId());
				if (merchant != null) {
					String authority = merchant.getAuthority();
					if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
						PQService service = merchantApi.getServiceByMerchant(merchant);
						TransactionError transactionError = transactionValidation.validateMerchantTransaction(
								String.valueOf(dto.getNetAmount()), user.getUsername(), service);
						if (transactionError.isValid()) {
							User u = userApi.findByUserName(user.getUsername());
							result = merchantApi.authenticatePayment(dto, u, merchant);
							result.setBalance(u.getAccountDetail().getBalance());
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(transactionError.getCode());
							result.setDetails(transactionError.getMessage());
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Unauthorized User");
						result.setDetails("Unauthorized User");
					}
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_SESSION);
			result.setMessage("Please, login and try again.");
			result.setDetails("Please, login and try again.");

		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/StatusCheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> authenticatePayment(@RequestBody StatusCheckDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();

		User merchant = userApi.findById(dto.getMerchantId());
		if (merchant != null) {
			String authority = merchant.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				boolean isValidKey = merchantApi.containsValidSecretKey(merchant, dto.getSecretKey());
				if (isValidKey) {
					TPTransaction merchantTransaction = merchantApi.getByTransactionRefNo(merchant,
							dto.getMerchantRefNo());
					if (merchantTransaction != null) {
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction Details");
						result.setDetails(com.thirdparty.util.ConvertUtil.convertTransaction(merchantTransaction));
					} else {
						result.setStatus(ResponseStatus.BAD_REQUEST);
						result.setMessage("Transaction Not Available");
						result.setDetails("Transaction Not Available");
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Not a valid key/token");
					result.setDetails("Not a valid key/token");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid merchant id");
				result.setMessage("Not a valid merchant id");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Not a valid user");
			result.setDetails("Not a valid user");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	// TODO Merchant Payment with UPI
	@RequestMapping(value = "/MerchantUpiAuth", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> authenticateUpiMerchant(@RequestBody UpiAuthenticationDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		System.err.println("TxnId : " + dto.getTransactionID());
		System.err.println("Additional Amount : " + dto.getAdditionalcharges());
		System.err.println("Original Amount : " + dto.getAmount());
		System.err.println("");
		System.err.println("before request");
		ResponseDTO result = new ResponseDTO();
		System.err.println("the id is::" + dto.getId());
		User user = userApi.findById(dto.getId());
		if (user != null) {
			System.err.println("user is not null");
			String authority = user.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				String token = merchantApi.getTokenByMerchant(user);
				dto.setToken(token);
				String receivedHash = dto.getHash();
				System.err.println("Received Hash Value : : "+ receivedHash);
				try {
					System.err.println("Calculation format : : :  " + ConvertUtil.convertUpiAuthenticationDTO(dto));
					String calculatedHash = SecurityUtil.md5(ConvertUtil.convertUpiAuthenticationDTO(dto));
					System.err.println("Caluculated Hash Value : : " + calculatedHash);
					if (receivedHash.equalsIgnoreCase(calculatedHash)) {
						result = merchantApi.authenticateMerchantUPI(dto, user);
					} else {
						result.setStatus(ResponseStatus.INVALID_HASH);
						result.setMessage("Invalid Hash");
						result.setDetails("Invalid Hash");
					}
					System.err.println("afer request");
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized User");
				result.setDetails("Permission not granted");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Unauthorized User");
			result.setDetails("User Not Avaiable");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiMerchantPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> authenticateUpiMerchantPayment(@RequestBody UpiRequest dto, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		User merchant = userApi.findById(dto.getMid());
		if (merchant != null) {
			String authority = merchant.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				result = merchantApi.authenticatePaymentUPI(dto, merchant);
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Unauthorized User");
				result.setDetails("Unauthorized User");
			}
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedirectMerchantUPI", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> redirectMerchantUPI(@RequestBody UPIResponse dto, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		ResponseDTO result = new ResponseDTO();
		try{
			TPTransaction tpTransaction = merchantApi.getByTransactionRefNo(dto.getReferenceId());
			if (tpTransaction != null) {
				if(!tpTransaction.getStatus().equals(Status.Success)){
					result = merchantApi.redirectMerchantUPI(dto);
					result.setDetails(com.thirdparty.util.ConvertUtil.convertTransaction(tpTransaction = merchantApi.getByTransactionRefNo(dto.getReferenceId())));
				}else{
					result.setStatus(ResponseStatus.FAILURE);
	                result.setMessage("Transaction already processed");
	                result.setDetails("Transaction already processed");
				}
			} else {
				result.setStatus(ResponseStatus.BAD_REQUEST);
				result.setMessage("Transaction Not Available");
				result.setDetails("Transaction Not Available");
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setStatus(ResponseStatus.BAD_REQUEST);
			result.setMessage("Transaction Not Available");
			result.setDetails("Transaction Not Available");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/StatusCheckUpi", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BescomStatusResponse> checkPaymentStatus(@RequestBody StatusCheckDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		BescomStatusResponse result = new BescomStatusResponse();
		User merchant = userApi.findById(dto.getMerchantId());
		if (merchant != null) {
			String authority = merchant.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				TPTransaction merchantTransaction = merchantApi.getByTransactionRefNo(merchant, dto.getMerchantRefNo());
				if (merchantTransaction != null) {
					long timestamp = merchantTransaction.getCreated().getTime();
					long currentTime = System.currentTimeMillis();
					if (!((currentTime - timestamp) < 1000 * 60 * 5)) {
						// TODO Failed the transaction which is inprocess 
						StatusResponse resp = com.thirdparty.util.ConvertUtil.convertTransaction(merchantTransaction);
//						System.err.println("The expiration date of your request has elapsed");
						result.setStatus(ResponseStatus.UPI_DATE_EXPIRED);
						result.setMessage("The expiration date of your request has elapsed. Therefore your transaction has automatically been rejected.");
						result.setDetails(resp);
					}else{
						StatusResponse resp = com.thirdparty.util.ConvertUtil.convertTransaction(merchantTransaction);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("Transaction Details");
						result.setDetails(resp);
					}
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Transaction Not Available");
					result.setDetails("Transaction Not Available");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid merchant id");
				result.setMessage("Not a valid merchant id");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Not a valid user");
			result.setDetails("Not a valid user");
		}
		return new ResponseEntity<BescomStatusResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SavingS2SUpdates", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> savingS2SUpdates(@RequestBody S2SRequest dto, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		User merchant = userApi.findById(Long.parseLong(dto.getKey()));
		if (merchant != null) {
			String authority = merchant.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				result = merchantApi.savingS2Supdates(dto, merchant);
				result.setStatus(ResponseStatus.SUCCESS);
	            result.setMessage("Successfully Saved");
	            result.setDetails("Successfully Saved");
				System.out.println("SavingS2SUpdates --------------- -------------- ----------------- " + result.getStatus());
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid merchant id");
				result.setMessage("Not a valid merchant id");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Not a valid user");
			result.setDetails("Not a valid user");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/CheckUpiTxnAfterRedirect", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> checkUpiTxnAferRedirect(@RequestBody StatusCheckDTO dto, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		User merchant = userApi.findById(dto.getMerchantId());
		if (merchant != null) {
			String authority = merchant.getAuthority();
			if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
				TPTransaction merchantTransaction = merchantApi.getByTransactionRefNo(merchant, dto.getMerchantRefNo());
				if (merchantTransaction != null) {
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Transaction Details");
					result.setDetails(com.thirdparty.util.ConvertUtil.convertTransaction(merchantTransaction));
				} else {
					result.setStatus(ResponseStatus.BAD_REQUEST);
					result.setMessage("Transaction Not Available");
					result.setDetails("Transaction Not Available");
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
				result.setMessage("Not a valid merchant id");
				result.setMessage("Not a valid merchant id");
			}
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
			result.setMessage("Not a valid user");
			result.setDetails("Not a valid user");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetValues", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> updateProfile(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SessionDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result.setStatus(ResponseStatus.SUCCESS);
							result.setMessage("Dashboard values");
							result.setDetails(userApi.getBescomDashValue(userApi.findByUserName(user.getUsername())));
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} catch (Exception e) {
							e.printStackTrace();
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed, Please try again later.");
							result.setDetails("Failed, Please try again later.");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/FetchCustDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getCustDetails(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BescomRefundRequestDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result = merchantApi.getCustDetailsRequest(dto, userApi.findByUserName(user.getUsername()));
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} catch (Exception e) {
							e.printStackTrace();
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed, Please try again later.");
							result.setDetails("Failed, Please try again later.");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
			}
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/BesomMerchantRefundRequest", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getRefundRequest(@RequestBody BescomRefundRequestDTO dto, @RequestHeader("hash") String hash, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseDTO result = new ResponseDTO();
		boolean isValidHash = SecurityUtil.isHashMatches(dto, hash);
		if (isValidHash) {
//			if (role.equalsIgnoreCase("Merchant")) {
				String sessionId = dto.getSessionId();
				UserSession userSession = userSessionRepository.findByActiveSessionId(sessionId);
				if (userSession != null) {
					UserDTO user = userApi.getUserById(userSession.getUser().getId());
					if (user.getAuthority().contains(Authorities.MERCHANT)
							&& user.getAuthority().contains(Authorities.AUTHENTICATED)) {
						persistingSessionRegistry.refreshLastRequest(sessionId);
						try {
							result = merchantApi.bescomRefundRequest(dto);
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						} catch (Exception e) {
							e.printStackTrace();
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Failed, Please try again later.");
							result.setDetails("Failed, Please try again later.");
							return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
						}
					} else {
						result.setStatus(ResponseStatus.UNAUTHORIZED_USER);
						result.setMessage("Failed, Unauthorized user.");
						result.setDetails("Failed, Unauthorized user.");
						return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
					}
				} else {
					result.setStatus(ResponseStatus.INVALID_SESSION);
					result.setMessage("Please, login and try again.");
					result.setDetails("Please, login and try again.");
					return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
				}
			/*} else {
				result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
				result.setMessage("Failed, Unauthorized user.");
				result.setDetails("Failed, Unauthorized user.");
				return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);

			}*/
		} else {
			result.setStatus(ResponseStatus.INVALID_HASH);
			result.setMessage("Failed, Please try again later.");
			return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
		}
	}
}
