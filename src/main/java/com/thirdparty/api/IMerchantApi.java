package com.thirdparty.api;

import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.BescomRefundRequestDTO;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.UPIResponse;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.request.S2SRequest;
import com.thirdparty.model.request.UpiAuthenticationDTO;
import com.upi.model.UpiRequest;

public interface IMerchantApi {
	ResponseDTO authenticateMerchant(AuthenticationDTO dto, User u);

	ResponseDTO authenticatePayment(PaymentDTO dto, User u, User m);

	PGDetails getDetailsByMerchant(User u);

	String getTokenByMerchant(User u);

	PQService getServiceByMerchant(User merchant);

	boolean containsValidSecretKey(User merchant, String key);

	TPTransaction getByTransactionRefNo(User merchant, String transactionRefNo);

	// TODO Merchant Payment by UPI
	
	ResponseDTO authenticateMerchantUPI(UpiAuthenticationDTO dto, User u);

	ResponseDTO authenticatePaymentUPI(UpiRequest dto, User merchant);
	
	ResponseDTO redirectMerchantUPI(UPIResponse dto);
	
	TPTransaction getByTransactionRefNo(User merchant, String transactionRefNo, double amount);
	
	TPTransaction getByTransactionRefNo(String transactionRefNo);
	
	void bescomS2SCall();
	
	ResponseDTO bescomRefundRequest(BescomRefundRequestDTO dto);
	
	ResponseDTO getAllRefundRequestTxns(PagingDTO dto);
	
	ResponseDTO getCustDetailsRequest(BescomRefundRequestDTO dto, User merchant);
	
	ResponseDTO savingS2Supdates(S2SRequest dto, User merchant);
}
