package com.thirdparty.api;

import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.util.ClientException;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.MerchantRegisterDTO;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.response.SDKResponseDTO;

public interface ISDKPaymentApi {

	SDKResponseDTO authenticateMerchant(AuthenticationDTO dto, User u);

	SDKResponseDTO authenticatePayment(PaymentDTO dto, User u, User m);

	PGDetails getDetailsByMerchant(User u);

	String getTokenByMerchant(User u);

	PQService getServiceByMerchant(User merchant);
	
	User setUnregisteredUser(BusPaymentInit paymentDto, String device);

	void saveUser(MerchantRegisterDTO dto, String device) throws ClientException;

	UserDTO checkPasswordTokenForSDKUser(String key);

	boolean resendMobileTokenSDKUser(String username);
}
