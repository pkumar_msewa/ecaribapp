package com.thirdparty.api.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.BescomRefundRequest;
import com.payqwikapp.entity.MRequestLog;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.UpiCustDetails;
import com.payqwikapp.entity.UpiPgDetails;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.BescomCustDetailsResp;
import com.payqwikapp.model.BescomRefundRequestDTO;
import com.payqwikapp.model.PagingDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UPIResponse;
import com.payqwikapp.model.mobile.ResponseDTO;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.repositories.BescomRefundRequestRepository;
import com.payqwikapp.repositories.MRequestLogRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQServiceRepository;
//import com.payqwikapp.repositories.PQTransactionRepository;
import com.payqwikapp.repositories.TPTransactionRepository;
import com.payqwikapp.repositories.UpiCustDetailsRepository;
import com.payqwikapp.repositories.UpiPgDetailsRepository;
import com.payqwikapp.util.ConvertUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.api.IMerchantApi;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.AuthenticationResponse;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.request.S2SRequest;
import com.thirdparty.model.request.UpiAuthenticationDTO;
import com.upi.api.IUpiApi;
import com.upi.model.UpiRequest;
import com.upi.util.UPIBescomConstants;

public class MerchantApi implements IMerchantApi {

	private final IUserApi userApi;
	private final ITransactionApi transactionApi;
	private final PQServiceRepository pqServiceRepository;
	private final MRequestLogRepository mRequestLogRepository;
	private final PGDetailsRepository pgDetailsRepository;
	private final TPTransactionRepository tpTransactionRepository;
	private final IUpiApi upiApi;
	private final UpiCustDetailsRepository upiCustDetailsRepository;
	private final BescomRefundRequestRepository bescomRefundRequestRepository;
	private final IMailSenderApi mailSenderApi;
	private final UpiPgDetailsRepository upiPgDetailsRepository;

	public MerchantApi(IUserApi userApi, ITransactionApi transactionApi, PQServiceRepository pqServiceRepository,
			MRequestLogRepository mRequestLogRepository, PGDetailsRepository pgDetailsRepository,
			TPTransactionRepository tpTransactionRepository, IUpiApi upiApi,
			UpiCustDetailsRepository upiCustDetailsRepository,
			BescomRefundRequestRepository bescomRefundRequestRepository, IMailSenderApi mailSenderApi,
			UpiPgDetailsRepository upiPgDetailsRepository) {
		this.userApi = userApi;
		this.transactionApi = transactionApi;
		this.pqServiceRepository = pqServiceRepository;
		this.mRequestLogRepository = mRequestLogRepository;
		this.pgDetailsRepository = pgDetailsRepository;
		this.tpTransactionRepository = tpTransactionRepository;
		this.upiApi = upiApi;
		this.upiCustDetailsRepository = upiCustDetailsRepository;
		this.bescomRefundRequestRepository = bescomRefundRequestRepository;
		this.mailSenderApi = mailSenderApi;
		this.upiPgDetailsRepository = upiPgDetailsRepository;
	}

	@Override
	public ResponseDTO authenticateMerchant(AuthenticationDTO dto, User u) {
		ResponseDTO response = new ResponseDTO();
		PGDetails pgDetails = pgDetailsRepository.findByUser(u);
		if (pgDetails != null) {
			boolean isPG = pgDetails.isPaymentGateway();
			if (isPG) {
				TPTransaction tpTransaction = tpTransactionRepository.findByOrderIdAndMerchant(dto.getTransactionID(),
						u);
				if (tpTransaction == null) {
					tpTransaction = new TPTransaction();
					tpTransaction.setMerchant(pgDetails.getUser());
					tpTransaction.setStatus(Status.Processing);
					tpTransaction.setAmount(Double.parseDouble(dto.getAmount()));
					tpTransaction.setOrderId(dto.getTransactionID());
					tpTransactionRepository.save(tpTransaction);
					MRequestLog log = new MRequestLog();
					log.setPgDetails(pgDetails);
					log.setRequest(String.valueOf(dto.toJSON()));
					AuthenticationResponse authenticationResponse = new AuthenticationResponse();
					authenticationResponse.setUsername(u.getUsername());
					authenticationResponse.setSuccessURL(pgDetails.getSuccessURL());
					authenticationResponse.setFailureURL(pgDetails.getReturnURL());
					authenticationResponse.setImage(u.getUserDetail().getImage());
					authenticationResponse.setMerchantId(u.getId());
					response.setStatus(ResponseStatus.SUCCESS);
					response.setMessage("Valid Merchant");
					response.setDetails(authenticationResponse);
					log.setStatus(Status.Success);
					mRequestLogRepository.save(log);
				} else {
					response.setStatus(ResponseStatus.FAILURE);
					response.setMessage("Transaction Already Exists");
				}
			} else {
				response.setStatus(ResponseStatus.FAILURE);
				response.setMessage("Not Authorized to use PG");
			}
		} else {
			response.setStatus(ResponseStatus.FAILURE);
			response.setMessage("Not a Valid Merchant");
		}
		return response;
	}

	@Override
	public ResponseDTO authenticatePayment(PaymentDTO dto, User u, User m) {
		ResponseDTO response = new ResponseDTO();
		PQService merchantService = null;
		PGDetails pgDetails = pgDetailsRepository.findByUser(m);
		if (pgDetails != null) {
			String transactionRefNo = "" + System.currentTimeMillis();
			String name = m.getUserDetail().getFirstName() + " " + m.getUserDetail().getLastName();
			TPTransaction tpTransaction = tpTransactionRepository.findByOrderId(dto.getTransactionID());
			String description = "Payment Of " + dto.getNetAmount() + " to " + name + " of "
					+ tpTransaction.getOrderId();
			if (tpTransaction != null) {
				merchantService = pgDetails.getService();
				Status status = tpTransaction.getStatus();
				if (status.equals(Status.Processing)) {
					tpTransaction.setTransactionRefNo(transactionRefNo);
					tpTransaction.setUser(u);
					transactionApi.initiateMerchantPayment(dto.getNetAmount(), description, null, merchantService,
							transactionRefNo, u.getUsername(), m.getUsername(), true);
					transactionApi.successMerchantPayment(transactionRefNo, true);
					tpTransaction.setStatus(Status.Success);
					tpTransactionRepository.save(tpTransaction);
					response.setStatus(ResponseStatus.SUCCESS);
					response.setMessage("Payment Successful");
					response.setDetails(
							"Your Payment is Successful your " + m.getUserDetail().getFirstName() + " transactionID is "
									+ dto.getTransactionID() + " and VPayQwik Transaction ID is " + transactionRefNo);
					response.setTxnId(transactionRefNo);
				} else {
					response.setStatus(ResponseStatus.FAILURE);
					response.setMessage("Transaction is already Processed");
				}
			} else {
				response.setStatus(ResponseStatus.FAILURE);
				response.setMessage("Transaction Not Exists");
			}
		} else {
			response.setStatus(ResponseStatus.FAILURE);
			response.setMessage("Payment Gateway Not Activated");
		}
		return response;
	}

	@Override
	public PGDetails getDetailsByMerchant(User u) {
		return pgDetailsRepository.findByUser(u);
	}

	@Override
	public String getTokenByMerchant(User u) {
		return pgDetailsRepository.findTokenByMerchant(u);
	}

	@Override
	public PQService getServiceByMerchant(User merchant) {
		return pgDetailsRepository.findServiceByUser(merchant);
	}

	@Override
	public boolean containsValidSecretKey(User merchant, String key) {
		boolean valid = false;
		PGDetails merchantPG = pgDetailsRepository.findByUser(merchant);
		if (merchantPG != null) {
			String validKey = merchantPG.getToken();
			if (validKey != null) {
				if (validKey.equalsIgnoreCase(key)) {
					valid = true;
				}
			}
		}
		return valid;
	}

	@Override
	public TPTransaction getByTransactionRefNo(User merchant, String transactionRefNo) {
		TPTransaction tpTransaction = tpTransactionRepository.findByOrderIdAndMerchant(transactionRefNo, merchant);
		return tpTransaction;
	}

	// Merchant Payment by UPI
	@Override
	public ResponseDTO authenticateMerchantUPI(UpiAuthenticationDTO dto, User u) {
		ResponseDTO response = new ResponseDTO();
		PGDetails pgDetails = pgDetailsRepository.findByUser(u);
		if (pgDetails != null) {
			boolean isPG = pgDetails.isPaymentGateway();
			if (isPG) {
				TPTransaction tpTransaction = tpTransactionRepository.findByOrderIdAndMerchant(dto.getTransactionID(),
						u);
				if (tpTransaction == null) {
					UpiCustDetails cDetails = new UpiCustDetails();
					cDetails.setFirstName(dto.getFirstname());
					cDetails.setLastName(dto.getLastname());
					cDetails.setPhone(dto.getPhone());
					cDetails.setEmail(dto.getEmail());
					cDetails.setAddress1(dto.getAddress1());
					cDetails.setAddress2(dto.getAddress2());
					cDetails.setCity(dto.getCity());
					cDetails.setState(dto.getState());
					cDetails.setCountry(dto.getCountry());
					cDetails.setZipcode(dto.getZipcode());
					cDetails.setOriginalAmount(dto.getAmount());
					cDetails.setAdditionalAmount(dto.getAdditionalcharges());
					cDetails.setProductInfo(dto.getProductinfo());
					cDetails.setSurl(dto.getSurl());
					cDetails.setFurl(dto.getFurl());
					cDetails.setCurl(dto.getCurl());
					cDetails.setCustom_note(dto.getCustom_note());
					cDetails.setVpa(dto.getVpa());
					cDetails.setAccountid(dto.getAccountid());
					cDetails.setPaymenttype(dto.getPaymenttype());
					cDetails.setTransactiontype(dto.getTransactiontype());
					cDetails.setSubdivisioncode(dto.getSubdivisioncode());
					cDetails.setConsumerid(dto.getConsumerid());
					cDetails.setLocaltionCode(dto.getLocationCode());
					cDetails.setBillNo(dto.getBillNo());
					upiCustDetailsRepository.save(cDetails);

					tpTransaction = new TPTransaction();
					tpTransaction.setMerchant(pgDetails.getUser());
					tpTransaction.setStatus(Status.Processing);
					tpTransaction.setApprovedStatus(Status.Processing);
					tpTransaction.setAmount(dto.getAmount());
					tpTransaction.setOrderId(dto.getTransactionID());
					tpTransaction.setCustDetails(cDetails);
					tpTransactionRepository.save(tpTransaction);

					MRequestLog log = new MRequestLog();
					log.setPgDetails(pgDetails);
					log.setRequest(String.valueOf(dto.toJSON()));
					AuthenticationResponse authenticationResponse = new AuthenticationResponse();
					authenticationResponse.setUsername(u.getUsername());
					authenticationResponse.setSuccessURL(pgDetails.getSuccessURL());
					authenticationResponse.setFailureURL(pgDetails.getReturnURL());
					authenticationResponse.setImage(u.getUserDetail().getImage());
					authenticationResponse.setMerchantId(u.getId());
					response.setStatus(ResponseStatus.SUCCESS);
					response.setMessage("Valid Merchant");
					response.setDetails(authenticationResponse);
					log.setStatus(Status.Success);
					mRequestLogRepository.save(log);
				} else {
					response.setStatus(ResponseStatus.FAILURE);
					response.setMessage("Transaction Already Exists");
				}
			} else {
				response.setStatus(ResponseStatus.FAILURE);
				response.setMessage("Not Authorized to use PG");
			}
		} else {
			response.setStatus(ResponseStatus.FAILURE);
			response.setMessage("Not a Valid Merchant");
		}
		return response;
	}

	@Override
	public ResponseDTO authenticatePaymentUPI(UpiRequest dto, User mercahnt) {
		ResponseDTO response = new ResponseDTO();
		PQService merchantService = null;
		PGDetails pgDetails = pgDetailsRepository.findByUser(mercahnt);
		if (pgDetails != null) {
			TPTransaction tpTransaction = tpTransactionRepository.findByOrderId(dto.getmTxnId());
			if (tpTransaction != null) {
				merchantService = pgDetails.getService();
				Status status = tpTransaction.getStatus();
				if (status.equals(Status.Processing)) {
					UpiPgDetails upiPgDetails = upiPgDetailsRepository.getUpiPgDetails(pgDetails);
					if (upiPgDetails != null) {
						response = upiApi.upiHandlerMerchant(com.thirdparty.util.ConvertUtil.getUpiRequest(dto, upiPgDetails), mercahnt, merchantService);
					} else {
						response.setStatus(ResponseStatus.FAILURE);
						response.setMessage("Merchant details not found");
					}
				} else {
					response.setStatus(ResponseStatus.FAILURE);
					response.setMessage("Transaction is already Processed");
				}
			} else {
				response.setStatus(ResponseStatus.FAILURE);
				response.setMessage("Transaction Not Exists");
			}
		} else {
			response.setStatus(ResponseStatus.FAILURE);
			response.setMessage("Payment Gateway Not Activated");
		}
		return response;
	}

	@Override
	public TPTransaction getByTransactionRefNo(User merchant, String transactionRefNo, double amount) {
		TPTransaction tpTransaction = tpTransactionRepository.findByOrderIdAndMerchantAmount(transactionRefNo, merchant,
				amount);
		return tpTransaction;
	}

	@Override
	public ResponseDTO redirectMerchantUPI(UPIResponse dto) {
		TPTransaction tpTransaction = getByTransactionRefNo(dto.getReferenceId());
		ResponseDTO result = new ResponseDTO();
		if (tpTransaction != null) {
			Status status = tpTransaction.getStatus();
			if (status.equals(Status.Processing)) {
				// TODO Call Success Txns Method
				result = upiApi.upiResponseHandlerMerchant(dto);
				System.err.println("Result Reponse; : " + result.getStatus());
				System.err.println("Status Reponse : : " + ResponseStatus.SUCCESS.getKey());
				if (result.getStatus().equals(ResponseStatus.SUCCESS.getKey())) {
					tpTransaction.setStatus(Status.Success);
					tpTransaction.setApprovedStatus(Status.Success);
					tpTransaction.setTransactionRefNo(result.getTxnId());
					tpTransactionRepository.save(tpTransaction);
					System.err.println("Saving transaction response : : -------------------- " + result.getTxnId());
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("Transaction Details");
				} else {
					tpTransaction.setStatus(Status.Failed);
					tpTransaction.setApprovedStatus(Status.Failed);
					tpTransaction.setTransactionRefNo(result.getTxnId());
					tpTransactionRepository.save(tpTransaction);
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction already processed");
				result.setDetails("Transaction already processed");
			}
		}
		return result;
	}

	@Override
	public TPTransaction getByTransactionRefNo(String transactionRefNo) {
		TPTransaction tpTransaction = tpTransactionRepository.findByOrderId(
				transactionApi.getTransactionByRetrivalReferenceNo(transactionRefNo).getAuthReferenceNo());
		return tpTransaction;
	}

	@Override
	public void bescomS2SCall() {
		try {
			List<TPTransaction> txnList = tpTransactionRepository.getAllApprovedTxns();
			if (!txnList.isEmpty()) {
				for (TPTransaction t : txnList) {
					Client client = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					S2SRequest request = ConvertUtil.convertS2SRequest(t, pgDetailsRepository.findByUser(t.getMerchant()));
					WebResource resource = client.resource(UPIBescomConstants.BESCOM_S2S_CALL_URL);
					ClientResponse response = resource.post(ClientResponse.class, request.toJSON());
					String strResponse = response.getEntity(String.class);
					System.err.println("string response ::" + strResponse);
					if (response.getStatus() == 200) {
						org.json.JSONObject json = new org.json.JSONObject(strResponse);
						System.err.println(json);
						if (json != null) {
							t.setAcknowledgeResp(strResponse);
							if (json.getString("status").equals("50")) {
								t.setAcknowledgeNo(json.getString("receiptId"));
								t.setApprovedStatus(Status.Completed);
								tpTransactionRepository.save(t);
								System.err.println("Transaction Saved Successfully");
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public ResponseDTO bescomRefundRequest(BescomRefundRequestDTO dto) {
		// TODO Auto-generated method stub
		ResponseDTO result = new ResponseDTO();
		try {
			PQTransaction txn = transactionApi.getTransactionByRefNo(dto.getTransactionRefNo() + "C");
			if (txn != null) {
				BescomRefundRequest b = bescomRefundRequestRepository
						.findByTransactionRefNo(dto.getTransactionRefNo() + "C");
				if (b == null) {
					if (!txn.getStatus().equals(Status.Refunded)) {
						if (txn.getStatus().equals(Status.Success)) {
							if (dto.getAmount() == txn.getAmount()) {
								b = new BescomRefundRequest();
								b.setAmount(dto.getAmount());
								b.setStatus(Status.Processing);
								b.setTrnasactionRefNo(txn.getTransactionRefNo());
								b.setTransaction(txn);
								bescomRefundRequestRepository.save(b);
								result.setStatus(ResponseStatus.SUCCESS);
								result.setMessage("Transaction refund request has successfully sent");
								result.setDetails("Transaction refund request has successfully sent");

								// TODO Implement Mail
								// mailSenderApi.refundRequestBescomMerchant(filedetails,
								// merchantName, merchantId);
							} else {
								result.setStatus(ResponseStatus.FAILURE);
								result.setMessage("Transaction amount doesn't match");
								result.setDetails("Transaction amount doesn't match");
							}
						} else {
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage("Transaction should be success and processed");
							result.setDetails("Transaction should be success and processed");
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage("Transaction already refunded");
						result.setDetails("Transaction already refunded");
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage("Transaction request is already processed");
					result.setDetails("Transaction request is already processed");
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not found");
				result.setDetails("Transaction not found");
			}
		} catch (Exception e) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}
		return result;
	}

	@Override
	public ResponseDTO getAllRefundRequestTxns(PagingDTO dto) {
		// TODO Auto-generated method stub
		ResponseDTO result = new ResponseDTO();
		try {
			Sort sort = new Sort(Sort.Direction.DESC, "id");
			Pageable page = new PageRequest(dto.getPage(), dto.getSize(), sort);
			Page<BescomRefundRequest> txnList = bescomRefundRequestRepository.findAll(page);
			if (txnList != null) {
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Merchant refund request transactions");
				result.setDetails(txnList);
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction not found");
				result.setDetails("Transaction not found");
			}
		} catch (Exception e) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}
		return result;
	}

	@Override
	public ResponseDTO getCustDetailsRequest(BescomRefundRequestDTO dto, User merchant) {
		// TODO Auto-generated method stub
		ResponseDTO result = new ResponseDTO();
		try {
			TPTransaction tp = tpTransactionRepository.findByTransactionRefNoAndMerchant(dto.getTransactionRefNo(),
					merchant);
			if (tp != null) {
				BescomCustDetailsResp resp = new BescomCustDetailsResp();
				resp.setEmail(tp.getCustDetails().getEmail());
				resp.setMobile(tp.getCustDetails().getPhone());
				resp.setVpa(tp.getCustDetails().getVpa());
				result.setStatus(ResponseStatus.SUCCESS);
				result.setMessage("Custmer details");
				result.setDetails(resp);
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction doesn't exits");
				result.setDetails("Transaction doesn't exits");
			}
		} catch (Exception e) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}
		return result;
	}

	@Override
	public ResponseDTO savingS2Supdates(S2SRequest dto, User merchant) {
		ResponseDTO result = new ResponseDTO();
		try {
			TPTransaction tp = tpTransactionRepository.findByOrderIdAndMerchant(dto.getMerchanttxnid(), merchant);
			if (tp != null) {
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
				// TODO Call S2S CallBack for both bescom urban and bescom rural
				if (merchant.getUsername().equals(UPIBescomConstants.BECOM_RURAL_MERCHANT)) {
					dto.setRural(true);
					dto.setMsg(com.thirdparty.util.ConvertUtil.convertBescomRuralRequest(dto, tp));
				}
				WebResource resource = client.resource(UPIBescomConstants.BESCOM_S2S_CALL_URL);
				ClientResponse response = resource.post(ClientResponse.class, dto.toJSON());
				String strResponse = response.getEntity(String.class);
				System.err.println("string response ::" + strResponse);
				if (strResponse != null) {
					org.json.JSONObject json = new org.json.JSONObject(strResponse);
					System.err.println(json);
					if (json != null) {
						tp.setAcknowledgeResp(strResponse);
						if (json.getString("status").equals("50")) {
							tp.setAcknowledgeNo(json.getString("receiptId"));
							tp.setApprovedStatus(Status.Completed);
							tpTransactionRepository.save(tp);
						}
					}
				}
			} else {
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage("Transaction doesn't exits");
				result.setDetails("Transaction doesn't exits");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
