package com.thirdparty.api.impl;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebs.model.EBSRedirectResponse;
import com.payqwikapp.entity.FRMDetails;
import com.payqwikapp.entity.LoginLog;
import com.payqwikapp.entity.SessionLog;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.FrmLoadMoneyDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.repositories.FRMDetailsRepository;
import com.payqwikapp.util.CommonUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.response.FRMResponseDTO;
import com.thirdparty.util.FRMConstants;

public class FRMApi implements IFRMApi {
	protected final Logger logger= LoggerFactory.getLogger(this.getClass());
	private static final String empty="";
	private final FRMDetailsRepository fRMDetailsRepository;
	
	
	public FRMApi(FRMDetailsRepository fRMDetailsRepository) {
		this.fRMDetailsRepository=fRMDetailsRepository;
	}

	@Override
	public FRMResponseDTO decideLoadMoney(FrmLoadMoneyDTO dto, User user) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			String payload = getDecideLoadMoneyJson(dto, user);
			response = getDecideResponse(payload, FRMConstants.frmFtDecide);
		} catch (Exception e) {
			response.setDecideResp(FRMConstants.getAdvice(false));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public FRMResponseDTO decideFundTransfer(SendMoneyBankDTO dto, User user) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			String payload = getDecideFundTransferJson(dto, user);
			response = getDecideResponse(payload, FRMConstants.frmFtDecide);
		} catch (Exception e) {
			response.setDecideResp(FRMConstants.getAdvice(false));
			e.printStackTrace();
		}
		return response;
	}

	private String getDecideFundTransferJson(SendMoneyBankDTO dto, User user) {
		StringWriter jsnReqStr = new StringWriter();
		JsonObjectBuilder payload=null;
		try {
			payload = getCommonJson(user.getId().toString(),"ft_fundstransfer","fundstransfer","ft");
			payload.add("msgBody", getMessageBody(dto,user));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}
	
	@Override
	public FRMResponseDTO decideFundTransferWallet(SendMoneyMobileDTO dto, User user) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			String payload = getDecideFundTransferWalletJson(dto, user);
			response = getDecideResponse(payload, FRMConstants.frmFtDecide);
		} catch (Exception e) {
			response.setDecideResp(FRMConstants.getAdvice(false));
			e.printStackTrace();
		}
		return response;
	}

	private String getDecideFundTransferWalletJson(SendMoneyMobileDTO dto, User user) {
		StringWriter jsnReqStr = new StringWriter();
		JsonObjectBuilder payload=null;
		try {
			payload = getCommonJson(user.getId().toString(),"ft_fundstransfer","fundstransfer","ft");
			payload.add("msgBody", getMessageBodyWallet(dto,user));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}
	
	private String getDecideLoadMoneyJson(FrmLoadMoneyDTO dto, User user) {
		StringWriter jsnReqStr = new StringWriter();
		JsonObjectBuilder payload=null;
		try {
			payload = getCommonJson(user.getId().toString(),"ft_fundstransfer","fundstransfer","ft");
			payload.add("msgBody", getMessageBodyLoadMoney(dto,user));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}
	
	@Override
	public FRMResponseDTO decideRecharge(CommonRechargeDTO dto, User user) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			String payload = getDecideFundRechargeJson(dto, user);
			response = getDecideResponse(payload, FRMConstants.frmFtDecide);
		} catch (Exception e) {
			response.setDecideResp(FRMConstants.getAdvice(false));
			e.printStackTrace();
		}
		return response;
	}

	private String getDecideFundRechargeJson(CommonRechargeDTO dto, User user) {
		StringWriter jsnReqStr = new StringWriter();
		JsonObjectBuilder payload=null;
		try {
			payload = getCommonJson(user.getId().toString(),"ft_fundstransfer","fundstransfer","ft");
			payload.add("msgBody", getRechargeMessageBody(dto,user));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}

	private JsonObjectBuilder getCommonJson(String hostUserId,String eventName,String eventSubType,String eventType) {
		JsonObjectBuilder payload = Json.createObjectBuilder();
		try {
			payload.add("eventId", System.nanoTime()+"");
			payload.add("hostUserId", hostUserId );
			payload.add("eventName",eventName );
			payload.add("eventSubtype", eventSubType);
			payload.add("eventType",eventType );
			payload.add("source", FRMConstants.channel);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return payload;
	}
	
	private JsonObjectBuilder getRechargeMessageBody(CommonRechargeDTO dto, User user) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", FRMConstants.channel);
			mesgBoday.add("mobile_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_acct_id", dto.getAccountNumber());
			mesgBoday.add("city_code", empty);
			mesgBoday.add("tran_id", dto.getTransactionRefNo());
			mesgBoday.add("payee_id", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_mob_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_code", empty);
			mesgBoday.add("branch_id", empty);
			mesgBoday.add("aadhar_no", empty);
			mesgBoday.add("sys_time", CommonUtil.getCurrentDateTime());
			mesgBoday.add("agent_code", empty);
			mesgBoday.add("acct_name", empty);
			mesgBoday.add("tran_rmks", dto.getServiceCode());
			mesgBoday.add("terminal_id", empty);
			mesgBoday.add("payee_ifsc_code", user.getUserDetail().getContactNo());
			mesgBoday.add("auth_type", empty);
			mesgBoday.add("device_id", user.getUserDetail().getImeiNo()!=null ? user.getUserDetail().getImeiNo() : "NA");
			mesgBoday.add("tran_type", 1);
			mesgBoday.add("vpa", empty);
			mesgBoday.add("payee_nick_name", empty);
			mesgBoday.add("ip_address", "172.16.7.29");
			mesgBoday.add("mcc_id", empty);
			mesgBoday.add("country_code", empty);
			mesgBoday.add("cust_card_id", user.getUserDetail().getContactNo());
			mesgBoday.add("account_id",dto.getAccountNumber() );
			mesgBoday.add("user_id", user.getId());
			mesgBoday.add("corporate_id", empty);
			mesgBoday.add("payee_name", user.getUserDetail().getContactNo());
			mesgBoday.add("tran_amt", dto.getAmount());
			mesgBoday.add("network_type", empty);
			mesgBoday.add("cust_id", user.getUserDetail().getContactNo());
			mesgBoday.add("tran_crncy_code", "INR");
			mesgBoday.add("eventts", CommonUtil.getCurrentDateTime());
			mesgBoday.add("account_type", "KYC".equals(user.getAccountDetail().getAccountType().getName()) ? "KYC" :"Non KYC");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}

	private JsonObjectBuilder getMessageBodyLoadMoney(FrmLoadMoneyDTO dto, User user) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", FRMConstants.channel);
			mesgBoday.add("mobile_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_acct_id", user.getUserDetail().getContactNo());
			mesgBoday.add("city_code", empty);
			mesgBoday.add("tran_id", dto.getTransactionRefNo());
			mesgBoday.add("payee_id", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_mob_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_code", empty);
			mesgBoday.add("branch_id", empty);
			mesgBoday.add("aadhar_no", empty);
			mesgBoday.add("sys_time", CommonUtil.getCurrentDateTime());
			mesgBoday.add("agent_code", empty);
			mesgBoday.add("acct_name", empty);
			mesgBoday.add("tran_rmks", dto.getServiceCode());
			mesgBoday.add("terminal_id", empty);
			mesgBoday.add("payee_ifsc_code", user.getUserDetail().getContactNo());
			mesgBoday.add("auth_type", empty);
			mesgBoday.add("device_id", user.getUserDetail().getImeiNo()!=null ? user.getUserDetail().getImeiNo() : "NA");
			mesgBoday.add("tran_type", 0);
			mesgBoday.add("vpa", empty);
			mesgBoday.add("payee_nick_name", empty);
			mesgBoday.add("ip_address", "172.16.7.29");
			mesgBoday.add("mcc_id", empty);
			mesgBoday.add("country_code", empty);
			mesgBoday.add("cust_card_id", user.getUserDetail().getContactNo());
			mesgBoday.add("account_id",user.getUsername());
			mesgBoday.add("user_id", user.getId());
			mesgBoday.add("corporate_id", empty);
			mesgBoday.add("payee_name", user.getUserDetail().getName());
			mesgBoday.add("tran_amt", dto.getAmount());
			mesgBoday.add("network_type", empty);
			mesgBoday.add("cust_id", user.getUserDetail().getContactNo());
			mesgBoday.add("tran_crncy_code", "INR");
			mesgBoday.add("eventts", CommonUtil.getCurrentDateTime());
			mesgBoday.add("account_type", "KYC".equals(user.getAccountDetail().getAccountType().getName()) ? "KYC" :"Non KYC");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}
	
	private JsonObjectBuilder getMessageBodyWallet(SendMoneyMobileDTO dto, User user) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", FRMConstants.channel);
			mesgBoday.add("mobile_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_acct_id", user.getUserDetail().getContactNo());
			mesgBoday.add("city_code", empty);
			mesgBoday.add("tran_id", dto.getTransactionrefno());
			mesgBoday.add("payee_id", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_mob_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_code", empty);
			mesgBoday.add("branch_id", empty);
			mesgBoday.add("aadhar_no", empty);
			mesgBoday.add("sys_time", CommonUtil.getCurrentDateTime());
			mesgBoday.add("agent_code", empty);
			mesgBoday.add("acct_name", empty);
			mesgBoday.add("tran_rmks", dto.getServiceCode());
			mesgBoday.add("terminal_id", empty);
			mesgBoday.add("payee_ifsc_code", user.getUserDetail().getContactNo());
			mesgBoday.add("auth_type", empty);
			mesgBoday.add("device_id", user.getUserDetail().getImeiNo()!=null ? user.getUserDetail().getImeiNo() : "NA");
			mesgBoday.add("tran_type", 1);
			mesgBoday.add("vpa", empty);
			mesgBoday.add("payee_nick_name", empty);
			mesgBoday.add("ip_address", "172.16.7.29");
			mesgBoday.add("mcc_id", empty);
			mesgBoday.add("country_code", empty);
			mesgBoday.add("cust_card_id", user.getUserDetail().getContactNo());
			mesgBoday.add("account_id",empty);
			mesgBoday.add("user_id", user.getId());
			mesgBoday.add("corporate_id", empty);
			mesgBoday.add("payee_name", empty);
			mesgBoday.add("tran_amt", dto.getAmount());
			mesgBoday.add("network_type", empty);
			mesgBoday.add("cust_id", user.getUserDetail().getContactNo());
			mesgBoday.add("tran_crncy_code", "INR");
			mesgBoday.add("eventts", CommonUtil.getCurrentDateTime());
			mesgBoday.add("account_type", "KYC".equals(user.getAccountDetail().getAccountType().getName()) ? "KYC" :"Non KYC");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}
	
	private JsonObjectBuilder getMessageBody(SendMoneyBankDTO dto, User user) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", FRMConstants.channel);
			mesgBoday.add("mobile_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_acct_id", dto.getAccountNumber());
			mesgBoday.add("city_code", empty);
			mesgBoday.add("tran_id", dto.getTransactionRefNo());
			mesgBoday.add("payee_id", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_mob_no", user.getUserDetail().getContactNo());
			mesgBoday.add("payee_code", empty);
			mesgBoday.add("branch_id", empty);
			mesgBoday.add("aadhar_no", empty);
			mesgBoday.add("sys_time", CommonUtil.getCurrentDateTime());
			mesgBoday.add("agent_code", empty);
			mesgBoday.add("acct_name", empty);
			mesgBoday.add("tran_rmks", dto.getServiceCode());
			mesgBoday.add("terminal_id", empty);
			mesgBoday.add("payee_ifsc_code", dto.getIfscCode());
			mesgBoday.add("auth_type", empty);
			mesgBoday.add("device_id", user.getUserDetail().getImeiNo()!=null ? user.getUserDetail().getImeiNo() : "NA");
			mesgBoday.add("tran_type", 1);
			mesgBoday.add("vpa", empty);
			mesgBoday.add("payee_nick_name", empty);
			mesgBoday.add("ip_address", "172.16.7.29");
			mesgBoday.add("mcc_id", empty);
			mesgBoday.add("country_code", empty);
			mesgBoday.add("cust_card_id", user.getUserDetail().getContactNo());
			mesgBoday.add("account_id",dto.getAccountNumber() );
			mesgBoday.add("user_id", user.getId());
			mesgBoday.add("corporate_id", empty);
			mesgBoday.add("payee_name", dto.getAccountName());
			mesgBoday.add("tran_amt", dto.getAmount());
			mesgBoday.add("network_type", empty);
			mesgBoday.add("cust_id", user.getUserDetail().getContactNo());
			mesgBoday.add("tran_crncy_code", "INR");
			mesgBoday.add("eventts", CommonUtil.getCurrentDateTime());
			mesgBoday.add("account_type", "KYC".equals(user.getAccountDetail().getAccountType().getName()) ? "KYC" :"Non KYC");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}

	@Override
	public void saveFrmDetails(String contact, String decideResp, boolean enqueueResponse, String transactionRefNo,
			String requestFor) {
		FRMDetails details = new FRMDetails();
		details.setDecideResponse(decideResp);
		details.setEnqueueResponse(enqueueResponse);
		details.setTransactionRefNo(transactionRefNo);
		details.setUserContact(contact);
		details.setRequestFor(requestFor);
		fRMDetailsRepository.save(details);
	}

	@Override
	public FRMResponseDTO decideRegistration(RegisterDTO user) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			String payload = getFrmRegistrationJson(user);
			response = getDecideResponse(payload, FRMConstants.frmRegDecide);
		} catch (Exception e) {
			response.setDecideResp(FRMConstants.getAdvice(false));
			e.printStackTrace();
		}
		return response;
	}

	private String getFrmRegistrationJson(RegisterDTO user) {
		StringWriter jsnReqStr = new StringWriter();
		JsonObjectBuilder payload=null;
		try {
			payload = getCommonJson(empty,"nft_walletreg","walletreg","nft");
			payload.add("msgBody", getRegistrationMesgBody(user));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}

	private JsonObjectBuilder getRegistrationMesgBody(RegisterDTO user) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", FRMConstants.channel);
			mesgBoday.add("mobile_no", user.getUsername());
			mesgBoday.add("virtual_account_id", user.getUsername());
			mesgBoday.add("event_id", System.nanoTime()+"");
			mesgBoday.add("email_id", user.getEmail());
			mesgBoday.add("pin_code", user.getLocationCode());
			mesgBoday.add("sys_time", CommonUtil.getCurrentDateTime());
			mesgBoday.add("device_id", user.getImeiNo()!=null ? user.getImeiNo() : "NA");
			mesgBoday.add("addr_network", "172.16.7.29");
			mesgBoday.add("country", "India");
			mesgBoday.add("succ_fail_flg", "S");
			mesgBoday.add("account_id",user.getUsername());
			mesgBoday.add("user_id", user.getUsername());
			mesgBoday.add("error_code", "S00");
			mesgBoday.add("error_desc", "Success");
			mesgBoday.add("mfa_type", "OTP");
			mesgBoday.add("cust_id", user.getUsername());
			mesgBoday.add("eventts", CommonUtil.getCurrentDateTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}


	@Override
	public FRMResponseDTO decideLogin(User user, SessionLog sessionLog,LoginLog loginLog) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			String payload = getFrmLoginJson(user, sessionLog,loginLog);
			response = getDecideResponse(payload, FRMConstants.frmLoginDecide);
		} catch (Exception e) {
			response.setDecideResp(FRMConstants.getAdvice(false));
			e.printStackTrace();
		}
		return response;
	}

	private String getFrmLoginJson(User user, SessionLog sessionLog,LoginLog loginLog) {
		StringWriter jsnReqStr = new StringWriter();
		JsonObjectBuilder payload=null;
		try {
			payload = getCommonJson(empty,"nft_dblogin","dblogin","nft");
			payload.add("msgBody", getLoginMesgBody(user,sessionLog,loginLog));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}

	private JsonObjectBuilder getLoginMesgBody(User user, SessionLog sessionLog,LoginLog loginLog) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", FRMConstants.channel);
			mesgBoday.add("mobile_no", user.getUsername());
			mesgBoday.add("corporate_id", user.getUsername());
			mesgBoday.add("lastLogints", sessionLog!=null ? sessionLog.getCreated().toString() : CommonUtil.getCurrentDateTime());
			mesgBoday.add("logints", CommonUtil.getCurrentDateTime());
			mesgBoday.add("last_login_ip", sessionLog!=null ? sessionLog.getRemoteAddress() :"172.16.7.29");
			mesgBoday.add("device_type", user.getOsName() !=null ? user.getOsName() :"Android");
			mesgBoday.add("user_type", UserType.User.toString());
			mesgBoday.add("sys_time", CommonUtil.getCurrentDateTime());
			mesgBoday.add("device_id", user.getUserDetail().getImeiNo()!=null ? user.getUserDetail().getImeiNo() : "NA");
			mesgBoday.add("addr_network", loginLog!=null ? loginLog.getServerIp() : "172.16.7.29");
			mesgBoday.add("country", "India");
			mesgBoday.add("succ_fail_flg", "S");
			mesgBoday.add("account_id",user.getUsername());
			mesgBoday.add("user_id", user.getUsername());
			mesgBoday.add("error_code", "S00");
			mesgBoday.add("error_desc", "Success");
			mesgBoday.add("mfa_type", "OTP");
			mesgBoday.add("cust_id", user.getUsername());
			mesgBoday.add("eventts", CommonUtil.getCurrentDateTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}
	private FRMResponseDTO getDecideResponse(String payload, String decideUrl) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			Client client = Client.create();
			client.setConnectTimeout(10);
			client.setReadTimeout(10);
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(decideUrl);
			ClientResponse clientResponse = resource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, payload);
			String frmResponse = clientResponse.getEntity(String.class);
			System.out.println("frm load money response in app: "+frmResponse);
			if (frmResponse != null && !frmResponse.isEmpty()) {
				JSONObject obj = new JSONObject(frmResponse);
				if (obj.getBoolean("success") 
						&& FRMConstants.getAdvice(true).equalsIgnoreCase(obj.getString("decideResp"))
						&& obj.getBoolean("enqueueResp")) {
					response.setDecideResp(FRMConstants.getAdvice(true));
					response.setEnqueueResp(obj.getBoolean("enqueueResp"));
					response.setSuccess(true);
				} else {
					response.setDecideResp(FRMConstants.getAdvice(false));
				}
			} else {
				response.setDecideResp(FRMConstants.getAdvice(false));
			}
		} catch (Exception e) {
			if(e.getMessage().equalsIgnoreCase("Read timed out")){
			response.setMessage(e.getMessage());
			response.setSuccess(true);
			}else{
				e.printStackTrace();
				response.setSuccess(false);
			}
		}
		return response;
	}
}
