package com.thirdparty.api.impl;

import java.text.SimpleDateFormat;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.payqwikapp.api.IMailSenderApi;
import com.payqwikapp.api.ISMSSenderApi;
import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.api.IUserApi;
import com.payqwikapp.entity.LocationDetails;
import com.payqwikapp.entity.MRequestLog;
import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.PQAccountDetail;
import com.payqwikapp.entity.PQAccountType;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PasswordHistory;
import com.payqwikapp.entity.SecurityAnswerDetails;
import com.payqwikapp.entity.SecurityQuestion;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.User;
import com.payqwikapp.entity.UserDetail;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.Status;
import com.payqwikapp.model.UserDTO;
import com.payqwikapp.model.UserType;
import com.payqwikapp.model.mobile.ResponseStatus;
import com.payqwikapp.model.travel.bus.BusPaymentInit;
import com.payqwikapp.repositories.BescomRefundRequestRepository;
import com.payqwikapp.repositories.LocationDetailsRepository;
import com.payqwikapp.repositories.MRequestLogRepository;
import com.payqwikapp.repositories.PGDetailsRepository;
import com.payqwikapp.repositories.PQAccountDetailRepository;
import com.payqwikapp.repositories.PQAccountTypeRepository;
import com.payqwikapp.repositories.PQServiceRepository;
import com.payqwikapp.repositories.PasswordHistoryRepository;
import com.payqwikapp.repositories.TPTransactionRepository;
import com.payqwikapp.repositories.UpiCustDetailsRepository;
import com.payqwikapp.repositories.UserDetailRepository;
import com.payqwikapp.repositories.UserRepository;
import com.payqwikapp.sms.util.SMSAccount;
import com.payqwikapp.sms.util.SMSTemplate;
import com.payqwikapp.util.Authorities;
import com.payqwikapp.util.ClientException;
import com.payqwikapp.util.CommonUtil;
import com.payqwikapp.util.ConvertUtil;
import com.payqwikapp.util.PayQwikUtil;
import com.thirdparty.api.ISDKPaymentApi;
import com.thirdparty.model.request.AuthenticationDTO;
import com.thirdparty.model.request.AuthenticationResponse;
import com.thirdparty.model.request.MerchantRegisterDTO;
import com.thirdparty.model.request.PaymentDTO;
import com.thirdparty.model.response.SDKResponseDTO;
import com.upi.api.IUpiApi;

public class SDKPaymentApi implements ISDKPaymentApi {
	
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
    private final IUserApi userApi;
    private final ITransactionApi transactionApi;
    private final PQServiceRepository pqServiceRepository;
    private final MRequestLogRepository mRequestLogRepository;
    private final PGDetailsRepository pgDetailsRepository;
    private final TPTransactionRepository tpTransactionRepository;
    private final ISMSSenderApi smsSenderApi ;
    private final IMailSenderApi mailSenderApi;
    private final UserRepository userRepository;
	private final UserDetailRepository userDetailRepository;
	private final PQAccountDetailRepository pqAccountDetailRepository;
	private final PQAccountTypeRepository pqAccountTypeRepository;
	private final PasswordEncoder passwordEncoder;
	private final LocationDetailsRepository locationDetailsRepository;
	private final PasswordHistoryRepository passwordHistoryRepository;

    public SDKPaymentApi(IUserApi userApi, ITransactionApi transactionApi, PQServiceRepository pqServiceRepository, MRequestLogRepository mRequestLogRepository,
    		PGDetailsRepository pgDetailsRepository, TPTransactionRepository tpTransactionRepository,ISMSSenderApi smsSenderApi,IMailSenderApi mailSenderApi,UserRepository userRepository,UserDetailRepository userDetailRepository,
    		 PQAccountDetailRepository pqAccountDetailRepository,PQAccountTypeRepository pqAccountTypeRepository, PasswordEncoder passwordEncoder,
    		 LocationDetailsRepository locationDetailsRepository,PasswordHistoryRepository passwordHistoryRepository) {
        this.userApi = userApi;
        this.transactionApi = transactionApi;
        this.pqServiceRepository = pqServiceRepository;
        this.mRequestLogRepository = mRequestLogRepository;
        this.pgDetailsRepository = pgDetailsRepository;
        this.tpTransactionRepository = tpTransactionRepository;
        this.smsSenderApi =smsSenderApi;
        this.mailSenderApi = mailSenderApi;
        this.userRepository=userRepository;
        this.userDetailRepository=userDetailRepository;
        this.pqAccountDetailRepository=pqAccountDetailRepository;
        this.pqAccountTypeRepository=pqAccountTypeRepository;
        this.passwordEncoder=passwordEncoder;
        this.locationDetailsRepository=locationDetailsRepository;
        this.passwordHistoryRepository=passwordHistoryRepository;
    }

	 @Override
	    public SDKResponseDTO authenticateMerchant(AuthenticationDTO dto,User u){
		 SDKResponseDTO response = new SDKResponseDTO();
	        PGDetails pgDetails = pgDetailsRepository.findByUser(u);
	        if(pgDetails != null) {
	            boolean isPG = pgDetails.isPaymentGateway();
	            if(isPG) {
	                TPTransaction tpTransaction = tpTransactionRepository.findByOrderIdAndMerchant(dto.getTransactionID(), u);
	                if (tpTransaction == null) {
	                    tpTransaction = new TPTransaction();
	                    tpTransaction.setMerchant(pgDetails.getUser());
	                    tpTransaction.setStatus(Status.Processing);
	                    tpTransaction.setAmount(Double.parseDouble(dto.getAmount()));
	                    tpTransaction.setOrderId(dto.getTransactionID());
	                    tpTransactionRepository.save(tpTransaction);
	                    MRequestLog log = new MRequestLog();
	                    log.setPgDetails(pgDetails);
	                    log.setRequest(String.valueOf(dto.toJSON()));
	                    AuthenticationResponse authenticationResponse = new AuthenticationResponse();
	                    authenticationResponse.setUsername(u.getUsername());
	                    authenticationResponse.setSuccessURL(pgDetails.getSuccessURL());
	                    authenticationResponse.setFailureURL(pgDetails.getReturnURL());
	                    authenticationResponse.setImage(u.getUserDetail().getImage());
	                    authenticationResponse.setMerchantId(u.getId());
	                    response.setStatus(ResponseStatus.SUCCESS);
	                    response.setMessage("Valid Merchant");
	                    response.setDetails(authenticationResponse);
	                    log.setStatus(Status.Success);
	                    mRequestLogRepository.save(log);
	                } else {
	                    response.setStatus(ResponseStatus.FAILURE);
	                    response.setMessage("Transaction Already Exists");
	                }
	            }else {
	                response.setStatus(ResponseStatus.FAILURE);
	                response.setMessage("Not Authorized to use PG");
	            }
	        }else {
	            response.setStatus(ResponseStatus.FAILURE);
	            response.setMessage("Not a Valid Merchant");
	        }
	    return response;
	    }

	@Override
	public SDKResponseDTO authenticatePayment(PaymentDTO dto, User u, User m) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public PGDetails getDetailsByMerchant(User u) {
        return pgDetailsRepository.findByUser(u);
    }

    @Override
    public String getTokenByMerchant(User u) {
        return pgDetailsRepository.findTokenByMerchant(u);
    }

    @Override
    public PQService getServiceByMerchant(User merchant) {
        return pgDetailsRepository.findServiceByUser(merchant);
    }

	@Override
	public User setUnregisteredUser(BusPaymentInit paymentDto,String device) {
		// TODO Auto-generated method stub
		User receiverUser = userApi.findByUserName(paymentDto.getUserMobile());
		if (receiverUser == null) {
			RegisterDTO dto = new RegisterDTO();
			dto.setFirstName(" ");
			dto.setLastName(" ");
			dto.setMiddleName(" ");
			dto.setEmail("");
			dto.setContactNo(paymentDto.getUserMobile());
			dto.setAddress("");
			dto.setLocationCode("");
			dto.setUserType(UserType.User);
			dto.setUsername(paymentDto.getUserMobile());
			dto.setPassword("");
			dto.setConfirmPassword("");
			try {
				receiverUser = userApi.saveUnregisteredSdkUser(dto,device);
				return receiverUser;
			} catch (ClientException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	@Override
	public void saveUser(MerchantRegisterDTO dto,String device) throws ClientException {
		try {
			User user = new User();
			PasswordHistory history = new PasswordHistory();
			UserDetail userDetail = new UserDetail();
			userDetail.setAddress(dto.getAddress());
			userDetail.setContactNo(dto.getMobileNumber());
			userDetail.setFirstName(dto.getName());
			userDetail.setLastName(dto.getName());
			userDetail.setEmail(dto.getEmail());
			userDetail.setDateOfBirth(formatter.parse(dto.getDateOfBirth()));
			userDetail.setGender(dto.getGender());
			userDetail.setBrand(dto.getBrand());
			userDetail.setModel(dto.getModel());
			userDetail.setImeiNo(dto.getImeiNo());

			if (dto.getUserType().equals(UserType.Admin) || dto.getUserType().equals(UserType.Merchant)) {
				if (dto.getUserType().equals(UserType.Admin)) {
					user.setAuthority(Authorities.ADMINISTRATOR + "," + Authorities.AUTHENTICATED);
				} else if (dto.getUserType().equals(UserType.Merchant)) {
					user.setAuthority(Authorities.MERCHANT + "," + Authorities.AUTHENTICATED);
				}
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				user.setPassword(hashedPassword);
				history.setPassword(hashedPassword);
				user.setMobileStatus(Status.Active);
				user.setEmailStatus(Status.Active);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
			} else {
				LocationDetails location = locationDetailsRepository.findLocationByPin(dto.getLocationCode());
				String hashedPassword = passwordEncoder.encode(dto.getPassword());
				userDetail.setLocation(location);
				user.setAuthority(Authorities.USER + "," + Authorities.AUTHENTICATED);
				user.setPassword(passwordEncoder.encode(dto.getPassword()));
				user.setMobileStatus(Status.Inactive);
				user.setEmailStatus(Status.Active);
				history.setPassword(hashedPassword);
				user.setUsername(dto.getUsername().toLowerCase());
				user.setUserType(dto.getUserType());
				user.setMobileToken(CommonUtil.generateSixDigitNumericString());
				user.setEmailToken("E" + System.currentTimeMillis());
				user.setOsName(device);
			}
			user.setUserDetail(userDetail);
			PQAccountType nonKYCAccountType = pqAccountTypeRepository.findByCode("NONKYC");
			PQAccountDetail pqAccountDetail = new PQAccountDetail();
			pqAccountDetail.setBalance(0);
			pqAccountDetail.setVBankCustomer(dto.isVbankCustomer());
			pqAccountDetail.setVijayaAccountNumber(dto.getAccountNumber());
			pqAccountDetail.setBranchCode(dto.getBranchCode());
			pqAccountDetail.setAccountType(nonKYCAccountType);
			user.setAccountDetail(pqAccountDetail);
			User tempUser = userRepository.findByUsernameAndStatus(user.getUsername(), Status.Inactive);
			if (tempUser == null) {
				userDetailRepository.save(userDetail);
				pqAccountDetail.setAccountNumber(PayQwikUtil.BASE_ACCOUNT_NUMBER + userDetail.getId());
				pqAccountDetailRepository.save(pqAccountDetail);
				userRepository.save(user);
				history.setUser(user);
				passwordHistoryRepository.save(history);

			} else {
				userRepository.delete(tempUser);
				userDetailRepository.delete(tempUser.getUserDetail());
				user.setAccountDetail(tempUser.getAccountDetail());
				userDetailRepository.save(userDetail);
				userRepository.save(user);
				history.setUser(user);
				passwordHistoryRepository.save(history);
			}
			// mailSenderApi.sendNoReplyMail("Email Verification",
			// MailTemplate.VERIFICATION_EMAIL, user,null);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_MOBILE, user, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClientException("Service Down.Please try Again Later.");
		}
	}
	
	@Override
	public UserDTO checkPasswordTokenForSDKUser(String key) {
		UserDTO dto = null;
		User user = userRepository.findByOTPAndStatus(key, Status.Active);
		if (user != null) {
			dto = ConvertUtil.convertUser(user);
			return dto;
		} else {
			User inactiveUser = userRepository.findByOTPAndStatus(key, Status.Inactive);
			if (inactiveUser != null) {
				inactiveUser.setMobileStatus(Status.Active);
				inactiveUser.setMobileToken(null);
				userRepository.save(inactiveUser);
				smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.VERIFICATION_SUCCESS, user, null);
				dto = ConvertUtil.convertUser(inactiveUser);
				return dto;
			}
		}
		return null;
	}
	
	@Override
	public boolean resendMobileTokenSDKUser(String username) {
		User u = userRepository.findByUsernameAndStatus(username, Status.Inactive);
		if (u == null) {
			return false;
		} else {
			u.setMobileToken(CommonUtil.generateSixDigitNumericString());
			userRepository.save(u);
			smsSenderApi.sendUserSMS(SMSAccount.PAYQWIK_OTP, SMSTemplate.REGENERATE_OTP, u, null);
			return true;
		}
	}

}
