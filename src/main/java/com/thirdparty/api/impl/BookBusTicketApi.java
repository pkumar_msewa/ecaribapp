package com.thirdparty.api.impl;

import java.util.Date;

import com.payqwikapp.api.ITransactionApi;
import com.payqwikapp.entity.PQService;
import com.payqwikapp.entity.PQTransaction;
import com.payqwikapp.entity.TravelBusDetail;
import com.payqwikapp.entity.TravelBusTransaction;
import com.payqwikapp.entity.TravelSeatDetail;
import com.payqwikapp.entity.TravelUserDetail;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.Status;
import com.payqwikapp.repositories.BusDetailRepository;
import com.payqwikapp.repositories.BusTransactionRepository;
import com.payqwikapp.repositories.PassengerDetailsRepository;
import com.payqwikapp.repositories.SeatDetailRepository;
import com.payqwikapp.repositories.TravelUserDetailRepository;
import com.payqwikapp.util.StartupUtil;
import com.payqwikapp.validation.CommonValidation;
import com.thirdparty.api.IBookBusTicketApi;
import com.thirdparty.model.request.BookBusTicketRequest;

public class BookBusTicketApi implements IBookBusTicketApi {

	private final BusDetailRepository busDetailRepository;
	private final SeatDetailRepository seatDetailRepository;
	private final TravelUserDetailRepository travelUserDetailRepository;
	private final BusTransactionRepository busTransactionRepository;
	private final PassengerDetailsRepository passengerDetailsRepository;
	private final ITransactionApi transactionApi;
	
	
	public  BookBusTicketApi(BusDetailRepository busDetailRepository,SeatDetailRepository seatDetailRepository,
			TravelUserDetailRepository travelUserDetailRepository,BusTransactionRepository busTransactionRepository,
			PassengerDetailsRepository passengerDetailsRepository, ITransactionApi transactionApi) {
	this.busDetailRepository = busDetailRepository;
	this.seatDetailRepository = seatDetailRepository;
	this.travelUserDetailRepository = travelUserDetailRepository;
	this.busTransactionRepository = busTransactionRepository;
	this.passengerDetailsRepository = passengerDetailsRepository;
    this.transactionApi = transactionApi;
	}
	
	@Override
	public void saveBusTicket(BookBusTicketRequest req, User user, PQService service) {
		
		TravelSeatDetail seatDetail = new TravelSeatDetail();
		seatDetail.setFare(req.getFare());
		// seatDetail.setLadiesSeat(req.getLadiesSeat());
		// seatDetail.setNetFare(req.getNetFare());
		seatDetail.setNoOfSeats(req.getNoOfSeats());
		seatDetail.setSeatNo(req.getSeatNo());
		seatDetail.setCancelPolicy(req.getCancelPolicy());
		seatDetailRepository.save(seatDetail);
		
		TravelUserDetail userDetail = new TravelUserDetail();
		userDetail.setAddress(req.getAddress());
		userDetail.setAge(req.getAge());
		userDetail.setEmailId(req.getEmailId());
		userDetail.setGender(req.getGender());
		userDetail.setMobileNo(req.getMobileNo());
		userDetail.setName(req.getName());
		userDetail.setPostalCode(req.getPostalCode());
		travelUserDetailRepository.save(userDetail);
		
		TravelBusDetail busDetail = new TravelBusDetail();
		busDetail.setActualFare(req.getFare());
		busDetail.setBlockRefNo(req.getBlockingRefNo());
		busDetail.setBookingDate(req.getBookingDate());
		busDetail.setBookingRefNo(req.getBookingRefNo());
		busDetail.setBusTypeName(req.getBusTypeName());
		busDetail.setDepartureTime(req.getDepartureTime());
		busDetail.setDestinationId(req.getDestinationId());
		busDetail.setDestinationName(req.getDestinationName());
		busDetail.setJourneyDate(req.getJourneyDate());
		busDetail.setProvider(req.getProvider());
		busDetail.setReturnDate(req.getReturnDate());
		busDetail.setSourceId(req.getSourceId());
		busDetail.setSourceName(req.getSourceName());
		busDetail.setTravelOperator(req.getTravelOperator());
		busDetail.setTripId(req.getTripId());
		busDetail.setSeatDetail(seatDetail);
		busDetail.setUserDetail(userDetail);
		busDetail.setServiceCharge(req.getServiceCharge());
		busDetail.setServiceTax(req.getServiceTax());
		busDetailRepository.save(busDetail);

		double amount=Double.parseDouble(req.getFare());
		String transactionRefNo=req.getBookingRefNo();
		
		System.err.println("amount  "+amount);
		System.err.println("USERNAME    "+user.getUsername());
		System.err.println("referenceNOOOO   "+transactionRefNo);
		
		transactionApi.initiateBusBooking(amount,
				"Bus Ticket Booked Rs " + req.getFare() + " from " + req.getSourceName() + " to "
						+ req.getDestinationName() + " to RefNo. " + req.getBookingRefNo(),
						service, transactionRefNo,user.getUsername(), StartupUtil.TRAVEL,  "");
		PQTransaction transaction = transactionApi.getTransactionByRefNo(transactionRefNo + "D");
		if (transaction != null) {
			String busTransactionRefNo = req.getTransactionRefNo();
			TravelBusTransaction busTransaction = new TravelBusTransaction();
			busTransaction.setAmount(req.getFare());
			busTransaction.setBookingRefNo(req.getBookingRefNo());
			busTransaction.setBusTransactionRefNo(busTransactionRefNo);
			busTransaction.setStatus(Status.Initiated);
			busTransaction.setTransaction(transaction);
			busTransaction.setDescription(transaction.getDescription());
			busTransaction.setBlockRefNo(req.getBlockingRefNo());
			busTransaction.setBookingDate(req.getBookingDate());
			busTransaction.setBusDetail(busDetail);
			busTransactionRepository.save(busTransaction);
		}
	}

	@Override
	public void bookBusTicket(BookBusTicketRequest req, User user) {
		System.err.println("----------------------");
		PQTransaction transaction = transactionApi.getTransactionByRefNo(req.getTransactionRefNo() + "D");
		System.err.println("Trasaction :: " + transaction);
		if (transaction != null) {
			System.err.println("TRANSation failure " + transaction.getTransactionRefNo());
			transactionApi.successTravelBus(req.getTransactionRefNo());
		}

		TravelBusTransaction busTransaction = busTransactionRepository.findByTransactionRefNo(req.getTransactionRefNo());
		if (busTransaction != null) {
			TravelBusDetail busDetail = busTransaction.getBusDetail();
			busDetailRepository.updateTravelBusDetailByBusId(req.getApiRefNo(), req.getBlockId(), req.getBookingDate(),
					req.getClientId(), busDetail.getId());
			busTransaction.setStatus(Status.Booked);

			busTransactionRepository.save(busTransaction);

		}
	}

	@Override
	public void failBookBusTicket(BookBusTicketRequest req) {
		PQTransaction transaction = transactionApi.getTransactionByRefNo(req.getTransactionRefNo() + "D");
		System.err.println("Trasaction :: " + transaction);
		if (transaction != null) {
			System.err.println("Transation failure " + transaction.getTransactionRefNo());
			transactionApi.failedBillPaymentNew(req.getTransactionRefNo());
		}


	}

	@Override
	public boolean checkBalance(User user, BookBusTicketRequest req) {
		boolean valid = false;
		double amount = Double.parseDouble(req.getFare());
		if (CommonValidation.balanceCheck(user.getAccountDetail().getBalance(), amount)) {
			valid = true;
		}
		return valid;
	}

	/*@Override
	public ResponseDTO saveBusDetails(BookBusTicketRequest request) throws ClientException {
		
		BusDetails busDetails = new BusDetails();
		BusPassengerTrip busPassengerTrip = new BusPassengerTrip();
		
		busDetails.setBoardingId(request.getBoardingId());
		busDetails.setBoardingAddress(request.getBoardingAddress());
		busDetails.setSource(request.getSourceName());
		busDetails.setDestination(request.getDestinationName());
		busDetails.setBusOperator(request.getTravelOperator());
	//	busDetails.setBusType(BusType.valueOf(request.getBusTypeName()));
		busDetails.setCreated(new Date());
		
		PassengerDetails passengerDetails = new PassengerDetails();
		passengerDetails.setTitle(Salutation.valueOf(request.getSalutation()));
		passengerDetails.setName(request.getName());
		passengerDetails.setAge(Long.parseLong(request.getAge()));
		passengerDetails.setGender(Gender.valueOf(request.getGender()));
		passengerDetails.setMobile(request.getMobileNo());
		passengerDetails.setAlternateMobile(request.getEmergencyMobileNo());
		passengerDetails.setEmail(request.getEmailId());
		passengerDetails.setCreated(new Date());
		
		PQTransaction transaction = transactionApi.getTransactionByRefNo(request.getTransactionRefNo() + "D");
		System.err.println("Trasaction :: " + transaction);
		if (transaction != null) {
			String busTransactionRefNo = request.getTransactionRefNo();
			TravelBusTransaction busTransaction = new TravelBusTransaction();
			busTransaction.setAmount(request.getFare());
			busTransaction.setBookingRefNo(request.getBookingRefNo());
			busTransaction.setBusTransactionRefNo(busTransactionRefNo);
			busTransaction.setStatus(Status.Success);
			busTransaction.setTransaction(transaction);
			busTransaction.setDescription(transaction.getDescription());
			busTransaction.setBlockRefNo(request.getBlockingRefNo());
	//		busTransaction.setBookingDate(request.getBookingDate());
	//		busTransaction.setBusDetail(busDetails);
			
			busTransactionRepository.save(busTransaction);
		}
		
		BusTripDetails tripDetails = new BusTripDetails();
		tripDetails.setBlockingRefNo(request.getBlockingRefNo());
		tripDetails.setBookingRefNo(request.getBookingRefNo());
		tripDetails.setBookingStatus(Status.valueOf(request.getBookingStatus()));
		
		
		
		
		busDetailRepository.save(busDetails);
		passengerDetailsRepository.save(passengerDetails);
		
		
		return null;
	}
*/

}

