package com.thirdparty.api;

import com.payqwikapp.entity.LoginLog;
import com.payqwikapp.entity.SessionLog;
import com.payqwikapp.entity.User;
import com.payqwikapp.model.CommonRechargeDTO;
import com.payqwikapp.model.FrmLoadMoneyDTO;
import com.payqwikapp.model.RegisterDTO;
import com.payqwikapp.model.SendMoneyBankDTO;
import com.payqwikapp.model.SendMoneyMobileDTO;
import com.thirdparty.model.response.FRMResponseDTO;

public interface IFRMApi {

	FRMResponseDTO decideFundTransfer(SendMoneyBankDTO dto, User u);
	void saveFrmDetails(String contact, String decideResp, boolean enqueueResponse, String transactionRefNo,
			String requestFor);
	FRMResponseDTO decideRegistration(RegisterDTO user);
	FRMResponseDTO decideLogin(User user, SessionLog sessionLog, LoginLog loginLog);
	FRMResponseDTO decideLoadMoney(FrmLoadMoneyDTO redirectResponse, User user);
	FRMResponseDTO decideFundTransferWallet(SendMoneyMobileDTO dto, User user);
	FRMResponseDTO decideRecharge(CommonRechargeDTO dto, User user);

}
