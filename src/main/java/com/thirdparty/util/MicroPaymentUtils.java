package com.thirdparty.util;

import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

public class MicroPaymentUtils {

	private static final String ALGO = "AES";
	private static final String NIKKI = "NIKKEY";
	private static final String PAYLO = "LOOPAY";
	private static final String SECRET_KEY = "";

	private static final String CONSUMER_SECRET_KEY = "4AB49896E140D83EBD305482A522D898"; // 4AB49896E140D83EBD305482A522D898
	private static final String CONSUMER_TOKEN_KEY = "760825F4D5EEBA1D493AD0DFBD425A37"; // 760825F4D5EEBA1D493AD0DFBD425A37

	// private static final String CONSUMER_SECRET_KEY =
	// "phani@niki.ai|918106932532"; //4AB49896E140D83EBD305482A522D898
	// private static final String CONSUMER_TOKEN_KEY =
	// "918106932532|phani@niki.ai"; //760825F4D5EEBA1D493AD0DFBD425A37
	
	public static void main(String[] args) throws Exception {
//		System.err.println(md5("365y48jblc2d16nkcgajipg8m"));
//		System.err.println(MicroPaymentUtils.decrypt("g7vBcuoFyszBDjtd9mleiXFKfYDabLSoIS4%2FsWwO5so%3D", NIKKI+"7022620747"));
		
	}

	public static String generateToken(String sessionId) throws Exception {
		String token = md5(sessionId);
		return token;
	}

	public static String md5(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}

	public static String generateSecret(String number, String sessionId) {
		String enc = null;
		try {
			enc = encrypt(NIKKI + number, sessionId);
		} catch (Exception e) {

		}
		return enc;
	}

	public static String generatePayloSecret(String number, String sessionId) {
		String enc = null;
		try {
			enc = encrypt(PAYLO + number, sessionId);
		} catch (Exception e) {

		}
		return enc;
	}

	public static String encrypt(String mobileNumber, String sessionId) {
		String ss = null;
		try {
			Key key = generateKey(mobileNumber);
			Cipher c = Cipher.getInstance(ALGO);
			c.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = c.doFinal(sessionId.getBytes());
			String encryptedValue = Base64.getEncoder().encodeToString(encVal);
			ss = URLEncoder.encode(encryptedValue, "UTF-8");
		} catch (Exception e) {

		}
		return ss;
	}
	public static String decrypt(String encryptedData, String mobileNumber) {
		String decryptedValue = null;
		try {
			String d = URLDecoder.decode(encryptedData, "UTF-8");
			Key key = generateKey(mobileNumber);
			Cipher c = Cipher.getInstance(ALGO);
			c.init(Cipher.DECRYPT_MODE, key);
			byte[] decordedValue = Base64.getDecoder().decode(d);
			byte[] decValue = c.doFinal(decordedValue);
			decryptedValue = new String(decValue);
		} catch (Exception e) {
              e.printStackTrace();
		}
		return decryptedValue;
	}
	
	
	private static Key generateKey(String workingKey) throws Exception {
		byte[] keyValue = workingKey.getBytes();
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}

	public static String getSessionId(String secretKet, String mobileNumber) {
		String sessionId = null;
		try {
			System.err.println("secretkey ::" + secretKet);
			System.err.println("mobileNumber ::" + mobileNumber);
			sessionId = decrypt(secretKet, NIKKI + mobileNumber);
			System.err.println("sessionid ::" + sessionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sessionId;
	}

	public static String getPayloSessionId(String secretKet, String mobileNumber) {
		String sessionId = null;
		try {
			sessionId = decrypt(secretKet, PAYLO + mobileNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sessionId;
	}

	public static boolean checkTokenKey(String sessionId, String secretKey) throws Exception {
		boolean flag = false;
		try {

			String calculatedSecretKey = MicroPaymentUtils.generateToken(sessionId);
			if (calculatedSecretKey.equals(secretKey)) {
				flag = true;
			}
		} catch (Exception e) {

		}
		return flag;
	}

	public static boolean checkSecretKey(String mobileNumber, String sessionId, String secretKey) throws Exception {
		boolean flag = false;
		try {
			String calculatedSecretKey = MicroPaymentUtils.decrypt(secretKey, NIKKI + mobileNumber);
			if (calculatedSecretKey.equals(sessionId)) {
				flag = true;
			}
		} catch (Exception e) {

		}
		return flag;
	}

	public static boolean checkConsumerSecretKey(String consumerSecretKey) {

		boolean flag = false;
		if (consumerSecretKey.equals(CONSUMER_SECRET_KEY)) {
			flag = true;
		}
		return flag;
	}

	public static boolean checkConsumerTokenKey(String consumerTokenKey) {

		boolean flag = false;
		if (consumerTokenKey.equals(CONSUMER_TOKEN_KEY)) {
			flag = true;
		}
		return flag;
	}
}
