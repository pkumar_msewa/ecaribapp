package com.thirdparty.util;

import com.payqwikapp.util.DeploymentConstants;

public class FRMConstants {
	
	public static String frmFtDecide= DeploymentConstants.WEB_URL+"/frm/decideFundTransfer";
	public static String frmFtEnqueue= DeploymentConstants.WEB_URL+"/frm/enqueueFundTransfer";
	
	public static String frmRegDecide= DeploymentConstants.WEB_URL+"/frm/decideRegistration";
	public static String frmRegEnqueue= DeploymentConstants.WEB_URL+"/frm/enqueueRegistration";
	
	public static String frmLoginDecide= DeploymentConstants.WEB_URL+"/frm/decideLogin";
	
	public static String channel="WALLET";
	public static String getAdvice(boolean success){
		return success ? "ALLOW": "DECLINE";
	}
	public static String funTr="FUND TRANSFER";
	public static String registration ="REGISTRATION";
	public static String dbLogin ="LOGIN";
}
