package com.thirdparty.util;
/*
 * MIT License
 *
 * Copyright (c) 2017 Kavin Varnan

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * MIT License
 *
 * Copyright (c) 2017 Kavin Varnan

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jettison.json.JSONObject;

import com.loopj.android.http.Base64;
import com.payqwikapp.model.VisaDTO;

public class CryptLib {
	
	

    /**
     * Encryption mode enumeration
     */
    private enum EncryptMode {
        ENCRYPT, DECRYPT;
    }

    // cipher to be used for encryption and decryption
    Cipher _cx;

    // encryption key and initialization vector
    byte[] _key, _iv;

    public CryptLib() throws NoSuchAlgorithmException, NoSuchPaddingException {
        // initialize the cipher with transformation AES/CBC/PKCS5Padding
        _cx = Cipher.getInstance("AES/CBC/PKCS5Padding");
        _key = new byte[32]; //256 bit key space
        _iv = new byte[16]; //128 bit IV
    }

    /**
     * Note: This function is no longer used.
     * This function generates md5 hash of the input string
     * @param inputString
     * @return md5 hash of the input string
     */
    public static final String md5(final String inputString) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(inputString.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     *
     * @param _inputText
     *            Text to be encrypted or decrypted
     * @param _encryptionKey
     *            Encryption key to used for encryption / decryption
     * @param _mode
     *            specify the mode encryption / decryption
     * @param _initVector
     * 	      Initialization vector
     * @return encrypted or decrypted string based on the mode
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private String encryptDecrypt(String _inputText, String _encryptionKey,
                                  EncryptMode _mode, String _initVector) throws UnsupportedEncodingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        String _out = "";

        int len = _encryptionKey.getBytes("UTF-8").length; // length of the key	provided

        if (_encryptionKey.getBytes("UTF-8").length > _key.length)
            len = _key.length;

        int ivlen = _initVector.getBytes("UTF-8").length;

        if(_initVector.getBytes("UTF-8").length > _iv.length)
            ivlen = _iv.length;

        System.arraycopy(_encryptionKey.getBytes("UTF-8"), 0, _key, 0, len);
        System.arraycopy(_initVector.getBytes("UTF-8"), 0, _iv, 0, ivlen);
        //KeyGenerator _keyGen = KeyGenerator.getInstance("AES");
        //_keyGen.init(128);

        SecretKeySpec keySpec = new SecretKeySpec(_key, "AES"); // Create a new SecretKeySpec
        // for the
        // specified key
        // data and
        // algorithm
        // name.

        IvParameterSpec ivSpec = new IvParameterSpec(_iv); // Create a new
        // IvParameterSpec
        // instance with the
        // bytes from the
        // specified buffer
        // iv used as
        // initialization
        // vector.

        // encryption
        if (_mode.equals(EncryptMode.ENCRYPT)) {
            // Potentially insecure random numbers on Android 4.3 and older.
            // Read
            // https://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
            // for more info.
            _cx.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);// Initialize this cipher instance
            byte[] results = _cx.doFinal(_inputText.getBytes("UTF-8")); // Finish
            // multi-part
            // transformation
            // (encryption)
            _out = Base64.encodeToString(results, Base64.DEFAULT); // ciphertext
            // output
        }

        // decryption
        if (_mode.equals(EncryptMode.DECRYPT)) {
            _cx.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);// Initialize this ipher instance

            byte[] decodedValue = Base64.decode(_inputText.getBytes("UTF-8"),Base64.DEFAULT);
            byte[] decryptedVal = _cx.doFinal(decodedValue); // Finish

            _out = new String(decryptedVal);
        }
        return _out; // return encrypted/decrypted string
    }

    /***
     * This function computes the SHA256 hash of input string
     * @param text input text whose SHA256 hash has to be computed
     * @param length length of the text to be returned
     * @return returns SHA256 hash of input text
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String SHA256 (String text, int length) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        String resultStr;
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(text.getBytes("UTF-8"));
        byte[] digest = md.digest();

        StringBuffer result = new StringBuffer();
        for (byte b : digest) {
            result.append(String.format("%02x", b)); //convert to hex
        }
        //return result.toString();

        if(length > result.toString().length())
        {
            resultStr = result.toString();
        }
        else
        {
            resultStr = result.toString().substring(0, length);
        }

        return resultStr;

    }

    /***
     * This function encrypts the plain text to cipher text using the key
     * provided. You'll have to use the same key for decryption
     *
     * @param _plainText
     *            Plain text to be encrypted
     * @param _key
     *            Encryption Key. You'll have to use the same key for decryption
     * @param _iv
     * 	    initialization Vector
     * @return returns encrypted (cipher) text
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */

    public String encrypt(String _plainText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        return encryptDecrypt(_plainText, _key, EncryptMode.ENCRYPT, _iv);
    }

    public String encryptSimple(String _plainText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException {
        return encryptDecrypt(_plainText, CryptLib.SHA256(_key, 32), EncryptMode.ENCRYPT, _iv);
    }


    /***
     * This funtion decrypts the encrypted text to plain text using the key
     * provided. You'll have to use the same key which you used during
     * encryprtion
     *
     * @param _encryptedText
     *            Encrypted/Cipher text to be decrypted
     * @param _key
     *            Encryption key which you used during encryption
     * @param _iv
     * 	    initialization Vector
     * @return encrypted value
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public String decrypt(String _encryptedText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        return encryptDecrypt(_encryptedText, _key, EncryptMode.DECRYPT, _iv);
    }

    public String decryptSimple(String _encryptedText, String _key, String _iv)
            throws InvalidKeyException, UnsupportedEncodingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException {
        return encryptDecrypt(_encryptedText, CryptLib.SHA256(_key, 32), EncryptMode.DECRYPT, _iv);
    }

    /**
     * this function generates random string for given length
     * @param length
     * 				Desired length
     * * @return
     */
    public static String generateRandomIV(int length)
    {
        SecureRandom ranGen = new SecureRandom();
        byte[] aesKey = new byte[16];
        ranGen.nextBytes(aesKey);
        StringBuffer result = new StringBuffer();
        for (byte b : aesKey) {
            result.append(String.format("%02x", b)); //convert to hex
        }
        if(length> result.toString().length())
        {
            return result.toString();
        }
        else
        {
            return result.toString().substring(0, length);
        }
    }
	
	public static VisaDTO getVisaDTOIOS(String encryptedString,String key,String iv) {
		
		VisaDTO visaDTO = new VisaDTO();
		try {
			CryptLib cryptLib = new CryptLib();
			String s = cryptLib.decryptSimple(encryptedString, key, iv);
			System.out.println("result"+s);
			org.json.JSONObject obj = new org.json.JSONObject(s.replace("\\", ""));
			visaDTO.setData(obj.getString("data"));
			visaDTO.setMerchantAddress(obj.getString("merchantAddress"));
			visaDTO.setMerchantId(obj.getString("merchantId"));
			visaDTO.setRequest(obj.getString("request"));
			visaDTO.setSessionId(obj.getString("sessionId"));
			visaDTO.setMerchantName(obj.getString("merchantName"));
			visaDTO.setAmount(Double.parseDouble(obj.getString("data")));
			visaDTO.setAdditionalData(obj.getString("additionalData"));
			visaDTO.setAccountNumber(obj.getString("accountNumber"));
            System.err.println("visadto ::" + visaDTO.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return visaDTO;
	}
	
	public static void main(String... a) {
		
//		{"request": "000201010211061661002401111415430827CITI000000300000050104034485204581253033565802IN5917CUB Test Merchant6007CHENNAI610660006162260111BILLNUMB0010707500010163045361","data": 2,"sessionId": "EBD61A7B6AA753B9B5C6638205E8EF8D","merchantName": "CUB Test Merchant","merchantId": "6100240111141543","merchantAddress": "CHENNAI","additionalData": "Extra field=;","accountNumber": "5087070200000824"}
		
//		VisaDTO visaDTO = getVisaDTOIOS("U%2B5BNxz8mmTI5ESW%2B7V48n3fDWM8XBFBFqlgJ0h87PgZVik7%2FPX1bZT4cEM65Bf5REFhB4EoEXr4%0AdmCIN%2Fy%2F%2FRngjTi5N5vBu3045ILHtNmncvSGabLu%2FgTeA3nNIQ05UPbtatuEmd1nl4EC5S%2FwDOJU%0AoB8I7QiotWvaowNoEMEfpN5kMJWWePBrXszbRxd%2FDlYZzo0Cn0S1Z6NA%2F0w1jz8bZrMGeJbb9NXK%0AO3BV3H9q3zpKlvqNzLyaxN8QUe1hJ2lbi1BhsJzqxR%2BZpcpS%2Fum4DM5EoSF0kAMPSf8hknWqKX30%0AYMjy%2Fp3Pl%2Bdy6x%2BU8JwWgOmgy7X5gxNdOdazbjJPgLz3PZfz4ybrHF0JhIzx0kK%2Bl509JYh1HhM1%0Azhbd6TjqkVnvGRWS1GrJpHHyeKpRU5f3fUrk9EQ1BZa103T9HPQq70T2bQ468j2xXUsJQGAixltp%0A3P5cy6pQ7QyiQh4hi4bdUhMevu40gvzH%2FdizbO8ibmpl0DZ4Bc%2FdrCJtP%2FWaBq2a%2B%2F57tv2gYn4X%0AseSQI%2FCJ0DHS5nmI0pcQ9gRY%2F9c7Z0PLTMIATBe4hjQH%0A=IOStype");
//		VisaDTO visaDTO = getVisaDTOIOS("qfDwNv7pqx7ZWlKulKusRBX1LG%2BMbONojo7029ltuo1Y7Cr9gDDfG0DDKmi2kPirOIh6MCAWLMFq8X1Hj5oicrV%2B0ri6IDDEnIudP0t/keCMh9uBXx%2B2eYaqNzfvl1QP3ckoAMzTn0e/YbIonDk6JVyGxCHniIBHHipB8xOvwu/wLhtT77%2Bk8qeCfwK8VFsx6GMvOQ3bxPjluULuQfJw3HdsTyyAg2r/7E3zntmHKvsOYhjstGiO5p2NHSLLtWXfKNxBQ7OLGqHNFVoSE7M7hq65I34jx66q7HzLhn5r7IgINwfxBQkzw0%2Bsg7upRnI7Ib7HYAObFkTgCT5YWjiO3MyCk7MpcwYS9aybnNGeUsEqIDwmA%2BkmeG4TU9nuqCmrH1DsNDrVEAJacBP/oOIYPos2NOvQ/30GlJ8spiFvbHX0R6M5mOhcjMw5ePO1StUl6ElOoJGY11vnSVYJ%2BMYpmwFtBZb8JMxmgB0MFCq6ex4PaOdIhIcO4Jh%2BAzgp1Vc3V/WC4BEquwW5J1ABh0PuBv%2BavozhYQyKSwVnmimHCZfDJaZtvuBxh8R9s6VSxWOJzFWynFKjLEvvc1wiqJfG5cP4pVOt24LfD9r6rhmgBrs=IOStype");
//		System.out.println("DTO :: "+visaDTO);
		try {
			JSONObject obj = new JSONObject();
			obj.put("sessionId", "mutghfj9vasv7sy0qumhktwr");
			obj.put("request", "00020101021102164054560000168096061661003000001179600811VIJB00013315204541153033565802IN5917SREE SAI CATERERS6009Bangalore61065600726304CB5A");
//			obj.put("request", "000201010211061661002401111415430827CITI000000300000050104034485204581253033565802IN5917CUB Test Merchant6007CHENNAI610660006162260111BILLNUMB0010707500010163045361");
			obj.put("data", "2");
			obj.put("merchantName", "SREE SAI CATERERS");
			obj.put("merchantId", "6100300000117960");
			obj.put("merchantAddress", "BANGALORE");
			obj.put("additionalData", "");
			obj.put("accountNumber", "5087070200009460");
			String plainText = StringEscapeUtils.escapeJava(obj.toString().trim());
			String key = "VIJAYSTHJKUOKNGA";
			String iv = "1234567890123456";
			CryptLib cryptLib = new CryptLib();
			String encryptedString = cryptLib.encryptSimple(plainText, key, iv);
			System.out.println("encryptedString=" + encryptedString);
//			VisaDTO visa = getVisaDTOIOS("qfDwNv7pqx7ZWlKulKusRBX1LG%2BMbONojo7029ltuo0hVQHbtcu7uerTmEzyPYbGeuVdNAXPaWL9XXSwZVlHjTPQ4wmf3RYt50V1r3nf9gb5djqkXotiWFRlq/O3fSHmWlMy99gUQTxUjxP%2B3x3JJ/xkSED0YVI3Zb9Asg5vfVhNVWqBGTvvNRhuw3DJoChIZ9KEzADmocYxk1MJfQk6fj652dR/T5lUYlb0oKaaHXzt7Kwyo5lA2NJs1c9LS1y37vb%2BZxaQQ2tvZCF8z3CEQc32pzI5xX95vY5zYFLLJYrWCpxfcGFt05WT63etISJS5TE4f%2Bt96%2BuGGXQ1fcKRykNqyePyGvktJan4P6Db9POwtViSKdtAwPxv9h4GoO9MNHwO8NpLYlSIiRKCGJVtBA1eqat9SwWbBf8m1GbMsNq2%2B5Vlc76GBpRcPhZXiCaD9/h0JP%2BF8rvO8UKEsxQ5B7UWNaEicu/ANgEIiLKlwy43YUji4o0w2fFDKzz7LPNqYx5H7PL9JIvwd/gOnYaaREUeUf2x0odTIqoNwH5JEz0=IOStype");
//			String encryptedString="CQJctB9vw8L3+aoc8sqQW/xRilj3ehjk/x3tdLuTCwVLHDKVjPc6GXFNzCmLyvLpdqYaoYyUJ2Le6BfKskonBx7whgX9alhuvUGK4Y5pEdJa6so2q8pJda+nXSIUwcEa4jReNeNaBQbPY/CQiL8KeGA8IPTqYYjpBcexdXq7dMw0a4so2NN90TKGc6KF2icdNUtJFfO/RBS0fYXJHy1rZduhGImBpX9/d/8cs0g91fFsJjHW9/fPGz/YUIOjme1d8DpOazh9XhnDfLWfzgDbPPYd+WBQ4sNXvsHxqaB1BZYQ43em07MFoPtb9uPLq9bIWD/6+WsVYv2i9K48G7fl9adkZJM4IYPnJe6bMgMMMfuiMrC1KhvJJqkfk8g11H2ku5f53BglAExFDOLiqcSlimbdXUfyQ6uOac+G93E66+jCzHPxGmdBWhx+/3QNV51KCBP6HR3Fq7A290Z7/3KPJxJuAdSoybS1b5fTrdRb6azZq4/jHuBpPVvGuFgf63HN2LN6HKfek6fDu21CXrK+EZ38XCW67toqr94WCDs5UHw=";
//			VisaDTO visa=getVisaDTOIOS(encryptedString);
			System.out.println("decryptedString " +  cryptLib.decryptSimple(encryptedString,key, iv));
	

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}