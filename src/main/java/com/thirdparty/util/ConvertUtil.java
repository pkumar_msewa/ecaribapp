package com.thirdparty.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.payqwikapp.entity.PGDetails;
import com.payqwikapp.entity.TPTransaction;
import com.payqwikapp.entity.UpiPgDetails;
import com.payqwikapp.util.DeploymentConstants;
import com.payqwikapp.util.SecurityUtil;
import com.thirdparty.model.request.S2SRequest;
import com.thirdparty.model.request.StatusResponse;
import com.upi.model.UpiRequest;
import com.upi.util.UPIBescomConstants;

public class ConvertUtil {
	public static String convertMerchantDetail(PGDetails pgDetails) {
		return pgDetails.getToken() + "|" + pgDetails.getSuccessURL() + "|" + pgDetails.getReturnURL();
	}

	public static StatusResponse convertTransaction(TPTransaction tpTransaction) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setAmount(String.valueOf(tpTransaction.getAmount()));
		statusResponse.setTransactionDate(sdf.format(tpTransaction.getCreated()));
		statusResponse.setPaymentId(tpTransaction.getTransactionRefNo());
		statusResponse.setStatus(tpTransaction.getStatus());
		statusResponse.setMerchantRefNo(tpTransaction.getOrderId());
		return statusResponse;
	}
	
	
	public static String convertDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String conDate = sdf.format(date);
		return conDate;
	}
	
	public static String convertBescomRuralRequest(S2SRequest dto, TPTransaction tp) throws Exception{
		String Op1 = "";
		String Op2 = "";
		String Op3 = "";
		String Op4 = "";
		String Op5 = "";
		String ComputeHashValue = UPIBescomConstants.BECOM_RURAL_KEY + "|" + UPIBescomConstants.BECOM_RURAL_TOKEN + dto.getMerchanttxnid() + "|" + dto.getPgtransactionid() + "|" + tp.getCustDetails().getConsumerid() + "|"
				+ dto.getBillamount() + "|" + tp.getCustDetails().getLocaltionCode() + "|" + tp.getCustDetails().getBillNo() + "|" + dto.getTransactiondatetime() + "|" + dto.getEmail()	 + "|" + Op1 + "|"
				+ Op2 + "|" + Op3 + "|" + Op4 + "|" + Op5;
		
		String EncData = SecurityUtil.md5(ComputeHashValue);
		
		String Msg =  dto.getStatus() + "|" + getBescomRuralCode(dto.getCode()) + "|" + UPIBescomConstants.BECOM_RURAL_KEY + "|" + UPIBescomConstants.BECOM_RURAL_TOKEN + "|" + dto.getMerchanttxnid() + "|" + dto.getPgtransactionid()
				+ "|" +  tp.getCustDetails().getConsumerid() + "|" + dto.getBillamount() + "|" + tp.getCustDetails().getLocaltionCode() + "|" + tp.getCustDetails().getBillNo() + "|" + dto.getTransactiondatetime() + "|"
				+ dto.getEmail() + "|" + EncData + "|" + Op1 + "|" + Op2 + "|" + Op3 + "|" + Op4 + "|" + Op5;
		return Msg;
	}
	
	
	public static String getBescomRuralCode(String code){
		if(code.equals("S00")){
			return "100";
		}
		return code;
	}
	
	public static UpiRequest getUpiRequest(UpiRequest req, UpiPgDetails upi) {
		if(DeploymentConstants.PRODUCTION){
			req.setKek(upi.getKekLive());
			req.setTxnPwd(upi.getTxnPwdLive());
			req.setPayeeVirAddr(upi.getPayeeVirAddrLive());
			req.setPayeeMobile(upi.getPayeeMobileLive());
			req.setMerchantId(upi.getMerchantIdLive());
			req.setVpaTerminalId(upi.getVpaTerminalIdLive());
			req.setPlanDek(upi.getPlanDekLive());
			req.setPublicKey(upi.getPublicKeyLive());
			req.setOrgId(upi.getOrgIdLive());
		}else{
			req.setKek(upi.getKekTest());
			req.setTxnPwd(upi.getTxnPwdTest());
			req.setPayeeVirAddr(upi.getPayeeVirAddrTest());
			req.setPayeeMobile(upi.getPayeeMobileTest());
			req.setMerchantId(upi.getMerchantIdTest());
			req.setVpaTerminalId(upi.getVpaTerminalIdTest());
			req.setPlanDek(upi.getPlanDekTest());
			req.setPublicKey(upi.getPublicKeyTest());
			req.setOrgId(upi.getOrgIdTest());
		}
		req.setSubMerchantId(upi.getSubMerchantId());
		req.setTerminalId(upi.getTerminalId());
		return req;
	}
	
	
	
	public static void main(String[] args) {
		System.err.println(convertDate(new Date()));
		Calendar now = Calendar.getInstance();
		System.err.println("year ::" + now.get(Calendar.YEAR));
		System.err.println("month ::" + (now.get(Calendar.MONTH) + 1));
		System.err.println("day ::" + now.get(Calendar.DATE));
	}
}
