package com.ebs.api;

import com.ebs.model.EBSRequest;
import com.ebs.model.EBSStatusRequest;
import com.ebs.model.EBSStatusResponse;

public interface IEBSRequestHandlerApi {

	EBSRequest request(EBSRequest ebsRequest);
	
	EBSStatusResponse statusApi(EBSStatusRequest request);
}
