package com.ebs.model;

public class EBSStatusResponse {
	
	private boolean success;
	private boolean valid;
	private String transactionId;
	private String ebsDate;
	private String paymentId;
	private String ebsStatus;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getEbsDate() {
		return ebsDate;
	}

	public void setEbsDate(String ebsDate) {
		this.ebsDate = ebsDate;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getEbsStatus() {
		return ebsStatus;
	}

	public void setEbsStatus(String ebsStatus) {
		this.ebsStatus = ebsStatus;
	}
}
