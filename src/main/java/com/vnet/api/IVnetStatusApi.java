package com.vnet.api;

import com.payqwikapp.model.VNetStatusRequest;
import com.payqwikapp.model.VNetStatusResponse;

public interface IVnetStatusApi {

	VNetStatusResponse vnetVarification(VNetStatusRequest dto);
}
