package com.vnet.api.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.payqwikapp.model.VNetStatusRequest;
import com.payqwikapp.model.VNetStatusResponse;
import com.payqwikapp.util.VNetUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.vnet.api.IVnetStatusApi;

public class VnetStatusApiImpl implements IVnetStatusApi {

	@Override
	public VNetStatusResponse vnetVarification(VNetStatusRequest request) {
		VNetStatusResponse result = new VNetStatusResponse();
		try{
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("PID", VNetUtils.VNET_PID);
			formData.add("PRN", request.getTxnRef());
			formData.add("ITC", request.getTxnRef());
			formData.add("AMT", request.getAmount());
			WebResource resource = client.resource(VNetUtils.VNET_STATUS);
			ClientResponse response = resource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("string response ::" + strResponse);
			if(strResponse != null){
				Document html = Jsoup.parse(strResponse);
				String h4 = html.body().getElementsByTag("h4").text();
				if(h4.equals("Your Payment is Successful")){
					System.err.println("TRUE");
					result.setSuccess(true);
					result.setStatus("Success");
					System.out.println("Afte parsing, Heading : " + h4);
				}else{
					result.setSuccess(false);
					result.setStatus("Failed");
					System.err.println("FALSE");
					System.out.println("Afte parsing, Heading : " + h4);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setStatus("Failed");
		}
		return result;
	}
}
